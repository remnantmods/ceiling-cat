using System;
using System.Collections.Generic;
using System.Text;

namespace H2.DataTypes
{
    public enum RawLocations
    {
        Internal,
        MainMenu,
        Shared,
        SinglePlayerShared,
        Unknown
    }
}
