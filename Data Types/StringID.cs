using System;
using System.Collections.Generic;
using System.Text;

namespace H2.DataTypes
{
    [Serializable]
    public class StringID
    {
        private char[] chars;
        private short _length;
        public short Length { get { return _length; } }

        public StringID()
        { chars = new char[128]; _length = 128; }

        public StringID(string name)
        { chars = name.ToCharArray(0, 128); _length = (short)name.TrimEnd(new char[]{'\0'}).Length; }

        public new string ToString()
        { return new string(chars, 0, Length); }

        public char[] ToCharArray()
        { return chars; }

        public void Rename(string newName)
        {
            char[] _chars = newName.ToCharArray();
            Array.Resize<char>(ref _chars, 128);
            chars = _chars;
            _length = (short)newName.Length;
        }
    }
}
