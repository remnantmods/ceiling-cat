using System;
using System.Collections.Generic;
using System.Text;

namespace H2.DataTypes
{
    [Serializable]
    public class Enum
    {
        #region Instances
        private long __value;
        private string[] __names;
        private int __length;
        #endregion

        #region Accessors
        public long Value { get { return __value; } set { __value = value; } }
        public string[] ValueNames { get { return __names; } set { __names = value; } }
        public int ByteCountLength { get { return __length; } }
        #endregion

        public Enum(string[] valueNames, int byteCountLength)
        {
            ValueNames = valueNames;
            __length = byteCountLength;
            Value = 0;
        }

        public Enum(byte ByteValue)
        {
            Value = (long)ByteValue;
            __length = 1;
        }

        public Enum(short ShortValue)
        {
            Value = (long)ShortValue;
            __length = 2;
        }

        public Enum(int IntValue)
        {
            Value = (long)IntValue;
            __length = 4;
        }

        public Enum(long LongValue)
        {
            Value = LongValue;
            __length = 4;
        }
    }
}
