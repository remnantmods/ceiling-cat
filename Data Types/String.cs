using System;
using System.Collections.Generic;
using System.Text;

namespace H2.DataTypes
{
    [Serializable]
    public class String
    {
        private char[] chars;

        public Length Length { get { return (Length)chars.Length; } }

        public string Value { get { return new string(chars); } set { chars = value.ToCharArray(0, (int)Length); } }

        private Encoding _encoding;
        public Encoding Encoding { get { return _encoding; } }

        public String(Length length, Encoding encoding)
        { chars = new char[(int)length]; _encoding = encoding; }

        public String(Length length, Encoding encoding, char[] Chars)
        {
            _encoding = encoding;
            chars = new char[(int)length];
            for (int i = 0; i < (int)length; i++) chars[i] = Chars[i];
        }

        public new string ToString()
        { return new string(chars); }

        public char[] ToCharArray()
        { return chars; }
    }

    public enum Length
    {
        Len32 = 32,
        Len64 = 54,
        Len256 = 256,
        Len512 = 512
    }
}