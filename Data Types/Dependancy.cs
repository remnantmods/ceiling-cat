using System;
using System.Collections.Generic;
using System.Text;

namespace H2.DataTypes
{
    [Serializable]
    public class Dependancy
    {
        char[] _class = new char[] { '\0', '\0', '\0', '\0' };
        string _path = "";

        public string ClassString
        {
            get
            {
                char[] c = (char[])_class.Clone();
                Array.Reverse(c);
                return new string(c);
            }
            set
            {
                char[] c = value.ToCharArray(0, 4);
                Array.Reverse(c);
                _class = c;
            }
        }
        public char[] Class { get { return _class; } set { _class[0] = value[0]; _class[1] = value[1]; _class[2] = value[2]; _class[3] = value[3]; } }

        public string Path { get { return _path; } set { _path = value; } }

        public Dependancy()
        { }

        public new string ToString()
        { 
            return ClassString + " - " + Path;
        }
    }
}
