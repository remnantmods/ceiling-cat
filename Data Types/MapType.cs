using System;
using System.Collections.Generic;
using System.Text;

namespace H2.DataTypes
{
    public enum MapType
    {
        SinglePlayerMap,
        MultiplayerMap,
        MainMenu,
        Shared,
        SinglePlayerShared
    }
}
