using System;
using System.Collections.Generic;
using System.Text;
using H2.DataTypes;

namespace H2.Sections
{
    public class Header
    {
        private MapStream Map;

        internal Header(MapStream map)
        {
            Map = map;
            Map.IndexOffset = IndexOffset;
            Map.MetaStart = MetaStart;
            Map.FileTableOffset = FileTableOffset;
        }

        #region Data
        /// <summary>
        /// Read Or Write Head
        /// </summary>
        public string Head { get { return Map.ReadReverseStringAt(0, 4, false, true); } set { Map.WriteReverseStringAt(0, value.Substring(0, 4), true); } }
        /// <summary>
        /// Read Or Write Version
        /// </summary>
        public int Version { get { return Map.ReadInt32At(4, true); } set { Map.WriteAt(4, value, true); } }
        /// <summary>
        /// Read Or Write Filesize
        /// </summary>
        public int Filesize { get { return Map.ReadInt32At(8, true); } set { Map.WriteAt(8, value, true); } }
        /// <summary>
        /// Read Or Write IndexOffset
        /// </summary>
        public int IndexOffset { get { return Map.ReadInt32At(16, true); } set { Map.WriteAt(16, value, true); Map.IndexOffset = value; Map.TagInfoIndexOffset = Map.Index.TagInfoIndexOffset; Map.SecondaryMagic = Map.Index.SecondaryMagic; Map.TagInfoCount = Map.Index.TagCount; } }
        /// <summary>
        /// Read Or Write IndexSize
        /// </summary>
        public int IndexSize { get { return Map.ReadInt32At(20, true); } set { Map.WriteAt(20, value, true); } }
        /// <summary>
        /// Calculate MetaStart
        /// </summary>
        public int MetaStart { get { return IndexOffset + IndexSize; } }
        /// <summary>
        /// Read Or Write MetaSize
        /// </summary>
        public int MetaSize { get { return Map.ReadInt32At(24, true); } set { Map.WriteAt(24, value, true); } }
        /// <summary>
        /// Read Or Write NonRawSize
        /// </summary>
        public int NonRawSize { get { return Map.ReadInt32At(28, true); } set { Map.WriteAt(28, value, true); } }
        /// <summary>
        /// Read Or Write Origin
        /// </summary>
        public string Origin { get { return Map.ReadStringAt(32, 256, true, true); } set { Map.WriteAt(32, value.Substring(0, 256), true); } }
        /// <summary>
        /// Read Or Write BuildDate
        /// </summary>
        public string BuildDate { get { return Map.ReadStringAt(288, 32, true, true); } set { Map.WriteAt(288, value.Substring(0, 30), true); } }
        /// <summary>
        /// Read Or Write MapType
        /// </summary>
        public int MapTypeInt { get { return Map.ReadInt32At(320, true); } set { Map.WriteAt(320, value, true); } }
        /// <summary>
        /// Read Or Write Enum Value Of Map Type
        /// </summary>
        public MapType MapType { get { return (MapType)Map.ReadInt32At(320, true); } set { Map.WriteAt(320, (int)value, true); } }
        /// <summary>
        /// Read Or Write StrangeFileStringsSize
        /// </summary>
        public int StrangeFileStringsSize { get { return Map.ReadInt32At(340, true); } set { Map.WriteAt(340, value, true); } }
        /// <summary>
        /// Read Or Write StrangeFileStringsOffset
        /// </summary>
        public int StrangeFileStringsOffset { get { return Map.ReadInt32At(344, true); } set { Map.WriteAt(344, value, true); } }
        /// <summary>
        /// Read Or Write StringsOffset128
        /// </summary>
        public int StringIDs128Offset { get { return Map.ReadInt32At(352, true); } set { Map.WriteAt(352, value, true); } }
        /// <summary>
        /// Read Or Write StringCount
        /// </summary>
        public int StringIDCount { get { return Map.ReadInt32At(356, true); } set { Map.WriteAt(356, value, true); } }
        /// <summary>
        /// Read Or Write StringsSize
        /// </summary>
        public int StringIDsSize { get { return Map.ReadInt32At(360, true); } set { Map.WriteAt(360, value, true); } }
        /// <summary>
        /// Read Or Write StringsIndex
        /// </summary>
        public int StringIDsIndex { get { return Map.ReadInt32At(364, true); } set { Map.WriteAt(364, value, true); } }
        /// <summary>
        /// Read Or Write StringsOffset
        /// </summary>
        public int StringIDsOffset { get { return Map.ReadInt32At(368, true); } set { Map.WriteAt(368, value, true); } }
        /// <summary>
        /// Read Or Write InternalName
        /// </summary>
        public string InternalName { get { return Map.ReadStringAt(408, 32, true, true); } set { Map.WriteAt(348, value.Substring(0, 32), true); } }
        /// <summary>
        /// Read Or Write ScenarioName
        /// </summary>
        public string ScenarioName { get { return Map.ReadStringAt(444, 256, true, true); } set { Map.WriteAt(444, value.Substring(0, 256), true); } }
        /// <summary>
        /// Read Or Write FileTableCount
        /// </summary>
        public int FileTableCount { get { return Map.ReadInt32At(704, true); } set { Map.WriteAt(704, value, true); } }
        /// <summary>
        /// Read Or Write FileTableOffset
        /// </summary>
        public int FileTableOffset { get { return Map.ReadInt32At(708, true); } set { Map.WriteAt(708, value, true); Map.FileTableOffset = value; } }
        /// <summary>
        /// Read Or Write FileTableSize
        /// </summary>
        public int FileTableSize { get { return Map.ReadInt32At(712, true); } set { Map.WriteAt(712, value, true); } }
        /// <summary>
        /// Read Or Write FileTableIndexOffset
        /// </summary>
        public int FileTableIndexOffset { get { return Map.ReadInt32At(716, true); } set { Map.WriteAt(716, value, true); } }
        /// <summary>
        /// Read Or Write Checksum
        /// </summary>
        public int Checksum { get { return Map.ReadInt32At(720, true); } set { Map.WriteAt(720, value, true); } }
        /// <summary>
        /// Read Or Write Foot
        /// </summary>
        public string Foot { get { return Map.ReadReverseStringAt(2044, 4, true, true); } set { Map.WriteReverseStringAt(2044, value.Substring(0, 4), true); } }
        #endregion
    }
}
