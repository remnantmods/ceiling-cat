using System;
using System.Collections.Generic;
using System.Text;

namespace H2.Sections
{
    public class Index
    {
        private MapStream Map;

        internal Index(MapStream map)
        {
            Map = map;
            Map.TagInfoIndexOffset = TagInfoIndexOffset;
            Map.TagInfoCount = TagCount;
            Map.SecondaryMagic = SecondaryMagic;
        }

        #region Data
        /// <summary>
        /// Read Or Write PrimaryMagicConstant
        /// </summary>
        public int PrimaryMagicConstant { get { return Map.ReadInt32At(Map.IndexOffset, true); } set { Map.WriteAt(Map.IndexOffset, value, true); } }
        /// <summary>
        /// Calculate PrimaryMagic
        /// </summary>
        public int PrimaryMagic { get { return PrimaryMagicConstant - (Map.IndexOffset + 32); } }
        /// <summary>
        /// Read Or Write TagInfoIndexPointer
        /// </summary>
        public int TagInfoIndexPointer { get { return Map.ReadInt32At(Map.IndexOffset + 8, true); } set { Map.WriteAt(Map.IndexOffset + 8, value, true); Map.TagInfoIndexOffset = value - PrimaryMagic; } }
        /// <summary>
        /// Calculate TagInfoIndexOffset
        /// </summary>
        public int TagInfoIndexOffset { get { return TagInfoIndexPointer - PrimaryMagic; } }
        /// <summary>
        /// Read Or Write ScnrID
        /// </summary>
        public int ScnrID { get { return Map.ReadInt32At(Map.IndexOffset + 12, true); } set { Map.WriteAt(Map.IndexOffset + 12, value, true); Map.Scnr.ID = value; } }
        /// <summary>
        /// Read Or Write FirstID
        /// </summary>
        public int GlobalsID { get { return Map.ReadInt32At(Map.IndexOffset + 16, true); } set { Map.WriteAt(Map.IndexOffset + 16, value, true); } }
        /// <summary>
        /// Read Or Write TagCount
        /// </summary>
        public int TagCount { get { return Map.ReadInt32At(Map.IndexOffset + 24, true); } set { Map.WriteAt(Map.IndexOffset + 24, value, true); Map.TagInfoCount = value; } }
        /// <summary>
        /// Read Or Write Tags
        /// </summary>
        public string Tags { get { return Map.ReadStringAt(Map.IndexOffset + 28, 4, false, true); } set { Map.WriteAt(Map.IndexOffset + 28, value.Substring(0, 4), true); } }
        /// <summary>
        /// Read FirstTagPointer
        /// </summary>
        public int FirstTagPointer { get { return Map.ReadInt32At(Map.TagInfoIndexOffset + 8, true); } }
        /// <summary>
        /// Calculate SecondaryMagic
        /// </summary>
        public int SecondaryMagic { get { return FirstTagPointer - Map.MetaStart; } }
        #endregion
    }
}
