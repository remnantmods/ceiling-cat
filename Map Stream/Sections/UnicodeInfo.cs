using System;
using System.Collections.Generic;
using System.Text;

namespace H2.Sections
{
    public class UnicodeInfo
    {
        int StartOffset;
        MapStream Map;

        internal UnicodeInfo(MapStream map, int startOffset)
        { Map = map; StartOffset = startOffset; }

        /// <summary>
        /// StringCount
        /// </summary>
        public int StringCount { get { return Map.ReadInt32At(StartOffset, true); } set { Map.WriteAt(StartOffset, value, true); } }
        /// <summary>
        /// StringTableSize
        /// </summary>
        public int StringTableSize { get { return Map.ReadInt32At(StartOffset + 4, true); } set { Map.WriteAt(StartOffset + 4, value, true); } }
        /// <summary>
        /// StringIndexOffset
        /// </summary>
        public int StringIndexOffset { get { return Map.ReadInt32At(StartOffset + 8, true); } set { Map.WriteAt(StartOffset + 8, value, true); } }
        /// <summary>
        /// StringTableOffset
        /// </summary>
        public int StringTableOffset { get { return Map.ReadInt32At(StartOffset + 12, true); } set { Map.WriteAt(StartOffset + 12, value, true); } }

        public string[] ReadTable()
        {
            return new string(Map.ReadFixedLengthStringAt(StringTableOffset, (H2.DataTypes.Length)StringTableSize, Encoding.Unicode, true).ToCharArray()).Split(new char[] { '\0' });
        }

        //public string this[int ] {get{}set{}}
    }
}
