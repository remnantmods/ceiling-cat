using System;
using System.Collections.Generic;
using System.Text;

namespace H2.Sections
{
    public class UnicodeCollection
    {
        private MapStream Map;
        private int GlobalsOffset;

        internal UnicodeCollection(MapStream map)
        {
            Map = map;
            GlobalsOffset = Map.Tags[Map.Index.GlobalsID, Tags.SearchType.ID].Offset;
            this.English = new UnicodeInfo(map, GlobalsOffset + 400);
            this.Japanese = new UnicodeInfo(map, GlobalsOffset + 428);
            this.Dutch = new UnicodeInfo(map, GlobalsOffset + 456);
            this.French = new UnicodeInfo(map, GlobalsOffset + 484);
            this.Spanish = new UnicodeInfo(map, GlobalsOffset + 512);
            this.Italian = new UnicodeInfo(map, GlobalsOffset + 540);
            this.Korean = new UnicodeInfo(map, GlobalsOffset + 568);
            this.Chinese = new UnicodeInfo(map, GlobalsOffset + 596);
            this.Portuguese = new UnicodeInfo(map, GlobalsOffset + 624);
        }

        #region Instance Data

        private UnicodeInfo __english__;
        private UnicodeInfo __japanese__;
        private UnicodeInfo __dutch__;
        private UnicodeInfo __french__;
        private UnicodeInfo __spanish__;
        private UnicodeInfo __italian__;
        private UnicodeInfo __korean__;
        private UnicodeInfo __chinese__;
        private UnicodeInfo __portuguese__;

        #endregion

        #region Accessors

        /// <summary>
        /// English
        /// </summary>
        public UnicodeInfo English { get { return __english__; } set { __english__ = value; } }
        /// <summary>
        /// Japanese
        /// </summary>
        public UnicodeInfo Japanese { get { return __japanese__; } set { __japanese__ = value; } }
        /// <summary>
        /// Dutch
        /// </summary>
        public UnicodeInfo Dutch { get { return __dutch__; } set { __dutch__ = value; } }
        /// <summary>
        /// French
        /// </summary>
        public UnicodeInfo French { get { return __french__; } set { __french__ = value; } }
        /// <summary>
        /// Spanish
        /// </summary>
        public UnicodeInfo Spanish { get { return __spanish__; } set { __spanish__ = value; } }
        /// <summary>
        /// Italian
        /// </summary>
        public UnicodeInfo Italian { get { return __italian__; } set { __italian__ = value; } }
        /// <summary>
        /// Korean
        /// </summary>
        public UnicodeInfo Korean { get { return __korean__; } set { __korean__ = value; } }
        /// <summary>
        /// Chinese
        /// </summary>
        public UnicodeInfo Chinese { get { return __chinese__; } set { __chinese__ = value; } }
        /// <summary>
        /// Portuguese
        /// </summary>
        public UnicodeInfo Portuguese { get { return __portuguese__; } set { __portuguese__ = value; } }

        #endregion

    }
}
