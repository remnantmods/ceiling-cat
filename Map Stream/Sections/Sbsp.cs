using System;
using System.Collections.Generic;
using System.Text;

namespace H2.Sections
{
    public class Sbsp
    {
        MapStream Map;

        internal Sections.SbspInfo[] SbspInfoCache;

        internal Sbsp(MapStream map)
        { 
            Map = map;
        }

        internal void CreateCache()
        {
            Map.Position = Map.Scnr.Offset + 528;

            //chunk count and translated reflexive
            int bspCount = Map.ReadInt32();
            int bspTransReflx = Map.ReadInt32() - Map.SecondaryMagic;

            SbspInfoCache = new SbspInfo[bspCount];

            //count through the bsp's in the Map
            for (int x = 0; x < bspCount; x++)
            {
                //each chunk is 68 bytes long
                Map.Position = bspTransReflx + (x * 68);

                //for storing values to come
                SbspInfoCache[x] = new SbspInfo(Map, (int)Map.Position, x);

                //fix the current bsp's meta offset and size
                TagInfo ti = Map.Tags[SbspInfoCache[x].ID, Tags.SearchType.ID];
                ti.OffsetSet = SbspInfoCache[x].Offset;
                ti.Size = SbspInfoCache[x].Size;
                
                //fix the current bsp's ltmp meta offset and size
                int LightmapID = SbspInfoCache[x].LightMapID;
                if (LightmapID != -1)//-1 = nulled so skip
                {
                    ti = Map.Tags[LightmapID, Tags.SearchType.ID];
                    ti.OffsetSet = SbspInfoCache[x].LightMap.Offset;
                    ti.Size = SbspInfoCache[x].LightMap.Size;
                    SbspInfoCache[x].LightMap.StartOffset = SbspInfoCache[x].LightMap.Offset;
                }
            }
        }

        public Sections.SbspInfo this[string Path]
        {
            get
            {
                foreach (Sections.SbspInfo si in SbspInfoCache)
                    if (si.TagPath == Path)
                        return si;
                return null;
            }
            set
            {
                for (int i = 0; i < SbspInfoCache.Length; i++)
                    if (SbspInfoCache[i].TagPath == Path)
                    {
                        SbspInfoCache[i] = value;
                        break;
                    }
            }
        }

        public Sections.SbspInfo this[int i, H2.Sections.Tags.SearchType st]
        {
            get
            {
                if (st == Tags.SearchType.Index)
                    return SbspInfoCache[i];
                else
                {
                    for (int x = 0; x < SbspInfoCache.Length; x++)
                        if (SbspInfoCache[x].ID == i)
                            return SbspInfoCache[x];
                }
                return null;
            }
            set
            {
                if (st == Tags.SearchType.Index)
                    SbspInfoCache[i] = value;
                else
                {
                    for (int x = 0; x < SbspInfoCache.Length; x++)
                        if (SbspInfoCache[x].ID == i)
                        {
                            SbspInfoCache[x] = value;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Read Or Write BSP Count
        /// </summary>
        public int BSPCount { get { return Map.ReadInt32At(Map.Scnr.Offset + 528, false); } set { Map.WriteAt(Map.Scnr.Offset + 528, value, false); } }
        /// <summary>
        /// Read Or Write BSPTransReflx
        /// </summary>
        public int BSPTransReflx { get { return Map.ReadInt32At(Map.Scnr.Offset + 532, false) - Map.SecondaryMagic; } set { Map.WriteAt(Map.Scnr.Offset + 532, value + Map.SecondaryMagic, false); } }
    }
}
