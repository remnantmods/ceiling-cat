using System;
using System.Collections.Generic;
using System.Text;

namespace H2.Sections
{
    public class SbspInfo
    {
        private MapStream Map;
        internal int StartOffset;
        internal LightMap LightMapCache;
        private string tagPathCache;

        #region Data
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get { return Map.ReadInt32At(StartOffset + 20, false); } set { Map.WriteAt(StartOffset + 20, value, false); } }
        /// <summary>
        /// LightMap ID
        /// </summary>
        public int LightMapID { get { return Map.ReadInt32At(StartOffset + 28, false); } set { Map.WriteAt(StartOffset + 28, value, false); } }
        /// <summary>
        /// Calculate PointerOffset
        /// </summary>
        public int PointerOffset { get { return StartOffset - Map.Scnr.Offset; } }
        /// <summary>
        /// MetaOffset
        /// </summary>
        public int Offset { get { return Map.ReadInt32At(StartOffset, false) & (int)0x3FFFFFFF; } set { Map.WriteAt(StartOffset, value, false); } }
        /// <summary>
        /// MetaSize
        /// </summary>
        public int Size { get { return Map.ReadInt32At(StartOffset + 4, false); } set { Map.WriteAt(StartOffset + 4, value, false); } }
        /// <summary>
        /// Magic
        /// </summary>
        public int Magic { get { return Map.ReadInt32At(StartOffset + 8, false) - Offset; } set { Map.WriteAt(StartOffset + 8, value + Offset, false); } }
        /// <summary>
        /// TagPath
        /// </summary>
        public string TagPath { get { return tagPathCache; } set { tagPathCache = value; Map.Tags.Rename(ID, Tags.SearchType.ID, value); } }
        /// <summary>
        /// LightMap
        /// </summary>
        public LightMap LightMap { get { return LightMapCache; } set { LightMapCache = value; } }
        #endregion

        #region New
        internal SbspInfo(MapStream map, int startOffset, int index)
        {
            Map = map;
            StartOffset = startOffset;
            tagPathCache = Map.Tags.GetTagPath(ID, Tags.SearchType.ID);
            LightMapCache = new LightMap(map, index);
        }
        #endregion
    }
}
