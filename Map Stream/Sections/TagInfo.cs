using System;
using System.Collections.Generic;
using System.Text;

namespace H2.Sections
{
    public class TagInfo
    {
        #region New
        public TagInfo()
        { }
        #endregion

        #region Instance Data

        private int __iD__;
        private int __rawMetaOffset__;
        private int __offset__;
        private int __size__;

        #endregion

        #region Accessors

        /// <summary>
        /// ID
        /// </summary>
        public int ID { get { return __iD__; } set { __iD__ = value; } }
        /// <summary>
        /// RawMetaOffset
        /// </summary>
        public int RawMetaOffset { get { return __rawMetaOffset__; } }
        /// <summary>
        /// Offset
        /// </summary>
        public int Offset { get { return __offset__; } }
        internal int OffsetSet { set { __offset__ = value; } }
        /// <summary>
        /// Size
        /// </summary>
        public int Size { get { return __size__; } set { __size__ = value; } }

        #endregion

        public void ChangeOffset(int NewOffset, int Magic)
        {
            __offset__ = NewOffset;
            __rawMetaOffset__ = NewOffset + Magic;
        }
    }
}
