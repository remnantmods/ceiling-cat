using System;
using System.Collections.Generic;
using System.Text;
using H2.DataTypes;

namespace H2.Sections
{
    public class StringIDs
    {
        MapStream Map;
        List<StringID> StringIDCache;

        internal StringIDs(MapStream map)
        { Map = map; CreateCache(); }

        private void CreateCache()
        {
            long pos = Map.Position;
            Map.Position = Map.Header.StringIDs128Offset;
            StringIDCache = new List<StringID>(Map.Header.StringIDCount);
            for (int i = 0; i < StringIDCache.Capacity; i++)
                StringIDCache.Add(new StringID(new string(Map.ReadChars(128))));
            Map.Position = pos;
        }

        public void WriteStringIDs()
        {
            long pos = Map.Position;
            Map.Position = Map.Header.StringIDs128Offset;
            for (int i = 0; i < StringIDCache.Capacity; i++)
                Map.Write(StringIDCache[i].ToCharArray());
            Map.Position = pos;
        }

        /// <summary>
        /// Read StringIDs Into An Array Of Strings
        /// </summary>
        /// <returns></returns>
        public string[] ToStringArray()
        {
            int count = Map.Header.StringIDCount;

            string[] strings = new string[count];
            for (int i = 0; i < count; i++)
                strings[i] = StringIDCache[i].ToString();

            return strings;
        }

        public void Add(StringID stringid)
        {
            throw new Exception();

            //Shift StringID Index
            //Shift StringID Table
            //Shift File Index
            //Shift Unicode Index/Table
            //Shift Crazy
            //Shift Bitmap Raw
            //Shift Index
            //Shift Meta
        }

        public ushort IndexOf(StringID stringid)
        {
            string FindString = stringid.ToString();
            for (ushort i = 0; i < StringIDCache.Count; i++)
                if (StringIDCache[i].ToString() == FindString) return i;
            return 0;
        }

        public ushort IndexOf(string name)
        {
            for (ushort i = 0; i < StringIDCache.Count; i++)
                if (StringIDCache[i].ToString() == name) return i;
            return 0;
        }

        /// <summary>
        /// Get or Set A StringID
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public StringID this[ushort index]
        {
            get
            {
                return StringIDCache[index];
            }
            set
            {
                Map.WriteAt(Map.Header.StringIDs128Offset + (128 * index), value.ToCharArray(), true);
                StringIDCache[index] = new StringID(value.ToString());
            }
        }
        // This Doesn't Take Account Of The Second StringID Table, Where Strings Are Separated By A Null Byte And The Table Size Is Stored In The Header.
    }
}
