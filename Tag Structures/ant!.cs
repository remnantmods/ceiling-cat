using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("ant!")]
        public class ant_
        {
[Value(Name="Attachment Marker Name", Offset=0)]
public StringID _0_Attachment_Marker_Name;
[Value(Name="Bitmap", Offset=4)]
public Dependancy _4_Bitmap;
[Value(Name="Physics", Offset=8)]
public Dependancy _8_Physics;
[Value(Offset=12)]
public Byte[] _12_ = new byte[80];
[Value(Name="Spring Strength Coefficient", Offset=92)]
public Single _92_Spring_Strength_Coefficient;
[Value(Name="Falloff Pixels", Offset=96)]
public Single _96_Falloff_Pixels;
[Value(Name="Cutoff Pixels", Offset=100)]
public Single _100_Cutoff_Pixels;
[Value(Offset=104)]
public Byte[] _104_ = new byte[40];
public _144_vertices[] _144_Vertices;
[Serializable][Reflexive(Name="Vertices", Offset=144, ChunkSize=128, Label="")]
public class _144_vertices
{
private int __StartOffset__;
[Value(Name="Spring Strength Coefficient", Offset=0)]
public Single _0_Spring_Strength_Coefficient;
[Value(Name="Angle Yaw", Offset=4)]
public Single _4_Angle_Yaw;
[Value(Name="Angle Pitch", Offset=8)]
public Single _8_Angle_Pitch;
[Value(Offset=12)]
public Byte[] _12_ = new byte[24];
[Value(Name="Length(world units)", Offset=36)]
public Single _36_Length_world_units_;
[Value(Name="Sequence index", Offset=40)]
public Int32 _40_Sequence_index;
[Value(Name="Color Alpha", Offset=44)]
public Single _44_Color_Alpha;
[Value(Name="Color Red", Offset=48)]
public Single _48_Color_Red;
[Value(Name="Color Green", Offset=52)]
public Single _52_Color_Green;
[Value(Name="Color Blue", Offset=56)]
public Single _56_Color_Blue;
[Value(Offset=60)]
public Byte[] _60_ = new byte[52];
[Value(Name="Color Alpha", Offset=112)]
public Single _112_Color_Alpha;
[Value(Name="Color Red", Offset=116)]
public Single _116_Color_Red;
[Value(Name="Color Green", Offset=120)]
public Single _120_Color_Green;
[Value(Name="Color Blue", Offset=124)]
public Single _124_Color_Blue;
}
        }
    }
}