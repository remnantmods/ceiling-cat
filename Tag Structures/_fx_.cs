using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("<fx>")]
        public class _fx_
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
public _12_unknown[] _12_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=12, ChunkSize=20, Label="")]
public class _12_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
public _4_unknown[] _4_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=4, ChunkSize=1, Label="")]
public class _4_unknown
{
private int __StartOffset__;
[Value(Name="Float", Offset=0)]
public Byte _0_Float;
}
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
public _20_sound_template[] _20_Sound_Template;
[Serializable][Reflexive(Name="Sound Template", Offset=20, ChunkSize=12, Label="")]
public class _20_sound_template
{
private int __StartOffset__;
public _0_unknown[] _0_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=0, ChunkSize=24, Label="")]
public class _0_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int16 _12_Unknown;
[Value(Name="Unknown", Offset=14)]
public Int16 _14_Unknown;
public _16_unknown[] _16_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=16, ChunkSize=16, Label="")]
public class _16_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
}
[Value(Name="Template Channel?", Offset=8)]
public StringID _8_Template_Channel_;
}
        }
    }
}