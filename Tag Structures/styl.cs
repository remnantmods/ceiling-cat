using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("styl")]
        public class styl
        {
[Value(Name="Unknown", Offset=0)]
public H2.DataTypes.String _0_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Int32 _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Int32 _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Int32 _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Int32 _72_Unknown;
public _76_unknown15[] _76_Unknown15;
[Serializable][Reflexive(Name="Unknown15", Offset=76, ChunkSize=4, Label="")]
public class _76_unknown15
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _84_unknown18[] _84_Unknown18;
[Serializable][Reflexive(Name="Unknown18", Offset=84, ChunkSize=32, Label="")]
public class _84_unknown18
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public H2.DataTypes.String _0_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
}
        }
    }
}