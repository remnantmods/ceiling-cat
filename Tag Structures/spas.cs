using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("spas")]
        public class spas
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[16];
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Offset=20)]
public Byte[] _20_ = new byte[8];
public _28_unknown6[] _28_Unknown6;
[Serializable][Reflexive(Name="Unknown6", Offset=28, ChunkSize=88, Label="")]
public class _28_unknown6
{
private int __StartOffset__;
public _0_unknown7[] _0_Unknown7;
[Serializable][Reflexive(Name="Unknown7", Offset=0, ChunkSize=306, Label="")]
public class _0_unknown7
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Int32 _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Int32 _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Int32 _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Offset=72)]
public Byte[] _72_ = new byte[4];
[Value(Name="Unknown", Offset=76)]
public Int32 _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Int32 _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Int32 _88_Unknown;
[Value(Name="Unknown", Offset=92)]
public Int32 _92_Unknown;
[Value(Name="Unknown", Offset=96)]
public Single _96_Unknown;
[Value(Name="Unknown", Offset=100)]
public Single _100_Unknown;
[Value(Name="Unknown", Offset=104)]
public Int32 _104_Unknown;
[Value(Name="Unknown", Offset=108)]
public Int32 _108_Unknown;
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Name="Unknown", Offset=124)]
public Int32 _124_Unknown;
[Value(Name="Unknown", Offset=128)]
public Int32 _128_Unknown;
[Value(Name="Unknown", Offset=132)]
public Int32 _132_Unknown;
[Value(Name="Unknown", Offset=136)]
public Int32 _136_Unknown;
[Value(Name="Unknown", Offset=140)]
public Int32 _140_Unknown;
[Value(Name="Unknown", Offset=144)]
public Int32 _144_Unknown;
[Value(Name="Unknown", Offset=148)]
public Int32 _148_Unknown;
[Value(Name="Unknown", Offset=152)]
public Int32 _152_Unknown;
[Value(Name="Unknown", Offset=156)]
public Int32 _156_Unknown;
[Value(Name="Unknown", Offset=160)]
public Int32 _160_Unknown;
[Value(Name="Unknown", Offset=164)]
public Int32 _164_Unknown;
[Value(Name="Unknown", Offset=168)]
public Int32 _168_Unknown;
[Value(Name="Unknown", Offset=172)]
public Int32 _172_Unknown;
[Value(Name="Unknown", Offset=176)]
public Int32 _176_Unknown;
[Value(Name="Unknown", Offset=180)]
public Int32 _180_Unknown;
[Value(Name="Unknown", Offset=184)]
public Int32 _184_Unknown;
[Value(Name="Unknown", Offset=188)]
public Int32 _188_Unknown;
[Value(Name="Unknown", Offset=192)]
public Int32 _192_Unknown;
[Value(Name="Unknown", Offset=196)]
public Int32 _196_Unknown;
[Value(Name="Unknown", Offset=200)]
public Int32 _200_Unknown;
[Value(Name="Unknown", Offset=204)]
public Int32 _204_Unknown;
[Value(Name="Unknown", Offset=208)]
public Int32 _208_Unknown;
[Value(Name="Unknown", Offset=212)]
public Int32 _212_Unknown;
[Value(Name="Unknown", Offset=216)]
public Int32 _216_Unknown;
[Value(Name="Unknown", Offset=220)]
public Int32 _220_Unknown;
[Value(Name="Unknown", Offset=224)]
public Int32 _224_Unknown;
[Value(Offset=228)]
public Byte[] _228_ = new byte[16];
[Value(Name="Unknown", Offset=244)]
public Int32 _244_Unknown;
[Value(Offset=248)]
public Byte[] _248_ = new byte[4];
[Value(Name="Unknown", Offset=252)]
public Dependancy _252_Unknown;
[Value(Name="Unknown", Offset=260)]
public Int32 _260_Unknown;
[Value(Name="Unknown", Offset=264)]
public Int32 _264_Unknown;
[Value(Offset=268)]
public Byte[] _268_ = new byte[4];
[Value(Name="Unknown", Offset=272)]
public Int32 _272_Unknown;
[Value(Name="Unknown", Offset=276)]
public Int32 _276_Unknown;
[Value(Name="Unknown", Offset=280)]
public Int32 _280_Unknown;
[Value(Name="Unknown", Offset=284)]
public Int32 _284_Unknown;
[Value(Name="Unknown", Offset=288)]
public Int32 _288_Unknown;
[Value(Name="Unknown", Offset=292)]
public Int32 _292_Unknown;
[Value(Name="Unknown", Offset=296)]
public StringID _296_Unknown;
[Value(Name="Unknown", Offset=300)]
public Int32 _300_Unknown;
[Value(Name="Unknown", Offset=304)]
public Int16 _304_Unknown;
}
public _8_unknown83[] _8_Unknown83;
[Serializable][Reflexive(Name="Unknown83", Offset=8, ChunkSize=4, Label="")]
public class _8_unknown83
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _16_unknown86[] _16_Unknown86;
[Serializable][Reflexive(Name="Unknown86", Offset=16, ChunkSize=5, Label="")]
public class _16_unknown86
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Byte _0_Unknown;
[Value(Name="Unknown", Offset=1)]
public Byte _1_Unknown;
[Value(Name="Unknown", Offset=2)]
public Byte _2_Unknown;
[Value(Name="Unknown", Offset=3)]
public Byte _3_Unknown;
[Value(Name="Unknown", Offset=4)]
public Byte _4_Unknown;
}
public _24_unknown93[] _24_Unknown93;
[Serializable][Reflexive(Name="Unknown93", Offset=24, ChunkSize=24, Label="")]
public class _24_unknown93
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[4];
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Offset=20)]
public Byte[] _20_ = new byte[4];
}
public _32_unknown101[] _32_Unknown101;
[Serializable][Reflexive(Name="Unknown101", Offset=32, ChunkSize=4, Label="")]
public class _32_unknown101
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _40_unknown104[] _40_Unknown104;
[Serializable][Reflexive(Name="Unknown104", Offset=40, ChunkSize=6, Label="")]
public class _40_unknown104
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int16 _4_Unknown;
}
public _48_unknown109[] _48_Unknown109;
[Serializable][Reflexive(Name="Unknown109", Offset=48, ChunkSize=32, Label="")]
public class _48_unknown109
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Offset=20)]
public Byte[] _20_ = new byte[12];
}
public _56_unknown117[] _56_Unknown117;
[Serializable][Reflexive(Name="Unknown117", Offset=56, ChunkSize=4, Label="")]
public class _56_unknown117
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
[Value(Offset=64)]
public Byte[] _64_ = new byte[24];
}
        }
    }
}