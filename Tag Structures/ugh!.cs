using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("ugh!")]
        public class ugh_
        {
public _0_unknown4[] _0_Unknown4;
[Serializable][Reflexive(Name="Unknown4", Offset=0, ChunkSize=56, Label="")]
public class _0_unknown4
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="", Offset=24)]
public Int16 _24_;
[Value(Name="", Offset=26)]
public Int16 _26_;
[Value(Name="Pi", Offset=28)]
public Single _28_Pi;
[Value(Name="Pi", Offset=32)]
public Single _32_Pi;
[Value(Offset=36)]
public Byte[] _36_ = new byte[20];
}
public _8_unknown17[] _8_Unknown17;
[Serializable][Reflexive(Name="Unknown17", Offset=8, ChunkSize=20, Label="")]
public class _8_unknown17
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="", Offset=8)]
public Int16 _8_;
[Value(Name="", Offset=10)]
public Int16 _10_;
[Value(Name="One", Offset=12)]
public Single _12_One;
[Value(Name="One", Offset=16)]
public Single _16_One;
}
public _16_sound_names25[] _16_Sound_Names25;
[Serializable][Reflexive(Name="Sound Names25", Offset=16, ChunkSize=4, Label="")]
public class _16_sound_names25
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
}
public _24_unknown28[] _24_Unknown28;
[Serializable][Reflexive(Name="Unknown28", Offset=24, ChunkSize=10, Label="")]
public class _24_unknown28
{
private int __StartOffset__;
[Value(Name="", Offset=0)]
public Int16 _0_;
[Value(Name="", Offset=2)]
public Int16 _2_;
[Value(Name="", Offset=4)]
public Int16 _4_;
[Value(Name="", Offset=6)]
public Int16 _6_;
[Value(Name="", Offset=8)]
public Int16 _8_;
}
public _32_sound_permutations35[] _32_Sound_Permutations35;
[Serializable][Reflexive(Name="Sound Permutations35", Offset=32, ChunkSize=12, Label="")]
public class _32_sound_permutations35
{
private int __StartOffset__;
[Value(Name="", Offset=0)]
public Int16 _0_;
[Value(Name="", Offset=2)]
public Int16 _2_;
[Value(Name="", Offset=4)]
public Byte _4_;
[Value(Name="Chunk", Offset=5)]
public Byte _5_Chunk;
[Value(Name="", Offset=6)]
public Int16 _6_;
[Value(Name="Index", Offset=8)]
public Int16 _8_Index;
[Value(Name="chunkCount", Offset=10)]
public Int16 _10_chunkCount;
}
public _40_sound_choices44[] _40_Sound_Choices44;
[Serializable][Reflexive(Name="Sound Choices44", Offset=40, ChunkSize=16, Label="")]
public class _40_sound_choices44
{
private int __StartOffset__;
[Value(Name="Name Index", Offset=0)]
public UInt32 _0_Name_Index;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Index", Offset=12)]
public Int16 _12_Index;
[Value(Name="chunkCount", Offset=14)]
public Int16 _14_chunkCount;
}
public _48_unknown51[] _48_Unknown51;
[Serializable][Reflexive(Name="Unknown51", Offset=48, ChunkSize=52, Label="")]
public class _48_unknown51
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[20];
public _20_unknown53[] _20_Unknown53;
[Serializable][Reflexive(Name="Unknown53", Offset=20, ChunkSize=72, Label="")]
public class _20_unknown53
{
private int __StartOffset__;
[Value(Name="One", Offset=0)]
public Int32 _0_One;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
[Value(Name="800", Offset=8)]
public Single _8_800;
[Value(Name="8000", Offset=12)]
public Single _12_8000;
[Value(Offset=16)]
public Byte[] _16_ = new byte[24];
[Value(Name="800", Offset=40)]
public Single _40_800;
[Value(Name="8000", Offset=44)]
public Single _44_8000;
[Value(Offset=48)]
public Byte[] _48_ = new byte[24];
}
[Value(Offset=28)]
public Byte[] _28_ = new byte[24];
}
public _56_zero65[] _56_Zero65;
[Serializable][Reflexive(Name="Zero65", Offset=56, ChunkSize=1, Label="")]
public class _56_zero65
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[1];
}
public _64_sound_raw_data68[] _64_Sound_Raw_Data68;
[Serializable][Reflexive(Name="Sound Raw Data68", Offset=64, ChunkSize=12, Label="")]
public class _64_sound_raw_data68
{
private int __StartOffset__;
[Value(Name="Raw Offset", Offset=0)]
public UInt32 _0_Raw_Offset;
[Value(Name="Size", Offset=4)]
public UInt32 _4_Size;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
}
public _72_unknown73[] _72_Unknown73;
[Serializable][Reflexive(Name="Unknown73", Offset=72, ChunkSize=28, Label="")]
public class _72_unknown73
{
private int __StartOffset__;
public _0_unknown74[] _0_Unknown74;
[Serializable][Reflexive(Name="Unknown74", Offset=0, ChunkSize=16, Label="")]
public class _0_unknown74
{
private int __StartOffset__;
[Value(Name="", Offset=0)]
public Int16 _0_;
[Value(Name="", Offset=2)]
public Int16 _2_;
[Value(Name="", Offset=4)]
public Single _4_;
[Value(Name="", Offset=8)]
public Int32 _8_;
[Value(Name="", Offset=12)]
public Int32 _12_;
}
public _8_unknown81[] _8_Unknown81;
[Serializable][Reflexive(Name="Unknown81", Offset=8, ChunkSize=4, Label="")]
public class _8_unknown81
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
}
[Value(Offset=16)]
public Byte[] _16_ = new byte[4];
[Value(Offset=20)]
public Byte[] _20_ = new byte[8];
}
public _80_diff_model_raw_data87[] _80_Diff_Model_Raw_Data87;
[Serializable][Reflexive(Name="Diff Model Raw Data87", Offset=80, ChunkSize=44, Label="")]
public class _80_diff_model_raw_data87
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Raw Offset", Offset=8)]
public UInt32 _8_Raw_Offset;
[Value(Name="Size", Offset=12)]
public UInt32 _12_Size;
[Value(Name="One", Offset=16)]
public Int32 _16_One;
[Value(Name="Data Size", Offset=20)]
public UInt32 _20_Data_Size;
public _24_rsrcs93[] _24_Rsrcs93;
[Serializable][Reflexive(Name="rsrcs93", Offset=24, ChunkSize=16, Label="")]
public class _24_rsrcs93
{
private int __StartOffset__;
[Value(Name="", Offset=0)]
public Int16 _0_;
[Value(Name="", Offset=2)]
public Int16 _2_;
[Value(Name="", Offset=4)]
public Int16 _4_;
[Value(Name="", Offset=6)]
public Int16 _6_;
[Value(Name="Size", Offset=8)]
public UInt32 _8_Size;
[Value(Name="Offset", Offset=12)]
public UInt32 _12_Offset;
}
[Value(Name="ughloneid", Offset=32)]
public Int32 _32_ughloneid;
[Value(Name="Constant", Offset=36)]
public Int32 _36_Constant;
[Value(Name="Constant", Offset=40)]
public Int32 _40_Constant;
}
        }
    }
}