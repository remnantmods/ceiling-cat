using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("sily")]
        public class sily
        {
[Value(Name="Setting Catergory", Offset=0)]
public H2.DataTypes.Enum _0_Setting_Catergory = new H2.DataTypes.Enum(new string[] {  "Match Round Setting", "Match CTF Score To Win", "Match Slayer Score To Win Round", "Match Oddball Score To Win Round", "Match King Score To Win Round", "Match Race Score To Win Round", "Match Headhunter Score To Win Round", "Match Juggernaught Score To Win Round", "Match Territories Score To Win Round", "Match Assault Score To Win Round", "Match Round Time Limit", "Match Rounds Reset Map", "Match Tie Resolution", "Match Observers", "Match Join In Progress", "Max Players", "Lives Per Round", "Respawn Time", "Suicide Penalty", "Shields", "Motion Sensor", "Invisibility", "Team Changing", "Team Scoring", "Friendly Fire", "Team Respawn Setting", "Betrayal Respawn Penalty", "Team Killer Management", "Slayer Bonus Points", "Slayer Suidcide Point Loss", "Slayer Death Point Loss", "Headhunter Moving Head Bin", "Headhunter Point Multiplier", "Headhunter Suicide Point Loss", "Headhunter Death Point Loss", "Headhunter Uncontested Bin", "Headhunter Speed With Heads", "Headhunter Max Heads Carried", "King Uncontested Hill", "King Team Time Multiplier", "King Moving Hill", "King Extra Damage On Hill", "King Damage Resistance On Hill", "Oddball Ball Spawn Count", "Oddball Ball Hit Damage", "Oddball Speed With Ball", "Oddball Driving/Gunning With Ball", "Oddball Waypoint To Ball", "Race Random Track", "Race Uncontested Flag", "CTF Sudden Death", "CTF Flag May Be Returned", "CTF Flag At Home To Score", "CTF Flag Reset Time", "CTF Speed With Flag", "CTF Flag Hit Damage", "CTF Driving/Gunning With Flag", "CTF Waypoint To Own Flag", "Assault Game Type", "Assault Sudden Death", "Assault Detonation Time", "Assault Bomb At Home To Score", "Assault Arming Time", "Assault Speed With Bomb", "Assault Bomb Hit Damage", "Assault Driving/Gunning With Bomb", "Assault Waypoint To Own Bomb", "Juggernaught Betrayal Point Loss", "Juggernaught Juggy Extra Damage", "Juggernaught Infinite Ammo", "Juggernaught Juggy Oversheilds", "Juggernaught Juggy Active Camo", "Juggernaught Juggy Montion Sensor", "Territories Territory Count", "Vehi. Respawn", "Vehi. Primary Light Land", "Vehi. Secondary Light Land", "Vehi. Primary Heavy Land", "Vehi. Primary Flying", "Vehi. Secondary Heavy Land", "Vehi. Primary Turret", "Vehi. Secondary Turret", "Equip. Weapons On Map", "Equip. Oversheilds On Map", "Equip. Active Camo On Map", "Equip. Grenades On Map", "Equip. Weapon Respawn Times", "Equip. Starting Grenades", "Equip. Primary Starting Equipment", "UNS. Max Living Players", "UNS. Teams Enabled", "UNS. Assault Bomb May Be Returned", "UNS. Max Teams", "UNS. Equip. Secondary Starting Equipment", "UNS. Assault Fuse Time", "UNS. Juggy Movement", "UNS. Sticky Fuse", "UNS. Terr. Contest Time", "UNS. Terr. Control Time", "UNS. Oddb. Carr. Invisible", "UNS. King Invisible In Hill", "UNS. Ball Carr. Dmg. Resistance", "UNS. King Dmg. Res. In Hill", "UNS. Players Extra Dmg", "UNS. Players Dmg. Resistance", "UNS. CTF Carr. Invisible", "UNS. Juggy Dmg. Resistance", "UNS. Bomb Carr. Dmg. Resistance", "UNS. Bomb Carr. Invisible", "UNS. Force Even Teams" }, 4);
[Value(Name="Value", Offset=4)]
public Int32 _4_Value;
[Value(Name="Unicode String List Of Options", Offset=8)]
public Dependancy _8_Unicode_String_List_Of_Options;
[Value(Name="Title Text", Offset=12)]
public StringID _12_Title_Text;
[Value(Name="Header Text", Offset=16)]
public StringID _16_Header_Text;
[Value(Name="Description Text", Offset=20)]
public StringID _20_Description_Text;
public _24_options[] _24_Options;
[Serializable][Reflexive(Name="Options", Offset=24, ChunkSize=12, Label="")]
public class _24_options
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Default" }, 4);
[Value(Name="Value", Offset=4)]
public Int16 _4_Value;
[Value(Offset=6)]
public Byte[] _6_ = new byte[2];
[Value(Name="Label", Offset=8)]
public StringID _8_Label;
}
        }
    }
}