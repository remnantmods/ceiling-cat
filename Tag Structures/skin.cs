using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("skin")]
        public class skin
        {
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="bitm", Offset=4)]
public Dependancy _4_bitm;
[Value(Name="Coord X Top arrow", Offset=8)]
public Int16 _8_Coord_X_Top_arrow;
[Value(Name="Coord Y Top arrow", Offset=10)]
public Int16 _10_Coord_Y_Top_arrow;
[Value(Name="Coord X Bottom arrow", Offset=12)]
public Int16 _12_Coord_X_Bottom_arrow;
[Value(Name="Coord Y Bottom arrow", Offset=14)]
public Int16 _14_Coord_Y_Bottom_arrow;
public _16_button_effects[] _16_Button_effects;
[Serializable][Reflexive(Name="Button effects", Offset=16, ChunkSize=16, Label="")]
public class _16_button_effects
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Brightness", Offset=4)]
public Int16 _4_Brightness;
[Value(Name="Brightness only once", Offset=6)]
public Int16 _6_Brightness_only_once;
public _8_button_switching_effects[] _8_Button_switching_effects;
[Serializable][Reflexive(Name="Button switching effects", Offset=8, ChunkSize=20, Label="")]
public class _8_button_switching_effects
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Bitmap Flashing speed", Offset=4)]
public Single _4_Bitmap_Flashing_speed;
[Value(Name="Button shifting space", Offset=8)]
public Single _8_Button_shifting_space;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
}
public _24_buttons_text_strings[] _24_Buttons_Text_strings;
[Serializable][Reflexive(Name="Buttons Text strings", Offset=24, ChunkSize=44, Label="")]
public class _24_buttons_text_strings
{
private int __StartOffset__;
[Value(Name="Text options", Offset=0)]
public Bitmask _0_Text_options = new Bitmask(new string[] { "1","2","Pulsing","Really tiny text","5" }, 4);
[Value(Name="Transition effect/movement", Offset=4)]
public Bitmask _4_Transition_effect_movement = new Bitmask(new string[] { "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25" }, 4);
[Value(Name="Unused", Offset=8)]
public Int16 _8_Unused;
[Value(Name="Font Type", Offset=10)]
public Int16 _10_Font_Type;
[Value(Name="Alpha", Offset=12)]
public Single _12_Alpha;
[Value(Name="Color Red", Offset=16)]
public Single _16_Color_Red;
[Value(Name="Color Green", Offset=20)]
public Single _20_Color_Green;
[Value(Name="Color Blue", Offset=24)]
public Single _24_Color_Blue;
[Value(Name="Text Placement coord Y from Top", Offset=28)]
public Int16 _28_Text_Placement_coord_Y_from_Top;
[Value(Name="Text Placement coord X from Right", Offset=30)]
public Int16 _30_Text_Placement_coord_X_from_Right;
[Value(Name="Text Placement coord Y from Bottom", Offset=32)]
public Int16 _32_Text_Placement_coord_Y_from_Bottom;
[Value(Name="Text Placement coord X from Left", Offset=34)]
public Int16 _34_Text_Placement_coord_X_from_Left;
[Value(Name="String name", Offset=36)]
public StringID _36_String_name;
[Value(Name="Unknown", Offset=40)]
public UInt32 _40_Unknown;
}
public _32_button_bitmaps[] _32_Button_bitmaps;
[Serializable][Reflexive(Name="Button bitmaps", Offset=32, ChunkSize=56, Label="")]
public class _32_button_bitmaps
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
[Value(Name="Transition effect/movement", Offset=4)]
public Bitmask _4_Transition_effect_movement = new Bitmask(new string[] { "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25" }, 4);
[Value(Name="Alpha transparence", Offset=8)]
public H2.DataTypes.Enum _8_Alpha_transparence = new H2.DataTypes.Enum(new string[] {  "Normal", "Inverted" }, 2);
[Value(Name="Xbox live bitmap setting", Offset=10)]
public Int16 _10_Xbox_live_bitmap_setting;
[Value(Name="Bitmap Placement coord X", Offset=12)]
public Int16 _12_Bitmap_Placement_coord_X;
[Value(Name="Bitmap Placement coord Y", Offset=14)]
public Int16 _14_Bitmap_Placement_coord_Y;
[Value(Name="Horizontal bitmap scrolling speed", Offset=16)]
public Single _16_Horizontal_bitmap_scrolling_speed;
[Value(Name="Unused", Offset=20)]
public Single _20_Unused;
[Value(Name="bitm", Offset=24)]
public Dependancy _24_bitm;
[Value(Name="Bitmap Layer Level", Offset=32)]
public Int16 _32_Bitmap_Layer_Level;
[Value(Name="Unused", Offset=34)]
public Int16 _34_Unused;
[Value(Name="Unused", Offset=36)]
public Single _36_Unused;
[Value(Name="Unused", Offset=40)]
public Int16 _40_Unused;
[Value(Name="Unused", Offset=42)]
public Int16 _42_Unused;
[Value(Name="Bitmap color variation fuction", Offset=44)]
public StringID _44_Bitmap_color_variation_fuction;
[Value(Name="Unused", Offset=48)]
public Single _48_Unused;
[Value(Name="Unused", Offset=52)]
public Single _52_Unused;
}
public _40_emblem[] _40_Emblem;
[Serializable][Reflexive(Name="Emblem", Offset=40, ChunkSize=36, Label="")]
public class _40_emblem
{
private int __StartOffset__;
[Value(Name="Space between buttons", Offset=0)]
public H2.DataTypes.Enum _0_Space_between_buttons = new H2.DataTypes.Enum(new string[] {  "Yes", "No" }, 2);
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Unknown", Offset=4)]
public Int16 _4_Unknown;
[Value(Name="Unknown", Offset=6)]
public Int16 _6_Unknown;
[Value(Name="Xbox live bitmap setting", Offset=8)]
public Int16 _8_Xbox_live_bitmap_setting;
[Value(Name="Unused", Offset=10)]
public Int16 _10_Unused;
[Value(Name="bitm", Offset=12)]
public Dependancy _12_bitm;
[Value(Name="shad", Offset=20)]
public Dependancy _20_shad;
[Value(Name="Coord X", Offset=28)]
public Int16 _28_Coord_X;
[Value(Name="Coord Y", Offset=30)]
public Int16 _30_Coord_Y;
[Value(Name="Coord X", Offset=32)]
public Int16 _32_Coord_X;
[Value(Name="Coord Y", Offset=34)]
public Int16 _34_Coord_Y;
}
public _48_buttons_holder[] _48_Buttons_Holder;
[Serializable][Reflexive(Name="Buttons Holder", Offset=48, ChunkSize=24, Label="")]
public class _48_buttons_holder
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Skin", Offset=4)]
public Dependancy _4_Skin;
[Value(Name="Coord X", Offset=12)]
public Int16 _12_Coord_X;
[Value(Name="Coord Y", Offset=14)]
public Int16 _14_Coord_Y;
[Value(Name="unknown", Offset=16)]
public Byte _16_unknown;
[Value(Name="Max Visible Buttons", Offset=17)]
public Byte _17_Max_Visible_Buttons;
[Value(Name="Unknown", Offset=18)]
public Byte _18_Unknown;
[Value(Name="Unknown", Offset=19)]
public Byte _19_Unknown;
[Value(Name="Coord X", Offset=20)]
public Int16 _20_Coord_X;
[Value(Name="Coord Y", Offset=22)]
public Int16 _22_Coord_Y;
}
        }
    }
}