using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("jpt!")]
        public class jpt_
        {
[Value(Name="Radius", Offset=0)]
public Single _0_Radius;
[Value(Name="...To", Offset=4)]
public Single _4____To;
[Value(Name="Cutoff Scale", Offset=8)]
public Single _8_Cutoff_Scale;
[Value(Name="Flags", Offset=12)]
public Bitmask _12_Flags = new Bitmask(new string[] { "Don't Scale Damage By Distance","Area Damage Players Only" }, 4);
[Value(Name="Side Effect", Offset=16)]
public H2.DataTypes.Enum _16_Side_Effect = new H2.DataTypes.Enum(new string[] {  "None", "Harmless", "Lethal To The Unsuspecting", "Emp" }, 2);
[Value(Name="Category", Offset=18)]
public H2.DataTypes.Enum _18_Category = new H2.DataTypes.Enum(new string[] {  "None", "Falling", "Bullet", "Grenade", "High Explosive", "Sniper", "Melee", "Flame", "Mounted Weapon", "Vehicle", "Plasma", "Needle", "Shotgun" }, 2);
[Value(Name="Flags", Offset=20)]
public Bitmask _20_Flags = new Bitmask(new string[] { "Does Not Hurt Owner","Can Cause Headshots","Pings Resistant Units","Does Not Hurt Friends","Does Not Ping Units","Detonates Explosives","Only Hurts Sheilds","Causes Flaming Death","Damage Indicators Always Point Down","Skips Sheilds","Only Hurts One Infection Form","Unused","Infection Form Pop","Ignore Seat Scale For Direct Damage","Forces Hard Ping","Does Not Hurt Players" }, 4);
[Value(Name="AOE Core Radius", Offset=24)]
public Single _24_AOE_Core_Radius;
[Value(Name="Damage Lower Bound", Offset=28)]
public Single _28_Damage_Lower_Bound;
[Value(Name="Damge Upper Bound", Offset=32)]
public Single _32_Damge_Upper_Bound;
[Value(Name="...To", Offset=36)]
public Single _36____To;
[Value(Name="Damage Inner Cone Angle", Offset=40)]
public Single _40_Damage_Inner_Cone_Angle;
[Value(Name="Damage Outter Cone Angle", Offset=44)]
public Single _44_Damage_Outter_Cone_Angle;
[Value(Name="Active Camoflage Damage", Offset=48)]
public Single _48_Active_Camoflage_Damage;
[Value(Name="Stun", Offset=52)]
public Single _52_Stun;
[Value(Name="Max Stun", Offset=56)]
public Single _56_Max_Stun;
[Value(Name="Stun Time", Offset=60)]
public Single _60_Stun_Time;
[Value(Name="Instantaneous Acceleration", Offset=64)]
public Single _64_Instantaneous_Acceleration;
[Value(Name="Rider Direct Damage Scale", Offset=68)]
public Single _68_Rider_Direct_Damage_Scale;
[Value(Name="Rider Max Transfer Damage", Offset=72)]
public Single _72_Rider_Max_Transfer_Damage;
[Value(Name="Rider Min Transfer Damage", Offset=76)]
public Single _76_Rider_Min_Transfer_Damage;
[Value(Name="General Damage", Offset=80)]
public StringID _80_General_Damage;
[Value(Name="Specific Damage", Offset=84)]
public StringID _84_Specific_Damage;
[Value(Name="AI Stun Radius", Offset=88)]
public Single _88_AI_Stun_Radius;
[Value(Name="AI Stun Bounds", Offset=92)]
public Single _92_AI_Stun_Bounds;
[Value(Name="...To", Offset=96)]
public Single _96____To;
[Value(Name="Shake Radius", Offset=100)]
public Single _100_Shake_Radius;
[Value(Name="Emp Radius", Offset=104)]
public Single _104_Emp_Radius;
public _108_player_responses[] _108_Player_Responses;
[Serializable][Reflexive(Name="Player Responses", Offset=108, ChunkSize=76, Label="")]
public class _108_player_responses
{
private int __StartOffset__;
[Value(Name="Response Type", Offset=0)]
public H2.DataTypes.Enum _0_Response_Type = new H2.DataTypes.Enum(new string[] {  "Sheilded", "UnSheiled", "All" }, 2);
[Value(Name="Screen Flash Type", Offset=2)]
public H2.DataTypes.Enum _2_Screen_Flash_Type = new H2.DataTypes.Enum(new string[] {  "None", "Lighten", "Darken", "Max", "Min", "Invert", "Tint" }, 4);
[Value(Name="Screen Flash Priority", Offset=6)]
public H2.DataTypes.Enum _6_Screen_Flash_Priority = new H2.DataTypes.Enum(new string[] {  "Low", "Medium", "High" }, 2);
[Value(Name="Screen Flash Duration", Offset=8)]
public Single _8_Screen_Flash_Duration;
[Value(Name="Screen Flash Fade Function", Offset=12)]
public H2.DataTypes.Enum _12_Screen_Flash_Fade_Function = new H2.DataTypes.Enum(new string[] {  "Linear", "Late", "Very Late", "Early", "Very Early", "Cosine", "Zero", "One" }, 4);
[Value(Name="Screen Flash Max Intensity", Offset=16)]
public Single _16_Screen_Flash_Max_Intensity;
[Value(Name="Screen Flash Color A", Offset=20)]
public Single _20_Screen_Flash_Color_A;
[Value(Name="Screen Flash Color R", Offset=24)]
public Single _24_Screen_Flash_Color_R;
[Value(Name="Screen Flash Color G", Offset=28)]
public Single _28_Screen_Flash_Color_G;
[Value(Name="Screen Flash Color B", Offset=32)]
public Single _32_Screen_Flash_Color_B;
[Value(Name="Low Frequency Vibration Duration", Offset=36)]
public Single _36_Low_Frequency_Vibration_Duration;
public _40_low_frequency_vibration[] _40_Low_Frequency_Vibration;
[Serializable][Reflexive(Name="Low Frequency Vibration", Offset=40, ChunkSize=1, Label="")]
public class _40_low_frequency_vibration
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="High Frequency Vibration Duration", Offset=48)]
public Single _48_High_Frequency_Vibration_Duration;
public _52_high_frequency_vibration[] _52_High_Frequency_Vibration;
[Serializable][Reflexive(Name="High Frequency Vibration", Offset=52, ChunkSize=1, Label="")]
public class _52_high_frequency_vibration
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Sound Effect Name", Offset=60)]
public StringID _60_Sound_Effect_Name;
[Value(Name="Sound Effect Duration", Offset=64)]
public Single _64_Sound_Effect_Duration;
public _68_sound_effect[] _68_Sound_Effect;
[Serializable][Reflexive(Name="Sound Effect", Offset=68, ChunkSize=12, Label="")]
public class _68_sound_effect
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[12];
}
}
[Value(Name="Temporary Camera Impulse Duration", Offset=116)]
public Single _116_Temporary_Camera_Impulse_Duration;
[Value(Name="Temporary Camera Impulse Fade Function", Offset=120)]
public H2.DataTypes.Enum _120_Temporary_Camera_Impulse_Fade_Function = new H2.DataTypes.Enum(new string[] {  "Linear", "Late", "Very Late", "Early", "Very Early", "Cosine", "Zero", "One" }, 4);
[Value(Name="Temporary Camera Impulse Rotation", Offset=124)]
public Single _124_Temporary_Camera_Impulse_Rotation;
[Value(Name="Temporary Camera Impulse Pushback", Offset=128)]
public Single _128_Temporary_Camera_Impulse_Pushback;
[Value(Name="Temporary Camera Impulse Jitter", Offset=132)]
public Single _132_Temporary_Camera_Impulse_Jitter;
[Value(Name="...To", Offset=136)]
public Single _136____To;
[Value(Name="Camera Shaking Duration", Offset=140)]
public Single _140_Camera_Shaking_Duration;
[Value(Name="Camera Shaking Falloff Function", Offset=144)]
public H2.DataTypes.Enum _144_Camera_Shaking_Falloff_Function = new H2.DataTypes.Enum(new string[] {  "Linear", "Late", "Very Late", "Early", "Very Early", "Cosine", "Zero", "One" }, 4);
[Value(Name="Camera Shaking Random Translation", Offset=148)]
public Single _148_Camera_Shaking_Random_Translation;
[Value(Name="Camera Shaking Random Rotation", Offset=152)]
public Single _152_Camera_Shaking_Random_Rotation;
[Value(Name="Camera Shaking Wobble Function", Offset=156)]
public H2.DataTypes.Enum _156_Camera_Shaking_Wobble_Function = new H2.DataTypes.Enum(new string[] {  "One", "Zero", "Cosine", "Cosine (Variable Period)", "Diagonal Wave", "Diagonal Wave (Variable Period)", "Slide", "Slide (Variable Period)", "Noise", "Jitter", "Wander", "Spark" }, 4);
[Value(Name="Camera Shaking Wobble Function Period", Offset=160)]
public Single _160_Camera_Shaking_Wobble_Function_Period;
[Value(Name="Camera Shaking Wobble Weight", Offset=164)]
public Single _164_Camera_Shaking_Wobble_Weight;
[Value(Name="Sound", Offset=168)]
public Dependancy _168_Sound;
[Value(Name="Breaking Effect Forward Velocity", Offset=172)]
public Single _172_Breaking_Effect_Forward_Velocity;
[Value(Name="Breaking Effect Forward Radius", Offset=176)]
public Single _176_Breaking_Effect_Forward_Radius;
[Value(Name="Breaking Effect Forward Exponent", Offset=180)]
public Single _180_Breaking_Effect_Forward_Exponent;
[Value(Name="Breaking Effect Outward Velocity", Offset=184)]
public Single _184_Breaking_Effect_Outward_Velocity;
[Value(Name="Breaking Effect Outward Radius", Offset=188)]
public Single _188_Breaking_Effect_Outward_Radius;
[Value(Name="Breaking Effect Outward Exponent", Offset=192)]
public Single _192_Breaking_Effect_Outward_Exponent;
        }
    }
}