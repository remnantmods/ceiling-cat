using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("mulg")]
        public class mulg
        {
public _0_universal[] _0_Universal;
[Serializable][Reflexive(Name="Universal", Offset=0, ChunkSize=32, Label="")]
public class _0_universal
{
private int __StartOffset__;
[Value(Name="Random Player names", Offset=0)]
public Dependancy _0_Random_Player_names;
[Value(Name="Team names", Offset=8)]
public Dependancy _8_Team_names;
public _16_team_colors[] _16_Team_Colors;
[Serializable][Reflexive(Name="Team Colors", Offset=16, ChunkSize=12, Label="")]
public class _16_team_colors
{
private int __StartOffset__;
[Value(Name="Color Red", Offset=0)]
public Single _0_Color_Red;
[Value(Name="Color Green", Offset=4)]
public Single _4_Color_Green;
[Value(Name="Color Blue", Offset=8)]
public Single _8_Color_Blue;
}
[Value(Name="Multiplayer Text", Offset=24)]
public Dependancy _24_Multiplayer_Text;
}
public _8_runtime[] _8_Runtime;
[Serializable][Reflexive(Name="Runtime", Offset=8, ChunkSize=1384, Label="")]
public class _8_runtime
{
private int __StartOffset__;
[Value(Name="Flag", Offset=0)]
public Dependancy _0_Flag;
[Value(Name="Ball", Offset=8)]
public Dependancy _8_Ball;
[Value(Name="Unit", Offset=16)]
public Dependancy _16_Unit;
[Value(Name="Flag Shader", Offset=24)]
public Dependancy _24_Flag_Shader;
[Value(Name="Hill Shader", Offset=32)]
public Dependancy _32_Hill_Shader;
[Value(Name="Head", Offset=40)]
public Dependancy _40_Head;
[Value(Name="Juggernaut Powerup", Offset=48)]
public Dependancy _48_Juggernaut_Powerup;
[Value(Name="Bomb", Offset=56)]
public Dependancy _56_Bomb;
[Value(Offset=64)]
public Byte[] _64_ = new byte[40];
public _104_weapon[] _104_Weapon;
[Serializable][Reflexive(Name="Weapon", Offset=104, ChunkSize=8, Label="")]
public class _104_weapon
{
private int __StartOffset__;
[Value(Name="Weapon", Offset=0)]
public Dependancy _0_Weapon;
}
public _112_vehicles[] _112_Vehicles;
[Serializable][Reflexive(Name="Vehicles", Offset=112, ChunkSize=8, Label="")]
public class _112_vehicles
{
private int __StartOffset__;
[Value(Name="Vehicle", Offset=0)]
public Dependancy _0_Vehicle;
}
public _120_grenades[] _120_Grenades;
[Serializable][Reflexive(Name="Grenades", Offset=120, ChunkSize=8, Label="")]
public class _120_grenades
{
private int __StartOffset__;
[Value(Name="Weapon", Offset=0)]
public Dependancy _0_Weapon;
}
public _128_powerups[] _128_Powerups;
[Serializable][Reflexive(Name="Powerups", Offset=128, ChunkSize=8, Label="")]
public class _128_powerups
{
private int __StartOffset__;
[Value(Name="Weapon", Offset=0)]
public Dependancy _0_Weapon;
}
[Value(Name="In Game Text", Offset=136)]
public Dependancy _136_In_Game_Text;
public _144_sounds[] _144_Sounds;
[Serializable][Reflexive(Name="Sounds", Offset=144, ChunkSize=8, Label="")]
public class _144_sounds
{
private int __StartOffset__;
[Value(Name="Sound", Offset=0)]
public Dependancy _0_Sound;
}
public _152_general_events[] _152_General_Events;
[Serializable][Reflexive(Name="General Events", Offset=152, ChunkSize=168, Label="")]
public class _152_general_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Kill", "Suicide", "Kill Teammate", "Victory", "Team Victory", "unused", "unused", "1 Min To Win", "Team 1 Min To Win", "30 Sec To Win", "Team 30 Sec To Win", "Player Quit", "Player Joined", "Killed By Unknown", "30 Minutes Left", "15 Minutes Left", "5 Minutes Left", "1 Minute Left", "Time Expired", "Game Over", "Respawn Tick", "Last Respawn Tick", "Teleporter Used", "Player Changed Team", "Player Rejoined", "Gained Lead", "Gained Team Lead", "Lost Lead", "Lost Team Lead", "Tied Leader", "Tied Team Leader", "Round Over", "30 Seconds Left", "10 Seconds Left", "Kill (Falling)", "Kill (Collision)", "Kill (Melee)", "Sudden Death", "Player Booted Player", "Kill (Flag Carrier)", "Kill (Bomb Carrier)", "Kill (Sticky Grenade)", "Kill (Standard Melee)", "Boarded Vehicle", "Start Team Notice", "Telefrag", "10 Sec To Win", "Team 10 Sec To Win" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
public _160_flavor_events[] _160_Flavor_Events;
[Serializable][Reflexive(Name="Flavor Events", Offset=160, ChunkSize=168, Label="")]
public class _160_flavor_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Double Kill", "Triple Kill", "Killtacular", "Killing Spree", "Running Riot", "Well Placed Kill", "Broke Killing Spree", "Kill Frenzy", "Killtrocity", "Killimajaro", "15 In A Row", "20 In A Row", "25 In A Row" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
public _168_slayer_events[] _168_Slayer_Events;
[Serializable][Reflexive(Name="Slayer Events", Offset=168, ChunkSize=168, Label="")]
public class _168_slayer_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "New Target" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
public _176_ctf_events[] _176_CTF_Events;
[Serializable][Reflexive(Name="CTF Events", Offset=176, ChunkSize=168, Label="")]
public class _176_ctf_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "Flag Taken", "Flag Dropped", "Flag Returned By Player", "Flag Returned By Timeout", "Flag Captured", "Flag New Defensive Team", "Flag Return Failure", "Side Switch Tick", "Side Switch Final Tick", "Side Switch 30 Seconds", "Side Switch 10 Seconds", "Flag Contested", "Flag Capture Failure" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
public _184_oddball_events[] _184_Oddball_Events;
[Serializable][Reflexive(Name="Oddball Events", Offset=184, ChunkSize=168, Label="")]
public class _184_oddball_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "Ball Spawned", "Ball Pickup", "Ball Dropped", "Ball Reset", "Ball Tick" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
[Value(Offset=192)]
public Byte[] _192_ = new byte[8];
public _200_koth_events[] _200_KOTH_Events;
[Serializable][Reflexive(Name="KOTH Events", Offset=200, ChunkSize=168, Label="")]
public class _200_koth_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "Hill Controlled", "Hill Contested", "Hill Tick", "Hill Move", "Hill Controlled Team", "Hill Contested Team" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
[Value(Offset=208)]
public Byte[] _208_ = new byte[8];
public _216_juggernaut_events[] _216_Juggernaut_Events;
[Serializable][Reflexive(Name="Juggernaut Events", Offset=216, ChunkSize=168, Label="")]
public class _216_juggernaut_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "New Juggernaught", "Juggernaught Killed" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
public _224_territorys_events[] _224_Territorys_Events;
[Serializable][Reflexive(Name="Territorys Events", Offset=224, ChunkSize=168, Label="")]
public class _224_territorys_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "Territory Control Gained", "Territory Contest Lost", "All Territories Controled", "Team Territory Control Gained", "Team Territory Control Lost", "Team All Territories Controled" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
public _232_assualt_events[] _232_Assualt_Events;
[Serializable][Reflexive(Name="Assualt Events", Offset=232, ChunkSize=168, Label="")]
public class _232_assualt_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Quantity Message" }, 4);
[Value(Name="Event", Offset=4)]
public H2.DataTypes.Enum _4_Event = new H2.DataTypes.Enum(new string[] {  "Game Start", "Bomb Taken", "Bomb Dropped", "Bomb Returned By Player", "Bomb Returned By Timeout", "Bomb Captured", "Bomb New Defensive Team", "Bomb Return Failure", "Side Switch Tick", "Side Switch Final Tick", "Side Switch 30 Seconds", "Side Switch 10 Seconds", "Bomb Returned By Defusing", "Bomb Placed On Enemy Post", "Bomb Arming Started", "Bomb Arming Completed", "Bomb Contested" }, 2);
[Value(Name="Audience", Offset=6)]
public H2.DataTypes.Enum _6_Audience = new H2.DataTypes.Enum(new string[] {  "Cause Player", "Cause Team", "Effect Player", "Effect Team", "All" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Display String", Offset=12)]
public StringID _12_Display_String;
[Value(Name="Required Field", Offset=16)]
public H2.DataTypes.Enum _16_Required_Field = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Excluded Audience", Offset=18)]
public H2.DataTypes.Enum _18_Excluded_Audience = new H2.DataTypes.Enum(new string[] {  "None", "Cause Player", "Cause Team", "Effect Player", "Effect Team" }, 2);
[Value(Name="Primary String", Offset=20)]
public StringID _20_Primary_String;
[Value(Name="Primary String Duration", Offset=24)]
public Single _24_Primary_String_Duration;
[Value(Name="Pural Display String", Offset=28)]
public StringID _28_Pural_Display_String;
[Value(Offset=32)]
public Byte[] _32_ = new byte[28];
[Value(Name="Sound Delay (Announcer Only)", Offset=60)]
public Single _60_Sound_Delay__Announcer_Only_;
[Value(Name="Sound Flags", Offset=64)]
public Bitmask _64_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=68)]
public Dependancy _68_English_Sound;
[Value(Name="Japanese Sound", Offset=76)]
public Dependancy _76_Japanese_Sound;
[Value(Name="German Sound", Offset=84)]
public Dependancy _84_German_Sound;
[Value(Name="French Sound", Offset=92)]
public Dependancy _92_French_Sound;
[Value(Name="Spainish Sound", Offset=100)]
public Dependancy _100_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=108)]
public Dependancy _108_Italinan_Sound;
[Value(Name="Korean Sound", Offset=116)]
public Dependancy _116_Korean_Sound;
[Value(Name="Chinese Sound", Offset=124)]
public Dependancy _124_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=132)]
public Dependancy _132_Portuguese_Sound;
public _140_sound_permutations[] _140_Sound_Permutations;
[Serializable][Reflexive(Name="Sound Permutations", Offset=140, ChunkSize=80, Label="")]
public class _140_sound_permutations
{
private int __StartOffset__;
[Value(Name="Sound Flags", Offset=0)]
public Bitmask _0_Sound_Flags = new Bitmask(new string[] { "Announcer Sound" }, 4);
[Value(Name="English Sound", Offset=4)]
public Dependancy _4_English_Sound;
[Value(Name="Japanese Sound", Offset=12)]
public Dependancy _12_Japanese_Sound;
[Value(Name="German Sound", Offset=20)]
public Dependancy _20_German_Sound;
[Value(Name="French Sound", Offset=28)]
public Dependancy _28_French_Sound;
[Value(Name="Spainish Sound", Offset=36)]
public Dependancy _36_Spainish_Sound;
[Value(Name="Italinan Sound", Offset=44)]
public Dependancy _44_Italinan_Sound;
[Value(Name="Korean Sound", Offset=52)]
public Dependancy _52_Korean_Sound;
[Value(Name="Chinese Sound", Offset=60)]
public Dependancy _60_Chinese_Sound;
[Value(Name="Portuguese Sound", Offset=68)]
public Dependancy _68_Portuguese_Sound;
[Value(Name="Probability", Offset=76)]
public Single _76_Probability;
}
[Value(Offset=148)]
public Byte[] _148_ = new byte[20];
}
[Value(Offset=240)]
public Byte[] _240_ = new byte[32];
[Value(Name="Default Item Collection 1", Offset=272)]
public Dependancy _272_Default_Item_Collection_1;
[Value(Name="Default Item Collection 2", Offset=280)]
public Dependancy _280_Default_Item_Collection_2;
[Value(Name="Default Frag Grenade Count", Offset=288)]
public Single _288_Default_Frag_Grenade_Count;
[Value(Name="Default Plasma Grenade Count", Offset=292)]
public Single _292_Default_Plasma_Grenade_Count;
[Value(Offset=296)]
public Byte[] _296_ = new byte[40];
[Value(Name="Dynamic Spawn Zone Upper Height", Offset=336)]
public Single _336_Dynamic_Spawn_Zone_Upper_Height;
[Value(Name="Dynamic Spawn Zone Lower Height", Offset=340)]
public Single _340_Dynamic_Spawn_Zone_Lower_Height;
[Value(Offset=344)]
public Byte[] _344_ = new byte[40];
[Value(Name="Enemy Inner Radius", Offset=384)]
public Single _384_Enemy_Inner_Radius;
[Value(Name="Enemy Outer Radius", Offset=388)]
public Single _388_Enemy_Outer_Radius;
[Value(Name="Enemy Weight", Offset=392)]
public Single _392_Enemy_Weight;
[Value(Offset=396)]
public Byte[] _396_ = new byte[16];
[Value(Name="Friend Inner Radius", Offset=412)]
public Single _412_Friend_Inner_Radius;
[Value(Name="Friend Outer Radius", Offset=416)]
public Single _416_Friend_Outer_Radius;
[Value(Name="Friend Weight", Offset=420)]
public Single _420_Friend_Weight;
[Value(Offset=424)]
public Byte[] _424_ = new byte[16];
[Value(Name="Enemy Vehicle Inner Radius", Offset=440)]
public Single _440_Enemy_Vehicle_Inner_Radius;
[Value(Name="Enemy Vehicle Outer Radius", Offset=444)]
public Single _444_Enemy_Vehicle_Outer_Radius;
[Value(Name="Enemy Vehicle Weight", Offset=448)]
public Single _448_Enemy_Vehicle_Weight;
[Value(Offset=452)]
public Byte[] _452_ = new byte[16];
[Value(Name="Friendly Vehicle Inner Radius", Offset=468)]
public Single _468_Friendly_Vehicle_Inner_Radius;
[Value(Name="Friendly Vehicle Outer Radius", Offset=472)]
public Single _472_Friendly_Vehicle_Outer_Radius;
[Value(Name="Friendly Vehicle Weight", Offset=476)]
public Single _476_Friendly_Vehicle_Weight;
[Value(Offset=480)]
public Byte[] _480_ = new byte[16];
[Value(Name="Empty Vehicle Inner Radius", Offset=496)]
public Single _496_Empty_Vehicle_Inner_Radius;
[Value(Name="Empty Vehicle Outer Radius", Offset=500)]
public Single _500_Empty_Vehicle_Outer_Radius;
[Value(Name="Empty Vehicle Weight", Offset=504)]
public Single _504_Empty_Vehicle_Weight;
[Value(Offset=508)]
public Byte[] _508_ = new byte[16];
[Value(Name="Oddball Inclusion Inner Radius", Offset=524)]
public Single _524_Oddball_Inclusion_Inner_Radius;
[Value(Name="Oddball Inclusion Outer Radius", Offset=528)]
public Single _528_Oddball_Inclusion_Outer_Radius;
[Value(Name="Oddball Inclusion Weight", Offset=532)]
public Single _532_Oddball_Inclusion_Weight;
[Value(Offset=536)]
public Byte[] _536_ = new byte[16];
[Value(Name="Oddball Exclusion Inner Radius", Offset=552)]
public Single _552_Oddball_Exclusion_Inner_Radius;
[Value(Name="Oddball Exclusion Outer Radius", Offset=556)]
public Single _556_Oddball_Exclusion_Outer_Radius;
[Value(Name="Oddball Exclusion Weight", Offset=560)]
public Single _560_Oddball_Exclusion_Weight;
[Value(Offset=564)]
public Byte[] _564_ = new byte[16];
[Value(Name="Hill Inclusion Inner Radius", Offset=580)]
public Single _580_Hill_Inclusion_Inner_Radius;
[Value(Name="Hill Inclusion Outer Radius", Offset=584)]
public Single _584_Hill_Inclusion_Outer_Radius;
[Value(Name="Hill Inclusion Weight", Offset=588)]
public Single _588_Hill_Inclusion_Weight;
[Value(Offset=592)]
public Byte[] _592_ = new byte[16];
[Value(Name="Hill Exclusion Inner Radius", Offset=608)]
public Single _608_Hill_Exclusion_Inner_Radius;
[Value(Name="Hill Exclusion Outer Radius", Offset=612)]
public Single _612_Hill_Exclusion_Outer_Radius;
[Value(Name="Hill Exclusion Weight", Offset=616)]
public Single _616_Hill_Exclusion_Weight;
[Value(Offset=620)]
public Byte[] _620_ = new byte[16];
[Value(Name="Last Race Flag Inner Radius", Offset=636)]
public Single _636_Last_Race_Flag_Inner_Radius;
[Value(Name="Last Race Flag Outer Radius", Offset=640)]
public Single _640_Last_Race_Flag_Outer_Radius;
[Value(Name="Last Race Flag Weight", Offset=644)]
public Single _644_Last_Race_Flag_Weight;
[Value(Offset=648)]
public Byte[] _648_ = new byte[16];
[Value(Name="Dead Ally Inner Radius", Offset=664)]
public Single _664_Dead_Ally_Inner_Radius;
[Value(Name="Dead Ally Outer Radius", Offset=668)]
public Single _668_Dead_Ally_Outer_Radius;
[Value(Name="Dead Ally Weight", Offset=672)]
public Single _672_Dead_Ally_Weight;
[Value(Offset=676)]
public Byte[] _676_ = new byte[16];
[Value(Name="Controlled Territory Inner Radius", Offset=692)]
public Single _692_Controlled_Territory_Inner_Radius;
[Value(Name="Controlled Territory Outer Radius", Offset=696)]
public Single _696_Controlled_Territory_Outer_Radius;
[Value(Name="Controlled Territory Weight", Offset=700)]
public Single _700_Controlled_Territory_Weight;
[Value(Offset=704)]
public Byte[] _704_ = new byte[624];
public _1328_multiplayer_constants[] _1328_Multiplayer_Constants;
[Serializable][Reflexive(Name="Multiplayer Constants", Offset=1328, ChunkSize=352, Label="")]
public class _1328_multiplayer_constants
{
private int __StartOffset__;
[Value(Name="Max Random Spawn Bias", Offset=0)]
public Single _0_Max_Random_Spawn_Bias;
[Value(Name="Teleporter Recharge Time (Sec)", Offset=4)]
public Single _4_Teleporter_Recharge_Time__Sec_;
[Value(Name="Grenade Danger Weight", Offset=8)]
public Single _8_Grenade_Danger_Weight;
[Value(Name="Grenade Danger Inner Radius", Offset=12)]
public Single _12_Grenade_Danger_Inner_Radius;
[Value(Name="Grenade Danger Outer Radius", Offset=16)]
public Single _16_Grenade_Danger_Outer_Radius;
[Value(Name="Grenade Danger Lead Time", Offset=20)]
public Single _20_Grenade_Danger_Lead_Time;
[Value(Name="Vehicle Danger Min Speed", Offset=24)]
public Single _24_Vehicle_Danger_Min_Speed;
[Value(Name="Vehicle Danger Weight", Offset=28)]
public Single _28_Vehicle_Danger_Weight;
[Value(Name="Vehicle Danger Radius", Offset=32)]
public Single _32_Vehicle_Danger_Radius;
[Value(Name="Vehicle Danger Lead Time", Offset=36)]
public Single _36_Vehicle_Danger_Lead_Time;
[Value(Name="Vehicle Nearby Player Distance", Offset=40)]
public Single _40_Vehicle_Nearby_Player_Distance;
[Value(Offset=44)]
public Byte[] _44_ = new byte[148];
[Value(Name="Hill Shader", Offset=192)]
public Dependancy _192_Hill_Shader;
[Value(Offset=200)]
public Byte[] _200_ = new byte[16];
[Value(Name="Flag Reset Stop Distance", Offset=216)]
public Single _216_Flag_Reset_Stop_Distance;
[Value(Name="Bomb Explode Effect", Offset=220)]
public Dependancy _220_Bomb_Explode_Effect;
[Value(Name="Bomb Explode Damage Effect", Offset=228)]
public Dependancy _228_Bomb_Explode_Damage_Effect;
[Value(Name="Bomb Explode Defuse Effect", Offset=236)]
public Dependancy _236_Bomb_Explode_Defuse_Effect;
[Value(Name="Bomb Defusal String", Offset=244)]
public StringID _244_Bomb_Defusal_String;
[Value(Name="Blocked Teleporter String", Offset=248)]
public StringID _248_Blocked_Teleporter_String;
[Value(Offset=252)]
public Byte[] _252_ = new byte[100];
}
public _1336_state_responses[] _1336_State_Responses;
[Serializable][Reflexive(Name="State Responses", Offset=1336, ChunkSize=28, Label="")]
public class _1336_state_responses
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "unused" }, 4);
[Value(Name="State", Offset=4)]
public H2.DataTypes.Enum _4_State = new H2.DataTypes.Enum(new string[] {  "Waiting For Space To Clear", "Observing", "Respawing Soon", "Sitting Out", "Out Of Lives", "Playing (Winning)", "Playing (Tied)", "Playing (Losing)", "Game Over (Won)", "Game Over (Tied)", "Game Over (Lost)", "You Have Flag", "Enemy Has Flag", "Flag Not Home", "Carrying Oddball", "You Are Juggernaught", "You Control Hill", "Switching Sides Soon", "Player Recently Started", "You Have Bomb", "Flag Contested", "Bomb Contested", "Limited Lives Left (Multiple)", "Limited Lives Left (Single)", "Limited Lives Left (Final)", "Playing (Winning, Unlimited)", "Playing (Tied, Unlimited)", "Playing (Losing, Unlimited)" }, 4);
[Value(Name="FFA Message", Offset=8)]
public StringID _8_FFA_Message;
[Value(Name="Team Message", Offset=12)]
public StringID _12_Team_Message;
[Value(Offset=16)]
public Byte[] _16_ = new byte[12];
}
[Value(Name="Scorebard HUD Definition", Offset=1344)]
public Dependancy _1344_Scorebard_HUD_Definition;
[Value(Name="Scorebard Emblem Shader", Offset=1352)]
public Dependancy _1352_Scorebard_Emblem_Shader;
[Value(Name="Scorebard Emblem Bitmap", Offset=1360)]
public Dependancy _1360_Scorebard_Emblem_Bitmap;
[Value(Name="Scorebard Dead Emblem Shader", Offset=1368)]
public Dependancy _1368_Scorebard_Dead_Emblem_Shader;
[Value(Name="Scorebard Dead Emblem Bitmap", Offset=1376)]
public Dependancy _1376_Scorebard_Dead_Emblem_Bitmap;
}
        }
    }
}