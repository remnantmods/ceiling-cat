using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("ltmp")]
        public class ltmp
        {
[Value(Name="Search Distance Lower Bound", Offset=0)]
public Single _0_Search_Distance_Lower_Bound;
[Value(Name="Search Distance Upper Bound", Offset=4)]
public Single _4_Search_Distance_Upper_Bound;
[Value(Name="Luminels Per World Unit", Offset=8)]
public Single _8_Luminels_Per_World_Unit;
[Value(Name="Output White Reference", Offset=12)]
public Single _12_Output_White_Reference;
[Value(Name="Output Black Reference", Offset=16)]
public Single _16_Output_Black_Reference;
[Value(Name="Output Schlick Reference", Offset=20)]
public Single _20_Output_Schlick_Reference;
[Value(Name="Diffuse Map Scale", Offset=24)]
public Single _24_Diffuse_Map_Scale;
[Value(Name="Sun Scale", Offset=28)]
public Single _28_Sun_Scale;
[Value(Name="Sky Scale", Offset=32)]
public Single _32_Sky_Scale;
[Value(Name="Indirect Scale", Offset=36)]
public Single _36_Indirect_Scale;
[Value(Name="Prt Scale", Offset=40)]
public Single _40_Prt_Scale;
[Value(Name="Surface Light Scale", Offset=44)]
public Single _44_Surface_Light_Scale;
[Value(Name="Scenario Light Scale", Offset=48)]
public Single _48_Scenario_Light_Scale;
[Value(Name="Lightprobe Interpolation Override", Offset=52)]
public Single _52_Lightprobe_Interpolation_Override;
[Value(Offset=56)]
public Byte[] _56_ = new byte[72];
public _128_lightmap_groups[] _128_Lightmap_Groups;
[Serializable][Reflexive(Name="Lightmap Groups", Offset=128, ChunkSize=104, Label="")]
public class _128_lightmap_groups
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  "Normal" }, 2);
[Value(Name="Flags", Offset=2)]
public Bitmask _2_Flags = new Bitmask(new string[] { "Unused" }, 2);
[Value(Name="Structure Checksum", Offset=4)]
public Int32 _4_Structure_Checksum;
public _8_section_palette[] _8_Section_Palette;
[Serializable][Reflexive(Name="Section Palette", Offset=8, ChunkSize=1024, Label="")]
public class _8_section_palette
{
private int __StartOffset__;
[Value(Name="First Palette Color", Offset=0)]
public Int32 _0_First_Palette_Color;
[Value(Offset=4)]
public Byte[] _4_ = new byte[1020];
}
public _16_writable_palettes[] _16_Writable_Palettes;
[Serializable][Reflexive(Name="Writable Palettes", Offset=16, ChunkSize=1024, Label="")]
public class _16_writable_palettes
{
private int __StartOffset__;
[Value(Name="First Palette Color", Offset=0)]
public Int32 _0_First_Palette_Color;
[Value(Offset=4)]
public Byte[] _4_ = new byte[1020];
}
[Value(Name="Bitmap Group", Offset=24)]
public Dependancy _24_Bitmap_Group;
public _32_clusters[] _32_Clusters;
[Serializable][Reflexive(Name="Clusters", Offset=32, ChunkSize=84, Label="")]
public class _32_clusters
{
private int __StartOffset__;
[Value(Name="Total Vertex Count", Offset=0)]
public Int16 _0_Total_Vertex_Count;
[Value(Name="Total Triangle Count", Offset=2)]
public Int16 _2_Total_Triangle_Count;
[Value(Name="Total Part Count", Offset=4)]
public Int16 _4_Total_Part_Count;
[Value(Name="Shadow-Casting Triangle Count", Offset=6)]
public Int16 _6_Shadow_Casting_Triangle_Count;
[Value(Name="Shadow-Casting Part Count", Offset=8)]
public Int16 _8_Shadow_Casting_Part_Count;
[Value(Name="Opaque Point Count", Offset=10)]
public Int16 _10_Opaque_Point_Count;
[Value(Name="Opaque Vertex Count", Offset=12)]
public Int16 _12_Opaque_Vertex_Count;
[Value(Name="Opaque Part Count", Offset=14)]
public Int16 _14_Opaque_Part_Count;
[Value(Name="Opague Max Nodes/Vertex", Offset=16)]
public Int16 _16_Opague_Max_Nodes_Vertex;
[Value(Name="Transparent Max Nodes/Vertex", Offset=18)]
public Int16 _18_Transparent_Max_Nodes_Vertex;
[Value(Name="Shadow-Casting Rigid Triangle", Offset=20)]
public Int16 _20_Shadow_Casting_Rigid_Triangle;
[Value(Name="Geometry Classification", Offset=22)]
public H2.DataTypes.Enum _22_Geometry_Classification = new H2.DataTypes.Enum(new string[] {  "World Space" }, 4);
[Value(Name="Geometry Compression Flags", Offset=26)]
public Bitmask _26_Geometry_Compression_Flags = new Bitmask(new string[] { "Compressed Position","Compressed Texcoord","Compressed Secondary Texcoord" }, 2);
[Value(Name="Hardware Node Count", Offset=28)]
public Int16 _28_Hardware_Node_Count;
[Value(Name="Node Map Size", Offset=30)]
public Int16 _30_Node_Map_Size;
[Value(Name="Software Plane Count", Offset=32)]
public Int16 _32_Software_Plane_Count;
[Value(Name="Total Subpart Count", Offset=34)]
public Int16 _34_Total_Subpart_Count;
[Value(Name="Section Lighting Flags", Offset=36)]
public Bitmask _36_Section_Lighting_Flags = new Bitmask(new string[] { "Has LM Texcoords","Has LM Inc. Rad.","Has LM Colors","Has LM Prt" }, 4);
[Value(Name="Block Offset", Offset=40)]
public Int32 _40_Block_Offset;
[Value(Name="Block Size", Offset=44)]
public Int32 _44_Block_Size;
[Value(Name="Section Data Size", Offset=48)]
public Int32 _48_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=52)]
public Int32 _52_Resource_Data_Size;
[Value(Offset=56)]
public Byte[] _56_ = new byte[12];
[Value(Name="Owner Tag Section Offset", Offset=68)]
public Int32 _68_Owner_Tag_Section_Offset;
[Value(Offset=72)]
public Byte[] _72_ = new byte[12];
}
public _40_cluster_render_info[] _40_Cluster_Render_Info;
[Serializable][Reflexive(Name="Cluster Render Info", Offset=40, ChunkSize=4, Label="")]
public class _40_cluster_render_info
{
private int __StartOffset__;
[Value(Name="Bitmap Index", Offset=0)]
public Int16 _0_Bitmap_Index;
[Value(Name="Palette Index", Offset=2)]
public Int16 _2_Palette_Index;
}
public _48_poop_definitions[] _48_Poop_Definitions;
[Serializable][Reflexive(Name="Poop Definitions", Offset=48, ChunkSize=84, Label="")]
public class _48_poop_definitions
{
private int __StartOffset__;
[Value(Name="Total Vertex Count", Offset=0)]
public Int16 _0_Total_Vertex_Count;
[Value(Name="Total Triangle Count", Offset=2)]
public Int16 _2_Total_Triangle_Count;
[Value(Name="Total Part Count", Offset=4)]
public Int16 _4_Total_Part_Count;
[Value(Name="Shadow-Casting Triangle Count", Offset=6)]
public Int16 _6_Shadow_Casting_Triangle_Count;
[Value(Name="Shadow-Casting Part Count", Offset=8)]
public Int16 _8_Shadow_Casting_Part_Count;
[Value(Name="Opaque Point Count", Offset=10)]
public Int16 _10_Opaque_Point_Count;
[Value(Name="Opaque Vertex Count", Offset=12)]
public Int16 _12_Opaque_Vertex_Count;
[Value(Name="Opaque Part Count", Offset=14)]
public Int16 _14_Opaque_Part_Count;
[Value(Name="Opague Max Nodes/Vertex", Offset=16)]
public Int16 _16_Opague_Max_Nodes_Vertex;
[Value(Name="Transparent Max Nodes/Vertex", Offset=18)]
public Int16 _18_Transparent_Max_Nodes_Vertex;
[Value(Name="Shadow-Casting Rigid Triangle", Offset=20)]
public Int16 _20_Shadow_Casting_Rigid_Triangle;
[Value(Name="Geometry Classification", Offset=22)]
public H2.DataTypes.Enum _22_Geometry_Classification = new H2.DataTypes.Enum(new string[] {  "World Space" }, 4);
[Value(Name="Geometry Compression Flags", Offset=26)]
public Bitmask _26_Geometry_Compression_Flags = new Bitmask(new string[] { "Compressed Position","Compressed Texcoord","Compressed Secondary Texcoord" }, 2);
[Value(Name="Hardware Node Count", Offset=28)]
public Int16 _28_Hardware_Node_Count;
[Value(Name="Node Map Size", Offset=30)]
public Int16 _30_Node_Map_Size;
[Value(Name="Software Plane Count", Offset=32)]
public Int16 _32_Software_Plane_Count;
[Value(Name="Total Subpart Count", Offset=34)]
public Int16 _34_Total_Subpart_Count;
[Value(Name="Section Lighting Flags", Offset=36)]
public Bitmask _36_Section_Lighting_Flags = new Bitmask(new string[] { "Has LM Texcoords","Has LM Inc. Rad.","Has LM Colors","Has LM Prt" }, 4);
[Value(Name="Block Offset", Offset=40)]
public Int32 _40_Block_Offset;
[Value(Name="Block Size", Offset=44)]
public Int32 _44_Block_Size;
[Value(Name="Section Data Size", Offset=48)]
public Int32 _48_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=52)]
public Int32 _52_Resource_Data_Size;
[Value(Offset=56)]
public Byte[] _56_ = new byte[12];
[Value(Name="Owner Tag Section Offset", Offset=68)]
public Int32 _68_Owner_Tag_Section_Offset;
[Value(Offset=72)]
public Byte[] _72_ = new byte[12];
}
public _56_lighting_environments[] _56_Lighting_Environments;
[Serializable][Reflexive(Name="Lighting Environments", Offset=56, ChunkSize=220, Label="")]
public class _56_lighting_environments
{
private int __StartOffset__;
[Value(Name="Sample Point X", Offset=0)]
public Single _0_Sample_Point_X;
[Value(Name="Sample Point Y", Offset=4)]
public Single _4_Sample_Point_Y;
[Value(Name="Sample Point Z", Offset=8)]
public Single _8_Sample_Point_Z;
[Value(Name="Red Coefficient", Offset=12)]
public Single _12_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=16)]
public Single _16_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=20)]
public Single _20_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=24)]
public Single _24_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=28)]
public Single _28_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=32)]
public Single _32_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=36)]
public Single _36_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=40)]
public Single _40_Red_Coefficient;
[Value(Name="Red Coefficient", Offset=44)]
public Single _44_Red_Coefficient;
[Value(Name="Green Coefficient", Offset=48)]
public Single _48_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=52)]
public Single _52_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=56)]
public Single _56_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=60)]
public Single _60_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=64)]
public Single _64_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=68)]
public Single _68_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=72)]
public Single _72_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=76)]
public Single _76_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=80)]
public Single _80_Green_Coefficient;
[Value(Name="Green Coefficient", Offset=84)]
public Single _84_Green_Coefficient;
[Value(Name="Blue Coefficient", Offset=88)]
public Single _88_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=92)]
public Single _92_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=96)]
public Single _96_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=100)]
public Single _100_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=104)]
public Single _104_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=108)]
public Single _108_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=112)]
public Single _112_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=116)]
public Single _116_Blue_Coefficient;
[Value(Name="Blue Coefficient", Offset=120)]
public Single _120_Blue_Coefficient;
[Value(Name="Mean Incoming Light Direction i", Offset=124)]
public Single _124_Mean_Incoming_Light_Direction_i;
[Value(Name="Mean Incoming Light Direction j", Offset=128)]
public Single _128_Mean_Incoming_Light_Direction_j;
[Value(Name="Mean Incoming Light Direction k", Offset=132)]
public Single _132_Mean_Incoming_Light_Direction_k;
[Value(Name="Incoming Light Intensity X", Offset=136)]
public Single _136_Incoming_Light_Intensity_X;
[Value(Name="Incoming Light Intensity Y", Offset=140)]
public Single _140_Incoming_Light_Intensity_Y;
[Value(Name="Incoming Light Intensity Z", Offset=144)]
public Single _144_Incoming_Light_Intensity_Z;
[Value(Name="Specular Bitmap Index", Offset=148)]
public Int32 _148_Specular_Bitmap_Index;
[Value(Name="Rotation Axis i", Offset=152)]
public Single _152_Rotation_Axis_i;
[Value(Name="Rotation Axis j", Offset=156)]
public Single _156_Rotation_Axis_j;
[Value(Name="Rotation Axis k", Offset=160)]
public Single _160_Rotation_Axis_k;
[Value(Name="Rotation Speed", Offset=164)]
public Single _164_Rotation_Speed;
[Value(Name="Bump Direction i", Offset=168)]
public Single _168_Bump_Direction_i;
[Value(Name="Bump Direction j", Offset=172)]
public Single _172_Bump_Direction_j;
[Value(Name="Bump Direction k", Offset=176)]
public Single _176_Bump_Direction_k;
[Value(Name="Color Tint", Offset=180)]
public Single _180_Color_Tint;
[Value(Name="Procedural Overide", Offset=184)]
public H2.DataTypes.Enum _184_Procedural_Overide = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Flags", Offset=188)]
public Bitmask _188_Flags = new Bitmask(new string[] { "Leave Me Alone Please" }, 4);
[Value(Name="Procedural Param0 i", Offset=192)]
public Single _192_Procedural_Param0_i;
[Value(Name="Procedural Param0 j", Offset=196)]
public Single _196_Procedural_Param0_j;
[Value(Name="Procedural Param0 k", Offset=200)]
public Single _200_Procedural_Param0_k;
[Value(Name="Procedural Param1.XYZ i", Offset=204)]
public Single _204_Procedural_Param1_XYZ_i;
[Value(Name="Procedural Param1.XYZ j", Offset=208)]
public Single _208_Procedural_Param1_XYZ_j;
[Value(Name="Procedural Param1.XYZ k", Offset=212)]
public Single _212_Procedural_Param1_XYZ_k;
[Value(Name="Procedural Param1.w", Offset=216)]
public Single _216_Procedural_Param1_w;
}
public _64_geometry_buckets[] _64_Geometry_Buckets;
[Serializable][Reflexive(Name="Geometry Buckets", Offset=64, ChunkSize=56, Label="")]
public class _64_geometry_buckets
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Incident Direction","Color" }, 4);
[Value(Offset=4)]
public Byte[] _4_ = new byte[8];
[Value(Name="Raw Offset", Offset=12)]
public Int32 _12_Raw_Offset;
[Value(Name="Raw Size", Offset=16)]
public Int32 _16_Raw_Size;
[Value(Name="Section Data Size", Offset=20)]
public Int32 _20_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=24)]
public Int32 _24_Resource_Data_Size;
public _28_resources[] _28_Resources;
[Serializable][Reflexive(Name="Resources", Offset=28, ChunkSize=16, Label="")]
public class _28_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Primary Locator", Offset=4)]
public Int16 _4_Primary_Locator;
[Value(Name="Secondary Locator", Offset=6)]
public Int16 _6_Secondary_Locator;
[Value(Name="Resource Data Size", Offset=8)]
public Int32 _8_Resource_Data_Size;
[Value(Name="Resource Data Offset", Offset=12)]
public Int32 _12_Resource_Data_Offset;
}
[Value(Name="Owner Tag Section Offset", Offset=36)]
public Int32 _36_Owner_Tag_Section_Offset;
[Value(Offset=40)]
public Byte[] _40_ = new byte[16];
}
public _72_instance_render_info[] _72_Instance_Render_Info;
[Serializable][Reflexive(Name="Instance Render Info", Offset=72, ChunkSize=4, Label="")]
public class _72_instance_render_info
{
private int __StartOffset__;
[Value(Name="Bitmap Index", Offset=0)]
public Int16 _0_Bitmap_Index;
[Value(Name="Palette Index", Offset=2)]
public Int16 _2_Palette_Index;
}
public _80_instance_bucket_refs[] _80_Instance_Bucket_Refs;
[Serializable][Reflexive(Name="Instance Bucket Refs", Offset=80, ChunkSize=12, Label="")]
public class _80_instance_bucket_refs
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Int16 _0_Flags;
[Value(Name="Bucket Index", Offset=2)]
public Int16 _2_Bucket_Index;
public _4_section_offsets[] _4_Section_Offsets;
[Serializable][Reflexive(Name="Section Offsets", Offset=4, ChunkSize=4, Label="")]
public class _4_section_offsets
{
private int __StartOffset__;
[Value(Name="Section Offset", Offset=0)]
public Int32 _0_Section_Offset;
}
}
public _88_scenary_object_info[] _88_Scenary_Object_Info;
[Serializable][Reflexive(Name="Scenary Object Info", Offset=88, ChunkSize=12, Label="")]
public class _88_scenary_object_info
{
private int __StartOffset__;
[Value(Name="Unique ID", Offset=0)]
public Int32 _0_Unique_ID;
[Value(Name="Origin BSP Index", Offset=4)]
public Int16 _4_Origin_BSP_Index;
[Value(Name="Type", Offset=6)]
public Byte _6_Type;
[Value(Name="Source", Offset=7)]
public Byte _7_Source;
[Value(Name="Render Model Checksum", Offset=8)]
public Int32 _8_Render_Model_Checksum;
}
public _96_scenary_object_bucket_refs[] _96_Scenary_Object_Bucket_Refs;
[Serializable][Reflexive(Name="Scenary Object Bucket Refs", Offset=96, ChunkSize=12, Label="")]
public class _96_scenary_object_bucket_refs
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Int16 _0_Flags;
[Value(Name="Bucket Index", Offset=2)]
public Int16 _2_Bucket_Index;
public _4_section_offsets[] _4_Section_Offsets;
[Serializable][Reflexive(Name="Section Offsets", Offset=4, ChunkSize=4, Label="")]
public class _4_section_offsets
{
private int __StartOffset__;
[Value(Name="Section Offset", Offset=0)]
public Int32 _0_Section_Offset;
}
}
}
[Value(Offset=136)]
public Byte[] _136_ = new byte[124];
        }
    }
}