using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("weat")]
        public class weat
        {
public _0_particle_system[] _0_Particle_System;
[Serializable][Reflexive(Name="Particle System", Offset=0, ChunkSize=140, Label="")]
public class _0_particle_system
{
private int __StartOffset__;
[Value(Name="Sprites", Offset=0)]
public Dependancy _0_Sprites;
[Value(Name="View Box Width", Offset=8)]
public Single _8_View_Box_Width;
[Value(Name="View Box Height", Offset=12)]
public Single _12_View_Box_Height;
[Value(Name="View Box Depth", Offset=16)]
public Single _16_View_Box_Depth;
[Value(Name="Exclusion Radius", Offset=20)]
public Single _20_Exclusion_Radius;
[Value(Name="Max Velocity", Offset=24)]
public Single _24_Max_Velocity;
[Value(Name="Min Mass", Offset=28)]
public Single _28_Min_Mass;
[Value(Name="Max Mass", Offset=32)]
public Single _32_Max_Mass;
[Value(Name="Min Size", Offset=36)]
public Single _36_Min_Size;
[Value(Name="Max Size", Offset=40)]
public Single _40_Max_Size;
[Value(Name="Maximum Number Of Particles", Offset=44)]
public Int32 _44_Maximum_Number_Of_Particles;
[Value(Name="Initial Velocity i", Offset=48)]
public Single _48_Initial_Velocity_i;
[Value(Name="Initial Velocity j", Offset=52)]
public Single _52_Initial_Velocity_j;
[Value(Name="Initial Velocity k", Offset=56)]
public Single _56_Initial_Velocity_k;
[Value(Offset=60)]
public Byte[] _60_ = new byte[4];
[Value(Name="Block Offset", Offset=64)]
public UInt32 _64_Block_Offset;
[Value(Name="Block Size", Offset=68)]
public UInt32 _68_Block_Size;
[Value(Name="Section Data Size", Offset=72)]
public UInt32 _72_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=76)]
public UInt32 _76_Resource_Data_Size;
public _80_resources[] _80_Resources;
[Serializable][Reflexive(Name="Resources", Offset=80, ChunkSize=16, Label="")]
public class _80_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Primary Locator", Offset=4)]
public Int16 _4_Primary_Locator;
[Value(Name="Secondary Locator", Offset=6)]
public Int16 _6_Secondary_Locator;
[Value(Name="Resource Data Size", Offset=8)]
public UInt32 _8_Resource_Data_Size;
[Value(Name="Resource Data Offset", Offset=12)]
public UInt32 _12_Resource_Data_Offset;
}
[Value(Name="Owner ID", Offset=88)]
public Int32 _88_Owner_ID;
[Value(Name="Owner Tag Section Offset", Offset=92)]
public UInt32 _92_Owner_Tag_Section_Offset;
[Value(Offset=96)]
public Byte[] _96_ = new byte[12];
[Value(Name="Type", Offset=108)]
public H2.DataTypes.Enum _108_Type = new H2.DataTypes.Enum(new string[] {  "Generic", "Snow", "Rain", "Rain Splash", "Bugs", "Sand Storm", "Debris", "Bubbles" }, 4);
[Value(Name="Minimum Opacity", Offset=112)]
public Single _112_Minimum_Opacity;
[Value(Name="Maximum Opacity", Offset=116)]
public Single _116_Maximum_Opacity;
[Value(Name="Rain Streak Scale", Offset=120)]
public Single _120_Rain_Streak_Scale;
[Value(Name="Rain Line Width", Offset=124)]
public Single _124_Rain_Line_Width;
[Value(Offset=128)]
public Byte[] _128_ = new byte[12];
}
public _8_background_plates[] _8_Background_Plates;
[Serializable][Reflexive(Name="Background Plates", Offset=8, ChunkSize=936, Label="")]
public class _8_background_plates
{
private int __StartOffset__;
[Value(Name="Texture 0", Offset=0)]
public Dependancy _0_Texture_0;
[Value(Name="Texture 1", Offset=8)]
public Dependancy _8_Texture_1;
[Value(Name="Texture 2", Offset=16)]
public Dependancy _16_Texture_2;
[Value(Name="Plate Position 0", Offset=24)]
public Single _24_Plate_Position_0;
[Value(Name="Plate Position 1", Offset=28)]
public Single _28_Plate_Position_1;
[Value(Name="Plate Position 2", Offset=32)]
public Single _32_Plate_Position_2;
[Value(Name="Move Speed 0 i", Offset=36)]
public Single _36_Move_Speed_0_i;
[Value(Name="Move Speed 0 j", Offset=40)]
public Single _40_Move_Speed_0_j;
[Value(Name="Move Speed 0 k", Offset=44)]
public Single _44_Move_Speed_0_k;
[Value(Name="Move Speed 1 i", Offset=48)]
public Single _48_Move_Speed_1_i;
[Value(Name="Move Speed 1 j", Offset=52)]
public Single _52_Move_Speed_1_j;
[Value(Name="Move Speed 1 k", Offset=56)]
public Single _56_Move_Speed_1_k;
[Value(Name="Move Speed 2 i", Offset=60)]
public Single _60_Move_Speed_2_i;
[Value(Name="Move Speed 2 j", Offset=64)]
public Single _64_Move_Speed_2_j;
[Value(Name="Move Speed 2 k", Offset=68)]
public Single _68_Move_Speed_2_k;
[Value(Name="Texture Scale 0", Offset=72)]
public Single _72_Texture_Scale_0;
[Value(Name="Texture Scale 1", Offset=76)]
public Single _76_Texture_Scale_1;
[Value(Name="Texture Scale 2", Offset=80)]
public Single _80_Texture_Scale_2;
[Value(Name="Jitter 0 i", Offset=84)]
public Single _84_Jitter_0_i;
[Value(Name="Jitter 0 j", Offset=88)]
public Single _88_Jitter_0_j;
[Value(Name="Jitter 0 k", Offset=92)]
public Single _92_Jitter_0_k;
[Value(Name="Jitter 1 i", Offset=96)]
public Single _96_Jitter_1_i;
[Value(Name="Jitter 1 j", Offset=100)]
public Single _100_Jitter_1_j;
[Value(Name="Jitter 1 k", Offset=104)]
public Single _104_Jitter_1_k;
[Value(Name="Jitter 2 i", Offset=108)]
public Single _108_Jitter_2_i;
[Value(Name="Jitter 2 j", Offset=112)]
public Single _112_Jitter_2_j;
[Value(Name="Jitter 2 k", Offset=116)]
public Single _116_Jitter_2_k;
[Value(Name="Plate Z Near", Offset=120)]
public Single _120_Plate_Z_Near;
[Value(Name="Plate Z Far", Offset=124)]
public Single _124_Plate_Z_Far;
[Value(Name="Depth Blend Z Near", Offset=128)]
public Single _128_Depth_Blend_Z_Near;
[Value(Name="Depth Blend Z Far", Offset=132)]
public Single _132_Depth_Blend_Z_Far;
[Value(Name="Opacity 0", Offset=136)]
public Single _136_Opacity_0;
[Value(Name="Opacity 1", Offset=140)]
public Single _140_Opacity_1;
[Value(Name="Opacity 2", Offset=144)]
public Single _144_Opacity_2;
[Value(Name="Flags", Offset=148)]
public Bitmask _148_Flags = new Bitmask(new string[] { "Forward","Auto Position Planes","Auto Scale Planesauto Update Spe" }, 4);
[Value(Name="Tint Color 0 R", Offset=152)]
public Single _152_Tint_Color_0_R;
[Value(Name="Tint Color 0 G", Offset=156)]
public Single _156_Tint_Color_0_G;
[Value(Name="Tint Color 0 B", Offset=160)]
public Single _160_Tint_Color_0_B;
[Value(Name="Tint Color 1 R", Offset=164)]
public Single _164_Tint_Color_1_R;
[Value(Name="Tint Color 1 G", Offset=168)]
public Single _168_Tint_Color_1_G;
[Value(Name="Tint Color 1 B", Offset=172)]
public Single _172_Tint_Color_1_B;
[Value(Name="Tint Color 2 R", Offset=176)]
public Single _176_Tint_Color_2_R;
[Value(Name="Tint Color 2 G", Offset=180)]
public Single _180_Tint_Color_2_G;
[Value(Name="Tint Color 2 B", Offset=184)]
public Single _184_Tint_Color_2_B;
[Value(Name="Mass 1", Offset=188)]
public Single _188_Mass_1;
[Value(Name="Mass 2", Offset=192)]
public Single _192_Mass_2;
[Value(Name="Mass 3", Offset=196)]
public Single _196_Mass_3;
[Value(Offset=200)]
public Byte[] _200_ = new byte[736];
}
[Value(Name="Wind Tiling Scale", Offset=16)]
public Single _16_Wind_Tiling_Scale;
[Value(Offset=20)]
public Byte[] _20_ = new byte[36];
[Value(Name="Wind Primary Heading i", Offset=56)]
public Single _56_Wind_Primary_Heading_i;
[Value(Name="Wind Primary Heading j", Offset=60)]
public Single _60_Wind_Primary_Heading_j;
[Value(Name="Wind Primary Heading k", Offset=64)]
public Single _64_Wind_Primary_Heading_k;
[Value(Offset=68)]
public Byte[] _68_ = new byte[8];
[Value(Name="Primary Rate Of Change", Offset=76)]
public Single _76_Primary_Rate_Of_Change;
[Value(Name="Primary Min Strength", Offset=80)]
public Single _80_Primary_Min_Strength;
[Value(Offset=84)]
public Byte[] _84_ = new byte[4];
[Value(Name="Wind Gusting Heading i", Offset=88)]
public Single _88_Wind_Gusting_Heading_i;
[Value(Name="Wind Gusting Heading j", Offset=92)]
public Single _92_Wind_Gusting_Heading_j;
[Value(Name="Wind Gusting Heading k", Offset=96)]
public Single _96_Wind_Gusting_Heading_k;
[Value(Offset=100)]
public Byte[] _100_ = new byte[12];
[Value(Name="Gust Directional Rate Of Change", Offset=112)]
public Single _112_Gust_Directional_Rate_Of_Change;
[Value(Name="Gust Strength Rate Of Change", Offset=116)]
public Single _116_Gust_Strength_Rate_Of_Change;
[Value(Name="Gust Cone Angle", Offset=120)]
public Single _120_Gust_Cone_Angle;
[Value(Offset=124)]
public Byte[] _124_ = new byte[16];
[Value(Name="Turbulance Rate Of Change", Offset=140)]
public Single _140_Turbulance_Rate_Of_Change;
[Value(Name="Turbulence Scale x,y,z i", Offset=144)]
public Single _144_Turbulence_Scale_x_y_z_i;
[Value(Name="Turbulence Scale x,y,z j", Offset=148)]
public Single _148_Turbulence_Scale_x_y_z_j;
[Value(Name="Turbulence Scale x,y,z k", Offset=152)]
public Single _152_Turbulence_Scale_x_y_z_k;
[Value(Name="Gravity Constant", Offset=156)]
public Single _156_Gravity_Constant;
[Value(Offset=160)]
public Byte[] _160_ = new byte[12];
[Value(Name="Fade Radius", Offset=172)]
public Single _172_Fade_Radius;
        }
    }
}