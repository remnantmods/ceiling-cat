using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("effe")]
        public class effe
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Deleted When Attachment Deactivated" }, 4);
[Value(Name="Loop Start Event #", Offset=4)]
public Int16 _4_Loop_Start_Event_No;
[Value(Offset=6)]
public Byte[] _6_ = new byte[6];
public _12_locations[] _12_Locations;
[Serializable][Reflexive(Name="Locations", Offset=12, ChunkSize=4, Label="")]
public class _12_locations
{
private int __StartOffset__;
[Value(Name="Marker Name", Offset=0)]
public StringID _0_Marker_Name;
}
public _20_events[] _20_Events;
[Serializable][Reflexive(Name="Events", Offset=20, ChunkSize=56, Label="")]
public class _20_events
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Disabled For Debugging" }, 4);
[Value(Name="Skip Fraction", Offset=4)]
public Single _4_Skip_Fraction;
[Value(Name="Delay Bounds", Offset=8)]
public Single _8_Delay_Bounds;
[Value(Name="...To", Offset=12)]
public Single _12____To;
[Value(Name="Duration Bounds", Offset=16)]
public Single _16_Duration_Bounds;
[Value(Name="...To", Offset=20)]
public Single _20____To;
public _24_parts[] _24_Parts;
[Serializable][Reflexive(Name="Parts", Offset=24, ChunkSize=56, Label="")]
public class _24_parts
{
private int __StartOffset__;
[Value(Name="Create In", Offset=0)]
public H2.DataTypes.Enum _0_Create_In = new H2.DataTypes.Enum(new string[] {  "Any Environment", "Air Only", "Water Only", "Space Only" }, 2);
[Value(Name="Create In", Offset=2)]
public H2.DataTypes.Enum _2_Create_In = new H2.DataTypes.Enum(new string[] {  "Either Mode", "Violent Mode Only", "NonViolent Mode Only" }, 2);
[Value(Name="Location", Offset=4)]
public Int16 _4_Location;
[Value(Name="Flags", Offset=6)]
public Bitmask _6_Flags = new Bitmask(new string[] { "Face Down Regardless Of Location","Offset Origin Away From Geometry","Never Attached To Object","Disabled For Debugging","Draw Regardless Of Distance" }, 2);
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Type", Offset=12)]
public Dependancy _12_Type;
[Value(Name="Velocity Bound", Offset=20)]
public Single _20_Velocity_Bound;
[Value(Name="...To", Offset=24)]
public Single _24____To;
[Value(Name="Velocity Cone Angle (radians)", Offset=28)]
public Single _28_Velocity_Cone_Angle__radians_;
[Value(Name="Angular Velocity Bound", Offset=32)]
public Single _32_Angular_Velocity_Bound;
[Value(Name="...To", Offset=36)]
public Single _36____To;
[Value(Name="Radius Modifier", Offset=40)]
public Single _40_Radius_Modifier;
[Value(Name="...To", Offset=44)]
public Single _44____To;
[Value(Name="A Scales Values", Offset=48)]
public Bitmask _48_A_Scales_Values = new Bitmask(new string[] { "Velocity","Velocity Delta","Velocity Cone Angle","Angular Velocity","Angular Velocity Delta","Type-Specific Scale" }, 4);
[Value(Name="B Scales Values", Offset=52)]
public Bitmask _52_B_Scales_Values = new Bitmask(new string[] { "Velocity","Velocity Delta","Velocity Cone Angle","Angular Velocity","Angular Velocity Delta","Type-Specific Scale" }, 4);
}
public _32_beams[] _32_Beams;
[Serializable][Reflexive(Name="Beams", Offset=32, ChunkSize=60, Label="")]
public class _32_beams
{
private int __StartOffset__;
[Value(Name="Shader", Offset=0)]
public Dependancy _0_Shader;
[Value(Name="Location", Offset=8)]
public Int16 _8_Location;
[Value(Offset=10)]
public Byte[] _10_ = new byte[2];
public _12_color[] _12_Color;
[Serializable][Reflexive(Name="Color", Offset=12, ChunkSize=1, Label="")]
public class _12_color
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _20_alpha[] _20_Alpha;
[Serializable][Reflexive(Name="Alpha", Offset=20, ChunkSize=1, Label="")]
public class _20_alpha
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _28_width[] _28_Width;
[Serializable][Reflexive(Name="Width", Offset=28, ChunkSize=1, Label="")]
public class _28_width
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _36_length[] _36_Length;
[Serializable][Reflexive(Name="Length", Offset=36, ChunkSize=1, Label="")]
public class _36_length
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _44_yaw[] _44_Yaw;
[Serializable][Reflexive(Name="Yaw", Offset=44, ChunkSize=1, Label="")]
public class _44_yaw
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _52_pitch[] _52_Pitch;
[Serializable][Reflexive(Name="Pitch", Offset=52, ChunkSize=1, Label="")]
public class _52_pitch
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
public _40_accelerations[] _40_Accelerations;
[Serializable][Reflexive(Name="Accelerations", Offset=40, ChunkSize=20, Label="")]
public class _40_accelerations
{
private int __StartOffset__;
[Value(Name="Create In", Offset=0)]
public H2.DataTypes.Enum _0_Create_In = new H2.DataTypes.Enum(new string[] {  "Any Environment", "Air Only", "Water Only", "Space Only" }, 2);
[Value(Name="Create In", Offset=2)]
public H2.DataTypes.Enum _2_Create_In = new H2.DataTypes.Enum(new string[] {  "Either Mode", "Violent Mode Only", "NonViolent Mode Only" }, 2);
[Value(Name="Location", Offset=4)]
public Int16 _4_Location;
[Value(Offset=6)]
public Byte[] _6_ = new byte[2];
[Value(Name="Acceleration", Offset=8)]
public Single _8_Acceleration;
[Value(Name="Inner Cone Angle", Offset=12)]
public Single _12_Inner_Cone_Angle;
[Value(Name="Outer Cone Angle", Offset=16)]
public Single _16_Outer_Cone_Angle;
}
public _48_particle_systems[] _48_Particle_Systems;
[Serializable][Reflexive(Name="Particle Systems", Offset=48, ChunkSize=56, Label="")]
public class _48_particle_systems
{
private int __StartOffset__;
[Value(Name="Particle", Offset=0)]
public Dependancy _0_Particle;
[Value(Name="Location", Offset=8)]
public Int16 _8_Location;
[Value(Offset=10)]
public Byte[] _10_ = new byte[2];
[Value(Name="Coordinate System", Offset=12)]
public H2.DataTypes.Enum _12_Coordinate_System = new H2.DataTypes.Enum(new string[] {  "World", "Local", "Parent" }, 2);
[Value(Name="Environment", Offset=14)]
public H2.DataTypes.Enum _14_Environment = new H2.DataTypes.Enum(new string[] {  "Any Environment", "Air Only", "Water Only", "Space Only" }, 2);
[Value(Name="Disposition", Offset=16)]
public H2.DataTypes.Enum _16_Disposition = new H2.DataTypes.Enum(new string[] {  "Either Mode", "Violent Mode Only", "NonViolent Mode Only" }, 2);
[Value(Name="Camera Mode", Offset=18)]
public H2.DataTypes.Enum _18_Camera_Mode = new H2.DataTypes.Enum(new string[] {  "Independant Of Camera Mode", "Only In 1st Person", "Only In 3rd Person", "Both 1st And 3rd" }, 2);
[Value(Name="Sort Bias", Offset=20)]
public Int16 _20_Sort_Bias;
[Value(Name="Flags", Offset=22)]
public Bitmask _22_Flags = new Bitmask(new string[] { "Glow","Cinematics","Looping Particle","Disabled For Debugging","Inherit Effect Velocity","Don't Render System","Render When Zoomed","Spread Between Ticks","Persistent Particle","Expensive Visibility" }, 4);
[Value(Offset=26)]
public Byte[] _26_ = new byte[2];
[Value(Name="LOD In Distance", Offset=28)]
public Single _28_LOD_In_Distance;
[Value(Name="LOD Feather In Delta", Offset=32)]
public Single _32_LOD_Feather_In_Delta;
[Value(Name="LOD Out Distance", Offset=36)]
public Single _36_LOD_Out_Distance;
[Value(Name="LOD Feather Out Delta", Offset=40)]
public Single _40_LOD_Feather_Out_Delta;
[Value(Offset=44)]
public Byte[] _44_ = new byte[4];
public _48_emitters[] _48_Emitters;
[Serializable][Reflexive(Name="Emitters", Offset=48, ChunkSize=184, Label="")]
public class _48_emitters
{
private int __StartOffset__;
[Value(Name="Particle Physics", Offset=0)]
public Dependancy _0_Particle_Physics;
[Value(Name="Output Modifier", Offset=8)]
public H2.DataTypes.Enum _8_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=12)]
public H2.DataTypes.Enum _12_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _16_particle_emission_rate[] _16_Particle_Emission_Rate;
[Serializable][Reflexive(Name="Particle Emission Rate", Offset=16, ChunkSize=1, Label="")]
public class _16_particle_emission_rate
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=24)]
public H2.DataTypes.Enum _24_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=28)]
public H2.DataTypes.Enum _28_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _32_particle_lifespan__sec_[] _32_Particle_Lifespan__sec_;
[Serializable][Reflexive(Name="Particle Lifespan (sec)", Offset=32, ChunkSize=1, Label="")]
public class _32_particle_lifespan__sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=40)]
public H2.DataTypes.Enum _40_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=44)]
public H2.DataTypes.Enum _44_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _48_particle_velocity__world_units_per_sec_[] _48_Particle_Velocity__World_Units_Per_Sec_;
[Serializable][Reflexive(Name="Particle Velocity (World Units Per Sec)", Offset=48, ChunkSize=1, Label="")]
public class _48_particle_velocity__world_units_per_sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=56)]
public H2.DataTypes.Enum _56_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=60)]
public H2.DataTypes.Enum _60_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _64_particle_angular_velocity__degrees_per_sec_[] _64_Particle_Angular_Velocity__Degrees_Per_Sec_;
[Serializable][Reflexive(Name="Particle Angular Velocity (Degrees Per Sec)", Offset=64, ChunkSize=1, Label="")]
public class _64_particle_angular_velocity__degrees_per_sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=72)]
public H2.DataTypes.Enum _72_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=76)]
public H2.DataTypes.Enum _76_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _80_particle_size__world_units_[] _80_Particle_Size__World_Units_;
[Serializable][Reflexive(Name="Particle Size (World Units)", Offset=80, ChunkSize=1, Label="")]
public class _80_particle_size__world_units_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=88)]
public H2.DataTypes.Enum _88_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=92)]
public H2.DataTypes.Enum _92_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _96_particle_tint[] _96_Particle_Tint;
[Serializable][Reflexive(Name="Particle Tint", Offset=96, ChunkSize=1, Label="")]
public class _96_particle_tint
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=104)]
public H2.DataTypes.Enum _104_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=108)]
public H2.DataTypes.Enum _108_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _112_particle_alpha[] _112_Particle_Alpha;
[Serializable][Reflexive(Name="Particle Alpha", Offset=112, ChunkSize=1, Label="")]
public class _112_particle_alpha
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Emission Shape", Offset=120)]
public H2.DataTypes.Enum _120_Emission_Shape = new H2.DataTypes.Enum(new string[] {  "Sprayer", "Disc", "Globe", "Implode", "Tube", "Halo", "Impact Contour", "Impact Area", "Debris", "Line" }, 4);
[Value(Name="Output Modifier", Offset=124)]
public H2.DataTypes.Enum _124_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=128)]
public H2.DataTypes.Enum _128_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _132_emission_radius__world_units_[] _132_Emission_Radius__World_Units_;
[Serializable][Reflexive(Name="Emission Radius (World Units)", Offset=132, ChunkSize=1, Label="")]
public class _132_emission_radius__world_units_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=140)]
public H2.DataTypes.Enum _140_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=144)]
public H2.DataTypes.Enum _144_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _148_emission_angle__degrees_[] _148_Emission_Angle__Degrees_;
[Serializable][Reflexive(Name="Emission Angle (Degrees)", Offset=148, ChunkSize=1, Label="")]
public class _148_emission_angle__degrees_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Translation Offset X", Offset=156)]
public Single _156_Translation_Offset_X;
[Value(Name="Translation Offset Y", Offset=160)]
public Single _160_Translation_Offset_Y;
[Value(Name="Translation Offset Z", Offset=164)]
public Single _164_Translation_Offset_Z;
[Value(Name="Relative Direction Yaw", Offset=168)]
public Single _168_Relative_Direction_Yaw;
[Value(Name="Relative Direction Pitch", Offset=172)]
public Single _172_Relative_Direction_Pitch;
[Value(Offset=176)]
public Byte[] _176_ = new byte[8];
}
}
}
[Value(Name="Looping Sound", Offset=28)]
public Dependancy _28_Looping_Sound;
[Value(Name="Location", Offset=32)]
public Int16 _32_Location;
[Value(Offset=34)]
public Byte[] _34_ = new byte[2];
[Value(Name="Always Play Distance", Offset=36)]
public Single _36_Always_Play_Distance;
[Value(Name="Never Play Distance", Offset=40)]
public Single _40_Never_Play_Distance;
        }
    }
}