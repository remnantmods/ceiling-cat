using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("colo")]
        public class colo
        {
public _0_colors[] _0_Colors;
[Serializable][Reflexive(Name="Colors", Offset=0, ChunkSize=48, Label="")]
public class _0_colors
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Alpha", Offset=32)]
public Single _32_Alpha;
[Value(Name="Red", Offset=36)]
public Single _36_Red;
[Value(Name="Green", Offset=40)]
public Single _40_Green;
[Value(Name="Blue", Offset=44)]
public Single _44_Blue;
}
        }
    }
}