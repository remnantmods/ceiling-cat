using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("fog")]
        public class fog
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Render Only Submerged Geometry","Extend Infinitely While Visible","Don't Flood Fill","Aggressive Flood Fill","Do Not Render","Do Not Render Unless Submerged" }, 2);
[Value(Name="Priority", Offset=2)]
public Int16 _2_Priority;
[Value(Name="Global Material Name", Offset=4)]
public StringID _4_Global_Material_Name;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Maximum Density", Offset=12)]
public Single _12_Maximum_Density;
[Value(Name="Opaque Distance", Offset=16)]
public Single _16_Opaque_Distance;
[Value(Name="Opaque Depth", Offset=20)]
public Single _20_Opaque_Depth;
[Value(Name="Atmospheric Planar Depth Lower", Offset=24)]
public Single _24_Atmospheric_Planar_Depth_Lower;
[Value(Name="Atmospheric Planar Depth Upper", Offset=28)]
public Single _28_Atmospheric_Planar_Depth_Upper;
[Value(Name="Eye Offset Scale", Offset=32)]
public Single _32_Eye_Offset_Scale;
[Value(Name="Color R", Offset=36)]
public Single _36_Color_R;
[Value(Name="Color G", Offset=40)]
public Single _40_Color_G;
[Value(Name="Color B", Offset=44)]
public Single _44_Color_B;
public _48_patchy_fog[] _48_Patchy_Fog;
[Serializable][Reflexive(Name="Patchy Fog", Offset=48, ChunkSize=52, Label="")]
public class _48_patchy_fog
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Offset=12)]
public Byte[] _12_ = new byte[12];
[Value(Name="Density Lower", Offset=24)]
public Single _24_Density_Lower;
[Value(Name="Density Upper", Offset=28)]
public Single _28_Density_Upper;
[Value(Name="Distance Lower", Offset=32)]
public Single _32_Distance_Lower;
[Value(Name="Distance Upper", Offset=36)]
public Single _36_Distance_Upper;
[Value(Name="Min Depth Fraction", Offset=40)]
public Single _40_Min_Depth_Fraction;
[Value(Name="Patchy Fog", Offset=44)]
public Dependancy _44_Patchy_Fog;
}
[Value(Name="Background Sound", Offset=56)]
public Dependancy _56_Background_Sound;
[Value(Name="Sound Enviroment", Offset=60)]
public Dependancy _60_Sound_Enviroment;
[Value(Name="Environment Damping Factor", Offset=64)]
public Single _64_Environment_Damping_Factor;
[Value(Name="Background Sound Gain", Offset=68)]
public Single _68_Background_Sound_Gain;
[Value(Name="Enter Sound", Offset=72)]
public Dependancy _72_Enter_Sound;
[Value(Name="Exit Sound", Offset=76)]
public Dependancy _76_Exit_Sound;
        }
    }
}