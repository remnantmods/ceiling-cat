using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("pphy")]
        public class pphy
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "unused","Collides With Structures","Collides With Water Surface","Uses Simple Wind","Uses Dampened Wind","No Gravity" }, 4);
[Value(Name="Density", Offset=4)]
public Single _4_Density;
[Value(Name="Air Friction", Offset=8)]
public Single _8_Air_Friction;
[Value(Name="Water Friction", Offset=12)]
public Single _12_Water_Friction;
[Value(Offset=16)]
public Byte[] _16_ = new byte[16];
[Value(Name="Surface Friction", Offset=32)]
public Single _32_Surface_Friction;
[Value(Name="Elasticity", Offset=36)]
public Single _36_Elasticity;
[Value(Offset=40)]
public Byte[] _40_ = new byte[24];
        }
    }
}