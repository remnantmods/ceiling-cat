using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("hlmt")]
        public class hlmt
        {
[Value(Name="Render Model", Offset=0)]
public Dependancy _0_Render_Model;
[Value(Name="Collision Model", Offset=4)]
public Dependancy _4_Collision_Model;
[Value(Name="Animation", Offset=8)]
public Dependancy _8_Animation;
[Value(Name="Physics", Offset=12)]
public Dependancy _12_Physics;
[Value(Name="Physics Model", Offset=16)]
public Dependancy _16_Physics_Model;
[Value(Name="Disappear Distance", Offset=20)]
public Single _20_Disappear_Distance;
[Value(Name="Begin Fade Distance", Offset=24)]
public Single _24_Begin_Fade_Distance;
[Value(Offset=28)]
public Byte[] _28_ = new byte[4];
[Value(Name="Reduce To L1", Offset=32)]
public Single _32_Reduce_To_L1;
[Value(Name="Reduce To L2", Offset=36)]
public Single _36_Reduce_To_L2;
[Value(Name="Reduce To L3", Offset=40)]
public Single _40_Reduce_To_L3;
[Value(Name="Reduce To L4", Offset=44)]
public Single _44_Reduce_To_L4;
[Value(Name="Reduce To L5", Offset=48)]
public Single _48_Reduce_To_L5;
[Value(Offset=52)]
public Byte[] _52_ = new byte[4];
[Value(Name="Spawn Type", Offset=56)]
public H2.DataTypes.Enum _56_Spawn_Type = new H2.DataTypes.Enum(new string[] {  "Fade At Super High Detail Level", "Fade At High Detail Level", "Fade At Medium Detail Level", "Fade At Low Detail Level", "Fade At Super Low Detail Level", "Fade Never" }, 2);
[Value(Offset=58)]
public Byte[] _58_ = new byte[2];
public _60_variants[] _60_Variants;
[Serializable][Reflexive(Name="Variants", Offset=60, ChunkSize=56, Label="")]
public class _60_variants
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Offset=4)]
public Byte[] _4_ = new byte[16];
public _20_regions[] _20_Regions;
[Serializable][Reflexive(Name="Regions", Offset=20, ChunkSize=20, Label="")]
public class _20_regions
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Parent Variant #", Offset=4)]
public Int16 _4_Parent_Variant_No;
[Value(Offset=6)]
public Byte[] _6_ = new byte[2];
public _8_permutation[] _8_Permutation;
[Serializable][Reflexive(Name="Permutation", Offset=8, ChunkSize=32, Label="")]
public class _8_permutation
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Copy States To All Permutations" }, 4);
[Value(Name="Probability", Offset=8)]
public Single _8_Probability;
public _12_states[] _12_States;
[Serializable][Reflexive(Name="States", Offset=12, ChunkSize=24, Label="")]
public class _12_states
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Property Flags", Offset=4)]
public Bitmask _4_Property_Flags = new Bitmask(new string[] { "Blurred","Hella Blurred","Shielded" }, 2);
[Value(Name="State", Offset=6)]
public H2.DataTypes.Enum _6_State = new H2.DataTypes.Enum(new string[] {  "Default", "Minor Damage", "Medium Damage", "Major Damage", "Destroyed" }, 2);
[Value(Name="Looping Effect", Offset=8)]
public Dependancy _8_Looping_Effect;
[Value(Name="Looping Effect Marker Name", Offset=16)]
public StringID _16_Looping_Effect_Marker_Name;
[Value(Name="Initial Probability", Offset=20)]
public Single _20_Initial_Probability;
}
[Value(Offset=20)]
public Byte[] _20_ = new byte[12];
}
[Value(Name="Sort Order", Offset=16)]
public H2.DataTypes.Enum _16_Sort_Order = new H2.DataTypes.Enum(new string[] {  "No Sorting", "-5 (Closest)", "-4", "-3", "-2", "-1", "0 (Same As Model)", "1", "2", "3", "4", "5 (Farthest)" }, 4);
}
public _28_objects[] _28_Objects;
[Serializable][Reflexive(Name="Objects", Offset=28, ChunkSize=16, Label="")]
public class _28_objects
{
private int __StartOffset__;
[Value(Name="Parent Marker", Offset=0)]
public StringID _0_Parent_Marker;
[Value(Name="Child Marker", Offset=4)]
public StringID _4_Child_Marker;
[Value(Name="Child Object", Offset=8)]
public Dependancy _8_Child_Object;
}
[Value(Offset=36)]
public Byte[] _36_ = new byte[8];
[Value(Name="Dialogue Sound Effect", Offset=44)]
public StringID _44_Dialogue_Sound_Effect;
[Value(Name="Dialogue", Offset=48)]
public Dependancy _48_Dialogue;
}
public _68_materials[] _68_Materials;
[Serializable][Reflexive(Name="Materials", Offset=68, ChunkSize=20, Label="")]
public class _68_materials
{
private int __StartOffset__;
[Value(Name="Material Name", Offset=0)]
public StringID _0_Material_Name;
[Value(Name="Material Type", Offset=4)]
public H2.DataTypes.Enum _4_Material_Type = new H2.DataTypes.Enum(new string[] {  "Dirt", "Sand", "Stone", "Snow", "Wood", "Metal (hollow)", "Metal (thin)", "Metal (thick)", "Rubber", "Glass", "Force Field", "Grunt", "Hunter Armor", "Hunter Skin", "Elite", "Jackal", "Jackal Energy Shield", "Engineer Skin", "Engineer Force Field", "Flood Combat Form", "Flood Carrier Form", "Cyborg Armor", "Cyborg Energy Shield", "Human Armor", "Human Skin", "Sentinel", "Monitor", "Plastic", "Water", "Leaves", "Elite Energy Shield", "Ice", "Hunter Shield" }, 4);
[Value(Name="Damage Section", Offset=8)]
public H2.DataTypes.Enum _8_Damage_Section = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Global Material Name", Offset=12)]
public StringID _12_Global_Material_Name;
[Value(Offset=16)]
public Byte[] _16_ = new byte[4];
}
public _76_new_damage_info[] _76_New_Damage_Info;
[Serializable][Reflexive(Name="New Damage Info", Offset=76, ChunkSize=248, Label="")]
public class _76_new_damage_info
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Takes Sheild Damage For Children","Takes Body Damage For Children","Always Shields Friendly Damage","Passes Area Damage To Children","Parent Never Takes Body Damage For Children","Only Damaged By Explosives","Parent Never Takes Shield Damage For Children","Cannot Die From Damage","Passes Attached Damage To Riders" }, 4);
[Value(Name="Global Indirect Material Name", Offset=4)]
public StringID _4_Global_Indirect_Material_Name;
[Value(Name="Indirect Damage Section", Offset=8)]
public Int32 _8_Indirect_Damage_Section;
[Value(Offset=12)]
public Byte[] _12_ = new byte[4];
[Value(Name="Collision Damage Reporting Type", Offset=16)]
public H2.DataTypes.Enum _16_Collision_Damage_Reporting_Type = new H2.DataTypes.Enum(new string[] {  "Teh Guardians!!1!!1!", "Falling Damage", "Generic Collision Damage", "Generic Melee Edamage", "Generic Explosion", "Magnum Pistol", "Plasma Pistol", "Needler", "Smg", "Plasma Rifle", "Battle Rifle", "carbine", "Shotgun", "Sniper Rifle", "Beam Rifle", "Rocket Launcher", "Flak Cannon", "Brute shot", "Disintegrator", "Brute Plasma Rifle", "Energy sword", "Frag Grenade", "Plasma Grenade", "Flasg Melee Damage", "Bomb Melee Damage", "Bomb Explosion Damage", "Ball Melee Damage", "Human Turret", "Plasma Turret", "Banshee", "Ghost", "Mongoose", "Scorpion", "Spectre Driver", "Spectre Gunner", "Warthog Driver", "Warthog Gunner", "Wraith", "Tank", "Sentinel beam", "Sentinel Rpg", "Teleporter" }, 2);
[Value(Name="Response Damage Reporting Type", Offset=18)]
public H2.DataTypes.Enum _18_Response_Damage_Reporting_Type = new H2.DataTypes.Enum(new string[] {  "Teh Guardians!!1!!1!", "Falling Damage", "Generic Collision Damage", "Generic Melee Edamage", "Generic Explosion", "Magnum Pistol", "Plasma Pistol", "Needler", "Smg", "Plasma Rifle", "Battle Rifle", "carbine", "Shotgun", "Sniper Rifle", "Beam Rifle", "Rocket Launcher", "Flak Cannon", "Brute shot", "Disintegrator", "Brute Plasma Rifle", "Energy sword", "Frag Grenade", "Plasma Grenade", "Flasg Melee Damage", "Bomb Melee Damage", "Bomb Explosion Damage", "Ball Melee Damage", "Human Turret", "Plasma Turret", "Banshee", "Ghost", "Mongoose", "Scorpion", "Spectre Driver", "Spectre Gunner", "Warthog Driver", "Warthog Gunner", "Wraith", "Tank", "Sentinel beam", "Sentinel Rpg", "Teleporter" }, 2);
[Value(Offset=20)]
public Byte[] _20_ = new byte[20];
[Value(Name="Max Vitality", Offset=40)]
public Single _40_Max_Vitality;
[Value(Name="Min Stun Damage", Offset=44)]
public Single _44_Min_Stun_Damage;
[Value(Name="Stun Time", Offset=48)]
public Single _48_Stun_Time;
[Value(Name="Recharge Time", Offset=52)]
public Single _52_Recharge_Time;
[Value(Name="Recharge Fraction", Offset=56)]
public Single _56_Recharge_Fraction;
[Value(Offset=60)]
public Byte[] _60_ = new byte[64];
[Value(Name="Shield Damaged FP Shader", Offset=124)]
public Dependancy _124_Shield_Damaged_FP_Shader;
[Value(Name="Shield Damaged Shader", Offset=132)]
public Dependancy _132_Shield_Damaged_Shader;
[Value(Name="Max Shield Vitality", Offset=140)]
public Single _140_Max_Shield_Vitality;
[Value(Name="Global Sheild Material Name", Offset=144)]
public StringID _144_Global_Sheild_Material_Name;
[Value(Name="Min Stun Damage", Offset=148)]
public Single _148_Min_Stun_Damage;
[Value(Name="Stun Time", Offset=152)]
public Single _152_Stun_Time;
[Value(Name="Shield Recharge Time", Offset=156)]
public Single _156_Shield_Recharge_Time;
[Value(Name="Sheild Damaged Threshold", Offset=160)]
public Single _160_Sheild_Damaged_Threshold;
[Value(Name="Sheild Damaged Effect", Offset=164)]
public Dependancy _164_Sheild_Damaged_Effect;
[Value(Name="Sheild Depleted Effect", Offset=172)]
public Dependancy _172_Sheild_Depleted_Effect;
[Value(Name="Sheild Recharging Effect", Offset=180)]
public Dependancy _180_Sheild_Recharging_Effect;
public _188_damage_secctions[] _188_Damage_Secctions;
[Serializable][Reflexive(Name="Damage Secctions", Offset=188, ChunkSize=56, Label="")]
public class _188_damage_secctions
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Absorbs Body Damage","Takes Full Damage When Object Dies","Cannot Die With Riders","Takes Full Damage When Object Destroyed","Restored On Ressurection","unused","unused","Headshotable","Ignores Sheilds" }, 4);
[Value(Name="Vitality Percentage", Offset=8)]
public Single _8_Vitality_Percentage;
public _12_instant_responses[] _12_Instant_Responses;
[Serializable][Reflexive(Name="Instant Responses", Offset=12, ChunkSize=80, Label="")]
public class _12_instant_responses
{
private int __StartOffset__;
[Value(Name="Response Type", Offset=0)]
public H2.DataTypes.Enum _0_Response_Type = new H2.DataTypes.Enum(new string[] {  "Receives All Damage", "Receives Area Effect Damage", "Receives Local Damage" }, 2);
[Value(Name="Constraint Damage Type", Offset=2)]
public H2.DataTypes.Enum _2_Constraint_Damage_Type = new H2.DataTypes.Enum(new string[] {  "None", "Destroy One Of Group", "Destroy Entire Group", "Loosen One Of Group", "Loosen Entire Group" }, 2);
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Kills Object","Inhibits Melee Attack","Inhibits Weapon Attack","Inhibits Walking","Forces Drop Weapon","Kills Weapon Primary Trigger","Kills Weapon Secondary Trigger","Light Damage Left Turn","Major Damage Left Turn","Light Damage Right Turn","Major Damage Right Turn","Light Damage Engine","Major Damage Engine","Kills Object (No Player Solo)","Causes Detonation","Destory All Group Constraints","Kills Variant Objects","Force Unattached Effects","Fires Under Threshold","Triggers Special Death","Only On Special Death","Only NOT on Special Death" }, 4);
[Value(Name="Damage Threshold", Offset=8)]
public Single _8_Damage_Threshold;
[Value(Name="Transition Effect", Offset=12)]
public Dependancy _12_Transition_Effect;
[Value(Name="Transition Damage Effect", Offset=20)]
public Dependancy _20_Transition_Damage_Effect;
[Value(Name="Region", Offset=28)]
public StringID _28_Region;
[Value(Name="New State", Offset=32)]
public H2.DataTypes.Enum _32_New_State = new H2.DataTypes.Enum(new string[] {  "Default", "Minor Damage", "Medium Damage", "Major Damage", "Destroyed" }, 2);
[Value(Name="Runtime Region Index", Offset=34)]
public Int16 _34_Runtime_Region_Index;
[Value(Name="Effect Marker Name", Offset=36)]
public StringID _36_Effect_Marker_Name;
[Value(Name="Damage Effect Marker Name", Offset=40)]
public StringID _40_Damage_Effect_Marker_Name;
[Value(Name="Response Delay", Offset=44)]
public Single _44_Response_Delay;
[Value(Name="Delay Effect", Offset=48)]
public Dependancy _48_Delay_Effect;
[Value(Name="Delay Effect Marker Name", Offset=56)]
public StringID _56_Delay_Effect_Marker_Name;
[Value(Name="Contraint/Group Name", Offset=60)]
public StringID _60_Contraint_Group_Name;
[Value(Name="Ejecting Seat Label", Offset=64)]
public StringID _64_Ejecting_Seat_Label;
[Value(Name="Skip Fraction", Offset=68)]
public Single _68_Skip_Fraction;
[Value(Name="Destroyed Child Object Marker Name", Offset=72)]
public StringID _72_Destroyed_Child_Object_Marker_Name;
[Value(Name="Total Damage Threshold", Offset=76)]
public Single _76_Total_Damage_Threshold;
}
[Value(Offset=20)]
public Byte[] _20_ = new byte[20];
[Value(Name="Stun Time", Offset=40)]
public Single _40_Stun_Time;
[Value(Name="Recharge Time", Offset=44)]
public Single _44_Recharge_Time;
[Value(Name="Resurrection Restored Region Name", Offset=48)]
public StringID _48_Resurrection_Restored_Region_Name;
[Value(Offset=52)]
public Byte[] _52_ = new byte[4];
}
public _196_nodes[] _196_Nodes;
[Serializable][Reflexive(Name="Nodes", Offset=196, ChunkSize=16, Label="")]
public class _196_nodes
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[16];
}
[Value(Offset=204)]
public Byte[] _204_ = new byte[12];
public _216_damage_seats[] _216_Damage_Seats;
[Serializable][Reflexive(Name="Damage Seats", Offset=216, ChunkSize=20, Label="")]
public class _216_damage_seats
{
private int __StartOffset__;
[Value(Name="Seat Label", Offset=0)]
public StringID _0_Seat_Label;
[Value(Name="Direct Damage Scale", Offset=4)]
public Single _4_Direct_Damage_Scale;
[Value(Name="Damage Transfer Fall-Off Radius", Offset=8)]
public Single _8_Damage_Transfer_Fall_Off_Radius;
[Value(Name="Max Transfer Damage Scale", Offset=12)]
public Single _12_Max_Transfer_Damage_Scale;
[Value(Name="Min Transfer Damage Scale", Offset=16)]
public Single _16_Min_Transfer_Damage_Scale;
}
public _224_damage_constraints[] _224_Damage_Constraints;
[Serializable][Reflexive(Name="Damage Constraints", Offset=224, ChunkSize=20, Label="")]
public class _224_damage_constraints
{
private int __StartOffset__;
[Value(Name="Physics Model Constraint Name", Offset=0)]
public StringID _0_Physics_Model_Constraint_Name;
[Value(Name="Damage Constraint Name", Offset=4)]
public StringID _4_Damage_Constraint_Name;
[Value(Name="Damage Constaint Goup Name", Offset=8)]
public StringID _8_Damage_Constaint_Goup_Name;
[Value(Name="Group Probability Scale", Offset=12)]
public Single _12_Group_Probability_Scale;
[Value(Offset=16)]
public Byte[] _16_ = new byte[4];
}
[Value(Name="Overshield FP Shader", Offset=232)]
public Dependancy _232_Overshield_FP_Shader;
[Value(Name="Overshield Shader", Offset=240)]
public Dependancy _240_Overshield_Shader;
}
public _84_targets[] _84_Targets;
[Serializable][Reflexive(Name="Targets", Offset=84, ChunkSize=28, Label="")]
public class _84_targets
{
private int __StartOffset__;
[Value(Name="Marker Name", Offset=0)]
public StringID _0_Marker_Name;
[Value(Name="Size", Offset=4)]
public Single _4_Size;
[Value(Name="Cone Angle", Offset=8)]
public Single _8_Cone_Angle;
[Value(Name="Damage Section #", Offset=12)]
public Int16 _12_Damage_Section_No;
[Value(Name="Variant #", Offset=14)]
public Int16 _14_Variant_No;
[Value(Name="Targeting Relevance", Offset=16)]
public Single _16_Targeting_Relevance;
[Value(Name="Flags", Offset=20)]
public Bitmask _20_Flags = new Bitmask(new string[] { "Locked By Human Tracking","Locked By Plasma Tracking","Headshot","Vulnerable","Always Locked By Plasma Tracking" }, 4);
[Value(Name="Lock-On Distance", Offset=24)]
public Single _24_Lock_On_Distance;
}
public _92_model_variations[] _92_Model_Variations;
[Serializable][Reflexive(Name="Model Variations", Offset=92, ChunkSize=16, Label="")]
public class _92_model_variations
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public StringID _0_Type;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
public _8_permutation[] _8_Permutation;
[Serializable][Reflexive(Name="Permutation", Offset=8, ChunkSize=8, Label="")]
public class _8_permutation
{
private int __StartOffset__;
[Value(Name="Variation", Offset=0)]
public StringID _0_Variation;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
}
}
public _100_nodes[] _100_Nodes;
[Serializable][Reflexive(Name="Nodes", Offset=100, ChunkSize=92, Label="")]
public class _100_nodes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Parent (Index)", Offset=4)]
public Int16 _4_Parent__Index_;
[Value(Name="First Child (Index)", Offset=6)]
public Int16 _6_First_Child__Index_;
[Value(Name="Next Sibling (Index)", Offset=8)]
public Int16 _8_Next_Sibling__Index_;
[Value(Offset=10)]
public Byte[] _10_ = new byte[2];
[Value(Name="Y", Offset=12)]
public Single _12_Y;
[Value(Name="X", Offset=16)]
public Single _16_X;
[Value(Name="Z", Offset=20)]
public Single _20_Z;
[Value(Name="i", Offset=24)]
public Single _24_i;
[Value(Name="j", Offset=28)]
public Single _28_j;
[Value(Name="k", Offset=32)]
public Single _32_k;
[Value(Name="w", Offset=36)]
public Single _36_w;
[Value(Name="Scale Factor", Offset=40)]
public Single _40_Scale_Factor;
[Value(Offset=44)]
public Byte[] _44_ = new byte[36];
[Value(Name="Y (Leave Child)", Offset=80)]
public Single _80_Y__Leave_Child_;
[Value(Name="X (Leave Child)", Offset=84)]
public Single _84_X__Leave_Child_;
[Value(Name="Z (Leave Child)", Offset=88)]
public Single _88_Z__Leave_Child_;
}
[Value(Offset=108)]
public Byte[] _108_ = new byte[4];
public _112_model_object_data[] _112_Model_Object_Data;
[Serializable][Reflexive(Name="Model Object Data", Offset=112, ChunkSize=20, Label="")]
public class _112_model_object_data
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="X", Offset=4)]
public Single _4_X;
[Value(Name="Y", Offset=8)]
public Single _8_Y;
[Value(Name="Z", Offset=12)]
public Single _12_Z;
[Value(Name="Radius", Offset=16)]
public Single _16_Radius;
}
[Value(Name="Unit Dialoge", Offset=120)]
public Dependancy _120_Unit_Dialoge;
[Value(Name="Active Camo Shader", Offset=124)]
public Dependancy _124_Active_Camo_Shader;
[Value(Name="Flags", Offset=128)]
public Bitmask _128_Flags = new Bitmask(new string[] { "Active Camo Always On","Active Camo Always Merge","Active Camo Never Merge" }, 4);
[Value(Name="Default Dialogue Effect", Offset=132)]
public StringID _132_Default_Dialogue_Effect;
[Value(Name="Runtime Flags", Offset=136)]
public Bitmask _136_Runtime_Flags = new Bitmask(new string[] { "Contains Run-Time Nodes" }, 4);
[Value(Offset=140)]
public Byte[] _140_ = new byte[72];
[Value(Name="Hologram Shader", Offset=212)]
public Dependancy _212_Hologram_Shader;
[Value(Name="Hologram Control Function", Offset=216)]
public StringID _216_Hologram_Control_Function;
        }
    }
}