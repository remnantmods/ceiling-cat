using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("vehc")]
        public class vehc
        {
public _0_vehicle_permutations[] _0_Vehicle_Permutations;
[Serializable][Reflexive(Name="Vehicle Permutations", Offset=0, ChunkSize=16, Label="")]
public class _0_vehicle_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Vehicle", Offset=4)]
public Dependancy _4_Vehicle;
[Value(Name="Variant Name", Offset=12)]
public StringID _12_Variant_Name;
}
[Value(Name="Unused Spawn Time    (in seconds, 0 = default)", Offset=8)]
public UInt32 _8_Unused_Spawn_Time_____in_seconds__0___default_;
        }
    }
}