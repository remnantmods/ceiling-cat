using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("mdlg")]
        public class mdlg
        {
public _0_unknown[] _0_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=0, ChunkSize=16, Label="")]
public class _0_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
public _4_unknown[] _4_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=4, ChunkSize=16, Label="")]
public class _4_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Dependancy _4_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[4];
}
[Value(Name="Unknown", Offset=12)]
public StringID _12_Unknown;
}
        }
    }
}