using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("coll")]
        public class coll
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[20];
public _20_materials[] _20_Materials;
[Serializable][Reflexive(Name="Materials", Offset=20, ChunkSize=4, Label="")]
public class _20_materials
{
private int __StartOffset__;
[Value(Name="Material Type", Offset=0)]
public StringID _0_Material_Type;
}
public _28_regions[] _28_Regions;
[Serializable][Reflexive(Name="Regions", Offset=28, ChunkSize=12, Label="")]
public class _28_regions
{
private int __StartOffset__;
[Value(Name="Peice", Offset=0)]
public StringID _0_Peice;
public _4_premutations[] _4_Premutations;
[Serializable][Reflexive(Name="Premutations", Offset=4, ChunkSize=20, Label="")]
public class _4_premutations
{
private int __StartOffset__;
[Value(Name="Premutation", Offset=0)]
public StringID _0_Premutation;
public _4_bsp[] _4_BSP;
[Serializable][Reflexive(Name="BSP", Offset=4, ChunkSize=68, Label="")]
public class _4_bsp
{
private int __StartOffset__;
[Value(Name="BSP", Offset=0)]
public Int32 _0_BSP;
public _4_3d_nodes[] _4_3D_Nodes;
[Serializable][Reflexive(Name="3D Nodes", Offset=4, ChunkSize=8, Label="")]
public class _4_3d_nodes
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="First Child", Offset=2)]
public Int16 _2_First_Child;
[Value(Name="Unknown", Offset=4)]
public Byte _4_Unknown;
[Value(Name="Second Child", Offset=5)]
public Int16 _5_Second_Child;
[Value(Name="Unknown", Offset=7)]
public Byte _7_Unknown;
}
public _12_planes[] _12_Planes;
[Serializable][Reflexive(Name="Planes", Offset=12, ChunkSize=16, Label="")]
public class _12_planes
{
private int __StartOffset__;
[Value(Name="i", Offset=0)]
public Int32 _0_i;
[Value(Name="I", Offset=4)]
public Int32 _4_I;
[Value(Name="k", Offset=8)]
public Int32 _8_k;
[Value(Name="d", Offset=12)]
public Int32 _12_d;
}
public _20_leaves[] _20_Leaves;
[Serializable][Reflexive(Name="Leaves", Offset=20, ChunkSize=4, Label="")]
public class _20_leaves
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Byte _0_Flags;
[Value(Name="2D Ref", Offset=1)]
public Byte _1_2D_Ref;
[Value(Name="Refrence count", Offset=2)]
public Int16 _2_Refrence_count;
}
public _28_2d_refrences[] _28_2D_Refrences;
[Serializable][Reflexive(Name="2D Refrences", Offset=28, ChunkSize=4, Label="")]
public class _28_2d_refrences
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="Node", Offset=2)]
public Int16 _2_Node;
}
public _36_planes[] _36_Planes;
[Serializable][Reflexive(Name="Planes", Offset=36, ChunkSize=16, Label="")]
public class _36_planes
{
private int __StartOffset__;
[Value(Name="Plane i", Offset=0)]
public Int32 _0_Plane_i;
[Value(Name="Plane j", Offset=4)]
public Int32 _4_Plane_j;
[Value(Name="Plane k", Offset=8)]
public Int32 _8_Plane_k;
[Value(Name="Left Child", Offset=12)]
public Int16 _12_Left_Child;
[Value(Name="Right Child", Offset=14)]
public Int16 _14_Right_Child;
}
public _44_surfaces[] _44_Surfaces;
[Serializable][Reflexive(Name="Surfaces", Offset=44, ChunkSize=8, Label="")]
public class _44_surfaces
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="First Edge", Offset=2)]
public Int16 _2_First_Edge;
[Value(Name="Flags", Offset=4)]
public Byte _4_Flags;
[Value(Name="Breakable Surface", Offset=5)]
public Byte _5_Breakable_Surface;
[Value(Name="Material", Offset=6)]
public Int16 _6_Material;
}
public _52_edge_buffer[] _52_Edge_Buffer;
[Serializable][Reflexive(Name="Edge Buffer", Offset=52, ChunkSize=12, Label="")]
public class _52_edge_buffer
{
private int __StartOffset__;
[Value(Name="Start Vertex", Offset=0)]
public Int16 _0_Start_Vertex;
[Value(Name="End Vertex", Offset=2)]
public Int16 _2_End_Vertex;
[Value(Name="Forward Edge", Offset=4)]
public Int16 _4_Forward_Edge;
[Value(Name="Reverse Edge", Offset=6)]
public Int16 _6_Reverse_Edge;
[Value(Name="Left Surface", Offset=8)]
public Int16 _8_Left_Surface;
[Value(Name="Right Surface", Offset=10)]
public Int16 _10_Right_Surface;
}
public _60_vertices[] _60_Vertices;
[Serializable][Reflexive(Name="Vertices", Offset=60, ChunkSize=16, Label="")]
public class _60_vertices
{
private int __StartOffset__;
[Value(Name="x", Offset=0)]
public Int32 _0_x;
[Value(Name="y", Offset=4)]
public Int32 _4_y;
[Value(Name="z", Offset=8)]
public Int32 _8_z;
[Value(Name="First Edge", Offset=12)]
public Int32 _12_First_Edge;
}
}
public _12_unknown[] _12_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=12, ChunkSize=112, Label="")]
public class _12_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Offset=28)]
public Byte[] _28_ = new byte[4];
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Offset=44)]
public Byte[] _44_ = new byte[4];
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Offset=52)]
public Byte[] _52_ = new byte[12];
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Offset=72)]
public Byte[] _72_ = new byte[4];
[Value(Name="Unknown", Offset=76)]
public Int32 _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Int32 _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Offset=88)]
public Byte[] _88_ = new byte[4];
[Value(Name="Unknown", Offset=92)]
public Int32 _92_Unknown;
[Value(Name="Unknown", Offset=96)]
public Int32 _96_Unknown;
public _100_unknown[] _100_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=100, ChunkSize=1, Label="")]
public class _100_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Byte _0_Unknown;
}
[Value(Offset=108)]
public Byte[] _108_ = new byte[4];
}
}
}
public _36_path_finding_spheres[] _36_Path_Finding_Spheres;
[Serializable][Reflexive(Name="Path Finding Spheres", Offset=36, ChunkSize=20, Label="")]
public class _36_path_finding_spheres
{
private int __StartOffset__;
[Value(Name="Node Chunk", Offset=0)]
public Int32 _0_Node_Chunk;
[Value(Name="Center Point X", Offset=4)]
public Single _4_Center_Point_X;
[Value(Name="Center Point Y", Offset=8)]
public Single _8_Center_Point_Y;
[Value(Name="Center Point Z", Offset=12)]
public Single _12_Center_Point_Z;
[Value(Name="Radius", Offset=16)]
public Single _16_Radius;
}
public _44_nodes[] _44_Nodes;
[Serializable][Reflexive(Name="Nodes", Offset=44, ChunkSize=12, Label="")]
public class _44_nodes
{
private int __StartOffset__;
[Value(Name="Node", Offset=0)]
public StringID _0_Node;
[Value(Name="Parent", Offset=4)]
public Int16 _4_Parent;
[Value(Name="Child", Offset=6)]
public Int16 _6_Child;
[Value(Name="Next Sibling", Offset=8)]
public Int16 _8_Next_Sibling;
[Value(Name="Final Sibling?", Offset=10)]
public Int16 _10_Final_Sibling_;
}
        }
    }
}