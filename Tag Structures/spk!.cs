using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("spk!")]
        public class spk_
        {
[Value(Name="Almost Never", Offset=0)]
public Single _0_Almost_Never;
[Value(Name="Rarely", Offset=4)]
public Single _4_Rarely;
[Value(Name="Somewhat", Offset=8)]
public Single _8_Somewhat;
[Value(Name="Often", Offset=12)]
public Single _12_Often;
[Value(Offset=16)]
public Byte[] _16_ = new byte[24];
        }
    }
}