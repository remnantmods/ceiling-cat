using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("wgit")]
        public class wgit
        {
[Value(Name="Back ground blur (blur=0 noblur=37)", Offset=0)]
public Int16 _0_Back_ground_blur__blur_0_noblur_37_;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Menu ID", Offset=4)]
public Int16 _4_Menu_ID;
[Value(Name="Visibles X-A-B Options Buttons", Offset=6)]
public Bitmask _6_Visibles_X_A_B_Options_Buttons = new Bitmask(new string[] { "A Select B Cancel","A Select B Cancel","X Party Privacy A Select B Cancel","X Options","A Select B Back" }, 2);
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unic", Offset=24)]
public Dependancy _24_Unic;
public _28_menu_content[] _28_Menu_Content;
[Serializable][Reflexive(Name="Menu Content", Offset=28, ChunkSize=76, Label="")]
public class _28_menu_content
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Int16 _0_Unused;
[Value(Name="Transition Type", Offset=2)]
public Int16 _2_Transition_Type;
public _4_buttons_1[] _4_Buttons_1;
[Serializable][Reflexive(Name="Buttons 1", Offset=4, ChunkSize=60, Label="")]
public class _4_buttons_1
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Int16 _0_Unused;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Button Transition type (bitmask)", Offset=4)]
public Int16 _4_Button_Transition_type__bitmask_;
[Value(Name="Unused", Offset=6)]
public Int16 _6_Unused;
[Value(Name="Unused", Offset=8)]
public Int16 _8_Unused;
[Value(Name="Font Type", Offset=10)]
public Int16 _10_Font_Type;
[Value(Name="Unused", Offset=12)]
public Single _12_Unused;
[Value(Name="Text Color Red", Offset=16)]
public Single _16_Text_Color_Red;
[Value(Name="Text Color Green", Offset=20)]
public Single _20_Text_Color_Green;
[Value(Name="Text Color Blue", Offset=24)]
public Single _24_Text_Color_Blue;
[Value(Name="Bitmap Placement Positive Coord Y", Offset=28)]
public Int16 _28_Bitmap_Placement_Positive_Coord_Y;
[Value(Name="Bitmap Placement Positive Coord X", Offset=30)]
public Int16 _30_Bitmap_Placement_Positive_Coord_X;
[Value(Name="Text Placement Negative Coord Y", Offset=32)]
public Int16 _32_Text_Placement_Negative_Coord_Y;
[Value(Name="Text Placement Negative Coord X", Offset=34)]
public Int16 _34_Text_Placement_Negative_Coord_X;
[Value(Name="Bitm", Offset=36)]
public Dependancy _36_Bitm;
[Value(Name="coord", Offset=44)]
public Byte _44_coord;
[Value(Name="coord", Offset=45)]
public Byte _45_coord;
[Value(Name="coord", Offset=46)]
public Byte _46_coord;
[Value(Name="coord", Offset=47)]
public Byte _47_coord;
[Value(Name="Something count", Offset=48)]
public Byte _48_Something_count;
[Value(Name="Bitmap Transiton type (bitmask)", Offset=49)]
public Byte _49_Bitmap_Transiton_type__bitmask_;
[Value(Name="Unused", Offset=50)]
public Byte _50_Unused;
[Value(Name="brightness something?", Offset=51)]
public Byte _51_brightness_something_;
[Value(Name="Unknown", Offset=52)]
public Int16 _52_Unknown;
[Value(Name="Unused", Offset=54)]
public Int16 _54_Unused;
[Value(Name="Unknown", Offset=56)]
public Int16 _56_Unknown;
[Value(Name="Unused", Offset=58)]
public Int16 _58_Unused;
}
public _12_buttons_2[] _12_Buttons_2;
[Serializable][Reflexive(Name="Buttons 2", Offset=12, ChunkSize=16, Label="")]
public class _12_buttons_2
{
private int __StartOffset__;
[Value(Name="Buttons loop", Offset=0)]
public H2.DataTypes.Enum _0_Buttons_loop = new H2.DataTypes.Enum(new string[] {  "Yes", "No" }, 2);
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Button Type (Palette [skin])", Offset=4)]
public Int16 _4_Button_Type__Palette__skin__;
[Value(Name="Number of visibles buttons", Offset=6)]
public Int16 _6_Number_of_visibles_buttons;
[Value(Name="Buttons Placement Coord X", Offset=8)]
public Int16 _8_Buttons_Placement_Coord_X;
[Value(Name="Buttons Placement Coord Y", Offset=10)]
public Int16 _10_Buttons_Placement_Coord_Y;
[Value(Name="Transition Type? (bitmask)", Offset=12)]
public Int16 _12_Transition_Type___bitmask_;
[Value(Name="Variation?", Offset=14)]
public Int16 _14_Variation_;
}
[Value(Name="Unused", Offset=20)]
public Single _20_Unused;
[Value(Name="Unused", Offset=24)]
public Single _24_Unused;
public _28_text_strings[] _28_Text_strings;
[Serializable][Reflexive(Name="Text strings", Offset=28, ChunkSize=44, Label="")]
public class _28_text_strings
{
private int __StartOffset__;
[Value(Name="Text options", Offset=0)]
public Bitmask _0_Text_options = new Bitmask(new string[] { "1","2","Pulsing","Auto Typing text","5" }, 4);
[Value(Name="Transtion type (bitmask)", Offset=4)]
public Int16 _4_Transtion_type__bitmask_;
[Value(Name="Variation?", Offset=6)]
public Int16 _6_Variation_;
[Value(Name="Unused", Offset=8)]
public Int16 _8_Unused;
[Value(Name="Font Type", Offset=10)]
public Int16 _10_Font_Type;
[Value(Name="Alpha", Offset=12)]
public Single _12_Alpha;
[Value(Name="Color Red", Offset=16)]
public Single _16_Color_Red;
[Value(Name="Color Green", Offset=20)]
public Single _20_Color_Green;
[Value(Name="Color Blue", Offset=24)]
public Single _24_Color_Blue;
[Value(Name="Text Placement coord Y from Top", Offset=28)]
public Int16 _28_Text_Placement_coord_Y_from_Top;
[Value(Name="Text Placement coord X from Right", Offset=30)]
public Int16 _30_Text_Placement_coord_X_from_Right;
[Value(Name="Text Placement coord Y from Bottom", Offset=32)]
public Int16 _32_Text_Placement_coord_Y_from_Bottom;
[Value(Name="Text Placement coord X from Left", Offset=34)]
public Int16 _34_Text_Placement_coord_X_from_Left;
[Value(Name="String name", Offset=36)]
public StringID _36_String_name;
[Value(Name="Unknown", Offset=40)]
public UInt32 _40_Unknown;
}
public _36_ui_bitmaps[] _36_UI_bitmaps;
[Serializable][Reflexive(Name="UI bitmaps", Offset=36, ChunkSize=56, Label="")]
public class _36_ui_bitmaps
{
private int __StartOffset__;
[Value(Name="unknown", Offset=0)]
public Int16 _0_unknown;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Transition type (bitmask)", Offset=4)]
public Int16 _4_Transition_type__bitmask_;
[Value(Name="Variation?", Offset=6)]
public Int16 _6_Variation_;
[Value(Name="Alpha transparence", Offset=8)]
public H2.DataTypes.Enum _8_Alpha_transparence = new H2.DataTypes.Enum(new string[] {  "Normal", "Inverted" }, 2);
[Value(Name="Xbox live bitmap setting", Offset=10)]
public Int16 _10_Xbox_live_bitmap_setting;
[Value(Name="Bitmap Placement coord X", Offset=12)]
public Int16 _12_Bitmap_Placement_coord_X;
[Value(Name="Bitmap Placement coord Y", Offset=14)]
public Int16 _14_Bitmap_Placement_coord_Y;
[Value(Name="Horizontal bitmap scrolling speed", Offset=16)]
public Single _16_Horizontal_bitmap_scrolling_speed;
[Value(Name="Unused", Offset=20)]
public Single _20_Unused;
[Value(Name="bitm", Offset=24)]
public Dependancy _24_bitm;
[Value(Name="Bitmap Layer Level", Offset=32)]
public Int16 _32_Bitmap_Layer_Level;
[Value(Name="Unused", Offset=34)]
public Int16 _34_Unused;
[Value(Name="Unused", Offset=36)]
public Single _36_Unused;
[Value(Name="Coord X for ?", Offset=40)]
public Int16 _40_Coord_X_for__;
[Value(Name="Coord Y for ?", Offset=42)]
public Int16 _42_Coord_Y_for__;
[Value(Name="Unused", Offset=44)]
public Single _44_Unused;
[Value(Name="Unused", Offset=48)]
public Single _48_Unused;
[Value(Name="Unused", Offset=52)]
public Single _52_Unused;
}
public _44_modelsscript_objects__scenery_bipeds____[] _44_ModelsScript_Objects__Scenery_Bipeds____;
[Serializable][Reflexive(Name="ModelsScript Objects: Scenery/Bipeds...)", Offset=44, ChunkSize=76, Label="")]
public class _44_modelsscript_objects__scenery_bipeds____
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Int16 _0_Unused;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Transition type (bitmask)", Offset=4)]
public Int16 _4_Transition_type__bitmask_;
[Value(Name="Variation?", Offset=6)]
public Int16 _6_Variation_;
[Value(Name="Unknown", Offset=8)]
public Int16 _8_Unknown;
[Value(Name="Unused", Offset=10)]
public Int16 _10_Unused;
public _12_script_object[] _12_Script_Object;
[Serializable][Reflexive(Name="Script Object", Offset=12, ChunkSize=32, Label="")]
public class _12_script_object
{
private int __StartOffset__;
[Value(Name="Script Object name", Offset=0)]
public H2.DataTypes.String _0_Script_Object_name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
}
public _20_unknown[] _20_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=20, ChunkSize=32, Label="")]
public class _20_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public H2.DataTypes.String _0_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
}
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="relatives: coord, size, palcement", Offset=52)]
public Byte _52_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=53)]
public Byte _53_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=54)]
public Byte _54_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=55)]
public Byte _55_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=56)]
public Byte _56_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=57)]
public Byte _57_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=58)]
public Byte _58_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=59)]
public Byte _59_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=60)]
public Byte _60_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=61)]
public Byte _61_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=62)]
public Byte _62_relatives__coord__size__palcement;
[Value(Name="relatives: coord, size, palcement", Offset=63)]
public Byte _63_relatives__coord__size__palcement;
[Value(Name="Unused", Offset=64)]
public Single _64_Unused;
[Value(Name="Unused", Offset=68)]
public Single _68_Unused;
[Value(Name="Unused", Offset=72)]
public Single _72_Unused;
}
[Value(Name="Unused", Offset=52)]
public Single _52_Unused;
[Value(Name="Unused", Offset=56)]
public Single _56_Unused;
[Value(Name="Unused", Offset=60)]
public Single _60_Unused;
[Value(Name="Unused", Offset=64)]
public Single _64_Unused;
public _68_variable_buttons[] _68_Variable_Buttons;
[Serializable][Reflexive(Name="Variable Buttons", Offset=68, ChunkSize=24, Label="")]
public class _68_variable_buttons
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Skin", Offset=4)]
public Dependancy _4_Skin;
[Value(Name="Coord X", Offset=12)]
public Int16 _12_Coord_X;
[Value(Name="Coord Y", Offset=14)]
public Int16 _14_Coord_Y;
[Value(Name="unknown", Offset=16)]
public Byte _16_unknown;
[Value(Name="Max Visible Buttons", Offset=17)]
public Byte _17_Max_Visible_Buttons;
[Value(Name="Unknown", Offset=18)]
public Byte _18_Unknown;
[Value(Name="Unknown", Offset=19)]
public Byte _19_Unknown;
[Value(Name="Coord X", Offset=20)]
public Int16 _20_Coord_X;
[Value(Name="Coord Y", Offset=22)]
public Int16 _22_Coord_Y;
}
}
[Value(Name="Font Type", Offset=36)]
public Int16 _36_Font_Type;
[Value(Name="Unused", Offset=38)]
public Int16 _38_Unused;
[Value(Name="Header Title Text String", Offset=40)]
public StringID _40_Header_Title_Text_String;
public _44_descriptions_help_text[] _44_Descriptions_Help_Text;
[Serializable][Reflexive(Name="Descriptions/Help Text", Offset=44, ChunkSize=12, Label="")]
public class _44_descriptions_help_text
{
private int __StartOffset__;
[Value(Name="Text Strings", Offset=0)]
public StringID _0_Text_Strings;
public _4_notification_messages[] _4_Notification_messages;
[Serializable][Reflexive(Name="Notification messages", Offset=4, ChunkSize=4, Label="")]
public class _4_notification_messages
{
private int __StartOffset__;
[Value(Name="Text Strings", Offset=0)]
public StringID _0_Text_Strings;
}
}
public _52_prededicated_resources__bitmap_tags[] _52_Prededicated_Resources__Bitmap_tags;
[Serializable][Reflexive(Name="Prededicated Resources: Bitmap tags", Offset=52, ChunkSize=8, Label="")]
public class _52_prededicated_resources__bitmap_tags
{
private int __StartOffset__;
[Value(Name="Bitm", Offset=0)]
public Dependancy _0_Bitm;
}
[Value(Name="SP Loading screen effects settings", Offset=60)]
public Single _60_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=64)]
public Single _64_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=68)]
public Single _68_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=72)]
public Single _72_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=76)]
public Single _76_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=80)]
public Single _80_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=84)]
public Single _84_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=88)]
public Single _88_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=92)]
public Single _92_SP_Loading_screen_effects_settings;
[Value(Name="SP Loading screen effects settings", Offset=96)]
public Single _96_SP_Loading_screen_effects_settings;
        }
    }
}