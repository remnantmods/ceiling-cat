using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("cont")]
        public class cont
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "First point unfaded","Last point unfaded","Points start pinned to media","Points start pinned to ground","Points always pinned to media","Points always pinned to ground","Edge effects fade slowly","Don't Inherit Object Change Color" }, 2);
[Value(Name="Scale Flags", Offset=2)]
public Bitmask _2_Scale_Flags = new Bitmask(new string[] { "Point Generation Rate","Point Velocity","Point Velocity Delta","Point Velocity Cone Angle","Inherited Velocity Fraction","Sequence Animation Rate","Texture Scale U","Texture Scale V","Texture Animation U","Texture Animation V" }, 2);
[Value(Name="Point Generation Rate", Offset=4)]
public Single _4_Point_Generation_Rate;
[Value(Name="Min Point Velocity", Offset=8)]
public Single _8_Min_Point_Velocity;
[Value(Name="Max Point Velocity", Offset=12)]
public Single _12_Max_Point_Velocity;
[Value(Name="Point Velocity Cone Angle", Offset=16)]
public Single _16_Point_Velocity_Cone_Angle;
[Value(Name="Inherited Velocity Fraction", Offset=20)]
public Single _20_Inherited_Velocity_Fraction;
[Value(Name="Render Type", Offset=24)]
public H2.DataTypes.Enum _24_Render_Type = new H2.DataTypes.Enum(new string[] {  "Verticle Orientation", "Horizontal Orientation", "Media Mapped", "Ground Mapped", "Viewer Facing", "Double Marker Linked" }, 2);
[Value(Offset=26)]
public Byte[] _26_ = new byte[2];
[Value(Name="Texture Repeats U", Offset=28)]
public Single _28_Texture_Repeats_U;
[Value(Name="Texture Repeats V", Offset=32)]
public Single _32_Texture_Repeats_V;
[Value(Name="Texture Animation U", Offset=36)]
public Single _36_Texture_Animation_U;
[Value(Name="Texture Animation V", Offset=40)]
public Single _40_Texture_Animation_V;
[Value(Name="Texture Animation Rate", Offset=44)]
public Single _44_Texture_Animation_Rate;
[Value(Name="Bitmap", Offset=48)]
public Dependancy _48_Bitmap;
[Value(Name="First Sequence Index", Offset=52)]
public Int16 _52_First_Sequence_Index;
[Value(Name="Sequence Count", Offset=54)]
public Int16 _54_Sequence_Count;
[Value(Offset=56)]
public Byte[] _56_ = new byte[36];
[Value(Name="Shader Flags", Offset=92)]
public Bitmask _92_Shader_Flags = new Bitmask(new string[] { "Sort Bias","Nonlinear Tint","Don't Overdraw FP Weapon" }, 2);
[Value(Name="Framebuffer Blend Function", Offset=94)]
public H2.DataTypes.Enum _94_Framebuffer_Blend_Function = new H2.DataTypes.Enum(new string[] {  "Alpha Blend", "Multiply", "Double Multiply", "Add", "Subtract", "Component Min", "Component Max", "Alpha-Multiply-Add" }, 2);
[Value(Name="Framebuffer Fade Mode", Offset=96)]
public H2.DataTypes.Enum _96_Framebuffer_Fade_Mode = new H2.DataTypes.Enum(new string[] {  "None", "Fade When Perpendicular", "Fade When Parallel", "Fade After Duration" }, 2);
[Value(Name="Map Flags", Offset=98)]
public Bitmask _98_Map_Flags = new Bitmask(new string[] { "Unfiltered" }, 4);
[Value(Offset=102)]
public Byte[] _102_ = new byte[30];
[Value(Name="Bitmap", Offset=132)]
public Dependancy _132_Bitmap;
[Value(Offset=136)]
public Byte[] _136_ = new byte[28];
[Value(Name="Anchor", Offset=164)]
public H2.DataTypes.Enum _164_Anchor = new H2.DataTypes.Enum(new string[] {  "With Primary", "With Screen Space", "ZSprite" }, 2);
[Value(Name="Flags", Offset=166)]
public Bitmask _166_Flags = new Bitmask(new string[] { "Unfiltered" }, 2);
[Value(Name="U-Animation Function", Offset=168)]
public H2.DataTypes.Enum _168_U_Animation_Function = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="U-Animation Period", Offset=170)]
public Single _170_U_Animation_Period;
[Value(Name="U-Animation Phase", Offset=174)]
public Single _174_U_Animation_Phase;
[Value(Name="U-Animation Scale", Offset=178)]
public Single _178_U_Animation_Scale;
[Value(Name="V-Animation Function", Offset=182)]
public H2.DataTypes.Enum _182_V_Animation_Function = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="V-Animation Period", Offset=184)]
public Single _184_V_Animation_Period;
[Value(Name="V-Animation Phase", Offset=188)]
public Single _188_V_Animation_Phase;
[Value(Name="V-Animation Scale", Offset=192)]
public Single _192_V_Animation_Scale;
[Value(Name="Rotation-Animation Function", Offset=196)]
public H2.DataTypes.Enum _196_Rotation_Animation_Function = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Rotation-Animation Period", Offset=198)]
public Single _198_Rotation_Animation_Period;
[Value(Name="Rotation-Animation Phase", Offset=202)]
public Single _202_Rotation_Animation_Phase;
[Value(Name="Rotation-Animation Scale", Offset=206)]
public Single _206_Rotation_Animation_Scale;
[Value(Name="Rotation-Animation Center X", Offset=210)]
public Single _210_Rotation_Animation_Center_X;
[Value(Name="Rotation-Animation Center Y", Offset=214)]
public Single _214_Rotation_Animation_Center_Y;
[Value(Name="ZSprite Radius Sclae", Offset=218)]
public Single _218_ZSprite_Radius_Sclae;
public _222_point_states[] _222_Point_States;
[Serializable][Reflexive(Name="Point States", Offset=222, ChunkSize=64, Label="")]
public class _222_point_states
{
private int __StartOffset__;
[Value(Name="Duration Lower", Offset=0)]
public Single _0_Duration_Lower;
[Value(Name="Duration Upper", Offset=4)]
public Single _4_Duration_Upper;
[Value(Name="Transition Lower", Offset=8)]
public Single _8_Transition_Lower;
[Value(Name="Transition Upper", Offset=12)]
public Single _12_Transition_Upper;
[Value(Name="Physics", Offset=16)]
public Dependancy _16_Physics;
[Value(Name="Width", Offset=24)]
public Single _24_Width;
[Value(Name="Color Lower Alpha", Offset=28)]
public Single _28_Color_Lower_Alpha;
[Value(Name="Color Lower Red", Offset=32)]
public Single _32_Color_Lower_Red;
[Value(Name="Color Lower Green", Offset=36)]
public Single _36_Color_Lower_Green;
[Value(Name="Color Lower Blue", Offset=40)]
public Single _40_Color_Lower_Blue;
[Value(Name="Color Upper Alpha", Offset=44)]
public Single _44_Color_Upper_Alpha;
[Value(Name="Color Upper Red", Offset=48)]
public Single _48_Color_Upper_Red;
[Value(Name="Color Upper Green", Offset=52)]
public Single _52_Color_Upper_Green;
[Value(Name="Color Upper Blue", Offset=56)]
public Single _56_Color_Upper_Blue;
[Value(Name="Scale Flags", Offset=60)]
public Bitmask _60_Scale_Flags = new Bitmask(new string[] { "Duration","Duration Delta","Transition Duration","Transition Duration Delta","Width","Color" }, 4);
}
        }
    }
}