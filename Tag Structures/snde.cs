using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("snde")]
        public class snde
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Priority", Offset=8)]
public Single _8_Priority;
[Value(Name="Room intensity(dB)", Offset=12)]
public Single _12_Room_intensity_dB_;
[Value(Name="Room intensity hf(dB)", Offset=16)]
public Single _16_Room_intensity_hf_dB_;
[Value(Name="Room Rolloff(0 to 10)", Offset=20)]
public Single _20_Room_Rolloff_0_to_10_;
[Value(Name="Decay Time(.1 to 20) Sec", Offset=24)]
public Single _24_Decay_Time__1_to_20__Sec;
[Value(Name="Decay Hf Ratio(.1 to 2)", Offset=28)]
public Single _28_Decay_Hf_Ratio__1_to_2_;
[Value(Name="Reflections Delay (0 to 0.3) Sec", Offset=32)]
public Single _32_Reflections_Delay__0_to_0_3__Sec;
[Value(Name="Reverb Intensity(dB -100,20)", Offset=36)]
public Single _36_Reverb_Intensity_dB__100_20_;
[Value(Name="Reverb Delay(0 to .1) Sec", Offset=40)]
public Single _40_Reverb_Delay_0_to__1__Sec;
[Value(Name="Diffusion", Offset=44)]
public Single _44_Diffusion;
[Value(Name="Density", Offset=48)]
public Single _48_Density;
[Value(Name="Hf Refrence(20 to 20,000)Hz", Offset=52)]
public Single _52_Hf_Refrence_20_to_20_000_Hz;
[Value(Offset=56)]
public Byte[] _56_ = new byte[16];
        }
    }
}