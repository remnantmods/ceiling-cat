using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("char")]
        public class char_
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Dependancy _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Dependancy _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Dependancy _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Dependancy _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Dependancy _20_Unknown;
public _24_variation[] _24_Variation;
[Serializable][Reflexive(Name="Variation", Offset=24, ChunkSize=12, Label="")]
public class _24_variation
{
private int __StartOffset__;
[Value(Name="Permutation", Offset=0)]
public StringID _0_Permutation;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Base permutation", Offset=8)]
public StringID _8_Base_permutation;
}
public _32_flags[] _32_Flags;
[Serializable][Reflexive(Name="Flags", Offset=32, ChunkSize=12, Label="")]
public class _32_flags
{
private int __StartOffset__;
[Value(Name="Flags 1", Offset=0)]
public Bitmask _0_Flags_1 = new Bitmask(new string[] { "Do not Spawn","Do Not Turn" }, 4);
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
public _40_char_stats[] _40_Char_Stats;
[Serializable][Reflexive(Name="Char Stats", Offset=40, ChunkSize=112, Label="")]
public class _40_char_stats
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Health on easy", Offset=4)]
public Single _4_Health_on_easy;
[Value(Name="Shields on easy", Offset=8)]
public Single _8_Shields_on_easy;
[Value(Name="Health on legendary", Offset=12)]
public Single _12_Health_on_legendary;
[Value(Name="Shields on legendary", Offset=16)]
public Single _16_Shields_on_legendary;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Shield recharge rate on easy", Offset=68)]
public Single _68_Shield_recharge_rate_on_easy;
[Value(Name="Shield recharge rate on legendary", Offset=72)]
public Single _72_Shield_recharge_rate_on_legendary;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Single _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Single _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Single _88_Unknown;
[Value(Name="Unknown", Offset=92)]
public Single _92_Unknown;
[Value(Name="Unknown", Offset=96)]
public Single _96_Unknown;
[Value(Offset=100)]
public Byte[] _100_ = new byte[4];
[Value(Name="Unknown", Offset=104)]
public Single _104_Unknown;
[Value(Name="Unknown", Offset=108)]
public Int32 _108_Unknown;
}
public _48_unknown54[] _48_Unknown54;
[Serializable][Reflexive(Name="Unknown54", Offset=48, ChunkSize=52, Label="")]
public class _48_unknown54
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
}
public _56_unknown68[] _56_Unknown68;
[Serializable][Reflexive(Name="Unknown68", Offset=56, ChunkSize=52, Label="")]
public class _56_unknown68
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
}
public _64_unknown83[] _64_Unknown83;
[Serializable][Reflexive(Name="Unknown83", Offset=64, ChunkSize=80, Label="")]
public class _64_unknown83
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Single _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Single _72_Unknown;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
}
public _72_unknown105[] _72_Unknown105;
[Serializable][Reflexive(Name="Unknown105", Offset=72, ChunkSize=36, Label="")]
public class _72_unknown105
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Offset=32)]
public Byte[] _32_ = new byte[4];
}
public _80_unknown116[] _80_Unknown116;
[Serializable][Reflexive(Name="Unknown116", Offset=80, ChunkSize=40, Label="")]
public class _80_unknown116
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
}
public _88_unknown128[] _88_Unknown128;
[Serializable][Reflexive(Name="Unknown128", Offset=88, ChunkSize=8, Label="")]
public class _88_unknown128
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
}
public _96_unknown132[] _96_Unknown132;
[Serializable][Reflexive(Name="Unknown132", Offset=96, ChunkSize=16, Label="")]
public class _96_unknown132
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
public _104_unknown138[] _104_Unknown138;
[Serializable][Reflexive(Name="Unknown138", Offset=104, ChunkSize=64, Label="")]
public class _104_unknown138
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Dependancy _56_Unknown;
}
public _112_unknown156[] _112_Unknown156;
[Serializable][Reflexive(Name="Unknown156", Offset=112, ChunkSize=20, Label="")]
public class _112_unknown156
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
public _120_unknown163[] _120_Unknown163;
[Serializable][Reflexive(Name="Unknown163", Offset=120, ChunkSize=64, Label="")]
public class _120_unknown163
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Offset=20)]
public Byte[] _20_ = new byte[4];
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
}
public _128_unknown181[] _128_Unknown181;
[Serializable][Reflexive(Name="Unknown181", Offset=128, ChunkSize=76, Label="")]
public class _128_unknown181
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Offset=40)]
public Byte[] _40_ = new byte[4];
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Dependancy _68_Unknown;
}
public _136_unknown202[] _136_Unknown202;
[Serializable][Reflexive(Name="Unknown202", Offset=136, ChunkSize=20, Label="")]
public class _136_unknown202
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
public _144_unknown209[] _144_Unknown209;
[Serializable][Reflexive(Name="Unknown209", Offset=144, ChunkSize=36, Label="")]
public class _144_unknown209
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
}
public _152_unknown220[] _152_Unknown220;
[Serializable][Reflexive(Name="Unknown220", Offset=152, ChunkSize=12, Label="")]
public class _152_unknown220
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
public _160_unknown225[] _160_Unknown225;
[Serializable][Reflexive(Name="Unknown225", Offset=160, ChunkSize=8, Label="")]
public class _160_unknown225
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
}
public _168_unknown229[] _168_Unknown229;
[Serializable][Reflexive(Name="Unknown229", Offset=168, ChunkSize=16, Label="")]
public class _168_unknown229
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
public _176_unknown235[] _176_Unknown235;
[Serializable][Reflexive(Name="Unknown235", Offset=176, ChunkSize=12, Label="")]
public class _176_unknown235
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
public _184_unknown240[] _184_Unknown240;
[Serializable][Reflexive(Name="Unknown240", Offset=184, ChunkSize=204, Label="")]
public class _184_unknown240
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Dependancy _4_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Single _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Single _72_Unknown;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Single _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Single _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Single _88_Unknown;
[Value(Name="Unknown", Offset=92)]
public Single _92_Unknown;
[Value(Offset=96)]
public Byte[] _96_ = new byte[24];
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Name="Unknown", Offset=124)]
public Single _124_Unknown;
[Value(Name="Unknown", Offset=128)]
public Single _128_Unknown;
[Value(Offset=132)]
public Byte[] _132_ = new byte[8];
[Value(Name="Unknown", Offset=140)]
public Single _140_Unknown;
[Value(Name="Unknown", Offset=144)]
public Single _144_Unknown;
[Value(Name="Unknown", Offset=148)]
public Int32 _148_Unknown;
[Value(Name="Unknown", Offset=152)]
public Single _152_Unknown;
[Value(Name="Unknown", Offset=156)]
public Single _156_Unknown;
[Value(Name="Unknown", Offset=160)]
public Single _160_Unknown;
[Value(Name="Unknown", Offset=164)]
public Single _164_Unknown;
[Value(Name="Unknown", Offset=168)]
public Single _168_Unknown;
[Value(Name="Unknown", Offset=172)]
public Single _172_Unknown;
[Value(Name="Unknown", Offset=176)]
public Single _176_Unknown;
[Value(Name="Unknown", Offset=180)]
public Single _180_Unknown;
[Value(Name="Unknown", Offset=184)]
public Single _184_Unknown;
[Value(Offset=188)]
public Byte[] _188_ = new byte[8];
[Value(Name="Unknown", Offset=196)]
public Dependancy _196_Unknown;
}
public _192_weapon_associations[] _192_Weapon_Associations;
[Serializable][Reflexive(Name="Weapon Associations", Offset=192, ChunkSize=16, Label="")]
public class _192_weapon_associations
{
private int __StartOffset__;
[Value(Name="Weapon", Offset=0)]
public Dependancy _0_Weapon;
public _8_unknown289[] _8_Unknown289;
[Serializable][Reflexive(Name="Unknown289", Offset=8, ChunkSize=64, Label="")]
public class _8_unknown289
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Int32 _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
}
}
public _200_unknown308[] _200_Unknown308;
[Serializable][Reflexive(Name="Unknown308", Offset=200, ChunkSize=60, Label="")]
public class _200_unknown308
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Int32 _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
}
public _208_vehicle_associations[] _208_Vehicle_Associations;
[Serializable][Reflexive(Name="Vehicle Associations", Offset=208, ChunkSize=180, Label="")]
public class _208_vehicle_associations
{
private int __StartOffset__;
[Value(Name="Vehicle", Offset=0)]
public Dependancy _0_Vehicle;
[Value(Name="Style", Offset=8)]
public Dependancy _8_Style;
[Value(Name="Flags", Offset=16)]
public Bitmask _16_Flags = new Bitmask(new string[] {  }, 4);
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Int32 _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Single _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Single _72_Unknown;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Single _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Single _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Single _88_Unknown;
[Value(Name="Unknown", Offset=92)]
public Single _92_Unknown;
[Value(Name="Unknown", Offset=96)]
public Single _96_Unknown;
[Value(Name="Unknown", Offset=100)]
public Single _100_Unknown;
[Value(Name="Unknown", Offset=104)]
public Single _104_Unknown;
[Value(Name="Unknown", Offset=108)]
public Single _108_Unknown;
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Single _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Single _120_Unknown;
[Value(Name="Unknown", Offset=124)]
public Single _124_Unknown;
[Value(Name="Unknown", Offset=128)]
public Single _128_Unknown;
[Value(Name="Unknown", Offset=132)]
public Single _132_Unknown;
[Value(Name="Unknown", Offset=136)]
public Single _136_Unknown;
[Value(Name="Unknown", Offset=140)]
public Single _140_Unknown;
[Value(Name="Unknown", Offset=144)]
public Single _144_Unknown;
[Value(Name="Unknown", Offset=148)]
public Single _148_Unknown;
[Value(Name="Unknown", Offset=152)]
public Single _152_Unknown;
[Value(Name="Unknown", Offset=156)]
public Single _156_Unknown;
[Value(Name="Unknown", Offset=160)]
public Single _160_Unknown;
[Value(Name="Unknown", Offset=164)]
public Single _164_Unknown;
[Value(Name="Unknown", Offset=168)]
public Single _168_Unknown;
[Value(Name="Unknown", Offset=172)]
public Single _172_Unknown;
[Value(Name="Unknown", Offset=176)]
public Int32 _176_Unknown;
}
        }
    }
}