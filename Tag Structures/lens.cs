using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("lens")]
        public class lens
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Falloff Angle (Degrees)", Offset=8)]
public Single _8_Falloff_Angle__Degrees_;
[Value(Name="Cuttoff Angle (Degrees)", Offset=12)]
public Single _12_Cuttoff_Angle__Degrees_;
[Value(Name="Occlusion Radius", Offset=16)]
public Single _16_Occlusion_Radius;
[Value(Name="Occlusion Offset Direction", Offset=20)]
public H2.DataTypes.Enum _20_Occlusion_Offset_Direction = new H2.DataTypes.Enum(new string[] {  "Toward Viewer", "Marker Forward", "None" }, 2);
[Value(Name="Occlusion Inner Radius Scale", Offset=22)]
public H2.DataTypes.Enum _22_Occlusion_Inner_Radius_Scale = new H2.DataTypes.Enum(new string[] {  "None", "1/2", "1/4", "1/8", "1/16", "1/32", "1/64" }, 2);
[Value(Name="Near Fade Distance", Offset=24)]
public Single _24_Near_Fade_Distance;
[Value(Name="Far Fade Distance", Offset=28)]
public Single _28_Far_Fade_Distance;
[Value(Name="Bitmap", Offset=32)]
public Dependancy _32_Bitmap;
[Value(Name="Flags", Offset=36)]
public Bitmask _36_Flags = new Bitmask(new string[] { "Sun","No Occlusion Test","Only Render In 1st Person","Only Render In 3rd Person","Fade In More Quickly","Scale By Marker" }, 4);
[Value(Name="Rotation Function", Offset=40)]
public H2.DataTypes.Enum _40_Rotation_Function = new H2.DataTypes.Enum(new string[] {  "None", "Rotation A", "Rotation B", "Rotation Translation", "Translation" }, 4);
[Value(Name="Rotation Function Scale", Offset=44)]
public Single _44_Rotation_Function_Scale;
[Value(Name="Corona Scale i", Offset=48)]
public Single _48_Corona_Scale_i;
[Value(Name="Corona Scale j", Offset=52)]
public Single _52_Corona_Scale_j;
[Value(Name="Falloff Function", Offset=56)]
public H2.DataTypes.Enum _56_Falloff_Function = new H2.DataTypes.Enum(new string[] {  "Linear", "Late", "Very Late", "Early", "Very Early", "Cosine", "Zero", "One" }, 4);
public _60_reflections[] _60_Reflections;
[Serializable][Reflexive(Name="Reflections", Offset=60, ChunkSize=48, Label="")]
public class _60_reflections
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Align Rotation With Screen Center","Radius Not Scaled By Distance","Radius Scaled By Occlusion Factor","Occluded By Solid Objects","Ignore Light Color","Not Affected By Inner Occlusion" }, 2);
[Value(Name="Bitmap Index", Offset=2)]
public Int16 _2_Bitmap_Index;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
[Value(Name="Position Along Flare Axis", Offset=8)]
public Single _8_Position_Along_Flare_Axis;
[Value(Name="Rotation Offset", Offset=12)]
public Single _12_Rotation_Offset;
[Value(Name="Radius", Offset=16)]
public Single _16_Radius;
[Value(Name="...To", Offset=20)]
public Single _20____To;
[Value(Name="Brightness", Offset=24)]
public Single _24_Brightness;
[Value(Name="...To", Offset=28)]
public Single _28____To;
[Value(Name="Modulation Factor", Offset=32)]
public Single _32_Modulation_Factor;
[Value(Name="Color R", Offset=36)]
public Single _36_Color_R;
[Value(Name="Color G", Offset=40)]
public Single _40_Color_G;
[Value(Name="Color B", Offset=44)]
public Single _44_Color_B;
}
[Value(Name="Flags", Offset=68)]
public Bitmask _68_Flags = new Bitmask(new string[] { "Synchronized" }, 4);
public _72_brightness[] _72_Brightness;
[Serializable][Reflexive(Name="Brightness", Offset=72, ChunkSize=188, Label="")]
public class _72_brightness
{
private int __StartOffset__;
public _0_function[] _0_Function;
[Serializable][Reflexive(Name="Function", Offset=0, ChunkSize=1, Label="")]
public class _0_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=8)]
public Byte[] _8_ = new byte[180];
}
public _80_color[] _80_Color;
[Serializable][Reflexive(Name="Color", Offset=80, ChunkSize=188, Label="")]
public class _80_color
{
private int __StartOffset__;
public _0_function[] _0_Function;
[Serializable][Reflexive(Name="Function", Offset=0, ChunkSize=1, Label="")]
public class _0_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=8)]
public Byte[] _8_ = new byte[180];
}
public _88_rotation[] _88_Rotation;
[Serializable][Reflexive(Name="Rotation", Offset=88, ChunkSize=188, Label="")]
public class _88_rotation
{
private int __StartOffset__;
public _0_function[] _0_Function;
[Serializable][Reflexive(Name="Function", Offset=0, ChunkSize=1, Label="")]
public class _0_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=8)]
public Byte[] _8_ = new byte[180];
}
        }
    }
}