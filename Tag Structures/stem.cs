using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("stem")]
        public class stem
        {
public _0_documentation[] _0_Documentation;
[Serializable][Reflexive(Name="Documentation", Offset=0, ChunkSize=1, Label="")]
public class _0_documentation
{
private int __StartOffset__;
[Value(Name="Char Data", Offset=0)]
public Byte _0_Char_Data;
}
[Value(Name="Default Material Name", Offset=8)]
public StringID _8_Default_Material_Name;
[Value(Name="Flags", Offset=12)]
public Bitmask _12_Flags = new Bitmask(new string[] { "Force Active Camo","Water","Foliage","Hide Standard Parameters" }, 4);
[Value(Offset=16)]
public Byte[] _16_ = new byte[16];
[Value(Name="Light Response", Offset=32)]
public Dependancy _32_Light_Response;
[Value(Offset=36)]
public Byte[] _36_ = new byte[24];
[Value(Name="Aux 1 Shader", Offset=60)]
public Dependancy _60_Aux_1_Shader;
[Value(Name="Aux 1 Layer", Offset=64)]
public H2.DataTypes.Enum _64_Aux_1_Layer = new H2.DataTypes.Enum(new string[] {  "Texaccum", "Environment Map", "Self-Illumination", "Overlay", "Transparent", "Lightmap (Indirect)", "Diffuse", "Specular", "Shadow Generate", "Shadow Apply", "Boom", "Fog", "Sh Prt", "Active Camo", "Water Edge Blend", "Decal", "Active Camo Stencil Modulate", "Hologram", "Light Albedo" }, 4);
[Value(Name="Aux 2 Shader", Offset=68)]
public Dependancy _68_Aux_2_Shader;
[Value(Name="Aux 2 Layer", Offset=72)]
public H2.DataTypes.Enum _72_Aux_2_Layer = new H2.DataTypes.Enum(new string[] {  "Texaccum", "Environment Map", "Self-Illumination", "Overlay", "Transparent", "Lightmap (Indirect)", "Diffuse", "Specular", "Shadow Generate", "Shadow Apply", "Boom", "Fog", "Sh Prt", "Active Camo", "Water Edge Blend", "Decal", "Active Camo Stencil Modulate", "Hologram", "Light Albedo" }, 4);
public _76_postprocess_definition[] _76_Postprocess_Definition;
[Serializable][Reflexive(Name="Postprocess Definition", Offset=76, ChunkSize=40, Label="")]
public class _76_postprocess_definition
{
private int __StartOffset__;
public _0_levels_of_detail[] _0_Levels_Of_Detail;
[Serializable][Reflexive(Name="Levels Of Detail", Offset=0, ChunkSize=10, Label="")]
public class _0_levels_of_detail
{
private int __StartOffset__;
[Value(Name="Block Index Data", Offset=0)]
public Int16 _0_Block_Index_Data;
[Value(Name="Available Layers", Offset=2)]
public Int32 _2_Available_Layers;
[Value(Name="Projected Height Percentage", Offset=6)]
public Single _6_Projected_Height_Percentage;
}
public _8_layers[] _8_Layers;
[Serializable][Reflexive(Name="Layers", Offset=8, ChunkSize=2, Label="")]
public class _8_layers
{
private int __StartOffset__;
[Value(Name="Block Index Data", Offset=0)]
public Int16 _0_Block_Index_Data;
}
public _16_passes[] _16_Passes;
[Serializable][Reflexive(Name="Passes", Offset=16, ChunkSize=10, Label="")]
public class _16_passes
{
private int __StartOffset__;
[Value(Name="Pass", Offset=0)]
public Dependancy _0_Pass;
[Value(Name="Block Index Data", Offset=8)]
public Int16 _8_Block_Index_Data;
}
public _24_implementations[] _24_Implementations;
[Serializable][Reflexive(Name="Implementations", Offset=24, ChunkSize=6, Label="")]
public class _24_implementations
{
private int __StartOffset__;
[Value(Name="Block Index Data", Offset=0)]
public Int16 _0_Block_Index_Data;
[Value(Name="Block Index Data", Offset=2)]
public Int16 _2_Block_Index_Data;
[Value(Name="Block Index Data", Offset=4)]
public Int16 _4_Block_Index_Data;
}
public _32_remappings[] _32_Remappings;
[Serializable][Reflexive(Name="Remappings", Offset=32, ChunkSize=4, Label="")]
public class _32_remappings
{
private int __StartOffset__;
[Value(Name="Source Index", Offset=0)]
public Int16 _0_Source_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
}
        }
    }
}