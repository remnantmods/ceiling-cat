using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("bipd")]
        public class bipd
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Does Not Cast Shadow","Search Cardinal Direction Lightmaps","unused","Not A Pathfinding Obstacle","Extension Of Parent","Does Not Cause Collision Damage","Early Mover","Early Mover Localized Physics","Use Static Massive Lightmap Sample","Object Scales Attachments","Inherits Player's Appearance","Dead Bipeds Can't Localize","Attach To Clusters By Dynamic Sphere","Effects Created By This Object Do Not" }, 4);
[Value(Name="Bounding Radius", Offset=4)]
public Single _4_Bounding_Radius;
[Value(Name="Bounding Offset X", Offset=8)]
public Single _8_Bounding_Offset_X;
[Value(Name="Bounding Offset Y", Offset=12)]
public Single _12_Bounding_Offset_Y;
[Value(Name="Bounding Offset Z", Offset=16)]
public Single _16_Bounding_Offset_Z;
[Value(Name="Acceleration Scale", Offset=20)]
public Single _20_Acceleration_Scale;
[Value(Name="Lightmap Shadow Mode", Offset=24)]
public H2.DataTypes.Enum _24_Lightmap_Shadow_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Never", "Always" }, 4);
[Value(Name="Sweetener Size", Offset=28)]
public H2.DataTypes.Enum _28_Sweetener_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Medium", "Large" }, 4);
[Value(Name="Dynamic Light Sphere Radius", Offset=32)]
public Single _32_Dynamic_Light_Sphere_Radius;
[Value(Name="Dynamic Light Sphere Offset X", Offset=36)]
public Single _36_Dynamic_Light_Sphere_Offset_X;
[Value(Name="Dynamic Light Sphere Offset Y", Offset=40)]
public Single _40_Dynamic_Light_Sphere_Offset_Y;
[Value(Name="Dynamic Light Sphere Offset Z", Offset=44)]
public Single _44_Dynamic_Light_Sphere_Offset_Z;
[Value(Name="Default Model Variant", Offset=48)]
public StringID _48_Default_Model_Variant;
[Value(Name="Model", Offset=52)]
public Dependancy _52_Model;
[Value(Name="Crate Object", Offset=56)]
public Dependancy _56_Crate_Object;
[Value(Name="Modifier Shader", Offset=60)]
public Dependancy _60_Modifier_Shader;
[Value(Name="Creation Effect", Offset=64)]
public Dependancy _64_Creation_Effect;
[Value(Name="Material Effects", Offset=68)]
public Dependancy _68_Material_Effects;
public _72_ai_properties[] _72_Ai_Properties;
[Serializable][Reflexive(Name="Ai Properties", Offset=72, ChunkSize=16, Label="")]
public class _72_ai_properties
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Destroyable Cover","Pathfinding Ignore When Dead","Dynamic Cover" }, 4);
[Value(Name="Type Name", Offset=4)]
public StringID _4_Type_Name;
[Value(Name="Size", Offset=8)]
public H2.DataTypes.Enum _8_Size = new H2.DataTypes.Enum(new string[] {  "Default", "Tiny", "Small", "Medium", "Large", "Huge", "Immobile" }, 4);
[Value(Name="Leap Jump Speed", Offset=12)]
public H2.DataTypes.Enum _12_Leap_Jump_Speed = new H2.DataTypes.Enum(new string[] {  "None", "Down", "Step", "Crouch", "Stand", "Storey", "Tower", "Infinite" }, 4);
}
public _80_functions[] _80_Functions;
[Serializable][Reflexive(Name="Functions", Offset=80, ChunkSize=32, Label="")]
public class _80_functions
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invert","Mapping Does Not Controls Active","Always Active","Random Time Offset" }, 4);
[Value(Name="Import Name", Offset=4)]
public StringID _4_Import_Name;
[Value(Name="Export Name", Offset=8)]
public StringID _8_Export_Name;
[Value(Name="Turn Off With", Offset=12)]
public StringID _12_Turn_Off_With;
[Value(Name="Min Value", Offset=16)]
public Single _16_Min_Value;
public _20_function_type__graph_[] _20_Function_Type__Graph_;
[Serializable][Reflexive(Name="Function Type (Graph)", Offset=20, ChunkSize=1, Label="")]
public class _20_function_type__graph_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Scale By", Offset=28)]
public StringID _28_Scale_By;
}
[Value(Name="Apply Collision Damage Scale", Offset=88)]
public Single _88_Apply_Collision_Damage_Scale;
[Value(Name="Min Game Acc (Default)", Offset=92)]
public Single _92_Min_Game_Acc__Default_;
[Value(Name="Max Game Acc (Default)", Offset=96)]
public Single _96_Max_Game_Acc__Default_;
[Value(Name="Min Gam Scale (Default)", Offset=100)]
public Single _100_Min_Gam_Scale__Default_;
[Value(Name="Max Gam Scale (Default)", Offset=104)]
public Single _104_Max_Gam_Scale__Default_;
[Value(Name="Min abs Acc (Default)", Offset=108)]
public Single _108_Min_abs_Acc__Default_;
[Value(Name="Max abs Acc (Default)", Offset=112)]
public Single _112_Max_abs_Acc__Default_;
[Value(Name="Min abs Scale (Default)", Offset=116)]
public Single _116_Min_abs_Scale__Default_;
[Value(Name="Max abs Scale (Default)", Offset=120)]
public Single _120_Max_abs_Scale__Default_;
[Value(Name="Hud Text Message Index", Offset=124)]
public Int16 _124_Hud_Text_Message_Index;
[Value(Offset=126)]
public Byte[] _126_ = new byte[2];
public _128_attachments[] _128_Attachments;
[Serializable][Reflexive(Name="Attachments", Offset=128, ChunkSize=24, Label="")]
public class _128_attachments
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
[Value(Name="Marker", Offset=8)]
public StringID _8_Marker;
[Value(Name="Change Color", Offset=12)]
public StringID _12_Change_Color;
[Value(Name="Primary Scale", Offset=16)]
public StringID _16_Primary_Scale;
[Value(Name="Secondary Scale", Offset=20)]
public StringID _20_Secondary_Scale;
}
public _136_widgets[] _136_Widgets;
[Serializable][Reflexive(Name="Widgets", Offset=136, ChunkSize=8, Label="")]
public class _136_widgets
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
}
public _144_old_functions[] _144_Old_Functions;
[Serializable][Reflexive(Name="Old Functions", Offset=144, ChunkSize=0, Label="")]
public class _144_old_functions
{
private int __StartOffset__;
}
public _152_change_colors[] _152_Change_Colors;
[Serializable][Reflexive(Name="Change Colors", Offset=152, ChunkSize=16, Label="")]
public class _152_change_colors
{
private int __StartOffset__;
public _0_initial_permutations[] _0_Initial_Permutations;
[Serializable][Reflexive(Name="Initial Permutations", Offset=0, ChunkSize=32, Label="")]
public class _0_initial_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Variant Name", Offset=28)]
public StringID _28_Variant_Name;
}
public _8_functions[] _8_Functions;
[Serializable][Reflexive(Name="Functions", Offset=8, ChunkSize=36, Label="")]
public class _8_functions
{
private int __StartOffset__;
[Value(Name="Scale Flags", Offset=0)]
public Bitmask _0_Scale_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Darken By", Offset=28)]
public StringID _28_Darken_By;
[Value(Name="Scale By", Offset=32)]
public StringID _32_Scale_By;
}
}
public _160_predicted_resource[] _160_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=160, ChunkSize=8, Label="")]
public class _160_predicted_resource
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Int16 _0_Type;
[Value(Name="Resource Index", Offset=2)]
public Int16 _2_Resource_Index;
[Value(Name="Tag Index", Offset=4)]
public Int32 _4_Tag_Index;
}
[Value(Name="Flags", Offset=168)]
public Bitmask _168_Flags = new Bitmask(new string[] { "Circular Aiming","Destroyed After Dying","Half-speed Interpolation","Fires From Camera","Entrance Inside Bounding sphere ","Doesn't Show Readied Weapon","Couses Passenger Dialogue","Resists Pings","Melee Attack Is Fatal","Don't Reface During Pings","Has No Aiming","Simple Creature","Impact Melle Attaches To Unit","Impact Melee Dies On Shield","Cannot Open Doors Automatically","Melee Attackers Cannot Attach","Not Instantly Killed By Melee","Shield Sapping","Runs Around Flaming","Inconsequential","Special Cinematic Unit","Ignored By AutoAiming","Shields Fry Infection Forms","unused","unused","Acts As Gunner For Parent","Controlled By Parent Gunner","Parent's Primary Weapon","Unit Has Boost" }, 4);
[Value(Name="Default Team", Offset=172)]
public H2.DataTypes.Enum _172_Default_Team = new H2.DataTypes.Enum(new string[] {  "Deafult", "Player", "Human", "Covenant", "Flood", "Sentinel", "Heretic", "Prophet" }, 2);
[Value(Name="Constant Sound Volume", Offset=174)]
public H2.DataTypes.Enum _174_Constant_Sound_Volume = new H2.DataTypes.Enum(new string[] {  "Silent", "Medium", "Loud", "Shout", "Quiet" }, 2);
[Value(Name="Integrated Light Toggle", Offset=176)]
public Dependancy _176_Integrated_Light_Toggle;
[Value(Name="Camera Field of View", Offset=180)]
public Single _180_Camera_Field_of_View;
[Value(Name="Camera Stiffness", Offset=184)]
public Single _184_Camera_Stiffness;
[Value(Name="Camera Marker Name", Offset=188)]
public StringID _188_Camera_Marker_Name;
[Value(Name="Camera Submerged Marker Name", Offset=192)]
public StringID _192_Camera_Submerged_Marker_Name;
[Value(Name="Pitch Auto-Level", Offset=196)]
public Single _196_Pitch_Auto_Level;
[Value(Name="Pitch Range", Offset=200)]
public Single _200_Pitch_Range;
[Value(Name="...To", Offset=204)]
public Single _204____To;
public _208_camera_tracks[] _208_Camera_Tracks;
[Serializable][Reflexive(Name="Camera Tracks", Offset=208, ChunkSize=8, Label="")]
public class _208_camera_tracks
{
private int __StartOffset__;
[Value(Name="Track", Offset=0)]
public Dependancy _0_Track;
}
[Value(Name="Acceleration Scale i", Offset=216)]
public Single _216_Acceleration_Scale_i;
[Value(Name="Acceleration Scale j", Offset=220)]
public Single _220_Acceleration_Scale_j;
[Value(Name="Acceleration Scale k", Offset=224)]
public Single _224_Acceleration_Scale_k;
[Value(Name="Acceleration Action Scale", Offset=228)]
public Single _228_Acceleration_Action_Scale;
[Value(Name="Acceleration Attach Scale", Offset=232)]
public Single _232_Acceleration_Attach_Scale;
[Value(Name="Soft Ping Threshold", Offset=236)]
public Single _236_Soft_Ping_Threshold;
[Value(Name="Soft Ping Interrupt Time", Offset=240)]
public Single _240_Soft_Ping_Interrupt_Time;
[Value(Name="Hard Ping Threshold", Offset=244)]
public Single _244_Hard_Ping_Threshold;
[Value(Name="Hard Ping Interrupt Time", Offset=248)]
public Single _248_Hard_Ping_Interrupt_Time;
[Value(Name="Hard Ping Death Threshold", Offset=252)]
public Single _252_Hard_Ping_Death_Threshold;
[Value(Name="Feign Death Threshold", Offset=256)]
public Single _256_Feign_Death_Threshold;
[Value(Name="Feign Death Time", Offset=260)]
public Single _260_Feign_Death_Time;
[Value(Name="Dist Of Evade Anim", Offset=264)]
public Single _264_Dist_Of_Evade_Anim;
[Value(Name="Dist of Dive Anim", Offset=268)]
public Single _268_Dist_of_Dive_Anim;
[Value(Name="Stunned Movement Threshold", Offset=272)]
public Single _272_Stunned_Movement_Threshold;
[Value(Name="Feign Death Chance", Offset=276)]
public Single _276_Feign_Death_Chance;
[Value(Name="Feign Repeat Chance", Offset=280)]
public Single _280_Feign_Repeat_Chance;
[Value(Name="Spawned Turret Actor", Offset=284)]
public Dependancy _284_Spawned_Turret_Actor;
[Value(Name="Spawned Actor Count", Offset=288)]
public Int16 _288_Spawned_Actor_Count;
[Value(Name="...To", Offset=290)]
public Int16 _290____To;
[Value(Name="Spawned Velocity", Offset=292)]
public Single _292_Spawned_Velocity;
[Value(Name="Aiming Velocity Max", Offset=296)]
public Single _296_Aiming_Velocity_Max;
[Value(Name="Aiming Accel Max", Offset=300)]
public Single _300_Aiming_Accel_Max;
[Value(Name="Casual Aiming Modifier", Offset=304)]
public Single _304_Casual_Aiming_Modifier;
[Value(Name="Looking Velocity Max", Offset=308)]
public Single _308_Looking_Velocity_Max;
[Value(Name="Looking Accel Max", Offset=312)]
public Single _312_Looking_Accel_Max;
[Value(Name="Right Hand Node", Offset=316)]
public StringID _316_Right_Hand_Node;
[Value(Name="Left Hand Node", Offset=320)]
public StringID _320_Left_Hand_Node;
[Value(Name="Preferred Gun Node", Offset=324)]
public StringID _324_Preferred_Gun_Node;
[Value(Name="Melee Damage", Offset=328)]
public Dependancy _328_Melee_Damage;
[Value(Name="Boarding Melee Damage", Offset=332)]
public Dependancy _332_Boarding_Melee_Damage;
[Value(Name="Boarding Melee Response", Offset=336)]
public Dependancy _336_Boarding_Melee_Response;
[Value(Name="Landing Melee Damage", Offset=340)]
public Dependancy _340_Landing_Melee_Damage;
[Value(Name="Flurry Melee Damage", Offset=344)]
public Dependancy _344_Flurry_Melee_Damage;
[Value(Name="Obstacle Smash Damage", Offset=348)]
public Dependancy _348_Obstacle_Smash_Damage;
[Value(Name="Motion Sensor Blip Size", Offset=352)]
public H2.DataTypes.Enum _352_Motion_Sensor_Blip_Size = new H2.DataTypes.Enum(new string[] {  "Medium", "Small", "Large" }, 4);
public _356_postures[] _356_Postures;
[Serializable][Reflexive(Name="Postures", Offset=356, ChunkSize=16, Label="")]
public class _356_postures
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Pill Offset i", Offset=4)]
public Single _4_Pill_Offset_i;
[Value(Name="Pill Offset j", Offset=8)]
public Single _8_Pill_Offset_j;
[Value(Name="Pill Offset k", Offset=12)]
public Single _12_Pill_Offset_k;
}
public _364_new_hud_interfaces[] _364_New_Hud_Interfaces;
[Serializable][Reflexive(Name="New Hud Interfaces", Offset=364, ChunkSize=8, Label="")]
public class _364_new_hud_interfaces
{
private int __StartOffset__;
[Value(Name="New Unit Hud Interface", Offset=0)]
public Dependancy _0_New_Unit_Hud_Interface;
}
public _372_dialogue_variants[] _372_Dialogue_Variants;
[Serializable][Reflexive(Name="Dialogue Variants", Offset=372, ChunkSize=24, Label="")]
public class _372_dialogue_variants
{
private int __StartOffset__;
[Value(Name="Variant Number", Offset=0)]
public Int16 _0_Variant_Number;
[Value(Offset=2)]
public Byte[] _2_ = new byte[10];
[Value(Name="Dialogue", Offset=12)]
public Dependancy _12_Dialogue;
[Value(Offset=20)]
public Byte[] _20_ = new byte[4];
}
[Value(Name="Grenade Velocity", Offset=380)]
public Single _380_Grenade_Velocity;
[Value(Name="Grenade Type", Offset=384)]
public H2.DataTypes.Enum _384_Grenade_Type = new H2.DataTypes.Enum(new string[] {  "Human Fragmentation", "Covenant Plasma" }, 2);
[Value(Name="Grenade Count", Offset=386)]
public UInt16 _386_Grenade_Count;
public _388_powered_seats[] _388_Powered_Seats;
[Serializable][Reflexive(Name="Powered Seats", Offset=388, ChunkSize=8, Label="")]
public class _388_powered_seats
{
private int __StartOffset__;
[Value(Name="Driver Powerup Time", Offset=0)]
public Single _0_Driver_Powerup_Time;
[Value(Name="Driver Powerdown Time", Offset=4)]
public Single _4_Driver_Powerdown_Time;
}
public _396_weapon[] _396_Weapon;
[Serializable][Reflexive(Name="Weapon", Offset=396, ChunkSize=8, Label="")]
public class _396_weapon
{
private int __StartOffset__;
[Value(Name="Weapon", Offset=0)]
public Dependancy _0_Weapon;
}
public _404_seats[] _404_Seats;
[Serializable][Reflexive(Name="Seats", Offset=404, ChunkSize=176, Label="")]
public class _404_seats
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invisible","Locked","Driver","Gunner","3rd Person Camera","Allows Weapons","3rd Person On Enter","1st Person Camera Slaved To Gun","Allow Vehicle Communication Animations","Not Valid Without Driver","Allow AI NonCombatants","Boarding Seat","AI Firing Disabled By Max Acceleration","Boarding Enters Seat","Boarding Need Any Passenger","Invaild For Player","Invaild For Non-Player","Gunner (Player Only)","Invisible Under Major Damage" }, 4);
[Value(Name="Label", Offset=4)]
public StringID _4_Label;
[Value(Name="Sitting Postion Marker", Offset=8)]
public StringID _8_Sitting_Postion_Marker;
[Value(Name="Entry Marker(s) Name", Offset=12)]
public StringID _12_Entry_Marker_s__Name;
[Value(Name="Boarding Grenade Marker", Offset=16)]
public StringID _16_Boarding_Grenade_Marker;
[Value(Name="Boarding Grenade String", Offset=20)]
public StringID _20_Boarding_Grenade_String;
[Value(Name="Boarding Melee String", Offset=24)]
public StringID _24_Boarding_Melee_String;
[Value(Name="Ping Scale", Offset=28)]
public Single _28_Ping_Scale;
[Value(Name="Turnover Time (sec)", Offset=32)]
public Single _32_Turnover_Time__sec_;
[Value(Name="Accel Range i", Offset=36)]
public Single _36_Accel_Range_i;
[Value(Name="Accel Range j", Offset=40)]
public Single _40_Accel_Range_j;
[Value(Name="Accel Range k", Offset=44)]
public Single _44_Accel_Range_k;
[Value(Name="Accel Action Scale", Offset=48)]
public Single _48_Accel_Action_Scale;
[Value(Name="Accel Attach Scale", Offset=52)]
public Single _52_Accel_Attach_Scale;
[Value(Name="AI Scariness", Offset=56)]
public Single _56_AI_Scariness;
[Value(Name="AI Seat Type", Offset=60)]
public H2.DataTypes.Enum _60_AI_Seat_Type = new H2.DataTypes.Enum(new string[] {  "None", "Passenger", "Gunner", "Small Cargo", "Large Cargo", "Driver" }, 2);
[Value(Name="Boarding Seat #", Offset=62)]
public Int16 _62_Boarding_Seat_No;
[Value(Name="Listener Interpolation Factor", Offset=64)]
public Single _64_Listener_Interpolation_Factor;
[Value(Name="Yaw Rate Bounds", Offset=68)]
public Single _68_Yaw_Rate_Bounds;
[Value(Name="...To", Offset=72)]
public Single _72____To;
[Value(Name="Pitch Rate Bounds", Offset=76)]
public Single _76_Pitch_Rate_Bounds;
[Value(Name="...To", Offset=80)]
public Single _80____To;
[Value(Name="Min Speed Ref", Offset=84)]
public Single _84_Min_Speed_Ref;
[Value(Name="Max Speed Ref", Offset=88)]
public Single _88_Max_Speed_Ref;
[Value(Name="Speed Exponent", Offset=92)]
public Single _92_Speed_Exponent;
[Value(Name="Camera Marker Name", Offset=96)]
public StringID _96_Camera_Marker_Name;
[Value(Name="Camera Submerged Marker name", Offset=100)]
public StringID _100_Camera_Submerged_Marker_name;
[Value(Name="Pitch Auto-level", Offset=104)]
public Single _104_Pitch_Auto_level;
[Value(Name="Pitch Range Lower", Offset=108)]
public Single _108_Pitch_Range_Lower;
[Value(Name="Pitch Range Upper", Offset=112)]
public Single _112_Pitch_Range_Upper;
public _116_camera_tracks[] _116_Camera_Tracks;
[Serializable][Reflexive(Name="Camera Tracks", Offset=116, ChunkSize=8, Label="")]
public class _116_camera_tracks
{
private int __StartOffset__;
[Value(Name="Track", Offset=0)]
public Dependancy _0_Track;
}
public _124_unit_hud_interface[] _124_Unit_Hud_Interface;
[Serializable][Reflexive(Name="Unit Hud Interface", Offset=124, ChunkSize=8, Label="")]
public class _124_unit_hud_interface
{
private int __StartOffset__;
[Value(Name="New Unit Hud Interface", Offset=0)]
public Dependancy _0_New_Unit_Hud_Interface;
}
[Value(Name="Enter Seat String", Offset=132)]
public StringID _132_Enter_Seat_String;
[Value(Name="Yaw Min", Offset=136)]
public Single _136_Yaw_Min;
[Value(Name="Yaw Max", Offset=140)]
public Single _140_Yaw_Max;
[Value(Name="Built-in Gunner", Offset=144)]
public Dependancy _144_Built_in_Gunner;
[Value(Name="Entry Radius", Offset=152)]
public Single _152_Entry_Radius;
[Value(Name="Entry Marker Cone Angle", Offset=156)]
public Single _156_Entry_Marker_Cone_Angle;
[Value(Name="Entry Marker Facing Angle", Offset=160)]
public Single _160_Entry_Marker_Facing_Angle;
[Value(Name="Maximum Relative Velocity", Offset=164)]
public Single _164_Maximum_Relative_Velocity;
[Value(Name="Invisible Seat Region", Offset=168)]
public StringID _168_Invisible_Seat_Region;
[Value(Name="Runtime Invisible Seat Region Index", Offset=172)]
public Single _172_Runtime_Invisible_Seat_Region_Index;
}
[Value(Name="Boost Peak Power", Offset=412)]
public Single _412_Boost_Peak_Power;
[Value(Name="Boost Rise Power", Offset=416)]
public Single _416_Boost_Rise_Power;
[Value(Name="Boost Peak Time", Offset=420)]
public Single _420_Boost_Peak_Time;
[Value(Name="Boost Fall Power", Offset=424)]
public Single _424_Boost_Fall_Power;
[Value(Name="Dead Time", Offset=428)]
public Single _428_Dead_Time;
[Value(Name="Lip Sync Attack Weight", Offset=432)]
public Single _432_Lip_Sync_Attack_Weight;
[Value(Name="Lip Sync decay Weight", Offset=436)]
public Single _436_Lip_Sync_decay_Weight;
[Value(Name="Moving Turning Speed (Degrees Per Second)", Offset=440)]
public Single _440_Moving_Turning_Speed__Degrees_Per_Second_;
[Value(Name="Flags", Offset=444)]
public Bitmask _444_Flags = new Bitmask(new string[] { "Turns Without Animating","Passes Through Other Bipeds","Immune To Falling Damage","Rotate While Airborne","Use Limp Body Physics","unused","Random Speed Increase","unused","Spawn Death Children On Destroy","Stunned by emp damage(RD1)","Dead Physics When Stunned(RD2)","Always Ragdoll When Dead(RD3)" }, 4);
[Value(Name="Stationary Turning Threshold", Offset=448)]
public Single _448_Stationary_Turning_Threshold;
[Value(Name="Jump Velocity", Offset=452)]
public Single _452_Jump_Velocity;
[Value(Name="Max Soft Landing Time", Offset=456)]
public Single _456_Max_Soft_Landing_Time;
[Value(Name="Max Hard Landing Time", Offset=460)]
public Single _460_Max_Hard_Landing_Time;
[Value(Name="Min Soft Landing Velocity", Offset=464)]
public Single _464_Min_Soft_Landing_Velocity;
[Value(Name="Min Hard Landing Velocity", Offset=468)]
public Single _468_Min_Hard_Landing_Velocity;
[Value(Name="Max Hard Landing Velocity", Offset=472)]
public Single _472_Max_Hard_Landing_Velocity;
[Value(Name="Death Hard Landing Velocity", Offset=476)]
public Single _476_Death_Hard_Landing_Velocity;
[Value(Name="Stun Duration", Offset=480)]
public Single _480_Stun_Duration;
[Value(Name="Standing Camera Height", Offset=484)]
public Single _484_Standing_Camera_Height;
[Value(Name="Crouching Camera Height", Offset=488)]
public Single _488_Crouching_Camera_Height;
[Value(Name="Crouch Trasition Time", Offset=492)]
public Single _492_Crouch_Trasition_Time;
[Value(Name="Camera Interpolation Start", Offset=496)]
public Single _496_Camera_Interpolation_Start;
[Value(Name="Camera Interpolation End", Offset=500)]
public Single _500_Camera_Interpolation_End;
[Value(Name="Camera Forward Movement Scale", Offset=504)]
public Single _504_Camera_Forward_Movement_Scale;
[Value(Name="Camera Side Movement Scale", Offset=508)]
public Single _508_Camera_Side_Movement_Scale;
[Value(Name="Camera Vertical Movement Scale", Offset=512)]
public Single _512_Camera_Vertical_Movement_Scale;
[Value(Name="Camera Exclusion Distance", Offset=516)]
public Single _516_Camera_Exclusion_Distance;
[Value(Name="Autoaim Width", Offset=520)]
public Single _520_Autoaim_Width;
[Value(Name="Flags", Offset=524)]
public Bitmask _524_Flags = new Bitmask(new string[] { "Locked By Human Targeting","Locked By Plasma Targeting","Always Locked By Human Targeting" }, 4);
[Value(Name="Lock On Distance", Offset=528)]
public Single _528_Lock_On_Distance;
[Value(Offset=532)]
public Byte[] _532_ = new byte[16];
[Value(Name="Head Shot Acc Scale", Offset=548)]
public Single _548_Head_Shot_Acc_Scale;
[Value(Name="Area Damage Effect", Offset=552)]
public Dependancy _552_Area_Damage_Effect;
[Value(Name="Flags", Offset=556)]
public Bitmask _556_Flags = new Bitmask(new string[] { "Centered At Origin","Shape Sperical","Use Player Physics","Climb Any Surface","Flying","Not Physical","Dead Character Collision Group" }, 4);
[Value(Name="Height Standing", Offset=560)]
public Single _560_Height_Standing;
[Value(Name="Height Crouching", Offset=564)]
public Single _564_Height_Crouching;
[Value(Name="Radius", Offset=568)]
public Single _568_Radius;
[Value(Name="Mass", Offset=572)]
public Single _572_Mass;
[Value(Name="Living Material Name", Offset=576)]
public StringID _576_Living_Material_Name;
[Value(Name="Dead Material Name", Offset=580)]
public StringID _580_Dead_Material_Name;
[Value(Offset=584)]
public Byte[] _584_ = new byte[4];
public _588_dead_sphere_shapes[] _588_Dead_Sphere_Shapes;
[Serializable][Reflexive(Name="Dead Sphere Shapes", Offset=588, ChunkSize=128, Label="")]
public class _588_dead_sphere_shapes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Material #", Offset=4)]
public Int16 _4_Material_No;
[Value(Name="Flags", Offset=6)]
public H2.DataTypes.Enum _6_Flags = new H2.DataTypes.Enum(new string[] {  "unused" }, 2);
[Value(Name="Relative Mass Scale", Offset=8)]
public Single _8_Relative_Mass_Scale;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Restitution", Offset=16)]
public Single _16_Restitution;
[Value(Name="Volume", Offset=20)]
public Single _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Phantom", Offset=28)]
public Int16 _28_Phantom;
[Value(Name="Size", Offset=30)]
public Int16 _30_Size;
[Value(Name="Count", Offset=32)]
public Int16 _32_Count;
[Value(Name="Radius", Offset=34)]
public Single _34_Radius;
[Value(Name="Size", Offset=38)]
public Int16 _38_Size;
[Value(Name="Count", Offset=40)]
public Int16 _40_Count;
[Value(Name="Rotation i i", Offset=42)]
public Single _42_Rotation_i_i;
[Value(Name="Rotation i j", Offset=46)]
public Single _46_Rotation_i_j;
[Value(Name="Rotation i k", Offset=50)]
public Single _50_Rotation_i_k;
[Value(Name="Rotation j i", Offset=54)]
public Single _54_Rotation_j_i;
[Value(Name="Rotation j j", Offset=58)]
public Single _58_Rotation_j_j;
[Value(Name="Rotation j k", Offset=62)]
public Single _62_Rotation_j_k;
[Value(Name="Rotation k i", Offset=66)]
public Single _66_Rotation_k_i;
[Value(Name="Rotation k j", Offset=70)]
public Single _70_Rotation_k_j;
[Value(Name="Rotation k k", Offset=74)]
public Single _74_Rotation_k_k;
[Value(Name="Translation i", Offset=78)]
public Single _78_Translation_i;
[Value(Name="Translation j", Offset=82)]
public Single _82_Translation_j;
[Value(Name="Translation k", Offset=86)]
public Single _86_Translation_k;
[Value(Offset=90)]
public Byte[] _90_ = new byte[38];
}
public _596_pill_shapes[] _596_Pill_Shapes;
[Serializable][Reflexive(Name="Pill Shapes", Offset=596, ChunkSize=80, Label="")]
public class _596_pill_shapes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Material #", Offset=4)]
public Int16 _4_Material_No;
[Value(Name="Flags", Offset=6)]
public H2.DataTypes.Enum _6_Flags = new H2.DataTypes.Enum(new string[] {  "unused" }, 2);
[Value(Name="Relative Mass Scale", Offset=8)]
public Single _8_Relative_Mass_Scale;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Restitution", Offset=16)]
public Single _16_Restitution;
[Value(Name="Volume", Offset=20)]
public Single _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Phantom", Offset=28)]
public Int16 _28_Phantom;
[Value(Name="Size", Offset=30)]
public Int16 _30_Size;
[Value(Name="Count", Offset=32)]
public Int16 _32_Count;
[Value(Name="Radius", Offset=34)]
public Single _34_Radius;
[Value(Name="Size", Offset=38)]
public Int16 _38_Size;
[Value(Name="Count", Offset=40)]
public Int16 _40_Count;
[Value(Name="Bottom i", Offset=42)]
public Single _42_Bottom_i;
[Value(Name="Bottom j", Offset=46)]
public Single _46_Bottom_j;
[Value(Name="Bottom k", Offset=50)]
public Single _50_Bottom_k;
[Value(Name="Top i", Offset=54)]
public Single _54_Top_i;
[Value(Name="Top j", Offset=58)]
public Single _58_Top_j;
[Value(Name="Top k", Offset=62)]
public Single _62_Top_k;
[Value(Offset=66)]
public Byte[] _66_ = new byte[14];
}
public _604_sphere_shapes[] _604_Sphere_Shapes;
[Serializable][Reflexive(Name="Sphere Shapes", Offset=604, ChunkSize=128, Label="")]
public class _604_sphere_shapes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Material #", Offset=4)]
public Int16 _4_Material_No;
[Value(Name="Flags", Offset=6)]
public H2.DataTypes.Enum _6_Flags = new H2.DataTypes.Enum(new string[] {  "unused" }, 2);
[Value(Name="Relative Mass Scale", Offset=8)]
public Single _8_Relative_Mass_Scale;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Restitution", Offset=16)]
public Single _16_Restitution;
[Value(Name="Volume", Offset=20)]
public Single _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Phantom", Offset=28)]
public Int16 _28_Phantom;
[Value(Name="Size", Offset=30)]
public Int16 _30_Size;
[Value(Name="Count", Offset=32)]
public Int16 _32_Count;
[Value(Name="Radius", Offset=34)]
public Single _34_Radius;
[Value(Name="Size", Offset=38)]
public Int16 _38_Size;
[Value(Name="Count", Offset=40)]
public Int16 _40_Count;
[Value(Name="Rotation i i", Offset=42)]
public Single _42_Rotation_i_i;
[Value(Name="Rotation i j", Offset=46)]
public Single _46_Rotation_i_j;
[Value(Name="Rotation i k", Offset=50)]
public Single _50_Rotation_i_k;
[Value(Name="Rotation j i", Offset=54)]
public Single _54_Rotation_j_i;
[Value(Name="Rotation j j", Offset=58)]
public Single _58_Rotation_j_j;
[Value(Name="Rotation j k", Offset=62)]
public Single _62_Rotation_j_k;
[Value(Name="Rotation k i", Offset=66)]
public Single _66_Rotation_k_i;
[Value(Name="Rotation k j", Offset=70)]
public Single _70_Rotation_k_j;
[Value(Name="Rotation k k", Offset=74)]
public Single _74_Rotation_k_k;
[Value(Name="Translation i", Offset=78)]
public Single _78_Translation_i;
[Value(Name="Translation j", Offset=82)]
public Single _82_Translation_j;
[Value(Name="Translation  k", Offset=86)]
public Single _86_Translation__k;
[Value(Offset=90)]
public Byte[] _90_ = new byte[38];
}
[Value(Name="Max Slope Angle", Offset=612)]
public Single _612_Max_Slope_Angle;
[Value(Name="Downhill Falloff Angle", Offset=616)]
public Single _616_Downhill_Falloff_Angle;
[Value(Name="Downhill Cutoff Angle", Offset=620)]
public Single _620_Downhill_Cutoff_Angle;
[Value(Name="Uphill Falloff Angle", Offset=624)]
public Single _624_Uphill_Falloff_Angle;
[Value(Name="Uphill Cutoff Angle", Offset=628)]
public Single _628_Uphill_Cutoff_Angle;
[Value(Name="Downhill Velocity Angle", Offset=632)]
public Single _632_Downhill_Velocity_Angle;
[Value(Name="Uphill Velocity Scale", Offset=636)]
public Single _636_Uphill_Velocity_Scale;
[Value(Name="Flying Bank Angle", Offset=640)]
public Single _640_Flying_Bank_Angle;
[Value(Name="Flying Bank Apply Time", Offset=644)]
public Single _644_Flying_Bank_Apply_Time;
[Value(Name="Flying Pitch Ratio", Offset=648)]
public Single _648_Flying_Pitch_Ratio;
[Value(Name="Flying Max Velocity", Offset=652)]
public Single _652_Flying_Max_Velocity;
[Value(Name="Flying Max Sidestep Velocity", Offset=656)]
public Single _656_Flying_Max_Sidestep_Velocity;
[Value(Name="Flying Acceleration", Offset=660)]
public Single _660_Flying_Acceleration;
[Value(Name="Flying Deceleration", Offset=664)]
public Single _664_Flying_Deceleration;
[Value(Name="Flying Angular Velocity Max", Offset=668)]
public Single _668_Flying_Angular_Velocity_Max;
[Value(Name="Flying Angular Acceleration Max", Offset=672)]
public Single _672_Flying_Angular_Acceleration_Max;
[Value(Name="Flying Crouch Velocity Modifier", Offset=676)]
public Single _676_Flying_Crouch_Velocity_Modifier;
[Value(Offset=680)]
public Byte[] _680_ = new byte[24];
public _704_contact_points[] _704_Contact_Points;
[Serializable][Reflexive(Name="Contact Points", Offset=704, ChunkSize=4, Label="")]
public class _704_contact_points
{
private int __StartOffset__;
[Value(Name="Marker", Offset=0)]
public StringID _0_Marker;
}
[Value(Name="Reanimation Character", Offset=712)]
public Dependancy _712_Reanimation_Character;
[Value(Name="Death Spawn Character", Offset=716)]
public Dependancy _716_Death_Spawn_Character;
[Value(Name="Spawn Count", Offset=720)]
public Int16 _720_Spawn_Count;
[Value(Offset=722)]
public Byte[] _722_ = new byte[2];
        }
    }
}