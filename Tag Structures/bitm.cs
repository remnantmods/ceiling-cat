using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("bitm")]
        public class bitm
        {
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  "2D Textures", "3D Textures", "CubeMap", "Sprite", "UI Bitmap" }, 2);
[Value(Name="Format", Offset=2)]
public H2.DataTypes.Enum _2_Format = new H2.DataTypes.Enum(new string[] {  "DXT1 Encoded", "DXT2/3 Encoded", "DXT4/5 Encoded", "16-Bit Colour", "32-Bit Colour", "Monochrome" }, 2);
[Value(Name="Usage", Offset=4)]
public H2.DataTypes.Enum _4_Usage = new H2.DataTypes.Enum(new string[] {  "Alpha Blend", "Default", "Height Map", "Light Map", "Vector Map", "Height Map BLUE 255", "embm", "Height Map A8L8", "Height Map G8B8", "Height Map G8B8 /w Alpha" }, 2);
[Value(Name="Format", Offset=6)]
public Bitmask _6_Format = new Bitmask(new string[] { "Enable Diffusion Dithering","Disable Height map compression","Uniform Sprite Sequnces","Filthy Sprite Bugfix","Use Sharp Bump Filter","unused","Use Clamped/Mirrored Bump Filter","Invert Detail Fade","Swap x-y Vector Components","Convert From Signed","Convert To Signed","Import Mipmap Chains","Internationally True Color" }, 2);
[Value(Name="Detail Fade Factor", Offset=8)]
public Single _8_Detail_Fade_Factor;
[Value(Name="Sharpen Amount", Offset=12)]
public Single _12_Sharpen_Amount;
[Value(Name="Bump Height", Offset=16)]
public Single _16_Bump_Height;
[Value(Name="Sprite Budget Size", Offset=20)]
public H2.DataTypes.Enum _20_Sprite_Budget_Size = new H2.DataTypes.Enum(new string[] {  "32x32", "64x64", "128x128", "256x256", "512x512" }, 2);
[Value(Name="Sprite Budget Count", Offset=22)]
public Int16 _22_Sprite_Budget_Count;
[Value(Name="Colour Plate Width", Offset=24)]
public Int16 _24_Colour_Plate_Width;
[Value(Name="Colour Plate Height", Offset=26)]
public Int16 _26_Colour_Plate_Height;
[Value(Name="Compressed Colour Plate Data", Offset=28)]
public Int32 _28_Compressed_Colour_Plate_Data;
[Value(Offset=32)]
public Byte[] _32_ = new byte[12];
[Value(Name="Blur Filter Size", Offset=44)]
public Single _44_Blur_Filter_Size;
[Value(Name="Alpha Bias", Offset=48)]
public Single _48_Alpha_Bias;
[Value(Name="MipMap Count", Offset=52)]
public Int16 _52_MipMap_Count;
[Value(Name="Sprite Usage", Offset=54)]
public H2.DataTypes.Enum _54_Sprite_Usage = new H2.DataTypes.Enum(new string[] {  "Blend/Add/Subtract/Max", "Multiply/Min", "Double Multiply" }, 2);
[Value(Name="Sprite Spacing", Offset=56)]
public Int16 _56_Sprite_Spacing;
[Value(Name="Face Format", Offset=58)]
public H2.DataTypes.Enum _58_Face_Format = new H2.DataTypes.Enum(new string[] {  "Default", "Force G8B8", "Force DXT1", "Force DXT3", "Force DXT5", "Force Alpha-Luminance8", "Force A4R4G4B4" }, 2);
public _60_squences[] _60_Squences;
[Serializable][Reflexive(Name="Squences", Offset=60, ChunkSize=60, Label="")]
public class _60_squences
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="First Bitmap Index", Offset=32)]
public Int16 _32_First_Bitmap_Index;
[Value(Name="Bitmap Count", Offset=34)]
public Int16 _34_Bitmap_Count;
[Value(Offset=36)]
public Byte[] _36_ = new byte[16];
public _52_sprites[] _52_Sprites;
[Serializable][Reflexive(Name="Sprites", Offset=52, ChunkSize=32, Label="")]
public class _52_sprites
{
private int __StartOffset__;
[Value(Name="Bitmap Index", Offset=0)]
public Int32 _0_Bitmap_Index;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
[Value(Name="Left", Offset=8)]
public Single _8_Left;
[Value(Name="Right", Offset=12)]
public Single _12_Right;
[Value(Name="Top", Offset=16)]
public Single _16_Top;
[Value(Name="Bottom", Offset=20)]
public Single _20_Bottom;
[Value(Name="Reg Point X", Offset=24)]
public Single _24_Reg_Point_X;
[Value(Name="Reg Point Y", Offset=28)]
public Single _28_Reg_Point_Y;
}
}
public _68_bitmap_data[] _68_Bitmap_Data;
[Serializable][Reflexive(Name="Bitmap Data", Offset=68, ChunkSize=116, Label="")]
public class _68_bitmap_data
{
private int __StartOffset__;
[Value(Name="Tag", Offset=0)]
public Dependancy _0_Tag;
[Value(Name="Width", Offset=4)]
public Int16 _4_Width;
[Value(Name="Height", Offset=6)]
public Int16 _6_Height;
[Value(Name="Depth", Offset=8)]
public Int16 _8_Depth;
[Value(Name="Type", Offset=10)]
public Int16 _10_Type;
[Value(Name="Format", Offset=12)]
public Int16 _12_Format;
[Value(Name="Flags", Offset=14)]
public Bitmask _14_Flags = new Bitmask(new string[] { "^2 Dimensions","Compressed","Palettized","Swizzled","Linear","v16u16","HUD Bitmap?","Always on?","Interlaced?" }, 2);
[Value(Name="Reg X", Offset=16)]
public Int16 _16_Reg_X;
[Value(Name="Reg Y", Offset=18)]
public Int16 _18_Reg_Y;
[Value(Name="MipMap Count", Offset=20)]
public Int16 _20_MipMap_Count;
[Value(Name="PixelOffset", Offset=22)]
public Int16 _22_PixelOffset;
[Value(Name="Zero", Offset=24)]
public Int32 _24_Zero;
[Value(Name="LOD1 Offset", Offset=28)]
public Int32 _28_LOD1_Offset;
[Value(Name="LOD2 Offset", Offset=32)]
public Int32 _32_LOD2_Offset;
[Value(Name="LOD3 Offset", Offset=36)]
public Int32 _36_LOD3_Offset;
[Value(Offset=40)]
public Byte[] _40_ = new byte[12];
[Value(Name="LOD1 Size", Offset=52)]
public Int32 _52_LOD1_Size;
[Value(Name="LOD2 Size", Offset=56)]
public Int32 _56_LOD2_Size;
[Value(Name="LOD3 Size", Offset=60)]
public Int32 _60_LOD3_Size;
[Value(Offset=64)]
public Byte[] _64_ = new byte[12];
[Value(Name="ID", Offset=76)]
public Int32 _76_ID;
[Value(Offset=80)]
public Byte[] _80_ = new byte[8];
[Value(Name="Flags(CBZ)", Offset=88)]
public Bitmask _88_Flags_CBZ_ = new Bitmask(new string[] {  }, 4);
[Value(Offset=92)]
public Byte[] _92_ = new byte[4];
[Value(Name="Unknown(CBZ)", Offset=96)]
public Int32 _96_Unknown_CBZ_;
[Value(Name="Unknown(CBZ)", Offset=100)]
public Int32 _100_Unknown_CBZ_;
[Value(Name="Unknown(CBZ)", Offset=104)]
public Int32 _104_Unknown_CBZ_;
[Value(Name="Unknown(CBZ)", Offset=108)]
public Int32 _108_Unknown_CBZ_;
[Value(Offset=112)]
public Byte[] _112_ = new byte[4];
}
        }
    }
}