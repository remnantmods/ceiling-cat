using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("nhdt")]
        public class nhdt
        {
[Value(Name="DO NOT USE", Offset=0)]
public Dependancy _0_DO_NOT_USE;
public _4_bitmap_widgets[] _4_Bitmap_Widgets;
[Serializable][Reflexive(Name="Bitmap Widgets", Offset=4, ChunkSize=100, Label="")]
public class _4_bitmap_widgets
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Input 1", Offset=4)]
public H2.DataTypes.Enum _4_Input_1 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 2", Offset=5)]
public H2.DataTypes.Enum _5_Input_2 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 3", Offset=6)]
public H2.DataTypes.Enum _6_Input_3 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 4", Offset=7)]
public H2.DataTypes.Enum _7_Input_4 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="[Do Draw If] Unit Flags", Offset=8)]
public Bitmask _8__Do_Draw_If__Unit_Flags = new Bitmask(new string[] { "Default","Grenade Type Is NONE","Grenade Type Is Frag","Grenade Type Is Plasma","Unit Is Single Wielding","Unit Is Dual Wielding","Unit Is Unzoomed","Unit Is Zoomed (Level 1)","Unit Is Zoomed (Level 2)","Grenades Disabled","Binoculars Enabled","Motion Sensor Enabled","Sheild Enabled","Dervish" }, 2);
[Value(Name="[Do Draw If] Extra Flags", Offset=10)]
public Bitmask _10__Do_Draw_If__Extra_Flags = new Bitmask(new string[] { "AutoAim - Friendly","AutoAim - Plasma","AutoAim - Headshot","AutoAim - Vulnerable","AutoAim - Invincible" }, 1);
[Value(Name="[Do Draw If] Weapon Flags", Offset=11)]
public Bitmask _11__Do_Draw_If__Weapon_Flags = new Bitmask(new string[] { "Primary Weapon","Secondary Weapon","Backpack Weapon","Age Below Cutoff","Clip Below Cutoff","Total Below Cutoff","Overheated","Out Of Ammo","Lock Target Available","Locking","Locked" }, 2);
[Value(Name="[Do Draw If] Game Engine State Flags", Offset=13)]
public Bitmask _13__Do_Draw_If__Game_Engine_State_Flags = new Bitmask(new string[] { "Campaign Solo","Campaign Coop","Free For All","Team Game","User Leading","User Not Leading","Timed Game","Untimed Game","Other Score Valid","Other Score Invalid","Player Is Arming Bomb","Player Talking" }, 2);
[Value(Name="[Don't Draw If] Unit Flags", Offset=15)]
public Bitmask _15__Don_t_Draw_If__Unit_Flags = new Bitmask(new string[] { "Default","Grenade Type Is NONE","Grenade Type Is Frag","Grenade Type Is Plasma","Unit Is Single Wielding","Unit Is Dual Wielding","Unit Is Unzoomed","Unit Is Zoomed (Level 1)","Unit Is Zoomed (Level 2)","Grenades Disabled","Binoculars Enabled","Motion Sensor Enabled","Sheild Enabled","Dervish" }, 2);
[Value(Name="[Don't Draw If] Extra Flags", Offset=17)]
public Bitmask _17__Don_t_Draw_If__Extra_Flags = new Bitmask(new string[] { "AutoAim - Friendly","AutoAim - Plasma","AutoAim - Headshot","AutoAim - Vulnerable","AutoAim - Invincible" }, 1);
[Value(Name="[Don't Draw If] Weapon Flags", Offset=18)]
public Bitmask _18__Don_t_Draw_If__Weapon_Flags = new Bitmask(new string[] { "Primary Weapon","Secondary Weapon","Backpack Weapon","Age Below Cutoff","Clip Below Cutoff","Total Below Cutoff","Overheated","Out Of Ammo","Lock Target Available","Locking","Locked" }, 2);
[Value(Name="[Don't Draw If] Game Engine State Flags", Offset=20)]
public Bitmask _20__Don_t_Draw_If__Game_Engine_State_Flags = new Bitmask(new string[] { "Campaign Solo","Campaign Coop","Free For All","Team Game","User Leading","User Not Leading","Timed Game","Untimed Game","Other Score Valid","Other Score Invalid","Player Is Arming Bomb","Player Talking" }, 2);
[Value(Name="Age Cutoff", Offset=22)]
public Int16 _22_Age_Cutoff;
[Value(Name="Clip Cutoff", Offset=24)]
public Int16 _24_Clip_Cutoff;
[Value(Name="Total Cutoff", Offset=26)]
public Int16 _26_Total_Cutoff;
[Value(Name="Anchor", Offset=28)]
public H2.DataTypes.Enum _28_Anchor = new H2.DataTypes.Enum(new string[] {  "Health And Sheild", "Weapon HUD", "Motion Sensor", "Scoreboard", "Crosshair", "Lock-on Target" }, 2);
[Value(Name="Flags", Offset=30)]
public Bitmask _30_Flags = new Bitmask(new string[] { "Flip Horizontally","Flip Vertically","(Scope) Mirror Horizontally","(Scope) Mirror Vertically","(Scope) Stretch" }, 2);
[Value(Name="Bitmap", Offset=32)]
public Dependancy _32_Bitmap;
[Value(Name="Shader", Offset=40)]
public Dependancy _40_Shader;
[Value(Name="Fullscreen Sequence Index", Offset=48)]
public Byte _48_Fullscreen_Sequence_Index;
[Value(Name="Halfscreen Sequence Index", Offset=49)]
public Byte _49_Halfscreen_Sequence_Index;
[Value(Name="Quarterscreen Sequence Index", Offset=50)]
public Byte _50_Quarterscreen_Sequence_Index;
[Value(Offset=51)]
public Byte[] _51_ = new byte[1];
[Value(Name="Fullscreen Offset X", Offset=52)]
public Int16 _52_Fullscreen_Offset_X;
[Value(Name="Fullscreen Offset Y", Offset=54)]
public Int16 _54_Fullscreen_Offset_Y;
[Value(Name="Halfscreen Offset X", Offset=56)]
public Int16 _56_Halfscreen_Offset_X;
[Value(Name="Halfscreen Offset Y", Offset=58)]
public Int16 _58_Halfscreen_Offset_Y;
[Value(Name="Quarterscreen Offset X", Offset=60)]
public Int16 _60_Quarterscreen_Offset_X;
[Value(Name="Quarterscreen Offset Y", Offset=62)]
public Int16 _62_Quarterscreen_Offset_Y;
[Value(Name="Fullscreen Registration Point X", Offset=64)]
public Single _64_Fullscreen_Registration_Point_X;
[Value(Name="Fullscreen Registration Point Y", Offset=68)]
public Single _68_Fullscreen_Registration_Point_Y;
[Value(Name="Halfscreen Registration Point X", Offset=72)]
public Single _72_Halfscreen_Registration_Point_X;
[Value(Name="Halfscreen Registration Point Y", Offset=76)]
public Single _76_Halfscreen_Registration_Point_Y;
[Value(Name="Quarterscreen Registration Point X", Offset=80)]
public Single _80_Quarterscreen_Registration_Point_X;
[Value(Name="Quarterscreen Registration Point Y", Offset=84)]
public Single _84_Quarterscreen_Registration_Point_Y;
public _88_effect[] _88_Effect;
[Serializable][Reflexive(Name="Effect", Offset=88, ChunkSize=104, Label="")]
public class _88_effect
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Apply Scale","Apply Theta","Apply Offset" }, 4);
[Value(Name="Input Name", Offset=4)]
public StringID _4_Input_Name;
[Value(Name="Range Name", Offset=8)]
public StringID _8_Range_Name;
[Value(Name="Time Period In Seconds", Offset=12)]
public Single _12_Time_Period_In_Seconds;
public _16_function[] _16_Function;
[Serializable][Reflexive(Name="Function", Offset=16, ChunkSize=1, Label="")]
public class _16_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=24)]
public StringID _24_Input_Name;
[Value(Name="Range Name", Offset=28)]
public StringID _28_Range_Name;
[Value(Name="Time Period In Seconds", Offset=32)]
public Single _32_Time_Period_In_Seconds;
public _36_function[] _36_Function;
[Serializable][Reflexive(Name="Function", Offset=36, ChunkSize=1, Label="")]
public class _36_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=44)]
public StringID _44_Input_Name;
[Value(Name="Range Name", Offset=48)]
public StringID _48_Range_Name;
[Value(Name="Time Period In Seconds", Offset=52)]
public Single _52_Time_Period_In_Seconds;
public _56_function[] _56_Function;
[Serializable][Reflexive(Name="Function", Offset=56, ChunkSize=1, Label="")]
public class _56_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=64)]
public StringID _64_Input_Name;
[Value(Name="Range Name", Offset=68)]
public StringID _68_Range_Name;
[Value(Name="Time Period In Seconds", Offset=72)]
public Single _72_Time_Period_In_Seconds;
public _76_function[] _76_Function;
[Serializable][Reflexive(Name="Function", Offset=76, ChunkSize=1, Label="")]
public class _76_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=84)]
public StringID _84_Input_Name;
[Value(Name="Range Name", Offset=88)]
public StringID _88_Range_Name;
[Value(Name="Time Period In Seconds", Offset=92)]
public Single _92_Time_Period_In_Seconds;
public _96_function[] _96_Function;
[Serializable][Reflexive(Name="Function", Offset=96, ChunkSize=1, Label="")]
public class _96_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
[Value(Name="Special HUD Type", Offset=96)]
public H2.DataTypes.Enum _96_Special_HUD_Type = new H2.DataTypes.Enum(new string[] {  "Unspecial", "S.B. Player Emblem", "S.B. Other Player Emblem", "S.B. Player Score Meter", "S.B. Other Player Score Meter", "Unit Sheild Meter", "Motion Sensor", "Territory Meter" }, 4);
}
public _12_numerical_elements[] _12_Numerical_Elements;
[Serializable][Reflexive(Name="Numerical Elements", Offset=12, ChunkSize=84, Label="")]
public class _12_numerical_elements
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Input 1", Offset=4)]
public H2.DataTypes.Enum _4_Input_1 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 2", Offset=5)]
public H2.DataTypes.Enum _5_Input_2 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 3", Offset=6)]
public H2.DataTypes.Enum _6_Input_3 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 4", Offset=7)]
public H2.DataTypes.Enum _7_Input_4 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="[Do Draw If] Unit Flags", Offset=8)]
public Bitmask _8__Do_Draw_If__Unit_Flags = new Bitmask(new string[] { "Default","Grenade Type Is NONE","Grenade Type Is Frag","Grenade Type Is Plasma","Unit Is Single Wielding","Unit Is Dual Wielding","Unit Is Unzoomed","Unit Is Zoomed (Level 1)","Unit Is Zoomed (Level 2)","Grenades Disabled","Binoculars Enabled","Motion Sensor Enabled","Sheild Enabled","Dervish" }, 2);
[Value(Name="[Do Draw If] Extra Flags", Offset=10)]
public Bitmask _10__Do_Draw_If__Extra_Flags = new Bitmask(new string[] { "AutoAim - Friendly","AutoAim - Plasma","AutoAim - Headshot","AutoAim - Vulnerable","AutoAim - Invincible" }, 1);
[Value(Name="[Do Draw If] Weapon Flags", Offset=11)]
public Bitmask _11__Do_Draw_If__Weapon_Flags = new Bitmask(new string[] { "Primary Weapon","Secondary Weapon","Backpack Weapon","Age Below Cutoff","Clip Below Cutoff","Total Below Cutoff","Overheated","Out Of Ammo","Lock Target Available","Locking","Locked" }, 2);
[Value(Name="[Do Draw If] Game Engine State Flags", Offset=13)]
public Bitmask _13__Do_Draw_If__Game_Engine_State_Flags = new Bitmask(new string[] { "Campaign Solo","Campaign Coop","Free For All","Team Game","User Leading","User Not Leading","Timed Game","Untimed Game","Other Score Valid","Other Score Invalid","Player Is Arming Bomb","Player Talking" }, 2);
[Value(Name="[Don't Draw If] Unit Flags", Offset=15)]
public Bitmask _15__Don_t_Draw_If__Unit_Flags = new Bitmask(new string[] { "Default","Grenade Type Is NONE","Grenade Type Is Frag","Grenade Type Is Plasma","Unit Is Single Wielding","Unit Is Dual Wielding","Unit Is Unzoomed","Unit Is Zoomed (Level 1)","Unit Is Zoomed (Level 2)","Grenades Disabled","Binoculars Enabled","Motion Sensor Enabled","Sheild Enabled","Dervish" }, 2);
[Value(Name="[Don't Draw If] Extra Flags", Offset=17)]
public Bitmask _17__Don_t_Draw_If__Extra_Flags = new Bitmask(new string[] { "AutoAim - Friendly","AutoAim - Plasma","AutoAim - Headshot","AutoAim - Vulnerable","AutoAim - Invincible" }, 1);
[Value(Name="[Don't Draw If] Weapon Flags", Offset=18)]
public Bitmask _18__Don_t_Draw_If__Weapon_Flags = new Bitmask(new string[] { "Primary Weapon","Secondary Weapon","Backpack Weapon","Age Below Cutoff","Clip Below Cutoff","Total Below Cutoff","Overheated","Out Of Ammo","Lock Target Available","Locking","Locked" }, 2);
[Value(Name="[Don't Draw If] Game Engine State Flags", Offset=20)]
public Bitmask _20__Don_t_Draw_If__Game_Engine_State_Flags = new Bitmask(new string[] { "Campaign Solo","Campaign Coop","Free For All","Team Game","User Leading","User Not Leading","Timed Game","Untimed Game","Other Score Valid","Other Score Invalid","Player Is Arming Bomb","Player Talking" }, 2);
[Value(Name="Age Cutoff", Offset=22)]
public Int16 _22_Age_Cutoff;
[Value(Name="Clip Cutoff", Offset=24)]
public Int16 _24_Clip_Cutoff;
[Value(Name="Total Cutoff", Offset=26)]
public Int16 _26_Total_Cutoff;
[Value(Name="Anchor", Offset=28)]
public H2.DataTypes.Enum _28_Anchor = new H2.DataTypes.Enum(new string[] {  "Health And Sheild", "Weapon HUD", "Motion Sensor", "Scoreboard", "Crosshair", "Lock-on Target" }, 2);
[Value(Name="Flags", Offset=30)]
public Bitmask _30_Flags = new Bitmask(new string[] { "String Is A Number","Force 2-Digit Number","Force 3-Digit Number","Talking Player Hack" }, 2);
[Value(Name="Shader", Offset=32)]
public Dependancy _32_Shader;
[Value(Name="String", Offset=40)]
public StringID _40_String;
[Value(Name="Justification", Offset=44)]
public H2.DataTypes.Enum _44_Justification = new H2.DataTypes.Enum(new string[] {  "Left", "Center", "Right" }, 2);
[Value(Name="Fullscreen Font Index", Offset=46)]
public H2.DataTypes.Enum _46_Fullscreen_Font_Index = new H2.DataTypes.Enum(new string[] {  "Default", "Number Font" }, 2);
[Value(Name="Halfscreen Font Index", Offset=48)]
public H2.DataTypes.Enum _48_Halfscreen_Font_Index = new H2.DataTypes.Enum(new string[] {  "Default", "Number Font" }, 2);
[Value(Name="Quarterscreen Font Index", Offset=50)]
public H2.DataTypes.Enum _50_Quarterscreen_Font_Index = new H2.DataTypes.Enum(new string[] {  "Default", "Number Font" }, 2);
[Value(Name="Fullscreen Scale", Offset=52)]
public Single _52_Fullscreen_Scale;
[Value(Name="Halfscreen Scale", Offset=56)]
public Single _56_Halfscreen_Scale;
[Value(Name="Quarterscreen Scale", Offset=60)]
public Single _60_Quarterscreen_Scale;
[Value(Name="Fullscreen Offset X", Offset=64)]
public Int16 _64_Fullscreen_Offset_X;
[Value(Name="Fullscreen Offset Y", Offset=66)]
public Int16 _66_Fullscreen_Offset_Y;
[Value(Name="Halfscreen Offset X", Offset=68)]
public Int16 _68_Halfscreen_Offset_X;
[Value(Name="Halfscreen Offset Y", Offset=70)]
public Int16 _70_Halfscreen_Offset_Y;
[Value(Name="Quarterscreen Offset X", Offset=72)]
public Int16 _72_Quarterscreen_Offset_X;
[Value(Name="Quarterscreen Offset Y", Offset=74)]
public Int16 _74_Quarterscreen_Offset_Y;
public _76_effect[] _76_Effect;
[Serializable][Reflexive(Name="Effect", Offset=76, ChunkSize=104, Label="")]
public class _76_effect
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Apply Scale","Apply Theta","Apply Offset" }, 4);
[Value(Name="Input Name", Offset=4)]
public StringID _4_Input_Name;
[Value(Name="Range Name", Offset=8)]
public StringID _8_Range_Name;
[Value(Name="Time Period In Seconds", Offset=12)]
public Single _12_Time_Period_In_Seconds;
public _16_function[] _16_Function;
[Serializable][Reflexive(Name="Function", Offset=16, ChunkSize=1, Label="")]
public class _16_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=24)]
public StringID _24_Input_Name;
[Value(Name="Range Name", Offset=28)]
public StringID _28_Range_Name;
[Value(Name="Time Period In Seconds", Offset=32)]
public Single _32_Time_Period_In_Seconds;
public _36_function[] _36_Function;
[Serializable][Reflexive(Name="Function", Offset=36, ChunkSize=1, Label="")]
public class _36_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=44)]
public StringID _44_Input_Name;
[Value(Name="Range Name", Offset=48)]
public StringID _48_Range_Name;
[Value(Name="Time Period In Seconds", Offset=52)]
public Single _52_Time_Period_In_Seconds;
public _56_function[] _56_Function;
[Serializable][Reflexive(Name="Function", Offset=56, ChunkSize=1, Label="")]
public class _56_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=64)]
public StringID _64_Input_Name;
[Value(Name="Range Name", Offset=68)]
public StringID _68_Range_Name;
[Value(Name="Time Period In Seconds", Offset=72)]
public Single _72_Time_Period_In_Seconds;
public _76_function[] _76_Function;
[Serializable][Reflexive(Name="Function", Offset=76, ChunkSize=1, Label="")]
public class _76_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Input Name", Offset=84)]
public StringID _84_Input_Name;
[Value(Name="Range Name", Offset=88)]
public StringID _88_Range_Name;
[Value(Name="Time Period In Seconds", Offset=92)]
public Single _92_Time_Period_In_Seconds;
public _96_function[] _96_Function;
[Serializable][Reflexive(Name="Function", Offset=96, ChunkSize=1, Label="")]
public class _96_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
}
[Value(Name="Low Clip Cutoff", Offset=20)]
public Int16 _20_Low_Clip_Cutoff;
[Value(Name="Low Ammo Cutoff", Offset=22)]
public Int16 _22_Low_Ammo_Cutoff;
[Value(Name="Age Cutoff", Offset=24)]
public Single _24_Age_Cutoff;
public _28_unknown98[] _28_Unknown98;
[Serializable][Reflexive(Name="Unknown98", Offset=28, ChunkSize=80, Label="")]
public class _28_unknown98
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Input 1", Offset=4)]
public H2.DataTypes.Enum _4_Input_1 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 2", Offset=5)]
public H2.DataTypes.Enum _5_Input_2 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 3", Offset=6)]
public H2.DataTypes.Enum _6_Input_3 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="Input 4", Offset=7)]
public H2.DataTypes.Enum _7_Input_4 = new H2.DataTypes.Enum(new string[] {  "BASIC Zero", "BASIC One", "BASIC Time", "BASIC Global HUD Fade", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "UNIT Sheild", "UNIT Body", "UNIT AutoAimed", "UNIT Has No Grenades", "UNIT Frag Grenade Count", "UNIT Plasma Grenade Count", "UNIT Time On DPL Sheild", "UNIT Zoom Fraction", "UNIT Camo Value", "...", "...", "...", "...", "...", "...", "...", "...", "PARENT Sheild", "PARENT Body", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "...", "WEAPON Clip Ammo", "WEAPON Heat", "WEAPON Battery", "WEAPON Total Ammo", "WEAPON Barrel Spin", "WEAPON Overheated", "WEAPON Clip Ammo Fraction", "WEAPON Time On Overheat", "WEAPON Battery Fraction", "WEAPON Locking Fraction", "...", "...", "...", "...", "...", "...", "...", "User Score Fraction", "Other User Score Fraction", "User Winning", "Bomb Arming Amount" }, 1);
[Value(Name="[Do Draw If] Unit Flags", Offset=8)]
public Bitmask _8__Do_Draw_If__Unit_Flags = new Bitmask(new string[] { "Default","Grenade Type Is NONE","Grenade Type Is Frag","Grenade Type Is Plasma","Unit Is Single Wielding","Unit Is Dual Wielding","Unit Is Unzoomed","Unit Is Zoomed (Level 1)","Unit Is Zoomed (Level 2)","Grenades Disabled","Binoculars Enabled","Motion Sensor Enabled","Sheild Enabled","Dervish" }, 2);
[Value(Name="[Do Draw If] Extra Flags", Offset=10)]
public Bitmask _10__Do_Draw_If__Extra_Flags = new Bitmask(new string[] { "AutoAim - Friendly","AutoAim - Plasma","AutoAim - Headshot","AutoAim - Vulnerable","AutoAim - Invincible" }, 1);
[Value(Name="[Do Draw If] Weapon Flags", Offset=11)]
public Bitmask _11__Do_Draw_If__Weapon_Flags = new Bitmask(new string[] { "Primary Weapon","Secondary Weapon","Backpack Weapon","Age Below Cutoff","Clip Below Cutoff","Total Below Cutoff","Overheated","Out Of Ammo","Lock Target Available","Locking","Locked" }, 2);
[Value(Name="[Do Draw If] Game Engine State Flags", Offset=13)]
public Bitmask _13__Do_Draw_If__Game_Engine_State_Flags = new Bitmask(new string[] { "Campaign Solo","Campaign Coop","Free For All","Team Game","User Leading","User Not Leading","Timed Game","Untimed Game","Other Score Valid","Other Score Invalid","Player Is Arming Bomb","Player Talking" }, 2);
[Value(Name="[Don't Draw If] Unit Flags", Offset=15)]
public Bitmask _15__Don_t_Draw_If__Unit_Flags = new Bitmask(new string[] { "Default","Grenade Type Is NONE","Grenade Type Is Frag","Grenade Type Is Plasma","Unit Is Single Wielding","Unit Is Dual Wielding","Unit Is Unzoomed","Unit Is Zoomed (Level 1)","Unit Is Zoomed (Level 2)","Grenades Disabled","Binoculars Enabled","Motion Sensor Enabled","Sheild Enabled","Dervish" }, 2);
[Value(Name="[Don't Draw If] Extra Flags", Offset=17)]
public Bitmask _17__Don_t_Draw_If__Extra_Flags = new Bitmask(new string[] { "AutoAim - Friendly","AutoAim - Plasma","AutoAim - Headshot","AutoAim - Vulnerable","AutoAim - Invincible" }, 1);
[Value(Name="[Don't Draw If] Weapon Flags", Offset=18)]
public Bitmask _18__Don_t_Draw_If__Weapon_Flags = new Bitmask(new string[] { "Primary Weapon","Secondary Weapon","Backpack Weapon","Age Below Cutoff","Clip Below Cutoff","Total Below Cutoff","Overheated","Out Of Ammo","Lock Target Available","Locking","Locked" }, 2);
[Value(Name="[Don't Draw If] Game Engine State Flags", Offset=20)]
public Bitmask _20__Don_t_Draw_If__Game_Engine_State_Flags = new Bitmask(new string[] { "Campaign Solo","Campaign Coop","Free For All","Team Game","User Leading","User Not Leading","Timed Game","Untimed Game","Other Score Valid","Other Score Invalid","Player Is Arming Bomb","Player Talking" }, 2);
[Value(Name="Age Cutoff", Offset=22)]
public Int16 _22_Age_Cutoff;
[Value(Name="Clip Cutoff", Offset=24)]
public Int16 _24_Clip_Cutoff;
[Value(Name="Total Cutoff", Offset=26)]
public Int16 _26_Total_Cutoff;
[Value(Name="Anchor", Offset=28)]
public H2.DataTypes.Enum _28_Anchor = new H2.DataTypes.Enum(new string[] {  "Health And Sheild", "Weapon HUD", "Motion Sensor", "Scoreboard", "Crosshair", "Lock-on Target" }, 2);
[Value(Name="Flags", Offset=30)]
public Bitmask _30_Flags = new Bitmask(new string[] { "unused" }, 2);
[Value(Name="Bitmap", Offset=32)]
public Dependancy _32_Bitmap;
[Value(Name="Fullscreen Effect", Offset=40)]
public Dependancy _40_Fullscreen_Effect;
[Value(Name="Halfscreen Effect", Offset=48)]
public Dependancy _48_Halfscreen_Effect;
[Value(Name="Quarterscreen Effect", Offset=56)]
public Dependancy _56_Quarterscreen_Effect;
[Value(Name="Fullscreen", Offset=64)]
public Byte _64_Fullscreen;
[Value(Name="Halfscreen", Offset=65)]
public Byte _65_Halfscreen;
[Value(Name="Quarterscreen", Offset=66)]
public Byte _66_Quarterscreen;
[Value(Offset=67)]
public Byte[] _67_ = new byte[1];
[Value(Name="Fullscreen Offset X", Offset=68)]
public Int16 _68_Fullscreen_Offset_X;
[Value(Name="Fullscreen Offset Y", Offset=70)]
public Int16 _70_Fullscreen_Offset_Y;
[Value(Name="Halfscreen Offset X", Offset=72)]
public Int16 _72_Halfscreen_Offset_X;
[Value(Name="Halfscreen Offset Y", Offset=74)]
public Int16 _74_Halfscreen_Offset_Y;
[Value(Name="Quarterscreen Offset X", Offset=76)]
public Int16 _76_Quarterscreen_Offset_X;
[Value(Name="Quarterscreen Offset Y", Offset=78)]
public Int16 _78_Quarterscreen_Offset_Y;
}
        }
    }
}