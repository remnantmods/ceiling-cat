using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("snd!")]
        public class snd_
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Split into premutations","3rd Person?","3rd Person?","Unknown" }, 2);
[Value(Name="Sound Class", Offset=2)]
public Int16 _2_Sound_Class;
[Value(Name="Format", Offset=4)]
public H2.DataTypes.Enum _4_Format = new H2.DataTypes.Enum(new string[] {  "Mono 22050kbps", "Stero 44100kbps", "WMA Specific" }, 1);
[Value(Name="Compression", Offset=5)]
public H2.DataTypes.Enum _5_Compression = new H2.DataTypes.Enum(new string[] {  "None", "XBox ADPCM", "ima adpcm?", "ogg?", "WMA" }, 1);
[Value(Name="Index to Sound Properties", Offset=6)]
public Int16 _6_Index_to_Sound_Properties;
[Value(Name="Index to Sound Premutations", Offset=8)]
public Int16 _8_Index_to_Sound_Premutations;
[Value(Name="Premutation Chunk Count", Offset=10)]
public Byte _10_Premutation_Chunk_Count;
[Value(Name="Index to Sound Unknown17", Offset=11)]
public Byte _11_Index_to_Sound_Unknown17;
[Value(Name="Not Index :D", Offset=12)]
public Int16 _12_Not_Index__D;
[Value(Name="Index to Combat Dialog Raw", Offset=14)]
public Int16 _14_Index_to_Combat_Dialog_Raw;
[Value(Name="Not Index :D", Offset=16)]
public Int32 _16_Not_Index__D;
        }
    }
}