using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("proj")]
        public class proj
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Does Not Cast Shadow","Search Cardinal Direction Lightmaps","unused","Not A Pathfinding Obstacle","Extension Of Parent","Does Not Cause Collision Damage","Early Mover","Early Mover Localized Physics","Use Static Massive Lightmap Sample","Object Scales Attachments","Inherits Player's Appearance","Dead Bipeds Can't Localize","Attach To Clusters By Dynamic Sphere","Effects Created By This Object Do Not" }, 4);
[Value(Name="Bounding Radius", Offset=4)]
public Single _4_Bounding_Radius;
[Value(Name="Bounding Offset X", Offset=8)]
public Single _8_Bounding_Offset_X;
[Value(Name="Bounding Offset Y", Offset=12)]
public Single _12_Bounding_Offset_Y;
[Value(Name="Bounding Offset Z", Offset=16)]
public Single _16_Bounding_Offset_Z;
[Value(Name="Acceleration Scale", Offset=20)]
public Single _20_Acceleration_Scale;
[Value(Name="Lightmap Shadow Mode", Offset=24)]
public H2.DataTypes.Enum _24_Lightmap_Shadow_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Never", "Always" }, 4);
[Value(Name="Sweetener Size", Offset=28)]
public H2.DataTypes.Enum _28_Sweetener_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Medium", "Large" }, 4);
[Value(Name="Dynamic Light Sphere Radius", Offset=32)]
public Single _32_Dynamic_Light_Sphere_Radius;
[Value(Name="Dynamic Light Sphere Offset X", Offset=36)]
public Single _36_Dynamic_Light_Sphere_Offset_X;
[Value(Name="Dynamic Light Sphere Offset Y", Offset=40)]
public Single _40_Dynamic_Light_Sphere_Offset_Y;
[Value(Name="Dynamic Light Sphere Offset Z", Offset=44)]
public Single _44_Dynamic_Light_Sphere_Offset_Z;
[Value(Name="Default Model Variant", Offset=48)]
public StringID _48_Default_Model_Variant;
[Value(Name="Model", Offset=52)]
public Dependancy _52_Model;
[Value(Name="Crate Object", Offset=56)]
public Dependancy _56_Crate_Object;
[Value(Name="Modifier Shader", Offset=60)]
public Dependancy _60_Modifier_Shader;
[Value(Name="Creation Effect", Offset=64)]
public Dependancy _64_Creation_Effect;
[Value(Name="Material Effects", Offset=68)]
public Dependancy _68_Material_Effects;
public _72_ai_properties[] _72_Ai_Properties;
[Serializable][Reflexive(Name="Ai Properties", Offset=72, ChunkSize=16, Label="")]
public class _72_ai_properties
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Destroyable Cover","Pathfinding Ignore When Dead","Dynamic Cover" }, 4);
[Value(Name="Type Name", Offset=4)]
public StringID _4_Type_Name;
[Value(Name="Size", Offset=8)]
public H2.DataTypes.Enum _8_Size = new H2.DataTypes.Enum(new string[] {  "Default", "Tiny", "Small", "Medium", "Large", "Huge", "Immobile" }, 4);
[Value(Name="Leap Jump Speed", Offset=12)]
public H2.DataTypes.Enum _12_Leap_Jump_Speed = new H2.DataTypes.Enum(new string[] {  "None", "Down", "Step", "Crouch", "Stand", "Storey", "Tower", "Infinite" }, 4);
}
public _80_functions[] _80_Functions;
[Serializable][Reflexive(Name="Functions", Offset=80, ChunkSize=32, Label="")]
public class _80_functions
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invert","Mapping Does Not Controls Active","Always Active","Random Time Offset" }, 4);
[Value(Name="Import Name", Offset=4)]
public StringID _4_Import_Name;
[Value(Name="Export Name", Offset=8)]
public StringID _8_Export_Name;
[Value(Name="Turn Off With", Offset=12)]
public StringID _12_Turn_Off_With;
[Value(Name="Min Value", Offset=16)]
public Single _16_Min_Value;
public _20_function_type__graph_[] _20_Function_Type__Graph_;
[Serializable][Reflexive(Name="Function Type (Graph)", Offset=20, ChunkSize=1, Label="")]
public class _20_function_type__graph_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Scale By", Offset=28)]
public StringID _28_Scale_By;
}
[Value(Name="Apply Collision Damage Scale", Offset=88)]
public Single _88_Apply_Collision_Damage_Scale;
[Value(Name="Min Game Acc (Default)", Offset=92)]
public Single _92_Min_Game_Acc__Default_;
[Value(Name="Max Game Acc (Default)", Offset=96)]
public Single _96_Max_Game_Acc__Default_;
[Value(Name="Min Gam Scale (Default)", Offset=100)]
public Single _100_Min_Gam_Scale__Default_;
[Value(Name="Max Gam Scale (Default)", Offset=104)]
public Single _104_Max_Gam_Scale__Default_;
[Value(Name="Min abs Acc (Default)", Offset=108)]
public Single _108_Min_abs_Acc__Default_;
[Value(Name="Max abs Acc (Default)", Offset=112)]
public Single _112_Max_abs_Acc__Default_;
[Value(Name="Min abs Scale (Default)", Offset=116)]
public Single _116_Min_abs_Scale__Default_;
[Value(Name="Max abs Scale (Default)", Offset=120)]
public Single _120_Max_abs_Scale__Default_;
[Value(Name="Hud Text Message Index", Offset=124)]
public Int16 _124_Hud_Text_Message_Index;
[Value(Offset=126)]
public Byte[] _126_ = new byte[2];
public _128_attachments[] _128_Attachments;
[Serializable][Reflexive(Name="Attachments", Offset=128, ChunkSize=24, Label="")]
public class _128_attachments
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
[Value(Name="Marker", Offset=8)]
public StringID _8_Marker;
[Value(Name="Change Color", Offset=12)]
public StringID _12_Change_Color;
[Value(Name="Primary Scale", Offset=16)]
public StringID _16_Primary_Scale;
[Value(Name="Secondary Scale", Offset=20)]
public StringID _20_Secondary_Scale;
}
public _136_widgets[] _136_Widgets;
[Serializable][Reflexive(Name="Widgets", Offset=136, ChunkSize=8, Label="")]
public class _136_widgets
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
}
public _144_old_functions[] _144_Old_Functions;
[Serializable][Reflexive(Name="Old Functions", Offset=144, ChunkSize=0, Label="")]
public class _144_old_functions
{
private int __StartOffset__;
}
public _152_change_colors[] _152_Change_Colors;
[Serializable][Reflexive(Name="Change Colors", Offset=152, ChunkSize=16, Label="")]
public class _152_change_colors
{
private int __StartOffset__;
public _0_initial_permutations[] _0_Initial_Permutations;
[Serializable][Reflexive(Name="Initial Permutations", Offset=0, ChunkSize=32, Label="")]
public class _0_initial_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Variant Name", Offset=28)]
public StringID _28_Variant_Name;
}
public _8_functions[] _8_Functions;
[Serializable][Reflexive(Name="Functions", Offset=8, ChunkSize=36, Label="")]
public class _8_functions
{
private int __StartOffset__;
[Value(Name="Scale Flags", Offset=0)]
public Bitmask _0_Scale_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Darken By", Offset=28)]
public StringID _28_Darken_By;
[Value(Name="Scale By", Offset=32)]
public StringID _32_Scale_By;
}
}
public _160_predicted_resource[] _160_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=160, ChunkSize=8, Label="")]
public class _160_predicted_resource
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Int16 _0_Type;
[Value(Name="Resource Index", Offset=2)]
public Int16 _2_Resource_Index;
[Value(Name="Tag Index", Offset=4)]
public Int32 _4_Tag_Index;
}
[Value(Name="Flags", Offset=168)]
public Bitmask _168_Flags = new Bitmask(new string[] { "Oriented Along Velocity","AI Must Use Ballistic Aiming","Detonation Max Time If Attached","Has Super Combining Explosion","Damage Scales Based On Distance","Travels Instantaneously","Steering Adjusts Orientation","Don't Noise Up Steering","Can Track Behind Itself","Robotron Steering","Faster When Owned By Player" }, 4);
[Value(Name="Detonation Timer Starts", Offset=172)]
public H2.DataTypes.Enum _172_Detonation_Timer_Starts = new H2.DataTypes.Enum(new string[] {  "Immediately", "After first bounce", "When at rest", "After First Bounce Off Any Surface" }, 2);
[Value(Name="Impact Noise", Offset=174)]
public H2.DataTypes.Enum _174_Impact_Noise = new H2.DataTypes.Enum(new string[] {  "Silent", "Medium", "Loud", "Shout", "Quiet" }, 2);
[Value(Name="AI Perception Radius", Offset=176)]
public Single _176_AI_Perception_Radius;
[Value(Name="Collision Radius", Offset=180)]
public Single _180_Collision_Radius;
[Value(Name="Arming Time", Offset=184)]
public Single _184_Arming_Time;
[Value(Name="Danger Radius", Offset=188)]
public Single _188_Danger_Radius;
[Value(Name="Timer(sec)", Offset=192)]
public Single _192_Timer_sec_;
[Value(Name="...To", Offset=196)]
public Single _196____To;
[Value(Name="Minimum Velocity", Offset=200)]
public Single _200_Minimum_Velocity;
[Value(Name="Maximum Range", Offset=204)]
public Single _204_Maximum_Range;
[Value(Name="Detonation Noise", Offset=208)]
public H2.DataTypes.Enum _208_Detonation_Noise = new H2.DataTypes.Enum(new string[] {  "Silent", "Medium", "Loud", "Shout", "Quiet" }, 2);
[Value(Name="Super Detonation Projectile Count", Offset=210)]
public Int16 _210_Super_Detonation_Projectile_Count;
[Value(Name="Detonation Started", Offset=212)]
public Dependancy _212_Detonation_Started;
[Value(Name="Airborne Detonation", Offset=216)]
public Dependancy _216_Airborne_Detonation;
[Value(Name="Ground Detonation", Offset=220)]
public Dependancy _220_Ground_Detonation;
[Value(Name="Detonation Damage", Offset=224)]
public Dependancy _224_Detonation_Damage;
[Value(Name="Attached Detonation Damage", Offset=228)]
public Dependancy _228_Attached_Detonation_Damage;
[Value(Name="Super Detonation", Offset=232)]
public Dependancy _232_Super_Detonation;
[Value(Name="Super Detonation Damage", Offset=236)]
public Dependancy _236_Super_Detonation_Damage;
[Value(Name="Detonation Sound", Offset=240)]
public Dependancy _240_Detonation_Sound;
[Value(Name="Damage Reporting Type", Offset=244)]
public H2.DataTypes.Enum _244_Damage_Reporting_Type = new H2.DataTypes.Enum(new string[] {  "The Guardians", "Falling Damage", "Generic Collision Damage", "Generic Melee Damage", "Generic Explosion", "Magnum Pistol", "Plasma Pistol", "Needler", "SMG", "Plasma Rifle", "Battle Rifle", "Carbine", "Shotgun", "Sniper Rifle", "Beam Rifle", "Rocket Launcher", "Flak Cannon", "Brute Shot", "Disintegrator", "Brute Plasma Rifle", "Energy Sword", "Frag Grenade", "Plasma Grenade", "Flag Melee Damage", "Bomb Melee Damage", "Bomb Explosion Damage", "Ball Melee Damage", "Human Turret", "Plasma Turret", "Banshee", "Ghost", "Mongoose", "Scorpion", "Spectre Driver", "Spectre Gunner", "Warthog Driver", "Warthog Gunner", "Wraith", "Tank", "Sentinal Beam", "Sentinal RPG", "Teleporter" }, 4);
[Value(Name="Super Attached Detonation Damage", Offset=248)]
public Dependancy _248_Super_Attached_Detonation_Damage;
[Value(Name="Material Effect Radius", Offset=252)]
public Single _252_Material_Effect_Radius;
[Value(Name="Fly By Sound", Offset=256)]
public Dependancy _256_Fly_By_Sound;
[Value(Name="Impact Effect", Offset=260)]
public Dependancy _260_Impact_Effect;
[Value(Name="Impact Damage", Offset=264)]
public Dependancy _264_Impact_Damage;
[Value(Name="Boarding Detonation Time", Offset=268)]
public Single _268_Boarding_Detonation_Time;
[Value(Name="Boarding Detonation Damage", Offset=272)]
public Dependancy _272_Boarding_Detonation_Damage;
[Value(Name="Boarding Attached Detonation Damage", Offset=276)]
public Dependancy _276_Boarding_Attached_Detonation_Damage;
[Value(Name="Air Gravity Scale", Offset=280)]
public Single _280_Air_Gravity_Scale;
[Value(Name="Air Damage Range", Offset=284)]
public Single _284_Air_Damage_Range;
[Value(Name="...To", Offset=288)]
public Single _288____To;
[Value(Name="Water Gravity Scale", Offset=292)]
public Single _292_Water_Gravity_Scale;
[Value(Name="Water Damage Range", Offset=296)]
public Single _296_Water_Damage_Range;
[Value(Name="...To", Offset=300)]
public Single _300____To;
[Value(Name="Initial Velocity", Offset=304)]
public Single _304_Initial_Velocity;
[Value(Name="Final Velocity", Offset=308)]
public Single _308_Final_Velocity;
[Value(Name="Guided Angular Velocity Lower (degrees per sec)", Offset=312)]
public Single _312_Guided_Angular_Velocity_Lower__degrees_per_sec_;
[Value(Name="Guided Angular Velocity Upper (degrees per sec)", Offset=316)]
public Single _316_Guided_Angular_Velocity_Upper__degrees_per_sec_;
[Value(Name="Acceleration range (world units)", Offset=320)]
public Single _320_Acceleration_range__world_units_;
[Value(Name="...To", Offset=324)]
public Single _324____To;
[Value(Name="Targeted Leading Fraction", Offset=328)]
public Single _328_Targeted_Leading_Fraction;
[Value(Name="Position Loops", Offset=332)]
public Single _332_Position_Loops;
public _336_material_responses[] _336_Material_Responses;
[Serializable][Reflexive(Name="Material Responses", Offset=336, ChunkSize=88, Label="")]
public class _336_material_responses
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Cannot Be Overpenetrated" }, 2);
[Value(Name="Default Response", Offset=2)]
public H2.DataTypes.Enum _2_Default_Response = new H2.DataTypes.Enum(new string[] {  "Impact (Detonate)", "Fizzle", "Overpenetrate", "Attach", "Bounce", "Bounce (Dud)", "Fizzle (Ricochet)" }, 2);
[Value(Name="DO NOT USE (OLD effect)", Offset=4)]
public Dependancy _4_DO_NOT_USE__OLD_effect_;
[Value(Name="Material Name", Offset=12)]
public StringID _12_Material_Name;
[Value(Offset=16)]
public Byte[] _16_ = new byte[2];
[Value(Name="Response", Offset=18)]
public H2.DataTypes.Enum _18_Response = new H2.DataTypes.Enum(new string[] {  "Impact (Detonate)", "Fizzle", "Overpenetrate", "Attach", "Bounce", "Bounce (Dud)", "Fizzle (Ricochet)" }, 2);
[Value(Name="Flags", Offset=20)]
public Bitmask _20_Flags = new Bitmask(new string[] { "Only Against Units","Never Against Units" }, 4);
[Value(Name="Chance Fraction", Offset=24)]
public Single _24_Chance_Fraction;
[Value(Name="Between", Offset=28)]
public Single _28_Between;
[Value(Name="To", Offset=32)]
public Single _32_To;
[Value(Name="And", Offset=36)]
public Single _36_And;
[Value(Name="To", Offset=40)]
public Single _40_To;
[Value(Name="DO NOT USE (OLD Effect)", Offset=44)]
public Dependancy _44_DO_NOT_USE__OLD_Effect_;
[Value(Name="Scale Effects By", Offset=52)]
public H2.DataTypes.Enum _52_Scale_Effects_By = new H2.DataTypes.Enum(new string[] {  "Damage", "Angle" }, 4);
[Value(Name="Angular Noise", Offset=56)]
public Single _56_Angular_Noise;
[Value(Name="Veloctiy Noise", Offset=60)]
public Single _60_Veloctiy_Noise;
[Value(Name="DO NOT USE (OLD Detonation)", Offset=64)]
public Dependancy _64_DO_NOT_USE__OLD_Detonation_;
[Value(Name="Initial Friction", Offset=72)]
public Single _72_Initial_Friction;
[Value(Name="Maximum Distance", Offset=76)]
public Single _76_Maximum_Distance;
[Value(Name="Parallel Friction", Offset=80)]
public Single _80_Parallel_Friction;
[Value(Name="Perpendicluar Friction", Offset=84)]
public Single _84_Perpendicluar_Friction;
}
        }
    }
}