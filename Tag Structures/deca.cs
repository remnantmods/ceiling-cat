using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("deca")]
        public class deca
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Geometry Inherited By Next Decal","Interpolate Colour","More Colour","No Random Rotation","unused","Water Effect","Special-EF-Snap To Axis","Special-EF-Incremental Counter","unused","Preserve Aspect","unused" }, 2);
[Value(Name="Type", Offset=2)]
public H2.DataTypes.Enum _2_Type = new H2.DataTypes.Enum(new string[] {  "Scratch", "Splatter", "Burn", "Painted Sign" }, 2);
[Value(Name="Layer", Offset=4)]
public H2.DataTypes.Enum _4_Layer = new H2.DataTypes.Enum(new string[] {  "Lit Alpha Blend Prelight", "Lit Alpha Blend", "Double Multiply", "Multiply", "Max", "Add", "Error" }, 2);
[Value(Name="Max Overlapping Count", Offset=6)]
public Int16 _6_Max_Overlapping_Count;
[Value(Name="Next Decal In Chain", Offset=8)]
public Dependancy _8_Next_Decal_In_Chain;
[Value(Name="Radius", Offset=12)]
public Single _12_Radius;
[Value(Name="^To(World Units)", Offset=16)]
public Single _16__To_World_Units_;
[Value(Name="Radius Overlapping Rejection", Offset=20)]
public Single _20_Radius_Overlapping_Rejection;
[Value(Name="Color Lower Bound R", Offset=24)]
public Single _24_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=28)]
public Single _28_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=32)]
public Single _32_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=36)]
public Single _36_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=40)]
public Single _40_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=44)]
public Single _44_Color_Upper_Bound_B;
[Value(Name="Lifetime", Offset=48)]
public Single _48_Lifetime;
[Value(Name="^To(Seconds)", Offset=52)]
public Single _52__To_Seconds_;
[Value(Name="Decay Time", Offset=56)]
public Single _56_Decay_Time;
[Value(Name="^To(Seconds)", Offset=60)]
public Single _60__To_Seconds_;
[Value(Offset=64)]
public Byte[] _64_ = new byte[68];
[Value(Name="Bitmap", Offset=132)]
public Dependancy _132_Bitmap;
[Value(Offset=136)]
public Byte[] _136_ = new byte[20];
[Value(Name="Maximun Sprite Extent(pixels)", Offset=156)]
public Single _156_Maximun_Sprite_Extent_pixels_;
[Value(Offset=160)]
public Byte[] _160_ = new byte[4];
        }
    }
}