using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("trak")]
        public class trak
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
public _4_control_points[] _4_Control_Points;
[Serializable][Reflexive(Name="Control Points", Offset=4, ChunkSize=28, Label="")]
public class _4_control_points
{
private int __StartOffset__;
[Value(Name="Pos i", Offset=0)]
public Single _0_Pos_i;
[Value(Name="Pos j", Offset=4)]
public Single _4_Pos_j;
[Value(Name="Pos k", Offset=8)]
public Single _8_Pos_k;
[Value(Name="Orientation i", Offset=12)]
public Single _12_Orientation_i;
[Value(Name="Orientation j", Offset=16)]
public Single _16_Orientation_j;
[Value(Name="Orientation k", Offset=20)]
public Single _20_Orientation_k;
[Value(Name="Orientation w", Offset=24)]
public Single _24_Orientation_w;
}
        }
    }
}