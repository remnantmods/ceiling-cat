using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("fpch")]
        public class fpch
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Separate Layer Depths","Sort Behind Transparents" }, 4);
[Value(Name="Movement Rotation Multiplier", Offset=4)]
public Single _4_Movement_Rotation_Multiplier;
[Value(Name="Movement Strafing Multiplier", Offset=8)]
public Single _8_Movement_Strafing_Multiplier;
[Value(Name="Movement Zoom Multiplier", Offset=12)]
public Single _12_Movement_Zoom_Multiplier;
[Value(Name="Noise Map Scale", Offset=16)]
public Single _16_Noise_Map_Scale;
[Value(Name="Noise Map", Offset=20)]
public Dependancy _20_Noise_Map;
[Value(Name="Noise Vertical Scale Forward", Offset=24)]
public Single _24_Noise_Vertical_Scale_Forward;
[Value(Name="Noise Vertical Scale Up", Offset=28)]
public Single _28_Noise_Vertical_Scale_Up;
[Value(Name="Noise Opacity Scale Up", Offset=32)]
public Single _32_Noise_Opacity_Scale_Up;
[Value(Name="Animation Period (sec)", Offset=36)]
public Single _36_Animation_Period__sec_;
[Value(Name="Wind Velocity", Offset=40)]
public Single _40_Wind_Velocity;
[Value(Name="...To", Offset=44)]
public Single _44____To;
[Value(Name="Wind Period", Offset=48)]
public Single _48_Wind_Period;
[Value(Name="...To", Offset=52)]
public Single _52____To;
[Value(Name="Wind Acceleration Weight", Offset=56)]
public Single _56_Wind_Acceleration_Weight;
[Value(Name="Wind Perpendicular Weight", Offset=60)]
public Single _60_Wind_Perpendicular_Weight;
[Value(Name="Wind Constant Velocity X", Offset=64)]
public Single _64_Wind_Constant_Velocity_X;
[Value(Name="Wind Constant Velocity Y", Offset=68)]
public Single _68_Wind_Constant_Velocity_Y;
[Value(Name="Wind Constant Velocity Z", Offset=72)]
public Single _72_Wind_Constant_Velocity_Z;
        }
    }
}