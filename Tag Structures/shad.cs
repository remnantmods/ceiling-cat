using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("shad")]
        public class shad
        {
[Value(Name="Unknown", Offset=0)]
public Dependancy _0_Unknown;
[Value(Name="Material Type", Offset=4)]
public StringID _4_Material_Type;
public _8_self_illumination_properties[] _8_Self_Illumination_Properties;
[Serializable][Reflexive(Name="Self-Illumination Properties", Offset=8, ChunkSize=80, Label="")]
public class _8_self_illumination_properties
{
private int __StartOffset__;
[Value(Name="Base Map", Offset=0)]
public Dependancy _0_Base_Map;
[Value(Name="Self-Illumination Map", Offset=8)]
public Dependancy _8_Self_Illumination_Map;
[Value(Name="Primary On Colour: Red", Offset=16)]
public Single _16_Primary_On_Colour__Red;
[Value(Name="Primary On Colour: Blue", Offset=20)]
public Single _20_Primary_On_Colour__Blue;
[Value(Name="Primary On Colour: Green", Offset=24)]
public Single _24_Primary_On_Colour__Green;
[Value(Name="Primary On Colour: Brightness", Offset=28)]
public Single _28_Primary_On_Colour__Brightness;
[Value(Offset=32)]
public Byte[] _32_ = new byte[4];
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Offset=40)]
public Byte[] _40_ = new byte[4];
[Value(Name="Unknown", Offset=44)]
public Dependancy _44_Unknown;
[Value(Name="Secondary Self-Illumination Map", Offset=52)]
public Dependancy _52_Secondary_Self_Illumination_Map;
[Value(Name="Secondary On Colour: Red", Offset=60)]
public Single _60_Secondary_On_Colour__Red;
[Value(Name="Secondary On Colour: Blue", Offset=64)]
public Single _64_Secondary_On_Colour__Blue;
[Value(Name="Secondary On Colour: Green", Offset=68)]
public Single _68_Secondary_On_Colour__Green;
[Value(Name="Secondary On Colour: Brightness", Offset=72)]
public Single _72_Secondary_On_Colour__Brightness;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
}
[Value(Name="type?", Offset=16)]
public Int16 _16_type_;
[Value(Name="Unknown", Offset=18)]
public Int16 _18_Unknown;
[Value(Offset=20)]
public Byte[] _20_ = new byte[8];
public _28_unknown[] _28_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=28, ChunkSize=124, Label="")]
public class _28_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown[] _4_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=4, ChunkSize=12, Label="")]
public class _4_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
public _12_unknown[] _12_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=12, ChunkSize=4, Label="")]
public class _12_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _20_detail_tiling___faloff_colours[] _20_Detail_Tiling___Faloff_Colours;
[Serializable][Reflexive(Name="Detail Tiling / Faloff Colours", Offset=20, ChunkSize=16, Label="")]
public class _20_detail_tiling___faloff_colours
{
private int __StartOffset__;
[Value(Name="U Tiling / Red Colour", Offset=0)]
public Single _0_U_Tiling___Red_Colour;
[Value(Name="Color Green / V tiling", Offset=4)]
public Single _4_Color_Green___V_tiling;
[Value(Name="W Tiling / Blue Colour", Offset=8)]
public Single _8_W_Tiling___Blue_Colour;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
public _28_unknown_[] _28_Unknown_;
[Serializable][Reflexive(Name="Unknown", Offset=28, ChunkSize=8, Label="")]
public class _28_unknown_
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
public _36_unknown[] _36_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=36, ChunkSize=2, Label="")]
public class _36_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
}
public _44_unknown[] _44_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=44, ChunkSize=4, Label="")]
public class _44_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _52_unknown[] _52_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=52, ChunkSize=12, Label="")]
public class _52_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
}
public _60_shader_effects[] _60_Shader_Effects;
[Serializable][Reflexive(Name="Shader Effects", Offset=60, ChunkSize=20, Label="")]
public class _60_shader_effects
{
private int __StartOffset__;
[Value(Name="Position?", Offset=0)]
public StringID _0_Position_;
[Value(Name="Unknown", Offset=4)]
public StringID _4_Unknown;
[Value(Name="Effect Duration (in seconds)", Offset=8)]
public Single _8_Effect_Duration__in_seconds_;
public _12_unknown[] _12_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=12, ChunkSize=1, Label="")]
public class _12_unknown
{
private int __StartOffset__;
[Value(Name="Effect Type", Offset=0)]
public Byte _0_Effect_Type;
}
}
public _68_unknown[] _68_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=68, ChunkSize=4, Label="")]
public class _68_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _76_unknown[] _76_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=76, ChunkSize=4, Label="")]
public class _76_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _84_unknown[] _84_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=84, ChunkSize=4, Label="")]
public class _84_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _92_unknown[] _92_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=92, ChunkSize=4, Label="")]
public class _92_unknown
{
private int __StartOffset__;
[Value(Name="index", Offset=0)]
public Int16 _0_index;
[Value(Name="index", Offset=2)]
public Int16 _2_index;
}
public _100_radiosity_properties__1st_chunk___colour_of_emitted_light__second_chunk___tint_colour[] _100_Radiosity_Properties__1st_chunk___colour_of_emitted_light__second_chunk___tint_colour;
[Serializable][Reflexive(Name="Radiosity Properties: 1st chunk = colour of emitted light, second chunk = tint colour", Offset=100, ChunkSize=12, Label="")]
public class _100_radiosity_properties__1st_chunk___colour_of_emitted_light__second_chunk___tint_colour
{
private int __StartOffset__;
[Value(Name="R", Offset=0)]
public Single _0_R;
[Value(Name="G", Offset=4)]
public Single _4_G;
[Value(Name="B", Offset=8)]
public Single _8_B;
}
public _108_brightness_levels[] _108_Brightness_Levels;
[Serializable][Reflexive(Name="Brightness Levels", Offset=108, ChunkSize=4, Label="")]
public class _108_brightness_levels
{
private int __StartOffset__;
[Value(Name="Brightness", Offset=0)]
public Single _0_Brightness;
}
[Value(Offset=116)]
public Byte[] _116_ = new byte[8];
}
[Value(Offset=36)]
public Byte[] _36_ = new byte[4];
public _40_resources[] _40_Resources;
[Serializable][Reflexive(Name="Resources", Offset=40, ChunkSize=8, Label="")]
public class _40_resources
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Bitmap Used", Offset=4)]
public Int32 _4_Bitmap_Used;
}
[Value(Name="Unknown", Offset=48)]
public Byte _48_Unknown;
[Value(Name="Unknown", Offset=49)]
public Byte _49_Unknown;
[Value(Name="Unknown", Offset=50)]
public Byte _50_Unknown;
[Value(Name="Unknown", Offset=51)]
public Byte _51_Unknown;
[Value(Offset=52)]
public Byte[] _52_ = new byte[4];
[Value(Name="Saturation", Offset=56)]
public Single _56_Saturation;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Specularity", Offset=64)]
public Single _64_Specularity;
[Value(Name="Diffuse Level", Offset=68)]
public Single _68_Diffuse_Level;
[Value(Offset=72)]
public Byte[] _72_ = new byte[8];
        }
    }
}