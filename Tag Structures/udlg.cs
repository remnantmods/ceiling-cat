using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("udlg")]
        public class udlg
        {
[Value(Name="Global Dialogue Info", Offset=0)]
public Dependancy _0_Global_Dialogue_Info;
[Value(Name="Flags", Offset=4)]
public H2.DataTypes.Enum _4_Flags = new H2.DataTypes.Enum(new string[] {  "Female" }, 4);
public _8_vocalizations[] _8_Vocalizations;
[Serializable][Reflexive(Name="Vocalizations", Offset=8, ChunkSize=16, Label="")]
public class _8_vocalizations
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "New Vocalization" }, 4);
[Value(Name="Vocalization", Offset=4)]
public StringID _4_Vocalization;
[Value(Name="Sound", Offset=8)]
public Dependancy _8_Sound;
}
[Value(Name="Mission Dialogue Designator", Offset=16)]
public StringID _16_Mission_Dialogue_Designator;
        }
    }
}