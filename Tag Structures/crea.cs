using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("crea")]
        public class crea
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Does Not Cast Shadow","Search Cardinal Direction Lightmaps","unused","Not A Pathfinding Obstacle","Extension Of Parent","Does Not Cause Collision Damage","Early Mover","Early Mover Localized Physics","Use Static Massive Lightmap Sample","Object Scales Attachments","Inherits Player's Appearance","Dead Bipeds Can't Localize","Attach To Clusters By Dynamic Sphere","Effects Created By This Object Do Not" }, 4);
[Value(Name="Bounding Radius", Offset=4)]
public Single _4_Bounding_Radius;
[Value(Name="Bounding Offset X", Offset=8)]
public Single _8_Bounding_Offset_X;
[Value(Name="Bounding Offset Y", Offset=12)]
public Single _12_Bounding_Offset_Y;
[Value(Name="Bounding Offset Z", Offset=16)]
public Single _16_Bounding_Offset_Z;
[Value(Name="Acceleration Scale", Offset=20)]
public Single _20_Acceleration_Scale;
[Value(Name="Lightmap Shadow Mode", Offset=24)]
public H2.DataTypes.Enum _24_Lightmap_Shadow_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Never", "Always" }, 4);
[Value(Name="Sweetener Size", Offset=28)]
public H2.DataTypes.Enum _28_Sweetener_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Medium", "Large" }, 4);
[Value(Name="Dynamic Light Sphere Radius", Offset=32)]
public Single _32_Dynamic_Light_Sphere_Radius;
[Value(Name="Dynamic Light Sphere Offset X", Offset=36)]
public Single _36_Dynamic_Light_Sphere_Offset_X;
[Value(Name="Dynamic Light Sphere Offset Y", Offset=40)]
public Single _40_Dynamic_Light_Sphere_Offset_Y;
[Value(Name="Dynamic Light Sphere Offset Z", Offset=44)]
public Single _44_Dynamic_Light_Sphere_Offset_Z;
[Value(Name="Default Model Variant", Offset=48)]
public StringID _48_Default_Model_Variant;
[Value(Name="Model", Offset=52)]
public Dependancy _52_Model;
[Value(Name="Crate Object", Offset=56)]
public Dependancy _56_Crate_Object;
[Value(Name="Modifier Shader", Offset=60)]
public Dependancy _60_Modifier_Shader;
[Value(Name="Creation Effect", Offset=64)]
public Dependancy _64_Creation_Effect;
[Value(Name="Material Effects", Offset=68)]
public Dependancy _68_Material_Effects;
public _72_ai_properties[] _72_Ai_Properties;
[Serializable][Reflexive(Name="Ai Properties", Offset=72, ChunkSize=16, Label="")]
public class _72_ai_properties
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Destroyable Cover","Pathfinding Ignore When Dead","Dynamic Cover" }, 4);
[Value(Name="Type Name", Offset=4)]
public StringID _4_Type_Name;
[Value(Name="Size", Offset=8)]
public H2.DataTypes.Enum _8_Size = new H2.DataTypes.Enum(new string[] {  "Default", "Tiny", "Small", "Medium", "Large", "Huge", "Immobile" }, 4);
[Value(Name="Leap Jump Speed", Offset=12)]
public H2.DataTypes.Enum _12_Leap_Jump_Speed = new H2.DataTypes.Enum(new string[] {  "None", "Down", "Step", "Crouch", "Stand", "Storey", "Tower", "Infinite" }, 4);
}
public _80_functions[] _80_Functions;
[Serializable][Reflexive(Name="Functions", Offset=80, ChunkSize=32, Label="")]
public class _80_functions
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invert","Mapping Does Not Controls Active","Always Active","Random Time Offset" }, 4);
[Value(Name="Import Name", Offset=4)]
public StringID _4_Import_Name;
[Value(Name="Export Name", Offset=8)]
public StringID _8_Export_Name;
[Value(Name="Turn Off With", Offset=12)]
public StringID _12_Turn_Off_With;
[Value(Name="Min Value", Offset=16)]
public Single _16_Min_Value;
public _20_function_type__graph_[] _20_Function_Type__Graph_;
[Serializable][Reflexive(Name="Function Type (Graph)", Offset=20, ChunkSize=1, Label="")]
public class _20_function_type__graph_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Scale By", Offset=28)]
public StringID _28_Scale_By;
}
[Value(Name="Apply Collision Damage Scale", Offset=88)]
public Single _88_Apply_Collision_Damage_Scale;
[Value(Name="Min Game Acc (Default)", Offset=92)]
public Single _92_Min_Game_Acc__Default_;
[Value(Name="Max Game Acc (Default)", Offset=96)]
public Single _96_Max_Game_Acc__Default_;
[Value(Name="Min Gam Scale (Default)", Offset=100)]
public Single _100_Min_Gam_Scale__Default_;
[Value(Name="Max Gam Scale (Default)", Offset=104)]
public Single _104_Max_Gam_Scale__Default_;
[Value(Name="Min abs Acc (Default)", Offset=108)]
public Single _108_Min_abs_Acc__Default_;
[Value(Name="Max abs Acc (Default)", Offset=112)]
public Single _112_Max_abs_Acc__Default_;
[Value(Name="Min abs Scale (Default)", Offset=116)]
public Single _116_Min_abs_Scale__Default_;
[Value(Name="Max abs Scale (Default)", Offset=120)]
public Single _120_Max_abs_Scale__Default_;
[Value(Name="Hud Text Message Index", Offset=124)]
public Int16 _124_Hud_Text_Message_Index;
[Value(Offset=126)]
public Byte[] _126_ = new byte[2];
public _128_attachments[] _128_Attachments;
[Serializable][Reflexive(Name="Attachments", Offset=128, ChunkSize=24, Label="")]
public class _128_attachments
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
[Value(Name="Marker", Offset=8)]
public StringID _8_Marker;
[Value(Name="Change Color", Offset=12)]
public StringID _12_Change_Color;
[Value(Name="Primary Scale", Offset=16)]
public StringID _16_Primary_Scale;
[Value(Name="Secondary Scale", Offset=20)]
public StringID _20_Secondary_Scale;
}
public _136_widgets[] _136_Widgets;
[Serializable][Reflexive(Name="Widgets", Offset=136, ChunkSize=8, Label="")]
public class _136_widgets
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
}
public _144_old_functions[] _144_Old_Functions;
[Serializable][Reflexive(Name="Old Functions", Offset=144, ChunkSize=0, Label="")]
public class _144_old_functions
{
private int __StartOffset__;
}
public _152_change_colors[] _152_Change_Colors;
[Serializable][Reflexive(Name="Change Colors", Offset=152, ChunkSize=16, Label="")]
public class _152_change_colors
{
private int __StartOffset__;
public _0_initial_permutations[] _0_Initial_Permutations;
[Serializable][Reflexive(Name="Initial Permutations", Offset=0, ChunkSize=32, Label="")]
public class _0_initial_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Variant Name", Offset=28)]
public StringID _28_Variant_Name;
}
public _8_functions[] _8_Functions;
[Serializable][Reflexive(Name="Functions", Offset=8, ChunkSize=36, Label="")]
public class _8_functions
{
private int __StartOffset__;
[Value(Name="Scale Flags", Offset=0)]
public Bitmask _0_Scale_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Darken By", Offset=28)]
public StringID _28_Darken_By;
[Value(Name="Scale By", Offset=32)]
public StringID _32_Scale_By;
}
}
public _160_predicted_resource[] _160_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=160, ChunkSize=8, Label="")]
public class _160_predicted_resource
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Int16 _0_Type;
[Value(Name="Resource Index", Offset=2)]
public Int16 _2_Resource_Index;
[Value(Name="Tag Index", Offset=4)]
public Int32 _4_Tag_Index;
}
[Value(Name="Flags", Offset=168)]
public Bitmask _168_Flags = new Bitmask(new string[] { "unused","Infection Form","Immune To Falling Damage","Rotate While Airborne","Zapped By Sheilds","Attach Upon Impact","Not On Motion Sensor" }, 4);
[Value(Name="Default Team", Offset=172)]
public H2.DataTypes.Enum _172_Default_Team = new H2.DataTypes.Enum(new string[] {  "Deafult", "Player", "Human", "Covenant", "Flood", "Sentinel", "Heretic", "Prophet" }, 2);
[Value(Name="Motion Sensor Blip Size", Offset=174)]
public H2.DataTypes.Enum _174_Motion_Sensor_Blip_Size = new H2.DataTypes.Enum(new string[] {  "Medium", "Small", "Large" }, 2);
[Value(Name="Turning Velocity Max", Offset=176)]
public Single _176_Turning_Velocity_Max;
[Value(Name="Turning Acceleration Max", Offset=180)]
public Single _180_Turning_Acceleration_Max;
[Value(Name="Casual Turing Modifer", Offset=184)]
public Single _184_Casual_Turing_Modifer;
[Value(Name="Autoaim Width", Offset=188)]
public Single _188_Autoaim_Width;
[Value(Name="Flags", Offset=192)]
public Bitmask _192_Flags = new Bitmask(new string[] { "Centered At Origin","Shape Sperical","Use Player Physics","Climb Any Surface","Flying","Not Physical","Dead Character Collision Group" }, 4);
[Value(Name="Height Standing", Offset=196)]
public Single _196_Height_Standing;
[Value(Name="Height Crouching", Offset=200)]
public Single _200_Height_Crouching;
[Value(Name="Radius", Offset=204)]
public Single _204_Radius;
[Value(Name="Mass", Offset=208)]
public Single _208_Mass;
[Value(Name="Living Material Name", Offset=212)]
public StringID _212_Living_Material_Name;
[Value(Name="Dead Material Name", Offset=216)]
public StringID _216_Dead_Material_Name;
[Value(Offset=220)]
public Byte[] _220_ = new byte[4];
public _224_dead_sphere_shapes[] _224_Dead_Sphere_Shapes;
[Serializable][Reflexive(Name="Dead Sphere Shapes", Offset=224, ChunkSize=132, Label="")]
public class _224_dead_sphere_shapes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Material #", Offset=4)]
public Int16 _4_Material_No;
[Value(Name="Flags", Offset=6)]
public H2.DataTypes.Enum _6_Flags = new H2.DataTypes.Enum(new string[] {  "unused" }, 2);
[Value(Name="Relative Mass Scale", Offset=8)]
public Single _8_Relative_Mass_Scale;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Restitution", Offset=16)]
public Single _16_Restitution;
[Value(Name="Volume", Offset=20)]
public Single _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Phantom", Offset=28)]
public Int16 _28_Phantom;
[Value(Name="Size", Offset=30)]
public Int16 _30_Size;
[Value(Name="Count", Offset=32)]
public Int16 _32_Count;
[Value(Name="Radius", Offset=34)]
public Single _34_Radius;
[Value(Name="Size", Offset=38)]
public Int16 _38_Size;
[Value(Name="Count", Offset=40)]
public Int16 _40_Count;
[Value(Name="Rotation i i", Offset=42)]
public Single _42_Rotation_i_i;
[Value(Name="Rotation i j", Offset=46)]
public Single _46_Rotation_i_j;
[Value(Name="Rotation i k", Offset=50)]
public Single _50_Rotation_i_k;
[Value(Name="Rotation j i", Offset=54)]
public Single _54_Rotation_j_i;
[Value(Name="Rotation j j", Offset=58)]
public Single _58_Rotation_j_j;
[Value(Name="Rotation j k", Offset=62)]
public Single _62_Rotation_j_k;
[Value(Name="Rotation k i", Offset=66)]
public Single _66_Rotation_k_i;
[Value(Name="Rotation k j", Offset=70)]
public Single _70_Rotation_k_j;
[Value(Name="Rotation k k", Offset=74)]
public Single _74_Rotation_k_k;
[Value(Name="Translation i", Offset=78)]
public Single _78_Translation_i;
[Value(Name="Translation j", Offset=82)]
public Single _82_Translation_j;
[Value(Name="Translation  k", Offset=86)]
public Single _86_Translation__k;
[Value(Offset=90)]
public Byte[] _90_ = new byte[42];
}
public _232_pill_shapes[] _232_Pill_Shapes;
[Serializable][Reflexive(Name="Pill Shapes", Offset=232, ChunkSize=80, Label="")]
public class _232_pill_shapes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Material #", Offset=4)]
public Int16 _4_Material_No;
[Value(Name="Flags", Offset=6)]
public H2.DataTypes.Enum _6_Flags = new H2.DataTypes.Enum(new string[] {  "unused" }, 2);
[Value(Name="Relative Mass Scale", Offset=8)]
public Single _8_Relative_Mass_Scale;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Restitution", Offset=16)]
public Single _16_Restitution;
[Value(Name="Volume", Offset=20)]
public Single _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Phantom", Offset=28)]
public Int16 _28_Phantom;
[Value(Name="Size", Offset=30)]
public Int16 _30_Size;
[Value(Name="Count", Offset=32)]
public Int16 _32_Count;
[Value(Name="Radius", Offset=34)]
public Single _34_Radius;
[Value(Name="Size", Offset=38)]
public Int16 _38_Size;
[Value(Name="Count", Offset=40)]
public Int16 _40_Count;
[Value(Name="Bottom i", Offset=42)]
public Single _42_Bottom_i;
[Value(Name="Bottom j", Offset=46)]
public Single _46_Bottom_j;
[Value(Name="Bottom k", Offset=50)]
public Single _50_Bottom_k;
[Value(Name="Top i", Offset=54)]
public Single _54_Top_i;
[Value(Name="Top j", Offset=58)]
public Single _58_Top_j;
[Value(Name="Top k", Offset=62)]
public Single _62_Top_k;
[Value(Offset=66)]
public Byte[] _66_ = new byte[14];
}
public _240_sphere_shapes[] _240_Sphere_Shapes;
[Serializable][Reflexive(Name="Sphere Shapes", Offset=240, ChunkSize=132, Label="")]
public class _240_sphere_shapes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Material #", Offset=4)]
public Int16 _4_Material_No;
[Value(Name="Flags", Offset=6)]
public H2.DataTypes.Enum _6_Flags = new H2.DataTypes.Enum(new string[] {  "unused" }, 2);
[Value(Name="Relative Mass Scale", Offset=8)]
public Single _8_Relative_Mass_Scale;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Restitution", Offset=16)]
public Single _16_Restitution;
[Value(Name="Volume", Offset=20)]
public Single _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Phantom", Offset=28)]
public Int16 _28_Phantom;
[Value(Name="Size", Offset=30)]
public Int16 _30_Size;
[Value(Name="Count", Offset=32)]
public Int16 _32_Count;
[Value(Name="Radius", Offset=34)]
public Single _34_Radius;
[Value(Name="Size", Offset=38)]
public Int16 _38_Size;
[Value(Name="Count", Offset=40)]
public Int16 _40_Count;
[Value(Name="Rotation i i", Offset=42)]
public Single _42_Rotation_i_i;
[Value(Name="Rotation i j", Offset=46)]
public Single _46_Rotation_i_j;
[Value(Name="Rotation i k", Offset=50)]
public Single _50_Rotation_i_k;
[Value(Name="Rotation j i", Offset=54)]
public Single _54_Rotation_j_i;
[Value(Name="Rotation j j", Offset=58)]
public Single _58_Rotation_j_j;
[Value(Name="Rotation j k", Offset=62)]
public Single _62_Rotation_j_k;
[Value(Name="Rotation k i", Offset=66)]
public Single _66_Rotation_k_i;
[Value(Name="Rotation k j", Offset=70)]
public Single _70_Rotation_k_j;
[Value(Name="Rotation k k", Offset=74)]
public Single _74_Rotation_k_k;
[Value(Name="Translation i", Offset=78)]
public Single _78_Translation_i;
[Value(Name="Translation j", Offset=82)]
public Single _82_Translation_j;
[Value(Name="Translation  k", Offset=86)]
public Single _86_Translation__k;
[Value(Offset=90)]
public Byte[] _90_ = new byte[42];
}
[Value(Name="Max Slope Angle", Offset=248)]
public Single _248_Max_Slope_Angle;
[Value(Name="Downhill Falloff Angle", Offset=252)]
public Single _252_Downhill_Falloff_Angle;
[Value(Name="Downhill Cutoff Angle", Offset=256)]
public Single _256_Downhill_Cutoff_Angle;
[Value(Name="Uphill Falloff Angle", Offset=260)]
public Single _260_Uphill_Falloff_Angle;
[Value(Name="Uphill Cutoff Angle", Offset=264)]
public Single _264_Uphill_Cutoff_Angle;
[Value(Name="Downhill Velocity Angle", Offset=268)]
public Single _268_Downhill_Velocity_Angle;
[Value(Name="Uphill Velocity Scale", Offset=272)]
public Single _272_Uphill_Velocity_Scale;
[Value(Name="Flying Bank Angle", Offset=276)]
public Single _276_Flying_Bank_Angle;
[Value(Name="Flying Bank Apply Time", Offset=280)]
public Single _280_Flying_Bank_Apply_Time;
[Value(Name="Flying Pitch Ratio", Offset=284)]
public Single _284_Flying_Pitch_Ratio;
[Value(Name="Flying Max Velocity", Offset=288)]
public Single _288_Flying_Max_Velocity;
[Value(Name="Flying Max Sidestep Velocity", Offset=292)]
public Single _292_Flying_Max_Sidestep_Velocity;
[Value(Name="Flying Acceleration", Offset=296)]
public Single _296_Flying_Acceleration;
[Value(Name="Flying Deceleration", Offset=300)]
public Single _300_Flying_Deceleration;
[Value(Name="Flying Angular Velocity Max", Offset=304)]
public Single _304_Flying_Angular_Velocity_Max;
[Value(Name="Flying Angular Acceleration Max", Offset=308)]
public Single _308_Flying_Angular_Acceleration_Max;
[Value(Name="Flying Crouch Velocity Modifier", Offset=312)]
public Single _312_Flying_Crouch_Velocity_Modifier;
[Value(Offset=316)]
public Byte[] _316_ = new byte[24];
[Value(Name="Sentinal Impact Damage", Offset=340)]
public Dependancy _340_Sentinal_Impact_Damage;
[Value(Name="Sentinal Sheild Impact Damage", Offset=344)]
public Dependancy _344_Sentinal_Sheild_Impact_Damage;
[Value(Name="Destroy After Death Time", Offset=348)]
public Single _348_Destroy_After_Death_Time;
[Value(Name="...To", Offset=352)]
public Single _352____To;
        }
    }
}