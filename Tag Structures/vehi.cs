using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("vehi")]
        public class vehi
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Does Not Cast Shadow","Search Cardinal Direction Lightmaps","unused","Not A Pathfinding Obstacle","Extension Of Parent","Does Not Cause Collision Damage","Early Mover","Early Mover Localized Physics","Use Static Massive Lightmap Sample","Object Scales Attachments","Inherits Player's Appearance","Dead Bipeds Can't Localize","Attach To Clusters By Dynamic Sphere","Effects Created By This Object Do Not" }, 4);
[Value(Name="Bounding Radius", Offset=4)]
public Single _4_Bounding_Radius;
[Value(Name="Bounding Offset X", Offset=8)]
public Single _8_Bounding_Offset_X;
[Value(Name="Bounding Offset Y", Offset=12)]
public Single _12_Bounding_Offset_Y;
[Value(Name="Bounding Offset Z", Offset=16)]
public Single _16_Bounding_Offset_Z;
[Value(Name="Acceleration Scale", Offset=20)]
public Single _20_Acceleration_Scale;
[Value(Name="Lightmap Shadow Mode", Offset=24)]
public H2.DataTypes.Enum _24_Lightmap_Shadow_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Never", "Always" }, 4);
[Value(Name="Sweetener Size", Offset=28)]
public H2.DataTypes.Enum _28_Sweetener_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Medium", "Large" }, 4);
[Value(Name="Dynamic Light Sphere Radius", Offset=32)]
public Single _32_Dynamic_Light_Sphere_Radius;
[Value(Name="Dynamic Light Sphere Offset X", Offset=36)]
public Single _36_Dynamic_Light_Sphere_Offset_X;
[Value(Name="Dynamic Light Sphere Offset Y", Offset=40)]
public Single _40_Dynamic_Light_Sphere_Offset_Y;
[Value(Name="Dynamic Light Sphere Offset Z", Offset=44)]
public Single _44_Dynamic_Light_Sphere_Offset_Z;
[Value(Name="Default Model Variant", Offset=48)]
public StringID _48_Default_Model_Variant;
[Value(Name="Model", Offset=52)]
public Dependancy _52_Model;
[Value(Name="Crate Object", Offset=56)]
public Dependancy _56_Crate_Object;
[Value(Name="Modifier Shader", Offset=60)]
public Dependancy _60_Modifier_Shader;
[Value(Name="Creation Effect", Offset=64)]
public Dependancy _64_Creation_Effect;
[Value(Name="Material Effects", Offset=68)]
public Dependancy _68_Material_Effects;
public _72_ai_properties[] _72_Ai_Properties;
[Serializable][Reflexive(Name="Ai Properties", Offset=72, ChunkSize=16, Label="")]
public class _72_ai_properties
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Destroyable Cover","Pathfinding Ignore When Dead","Dynamic Cover" }, 4);
[Value(Name="Type Name", Offset=4)]
public StringID _4_Type_Name;
[Value(Name="Size", Offset=8)]
public H2.DataTypes.Enum _8_Size = new H2.DataTypes.Enum(new string[] {  "Default", "Tiny", "Small", "Medium", "Large", "Huge", "Immobile" }, 4);
[Value(Name="Leap Jump Speed", Offset=12)]
public H2.DataTypes.Enum _12_Leap_Jump_Speed = new H2.DataTypes.Enum(new string[] {  "None", "Down", "Step", "Crouch", "Stand", "Storey", "Tower", "Infinite" }, 4);
}
public _80_functions[] _80_Functions;
[Serializable][Reflexive(Name="Functions", Offset=80, ChunkSize=32, Label="")]
public class _80_functions
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invert","Mapping Does Not Controls Active","Always Active","Random Time Offset" }, 4);
[Value(Name="Import Name", Offset=4)]
public StringID _4_Import_Name;
[Value(Name="Export Name", Offset=8)]
public StringID _8_Export_Name;
[Value(Name="Turn Off With", Offset=12)]
public StringID _12_Turn_Off_With;
[Value(Name="Min Value", Offset=16)]
public Single _16_Min_Value;
public _20_function_type__graph_[] _20_Function_Type__Graph_;
[Serializable][Reflexive(Name="Function Type (Graph)", Offset=20, ChunkSize=1, Label="")]
public class _20_function_type__graph_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Scale By", Offset=28)]
public StringID _28_Scale_By;
}
[Value(Name="Apply Collision Damage Scale", Offset=88)]
public Single _88_Apply_Collision_Damage_Scale;
[Value(Name="Min Game Acc (Default)", Offset=92)]
public Single _92_Min_Game_Acc__Default_;
[Value(Name="Max Game Acc (Default)", Offset=96)]
public Single _96_Max_Game_Acc__Default_;
[Value(Name="Min Gam Scale (Default)", Offset=100)]
public Single _100_Min_Gam_Scale__Default_;
[Value(Name="Max Gam Scale (Default)", Offset=104)]
public Single _104_Max_Gam_Scale__Default_;
[Value(Name="Min abs Acc (Default)", Offset=108)]
public Single _108_Min_abs_Acc__Default_;
[Value(Name="Max abs Acc (Default)", Offset=112)]
public Single _112_Max_abs_Acc__Default_;
[Value(Name="Min abs Scale (Default)", Offset=116)]
public Single _116_Min_abs_Scale__Default_;
[Value(Name="Max abs Scale (Default)", Offset=120)]
public Single _120_Max_abs_Scale__Default_;
[Value(Name="Hud Text Message Index", Offset=124)]
public Int16 _124_Hud_Text_Message_Index;
[Value(Offset=126)]
public Byte[] _126_ = new byte[2];
public _128_attachments[] _128_Attachments;
[Serializable][Reflexive(Name="Attachments", Offset=128, ChunkSize=24, Label="")]
public class _128_attachments
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
[Value(Name="Marker", Offset=8)]
public StringID _8_Marker;
[Value(Name="Change Color", Offset=12)]
public StringID _12_Change_Color;
[Value(Name="Primary Scale", Offset=16)]
public StringID _16_Primary_Scale;
[Value(Name="Secondary Scale", Offset=20)]
public StringID _20_Secondary_Scale;
}
public _136_widgets[] _136_Widgets;
[Serializable][Reflexive(Name="Widgets", Offset=136, ChunkSize=8, Label="")]
public class _136_widgets
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
}
public _144_old_functions[] _144_Old_Functions;
[Serializable][Reflexive(Name="Old Functions", Offset=144, ChunkSize=0, Label="")]
public class _144_old_functions
{
private int __StartOffset__;
}
public _152_change_colors[] _152_Change_Colors;
[Serializable][Reflexive(Name="Change Colors", Offset=152, ChunkSize=16, Label="")]
public class _152_change_colors
{
private int __StartOffset__;
public _0_initial_permutations[] _0_Initial_Permutations;
[Serializable][Reflexive(Name="Initial Permutations", Offset=0, ChunkSize=32, Label="")]
public class _0_initial_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Variant Name", Offset=28)]
public StringID _28_Variant_Name;
}
public _8_functions[] _8_Functions;
[Serializable][Reflexive(Name="Functions", Offset=8, ChunkSize=36, Label="")]
public class _8_functions
{
private int __StartOffset__;
[Value(Name="Scale Flags", Offset=0)]
public Bitmask _0_Scale_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Darken By", Offset=28)]
public StringID _28_Darken_By;
[Value(Name="Scale By", Offset=32)]
public StringID _32_Scale_By;
}
}
public _160_predicted_resource[] _160_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=160, ChunkSize=8, Label="")]
public class _160_predicted_resource
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Int16 _0_Type;
[Value(Name="Resource Index", Offset=2)]
public Int16 _2_Resource_Index;
[Value(Name="Tag Index", Offset=4)]
public Int32 _4_Tag_Index;
}
[Value(Name="Flags", Offset=168)]
public Bitmask _168_Flags = new Bitmask(new string[] { "Circular Aiming","Destroyed After Dying","Half-speed Interpolation","Fires From Camera","Entrance Inside Bounding sphere ","Doesn't Show Readied Weapon","Couses Passenger Dialogue","Resists Pings","Melee Attack Is Fatal","Don't Reface During Pings","Has No Aiming","Simple Creature","Impact Melle Attaches To Unit","Impact Melee Dies On Shield","Cannot Open Doors Automatically","Melee Attackers Cannot Attach","Not Instantly Killed By Melee","Shield Sapping","Runs Around Flaming","Inconsequential","Special Cinematic Unit","Ignored By AutoAiming","Shields Fry Infection Forms","unused","unused","Acts As Gunner For Parent","Controlled By Parent Gunner","Parent's Primary Weapon","Unit Has Boost" }, 4);
[Value(Name="Default Team", Offset=172)]
public H2.DataTypes.Enum _172_Default_Team = new H2.DataTypes.Enum(new string[] {  "Deafult", "Player", "Human", "Covenant", "Flood", "Sentinel", "Heretic", "Prophet" }, 2);
[Value(Name="Constant Sound Volume", Offset=174)]
public H2.DataTypes.Enum _174_Constant_Sound_Volume = new H2.DataTypes.Enum(new string[] {  "Silent", "Medium", "Loud", "Shout", "Quiet" }, 2);
[Value(Name="Integrated Light Toggle", Offset=176)]
public Dependancy _176_Integrated_Light_Toggle;
[Value(Name="Camera Field of View", Offset=180)]
public Single _180_Camera_Field_of_View;
[Value(Name="Camera Stiffness", Offset=184)]
public Single _184_Camera_Stiffness;
[Value(Name="Camera Marker Name", Offset=188)]
public StringID _188_Camera_Marker_Name;
[Value(Name="Camera Submerged Marker Name", Offset=192)]
public StringID _192_Camera_Submerged_Marker_Name;
[Value(Name="Pitch Auto-Level", Offset=196)]
public Single _196_Pitch_Auto_Level;
[Value(Name="Pitch Range", Offset=200)]
public Single _200_Pitch_Range;
[Value(Name="...To", Offset=204)]
public Single _204____To;
public _208_camera_tracks[] _208_Camera_Tracks;
[Serializable][Reflexive(Name="Camera Tracks", Offset=208, ChunkSize=8, Label="")]
public class _208_camera_tracks
{
private int __StartOffset__;
[Value(Name="Track", Offset=0)]
public Dependancy _0_Track;
}
[Value(Name="Acceleration Scale i", Offset=216)]
public Single _216_Acceleration_Scale_i;
[Value(Name="Acceleration Scale j", Offset=220)]
public Single _220_Acceleration_Scale_j;
[Value(Name="Acceleration Scale k", Offset=224)]
public Single _224_Acceleration_Scale_k;
[Value(Name="Acceleration Action Scale", Offset=228)]
public Single _228_Acceleration_Action_Scale;
[Value(Name="Acceleration Attach Scale", Offset=232)]
public Single _232_Acceleration_Attach_Scale;
[Value(Name="Soft Ping Threshold", Offset=236)]
public Single _236_Soft_Ping_Threshold;
[Value(Name="Soft Ping Interrupt Time", Offset=240)]
public Single _240_Soft_Ping_Interrupt_Time;
[Value(Name="Hard Ping Threshold", Offset=244)]
public Single _244_Hard_Ping_Threshold;
[Value(Name="Hard Ping Interrupt Time", Offset=248)]
public Single _248_Hard_Ping_Interrupt_Time;
[Value(Name="Hard Ping Death Threshold", Offset=252)]
public Single _252_Hard_Ping_Death_Threshold;
[Value(Name="Feign Death Threshold", Offset=256)]
public Single _256_Feign_Death_Threshold;
[Value(Name="Feign Death Time", Offset=260)]
public Single _260_Feign_Death_Time;
[Value(Name="Dist Of Evade Anim", Offset=264)]
public Single _264_Dist_Of_Evade_Anim;
[Value(Name="Dist of Dive Anim", Offset=268)]
public Single _268_Dist_of_Dive_Anim;
[Value(Name="Stunned Movement Threshold", Offset=272)]
public Single _272_Stunned_Movement_Threshold;
[Value(Name="Feign Death Chance", Offset=276)]
public Single _276_Feign_Death_Chance;
[Value(Name="Feign Repeat Chance", Offset=280)]
public Single _280_Feign_Repeat_Chance;
[Value(Name="Spawned Turret Actor", Offset=284)]
public Dependancy _284_Spawned_Turret_Actor;
[Value(Name="Spawned Actor Count", Offset=288)]
public Int16 _288_Spawned_Actor_Count;
[Value(Name="...To", Offset=290)]
public Int16 _290____To;
[Value(Name="Spawned Velocity", Offset=292)]
public Single _292_Spawned_Velocity;
[Value(Name="Aiming Velocity Max", Offset=296)]
public Single _296_Aiming_Velocity_Max;
[Value(Name="Aiming Accel Max", Offset=300)]
public Single _300_Aiming_Accel_Max;
[Value(Name="Casual Aiming Modifier", Offset=304)]
public Single _304_Casual_Aiming_Modifier;
[Value(Name="Looking Velocity Max", Offset=308)]
public Single _308_Looking_Velocity_Max;
[Value(Name="Looking Accel Max", Offset=312)]
public Single _312_Looking_Accel_Max;
[Value(Name="Right Hand Node", Offset=316)]
public StringID _316_Right_Hand_Node;
[Value(Name="Left Hand Node", Offset=320)]
public StringID _320_Left_Hand_Node;
[Value(Name="Preferred Gun Node", Offset=324)]
public StringID _324_Preferred_Gun_Node;
[Value(Name="Melee Damage", Offset=328)]
public Dependancy _328_Melee_Damage;
[Value(Name="Boarding Melee Damage", Offset=332)]
public Dependancy _332_Boarding_Melee_Damage;
[Value(Name="Boarding Melee Response", Offset=336)]
public Dependancy _336_Boarding_Melee_Response;
[Value(Name="Landing Melee Damage", Offset=340)]
public Dependancy _340_Landing_Melee_Damage;
[Value(Name="Flurry Melee Damage", Offset=344)]
public Dependancy _344_Flurry_Melee_Damage;
[Value(Name="Obstacle Smash Damage", Offset=348)]
public Dependancy _348_Obstacle_Smash_Damage;
[Value(Name="Motion Sensor Blip Size", Offset=352)]
public H2.DataTypes.Enum _352_Motion_Sensor_Blip_Size = new H2.DataTypes.Enum(new string[] {  "Medium", "Small", "Large" }, 4);
public _356_postures[] _356_Postures;
[Serializable][Reflexive(Name="Postures", Offset=356, ChunkSize=16, Label="")]
public class _356_postures
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Pill Offset i", Offset=4)]
public Single _4_Pill_Offset_i;
[Value(Name="Pill Offset j", Offset=8)]
public Single _8_Pill_Offset_j;
[Value(Name="Pill Offset k", Offset=12)]
public Single _12_Pill_Offset_k;
}
public _364_new_hud_interfaces[] _364_New_Hud_Interfaces;
[Serializable][Reflexive(Name="New Hud Interfaces", Offset=364, ChunkSize=8, Label="")]
public class _364_new_hud_interfaces
{
private int __StartOffset__;
[Value(Name="New Unit Hud Interface", Offset=0)]
public Dependancy _0_New_Unit_Hud_Interface;
}
public _372_dialogue_variants[] _372_Dialogue_Variants;
[Serializable][Reflexive(Name="Dialogue Variants", Offset=372, ChunkSize=24, Label="")]
public class _372_dialogue_variants
{
private int __StartOffset__;
[Value(Name="Variant Number", Offset=0)]
public Int16 _0_Variant_Number;
[Value(Offset=2)]
public Byte[] _2_ = new byte[10];
[Value(Name="Dialogue", Offset=12)]
public Dependancy _12_Dialogue;
[Value(Offset=20)]
public Byte[] _20_ = new byte[4];
}
[Value(Name="Grenade Velocity", Offset=380)]
public Single _380_Grenade_Velocity;
[Value(Name="Grenade Type", Offset=384)]
public H2.DataTypes.Enum _384_Grenade_Type = new H2.DataTypes.Enum(new string[] {  "Human Fragmentation", "Covenant Plasma" }, 2);
[Value(Name="Grenade Count", Offset=386)]
public UInt16 _386_Grenade_Count;
public _388_powered_seats[] _388_Powered_Seats;
[Serializable][Reflexive(Name="Powered Seats", Offset=388, ChunkSize=8, Label="")]
public class _388_powered_seats
{
private int __StartOffset__;
[Value(Name="Driver Powerup Time", Offset=0)]
public Single _0_Driver_Powerup_Time;
[Value(Name="Driver Powerdown Time", Offset=4)]
public Single _4_Driver_Powerdown_Time;
}
public _396_weapon[] _396_Weapon;
[Serializable][Reflexive(Name="Weapon", Offset=396, ChunkSize=8, Label="")]
public class _396_weapon
{
private int __StartOffset__;
[Value(Name="Weapon", Offset=0)]
public Dependancy _0_Weapon;
}
public _404_seats[] _404_Seats;
[Serializable][Reflexive(Name="Seats", Offset=404, ChunkSize=176, Label="")]
public class _404_seats
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invisible","Locked","Driver","Gunner","3rd Person Camera","Allows Weapons","3rd Person On Enter","1st Person Camera Slaved To Gun","Allow Vehicle Communication Animations","Not Valid Without Driver","Allow AI NonCombatants","Boarding Seat","AI Firing Disabled By Max Acceleration","Boarding Enters Seat","Boarding Need Any Passenger","Invaild For Player","Invaild For Non-Player","Gunner (Player Only)","Invisible Under Major Damage" }, 4);
[Value(Name="Label", Offset=4)]
public StringID _4_Label;
[Value(Name="Sitting Postion Marker", Offset=8)]
public StringID _8_Sitting_Postion_Marker;
[Value(Name="Entry Marker(s) Name", Offset=12)]
public StringID _12_Entry_Marker_s__Name;
[Value(Name="Boarding Grenade Marker", Offset=16)]
public StringID _16_Boarding_Grenade_Marker;
[Value(Name="Boarding Grenade String", Offset=20)]
public StringID _20_Boarding_Grenade_String;
[Value(Name="Boarding Melee String", Offset=24)]
public StringID _24_Boarding_Melee_String;
[Value(Name="Ping Scale", Offset=28)]
public Single _28_Ping_Scale;
[Value(Name="Turnover Time (sec)", Offset=32)]
public Single _32_Turnover_Time__sec_;
[Value(Name="Accel Range i", Offset=36)]
public Single _36_Accel_Range_i;
[Value(Name="Accel Range j", Offset=40)]
public Single _40_Accel_Range_j;
[Value(Name="Accel Range k", Offset=44)]
public Single _44_Accel_Range_k;
[Value(Name="Accel Action Scale", Offset=48)]
public Single _48_Accel_Action_Scale;
[Value(Name="Accel Attach Scale", Offset=52)]
public Single _52_Accel_Attach_Scale;
[Value(Name="AI Scariness", Offset=56)]
public Single _56_AI_Scariness;
[Value(Name="AI Seat Type", Offset=60)]
public H2.DataTypes.Enum _60_AI_Seat_Type = new H2.DataTypes.Enum(new string[] {  "None", "Passenger", "Gunner", "Small Cargo", "Large Cargo", "Driver" }, 2);
[Value(Name="Boarding Seat #", Offset=62)]
public Int16 _62_Boarding_Seat_No;
[Value(Name="Listener Interpolation Factor", Offset=64)]
public Single _64_Listener_Interpolation_Factor;
[Value(Name="Yaw Rate Bounds", Offset=68)]
public Single _68_Yaw_Rate_Bounds;
[Value(Name="...To", Offset=72)]
public Single _72____To;
[Value(Name="Pitch Rate Bounds", Offset=76)]
public Single _76_Pitch_Rate_Bounds;
[Value(Name="...To", Offset=80)]
public Single _80____To;
[Value(Name="Min Speed Ref", Offset=84)]
public Single _84_Min_Speed_Ref;
[Value(Name="Max Speed Ref", Offset=88)]
public Single _88_Max_Speed_Ref;
[Value(Name="Speed Exponent", Offset=92)]
public Single _92_Speed_Exponent;
[Value(Name="Camera Marker Name", Offset=96)]
public StringID _96_Camera_Marker_Name;
[Value(Name="Camera Submerged Marker name", Offset=100)]
public StringID _100_Camera_Submerged_Marker_name;
[Value(Name="Pitch Auto-level", Offset=104)]
public Single _104_Pitch_Auto_level;
[Value(Name="Pitch Range Lower", Offset=108)]
public Single _108_Pitch_Range_Lower;
[Value(Name="Pitch Range Upper", Offset=112)]
public Single _112_Pitch_Range_Upper;
public _116_camera_tracks[] _116_Camera_Tracks;
[Serializable][Reflexive(Name="Camera Tracks", Offset=116, ChunkSize=8, Label="")]
public class _116_camera_tracks
{
private int __StartOffset__;
[Value(Name="Track", Offset=0)]
public Dependancy _0_Track;
}
public _124_unit_hud_interface[] _124_Unit_Hud_Interface;
[Serializable][Reflexive(Name="Unit Hud Interface", Offset=124, ChunkSize=8, Label="")]
public class _124_unit_hud_interface
{
private int __StartOffset__;
[Value(Name="New Unit Hud Interface", Offset=0)]
public Dependancy _0_New_Unit_Hud_Interface;
}
[Value(Name="Enter Seat String", Offset=132)]
public StringID _132_Enter_Seat_String;
[Value(Name="Yaw Min", Offset=136)]
public Single _136_Yaw_Min;
[Value(Name="Yaw Max", Offset=140)]
public Single _140_Yaw_Max;
[Value(Name="Built-in Gunner", Offset=144)]
public Dependancy _144_Built_in_Gunner;
[Value(Name="Entry Radius", Offset=152)]
public Single _152_Entry_Radius;
[Value(Name="Entry Marker Cone Angle", Offset=156)]
public Single _156_Entry_Marker_Cone_Angle;
[Value(Name="Entry Marker Facing Angle", Offset=160)]
public Single _160_Entry_Marker_Facing_Angle;
[Value(Name="Maximum Relative Velocity", Offset=164)]
public Single _164_Maximum_Relative_Velocity;
[Value(Name="Invisible Seat Region", Offset=168)]
public StringID _168_Invisible_Seat_Region;
[Value(Name="Runtime Invisible Seat Region Index", Offset=172)]
public Single _172_Runtime_Invisible_Seat_Region_Index;
}
[Value(Name="Boost Peak Power", Offset=412)]
public Single _412_Boost_Peak_Power;
[Value(Name="Boost Rise Power", Offset=416)]
public Single _416_Boost_Rise_Power;
[Value(Name="Boost Peak Time", Offset=420)]
public Single _420_Boost_Peak_Time;
[Value(Name="Boost Fall Power", Offset=424)]
public Single _424_Boost_Fall_Power;
[Value(Name="Dead Time", Offset=428)]
public Single _428_Dead_Time;
[Value(Name="Lip Sync Attack Weight", Offset=432)]
public Single _432_Lip_Sync_Attack_Weight;
[Value(Name="Lip Sync decay Weight", Offset=436)]
public Single _436_Lip_Sync_decay_Weight;
[Value(Name="Flags", Offset=440)]
public Bitmask _440_Flags = new Bitmask(new string[] { "Speed Wakes Physics","Turn Wakes Physics","Driver Power Wakes Physics","Gunner Power Wakes Physics","Control Opposite Speed Sets Brake","Slide Wakes Physics","Kills Riders at Terminal Velocity","Causes Collision Damage","AI Weapon Cannot Rotate","AI Does Not Require Driver","AI Unused","AI Driver Enable","AI Driver Flying","AI Driver Can-Sidestep","AI Driver Hovering","Vehicle Steers Directly","Unused","Has E-Brake","Noncombat Vehicle","No Friction W/Driver","Can Trigger Automatic Opening doors","Autoaim When Teamless" }, 4);
[Value(Name="Type", Offset=444)]
public H2.DataTypes.Enum _444_Type = new H2.DataTypes.Enum(new string[] {  "Human Tank", "Human Jeep", "Human Boat", "Human Plane", "Covie Scout", "Covie Fighter", "Turret" }, 2);
[Value(Name="Control", Offset=446)]
public H2.DataTypes.Enum _446_Control = new H2.DataTypes.Enum(new string[] {  "Vehicle Control Normal", "Vehicle Control Unused", "Vehicle Control Tank" }, 2);
[Value(Name="Maximum Forward Speed", Offset=448)]
public Single _448_Maximum_Forward_Speed;
[Value(Name="Max Reverse Speed", Offset=452)]
public Single _452_Max_Reverse_Speed;
[Value(Name="Speed Accel", Offset=456)]
public Single _456_Speed_Accel;
[Value(Name="Speed Decel", Offset=460)]
public Single _460_Speed_Decel;
[Value(Name="Max Left Turn", Offset=464)]
public Single _464_Max_Left_Turn;
[Value(Name="Max Right Turn (negative)", Offset=468)]
public Single _468_Max_Right_Turn__negative_;
[Value(Name="Wheel Circumference", Offset=472)]
public Single _472_Wheel_Circumference;
[Value(Name="Turn Rate", Offset=476)]
public Single _476_Turn_Rate;
[Value(Name="Blur Speed", Offset=480)]
public Single _480_Blur_Speed;
[Value(Name="Specific Type", Offset=484)]
public H2.DataTypes.Enum _484_Specific_Type = new H2.DataTypes.Enum(new string[] {  "None", "Ghost", "Wraith", "Spectre", "Sentinal Enforcer" }, 2);
[Value(Name="Player Training Vehicle Type", Offset=486)]
public H2.DataTypes.Enum _486_Player_Training_Vehicle_Type = new H2.DataTypes.Enum(new string[] {  "None", "Warthog", "Warthog Turret", "Ghost", "Banshee", "Tank", "Wraith" }, 2);
[Value(Name="Flip message", Offset=488)]
public StringID _488_Flip_message;
[Value(Name="Turn Scale", Offset=492)]
public Single _492_Turn_Scale;
[Value(Name="Speed Turn Penalty Power (0.5 .. 2)", Offset=496)]
public Single _496_Speed_Turn_Penalty_Power__0_5____2_;
[Value(Name="Speed Turn Penalty (0 = none, 1 = can't turn at top speed)", Offset=500)]
public Single _500_Speed_Turn_Penalty__0___none__1___can_t_turn_at_top_speed_;
[Value(Name="Maximum Left Slide", Offset=504)]
public Single _504_Maximum_Left_Slide;
[Value(Name="Maximum Right Slide", Offset=508)]
public Single _508_Maximum_Right_Slide;
[Value(Name="Slide Acceleration", Offset=512)]
public Single _512_Slide_Acceleration;
[Value(Name="Slide Deceleration", Offset=516)]
public Single _516_Slide_Deceleration;
[Value(Name="Minimum Flipping Angular Velocity", Offset=520)]
public Single _520_Minimum_Flipping_Angular_Velocity;
[Value(Name="Maximum Flipping Angular Velocity", Offset=524)]
public Single _524_Maximum_Flipping_Angular_Velocity;
[Value(Name="Vehicle Size", Offset=528)]
public H2.DataTypes.Enum _528_Vehicle_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Large" }, 4);
[Value(Name="Fixed Gun Yaw", Offset=532)]
public Single _532_Fixed_Gun_Yaw;
[Value(Name="Fixed Gun Pitch", Offset=536)]
public Single _536_Fixed_Gun_Pitch;
[Value(Name="Steering Overdampening Cusp Angle (degrees)", Offset=540)]
public Single _540_Steering_Overdampening_Cusp_Angle__degrees_;
[Value(Name="Steering Overdampening Exponent", Offset=544)]
public Single _544_Steering_Overdampening_Exponent;
[Value(Name="Crouch Transition Time (seconds)", Offset=548)]
public Single _548_Crouch_Transition_Time__seconds_;
[Value(Offset=552)]
public Byte[] _552_ = new byte[4];
[Value(Name="Engine", Offset=556)]
public Single _556_Engine;
[Value(Name="Engine Max Angular Velocity", Offset=560)]
public Single _560_Engine_Max_Angular_Velocity;
public _564_gears__0_reverse__1_1st_gear_etc_[] _564_Gears__0_Reverse__1_1st_Gear_etc_;
[Serializable][Reflexive(Name="Gears (0=Reverse, 1=1st Gear etc)", Offset=564, ChunkSize=68, Label="")]
public class _564_gears__0_reverse__1_1st_gear_etc_
{
private int __StartOffset__;
[Value(Name="Loaded Torque - Min Torque", Offset=0)]
public Single _0_Loaded_Torque___Min_Torque;
[Value(Name="Loaded Torque - Max Torque", Offset=4)]
public Single _4_Loaded_Torque___Max_Torque;
[Value(Name="Loaded Torque - Peak Torque Scale", Offset=8)]
public Single _8_Loaded_Torque___Peak_Torque_Scale;
[Value(Name="Loaded Torque - Past Peak Torque Exponent", Offset=12)]
public Single _12_Loaded_Torque___Past_Peak_Torque_Exponent;
[Value(Name="Loaded Torque - Torque At Max Angular Velocity", Offset=16)]
public Single _16_Loaded_Torque___Torque_At_Max_Angular_Velocity;
[Value(Name="Loaded Torque - Torque At 2X Max Angular Velocity", Offset=20)]
public Single _20_Loaded_Torque___Torque_At_2X_Max_Angular_Velocity;
[Value(Name="Cruising Torque - Min Torque", Offset=24)]
public Single _24_Cruising_Torque___Min_Torque;
[Value(Name="Cruising Torque - Max Torque", Offset=28)]
public Single _28_Cruising_Torque___Max_Torque;
[Value(Name="Cruising Torque - Peak Torque Scale", Offset=32)]
public Single _32_Cruising_Torque___Peak_Torque_Scale;
[Value(Name="Cruising Torque - Past Peak Torque Exponent", Offset=36)]
public Single _36_Cruising_Torque___Past_Peak_Torque_Exponent;
[Value(Name="Cruising Torque - Torque At Max Angular Velocity", Offset=40)]
public Single _40_Cruising_Torque___Torque_At_Max_Angular_Velocity;
[Value(Name="Cruising Torque - Torque At 2X Max Angular Velocity", Offset=44)]
public Single _44_Cruising_Torque___Torque_At_2X_Max_Angular_Velocity;
[Value(Name="Min Time To Upshift", Offset=48)]
public Single _48_Min_Time_To_Upshift;
[Value(Name="Engine Up-Shift Scale", Offset=52)]
public Single _52_Engine_Up_Shift_Scale;
[Value(Name="Gear Ratio", Offset=56)]
public Single _56_Gear_Ratio;
[Value(Name="Min Time To Downshift", Offset=60)]
public Single _60_Min_Time_To_Downshift;
[Value(Name="Engine Down-Shift Scale", Offset=64)]
public Single _64_Engine_Down_Shift_Scale;
}
[Value(Name="Flying Torque Scale", Offset=572)]
public Single _572_Flying_Torque_Scale;
[Value(Name="Seat Enterance Acceleration Scale", Offset=576)]
public Single _576_Seat_Enterance_Acceleration_Scale;
[Value(Name="Seat Exit Accelersation Scale", Offset=580)]
public Single _580_Seat_Exit_Accelersation_Scale;
[Value(Name="Air Friction Deceleration", Offset=584)]
public Single _584_Air_Friction_Deceleration;
[Value(Name="Thrust Scale", Offset=588)]
public Single _588_Thrust_Scale;
[Value(Name="Suspension Sound", Offset=592)]
public Dependancy _592_Suspension_Sound;
[Value(Name="Crash Sound", Offset=596)]
public Dependancy _596_Crash_Sound;
[Value(Name="unused", Offset=600)]
public Dependancy _600_unused;
[Value(Name="Special Effect", Offset=604)]
public Dependancy _604_Special_Effect;
[Value(Name="Unused Effect", Offset=608)]
public Dependancy _608_Unused_Effect;
[Value(Name="Flags", Offset=612)]
public Bitmask _612_Flags = new Bitmask(new string[] { "Invaild" }, 4);
[Value(Name="Ground Friction", Offset=616)]
public Single _616_Ground_Friction;
[Value(Name="Ground Depth", Offset=620)]
public Single _620_Ground_Depth;
[Value(Name="Ground Damp Factor", Offset=624)]
public Single _624_Ground_Damp_Factor;
[Value(Name="Ground Moving Friction", Offset=628)]
public Single _628_Ground_Moving_Friction;
[Value(Name="Ground Maximum Slope 0", Offset=632)]
public Single _632_Ground_Maximum_Slope_0;
[Value(Name="Ground Maximum Slope 1", Offset=636)]
public Single _636_Ground_Maximum_Slope_1;
[Value(Offset=640)]
public Byte[] _640_ = new byte[16];
[Value(Name="Anti Gravity Bank Lift", Offset=656)]
public Single _656_Anti_Gravity_Bank_Lift;
[Value(Name="Steering Bank Reaction Scale", Offset=660)]
public Single _660_Steering_Bank_Reaction_Scale;
[Value(Name="Gravity Scale", Offset=664)]
public Single _664_Gravity_Scale;
[Value(Name="Radius", Offset=668)]
public Single _668_Radius;
public _672_antigrav_points[] _672_Antigrav_Points;
[Serializable][Reflexive(Name="Antigrav Points", Offset=672, ChunkSize=76, Label="")]
public class _672_antigrav_points
{
private int __StartOffset__;
[Value(Name="Marker Name", Offset=0)]
public StringID _0_Marker_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Gets Damage From Region" }, 4);
[Value(Name="Antigrav Strength", Offset=8)]
public Single _8_Antigrav_Strength;
[Value(Name="Antigrav Offset", Offset=12)]
public Single _12_Antigrav_Offset;
[Value(Name="Antigrav Height", Offset=16)]
public Single _16_Antigrav_Height;
[Value(Name="Antigrav Damp Factor", Offset=20)]
public Single _20_Antigrav_Damp_Factor;
[Value(Name="Antigrav Normal K1", Offset=24)]
public Single _24_Antigrav_Normal_K1;
[Value(Name="Antigrav Normal K0", Offset=28)]
public Single _28_Antigrav_Normal_K0;
[Value(Name="Radius", Offset=32)]
public Single _32_Radius;
[Value(Offset=36)]
public Byte[] _36_ = new byte[16];
[Value(Name="Damage Source Region Name", Offset=52)]
public StringID _52_Damage_Source_Region_Name;
[Value(Name="Default State Error", Offset=56)]
public Single _56_Default_State_Error;
[Value(Name="Minor Damage Error", Offset=60)]
public Single _60_Minor_Damage_Error;
[Value(Name="Medium Damage Error", Offset=64)]
public Single _64_Medium_Damage_Error;
[Value(Name="Major Damage Error", Offset=68)]
public Single _68_Major_Damage_Error;
[Value(Name="Destoryed State Error", Offset=72)]
public Single _72_Destoryed_State_Error;
}
public _680_friction_points[] _680_Friction_Points;
[Serializable][Reflexive(Name="Friction Points", Offset=680, ChunkSize=76, Label="")]
public class _680_friction_points
{
private int __StartOffset__;
[Value(Name="Marker Name", Offset=0)]
public StringID _0_Marker_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Gets Damage From Region","Powered","Front Turning","Rear Turning","Attached To E-Brake","Can Be Destroyed" }, 4);
[Value(Name="Fraction Of Total Mass", Offset=8)]
public Single _8_Fraction_Of_Total_Mass;
[Value(Name="Radius", Offset=12)]
public Single _12_Radius;
[Value(Name="Damaged Radius", Offset=16)]
public Single _16_Damaged_Radius;
[Value(Name="Friction Type", Offset=20)]
public H2.DataTypes.Enum _20_Friction_Type = new H2.DataTypes.Enum(new string[] {  "Point", "Forward" }, 4);
[Value(Name="Moving Friction Velocity Diff", Offset=24)]
public Single _24_Moving_Friction_Velocity_Diff;
[Value(Name="E-Brake Moving Friction", Offset=28)]
public Single _28_E_Brake_Moving_Friction;
[Value(Name="E-Brake Friction", Offset=32)]
public Single _32_E_Brake_Friction;
[Value(Name="E-Brake Moving Friction Vel Diff", Offset=36)]
public Single _36_E_Brake_Moving_Friction_Vel_Diff;
[Value(Offset=40)]
public Byte[] _40_ = new byte[20];
[Value(Name="Collision Global Material Name", Offset=60)]
public StringID _60_Collision_Global_Material_Name;
[Value(Name="Model State Destroyed", Offset=64)]
public H2.DataTypes.Enum _64_Model_State_Destroyed = new H2.DataTypes.Enum(new string[] {  "Default", "Minor Damage", "Medium Damage", "Major Damage", "Destroyed" }, 4);
[Value(Name="Region", Offset=68)]
public StringID _68_Region;
[Value(Offset=72)]
public Byte[] _72_ = new byte[4];
}
public _688_havok_vehicle_physics[] _688_Havok_Vehicle_Physics;
[Serializable][Reflexive(Name="Havok Vehicle Physics", Offset=688, ChunkSize=672, Label="")]
public class _688_havok_vehicle_physics
{
private int __StartOffset__;
[Value(Name="Size", Offset=0)]
public Int16 _0_Size;
[Value(Name="Count", Offset=2)]
public Int16 _2_Count;
[Value(Name="Child Shapes Size", Offset=4)]
public Int32 _4_Child_Shapes_Size;
[Value(Name="Child Shapes Capacity", Offset=8)]
public Int32 _8_Child_Shapes_Capacity;
[Value(Name="Shape Type", Offset=12)]
public H2.DataTypes.Enum _12_Shape_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Shape", Offset=16)]
public H2.DataTypes.Enum _16_Shape = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Collision Filter", Offset=20)]
public Int32 _20_Collision_Filter;
[Value(Name="Shape Type", Offset=24)]
public H2.DataTypes.Enum _24_Shape_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Shape", Offset=28)]
public H2.DataTypes.Enum _28_Shape = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Collision Filter", Offset=32)]
public Int32 _32_Collision_Filter;
[Value(Name="Shape Type", Offset=36)]
public H2.DataTypes.Enum _36_Shape_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Shape", Offset=40)]
public H2.DataTypes.Enum _40_Shape = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Collision Filter", Offset=44)]
public Int32 _44_Collision_Filter;
[Value(Name="Shape Type", Offset=48)]
public H2.DataTypes.Enum _48_Shape_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Shape", Offset=52)]
public H2.DataTypes.Enum _52_Shape = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Collision Filter", Offset=56)]
public Single _56_Collision_Filter;
[Value(Name="Multisphere Count", Offset=60)]
public Int32 _60_Multisphere_Count;
[Value(Name="Flags", Offset=64)]
public Bitmask _64_Flags = new Bitmask(new string[] { "Has AABB Phantom","BORK BORK BORK!" }, 4);
[Value(Offset=68)]
public Byte[] _68_ = new byte[4];
[Value(Name="X0", Offset=72)]
public Single _72_X0;
[Value(Name="X1", Offset=76)]
public Single _76_X1;
[Value(Name="Y0", Offset=80)]
public Single _80_Y0;
[Value(Name="Y1", Offset=84)]
public Single _84_Y1;
[Value(Name="Z0", Offset=88)]
public Single _88_Z0;
[Value(Name="Z1", Offset=92)]
public Single _92_Z1;
[Value(Offset=96)]
public Byte[] _96_ = new byte[8];
[Value(Name="Size", Offset=104)]
public Int16 _104_Size;
[Value(Name="Count", Offset=106)]
public Int16 _106_Count;
[Value(Name="Num Of Spheres", Offset=108)]
public Int32 _108_Num_Of_Spheres;
[Value(Name="Sphere 0 i", Offset=112)]
public Single _112_Sphere_0_i;
[Value(Name="Sphere 0 j", Offset=116)]
public Single _116_Sphere_0_j;
[Value(Name="Sphere 0 k", Offset=120)]
public Single _120_Sphere_0_k;
[Value(Name="Sphere 1 i", Offset=124)]
public Single _124_Sphere_1_i;
[Value(Name="Sphere 1 j", Offset=128)]
public Single _128_Sphere_1_j;
[Value(Name="Sphere 1 k", Offset=132)]
public Single _132_Sphere_1_k;
[Value(Name="Sphere 2 i", Offset=136)]
public Single _136_Sphere_2_i;
[Value(Name="Sphere 2 j", Offset=140)]
public Single _140_Sphere_2_j;
[Value(Name="Sphere 2 k", Offset=144)]
public Single _144_Sphere_2_k;
[Value(Name="Sphere 3 i", Offset=148)]
public Single _148_Sphere_3_i;
[Value(Name="Sphere 3 j", Offset=152)]
public Single _152_Sphere_3_j;
[Value(Name="Sphere 3 k", Offset=156)]
public Single _156_Sphere_3_k;
[Value(Name="Sphere 4 i", Offset=160)]
public Single _160_Sphere_4_i;
[Value(Name="Sphere 4 j", Offset=164)]
public Single _164_Sphere_4_j;
[Value(Name="Sphere 4 k", Offset=168)]
public Single _168_Sphere_4_k;
[Value(Name="Sphere 5 i", Offset=172)]
public Single _172_Sphere_5_i;
[Value(Name="Sphere 5 j", Offset=176)]
public Single _176_Sphere_5_j;
[Value(Name="Sphere 5 k", Offset=180)]
public Single _180_Sphere_5_k;
[Value(Name="Sphere 6 i", Offset=184)]
public Single _184_Sphere_6_i;
[Value(Name="Sphere 6 j", Offset=188)]
public Single _188_Sphere_6_j;
[Value(Name="Sphere 6 k", Offset=192)]
public Single _192_Sphere_6_k;
[Value(Name="Sphere 7 i", Offset=196)]
public Single _196_Sphere_7_i;
[Value(Name="Sphere 7 j", Offset=200)]
public Single _200_Sphere_7_j;
[Value(Name="Sphere 7 k", Offset=204)]
public Single _204_Sphere_7_k;
[Value(Name="Sphere 8 i", Offset=208)]
public Single _208_Sphere_8_i;
[Value(Name="Sphere 8 j", Offset=212)]
public Single _212_Sphere_8_j;
[Value(Name="Sphere 8 k", Offset=216)]
public Single _216_Sphere_8_k;
[Value(Name="Sphere 9 i", Offset=220)]
public Single _220_Sphere_9_i;
[Value(Name="Sphere 9 j", Offset=224)]
public Single _224_Sphere_9_j;
[Value(Name="Sphere 9 k", Offset=228)]
public Single _228_Sphere_9_k;
[Value(Name="Sphere 10 i", Offset=232)]
public Single _232_Sphere_10_i;
[Value(Name="Sphere 10 j", Offset=236)]
public Single _236_Sphere_10_j;
[Value(Name="Sphere 10 k", Offset=240)]
public Single _240_Sphere_10_k;
[Value(Name="Size", Offset=244)]
public Int16 _244_Size;
[Value(Name="Count", Offset=246)]
public Int16 _246_Count;
[Value(Name="Num Of Spheres", Offset=248)]
public Int32 _248_Num_Of_Spheres;
[Value(Name="Sphere 1 i", Offset=252)]
public Single _252_Sphere_1_i;
[Value(Name="Sphere 1 j", Offset=256)]
public Single _256_Sphere_1_j;
[Value(Name="Sphere 1 k", Offset=260)]
public Single _260_Sphere_1_k;
[Value(Name="Sphere 2 i", Offset=264)]
public Single _264_Sphere_2_i;
[Value(Name="Sphere 2 j", Offset=268)]
public Single _268_Sphere_2_j;
[Value(Name="Sphere 2 k", Offset=272)]
public Single _272_Sphere_2_k;
[Value(Name="Sphere 3 i", Offset=276)]
public Single _276_Sphere_3_i;
[Value(Name="Sphere 3 j", Offset=280)]
public Single _280_Sphere_3_j;
[Value(Name="Sphere 3 k", Offset=284)]
public Single _284_Sphere_3_k;
[Value(Name="Sphere 4 i", Offset=288)]
public Single _288_Sphere_4_i;
[Value(Name="Sphere 4 j", Offset=292)]
public Single _292_Sphere_4_j;
[Value(Name="Sphere 4 k", Offset=296)]
public Single _296_Sphere_4_k;
[Value(Name="Sphere 5 i", Offset=300)]
public Single _300_Sphere_5_i;
[Value(Name="Sphere 5 j", Offset=304)]
public Single _304_Sphere_5_j;
[Value(Name="Sphere 5 k", Offset=308)]
public Single _308_Sphere_5_k;
[Value(Name="Sphere 6 i", Offset=312)]
public Single _312_Sphere_6_i;
[Value(Name="Sphere 6 j", Offset=316)]
public Single _316_Sphere_6_j;
[Value(Name="Sphere 6 k", Offset=320)]
public Single _320_Sphere_6_k;
[Value(Name="Sphere 7 i", Offset=324)]
public Single _324_Sphere_7_i;
[Value(Name="Sphere 7 j", Offset=328)]
public Single _328_Sphere_7_j;
[Value(Name="Sphere 7 k", Offset=332)]
public Single _332_Sphere_7_k;
[Value(Name="Sphere 8 i", Offset=336)]
public Single _336_Sphere_8_i;
[Value(Name="Sphere 8 j", Offset=340)]
public Single _340_Sphere_8_j;
[Value(Name="Sphere 8 k", Offset=344)]
public Single _344_Sphere_8_k;
[Value(Name="Sphere 9 i", Offset=348)]
public Single _348_Sphere_9_i;
[Value(Name="Sphere 9 j", Offset=352)]
public Single _352_Sphere_9_j;
[Value(Name="Sphere 9 k", Offset=356)]
public Single _356_Sphere_9_k;
[Value(Name="Sphere 10 i", Offset=360)]
public Single _360_Sphere_10_i;
[Value(Name="Sphere 10 j", Offset=364)]
public Single _364_Sphere_10_j;
[Value(Name="Sphere 10 k", Offset=368)]
public Single _368_Sphere_10_k;
[Value(Name="Size", Offset=372)]
public Int16 _372_Size;
[Value(Name="Count", Offset=374)]
public Int16 _374_Count;
[Value(Name="Num Of Spheres", Offset=376)]
public Int32 _376_Num_Of_Spheres;
[Value(Name="Sphere 1 i", Offset=380)]
public Single _380_Sphere_1_i;
[Value(Name="Sphere 1 j", Offset=384)]
public Single _384_Sphere_1_j;
[Value(Name="Sphere 1 k", Offset=388)]
public Single _388_Sphere_1_k;
[Value(Name="Sphere 2 i", Offset=392)]
public Single _392_Sphere_2_i;
[Value(Name="Sphere 2 j", Offset=396)]
public Single _396_Sphere_2_j;
[Value(Name="Sphere 2 k", Offset=400)]
public Single _400_Sphere_2_k;
[Value(Name="Sphere 3 i", Offset=404)]
public Single _404_Sphere_3_i;
[Value(Name="Sphere 3 j", Offset=408)]
public Single _408_Sphere_3_j;
[Value(Name="Sphere 3 k", Offset=412)]
public Single _412_Sphere_3_k;
[Value(Name="Sphere 4 i", Offset=416)]
public Single _416_Sphere_4_i;
[Value(Name="Sphere 4 j", Offset=420)]
public Single _420_Sphere_4_j;
[Value(Name="Sphere 4 k", Offset=424)]
public Single _424_Sphere_4_k;
[Value(Name="Sphere 5 i", Offset=428)]
public Single _428_Sphere_5_i;
[Value(Name="Sphere 5 j", Offset=432)]
public Single _432_Sphere_5_j;
[Value(Name="Sphere 5 k", Offset=436)]
public Single _436_Sphere_5_k;
[Value(Name="Sphere 6 i", Offset=440)]
public Single _440_Sphere_6_i;
[Value(Name="Sphere 6 j", Offset=444)]
public Single _444_Sphere_6_j;
[Value(Name="Sphere 6 k", Offset=448)]
public Single _448_Sphere_6_k;
[Value(Name="Sphere 7 i", Offset=452)]
public Single _452_Sphere_7_i;
[Value(Name="Sphere 7 j", Offset=456)]
public Single _456_Sphere_7_j;
[Value(Name="Sphere 7 k", Offset=460)]
public Single _460_Sphere_7_k;
[Value(Name="Sphere 8 i", Offset=464)]
public Single _464_Sphere_8_i;
[Value(Name="Sphere 8 j", Offset=468)]
public Single _468_Sphere_8_j;
[Value(Name="Sphere 8 k", Offset=472)]
public Single _472_Sphere_8_k;
[Value(Name="Sphere 9 i", Offset=476)]
public Single _476_Sphere_9_i;
[Value(Name="Sphere 9 j", Offset=480)]
public Single _480_Sphere_9_j;
[Value(Name="Sphere 9 k", Offset=484)]
public Single _484_Sphere_9_k;
[Value(Name="Sphere 10 i", Offset=488)]
public Single _488_Sphere_10_i;
[Value(Name="Sphere 10 j", Offset=492)]
public Single _492_Sphere_10_j;
[Value(Name="Sphere 10 k", Offset=496)]
public Single _496_Sphere_10_k;
[Value(Name="Size", Offset=500)]
public Int16 _500_Size;
[Value(Name="Count", Offset=502)]
public Int16 _502_Count;
[Value(Name="Num Of Spheres", Offset=504)]
public Int32 _504_Num_Of_Spheres;
[Value(Name="For", Offset=508)]
public Single _508_For;
[Value(Name="Any", Offset=512)]
public Single _512_Any;
[Value(Name="One", Offset=516)]
public Single _516_One;
[Value(Name="Who", Offset=520)]
public Single _520_Who;
[Value(Name="Cares", Offset=524)]
public Single _524_Cares;
[Value(Name="This Reflexive Isn't 100% Accurate", Offset=528)]
public Single _528_This_Reflexive_Isn_t_100__Accurate;
[Value(Name="Size", Offset=532)]
public Int16 _532_Size;
[Value(Name="Count", Offset=534)]
public Int16 _534_Count;
[Value(Name="Num Of Spheres", Offset=536)]
public Int32 _536_Num_Of_Spheres;
[Value(Name="Sphere 0 ", Offset=540)]
public Single _540_Sphere_0_;
[Value(Name="Sphere 0 ", Offset=544)]
public Single _544_Sphere_0_;
[Value(Name="Sphere 0 ", Offset=548)]
public Single _548_Sphere_0_;
[Value(Name="Sphere 1 ", Offset=552)]
public Single _552_Sphere_1_;
[Value(Name="Sphere 1 ", Offset=556)]
public Single _556_Sphere_1_;
[Value(Name="Sphere 1 ", Offset=560)]
public Single _560_Sphere_1_;
[Value(Name="Sphere 2 ", Offset=564)]
public Single _564_Sphere_2_;
[Value(Name="Sphere 2 ", Offset=568)]
public Single _568_Sphere_2_;
[Value(Name="Sphere 2 ", Offset=572)]
public Single _572_Sphere_2_;
[Value(Name="Sphere 3 ", Offset=576)]
public Single _576_Sphere_3_;
[Value(Name="Sphere 3 ", Offset=580)]
public Single _580_Sphere_3_;
[Value(Name="Sphere 3 ", Offset=584)]
public Single _584_Sphere_3_;
[Value(Name="Sphere 4 ", Offset=588)]
public Single _588_Sphere_4_;
[Value(Name="Sphere 4 ", Offset=592)]
public Single _592_Sphere_4_;
[Value(Name="Sphere 4 ", Offset=596)]
public Single _596_Sphere_4_;
[Value(Name="Sphere 5 ", Offset=600)]
public Single _600_Sphere_5_;
[Value(Name="Sphere 5 ", Offset=604)]
public Single _604_Sphere_5_;
[Value(Name="Sphere 5 ", Offset=608)]
public Single _608_Sphere_5_;
[Value(Name="Sphere 6 ", Offset=612)]
public Single _612_Sphere_6_;
[Value(Name="Sphere 6 ", Offset=616)]
public Single _616_Sphere_6_;
[Value(Name="Sphere 6 ", Offset=620)]
public Single _620_Sphere_6_;
[Value(Name="Sphere 7 ", Offset=624)]
public Single _624_Sphere_7_;
[Value(Name="Sphere 7 ", Offset=628)]
public Single _628_Sphere_7_;
[Value(Name="Sphere 7 ", Offset=632)]
public Single _632_Sphere_7_;
[Value(Name="Sphere 8 ", Offset=636)]
public Single _636_Sphere_8_;
[Value(Name="Sphere 8 ", Offset=640)]
public Single _640_Sphere_8_;
[Value(Name="Sphere 8 ", Offset=644)]
public Single _644_Sphere_8_;
[Value(Name="Sphere 9 ", Offset=648)]
public Single _648_Sphere_9_;
[Value(Name="Sphere 9 ", Offset=652)]
public Single _652_Sphere_9_;
[Value(Name="Sphere 9 ", Offset=656)]
public Single _656_Sphere_9_;
[Value(Name="Sphere 10 ", Offset=660)]
public Single _660_Sphere_10_;
[Value(Name="Sphere 10 ", Offset=664)]
public Single _664_Sphere_10_;
[Value(Name="Sphere 10 ", Offset=668)]
public Single _668_Sphere_10_;
}
        }
    }
}