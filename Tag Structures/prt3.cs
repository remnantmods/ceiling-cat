using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("prt3")]
        public class prt3
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Spins","Random U Mirror","Random V Mirror","Frame Animation One Shot","Select Random Sequence","Disable Frame Blending","Receive Lightmap Lighting","Tint From Diffuse Texture","Dies At Rest","Dies On Structure Collision","Dies In Media","Dies In Air","Bitmap Authored Vertically","Has Sweetener" }, 4);
[Value(Name="Particle Billboard Style", Offset=4)]
public H2.DataTypes.Enum _4_Particle_Billboard_Style = new H2.DataTypes.Enum(new string[] {  "Screen Facing", "Parallel To Direction", "Perpendicular To Direction", "Vertical", "Horizontal" }, 4);
[Value(Name="First Billboard Style", Offset=8)]
public Int16 _8_First_Billboard_Style;
[Value(Name="Sequence Count", Offset=10)]
public Int16 _10_Sequence_Count;
[Value(Name="Shader Template", Offset=12)]
public Dependancy _12_Shader_Template;
public _16_shader[] _16_Shader;
[Serializable][Reflexive(Name="Shader", Offset=16, ChunkSize=40, Label="")]
public class _16_shader
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Dependancy _8_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Offset=32)]
public Byte[] _32_ = new byte[8];
}
[Value(Name="Output Modifier", Offset=24)]
public H2.DataTypes.Enum _24_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=28)]
public H2.DataTypes.Enum _28_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _32_color[] _32_Color;
[Serializable][Reflexive(Name="Color", Offset=32, ChunkSize=1, Label="")]
public class _32_color
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=40)]
public H2.DataTypes.Enum _40_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=44)]
public H2.DataTypes.Enum _44_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _48_alpha[] _48_Alpha;
[Serializable][Reflexive(Name="Alpha", Offset=48, ChunkSize=1, Label="")]
public class _48_alpha
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=56)]
public H2.DataTypes.Enum _56_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=60)]
public H2.DataTypes.Enum _60_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _64_scale[] _64_Scale;
[Serializable][Reflexive(Name="Scale", Offset=64, ChunkSize=1, Label="")]
public class _64_scale
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=72)]
public H2.DataTypes.Enum _72_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=76)]
public H2.DataTypes.Enum _76_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _80_rotation[] _80_Rotation;
[Serializable][Reflexive(Name="Rotation", Offset=80, ChunkSize=1, Label="")]
public class _80_rotation
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=88)]
public H2.DataTypes.Enum _88_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=92)]
public H2.DataTypes.Enum _92_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _96_frame_index[] _96_Frame_Index;
[Serializable][Reflexive(Name="Frame Index", Offset=96, ChunkSize=1, Label="")]
public class _96_frame_index
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Collision Effect", Offset=104)]
public Dependancy _104_Collision_Effect;
[Value(Name="Death Effect", Offset=108)]
public Dependancy _108_Death_Effect;
public _112_locations[] _112_Locations;
[Serializable][Reflexive(Name="Locations", Offset=112, ChunkSize=4, Label="")]
public class _112_locations
{
private int __StartOffset__;
[Value(Name="Location", Offset=0)]
public StringID _0_Location;
}
public _120_attached_particle_systems[] _120_Attached_Particle_Systems;
[Serializable][Reflexive(Name="Attached Particle Systems", Offset=120, ChunkSize=56, Label="")]
public class _120_attached_particle_systems
{
private int __StartOffset__;
[Value(Name="Particle", Offset=0)]
public Dependancy _0_Particle;
[Value(Name="Location", Offset=8)]
public Int16 _8_Location;
[Value(Offset=10)]
public Byte[] _10_ = new byte[2];
[Value(Name="Coordinate System", Offset=12)]
public H2.DataTypes.Enum _12_Coordinate_System = new H2.DataTypes.Enum(new string[] {  "World", "Local", "Parent" }, 2);
[Value(Name="Environment", Offset=14)]
public H2.DataTypes.Enum _14_Environment = new H2.DataTypes.Enum(new string[] {  "Any Environment", "Air Only", "Water Only", "Space Only" }, 2);
[Value(Name="Disposition", Offset=16)]
public H2.DataTypes.Enum _16_Disposition = new H2.DataTypes.Enum(new string[] {  "Either Mode", "Violent Mode Only", "NonViolent Mode Only" }, 2);
[Value(Name="Camera Mode", Offset=18)]
public H2.DataTypes.Enum _18_Camera_Mode = new H2.DataTypes.Enum(new string[] {  "Independant Of Camera Mode", "Only In 1st Person", "Only In 3rd Person", "Both 1st And 3rd" }, 2);
[Value(Name="Sort Bias", Offset=20)]
public Int16 _20_Sort_Bias;
[Value(Name="Flags", Offset=22)]
public Bitmask _22_Flags = new Bitmask(new string[] { "Glow","Cinematics","Looping Particle","Disabled For Debugging","Inherit Effect Velocity","Don't Render System","Render When Zoomed","Spread Between Ticks","Persistent Particle","Expensive Visibility" }, 4);
[Value(Offset=26)]
public Byte[] _26_ = new byte[2];
[Value(Name="LOD In Distance", Offset=28)]
public Single _28_LOD_In_Distance;
[Value(Name="LOD Feather In Delta", Offset=32)]
public Single _32_LOD_Feather_In_Delta;
[Value(Name="LOD Out Distance", Offset=36)]
public Single _36_LOD_Out_Distance;
[Value(Name="LOD Feather Out Delta", Offset=40)]
public Single _40_LOD_Feather_Out_Delta;
[Value(Offset=44)]
public Byte[] _44_ = new byte[4];
public _48_emitters[] _48_Emitters;
[Serializable][Reflexive(Name="Emitters", Offset=48, ChunkSize=184, Label="")]
public class _48_emitters
{
private int __StartOffset__;
[Value(Name="Particle Physics", Offset=0)]
public Dependancy _0_Particle_Physics;
[Value(Name="Output Modifier", Offset=8)]
public H2.DataTypes.Enum _8_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=12)]
public H2.DataTypes.Enum _12_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _16_particle_emission_rate[] _16_Particle_Emission_Rate;
[Serializable][Reflexive(Name="Particle Emission Rate", Offset=16, ChunkSize=1, Label="")]
public class _16_particle_emission_rate
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=24)]
public H2.DataTypes.Enum _24_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=28)]
public H2.DataTypes.Enum _28_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _32_particle_lifespan__sec_[] _32_Particle_Lifespan__sec_;
[Serializable][Reflexive(Name="Particle Lifespan (sec)", Offset=32, ChunkSize=1, Label="")]
public class _32_particle_lifespan__sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=40)]
public H2.DataTypes.Enum _40_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=44)]
public H2.DataTypes.Enum _44_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _48_particle_velocity__world_units_per_sec_[] _48_Particle_Velocity__World_Units_Per_Sec_;
[Serializable][Reflexive(Name="Particle Velocity (World Units Per Sec)", Offset=48, ChunkSize=1, Label="")]
public class _48_particle_velocity__world_units_per_sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=56)]
public H2.DataTypes.Enum _56_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=60)]
public H2.DataTypes.Enum _60_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _64_particle_angular_velocity__degrees_per_sec_[] _64_Particle_Angular_Velocity__Degrees_Per_Sec_;
[Serializable][Reflexive(Name="Particle Angular Velocity (Degrees Per Sec)", Offset=64, ChunkSize=1, Label="")]
public class _64_particle_angular_velocity__degrees_per_sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=72)]
public H2.DataTypes.Enum _72_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=76)]
public H2.DataTypes.Enum _76_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _80_particle_size__world_units_[] _80_Particle_Size__World_Units_;
[Serializable][Reflexive(Name="Particle Size (World Units)", Offset=80, ChunkSize=1, Label="")]
public class _80_particle_size__world_units_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=88)]
public H2.DataTypes.Enum _88_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=92)]
public H2.DataTypes.Enum _92_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _96_particle_tint[] _96_Particle_Tint;
[Serializable][Reflexive(Name="Particle Tint", Offset=96, ChunkSize=1, Label="")]
public class _96_particle_tint
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=104)]
public H2.DataTypes.Enum _104_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=108)]
public H2.DataTypes.Enum _108_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _112_particle_alpha[] _112_Particle_Alpha;
[Serializable][Reflexive(Name="Particle Alpha", Offset=112, ChunkSize=1, Label="")]
public class _112_particle_alpha
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Emission Shape", Offset=120)]
public H2.DataTypes.Enum _120_Emission_Shape = new H2.DataTypes.Enum(new string[] {  "Sprayer", "Disc", "Globe", "Implode", "Tube", "Halo", "Impact Contour", "Impact Area", "Debris", "Line" }, 4);
[Value(Name="Output Modifier", Offset=124)]
public H2.DataTypes.Enum _124_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=128)]
public H2.DataTypes.Enum _128_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _132_emission_radius__world_units_[] _132_Emission_Radius__World_Units_;
[Serializable][Reflexive(Name="Emission Radius (World Units)", Offset=132, ChunkSize=1, Label="")]
public class _132_emission_radius__world_units_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=140)]
public H2.DataTypes.Enum _140_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=144)]
public H2.DataTypes.Enum _144_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _148_emission_angle__degrees_[] _148_Emission_Angle__Degrees_;
[Serializable][Reflexive(Name="Emission Angle (Degrees)", Offset=148, ChunkSize=1, Label="")]
public class _148_emission_angle__degrees_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Translation Offset X", Offset=156)]
public Single _156_Translation_Offset_X;
[Value(Name="Translation Offset Y", Offset=160)]
public Single _160_Translation_Offset_Y;
[Value(Name="Translation Offset Z", Offset=164)]
public Single _164_Translation_Offset_Z;
[Value(Name="Relative Direction Yaw", Offset=168)]
public Single _168_Relative_Direction_Yaw;
[Value(Name="Relative Direction Pitch", Offset=172)]
public Single _172_Relative_Direction_Pitch;
[Value(Offset=176)]
public Byte[] _176_ = new byte[8];
}
}
public _128_unknown116[] _128_Unknown116;
[Serializable][Reflexive(Name="Unknown116", Offset=128, ChunkSize=132, Label="")]
public class _128_unknown116
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown118[] _4_Unknown118;
[Serializable][Reflexive(Name="Unknown118", Offset=4, ChunkSize=12, Label="")]
public class _4_unknown118
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
public _12_unknown123[] _12_Unknown123;
[Serializable][Reflexive(Name="Unknown123", Offset=12, ChunkSize=4, Label="")]
public class _12_unknown123
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
}
public _20_unknown126[] _20_Unknown126;
[Serializable][Reflexive(Name="Unknown126", Offset=20, ChunkSize=16, Label="")]
public class _20_unknown126
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
public _28_unknown132[] _28_Unknown132;
[Serializable][Reflexive(Name="Unknown132", Offset=28, ChunkSize=8, Label="")]
public class _28_unknown132
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
public _36_unknown136[] _36_Unknown136;
[Serializable][Reflexive(Name="Unknown136", Offset=36, ChunkSize=2, Label="")]
public class _36_unknown136
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
}
public _44_unknown139[] _44_Unknown139;
[Serializable][Reflexive(Name="Unknown139", Offset=44, ChunkSize=4, Label="")]
public class _44_unknown139
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _52_unknown142[] _52_Unknown142;
[Serializable][Reflexive(Name="Unknown142", Offset=52, ChunkSize=12, Label="")]
public class _52_unknown142
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
}
public _60_unknown146[] _60_Unknown146;
[Serializable][Reflexive(Name="Unknown146", Offset=60, ChunkSize=20, Label="")]
public class _60_unknown146
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
public _12_unknown146_0[] _12_Unknown146_0;
[Serializable][Reflexive(Name="Unknown146.0", Offset=12, ChunkSize=14, Label="")]
public class _12_unknown146_0
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[14];
}
}
public _68_unknown1392[] _68_Unknown1392;
[Serializable][Reflexive(Name="Unknown1392", Offset=68, ChunkSize=4, Label="")]
public class _68_unknown1392
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _76_unknown136[] _76_Unknown136;
[Serializable][Reflexive(Name="Unknown136", Offset=76, ChunkSize=2, Label="")]
public class _76_unknown136
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
}
public _84_unknown1392[] _84_Unknown1392;
[Serializable][Reflexive(Name="Unknown1392", Offset=84, ChunkSize=4, Label="")]
public class _84_unknown1392
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _92_unknown158[] _92_Unknown158;
[Serializable][Reflexive(Name="Unknown158", Offset=92, ChunkSize=4, Label="")]
public class _92_unknown158
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
}
public _100_unknown1583[] _100_Unknown1583;
[Serializable][Reflexive(Name="Unknown1583", Offset=100, ChunkSize=4, Label="")]
public class _100_unknown1583
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
}
public _108_unknown1584[] _108_Unknown1584;
[Serializable][Reflexive(Name="Unknown1584", Offset=108, ChunkSize=4, Label="")]
public class _108_unknown1584
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
}
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Offset=124)]
public Byte[] _124_ = new byte[8];
}
[Value(Offset=136)]
public Byte[] _136_ = new byte[4];
[Value(Name="Unknown", Offset=140)]
public Int32 _140_Unknown;
[Value(Name="Unknown", Offset=144)]
public Int32 _144_Unknown;
[Value(Name="Unknown", Offset=148)]
public Int32 _148_Unknown;
[Value(Name="Unknown", Offset=152)]
public Single _152_Unknown;
[Value(Name="Unknown", Offset=156)]
public Single _156_Unknown;
[Value(Name="Unknown", Offset=160)]
public Single _160_Unknown;
[Value(Name="Unknown", Offset=164)]
public Single _164_Unknown;
[Value(Name="Unknown", Offset=168)]
public Single _168_Unknown;
[Value(Name="Unknown", Offset=172)]
public Single _172_Unknown;
        }
    }
}