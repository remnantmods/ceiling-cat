using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("pmov")]
        public class pmov
        {
[Value(Name="Particle Physics", Offset=0)]
public Dependancy _0_Particle_Physics;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
public _8_unknown6[] _8_Unknown6;
[Serializable][Reflexive(Name="Unknown6", Offset=8, ChunkSize=20, Label="")]
public class _8_unknown6
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown8[] _4_Unknown8;
[Serializable][Reflexive(Name="Unknown8", Offset=4, ChunkSize=20, Label="")]
public class _4_unknown8
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
public _12_unknown12[] _12_Unknown12;
[Serializable][Reflexive(Name="Unknown12", Offset=12, ChunkSize=1, Label="")]
public class _12_unknown12
{
private int __StartOffset__;
[Value(Name="Floats", Offset=0)]
public Byte _0_Floats;
}
}
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
}
        }
    }
}