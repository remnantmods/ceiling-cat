using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("phys")]
        public class phys
        {
[Value(Name="Radius", Offset=0)]
public Single _0_Radius;
[Value(Name="Moment Scale", Offset=4)]
public Single _4_Moment_Scale;
[Value(Name="Mass", Offset=8)]
public Single _8_Mass;
[Value(Name="Center of mass x", Offset=12)]
public Single _12_Center_of_mass_x;
[Value(Name="Center of mass y", Offset=16)]
public Single _16_Center_of_mass_y;
[Value(Name="Center of mass z", Offset=20)]
public Single _20_Center_of_mass_z;
[Value(Name="Density", Offset=24)]
public Single _24_Density;
[Value(Name="Gravity scale", Offset=28)]
public Single _28_Gravity_scale;
[Value(Name="Ground friction", Offset=32)]
public Single _32_Ground_friction;
[Value(Name="Ground Depth", Offset=36)]
public Single _36_Ground_Depth;
[Value(Name="Ground damp function", Offset=40)]
public Single _40_Ground_damp_function;
[Value(Name="Normal K1", Offset=44)]
public Single _44_Normal_K1;
[Value(Name="Normal K0", Offset=48)]
public Single _48_Normal_K0;
[Value(Offset=52)]
public Byte[] _52_ = new byte[4];
[Value(Name="Water Friction", Offset=56)]
public Single _56_Water_Friction;
[Value(Name="Water Depth", Offset=60)]
public Single _60_Water_Depth;
[Value(Name="Water Density", Offset=64)]
public Single _64_Water_Density;
[Value(Offset=68)]
public Byte[] _68_ = new byte[4];
[Value(Name="Air friction", Offset=72)]
public Single _72_Air_friction;
[Value(Offset=76)]
public Byte[] _76_ = new byte[4];
[Value(Name="xx movement", Offset=80)]
public Single _80_xx_movement;
[Value(Name="yy movement", Offset=84)]
public Single _84_yy_movement;
[Value(Name="zz movement", Offset=88)]
public Single _88_zz_movement;
public _92_internal_matrix[] _92_Internal_Matrix;
[Serializable][Reflexive(Name="Internal Matrix", Offset=92, ChunkSize=36, Label="")]
public class _92_internal_matrix
{
private int __StartOffset__;
[Value(Name="yy+zz", Offset=0)]
public Single _0_yy_zz;
[Value(Name="-xy", Offset=4)]
public Single _4__xy;
[Value(Name="-zx", Offset=8)]
public Single _8__zx;
[Value(Name="-xy", Offset=12)]
public Single _12__xy;
[Value(Name="zz+xx", Offset=16)]
public Single _16_zz_xx;
[Value(Name="-yz", Offset=20)]
public Single _20__yz;
[Value(Name="-zx", Offset=24)]
public Single _24__zx;
[Value(Name="-yz", Offset=28)]
public Single _28__yz;
[Value(Name="xx+yy", Offset=32)]
public Single _32_xx_yy;
}
public _100_mass_points[] _100_Mass_Points;
[Serializable][Reflexive(Name="Mass Points", Offset=100, ChunkSize=60, Label="")]
public class _100_mass_points
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="flags", Offset=32)]
public Bitmask _32_flags = new Bitmask(new string[] { "ground friction","water friction","air friction","water lift","air lift","thrust","antigrav" }, 4);
[Value(Name="strength", Offset=36)]
public Single _36_strength;
[Value(Name="offset    real", Offset=40)]
public Single _40_offset____real;
[Value(Name="height    real", Offset=44)]
public Single _44_height____real;
[Value(Name="damp fraction", Offset=48)]
public Single _48_damp_fraction;
[Value(Name="normal k1", Offset=52)]
public Single _52_normal_k1;
[Value(Name="normal k0", Offset=56)]
public Single _56_normal_k0;
}
public _108_mass_points[] _108_Mass_Points;
[Serializable][Reflexive(Name="Mass Points", Offset=108, ChunkSize=128, Label="")]
public class _108_mass_points
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Model Node", Offset=32)]
public Int16 _32_Model_Node;
[Value(Name="Powered Mass Point", Offset=34)]
public Int16 _34_Powered_Mass_Point;
[Value(Name="flags", Offset=36)]
public Bitmask _36_flags = new Bitmask(new string[] { "Metallic" }, 4);
[Value(Name="Relative Mass", Offset=40)]
public Single _40_Relative_Mass;
[Value(Name="Mass", Offset=44)]
public Single _44_Mass;
[Value(Name="Relative Density", Offset=48)]
public Single _48_Relative_Density;
[Value(Name="Density", Offset=52)]
public Single _52_Density;
[Value(Name="Pos X", Offset=56)]
public Single _56_Pos_X;
[Value(Name="Pos Y", Offset=60)]
public Single _60_Pos_Y;
[Value(Name="Pos Z", Offset=64)]
public Single _64_Pos_Z;
[Value(Name="Forward i", Offset=68)]
public Single _68_Forward_i;
[Value(Name="Forward j", Offset=72)]
public Single _72_Forward_j;
[Value(Name="Forward k", Offset=76)]
public Single _76_Forward_k;
[Value(Name="Up i", Offset=80)]
public Single _80_Up_i;
[Value(Name="Up j", Offset=84)]
public Single _84_Up_j;
[Value(Name="Up k", Offset=88)]
public Single _88_Up_k;
[Value(Name="friction type", Offset=92)]
public H2.DataTypes.Enum _92_friction_type = new H2.DataTypes.Enum(new string[] {  "point", "forward", "left", "up" }, 4);
[Value(Name="friction parallel scale", Offset=96)]
public Single _96_friction_parallel_scale;
[Value(Name="friction perependicular scale", Offset=100)]
public Single _100_friction_perependicular_scale;
[Value(Name="radius", Offset=104)]
public Single _104_radius;
[Value(Offset=108)]
public Byte[] _108_ = new byte[20];
}
        }
    }
}