using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("sbsp")]
        public class sbsp
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[12];
[Value(Name="sbsp", Offset=12)]
public Dependancy _12_sbsp;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
public _24_collision_materials[] _24_Collision_Materials;
[Serializable][Reflexive(Name="Collision Materials", Offset=24, ChunkSize=20, Label="")]
public class _24_collision_materials
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Dependancy _0_Unused;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Shader", Offset=12)]
public Dependancy _12_Shader;
}
public _32_collision_bsp[] _32_Collision_Bsp;
[Serializable][Reflexive(Name="Collision Bsp", Offset=32, ChunkSize=68, Label="")]
public class _32_collision_bsp
{
private int __StartOffset__;
public _0_3d_nodes[] _0_3D_Nodes;
[Serializable][Reflexive(Name="3D Nodes", Offset=0, ChunkSize=8, Label="")]
public class _0_3d_nodes
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="Back child", Offset=2)]
public Int16 _2_Back_child;
[Value(Name="Flag/padding/Unknown", Offset=4)]
public Byte _4_Flag_padding_Unknown;
[Value(Name="Front Child", Offset=5)]
public Int16 _5_Front_Child;
[Value(Name="Flag/padding/Unknown2", Offset=7)]
public Byte _7_Flag_padding_Unknown2;
}
public _8_planes[] _8_Planes;
[Serializable][Reflexive(Name="Planes", Offset=8, ChunkSize=16, Label="")]
public class _8_planes
{
private int __StartOffset__;
[Value(Name="Plane i", Offset=0)]
public Single _0_Plane_i;
[Value(Name="Plane j", Offset=4)]
public Single _4_Plane_j;
[Value(Name="Plane k", Offset=8)]
public Single _8_Plane_k;
[Value(Name="Plane d", Offset=12)]
public Single _12_Plane_d;
}
public _16_leaves[] _16_Leaves;
[Serializable][Reflexive(Name="Leaves", Offset=16, ChunkSize=4, Label="")]
public class _16_leaves
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Contains Double-Sided Surfaces" }, 1);
[Value(Name="BSP 2D Reference Count", Offset=1)]
public Byte _1_BSP_2D_Reference_Count;
[Value(Name="First BSP 2D Reference", Offset=2)]
public Int16 _2_First_BSP_2D_Reference;
}
public _24_bsp_2d_refrences[] _24_BSP_2D_Refrences;
[Serializable][Reflexive(Name="BSP 2D Refrences", Offset=24, ChunkSize=4, Label="")]
public class _24_bsp_2d_refrences
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="BSP 2D Node", Offset=2)]
public Int16 _2_BSP_2D_Node;
}
public _32_bsp2d_nodes[] _32_BSP2D_Nodes;
[Serializable][Reflexive(Name="BSP2D Nodes", Offset=32, ChunkSize=16, Label="")]
public class _32_bsp2d_nodes
{
private int __StartOffset__;
[Value(Name="Plane i", Offset=0)]
public Single _0_Plane_i;
[Value(Name="Plane j", Offset=4)]
public Single _4_Plane_j;
[Value(Name="Plane d", Offset=8)]
public Single _8_Plane_d;
[Value(Name="Left Child", Offset=12)]
public Int16 _12_Left_Child;
[Value(Name="Right Child", Offset=14)]
public Int16 _14_Right_Child;
}
public _40_surfaces[] _40_Surfaces;
[Serializable][Reflexive(Name="Surfaces", Offset=40, ChunkSize=8, Label="")]
public class _40_surfaces
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="First Edge", Offset=2)]
public Int16 _2_First_Edge;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Two-Sided","Invisible","Climable","Breakable","Invaild","Conveyor" }, 1);
[Value(Name="Breakable Surface #", Offset=5)]
public Byte _5_Breakable_Surface_No;
[Value(Name="Material #", Offset=6)]
public Int16 _6_Material_No;
}
public _48_edges[] _48_Edges;
[Serializable][Reflexive(Name="Edges", Offset=48, ChunkSize=12, Label="")]
public class _48_edges
{
private int __StartOffset__;
[Value(Name="Start Vertex", Offset=0)]
public Int16 _0_Start_Vertex;
[Value(Name="End Vertex", Offset=2)]
public Int16 _2_End_Vertex;
[Value(Name="Forward Edge", Offset=4)]
public Int16 _4_Forward_Edge;
[Value(Name="Backward Edge", Offset=6)]
public Int16 _6_Backward_Edge;
[Value(Name="Left Surface", Offset=8)]
public Int16 _8_Left_Surface;
[Value(Name="Right Surface", Offset=10)]
public Int16 _10_Right_Surface;
}
public _56_collsion_model[] _56_Collsion_Model;
[Serializable][Reflexive(Name="Collsion Model", Offset=56, ChunkSize=16, Label="")]
public class _56_collsion_model
{
private int __StartOffset__;
[Value(Name="Point X", Offset=0)]
public Single _0_Point_X;
[Value(Name="Point Y", Offset=4)]
public Single _4_Point_Y;
[Value(Name="Point Z", Offset=8)]
public Single _8_Point_Z;
[Value(Name="First Edge", Offset=12)]
public Int16 _12_First_Edge;
[Value(Offset=14)]
public Byte[] _14_ = new byte[2];
}
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
}
[Value(Name="Vehicle Floor", Offset=40)]
public Single _40_Vehicle_Floor;
[Value(Name="Vehicle Ceiling", Offset=44)]
public Single _44_Vehicle_Ceiling;
[Value(Offset=48)]
public Byte[] _48_ = new byte[8];
public _56_leaves[] _56_Leaves;
[Serializable][Reflexive(Name="Leaves", Offset=56, ChunkSize=8, Label="")]
public class _56_leaves
{
private int __StartOffset__;
[Value(Name="Cluster", Offset=0)]
public Int16 _0_Cluster;
[Value(Name="Surface Count", Offset=2)]
public Int16 _2_Surface_Count;
[Value(Name="Surface refrence", Offset=4)]
public Int32 _4_Surface_refrence;
}
[Value(Name="World Bounds X", Offset=64)]
public Single _64_World_Bounds_X;
[Value(Name="...To", Offset=68)]
public Single _68____To;
[Value(Name="World Bounds Y", Offset=72)]
public Single _72_World_Bounds_Y;
[Value(Name="...To", Offset=76)]
public Single _76____To;
[Value(Name="World Bounds Z", Offset=80)]
public Single _80_World_Bounds_Z;
[Value(Name="...To", Offset=84)]
public Single _84____To;
public _88_surface_references[] _88_Surface_References;
[Serializable][Reflexive(Name="Surface References", Offset=88, ChunkSize=8, Label="")]
public class _88_surface_references
{
private int __StartOffset__;
[Value(Name="Strip Index", Offset=0)]
public Int16 _0_Strip_Index;
[Value(Name="Lightmap Triangle Index", Offset=2)]
public Int16 _2_Lightmap_Triangle_Index;
[Value(Name="BSP Node Index", Offset=4)]
public Int32 _4_BSP_Node_Index;
}
public _96_cluster_data[] _96_Cluster_Data;
[Serializable][Reflexive(Name="Cluster Data", Offset=96, ChunkSize=1, Label="")]
public class _96_cluster_data
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _104_cluster_portals[] _104_Cluster_Portals;
[Serializable][Reflexive(Name="Cluster Portals", Offset=104, ChunkSize=36, Label="")]
public class _104_cluster_portals
{
private int __StartOffset__;
[Value(Name="Back Cluster", Offset=0)]
public Int16 _0_Back_Cluster;
[Value(Name="Front Cluster", Offset=2)]
public Int16 _2_Front_Cluster;
[Value(Name="Plane Index", Offset=4)]
public Int32 _4_Plane_Index;
[Value(Name="Centroid X", Offset=8)]
public Single _8_Centroid_X;
[Value(Name="Centroid Y", Offset=12)]
public Single _12_Centroid_Y;
[Value(Name="Centroid Z", Offset=16)]
public Single _16_Centroid_Z;
[Value(Name="Bounding Radius", Offset=20)]
public Single _20_Bounding_Radius;
[Value(Name="Flags", Offset=24)]
public Bitmask _24_Flags = new Bitmask(new string[] { "AI Cannot Hear Through This","One-Way","Door","No-Way","One-Way Reversed","No One Can Hear Through This" }, 4);
public _28_vertices[] _28_Vertices;
[Serializable][Reflexive(Name="Vertices", Offset=28, ChunkSize=12, Label="")]
public class _28_vertices
{
private int __StartOffset__;
[Value(Name="X", Offset=0)]
public Single _0_X;
[Value(Name="Y", Offset=4)]
public Single _4_Y;
[Value(Name="Z", Offset=8)]
public Single _8_Z;
}
}
public _112_fog_planes[] _112_Fog_Planes;
[Serializable][Reflexive(Name="Fog Planes", Offset=112, ChunkSize=24, Label="")]
public class _112_fog_planes
{
private int __StartOffset__;
[Value(Name="Scenario Planar Fog Index", Offset=0)]
public Int16 _0_Scenario_Planar_Fog_Index;
[Value(Name="Plane i", Offset=2)]
public Single _2_Plane_i;
[Value(Name="Plane j", Offset=6)]
public Single _6_Plane_j;
[Value(Name="Plane k", Offset=10)]
public Single _10_Plane_k;
[Value(Name="Plane d", Offset=14)]
public Single _14_Plane_d;
[Value(Name="Flags", Offset=18)]
public Bitmask _18_Flags = new Bitmask(new string[] { "Extend Infinitely While Visible","Do Not Floodfill","Aggressive Floodfill" }, 4);
[Value(Name="Priority", Offset=22)]
public Int16 _22_Priority;
}
[Value(Offset=120)]
public Byte[] _120_ = new byte[24];
public _144_weather_pallete[] _144_Weather_Pallete;
[Serializable][Reflexive(Name="Weather Pallete", Offset=144, ChunkSize=120, Label="")]
public class _144_weather_pallete
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Weather System", Offset=32)]
public Dependancy _32_Weather_System;
[Value(Offset=40)]
public Byte[] _40_ = new byte[36];
[Value(Name="Wind", Offset=76)]
public Dependancy _76_Wind;
[Value(Name="Wind Direction i", Offset=84)]
public Single _84_Wind_Direction_i;
[Value(Name="Wind Direction j", Offset=88)]
public Single _88_Wind_Direction_j;
[Value(Name="Wind Direction k", Offset=92)]
public Single _92_Wind_Direction_k;
[Value(Name="Wind Magnitude", Offset=96)]
public Single _96_Wind_Magnitude;
[Value(Name="Wind Scale Function", Offset=100)]
public StringID _100_Wind_Scale_Function;
[Value(Offset=104)]
public Byte[] _104_ = new byte[16];
}
public _152_weather_polyhedral[] _152_Weather_Polyhedral;
[Serializable][Reflexive(Name="Weather Polyhedral", Offset=152, ChunkSize=24, Label="")]
public class _152_weather_polyhedral
{
private int __StartOffset__;
[Value(Name="Bounding Sphere Center X", Offset=0)]
public Single _0_Bounding_Sphere_Center_X;
[Value(Name="Bounding Sphere Center Y", Offset=4)]
public Single _4_Bounding_Sphere_Center_Y;
[Value(Name="Bounding Sphere Center Z", Offset=8)]
public Single _8_Bounding_Sphere_Center_Z;
[Value(Name="Bounding Sphere Radius", Offset=12)]
public Single _12_Bounding_Sphere_Radius;
public _16_planes[] _16_Planes;
[Serializable][Reflexive(Name="Planes", Offset=16, ChunkSize=16, Label="")]
public class _16_planes
{
private int __StartOffset__;
[Value(Name="i", Offset=0)]
public Single _0_i;
[Value(Name="j", Offset=4)]
public Single _4_j;
[Value(Name="k", Offset=8)]
public Single _8_k;
[Value(Name="d", Offset=12)]
public Single _12_d;
}
}
public _160_sound_data[] _160_Sound_Data;
[Serializable][Reflexive(Name="Sound Data", Offset=160, ChunkSize=36, Label="")]
public class _160_sound_data
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[36];
}
public _168_detail_objects[] _168_Detail_Objects;
[Serializable][Reflexive(Name="Detail Objects", Offset=168, ChunkSize=176, Label="")]
public class _168_detail_objects
{
private int __StartOffset__;
[Value(Name="Vertice Count", Offset=0)]
public Int32 _0_Vertice_Count;
[Value(Name="Face Count", Offset=4)]
public Int32 _4_Face_Count;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Offset=24)]
public Byte[] _24_ = new byte[12];
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Raw Offset", Offset=40)]
public Int32 _40_Raw_Offset;
[Value(Name="Raw Size", Offset=44)]
public Int32 _44_Raw_Size;
[Value(Name="Raw Header Size", Offset=48)]
public Int32 _48_Raw_Header_Size;
[Value(Name="Size of Model without header", Offset=52)]
public Int32 _52_Size_of_Model_without_header;
public _56_unknown[] _56_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=56, ChunkSize=16, Label="")]
public class _56_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
}
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Offset=72)]
public Byte[] _72_ = new byte[12];
[Value(Name="Max X", Offset=84)]
public Single _84_Max_X;
[Value(Name="Min X", Offset=88)]
public Single _88_Min_X;
[Value(Name="Max Y", Offset=92)]
public Single _92_Max_Y;
[Value(Name="Min Y", Offset=96)]
public Single _96_Min_Y;
[Value(Name="Max Z", Offset=100)]
public Single _100_Max_Z;
[Value(Name="Max Z", Offset=104)]
public Single _104_Max_Z;
[Value(Name="Unknown", Offset=108)]
public Int32 _108_Unknown;
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Name="Unknown", Offset=124)]
public Int32 _124_Unknown;
[Value(Name="Unknown", Offset=128)]
public Int32 _128_Unknown;
public _132_unknown[] _132_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=132, ChunkSize=8, Label="")]
public class _132_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
public _140_unknown[] _140_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=140, ChunkSize=8, Label="")]
public class _140_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
[Value(Name="Unknown", Offset=148)]
public Int32 _148_Unknown;
[Value(Offset=152)]
public Byte[] _152_ = new byte[24];
}
public _176_materials[] _176_Materials;
[Serializable][Reflexive(Name="Materials", Offset=176, ChunkSize=32, Label="")]
public class _176_materials
{
private int __StartOffset__;
[Value(Name="Old Shader", Offset=0)]
public Dependancy _0_Old_Shader;
[Value(Name="Shader", Offset=8)]
public Dependancy _8_Shader;
public _16_properties[] _16_Properties;
[Serializable][Reflexive(Name="Properties", Offset=16, ChunkSize=8, Label="")]
public class _16_properties
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Int Value", Offset=2)]
public Int16 _2_Int_Value;
[Value(Name="Real Value", Offset=4)]
public Single _4_Real_Value;
}
[Value(Name="Breakable Surface Index", Offset=24)]
public Int16 _24_Breakable_Surface_Index;
[Value(Offset=26)]
public Byte[] _26_ = new byte[6];
}
public _184_sky_owner_cluster[] _184_Sky_Owner_Cluster;
[Serializable][Reflexive(Name="Sky Owner Cluster", Offset=184, ChunkSize=2, Label="")]
public class _184_sky_owner_cluster
{
private int __StartOffset__;
[Value(Name="Cluster Owner", Offset=0)]
public Int16 _0_Cluster_Owner;
}
public _192_conveyor_surfaces[] _192_Conveyor_Surfaces;
[Serializable][Reflexive(Name="Conveyor Surfaces", Offset=192, ChunkSize=24, Label="")]
public class _192_conveyor_surfaces
{
private int __StartOffset__;
[Value(Name="U i", Offset=0)]
public Single _0_U_i;
[Value(Name="U j", Offset=4)]
public Single _4_U_j;
[Value(Name="U k", Offset=8)]
public Single _8_U_k;
[Value(Name="V i", Offset=12)]
public Single _12_V_i;
[Value(Name="V j", Offset=16)]
public Single _16_V_j;
[Value(Name="V j", Offset=20)]
public Single _20_V_j;
}
public _200_breakable_surfaces[] _200_Breakable_Surfaces;
[Serializable][Reflexive(Name="Breakable Surfaces", Offset=200, ChunkSize=24, Label="")]
public class _200_breakable_surfaces
{
private int __StartOffset__;
[Value(Name="Instanced Geometry Instance", Offset=0)]
public Int16 _0_Instanced_Geometry_Instance;
[Value(Name="Breakable Surface Index", Offset=2)]
public Int16 _2_Breakable_Surface_Index;
[Value(Name="Centroid X", Offset=4)]
public Single _4_Centroid_X;
[Value(Name="Centroid Y", Offset=8)]
public Single _8_Centroid_Y;
[Value(Name="Centroid Z", Offset=12)]
public Single _12_Centroid_Z;
[Value(Name="Radius", Offset=16)]
public Single _16_Radius;
[Value(Name="Collision Surface Index", Offset=20)]
public Int32 _20_Collision_Surface_Index;
}
public _208_unknown[] _208_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=208, ChunkSize=116, Label="")]
public class _208_unknown
{
private int __StartOffset__;
public _0_unknown[] _0_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=0, ChunkSize=8, Label="")]
public class _0_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
public _8_unknown[] _8_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=8, ChunkSize=16, Label="")]
public class _8_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
}
public _16_unknown[] _16_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=16, ChunkSize=4, Label="")]
public class _16_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _24_unknown[] _24_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=24, ChunkSize=20, Label="")]
public class _24_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
}
[Value(Offset=32)]
public Byte[] _32_ = new byte[8];
public _40_unknown[] _40_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=40, ChunkSize=12, Label="")]
public class _40_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
public _48_unknown[] _48_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=48, ChunkSize=28, Label="")]
public class _48_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
public _12_unknown[] _12_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=12, ChunkSize=16, Label="")]
public class _12_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
}
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
}
public _56_unknown[] _56_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=56, ChunkSize=20, Label="")]
public class _56_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
}
public _64_unknown[] _64_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=64, ChunkSize=4, Label="")]
public class _64_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
}
[Value(Name="Unknown", Offset=72)]
public Int32 _72_Unknown;
[Value(Offset=76)]
public Byte[] _76_ = new byte[32];
public _108_unknown[] _108_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=108, ChunkSize=72, Label="")]
public class _108_unknown
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[16];
public _16_unknown[] _16_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=16, ChunkSize=36, Label="")]
public class _16_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
}
public _24_unknown[] _24_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=24, ChunkSize=68, Label="")]
public class _24_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
}
[Value(Offset=32)]
public Byte[] _32_ = new byte[8];
public _40_unknown[] _40_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=40, ChunkSize=8, Label="")]
public class _40_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int16 _4_Unknown;
[Value(Name="Unknown", Offset=6)]
public Int16 _6_Unknown;
}
public _48_unknown[] _48_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=48, ChunkSize=4, Label="")]
public class _48_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
}
[Value(Offset=56)]
public Byte[] _56_ = new byte[8];
public _64_unknown[] _64_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=64, ChunkSize=8, Label="")]
public class _64_unknown
{
private int __StartOffset__;
public _0_unknown[] _0_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=0, ChunkSize=12, Label="")]
public class _0_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
}
}
}
}
[Value(Offset=216)]
public Byte[] _216_ = new byte[8];
public _224_background_sound_palette[] _224_Background_Sound_Palette;
[Serializable][Reflexive(Name="Background Sound Palette", Offset=224, ChunkSize=100, Label="")]
public class _224_background_sound_palette
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Background Sound", Offset=32)]
public Dependancy _32_Background_Sound;
[Value(Name="Inside Cluster Sound", Offset=40)]
public Dependancy _40_Inside_Cluster_Sound;
[Value(Name="Cutoff Distance", Offset=48)]
public Single _48_Cutoff_Distance;
[Value(Name="Scale Flags", Offset=52)]
public Bitmask _52_Scale_Flags = new Bitmask(new string[] { "Override Default Scale","Use Adjacent Cluster As Portal Scale","Use Adjacent Cluster As Exterior Scale","Scale With Weather Intensity" }, 4);
[Value(Name="Interior Scale", Offset=56)]
public Single _56_Interior_Scale;
[Value(Name="Portal Scale", Offset=60)]
public Single _60_Portal_Scale;
[Value(Name="Exterior Scale", Offset=64)]
public Single _64_Exterior_Scale;
[Value(Name="Interpolation Speed", Offset=68)]
public Single _68_Interpolation_Speed;
[Value(Name="unused?", Offset=72)]
public Int32 _72_unused_;
[Value(Name="unused?", Offset=76)]
public Int32 _76_unused_;
[Value(Name="unused?", Offset=80)]
public Int32 _80_unused_;
[Value(Name="unused?", Offset=84)]
public Int32 _84_unused_;
[Value(Name="unused?", Offset=88)]
public Single _88_unused_;
[Value(Name="unused?", Offset=92)]
public Int32 _92_unused_;
[Value(Name="unused?", Offset=96)]
public Int32 _96_unused_;
}
public _232_sound_environment_palette[] _232_Sound_Environment_Palette;
[Serializable][Reflexive(Name="Sound Environment Palette", Offset=232, ChunkSize=72, Label="")]
public class _232_sound_environment_palette
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Sound Environment", Offset=32)]
public Dependancy _32_Sound_Environment;
[Value(Name="Cutoff Distance", Offset=40)]
public Single _40_Cutoff_Distance;
[Value(Name="Interpolation Speed", Offset=44)]
public Single _44_Interpolation_Speed;
[Value(Offset=48)]
public Byte[] _48_ = new byte[24];
}
public _240_sound_pas_data[] _240_Sound_PAS_Data;
[Serializable][Reflexive(Name="Sound PAS Data", Offset=240, ChunkSize=1, Label="")]
public class _240_sound_pas_data
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _248_markers[] _248_Markers;
[Serializable][Reflexive(Name="Markers", Offset=248, ChunkSize=60, Label="")]
public class _248_markers
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Rotation i", Offset=32)]
public Single _32_Rotation_i;
[Value(Name="Rotation j", Offset=36)]
public Single _36_Rotation_j;
[Value(Name="Rotation k", Offset=40)]
public Single _40_Rotation_k;
[Value(Name="Rotation w", Offset=44)]
public Single _44_Rotation_w;
[Value(Name="Position X", Offset=48)]
public Single _48_Position_X;
[Value(Name="Position Y", Offset=52)]
public Single _52_Position_Y;
[Value(Name="Position Z", Offset=56)]
public Single _56_Position_Z;
}
public _256_runtime_decals[] _256_Runtime_Decals;
[Serializable][Reflexive(Name="Runtime Decals", Offset=256, ChunkSize=16, Label="")]
public class _256_runtime_decals
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
}
public _264_environment_object_palette[] _264_Environment_Object_Palette;
[Serializable][Reflexive(Name="Environment Object Palette", Offset=264, ChunkSize=20, Label="")]
public class _264_environment_object_palette
{
private int __StartOffset__;
[Value(Name="Definition", Offset=0)]
public Dependancy _0_Definition;
[Value(Name="Model", Offset=8)]
public Dependancy _8_Model;
[Value(Offset=16)]
public Byte[] _16_ = new byte[4];
}
public _272_environment_objects[] _272_Environment_Objects;
[Serializable][Reflexive(Name="Environment Objects", Offset=272, ChunkSize=104, Label="")]
public class _272_environment_objects
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Name="Rotation i", Offset=32)]
public Single _32_Rotation_i;
[Value(Name="Rotation j", Offset=36)]
public Single _36_Rotation_j;
[Value(Name="Rotation k", Offset=40)]
public Single _40_Rotation_k;
[Value(Name="Rotation w", Offset=44)]
public Single _44_Rotation_w;
[Value(Name="X", Offset=48)]
public Single _48_X;
[Value(Name="Y", Offset=52)]
public Single _52_Y;
[Value(Name="Z", Offset=56)]
public Single _56_Z;
[Value(Name="Palette Index", Offset=60)]
public Int16 _60_Palette_Index;
[Value(Name="Unique ID", Offset=62)]
public Int32 _62_Unique_ID;
[Value(Name="Exported Object Type", Offset=66)]
public Int16 _66_Exported_Object_Type;
[Value(Name="Scenario Object Name", Offset=68)]
public H2.DataTypes.String _68_Scenario_Object_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Offset=100)]
public Byte[] _100_ = new byte[4];
}
[Value(Offset=280)]
public Byte[] _280_ = new byte[44];
public _324_bsp_permutations[] _324_Bsp_Permutations;
[Serializable][Reflexive(Name="Bsp Permutations", Offset=324, ChunkSize=200, Label="")]
public class _324_bsp_permutations
{
private int __StartOffset__;
[Value(Name="Total Vertex Count", Offset=0)]
public Int16 _0_Total_Vertex_Count;
[Value(Name="Total Triangle count", Offset=2)]
public Int16 _2_Total_Triangle_count;
[Value(Name="Total Part Count", Offset=4)]
public Int16 _4_Total_Part_Count;
[Value(Name="Shadow-Casting Triangle Count", Offset=6)]
public Int16 _6_Shadow_Casting_Triangle_Count;
[Value(Name="Shadow-Casting Part Count", Offset=8)]
public Int16 _8_Shadow_Casting_Part_Count;
[Value(Name="Opaque Point Count", Offset=10)]
public Int16 _10_Opaque_Point_Count;
[Value(Name="Opaque Vertex Count", Offset=12)]
public Int16 _12_Opaque_Vertex_Count;
[Value(Name="Opaque Part Count", Offset=14)]
public Int16 _14_Opaque_Part_Count;
[Value(Name="Opaque Max Nodes/Vertex", Offset=16)]
public Int16 _16_Opaque_Max_Nodes_Vertex;
[Value(Name="Transparent Max Nodes/Vertex", Offset=18)]
public Int16 _18_Transparent_Max_Nodes_Vertex;
[Value(Name="Shadow-Casting Rigid Triangle", Offset=20)]
public Int16 _20_Shadow_Casting_Rigid_Triangle;
[Value(Name="Geometry Classification", Offset=22)]
public H2.DataTypes.Enum _22_Geometry_Classification = new H2.DataTypes.Enum(new string[] {  }, 2);
public _24_bounding_box[] _24_Bounding_Box;
[Serializable][Reflexive(Name="Bounding Box", Offset=24, ChunkSize=40, Label="")]
public class _24_bounding_box
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
}
[Value(Name="Geometry Compression Flags", Offset=32)]
public Bitmask _32_Geometry_Compression_Flags = new Bitmask(new string[] { "Compressed Postion","Compressed Texcoord","Compressed Secondary Texcoord" }, 2);
[Value(Name="Hardware Node Count", Offset=34)]
public Byte _34_Hardware_Node_Count;
[Value(Name="Node Map Size", Offset=35)]
public Byte _35_Node_Map_Size;
[Value(Name="Software Plane Count", Offset=36)]
public Byte _36_Software_Plane_Count;
[Value(Name="Total Subpart Count", Offset=37)]
public Byte _37_Total_Subpart_Count;
[Value(Name="Section Lighting Flags", Offset=38)]
public Bitmask _38_Section_Lighting_Flags = new Bitmask(new string[] { "Has Im Texcoords","Has Im Inc. Rad.","Has Im Colors","Has Im Prt" }, 2);
[Value(Name="Raw Offset", Offset=40)]
public Int32 _40_Raw_Offset;
[Value(Name="Raw Size", Offset=44)]
public Int32 _44_Raw_Size;
[Value(Name="Header Size", Offset=48)]
public Int32 _48_Header_Size;
[Value(Name="Size - Header Size", Offset=52)]
public Int32 _52_Size___Header_Size;
public _56_resources[] _56_Resources;
[Serializable][Reflexive(Name="Resources", Offset=56, ChunkSize=16, Label="")]
public class _56_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Primary Locator", Offset=4)]
public Int16 _4_Primary_Locator;
[Value(Name="Secondary Locator", Offset=6)]
public Int16 _6_Secondary_Locator;
[Value(Name="Resource Data Size", Offset=8)]
public Int32 _8_Resource_Data_Size;
[Value(Name="Resource Data Offset", Offset=12)]
public Int32 _12_Resource_Data_Offset;
}
[Value(Name="Owner Tag Section Offset", Offset=64)]
public Int16 _64_Owner_Tag_Section_Offset;
[Value(Offset=66)]
public Byte[] _66_ = new byte[10];
public _76_render_data[] _76_Render_Data;
[Serializable][Reflexive(Name="Render Data", Offset=76, ChunkSize=64, Label="")]
public class _76_render_data
{
private int __StartOffset__;
public _0_parts[] _0_Parts;
[Serializable][Reflexive(Name="Parts", Offset=0, ChunkSize=0, Label="")]
public class _0_parts
{
private int __StartOffset__;
}
public _8_subparts[] _8_Subparts;
[Serializable][Reflexive(Name="Subparts", Offset=8, ChunkSize=0, Label="")]
public class _8_subparts
{
private int __StartOffset__;
}
public _16_visibility_bounds[] _16_Visibility_Bounds;
[Serializable][Reflexive(Name="Visibility Bounds", Offset=16, ChunkSize=0, Label="")]
public class _16_visibility_bounds
{
private int __StartOffset__;
}
public _24_raw_vertices[] _24_Raw_Vertices;
[Serializable][Reflexive(Name="Raw Vertices", Offset=24, ChunkSize=0, Label="")]
public class _24_raw_vertices
{
private int __StartOffset__;
}
public _32_strip_indices[] _32_Strip_Indices;
[Serializable][Reflexive(Name="Strip Indices", Offset=32, ChunkSize=0, Label="")]
public class _32_strip_indices
{
private int __StartOffset__;
}
public _40_visibility_mopp_code[] _40_Visibility_MOPP_Code;
[Serializable][Reflexive(Name="Visibility MOPP Code", Offset=40, ChunkSize=1, Label="")]
public class _40_visibility_mopp_code
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _48_mopp_reorder_table[] _48_MOPP_Reorder_Table;
[Serializable][Reflexive(Name="MOPP Reorder Table", Offset=48, ChunkSize=0, Label="")]
public class _48_mopp_reorder_table
{
private int __StartOffset__;
}
public _56_vertex_buffers[] _56_Vertex_Buffers;
[Serializable][Reflexive(Name="Vertex Buffers", Offset=56, ChunkSize=0, Label="")]
public class _56_vertex_buffers
{
private int __StartOffset__;
}
}
public _84_index_reorder_table[] _84_Index_Reorder_Table;
[Serializable][Reflexive(Name="Index Reorder Table", Offset=84, ChunkSize=4, Label="")]
public class _84_index_reorder_table
{
private int __StartOffset__;
[Value(Name="Index", Offset=0)]
public Int16 _0_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
[Value(Name="Checksum", Offset=92)]
public Int32 _92_Checksum;
[Value(Name="Bounding Sphere X", Offset=96)]
public Single _96_Bounding_Sphere_X;
[Value(Name="Bounding Sphere Y", Offset=100)]
public Single _100_Bounding_Sphere_Y;
[Value(Name="Bounding Sphere Z", Offset=104)]
public Single _104_Bounding_Sphere_Z;
[Value(Name="Bounding Sphere Radius", Offset=108)]
public Single _108_Bounding_Sphere_Radius;
public _112_bsp_3d_nodes[] _112_BSP_3D_Nodes;
[Serializable][Reflexive(Name="BSP 3D Nodes", Offset=112, ChunkSize=0, Label="")]
public class _112_bsp_3d_nodes
{
private int __StartOffset__;
}
public _120_planes[] _120_Planes;
[Serializable][Reflexive(Name="Planes", Offset=120, ChunkSize=16, Label="")]
public class _120_planes
{
private int __StartOffset__;
[Value(Name="Plane i", Offset=0)]
public Single _0_Plane_i;
[Value(Name="Plane j", Offset=4)]
public Single _4_Plane_j;
[Value(Name="Plane k", Offset=8)]
public Single _8_Plane_k;
[Value(Name="Plane d", Offset=12)]
public Single _12_Plane_d;
}
public _128_leaves[] _128_Leaves;
[Serializable][Reflexive(Name="Leaves", Offset=128, ChunkSize=4, Label="")]
public class _128_leaves
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Contains Double-Sided Surfaces" }, 1);
[Value(Name="BSP 2D Reference Count", Offset=1)]
public Byte _1_BSP_2D_Reference_Count;
[Value(Name="First BSP 2D Reference", Offset=2)]
public Int16 _2_First_BSP_2D_Reference;
}
public _136_bsp_2d_references[] _136_BSP_2D_References;
[Serializable][Reflexive(Name="BSP 2D References", Offset=136, ChunkSize=4, Label="")]
public class _136_bsp_2d_references
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="BSP 2D Node", Offset=2)]
public Int16 _2_BSP_2D_Node;
}
public _144_bsp_2d_nodes[] _144_BSP_2D_Nodes;
[Serializable][Reflexive(Name="BSP 2D Nodes", Offset=144, ChunkSize=16, Label="")]
public class _144_bsp_2d_nodes
{
private int __StartOffset__;
[Value(Name="Plane i", Offset=0)]
public Single _0_Plane_i;
[Value(Name="Plane j", Offset=4)]
public Single _4_Plane_j;
[Value(Name="Plane d", Offset=8)]
public Single _8_Plane_d;
[Value(Name="Left Child", Offset=12)]
public Int16 _12_Left_Child;
[Value(Name="Right Child", Offset=14)]
public Int16 _14_Right_Child;
}
public _152_surfaces[] _152_Surfaces;
[Serializable][Reflexive(Name="Surfaces", Offset=152, ChunkSize=8, Label="")]
public class _152_surfaces
{
private int __StartOffset__;
[Value(Name="Plane", Offset=0)]
public Int16 _0_Plane;
[Value(Name="First Edge", Offset=2)]
public Int16 _2_First_Edge;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Two-Sided","Invisible","Climable","Breakable","Invaild","Conveyor" }, 1);
[Value(Name="Breakable Surface #", Offset=5)]
public Byte _5_Breakable_Surface_No;
[Value(Name="Material #", Offset=6)]
public Int16 _6_Material_No;
}
public _160_edges[] _160_Edges;
[Serializable][Reflexive(Name="Edges", Offset=160, ChunkSize=12, Label="")]
public class _160_edges
{
private int __StartOffset__;
[Value(Name="Start Vertex", Offset=0)]
public Int16 _0_Start_Vertex;
[Value(Name="End Vertex", Offset=2)]
public Int16 _2_End_Vertex;
[Value(Name="Forward Edge", Offset=4)]
public Int16 _4_Forward_Edge;
[Value(Name="Backward Edge", Offset=6)]
public Int16 _6_Backward_Edge;
[Value(Name="Left Surface", Offset=8)]
public Int16 _8_Left_Surface;
[Value(Name="Right Surface", Offset=10)]
public Int16 _10_Right_Surface;
}
public _168_vertices[] _168_Vertices;
[Serializable][Reflexive(Name="Vertices", Offset=168, ChunkSize=16, Label="")]
public class _168_vertices
{
private int __StartOffset__;
[Value(Name="Point X", Offset=0)]
public Single _0_Point_X;
[Value(Name="Point Y", Offset=4)]
public Single _4_Point_Y;
[Value(Name="Point Z", Offset=8)]
public Single _8_Point_Z;
[Value(Name="First Edge", Offset=12)]
public Int16 _12_First_Edge;
[Value(Offset=14)]
public Byte[] _14_ = new byte[2];
}
public _176_bsp_physics[] _176_BSP_Physics;
[Serializable][Reflexive(Name="BSP Physics", Offset=176, ChunkSize=112, Label="")]
public class _176_bsp_physics
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Int32 _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Offset=52)]
public Byte[] _52_ = new byte[12];
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Int32 _72_Unknown;
[Value(Name="Unknown", Offset=76)]
public Int32 _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Int32 _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Int32 _88_Unknown;
[Value(Name="Unknown", Offset=92)]
public Int32 _92_Unknown;
[Value(Name="Unknown", Offset=96)]
public Int32 _96_Unknown;
public _100_mopp_code_data[] _100_MOPP_Code_Data;
[Serializable][Reflexive(Name="MOPP Code Data", Offset=100, ChunkSize=1, Label="")]
public class _100_mopp_code_data
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Unknown", Offset=108)]
public Int32 _108_Unknown;
}
public _184_render_leaves[] _184_Render_Leaves;
[Serializable][Reflexive(Name="Render Leaves", Offset=184, ChunkSize=8, Label="")]
public class _184_render_leaves
{
private int __StartOffset__;
[Value(Name="Cluster", Offset=0)]
public Int16 _0_Cluster;
[Value(Name="Surface Reference Count", Offset=2)]
public Int16 _2_Surface_Reference_Count;
[Value(Name="First Surface Reference Index", Offset=4)]
public Int32 _4_First_Surface_Reference_Index;
}
public _192_surface_references[] _192_Surface_References;
[Serializable][Reflexive(Name="Surface References", Offset=192, ChunkSize=8, Label="")]
public class _192_surface_references
{
private int __StartOffset__;
[Value(Name="Strip Index", Offset=0)]
public Int16 _0_Strip_Index;
[Value(Name="Lightmap Triangle", Offset=2)]
public Int16 _2_Lightmap_Triangle;
[Value(Name="BSP Node Index", Offset=4)]
public Int32 _4_BSP_Node_Index;
}
}
public _332_instanced_geometry_instances[] _332_Instanced_Geometry_Instances;
[Serializable][Reflexive(Name="Instanced Geometry Instances", Offset=332, ChunkSize=88, Label="")]
public class _332_instanced_geometry_instances
{
private int __StartOffset__;
[Value(Name="Scale", Offset=0)]
public Single _0_Scale;
[Value(Name="Forward i", Offset=4)]
public Single _4_Forward_i;
[Value(Name="Forward j", Offset=8)]
public Single _8_Forward_j;
[Value(Name="Forward k", Offset=12)]
public Single _12_Forward_k;
[Value(Name="Left i", Offset=16)]
public Single _16_Left_i;
[Value(Name="Left j", Offset=20)]
public Single _20_Left_j;
[Value(Name="Left k", Offset=24)]
public Single _24_Left_k;
[Value(Name="Up i", Offset=28)]
public Single _28_Up_i;
[Value(Name="Up j", Offset=32)]
public Single _32_Up_j;
[Value(Name="Up k", Offset=36)]
public Single _36_Up_k;
[Value(Name="Position X", Offset=40)]
public Single _40_Position_X;
[Value(Name="Position Y", Offset=44)]
public Single _44_Position_Y;
[Value(Name="Position Z", Offset=48)]
public Single _48_Position_Z;
[Value(Name="Instance Definition #", Offset=52)]
public Int16 _52_Instance_Definition_No;
[Value(Name="Flags", Offset=54)]
public Bitmask _54_Flags = new Bitmask(new string[] { "Not In Lightprobes" }, 2);
[Value(Name="Checksum", Offset=56)]
public Int32 _56_Checksum;
[Value(Offset=60)]
public Byte[] _60_ = new byte[20];
[Value(Name="Name", Offset=80)]
public StringID _80_Name;
[Value(Name="Pathfinding Policy", Offset=84)]
public H2.DataTypes.Enum _84_Pathfinding_Policy = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Lightmapping Policy", Offset=86)]
public H2.DataTypes.Enum _86_Lightmapping_Policy = new H2.DataTypes.Enum(new string[] {  }, 2);
}
public _340_ambience_sound_clusters[] _340_Ambience_Sound_Clusters;
[Serializable][Reflexive(Name="Ambience Sound Clusters", Offset=340, ChunkSize=20, Label="")]
public class _340_ambience_sound_clusters
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
public _4_enclosing_portal_designators[] _4_Enclosing_Portal_Designators;
[Serializable][Reflexive(Name="Enclosing Portal Designators", Offset=4, ChunkSize=4, Label="")]
public class _4_enclosing_portal_designators
{
private int __StartOffset__;
[Value(Name="Portal Designator", Offset=0)]
public Int16 _0_Portal_Designator;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
public _12_interior_cluster_indices[] _12_Interior_Cluster_Indices;
[Serializable][Reflexive(Name="Interior Cluster Indices", Offset=12, ChunkSize=4, Label="")]
public class _12_interior_cluster_indices
{
private int __StartOffset__;
[Value(Name="Interior Cluster Index", Offset=0)]
public Int16 _0_Interior_Cluster_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
}
public _348_reverb_sound_clusters[] _348_Reverb_Sound_Clusters;
[Serializable][Reflexive(Name="Reverb Sound Clusters", Offset=348, ChunkSize=20, Label="")]
public class _348_reverb_sound_clusters
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
public _4_enclosing_portal_designators[] _4_Enclosing_Portal_Designators;
[Serializable][Reflexive(Name="Enclosing Portal Designators", Offset=4, ChunkSize=4, Label="")]
public class _4_enclosing_portal_designators
{
private int __StartOffset__;
[Value(Name="Portal Designator", Offset=0)]
public Int16 _0_Portal_Designator;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
public _12_interior_cluster_indices[] _12_Interior_Cluster_Indices;
[Serializable][Reflexive(Name="Interior Cluster Indices", Offset=12, ChunkSize=4, Label="")]
public class _12_interior_cluster_indices
{
private int __StartOffset__;
[Value(Name="Interior Cluster Index", Offset=0)]
public Int16 _0_Interior_Cluster_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
}
public _356_transparent_planes[] _356_Transparent_Planes;
[Serializable][Reflexive(Name="Transparent Planes", Offset=356, ChunkSize=20, Label="")]
public class _356_transparent_planes
{
private int __StartOffset__;
[Value(Name="Section Index", Offset=0)]
public Int16 _0_Section_Index;
[Value(Name="Part Index", Offset=2)]
public Int16 _2_Part_Index;
[Value(Name="Plane i", Offset=4)]
public Single _4_Plane_i;
[Value(Name="Plane j", Offset=8)]
public Single _8_Plane_j;
[Value(Name="Plane k", Offset=12)]
public Single _12_Plane_k;
[Value(Name="Plane d", Offset=16)]
public Single _16_Plane_d;
}
[Value(Name="Vehicle Sperical Limit Radius", Offset=364)]
public Single _364_Vehicle_Sperical_Limit_Radius;
[Value(Name="Vehicle Sperical Limit X", Offset=368)]
public Single _368_Vehicle_Sperical_Limit_X;
[Value(Name="Vehicle Sperical Limit Y", Offset=372)]
public Single _372_Vehicle_Sperical_Limit_Y;
[Value(Name="Vehicle Sperical Limit Z", Offset=376)]
public Single _376_Vehicle_Sperical_Limit_Z;
[Value(Offset=380)]
public Byte[] _380_ = new byte[96];
public _476_unknown[] _476_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=476, ChunkSize=88, Label="")]
public class _476_unknown
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[64];
public _64_unknown[] _64_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=64, ChunkSize=84, Label="")]
public class _64_unknown
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[74];
[Value(Name="Unknown", Offset=74)]
public Int16 _74_Unknown;
[Value(Name="Unknown", Offset=76)]
public Int16 _76_Unknown;
[Value(Name="Unknown", Offset=78)]
public Int16 _78_Unknown;
[Value(Name="Unknown", Offset=80)]
public Int16 _80_Unknown;
[Value(Name="Unknown", Offset=82)]
public Int16 _82_Unknown;
}
[Value(Offset=72)]
public Byte[] _72_ = new byte[16];
}
[Value(Name="Decorators", Offset=484)]
public Dependancy _484_Decorators;
public _488_mopp_code[] _488_MOPP_Code;
[Serializable][Reflexive(Name="MOPP Code", Offset=488, ChunkSize=1, Label="")]
public class _488_mopp_code
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=496)]
public Byte[] _496_ = new byte[4];
[Value(Name="MOPP Bounds Min X", Offset=500)]
public Single _500_MOPP_Bounds_Min_X;
[Value(Name="MOPP Bounds Min Y", Offset=504)]
public Single _504_MOPP_Bounds_Min_Y;
[Value(Name="MOPP Bounds Min Z", Offset=508)]
public Single _508_MOPP_Bounds_Min_Z;
[Value(Name="MOPP Bounds Max X", Offset=512)]
public Single _512_MOPP_Bounds_Max_X;
[Value(Name="MOPP Bounds Max Y", Offset=516)]
public Single _516_MOPP_Bounds_Max_Y;
[Value(Name="MOPP Bounds Max Z", Offset=520)]
public Single _520_MOPP_Bounds_Max_Z;
public _524_mopp_code[] _524_MOPP_Code;
[Serializable][Reflexive(Name="MOPP Code", Offset=524, ChunkSize=1, Label="")]
public class _524_mopp_code
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _532_breakable_surface_key_table[] _532_Breakable_Surface_Key_Table;
[Serializable][Reflexive(Name="Breakable Surface Key Table", Offset=532, ChunkSize=32, Label="")]
public class _532_breakable_surface_key_table
{
private int __StartOffset__;
[Value(Name="Instance Geometry Index", Offset=0)]
public Int16 _0_Instance_Geometry_Index;
[Value(Name="Breakable Surface Index", Offset=2)]
public Int16 _2_Breakable_Surface_Index;
[Value(Name="Seed Surface Index", Offset=4)]
public Int32 _4_Seed_Surface_Index;
[Value(Name="X0", Offset=8)]
public Single _8_X0;
[Value(Name="X1", Offset=12)]
public Single _12_X1;
[Value(Name="Y0", Offset=16)]
public Single _16_Y0;
[Value(Name="Y1", Offset=20)]
public Single _20_Y1;
[Value(Name="Z0", Offset=24)]
public Single _24_Z0;
[Value(Name="Z1", Offset=28)]
public Single _28_Z1;
}
public _540_water_definitions[] _540_Water_Definitions;
[Serializable][Reflexive(Name="Water Definitions", Offset=540, ChunkSize=172, Label="")]
public class _540_water_definitions
{
private int __StartOffset__;
[Value(Name="Shader", Offset=0)]
public Dependancy _0_Shader;
public _8_section[] _8_Section;
[Serializable][Reflexive(Name="Section", Offset=8, ChunkSize=64, Label="")]
public class _8_section
{
private int __StartOffset__;
public _0_parts[] _0_Parts;
[Serializable][Reflexive(Name="Parts", Offset=0, ChunkSize=0, Label="")]
public class _0_parts
{
private int __StartOffset__;
}
public _8_subparts[] _8_Subparts;
[Serializable][Reflexive(Name="Subparts", Offset=8, ChunkSize=0, Label="")]
public class _8_subparts
{
private int __StartOffset__;
}
public _16_visibility_bounds[] _16_Visibility_Bounds;
[Serializable][Reflexive(Name="Visibility Bounds", Offset=16, ChunkSize=0, Label="")]
public class _16_visibility_bounds
{
private int __StartOffset__;
}
public _24_raw_vertices[] _24_Raw_Vertices;
[Serializable][Reflexive(Name="Raw Vertices", Offset=24, ChunkSize=0, Label="")]
public class _24_raw_vertices
{
private int __StartOffset__;
}
public _32_strip_indices[] _32_Strip_Indices;
[Serializable][Reflexive(Name="Strip Indices", Offset=32, ChunkSize=0, Label="")]
public class _32_strip_indices
{
private int __StartOffset__;
}
public _40_visibility_mopp_code[] _40_Visibility_MOPP_Code;
[Serializable][Reflexive(Name="Visibility MOPP Code", Offset=40, ChunkSize=1, Label="")]
public class _40_visibility_mopp_code
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _48_mopp_reorder_table[] _48_MOPP_Reorder_Table;
[Serializable][Reflexive(Name="MOPP Reorder Table", Offset=48, ChunkSize=0, Label="")]
public class _48_mopp_reorder_table
{
private int __StartOffset__;
}
public _56_vertex_buffers[] _56_Vertex_Buffers;
[Serializable][Reflexive(Name="Vertex Buffers", Offset=56, ChunkSize=0, Label="")]
public class _56_vertex_buffers
{
private int __StartOffset__;
}
}
[Value(Name="Block Offset", Offset=16)]
public Int32 _16_Block_Offset;
[Value(Name="Block Size", Offset=20)]
public Int32 _20_Block_Size;
[Value(Name="Section Data Size", Offset=24)]
public Int32 _24_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=28)]
public Int32 _28_Resource_Data_Size;
public _32_resources[] _32_Resources;
[Serializable][Reflexive(Name="Resources", Offset=32, ChunkSize=16, Label="")]
public class _32_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Primary Locator", Offset=2)]
public Int16 _2_Primary_Locator;
[Value(Name="Secondary Locator", Offset=4)]
public Int16 _4_Secondary_Locator;
[Value(Name="Resource Size", Offset=6)]
public Int16 _6_Resource_Size;
[Value(Name="Resource Data Size", Offset=8)]
public Int32 _8_Resource_Data_Size;
[Value(Name="Resource Data Offset", Offset=12)]
public Int32 _12_Resource_Data_Offset;
}
[Value(Offset=40)]
public Byte[] _40_ = new byte[8];
[Value(Name="Owner Tag Section Offset", Offset=48)]
public Int16 _48_Owner_Tag_Section_Offset;
[Value(Offset=50)]
public Byte[] _50_ = new byte[2];
[Value(Name="Sun Spot Color R", Offset=52)]
public Single _52_Sun_Spot_Color_R;
[Value(Name="Sun Spot Color G", Offset=56)]
public Single _56_Sun_Spot_Color_G;
[Value(Name="Sun Spot Color B", Offset=60)]
public Single _60_Sun_Spot_Color_B;
[Value(Name="Reflection Tint R", Offset=64)]
public Single _64_Reflection_Tint_R;
[Value(Name="Reflection Tint G", Offset=68)]
public Single _68_Reflection_Tint_G;
[Value(Name="Reflection Tint B", Offset=72)]
public Single _72_Reflection_Tint_B;
[Value(Name="Refraction Tint R", Offset=76)]
public Single _76_Refraction_Tint_R;
[Value(Name="Refraction Tint G", Offset=80)]
public Single _80_Refraction_Tint_G;
[Value(Name="Refraction Tint B", Offset=84)]
public Single _84_Refraction_Tint_B;
[Value(Name="Horizion Color R", Offset=88)]
public Single _88_Horizion_Color_R;
[Value(Name="Horizion Color G", Offset=92)]
public Single _92_Horizion_Color_G;
[Value(Name="Horizion Color B", Offset=96)]
public Single _96_Horizion_Color_B;
[Value(Name="Sun Specular Power", Offset=100)]
public Single _100_Sun_Specular_Power;
[Value(Name="Reflection Bump Scale", Offset=104)]
public Single _104_Reflection_Bump_Scale;
[Value(Name="Refraction Bump Scale", Offset=108)]
public Single _108_Refraction_Bump_Scale;
[Value(Name="Fresnel Scale", Offset=112)]
public Single _112_Fresnel_Scale;
[Value(Name="Sun Dir Heading", Offset=116)]
public Single _116_Sun_Dir_Heading;
[Value(Name="Sun Dir Pitch", Offset=120)]
public Single _120_Sun_Dir_Pitch;
[Value(Name="FOV", Offset=124)]
public Single _124_FOV;
[Value(Name="Aspect", Offset=128)]
public Single _128_Aspect;
[Value(Name="Height", Offset=132)]
public Single _132_Height;
[Value(Name="Farz", Offset=136)]
public Single _136_Farz;
[Value(Name="Rotate Offset", Offset=140)]
public Single _140_Rotate_Offset;
[Value(Name="Center i", Offset=144)]
public Single _144_Center_i;
[Value(Name="Center j", Offset=148)]
public Single _148_Center_j;
[Value(Name="Extents i", Offset=152)]
public Single _152_Extents_i;
[Value(Name="Extents j", Offset=156)]
public Single _156_Extents_j;
[Value(Name="Fog Near", Offset=160)]
public Single _160_Fog_Near;
[Value(Name="Fog Far", Offset=164)]
public Single _164_Fog_Far;
[Value(Name="Dynamic Height Bias", Offset=168)]
public Single _168_Dynamic_Height_Bias;
}
public _548_portal___device_mapping[] _548_Portal___Device_Mapping;
[Serializable][Reflexive(Name="Portal - Device Mapping", Offset=548, ChunkSize=16, Label="")]
public class _548_portal___device_mapping
{
private int __StartOffset__;
public _0_device_portal_association[] _0_Device_Portal_Association;
[Serializable][Reflexive(Name="Device Portal Association", Offset=0, ChunkSize=14, Label="")]
public class _0_device_portal_association
{
private int __StartOffset__;
[Value(Name="Unique ID", Offset=0)]
public Int32 _0_Unique_ID;
[Value(Name="Origin BSP Index", Offset=4)]
public Int16 _4_Origin_BSP_Index;
[Value(Name="Type", Offset=6)]
public H2.DataTypes.Enum _6_Type = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Source", Offset=8)]
public H2.DataTypes.Enum _8_Source = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="First Game Potal Index", Offset=10)]
public Int16 _10_First_Game_Potal_Index;
[Value(Name="Game Portal Count", Offset=12)]
public Int16 _12_Game_Portal_Count;
}
public _8_game_portal_to_portal_map[] _8_Game_Portal_To_Portal_Map;
[Serializable][Reflexive(Name="Game Portal To Portal Map", Offset=8, ChunkSize=4, Label="")]
public class _8_game_portal_to_portal_map
{
private int __StartOffset__;
[Value(Name="Portal Index", Offset=0)]
public Int16 _0_Portal_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
}
public _556_audibility[] _556_Audibility;
[Serializable][Reflexive(Name="Audibility", Offset=556, ChunkSize=52, Label="")]
public class _556_audibility
{
private int __StartOffset__;
[Value(Name="Door Portal Count", Offset=0)]
public Int32 _0_Door_Portal_Count;
[Value(Name="Cluster Distance Bounds", Offset=4)]
public Single _4_Cluster_Distance_Bounds;
[Value(Name="...To", Offset=8)]
public Single _8____To;
public _12_encoded_door_pas[] _12_Encoded_Door_Pas;
[Serializable][Reflexive(Name="Encoded Door Pas", Offset=12, ChunkSize=0, Label="")]
public class _12_encoded_door_pas
{
private int __StartOffset__;
}
public _20_cluster_door_portal_encoded_pas[] _20_Cluster_Door_Portal_Encoded_Pas;
[Serializable][Reflexive(Name="Cluster Door Portal Encoded Pas", Offset=20, ChunkSize=0, Label="")]
public class _20_cluster_door_portal_encoded_pas
{
private int __StartOffset__;
}
public _28_ai_deafening_pas[] _28_AI_Deafening_Pas;
[Serializable][Reflexive(Name="AI Deafening Pas", Offset=28, ChunkSize=0, Label="")]
public class _28_ai_deafening_pas
{
private int __StartOffset__;
}
public _36_cluster_distances[] _36_Cluster_Distances;
[Serializable][Reflexive(Name="Cluster Distances", Offset=36, ChunkSize=0, Label="")]
public class _36_cluster_distances
{
private int __StartOffset__;
}
public _44_machine_door_mapping[] _44_Machine_Door_Mapping;
[Serializable][Reflexive(Name="Machine Door Mapping", Offset=44, ChunkSize=4, Label="")]
public class _44_machine_door_mapping
{
private int __StartOffset__;
[Value(Name="Machine Door Index", Offset=0)]
public Int16 _0_Machine_Door_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
}
}
public _564_object_fake_lightprobes[] _564_Object_Fake_Lightprobes;
[Serializable][Reflexive(Name="Object Fake Lightprobes", Offset=564, ChunkSize=92, Label="")]
public class _564_object_fake_lightprobes
{
private int __StartOffset__;
[Value(Name="Unique ID", Offset=0)]
public Int32 _0_Unique_ID;
[Value(Name="Origin BSP Index", Offset=4)]
public Int16 _4_Origin_BSP_Index;
[Value(Name="Type", Offset=6)]
public H2.DataTypes.Enum _6_Type = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Source", Offset=8)]
public H2.DataTypes.Enum _8_Source = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Unknown", Offset=10)]
public Int16 _10_Unknown;
[Value(Name="Ambient R", Offset=12)]
public Single _12_Ambient_R;
[Value(Name="Ambient G", Offset=16)]
public Single _16_Ambient_G;
[Value(Name="Ambient B", Offset=20)]
public Single _20_Ambient_B;
[Value(Name="Shadow Direction i", Offset=24)]
public Single _24_Shadow_Direction_i;
[Value(Name="Shadow Direction j", Offset=28)]
public Single _28_Shadow_Direction_j;
[Value(Name="Shadow Direction k", Offset=32)]
public Single _32_Shadow_Direction_k;
[Value(Name="Lighting Accuracy", Offset=36)]
public Single _36_Lighting_Accuracy;
[Value(Name="Shadow Opacity", Offset=40)]
public Single _40_Shadow_Opacity;
[Value(Name="Primary Direction Color R", Offset=44)]
public Single _44_Primary_Direction_Color_R;
[Value(Name="Primary Direction Color G", Offset=48)]
public Single _48_Primary_Direction_Color_G;
[Value(Name="Primary Direction Color B", Offset=52)]
public Single _52_Primary_Direction_Color_B;
[Value(Name="Primary Direction i", Offset=56)]
public Single _56_Primary_Direction_i;
[Value(Name="Primary Direction j", Offset=60)]
public Single _60_Primary_Direction_j;
[Value(Name="Primary Direction k", Offset=64)]
public Single _64_Primary_Direction_k;
[Value(Name="Secondary Direction Color R", Offset=68)]
public Single _68_Secondary_Direction_Color_R;
[Value(Name="Secondary Direction Color G", Offset=72)]
public Single _72_Secondary_Direction_Color_G;
[Value(Name="Secondary Direction Color B", Offset=76)]
public Single _76_Secondary_Direction_Color_B;
[Value(Name="Secondary Direction i", Offset=80)]
public Single _80_Secondary_Direction_i;
[Value(Name="Secondary Direction j", Offset=84)]
public Single _84_Secondary_Direction_j;
[Value(Name="Secondary Direction k", Offset=88)]
public Single _88_Secondary_Direction_k;
}
public _572_decorators[] _572_Decorators;
[Serializable][Reflexive(Name="Decorators", Offset=572, ChunkSize=48, Label="")]
public class _572_decorators
{
private int __StartOffset__;
[Value(Name="Grid Origin X", Offset=0)]
public Single _0_Grid_Origin_X;
[Value(Name="Grid Origin Y", Offset=4)]
public Single _4_Grid_Origin_Y;
[Value(Name="Grid Origin Z", Offset=8)]
public Single _8_Grid_Origin_Z;
[Value(Name="Cell Count Per Dimension", Offset=12)]
public Single _12_Cell_Count_Per_Dimension;
public _16_cache_blocks[] _16_Cache_Blocks;
[Serializable][Reflexive(Name="Cache Blocks", Offset=16, ChunkSize=44, Label="")]
public class _16_cache_blocks
{
private int __StartOffset__;
[Value(Name="Block Offset", Offset=0)]
public UInt32 _0_Block_Offset;
[Value(Name="Block Size", Offset=4)]
public UInt32 _4_Block_Size;
[Value(Name="Section Data Size", Offset=8)]
public UInt32 _8_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=12)]
public UInt32 _12_Resource_Data_Size;
public _16_resources[] _16_Resources;
[Serializable][Reflexive(Name="Resources", Offset=16, ChunkSize=16, Label="")]
public class _16_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Primary Locator", Offset=4)]
public Int16 _4_Primary_Locator;
[Value(Name="Secondary Locator", Offset=6)]
public Int16 _6_Secondary_Locator;
[Value(Name="Resource Data Size", Offset=8)]
public UInt32 _8_Resource_Data_Size;
[Value(Name="Resource Data Offset", Offset=12)]
public UInt32 _12_Resource_Data_Offset;
}
[Value(Name="Owner Tag Section Offset", Offset=24)]
public Int32 _24_Owner_Tag_Section_Offset;
[Value(Offset=28)]
public Byte[] _28_ = new byte[8];
public _36_cache_block_data[] _36_Cache_Block_Data;
[Serializable][Reflexive(Name="Cache Block Data", Offset=36, ChunkSize=40, Label="")]
public class _36_cache_block_data
{
private int __StartOffset__;
public _0_placements[] _0_Placements;
[Serializable][Reflexive(Name="Placements", Offset=0, ChunkSize=0, Label="")]
public class _0_placements
{
private int __StartOffset__;
}
public _8_decal_vertices[] _8_Decal_Vertices;
[Serializable][Reflexive(Name="Decal Vertices", Offset=8, ChunkSize=0, Label="")]
public class _8_decal_vertices
{
private int __StartOffset__;
}
public _16_decal_indices[] _16_Decal_Indices;
[Serializable][Reflexive(Name="Decal Indices", Offset=16, ChunkSize=0, Label="")]
public class _16_decal_indices
{
private int __StartOffset__;
}
public _24_sprite_vertices[] _24_Sprite_Vertices;
[Serializable][Reflexive(Name="Sprite Vertices", Offset=24, ChunkSize=0, Label="")]
public class _24_sprite_vertices
{
private int __StartOffset__;
}
public _32_sprite_indices[] _32_Sprite_Indices;
[Serializable][Reflexive(Name="Sprite Indices", Offset=32, ChunkSize=0, Label="")]
public class _32_sprite_indices
{
private int __StartOffset__;
}
}
}
public _24_groups[] _24_Groups;
[Serializable][Reflexive(Name="Groups", Offset=24, ChunkSize=24, Label="")]
public class _24_groups
{
private int __StartOffset__;
[Value(Name="Decorator Set", Offset=0)]
public Int16 _0_Decorator_Set;
[Value(Name="Decorator Type", Offset=2)]
public H2.DataTypes.Enum _2_Decorator_Type = new H2.DataTypes.Enum(new string[] {  "Model", "Floating Decal", "Projected Decal", "Screen Facing Quad", "Axis Rotating Quad", "Cross Quad" }, 4);
[Value(Name="Shader Index", Offset=6)]
public Int16 _6_Shader_Index;
[Value(Name="Compressed Radius", Offset=8)]
public Int16 _8_Compressed_Radius;
[Value(Name="Cluster", Offset=10)]
public Int16 _10_Cluster;
[Value(Name="Cache Block #", Offset=12)]
public Int16 _12_Cache_Block_No;
[Value(Name="Decorator Start Index", Offset=14)]
public Byte _14_Decorator_Start_Index;
[Value(Name="Decorator Count", Offset=15)]
public Byte _15_Decorator_Count;
[Value(Name="Vertex Start Offset", Offset=16)]
public Byte _16_Vertex_Start_Offset;
[Value(Name="Vertex Count", Offset=17)]
public Byte _17_Vertex_Count;
[Value(Name="Index Start Offset", Offset=18)]
public Byte _18_Index_Start_Offset;
[Value(Name="Index Count", Offset=19)]
public Byte _19_Index_Count;
[Value(Name="Compressed Bounding Center", Offset=20)]
public Single _20_Compressed_Bounding_Center;
}
public _32_cells[] _32_Cells;
[Serializable][Reflexive(Name="Cells", Offset=32, ChunkSize=24, Label="")]
public class _32_cells
{
private int __StartOffset__;
[Value(Name="Child Index", Offset=0)]
public Int16 _0_Child_Index;
[Value(Name="Child Index", Offset=2)]
public Int16 _2_Child_Index;
[Value(Name="Child Index", Offset=4)]
public Int16 _4_Child_Index;
[Value(Name="Child Index", Offset=6)]
public Int16 _6_Child_Index;
[Value(Name="Child Index", Offset=8)]
public Int16 _8_Child_Index;
[Value(Name="Child Index", Offset=10)]
public Int16 _10_Child_Index;
[Value(Name="Child Index", Offset=12)]
public Int16 _12_Child_Index;
[Value(Name="Child Index", Offset=14)]
public Int16 _14_Child_Index;
[Value(Name="Cache Block Index", Offset=16)]
public Int16 _16_Cache_Block_Index;
[Value(Name="Group Count", Offset=18)]
public Int16 _18_Group_Count;
[Value(Name="Group Start Index", Offset=20)]
public Int32 _20_Group_Start_Index;
}
public _40_decals[] _40_Decals;
[Serializable][Reflexive(Name="Decals", Offset=40, ChunkSize=68, Label="")]
public class _40_decals
{
private int __StartOffset__;
[Value(Name="Decorator Set", Offset=0)]
public Int16 _0_Decorator_Set;
[Value(Name="Decorator Class", Offset=2)]
public Int16 _2_Decorator_Class;
[Value(Name="Decorator Permutation", Offset=4)]
public Int16 _4_Decorator_Permutation;
[Value(Name="Sprite Index", Offset=6)]
public Int16 _6_Sprite_Index;
[Value(Name="Position X", Offset=8)]
public Single _8_Position_X;
[Value(Name="Position Y", Offset=12)]
public Single _12_Position_Y;
[Value(Name="Position Z", Offset=16)]
public Single _16_Position_Z;
[Value(Name="Left i", Offset=20)]
public Single _20_Left_i;
[Value(Name="Left j", Offset=24)]
public Single _24_Left_j;
[Value(Name="Left k", Offset=28)]
public Single _28_Left_k;
[Value(Name="Up i", Offset=32)]
public Single _32_Up_i;
[Value(Name="Up j", Offset=36)]
public Single _36_Up_j;
[Value(Name="Up k", Offset=40)]
public Single _40_Up_k;
[Value(Name="Extents i", Offset=44)]
public Single _44_Extents_i;
[Value(Name="Extents j", Offset=48)]
public Single _48_Extents_j;
[Value(Name="Extents k", Offset=52)]
public Single _52_Extents_k;
[Value(Name="Previous Position X", Offset=56)]
public Single _56_Previous_Position_X;
[Value(Name="Previous Position Y", Offset=60)]
public Single _60_Previous_Position_Y;
[Value(Name="Previous Position Z", Offset=64)]
public Single _64_Previous_Position_Z;
}
}
        }
    }
}