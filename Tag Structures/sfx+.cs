using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("sfx+")]
        public class sfx_
        {
public _0_unknown3[] _0_Unknown3;
[Serializable][Reflexive(Name="Unknown3", Offset=0, ChunkSize=56, Label="")]
public class _0_unknown3
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Offset=4)]
public Byte[] _4_ = new byte[8];
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Offset=16)]
public Byte[] _16_ = new byte[8];
public _24_unknown8[] _24_Unknown8;
[Serializable][Reflexive(Name="Unknown8", Offset=24, ChunkSize=72, Label="")]
public class _24_unknown8
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Offset=16)]
public Byte[] _16_ = new byte[8];
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Offset=32)]
public Byte[] _32_ = new byte[8];
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Offset=48)]
public Byte[] _48_ = new byte[8];
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Offset=64)]
public Byte[] _64_ = new byte[8];
}
public _32_unknown24[] _32_Unknown24;
[Serializable][Reflexive(Name="Unknown24", Offset=32, ChunkSize=48, Label="")]
public class _32_unknown24
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[16];
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Offset=40)]
public Byte[] _40_ = new byte[8];
}
public _40_unknown34[] _40_Unknown34;
[Serializable][Reflexive(Name="Unknown34", Offset=40, ChunkSize=64, Label="")]
public class _40_unknown34
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[16];
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Offset=40)]
public Byte[] _40_ = new byte[8];
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Offset=56)]
public Byte[] _56_ = new byte[8];
}
public _48_unknown47[] _48_Unknown47;
[Serializable][Reflexive(Name="Unknown47", Offset=48, ChunkSize=40, Label="")]
public class _48_unknown47
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Dependancy _0_Unknown;
public _8_unknown50[] _8_Unknown50;
[Serializable][Reflexive(Name="Unknown50", Offset=8, ChunkSize=16, Label="")]
public class _8_unknown50
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Dependancy _0_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[4];
}
[Value(Offset=16)]
public Byte[] _16_ = new byte[16];
public _32_unknown57[] _32_Unknown57;
[Serializable][Reflexive(Name="Unknown57", Offset=32, ChunkSize=20, Label="")]
public class _32_unknown57
{
private int __StartOffset__;
public _0_unknown58[] _0_Unknown58;
[Serializable][Reflexive(Name="Unknown58", Offset=0, ChunkSize=28, Label="")]
public class _0_unknown58
{
private int __StartOffset__;
public _0_unknown59[] _0_Unknown59;
[Serializable][Reflexive(Name="Unknown59", Offset=0, ChunkSize=16, Label="")]
public class _0_unknown59
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown61[] _4_Unknown61;
[Serializable][Reflexive(Name="Unknown61", Offset=4, ChunkSize=1, Label="")]
public class _4_unknown61
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Byte _0_Unknown;
}
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
public _8_unknown66[] _8_Unknown66;
[Serializable][Reflexive(Name="Unknown66", Offset=8, ChunkSize=4, Label="")]
public class _8_unknown66
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
}
public _16_unknown69[] _16_Unknown69;
[Serializable][Reflexive(Name="Unknown69", Offset=16, ChunkSize=4, Label="")]
public class _16_unknown69
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
}
public _8_unknown74[] _8_Unknown74;
[Serializable][Reflexive(Name="Unknown74", Offset=8, ChunkSize=16, Label="")]
public class _8_unknown74
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown76[] _4_Unknown76;
[Serializable][Reflexive(Name="Unknown76", Offset=4, ChunkSize=1, Label="")]
public class _4_unknown76
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Byte _0_Unknown;
}
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
}
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
}
}
}
        }
    }
}