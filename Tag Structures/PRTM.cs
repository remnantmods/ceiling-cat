using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("PRTM")]
        public class PRTM
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Spins","Random U Mirror","Random V Mirror","Frame Animation One Shot","Select Random Sequence","Disable Frame Blending","Can Animate Backwards","Receive Lightmap Lighting","Tint From Diffuse Texture","Dies At Rest","Dies On Structure Collision","Dies In Media","Dies In Air","Bitmap Authored Vertically","Has Sweetener" }, 4);
[Value(Name="Orientation", Offset=4)]
public H2.DataTypes.Enum _4_Orientation = new H2.DataTypes.Enum(new string[] {  "Screen Facing", "Parallel To Direction", "Perpendicular To Direction", "Vertical", "Horizontal" }, 4);
[Value(Offset=8)]
public Byte[] _8_ = new byte[16];
[Value(Name="Shader", Offset=24)]
public Dependancy _24_Shader;
[Value(Name="Output Modifier", Offset=28)]
public H2.DataTypes.Enum _28_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=32)]
public H2.DataTypes.Enum _32_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _36_scale_x[] _36_Scale_X;
[Serializable][Reflexive(Name="Scale X", Offset=36, ChunkSize=1, Label="")]
public class _36_scale_x
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=44)]
public H2.DataTypes.Enum _44_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=48)]
public H2.DataTypes.Enum _48_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _52_scale_y[] _52_Scale_Y;
[Serializable][Reflexive(Name="Scale Y", Offset=52, ChunkSize=1, Label="")]
public class _52_scale_y
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=60)]
public H2.DataTypes.Enum _60_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=64)]
public H2.DataTypes.Enum _64_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _68_scale_z[] _68_Scale_Z;
[Serializable][Reflexive(Name="Scale Z", Offset=68, ChunkSize=1, Label="")]
public class _68_scale_z
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=76)]
public H2.DataTypes.Enum _76_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=80)]
public H2.DataTypes.Enum _80_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _84_rotation[] _84_Rotation;
[Serializable][Reflexive(Name="Rotation", Offset=84, ChunkSize=1, Label="")]
public class _84_rotation
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Collision Effect", Offset=92)]
public Dependancy _92_Collision_Effect;
[Value(Name="Death Effect", Offset=96)]
public Dependancy _96_Death_Effect;
public _100_locations[] _100_Locations;
[Serializable][Reflexive(Name="Locations", Offset=100, ChunkSize=4, Label="")]
public class _100_locations
{
private int __StartOffset__;
[Value(Name="Marker Name", Offset=0)]
public StringID _0_Marker_Name;
}
public _108_attached_particle_system[] _108_Attached_Particle_System;
[Serializable][Reflexive(Name="Attached Particle System", Offset=108, ChunkSize=56, Label="")]
public class _108_attached_particle_system
{
private int __StartOffset__;
[Value(Name="Particle", Offset=0)]
public Dependancy _0_Particle;
[Value(Offset=8)]
public Byte[] _8_ = new byte[8];
[Value(Name="Location #", Offset=16)]
public Int16 _16_Location_No;
[Value(Name="Coordinate System", Offset=18)]
public H2.DataTypes.Enum _18_Coordinate_System = new H2.DataTypes.Enum(new string[] {  "World", "Local", "Parent" }, 2);
[Value(Name="Environment", Offset=20)]
public H2.DataTypes.Enum _20_Environment = new H2.DataTypes.Enum(new string[] {  "Any", "Air Only", "Water Only", "Space Only" }, 2);
[Value(Name="Disposition", Offset=22)]
public H2.DataTypes.Enum _22_Disposition = new H2.DataTypes.Enum(new string[] {  "Either Mode", "Violent Mode", "Non-Violent Mode" }, 2);
[Value(Name="Camera Mode", Offset=24)]
public H2.DataTypes.Enum _24_Camera_Mode = new H2.DataTypes.Enum(new string[] {  "Independent Of Camera Mode", "Only In First Person", "Only In Third Person", "Both First And Third" }, 2);
[Value(Name="Sort Bias", Offset=26)]
public Int16 _26_Sort_Bias;
[Value(Name="Flags", Offset=28)]
public Bitmask _28_Flags = new Bitmask(new string[] { "Glow","Cinematics","Loop Particle","Disabled For Debugging","Inherit Effect Velocity","Render When Zoomed","Spread Between Ticks","persistent Particle","Expensive Visibility" }, 4);
[Value(Name="LOD In Distance", Offset=32)]
public Single _32_LOD_In_Distance;
[Value(Name="LOD Feather In Delta", Offset=36)]
public Single _36_LOD_Feather_In_Delta;
[Value(Name="LOD Out Distance", Offset=40)]
public Single _40_LOD_Out_Distance;
[Value(Name="LOD Feather Out Delta", Offset=44)]
public Single _44_LOD_Feather_Out_Delta;
public _48_emitters[] _48_Emitters;
[Serializable][Reflexive(Name="Emitters", Offset=48, ChunkSize=184, Label="")]
public class _48_emitters
{
private int __StartOffset__;
[Value(Name="Particle Physics", Offset=0)]
public Dependancy _0_Particle_Physics;
[Value(Name="Output Modifier", Offset=8)]
public H2.DataTypes.Enum _8_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=12)]
public H2.DataTypes.Enum _12_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _16_particle_emission_rate[] _16_Particle_Emission_Rate;
[Serializable][Reflexive(Name="Particle Emission Rate", Offset=16, ChunkSize=1, Label="")]
public class _16_particle_emission_rate
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=24)]
public H2.DataTypes.Enum _24_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=28)]
public H2.DataTypes.Enum _28_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _32_particle_lifespan__sec_[] _32_Particle_Lifespan__sec_;
[Serializable][Reflexive(Name="Particle Lifespan (sec)", Offset=32, ChunkSize=1, Label="")]
public class _32_particle_lifespan__sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=40)]
public H2.DataTypes.Enum _40_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=44)]
public H2.DataTypes.Enum _44_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _48_particle_velocity__world_units_per_sec_[] _48_Particle_Velocity__World_Units_Per_Sec_;
[Serializable][Reflexive(Name="Particle Velocity (World Units Per Sec)", Offset=48, ChunkSize=1, Label="")]
public class _48_particle_velocity__world_units_per_sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=56)]
public H2.DataTypes.Enum _56_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=60)]
public H2.DataTypes.Enum _60_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _64_particle_angular_velocity__degrees_per_sec_[] _64_Particle_Angular_Velocity__Degrees_Per_Sec_;
[Serializable][Reflexive(Name="Particle Angular Velocity (Degrees Per Sec)", Offset=64, ChunkSize=1, Label="")]
public class _64_particle_angular_velocity__degrees_per_sec_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=72)]
public H2.DataTypes.Enum _72_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=76)]
public H2.DataTypes.Enum _76_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _80_particle_size__world_units_[] _80_Particle_Size__World_Units_;
[Serializable][Reflexive(Name="Particle Size (World Units)", Offset=80, ChunkSize=1, Label="")]
public class _80_particle_size__world_units_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=88)]
public H2.DataTypes.Enum _88_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=92)]
public H2.DataTypes.Enum _92_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _96_particle_tint[] _96_Particle_Tint;
[Serializable][Reflexive(Name="Particle Tint", Offset=96, ChunkSize=1, Label="")]
public class _96_particle_tint
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=104)]
public H2.DataTypes.Enum _104_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=108)]
public H2.DataTypes.Enum _108_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _112_particle_alpha[] _112_Particle_Alpha;
[Serializable][Reflexive(Name="Particle Alpha", Offset=112, ChunkSize=1, Label="")]
public class _112_particle_alpha
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Emission Shape", Offset=120)]
public H2.DataTypes.Enum _120_Emission_Shape = new H2.DataTypes.Enum(new string[] {  "Sprayer", "Disc", "Globe", "Implode", "Tube", "Halo", "Impact Contour", "Impact Area", "Debris", "Line" }, 4);
[Value(Name="Output Modifier", Offset=124)]
public H2.DataTypes.Enum _124_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=128)]
public H2.DataTypes.Enum _128_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _132_emission_radius__world_units_[] _132_Emission_Radius__World_Units_;
[Serializable][Reflexive(Name="Emission Radius (World Units)", Offset=132, ChunkSize=1, Label="")]
public class _132_emission_radius__world_units_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Output Modifier", Offset=140)]
public H2.DataTypes.Enum _140_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "", "Plus", "Times" }, 4);
[Value(Name="Output Modifier", Offset=144)]
public H2.DataTypes.Enum _144_Output_Modifier = new H2.DataTypes.Enum(new string[] {  "Particle Age", "Particle Emit Time", "Particle Random 1", "Particle Random 2", "Emitter Age", "Emitter Random 1", "Emitter Random 2", "System LOD", "Game Time", "Effect A Scale", "Effect B Scale", "Particle Rotation", "Explosion Animation", "Explosion Rotation", "Particle Random 3", "Particle Random 4", "Location Random" }, 4);
public _148_emission_angle__degrees_[] _148_Emission_Angle__Degrees_;
[Serializable][Reflexive(Name="Emission Angle (Degrees)", Offset=148, ChunkSize=1, Label="")]
public class _148_emission_angle__degrees_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Translation Offset X", Offset=156)]
public Single _156_Translation_Offset_X;
[Value(Name="Translation Offset Y", Offset=160)]
public Single _160_Translation_Offset_Y;
[Value(Name="Translation Offset Z", Offset=164)]
public Single _164_Translation_Offset_Z;
[Value(Name="Relative Direction Yaw", Offset=168)]
public Single _168_Relative_Direction_Yaw;
[Value(Name="Relative Direction Pitch", Offset=172)]
public Single _172_Relative_Direction_Pitch;
[Value(Offset=176)]
public Byte[] _176_ = new byte[8];
}
}
public _116_models[] _116_Models;
[Serializable][Reflexive(Name="Models", Offset=116, ChunkSize=8, Label="")]
public class _116_models
{
private int __StartOffset__;
[Value(Name="Model Name", Offset=0)]
public StringID _0_Model_Name;
[Value(Name="Index Start", Offset=4)]
public Int16 _4_Index_Start;
[Value(Name="Index Count", Offset=6)]
public Int16 _6_Index_Count;
}
public _124_raw_vertices[] _124_Raw_Vertices;
[Serializable][Reflexive(Name="Raw Vertices", Offset=124, ChunkSize=56, Label="")]
public class _124_raw_vertices
{
private int __StartOffset__;
[Value(Name="Pos X", Offset=0)]
public Single _0_Pos_X;
[Value(Name="Pos Y", Offset=4)]
public Single _4_Pos_Y;
[Value(Name="Pos Z", Offset=8)]
public Single _8_Pos_Z;
[Value(Name="Normal i", Offset=12)]
public Single _12_Normal_i;
[Value(Name="Normal j", Offset=16)]
public Single _16_Normal_j;
[Value(Name="Normal k", Offset=20)]
public Single _20_Normal_k;
[Value(Name="Tangent i", Offset=24)]
public Single _24_Tangent_i;
[Value(Name="Tangent j", Offset=28)]
public Single _28_Tangent_j;
[Value(Name="Tangent k", Offset=32)]
public Single _32_Tangent_k;
[Value(Name="Binormal i", Offset=36)]
public Single _36_Binormal_i;
[Value(Name="Binormal j", Offset=40)]
public Single _40_Binormal_j;
[Value(Name="Binormal k", Offset=44)]
public Single _44_Binormal_k;
[Value(Name="Texture Coord X", Offset=48)]
public Single _48_Texture_Coord_X;
[Value(Name="Texture Coord Y", Offset=52)]
public Single _52_Texture_Coord_Y;
}
public _132_indices[] _132_Indices;
[Serializable][Reflexive(Name="Indices", Offset=132, ChunkSize=2, Label="")]
public class _132_indices
{
private int __StartOffset__;
[Value(Name="Index", Offset=0)]
public Int16 _0_Index;
}
public _140_cached_data[] _140_Cached_Data;
[Serializable][Reflexive(Name="Cached Data", Offset=140, ChunkSize=0, Label="")]
public class _140_cached_data
{
private int __StartOffset__;
}
[Value(Name="Raw Offset", Offset=148)]
public UInt32 _148_Raw_Offset;
[Value(Name="Raw Size", Offset=152)]
public UInt32 _152_Raw_Size;
[Value(Name="Raw Header Size", Offset=156)]
public UInt32 _156_Raw_Header_Size;
[Value(Name="Raw Data Size", Offset=160)]
public UInt32 _160_Raw_Data_Size;
public _164_resources[] _164_Resources;
[Serializable][Reflexive(Name="Resources", Offset=164, ChunkSize=16, Label="")]
public class _164_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Primary Location", Offset=4)]
public Int16 _4_Primary_Location;
[Value(Name="Secondary Location", Offset=6)]
public Int16 _6_Secondary_Location;
[Value(Name="Raw Data Size", Offset=8)]
public UInt32 _8_Raw_Data_Size;
[Value(Name="Raw Data Offset", Offset=12)]
public UInt32 _12_Raw_Data_Offset;
}
[Value(Offset=172)]
public Byte[] _172_ = new byte[32];
[Value(Name="Owner Tag Section Offset", Offset=204)]
public Int16 _204_Owner_Tag_Section_Offset;
[Value(Offset=206)]
public Byte[] _206_ = new byte[2];
        }
    }
}