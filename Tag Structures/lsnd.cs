using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("lsnd")]
        public class lsnd
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Deafening To AIs","Not A Loop","Stops Music","Always Spartialize","Synchronize Playback","Synchronize Tracks","Fake Spatialization With Distance","Combine All 3D Playback" }, 4);
[Value(Name="Marty's Music Time", Offset=4)]
public Single _4_Marty_s_Music_Time;
[Value(Offset=8)]
public Byte[] _8_ = new byte[20];
public _28_tracks[] _28_Tracks;
[Serializable][Reflexive(Name="Tracks", Offset=28, ChunkSize=88, Label="")]
public class _28_tracks
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Fade In At Start","Fade Out At Stop","Crossfade Alternate Loop","Master Surround Sound Track","Fade Out At Alternate Stop" }, 4);
[Value(Name="Gain dB", Offset=8)]
public Single _8_Gain_dB;
[Value(Name="Fade In Duration Sec", Offset=12)]
public Single _12_Fade_In_Duration_Sec;
[Value(Name="Fade Out Duration Sec", Offset=16)]
public Single _16_Fade_Out_Duration_Sec;
[Value(Name="In", Offset=20)]
public Dependancy _20_In;
[Value(Name="Loop", Offset=28)]
public Dependancy _28_Loop;
[Value(Name="Out", Offset=36)]
public Dependancy _36_Out;
[Value(Name="Alternate Loop", Offset=44)]
public Dependancy _44_Alternate_Loop;
[Value(Name="Alternate Out", Offset=52)]
public Dependancy _52_Alternate_Out;
[Value(Name="Output Effect", Offset=60)]
public H2.DataTypes.Enum _60_Output_Effect = new H2.DataTypes.Enum(new string[] {  "None", "Output Front Speakers", "Output Rear Speakers", "Output Center Speakers" }, 4);
[Value(Name="Alternate Trans In", Offset=64)]
public Dependancy _64_Alternate_Trans_In;
[Value(Name="Alternate Trans Out", Offset=72)]
public Dependancy _72_Alternate_Trans_Out;
[Value(Name="Alternate Crossfade", Offset=80)]
public Single _80_Alternate_Crossfade;
[Value(Name="Alternate Fade Out Duration", Offset=84)]
public Single _84_Alternate_Fade_Out_Duration;
}
public _36_detail_sounds[] _36_Detail_Sounds;
[Serializable][Reflexive(Name="Detail Sounds", Offset=36, ChunkSize=52, Label="")]
public class _36_detail_sounds
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Sound", Offset=4)]
public Dependancy _4_Sound;
[Value(Name="Random Period Bounds Lower", Offset=12)]
public Single _12_Random_Period_Bounds_Lower;
[Value(Name="Random Period Bounds Upper", Offset=16)]
public Single _16_Random_Period_Bounds_Upper;
[Value(Name="Flags", Offset=20)]
public Bitmask _20_Flags = new Bitmask(new string[] { "Don't Play With Alternate","Don't Play Without Alternate","Start Immeditely With Loop" }, 4);
[Value(Offset=24)]
public Byte[] _24_ = new byte[4];
[Value(Name="Yaw Bounds Lower", Offset=28)]
public Single _28_Yaw_Bounds_Lower;
[Value(Name="Yaw Bounds Upper", Offset=32)]
public Single _32_Yaw_Bounds_Upper;
[Value(Name="Pitch Bounds Lower", Offset=36)]
public Single _36_Pitch_Bounds_Lower;
[Value(Name="Pitch Bounds Upper", Offset=40)]
public Single _40_Pitch_Bounds_Upper;
[Value(Name="Distance Bounds Lower", Offset=44)]
public Single _44_Distance_Bounds_Lower;
[Value(Name="Distance Bounds Upper", Offset=48)]
public Single _48_Distance_Bounds_Upper;
}
        }
    }
}