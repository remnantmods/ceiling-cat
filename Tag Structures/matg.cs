using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable]
        [Tag("matg")]
        public class matg
        {
            [Value(Offset = 0)]
            public Byte[] _0_ = new byte[172];
            [Value(Name = "Language", Offset = 172)]
            public H2.DataTypes.Enum _172_Language = new H2.DataTypes.Enum(new string[] { }, 4);
            public _176_havok_cleanup_resources[] _176_Havok_Cleanup_Resources;
            [Serializable]
            [Reflexive(Name = "Havok Cleanup Resources", Offset = 176, ChunkSize = 8, Label = "")]
            public class _176_havok_cleanup_resources
            {
                private int __StartOffset__;
                [Value(Name = "Object Cleanup Effect", Offset = 0)]
                public Dependancy _0_Object_Cleanup_Effect;
            }
            public _184_collision_damage[] _184_Collision_Damage;
            [Serializable]
            [Reflexive(Name = "Collision Damage", Offset = 184, ChunkSize = 72, Label = "")]
            public class _184_collision_damage
            {
                private int __StartOffset__;
                [Value(Name = "Collision Damage", Offset = 0)]
                public Dependancy _0_Collision_Damage;
                [Value(Name = "Min Game Acc (Default)", Offset = 8)]
                public Single _8_Min_Game_Acc__Default_;
                [Value(Name = "Max Game Acc (Default)", Offset = 12)]
                public Single _12_Max_Game_Acc__Default_;
                [Value(Name = "Min Gam Scale (Default)", Offset = 16)]
                public Single _16_Min_Gam_Scale__Default_;
                [Value(Name = "Max Gam Scale (Default)", Offset = 20)]
                public Single _20_Max_Gam_Scale__Default_;
                [Value(Name = "Min abs Acc (Default)", Offset = 24)]
                public Single _24_Min_abs_Acc__Default_;
                [Value(Name = "Max abs Acc (Default)", Offset = 28)]
                public Single _28_Max_abs_Acc__Default_;
                [Value(Name = "Min abs Scale (Default)", Offset = 32)]
                public Single _32_Min_abs_Scale__Default_;
                [Value(Name = "Max abs Scale (Default)", Offset = 36)]
                public Single _36_Max_abs_Scale__Default_;
                [Value(Offset = 40)]
                public Byte[] _40_ = new byte[32];
            }
            public _192_sound_info[] _192_Sound_Info;
            [Serializable]
            [Reflexive(Name = "Sound Info", Offset = 192, ChunkSize = 32, Label = "")]
            public class _192_sound_info
            {
                private int __StartOffset__;
                [Value(Name = "Sound Class", Offset = 0)]
                public Dependancy _0_Sound_Class;
                [Value(Name = "Sound FX", Offset = 8)]
                public Dependancy _8_Sound_FX;
                [Value(Name = "Sound Mix", Offset = 16)]
                public Dependancy _16_Sound_Mix;
                [Value(Name = "Sound Combat Dialogue", Offset = 24)]
                public Dependancy _24_Sound_Combat_Dialogue;
            }
            public _200_ai_information[] _200_AI_Information;
            [Serializable]
            [Reflexive(Name = "AI Information", Offset = 200, ChunkSize = 360, Label = "")]
            public class _200_ai_information
            {
                private int __StartOffset__;
                [Value(Name = "Danger Broadly Facing", Offset = 0)]
                public Single _0_Danger_Broadly_Facing;
                [Value(Offset = 4)]
                public Byte[] _4_ = new byte[4];
                [Value(Name = "Danger Shooting Near", Offset = 8)]
                public Single _8_Danger_Shooting_Near;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[4];
                [Value(Name = "Danger Shooting At", Offset = 16)]
                public Single _16_Danger_Shooting_At;
                [Value(Offset = 20)]
                public Byte[] _20_ = new byte[4];
                [Value(Name = "Danger Extremely Close", Offset = 24)]
                public Single _24_Danger_Extremely_Close;
                [Value(Offset = 28)]
                public Byte[] _28_ = new byte[4];
                [Value(Name = "Danger Sheild Damage", Offset = 32)]
                public Single _32_Danger_Sheild_Damage;
                [Value(Name = "Danger Extended Shield Damage", Offset = 36)]
                public Single _36_Danger_Extended_Shield_Damage;
                [Value(Name = "Danger Body Damage", Offset = 40)]
                public Single _40_Danger_Body_Damage;
                [Value(Name = "Danger Extended Body Damage", Offset = 44)]
                public Single _44_Danger_Extended_Body_Damage;
                [Value(Offset = 48)]
                public Byte[] _48_ = new byte[48];
                [Value(Name = "AI Dialog", Offset = 96)]
                public Dependancy _96_AI_Dialog;
                [Value(Offset = 104)]
                public Byte[] _104_ = new byte[24];
                [Value(Name = "Jump Down", Offset = 128)]
                public Single _128_Jump_Down;
                [Value(Name = "Jump Step", Offset = 132)]
                public Single _132_Jump_Step;
                [Value(Name = "Jump Crouch", Offset = 136)]
                public Single _136_Jump_Crouch;
                [Value(Name = "Jump Stand", Offset = 140)]
                public Single _140_Jump_Stand;
                [Value(Name = "Jump Storey", Offset = 144)]
                public Single _144_Jump_Storey;
                [Value(Name = "Jump Tower", Offset = 148)]
                public Single _148_Jump_Tower;
                [Value(Name = "Max Jump Down Height Down", Offset = 152)]
                public Single _152_Max_Jump_Down_Height_Down;
                [Value(Name = "Max Jump Down Height Step", Offset = 156)]
                public Single _156_Max_Jump_Down_Height_Step;
                [Value(Name = "Max Jump Down Height Crouch", Offset = 160)]
                public Single _160_Max_Jump_Down_Height_Crouch;
                [Value(Name = "Max Jump Down Height Stand", Offset = 164)]
                public Single _164_Max_Jump_Down_Height_Stand;
                [Value(Name = "Max Jump Down Height Storey", Offset = 168)]
                public Single _168_Max_Jump_Down_Height_Storey;
                [Value(Name = "Max Jump Down Height Tower", Offset = 172)]
                public Single _172_Max_Jump_Down_Height_Tower;
                [Value(Name = "Hoist Step", Offset = 176)]
                public Single _176_Hoist_Step;
                [Value(Name = "...To", Offset = 180)]
                public Single _180____To;
                [Value(Name = "Hoist Crouch", Offset = 184)]
                public Single _184_Hoist_Crouch;
                [Value(Name = "...To", Offset = 188)]
                public Single _188____To;
                [Value(Name = "Hoist Stand", Offset = 192)]
                public Single _192_Hoist_Stand;
                [Value(Name = "...To", Offset = 196)]
                public Single _196____To;
                [Value(Offset = 200)]
                public Byte[] _200_ = new byte[24];
                [Value(Name = "Vault Step", Offset = 224)]
                public Single _224_Vault_Step;
                [Value(Name = "...To", Offset = 228)]
                public Single _228____To;
                [Value(Name = "Vault Crouch", Offset = 232)]
                public Single _232_Vault_Crouch;
                [Value(Name = "...To", Offset = 236)]
                public Single _236____To;
                [Value(Offset = 240)]
                public Byte[] _240_ = new byte[48];
                public _288_gravemind_properties[] _288_Gravemind_Properties;
                [Serializable]
                [Reflexive(Name = "Gravemind Properties", Offset = 288, ChunkSize = 12, Label = "")]
                public class _288_gravemind_properties
                {
                    private int __StartOffset__;
                    [Value(Name = "Min Retreat Time", Offset = 0)]
                    public Single _0_Min_Retreat_Time;
                    [Value(Name = "Ideal Retreat Time", Offset = 4)]
                    public Single _4_Ideal_Retreat_Time;
                    [Value(Name = "Max Retreat Time", Offset = 8)]
                    public Single _8_Max_Retreat_Time;
                }
                [Value(Offset = 296)]
                public Byte[] _296_ = new byte[48];
                [Value(Name = "Scary Target Threhold", Offset = 344)]
                public Single _344_Scary_Target_Threhold;
                [Value(Name = "Scary Weapon Threhold", Offset = 348)]
                public Single _348_Scary_Weapon_Threhold;
                [Value(Name = "Player Scariness", Offset = 352)]
                public Single _352_Player_Scariness;
                [Value(Name = "Berserking Actor Scariness", Offset = 356)]
                public Single _356_Berserking_Actor_Scariness;
            }
            public _208_damage_table[] _208_Damage_Table;
            [Serializable]
            [Reflexive(Name = "Damage Table", Offset = 208, ChunkSize = 8, Label = "")]
            public class _208_damage_table
            {
                private int __StartOffset__;
                public _0_damage_groups[] _0_Damage_Groups;
                [Serializable]
                [Reflexive(Name = "Damage Groups", Offset = 0, ChunkSize = 12, Label = "")]
                public class _0_damage_groups
                {
                    private int __StartOffset__;
                    [Value(Name = "Name", Offset = 0)]
                    public StringID _0_Name;
                    public _4_armor_modifiers[] _4_Armor_Modifiers;
                    [Serializable]
                    [Reflexive(Name = "Armor Modifiers", Offset = 4, ChunkSize = 8, Label = "")]
                    public class _4_armor_modifiers
                    {
                        private int __StartOffset__;
                        [Value(Name = "Name", Offset = 0)]
                        public StringID _0_Name;
                        [Value(Name = "Damage Multiplier", Offset = 4)]
                        public Single _4_Damage_Multiplier;
                    }
                }
            }
            [Value(Offset = 216)]
            public Byte[] _216_ = new byte[8];
            public _224_sounds__obsolete_[] _224_Sounds__Obsolete_;
            [Serializable]
            [Reflexive(Name = "Sounds (Obsolete)", Offset = 224, ChunkSize = 8, Label = "")]
            public class _224_sounds__obsolete_
            {
                private int __StartOffset__;
                [Value(Name = "Sound", Offset = 0)]
                public Dependancy _0_Sound;
            }
            public _232_camera[] _232_Camera;
            [Serializable]
            [Reflexive(Name = "Camera", Offset = 232, ChunkSize = 20, Label = "")]
            public class _232_camera
            {
                private int __StartOffset__;
                [Value(Name = "Default Unit Camera Track", Offset = 0)]
                public Dependancy _0_Default_Unit_Camera_Track;
                [Value(Name = "Default Change Pause", Offset = 8)]
                public Single _8_Default_Change_Pause;
                [Value(Name = "1st Person Change Pause", Offset = 12)]
                public Single _12_1st_Person_Change_Pause;
                [Value(Name = "Following Camera Change Pause", Offset = 16)]
                public Single _16_Following_Camera_Change_Pause;
            }
            public _240_player_control[] _240_Player_Control;
            [Serializable]
            [Reflexive(Name = "Player Control", Offset = 240, ChunkSize = 128, Label = "")]
            public class _240_player_control
            {
                private int __StartOffset__;
                [Value(Name = "Magnetism Friction", Offset = 0)]
                public Single _0_Magnetism_Friction;
                [Value(Name = "Magnetism Adhesion", Offset = 4)]
                public Single _4_Magnetism_Adhesion;
                [Value(Name = "Inconsequential Target Scale", Offset = 8)]
                public Single _8_Inconsequential_Target_Scale;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[12];
                [Value(Name = "Crosshair Location X", Offset = 24)]
                public Single _24_Crosshair_Location_X;
                [Value(Name = "Crosshair Location Y", Offset = 28)]
                public Single _28_Crosshair_Location_Y;
                [Value(Name = "Seconds To Start", Offset = 32)]
                public Single _32_Seconds_To_Start;
                [Value(Name = "Seconds To Full Speed", Offset = 36)]
                public Single _36_Seconds_To_Full_Speed;
                [Value(Name = "Decay Rate", Offset = 40)]
                public Single _40_Decay_Rate;
                [Value(Name = "Full Speed Multiplier", Offset = 44)]
                public Single _44_Full_Speed_Multiplier;
                [Value(Name = "Pegged Magnitude", Offset = 48)]
                public Single _48_Pegged_Magnitude;
                [Value(Name = "Pegged Angular Threshold", Offset = 52)]
                public Single _52_Pegged_Angular_Threshold;
                [Value(Offset = 56)]
                public Byte[] _56_ = new byte[8];
                [Value(Name = "Look Default Pitch Rate", Offset = 64)]
                public Single _64_Look_Default_Pitch_Rate;
                [Value(Name = "Look Default Yaw Rate", Offset = 68)]
                public Single _68_Look_Default_Yaw_Rate;
                [Value(Name = "Look Peg Threshold", Offset = 72)]
                public Single _72_Look_Peg_Threshold;
                [Value(Name = "Look Yaw Acceleration Time", Offset = 76)]
                public Single _76_Look_Yaw_Acceleration_Time;
                [Value(Name = "Look Yaw Acceleration Scale", Offset = 80)]
                public Single _80_Look_Yaw_Acceleration_Scale;
                [Value(Name = "Look Pitch Acceleration Time", Offset = 84)]
                public Single _84_Look_Pitch_Acceleration_Time;
                [Value(Name = "Look Pitch Acceleration Scale", Offset = 88)]
                public Single _88_Look_Pitch_Acceleration_Scale;
                [Value(Name = "Look Auto-leveling Scale", Offset = 92)]
                public Single _92_Look_Auto_leveling_Scale;
                [Value(Offset = 96)]
                public Byte[] _96_ = new byte[12];
                [Value(Name = "Min Weapon Swap Ticks", Offset = 108)]
                public Int16 _108_Min_Weapon_Swap_Ticks;
                [Value(Name = "Min Autoleveling Ticks", Offset = 110)]
                public Int16 _110_Min_Autoleveling_Ticks;
                [Value(Name = "Gravity Scale", Offset = 112)]
                public Single _112_Gravity_Scale;
                public _116_look_function[] _116_Look_Function;
                [Serializable]
                [Reflexive(Name = "Look Function", Offset = 116, ChunkSize = 4, Label = "")]
                public class _116_look_function
                {
                    private int __StartOffset__;
                    [Value(Name = "Scale", Offset = 0)]
                    public Single _0_Scale;
                }
                [Value(Name = "Minimmum Action Hold Time", Offset = 124)]
                public Single _124_Minimmum_Action_Hold_Time;
            }
            public _248_difficulty[] _248_Difficulty;
            [Serializable]
            [Reflexive(Name = "Difficulty", Offset = 248, ChunkSize = 644, Label = "")]
            public class _248_difficulty
            {
                private int __StartOffset__;
                [Value(Name = "Easy Enemy Damage", Offset = 0)]
                public Single _0_Easy_Enemy_Damage;
                [Value(Name = "Normal Enemy Damage", Offset = 4)]
                public Single _4_Normal_Enemy_Damage;
                [Value(Name = "Heroic Enemy Damage", Offset = 8)]
                public Single _8_Heroic_Enemy_Damage;
                [Value(Name = "Legendary Enemy Damage", Offset = 12)]
                public Single _12_Legendary_Enemy_Damage;
                [Value(Name = "Easy Enemy Vitality", Offset = 16)]
                public Single _16_Easy_Enemy_Vitality;
                [Value(Name = "Normal Enemy Vitality", Offset = 20)]
                public Single _20_Normal_Enemy_Vitality;
                [Value(Name = "Heroic Enemy Vitality", Offset = 24)]
                public Single _24_Heroic_Enemy_Vitality;
                [Value(Name = "Legendary Enemy Vitality", Offset = 28)]
                public Single _28_Legendary_Enemy_Vitality;
                [Value(Name = "Easy Enemy Sheild", Offset = 32)]
                public Single _32_Easy_Enemy_Sheild;
                [Value(Name = "Normal Enemy Sheild", Offset = 36)]
                public Single _36_Normal_Enemy_Sheild;
                [Value(Name = "Heroic Enemy Sheild", Offset = 40)]
                public Single _40_Heroic_Enemy_Sheild;
                [Value(Name = "Legendary Enemy Sheild", Offset = 44)]
                public Single _44_Legendary_Enemy_Sheild;
                [Value(Name = "Easy Enemy Recharge", Offset = 48)]
                public Single _48_Easy_Enemy_Recharge;
                [Value(Name = "Normal Enemy Recharge", Offset = 52)]
                public Single _52_Normal_Enemy_Recharge;
                [Value(Name = "Heroic Enemy Recharge", Offset = 56)]
                public Single _56_Heroic_Enemy_Recharge;
                [Value(Name = "Legendary Enemy Recharge", Offset = 60)]
                public Single _60_Legendary_Enemy_Recharge;
                [Value(Name = "Easy Friend Damage", Offset = 64)]
                public Single _64_Easy_Friend_Damage;
                [Value(Name = "Normal Friend Damage", Offset = 68)]
                public Single _68_Normal_Friend_Damage;
                [Value(Name = "Heroic Friend Damage", Offset = 72)]
                public Single _72_Heroic_Friend_Damage;
                [Value(Name = "Legendary Friend Damage", Offset = 76)]
                public Single _76_Legendary_Friend_Damage;
                [Value(Name = "Easy Friend Vitality", Offset = 80)]
                public Single _80_Easy_Friend_Vitality;
                [Value(Name = "Normal Friend Vitality", Offset = 84)]
                public Single _84_Normal_Friend_Vitality;
                [Value(Name = "Heroic Friend Vitality", Offset = 88)]
                public Single _88_Heroic_Friend_Vitality;
                [Value(Name = "Legendary Friend Vitality", Offset = 92)]
                public Single _92_Legendary_Friend_Vitality;
                [Value(Name = "Easy Friend Sheild", Offset = 96)]
                public Single _96_Easy_Friend_Sheild;
                [Value(Name = "Normal Friend Sheild", Offset = 100)]
                public Single _100_Normal_Friend_Sheild;
                [Value(Name = "Heroic Friend Sheild", Offset = 104)]
                public Single _104_Heroic_Friend_Sheild;
                [Value(Name = "Legendary Friend Sheild", Offset = 108)]
                public Single _108_Legendary_Friend_Sheild;
                [Value(Name = "Easy Friend Recharge", Offset = 112)]
                public Single _112_Easy_Friend_Recharge;
                [Value(Name = "Normal Friend Recharge", Offset = 116)]
                public Single _116_Normal_Friend_Recharge;
                [Value(Name = "Heroic Friend Recharge", Offset = 120)]
                public Single _120_Heroic_Friend_Recharge;
                [Value(Name = "Legendary Friend Recharge", Offset = 124)]
                public Single _124_Legendary_Friend_Recharge;
                [Value(Name = "Easy Infection Forms", Offset = 128)]
                public Single _128_Easy_Infection_Forms;
                [Value(Name = "Normal Infection Forms", Offset = 132)]
                public Single _132_Normal_Infection_Forms;
                [Value(Name = "Heroic Infection Forms", Offset = 136)]
                public Single _136_Heroic_Infection_Forms;
                [Value(Name = "Legendary Infection Forms", Offset = 140)]
                public Single _140_Legendary_Infection_Forms;
                [Value(Offset = 144)]
                public Byte[] _144_ = new byte[16];
                [Value(Name = "Easy Rate Of Fire", Offset = 160)]
                public Single _160_Easy_Rate_Of_Fire;
                [Value(Name = "Normal Rate Of Fire", Offset = 164)]
                public Single _164_Normal_Rate_Of_Fire;
                [Value(Name = "Heroic Rate Of Fire", Offset = 168)]
                public Single _168_Heroic_Rate_Of_Fire;
                [Value(Name = "Legendary Rate Of Fire", Offset = 172)]
                public Single _172_Legendary_Rate_Of_Fire;
                [Value(Name = "Easy Projectile Error", Offset = 176)]
                public Single _176_Easy_Projectile_Error;
                [Value(Name = "Normal Projectile Error", Offset = 180)]
                public Single _180_Normal_Projectile_Error;
                [Value(Name = "Heroic Projectile Error", Offset = 184)]
                public Single _184_Heroic_Projectile_Error;
                [Value(Name = "Legendary Projectile Error", Offset = 188)]
                public Single _188_Legendary_Projectile_Error;
                [Value(Name = "Easy Burst Error", Offset = 192)]
                public Single _192_Easy_Burst_Error;
                [Value(Name = "Normal Burst Error", Offset = 196)]
                public Single _196_Normal_Burst_Error;
                [Value(Name = "Heroic Burst Error", Offset = 200)]
                public Single _200_Heroic_Burst_Error;
                [Value(Name = "Legendary Burst Error", Offset = 204)]
                public Single _204_Legendary_Burst_Error;
                [Value(Name = "Easy New Target Delay", Offset = 208)]
                public Single _208_Easy_New_Target_Delay;
                [Value(Name = "Normal New Target Delay", Offset = 212)]
                public Single _212_Normal_New_Target_Delay;
                [Value(Name = "Heroic New Target Delay", Offset = 216)]
                public Single _216_Heroic_New_Target_Delay;
                [Value(Name = "Legendary New Target Delay", Offset = 220)]
                public Single _220_Legendary_New_Target_Delay;
                [Value(Name = "Easy Burst Separation", Offset = 224)]
                public Single _224_Easy_Burst_Separation;
                [Value(Name = "Normal Burst Separation", Offset = 228)]
                public Single _228_Normal_Burst_Separation;
                [Value(Name = "Heroic Burst Separation", Offset = 232)]
                public Single _232_Heroic_Burst_Separation;
                [Value(Name = "Legendary Burst Separation", Offset = 236)]
                public Single _236_Legendary_Burst_Separation;
                [Value(Name = "Easy Target Tracking", Offset = 240)]
                public Single _240_Easy_Target_Tracking;
                [Value(Name = "Normal Target Tracking", Offset = 244)]
                public Single _244_Normal_Target_Tracking;
                [Value(Name = "Heroic Target Tracking", Offset = 248)]
                public Single _248_Heroic_Target_Tracking;
                [Value(Name = "Legendary Target Tracking", Offset = 252)]
                public Single _252_Legendary_Target_Tracking;
                [Value(Name = "Easy Target Leading", Offset = 256)]
                public Single _256_Easy_Target_Leading;
                [Value(Name = "Normal Target Leading", Offset = 260)]
                public Single _260_Normal_Target_Leading;
                [Value(Name = "Heroic Target Leading", Offset = 264)]
                public Single _264_Heroic_Target_Leading;
                [Value(Name = "Legendary Target Leading", Offset = 268)]
                public Single _268_Legendary_Target_Leading;
                [Value(Name = "Easy Overcharge Chance", Offset = 272)]
                public Single _272_Easy_Overcharge_Chance;
                [Value(Name = "Normal Overcharge Chance", Offset = 276)]
                public Single _276_Normal_Overcharge_Chance;
                [Value(Name = "Heroic Overcharge Chance", Offset = 280)]
                public Single _280_Heroic_Overcharge_Chance;
                [Value(Name = "Legendary Overcharge Chance", Offset = 284)]
                public Single _284_Legendary_Overcharge_Chance;
                [Value(Name = "Easy Special Fire Delay", Offset = 288)]
                public Single _288_Easy_Special_Fire_Delay;
                [Value(Name = "Normal Special Fire Delay", Offset = 292)]
                public Single _292_Normal_Special_Fire_Delay;
                [Value(Name = "Heroic Special Fire Delay", Offset = 296)]
                public Single _296_Heroic_Special_Fire_Delay;
                [Value(Name = "Legendary Special Fire Delay", Offset = 300)]
                public Single _300_Legendary_Special_Fire_Delay;
                [Value(Name = "Easy Guidance Vs Player", Offset = 304)]
                public Single _304_Easy_Guidance_Vs_Player;
                [Value(Name = "Normal Guidance Vs Player", Offset = 308)]
                public Single _308_Normal_Guidance_Vs_Player;
                [Value(Name = "Heroic Guidance Vs Player", Offset = 312)]
                public Single _312_Heroic_Guidance_Vs_Player;
                [Value(Name = "Legendary Guidance Vs Player", Offset = 316)]
                public Single _316_Legendary_Guidance_Vs_Player;
                [Value(Name = "Easy Melee Delay Base", Offset = 320)]
                public Single _320_Easy_Melee_Delay_Base;
                [Value(Name = "Normal Melee Delay Base", Offset = 324)]
                public Single _324_Normal_Melee_Delay_Base;
                [Value(Name = "Heroic Melee Delay Base", Offset = 328)]
                public Single _328_Heroic_Melee_Delay_Base;
                [Value(Name = "Legendary Melee Delay Base", Offset = 332)]
                public Single _332_Legendary_Melee_Delay_Base;
                [Value(Name = "Easy Melee Delay Scale", Offset = 336)]
                public Single _336_Easy_Melee_Delay_Scale;
                [Value(Name = "Normal Melee Delay Scale", Offset = 340)]
                public Single _340_Normal_Melee_Delay_Scale;
                [Value(Name = "Heroic Melee Delay Scale", Offset = 344)]
                public Single _344_Heroic_Melee_Delay_Scale;
                [Value(Name = "Legendary Melee Delay Scale", Offset = 348)]
                public Single _348_Legendary_Melee_Delay_Scale;
                [Value(Offset = 352)]
                public Byte[] _352_ = new byte[16];
                [Value(Name = "Easy Grenade Chance Scale", Offset = 368)]
                public Single _368_Easy_Grenade_Chance_Scale;
                [Value(Name = "Normal Grenade Chance Scale", Offset = 372)]
                public Single _372_Normal_Grenade_Chance_Scale;
                [Value(Name = "Heroic Grenade Chance Scale", Offset = 376)]
                public Single _376_Heroic_Grenade_Chance_Scale;
                [Value(Name = "Legendary Grenade Chance Scale", Offset = 380)]
                public Single _380_Legendary_Grenade_Chance_Scale;
                [Value(Name = "Easy Grenade Timer Scale", Offset = 384)]
                public Single _384_Easy_Grenade_Timer_Scale;
                [Value(Name = "Normal Grenade Timer Scale", Offset = 388)]
                public Single _388_Normal_Grenade_Timer_Scale;
                [Value(Name = "Heroic Grenade Timer Scale", Offset = 392)]
                public Single _392_Heroic_Grenade_Timer_Scale;
                [Value(Name = "Legendary Grenade Timer Scale", Offset = 396)]
                public Single _396_Legendary_Grenade_Timer_Scale;
                [Value(Offset = 400)]
                public Byte[] _400_ = new byte[48];
                [Value(Name = "Easy Major Upgrade (Normal)", Offset = 448)]
                public Single _448_Easy_Major_Upgrade__Normal_;
                [Value(Name = "Normal Major Upgrade (Normal)", Offset = 452)]
                public Single _452_Normal_Major_Upgrade__Normal_;
                [Value(Name = "Heroic Major Upgrade (Normal)", Offset = 456)]
                public Single _456_Heroic_Major_Upgrade__Normal_;
                [Value(Name = "Legendary Major Upgrade (Normal)", Offset = 460)]
                public Single _460_Legendary_Major_Upgrade__Normal_;
                [Value(Name = "Easy Major Upgrade (Few)", Offset = 464)]
                public Single _464_Easy_Major_Upgrade__Few_;
                [Value(Name = "Normal Major Upgrade (Few)", Offset = 468)]
                public Single _468_Normal_Major_Upgrade__Few_;
                [Value(Name = "Heroic Major Upgrade (Few)", Offset = 472)]
                public Single _472_Heroic_Major_Upgrade__Few_;
                [Value(Name = "Legendary Major Upgrade (Few)", Offset = 476)]
                public Single _476_Legendary_Major_Upgrade__Few_;
                [Value(Name = "Easy Major Upgrade (Many)", Offset = 480)]
                public Single _480_Easy_Major_Upgrade__Many_;
                [Value(Name = "Normal Major Upgrade (Many)", Offset = 484)]
                public Single _484_Normal_Major_Upgrade__Many_;
                [Value(Name = "Heroic Major Upgrade (Many)", Offset = 488)]
                public Single _488_Heroic_Major_Upgrade__Many_;
                [Value(Name = "Legendary Major Upgrade (Many)", Offset = 492)]
                public Single _492_Legendary_Major_Upgrade__Many_;
                [Value(Name = "Easy Player Vehicle Ram Chance", Offset = 496)]
                public Single _496_Easy_Player_Vehicle_Ram_Chance;
                [Value(Name = "Normal Player Vehicle Ram Chance", Offset = 500)]
                public Single _500_Normal_Player_Vehicle_Ram_Chance;
                [Value(Name = "Heroic Player Vehicle Ram Chance", Offset = 504)]
                public Single _504_Heroic_Player_Vehicle_Ram_Chance;
                [Value(Name = "Legendary Player Vehicle Ram Chance", Offset = 508)]
                public Single _508_Legendary_Player_Vehicle_Ram_Chance;
                [Value(Offset = 512)]
                public Byte[] _512_ = new byte[132];
            }
            public _256_grenades[] _256_Grenades;
            [Serializable]
            [Reflexive(Name = "Grenades", Offset = 256, ChunkSize = 44, Label = "")]
            public class _256_grenades
            {
                private int __StartOffset__;
                [Value(Name = "Max Count", Offset = 0)]
                public Int16 _0_Max_Count;
                [Value(Offset = 2)]
                public Byte[] _2_ = new byte[2];
                [Value(Name = "Throwing Effect", Offset = 4)]
                public Dependancy _4_Throwing_Effect;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[16];
                [Value(Name = "Equipment", Offset = 28)]
                public Dependancy _28_Equipment;
                [Value(Name = "Projectile", Offset = 36)]
                public Dependancy _36_Projectile;
            }
            public _264_rasterizer_data[] _264_Rasterizer_Data;
            [Serializable]
            [Reflexive(Name = "Rasterizer Data", Offset = 264, ChunkSize = 264, Label = "")]
            public class _264_rasterizer_data
            {
                private int __StartOffset__;
                [Value(Name = "Distance Attenuation", Offset = 0)]
                public Dependancy _0_Distance_Attenuation;
                [Value(Name = "Vector Normalization", Offset = 8)]
                public Dependancy _8_Vector_Normalization;
                [Value(Name = "Gradients", Offset = 16)]
                public Dependancy _16_Gradients;
                [Value(Name = "Loading Screen", Offset = 24)]
                public Dependancy _24_Loading_Screen;
                [Value(Name = "Loading Screen Sweep", Offset = 32)]
                public Dependancy _32_Loading_Screen_Sweep;
                [Value(Name = "Loading Screen Spinner", Offset = 40)]
                public Dependancy _40_Loading_Screen_Spinner;
                [Value(Name = "Glow", Offset = 48)]
                public Dependancy _48_Glow;
                [Value(Name = "Loading Screen Logos", Offset = 56)]
                public Dependancy _56_Loading_Screen_Logos;
                [Value(Name = "Loading Screen Tickers", Offset = 64)]
                public Dependancy _64_Loading_Screen_Tickers;
                [Value(Offset = 72)]
                public Byte[] _72_ = new byte[16];
                public _88_global_vertex_shaders[] _88_Global_Vertex_Shaders;
                [Serializable]
                [Reflexive(Name = "Global Vertex Shaders", Offset = 88, ChunkSize = 8, Label = "")]
                public class _88_global_vertex_shaders
                {
                    private int __StartOffset__;
                    [Value(Name = "Vertex Shader", Offset = 0)]
                    public Dependancy _0_Vertex_Shader;
                }
                [Value(Name = "Default 2D", Offset = 96)]
                public Dependancy _96_Default_2D;
                [Value(Name = "Default 3D", Offset = 104)]
                public Dependancy _104_Default_3D;
                [Value(Name = "Default Cube Map", Offset = 112)]
                public Dependancy _112_Default_Cube_Map;
                [Value(Offset = 120)]
                public Byte[] _120_ = new byte[48];
                [Value(Offset = 168)]
                public Byte[] _168_ = new byte[36];
                [Value(Name = "Global Shader", Offset = 204)]
                public Dependancy _204_Global_Shader;
                [Value(Name = "Flags", Offset = 212)]
                public Bitmask _212_Flags = new Bitmask(new string[] { "Tint Edge Density" }, 4);
                [Value(Name = "Active Camo Refraction Amount", Offset = 216)]
                public Single _216_Active_Camo_Refraction_Amount;
                [Value(Name = "Active Camo Distance Falloff", Offset = 220)]
                public Single _220_Active_Camo_Distance_Falloff;
                [Value(Name = "Active Camo Tint Color R", Offset = 224)]
                public Single _224_Active_Camo_Tint_Color_R;
                [Value(Name = "Active Camo Tint Color G", Offset = 228)]
                public Single _228_Active_Camo_Tint_Color_G;
                [Value(Name = "Active Camo Tint Color B", Offset = 232)]
                public Single _232_Active_Camo_Tint_Color_B;
                [Value(Name = "Active Camo Hyper Stealth Refraction", Offset = 236)]
                public Single _236_Active_Camo_Hyper_Stealth_Refraction;
                [Value(Name = "Active Camo Hyper Stealth Distance Falloff", Offset = 240)]
                public Single _240_Active_Camo_Hyper_Stealth_Distance_Falloff;
                [Value(Name = "Active Camo Hyper Stealth Tint  R", Offset = 244)]
                public Single _244_Active_Camo_Hyper_Stealth_Tint__R;
                [Value(Name = "Active Camo Hyper Stealth Tint G", Offset = 248)]
                public Single _248_Active_Camo_Hyper_Stealth_Tint_G;
                [Value(Name = "Active Camo Hyper Stealth Tint B", Offset = 252)]
                public Single _252_Active_Camo_Hyper_Stealth_Tint_B;
                [Value(Name = "unused", Offset = 256)]
                public Dependancy _256_unused;
            }
            public _272_interface_tag_refernces[] _272_Interface_Tag_Refernces;
            [Serializable]
            [Reflexive(Name = "Interface Tag Refernces", Offset = 272, ChunkSize = 152, Label = "")]
            public class _272_interface_tag_refernces
            {
                private int __StartOffset__;
                [Value(Name = "Font System", Offset = 0)]
                public Dependancy _0_Font_System;
                [Value(Name = "Font Terminal", Offset = 8)]
                public Dependancy _8_Font_Terminal;
                [Value(Name = "Screen Color Table", Offset = 16)]
                public Dependancy _16_Screen_Color_Table;
                [Value(Name = "Hud Color Table", Offset = 24)]
                public Dependancy _24_Hud_Color_Table;
                [Value(Name = "Editor Color Table", Offset = 32)]
                public Dependancy _32_Editor_Color_Table;
                [Value(Name = "Dialog Color Table", Offset = 40)]
                public Dependancy _40_Dialog_Color_Table;
                [Value(Name = "Hud Globals", Offset = 48)]
                public Dependancy _48_Hud_Globals;
                [Value(Name = "Motion Sensor Sweep Bitmap", Offset = 56)]
                public Dependancy _56_Motion_Sensor_Sweep_Bitmap;
                [Value(Name = "Motion Sensor Sweep Bitmap Mask", Offset = 64)]
                public Dependancy _64_Motion_Sensor_Sweep_Bitmap_Mask;
                [Value(Name = "Multiplayer Hud Bitmap", Offset = 72)]
                public Dependancy _72_Multiplayer_Hud_Bitmap;
                [Value(Offset = 80)]
                public Byte[] _80_ = new byte[8];
                [Value(Name = "Hud Digits Definition", Offset = 88)]
                public Dependancy _88_Hud_Digits_Definition;
                [Value(Name = "Motion Sensor Blip Bitmap", Offset = 96)]
                public Dependancy _96_Motion_Sensor_Blip_Bitmap;
                [Value(Name = "Interface Goo Map 1", Offset = 104)]
                public Dependancy _104_Interface_Goo_Map_1;
                [Value(Name = "Interface Goo Map 2", Offset = 112)]
                public Dependancy _112_Interface_Goo_Map_2;
                [Value(Name = "Interface Goo Map 3", Offset = 120)]
                public Dependancy _120_Interface_Goo_Map_3;
                [Value(Name = "Mainmenu UI Globals", Offset = 128)]
                public Dependancy _128_Mainmenu_UI_Globals;
                [Value(Name = "Singleplayer UI Globals", Offset = 136)]
                public Dependancy _136_Singleplayer_UI_Globals;
                [Value(Name = "Multiplayer UI Globals", Offset = 144)]
                public Dependancy _144_Multiplayer_UI_Globals;
            }
            public _280_weapon_list__to_do_with_hard_coded_enum_[] _280_Weapon_List__To_Do_With_Hard_Coded_Enum_;
            [Serializable]
            [Reflexive(Name = "Weapon List (To Do With Hard Coded Enum)", Offset = 280, ChunkSize = 8, Label = "")]
            public class _280_weapon_list__to_do_with_hard_coded_enum_
            {
                private int __StartOffset__;
                [Value(Name = "Weapon", Offset = 0)]
                public Dependancy _0_Weapon;
            }
            public _288_cheat_powerups[] _288_Cheat_Powerups;
            [Serializable]
            [Reflexive(Name = "Cheat Powerups", Offset = 288, ChunkSize = 8, Label = "")]
            public class _288_cheat_powerups
            {
                private int __StartOffset__;
                [Value(Name = "Powerup", Offset = 0)]
                public Dependancy _0_Powerup;
            }
            public _296_multiplayer_information__depricated_[] _296_Multiplayer_Information__depricated_;
            [Serializable]
            [Reflexive(Name = "Multiplayer Information (depricated)", Offset = 296, ChunkSize = 152, Label = "")]
            public class _296_multiplayer_information__depricated_
            {
                private int __StartOffset__;
                [Value(Name = "Flag", Offset = 0)]
                public Dependancy _0_Flag;
                [Value(Name = "Unit", Offset = 8)]
                public Dependancy _8_Unit;
                [Value(Offset = 16)]
                public Byte[] _16_ = new byte[8];
                [Value(Name = "Hill Shader", Offset = 24)]
                public Dependancy _24_Hill_Shader;
                [Value(Name = "Flag Shader", Offset = 32)]
                public Dependancy _32_Flag_Shader;
                [Value(Name = "Ball", Offset = 40)]
                public Dependancy _40_Ball;
                [Value(Offset = 48)]
                public Byte[] _48_ = new byte[104];
            }
            public _304_player_information[] _304_Player_Information;
            [Serializable]
            [Reflexive(Name = "Player Information", Offset = 304, ChunkSize = 284, Label = "")]
            public class _304_player_information
            {
                private int __StartOffset__;
                [Value(Name = "unused", Offset = 0)]
                public Dependancy _0_unused;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[28];
                [Value(Name = "Walking Speed", Offset = 36)]
                public Single _36_Walking_Speed;
                [Value(Offset = 40)]
                public Byte[] _40_ = new byte[4];
                [Value(Name = "Forward Run Speed", Offset = 44)]
                public Single _44_Forward_Run_Speed;
                [Value(Name = "Backward Run Speed", Offset = 48)]
                public Single _48_Backward_Run_Speed;
                [Value(Name = "Sideways Run Speed", Offset = 52)]
                public Single _52_Sideways_Run_Speed;
                [Value(Name = "Run Acceleration", Offset = 56)]
                public Single _56_Run_Acceleration;
                [Value(Name = "Forward Crouch Speed", Offset = 60)]
                public Single _60_Forward_Crouch_Speed;
                [Value(Name = "Backward Crouch Speed", Offset = 64)]
                public Single _64_Backward_Crouch_Speed;
                [Value(Name = "Sideways Crouch Speed", Offset = 68)]
                public Single _68_Sideways_Crouch_Speed;
                [Value(Name = "Crouch Acceleration", Offset = 72)]
                public Single _72_Crouch_Acceleration;
                [Value(Name = "Airborn Acceleration", Offset = 76)]
                public Single _76_Airborn_Acceleration;
                [Value(Offset = 80)]
                public Byte[] _80_ = new byte[16];
                [Value(Name = "Grenade Origin X", Offset = 96)]
                public Single _96_Grenade_Origin_X;
                [Value(Name = "Grenade Origin Y", Offset = 100)]
                public Single _100_Grenade_Origin_Y;
                [Value(Name = "Grenade Origin Z", Offset = 104)]
                public Single _104_Grenade_Origin_Z;
                [Value(Offset = 108)]
                public Byte[] _108_ = new byte[12];
                [Value(Name = "Stun Penalty Movement", Offset = 120)]
                public Single _120_Stun_Penalty_Movement;
                [Value(Name = "Stun Penalty Turning", Offset = 124)]
                public Single _124_Stun_Penalty_Turning;
                [Value(Name = "Stun Penalty Jumping", Offset = 128)]
                public Single _128_Stun_Penalty_Jumping;
                [Value(Name = "Min Stun Time", Offset = 132)]
                public Single _132_Min_Stun_Time;
                [Value(Name = "Max Stun Time", Offset = 136)]
                public Single _136_Max_Stun_Time;
                [Value(Offset = 140)]
                public Byte[] _140_ = new byte[8];
                [Value(Name = "Fp Idle Time", Offset = 148)]
                public Single _148_Fp_Idle_Time;
                [Value(Name = "...To", Offset = 152)]
                public Single _152____To;
                [Value(Name = "Fp Skip Fraction", Offset = 156)]
                public Single _156_Fp_Skip_Fraction;
                [Value(Offset = 160)]
                public Byte[] _160_ = new byte[16];
                [Value(Name = "Coop Respawn Effect", Offset = 176)]
                public Dependancy _176_Coop_Respawn_Effect;
                [Value(Name = "Binoculars Zoom Count", Offset = 184)]
                public Single _184_Binoculars_Zoom_Count;
                [Value(Name = "Binoculars Zoom Range", Offset = 188)]
                public Single _188_Binoculars_Zoom_Range;
                [Value(Name = "...To", Offset = 192)]
                public Single _192____To;
                [Value(Name = "Binoculars Zoom In Sound", Offset = 196)]
                public Dependancy _196_Binoculars_Zoom_In_Sound;
                [Value(Name = "Binoculars Zoom Out Sound", Offset = 204)]
                public Dependancy _204_Binoculars_Zoom_Out_Sound;
                [Value(Offset = 212)]
                public Byte[] _212_ = new byte[16];
                [Value(Name = "Active Camo On", Offset = 228)]
                public Dependancy _228_Active_Camo_On;
                [Value(Name = "Active Camo Off", Offset = 236)]
                public Dependancy _236_Active_Camo_Off;
                [Value(Name = "Active Camo Error", Offset = 244)]
                public Dependancy _244_Active_Camo_Error;
                [Value(Name = "Active Camo Ready", Offset = 252)]
                public Dependancy _252_Active_Camo_Ready;
                [Value(Name = "Flashlight On", Offset = 260)]
                public Dependancy _260_Flashlight_On;
                [Value(Name = "Flashlight Off", Offset = 268)]
                public Dependancy _268_Flashlight_Off;
                [Value(Name = "Ice Cream", Offset = 276)]
                public Dependancy _276_Ice_Cream;
            }
            public _312_player_representation[] _312_Player_Representation;
            [Serializable]
            [Reflexive(Name = "Player Representation", Offset = 312, ChunkSize = 188, Label = "")]
            public class _312_player_representation
            {
                private int __StartOffset__;
                [Value(Name = "1st Person Hands", Offset = 0)]
                public Dependancy _0_1st_Person_Hands;
                [Value(Name = "1st Person Body", Offset = 8)]
                public Dependancy _8_1st_Person_Body;
                [Value(Offset = 16)]
                public Byte[] _16_ = new byte[160];
                [Value(Name = "3rd Person Unit", Offset = 176)]
                public Dependancy _176_3rd_Person_Unit;
                [Value(Name = "3rd Person Unit Variant", Offset = 184)]
                public StringID _184_3rd_Person_Unit_Variant;
            }
            public _320_falling_damage[] _320_Falling_Damage;
            [Serializable]
            [Reflexive(Name = "Falling Damage", Offset = 320, ChunkSize = 108, Label = "")]
            public class _320_falling_damage
            {
                private int __StartOffset__;
                [Value(Offset = 0)]
                public Byte[] _0_ = new byte[8];
                [Value(Name = "Harmful Falling Distance", Offset = 8)]
                public Single _8_Harmful_Falling_Distance;
                [Value(Name = "...To", Offset = 12)]
                public Single _12____To;
                [Value(Name = "Falling Damage", Offset = 16)]
                public Dependancy _16_Falling_Damage;
                [Value(Offset = 24)]
                public Byte[] _24_ = new byte[8];
                [Value(Name = "Maximum Falling Distance", Offset = 32)]
                public Single _32_Maximum_Falling_Distance;
                [Value(Name = "Distance Damage", Offset = 36)]
                public Dependancy _36_Distance_Damage;
                [Value(Name = "Vehicle Environment Collision Damage", Offset = 44)]
                public Dependancy _44_Vehicle_Environment_Collision_Damage;
                [Value(Name = "Vehicle Killed Unit Damage Effect", Offset = 52)]
                public Dependancy _52_Vehicle_Killed_Unit_Damage_Effect;
                [Value(Name = "Vehicle Collision Damage", Offset = 60)]
                public Dependancy _60_Vehicle_Collision_Damage;
                [Value(Name = "Flaming Death Damage", Offset = 68)]
                public Dependancy _68_Flaming_Death_Damage;
                [Value(Offset = 76)]
                public Byte[] _76_ = new byte[32];
            }
            public _328_old_materials[] _328_Old_Materials;
            [Serializable]
            [Reflexive(Name = "Old Materials", Offset = 328, ChunkSize = 36, Label = "")]
            public class _328_old_materials
            {
                private int __StartOffset__;
                [Value(Name = "Material Name", Offset = 0)]
                public StringID _0_Material_Name;
                [Value(Name = "General Material Name", Offset = 4)]
                public StringID _4_General_Material_Name;
                [Value(Name = "Ground Friction Scale", Offset = 8)]
                public Single _8_Ground_Friction_Scale;
                [Value(Name = "Ground Friction Normal K0 Scale", Offset = 12)]
                public Single _12_Ground_Friction_Normal_K0_Scale;
                [Value(Name = "Ground Friction Normal K1 Scale", Offset = 16)]
                public Single _16_Ground_Friction_Normal_K1_Scale;
                [Value(Name = "Ground Depth Scale", Offset = 20)]
                public Single _20_Ground_Depth_Scale;
                [Value(Name = "Ground Damp Fraction Scale", Offset = 24)]
                public Single _24_Ground_Damp_Fraction_Scale;
                [Value(Name = "Melee Hit Sound", Offset = 28)]
                public Dependancy _28_Melee_Hit_Sound;
            }
            public _336_materials[] _336_Materials;
            [Serializable]
            [Reflexive(Name = "Materials", Offset = 336, ChunkSize = 180, Label = "")]
            public class _336_materials
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Name = "Parent Name", Offset = 4)]
                public StringID _4_Parent_Name;
                [Value(Name = "Flags", Offset = 8)]
                public Bitmask _8_Flags = new Bitmask(new string[] { }, 4);
                [Value(Name = "Old Material Type", Offset = 12)]
                public H2.DataTypes.Enum _12_Old_Material_Type = new H2.DataTypes.Enum(new string[] { "Dirt", "Sand", "Stone", "Snow", "Wood", "Metal (hollow)", "Metal (thin)", "Metal (thick)", "Rubber", "Glass", "Force Field", "Grunt", "Hunter Armor", "Hunter Skin", "Elite", "Jackal", "Jackal Energy Shield", "Engineer Skin", "Engineer Force Field", "Flood Combat Form", "Flood Carrier Form", "Cyborg Armor", "Cyborg Energy Shield", "Human Armor", "Human Skin", "Sentinel", "Monitor", "Plastic", "Water", "Leaves", "Elite Energy Shield", "Ice", "Hunter Shield" }, 4);
                [Value(Name = "General Armor", Offset = 16)]
                public StringID _16_General_Armor;
                [Value(Name = "Specific Armor", Offset = 20)]
                public StringID _20_Specific_Armor;
                [Value(Offset = 24)]
                public Byte[] _24_ = new byte[4];
                [Value(Name = "Friction", Offset = 28)]
                public Single _28_Friction;
                [Value(Name = "Restitution", Offset = 32)]
                public Single _32_Restitution;
                [Value(Name = "Density", Offset = 36)]
                public Single _36_Density;
                [Value(Name = "Old Material Physics", Offset = 40)]
                public Dependancy _40_Old_Material_Physics;
                [Value(Name = "Breakable Surface", Offset = 48)]
                public Dependancy _48_Breakable_Surface;
                [Value(Name = "Sound Sweetener (small)", Offset = 56)]
                public Dependancy _56_Sound_Sweetener__small_;
                [Value(Name = "Sound Sweetener (medium)", Offset = 64)]
                public Dependancy _64_Sound_Sweetener__medium_;
                [Value(Name = "Sound Sweetener (large)", Offset = 72)]
                public Dependancy _72_Sound_Sweetener__large_;
                [Value(Name = "Sound Sweetener Rolling", Offset = 80)]
                public Dependancy _80_Sound_Sweetener_Rolling;
                [Value(Name = "Sound Sweetener Grinding", Offset = 88)]
                public Dependancy _88_Sound_Sweetener_Grinding;
                [Value(Name = "Sound Sweetener (melee)", Offset = 96)]
                public Dependancy _96_Sound_Sweetener__melee_;
                [Value(Offset = 104)]
                public Byte[] _104_ = new byte[8];
                [Value(Name = "Effect Sweetener (small)", Offset = 112)]
                public Dependancy _112_Effect_Sweetener__small_;
                [Value(Name = "Effect Sweetener (medium)", Offset = 120)]
                public Dependancy _120_Effect_Sweetener__medium_;
                [Value(Name = "Effect Sweetener (large)", Offset = 128)]
                public Dependancy _128_Effect_Sweetener__large_;
                [Value(Name = "Effect Sweetener Rolling", Offset = 136)]
                public Dependancy _136_Effect_Sweetener_Rolling;
                [Value(Name = "Effect Sweetener Grinding", Offset = 144)]
                public Dependancy _144_Effect_Sweetener_Grinding;
                [Value(Name = "Effect Sweetener (melee)", Offset = 152)]
                public Dependancy _152_Effect_Sweetener__melee_;
                [Value(Offset = 160)]
                public Byte[] _160_ = new byte[8];
                [Value(Name = "Sweetener Inheritance Flags", Offset = 168)]
                public Bitmask _168_Sweetener_Inheritance_Flags = new Bitmask(new string[] { "Sound Small", "Sound Medium", "Sound Large", "Sound Rolling", "Sound Grinding", "Sound Melee", "", "Effect Small", "Effect Medium", "Effect Large", "Effect Rolling", "Effect Grinding", "Effect Melee" }, 4);
                [Value(Name = "Material Effects", Offset = 172)]
                public Dependancy _172_Material_Effects;
            }
            public _344_multiplayer_ui__obsolete_[] _344_Multiplayer_UI__obsolete_;
            [Serializable]
            [Reflexive(Name = "Multiplayer UI (obsolete)", Offset = 344, ChunkSize = 32, Label = "")]
            public class _344_multiplayer_ui__obsolete_
            {
                private int __StartOffset__;
                [Value(Offset = 0)]
                public Byte[] _0_ = new byte[32];
            }
            public _352_profile_colors[] _352_Profile_Colors;
            [Serializable]
            [Reflexive(Name = "Profile Colors", Offset = 352, ChunkSize = 12, Label = "")]
            public class _352_profile_colors
            {
                private int __StartOffset__;
                [Value(Name = "R", Offset = 0)]
                public Single _0_R;
                [Value(Name = "G", Offset = 4)]
                public Single _4_G;
                [Value(Name = "B", Offset = 8)]
                public Single _8_B;
            }
            [Value(Name = "Multiplayer Globals", Offset = 360)]
            public Dependancy _360_Multiplayer_Globals;
            public _368_runtime_level_data[] _368_Runtime_Level_Data;
            [Serializable]
            [Reflexive(Name = "Runtime Level Data", Offset = 368, ChunkSize = 8, Label = "")]
            public class _368_runtime_level_data
            {
                private int __StartOffset__;
                public _0_campaign_levels[] _0_Campaign_Levels;
                [Serializable]
                [Reflexive(Name = "Campaign Levels", Offset = 0, ChunkSize = 264, Label = "")]
                public class _0_campaign_levels
                {
                    private int __StartOffset__;
                    [Value(Name = "Campaign ID", Offset = 0)]
                    public Int32 _0_Campaign_ID;
                    [Value(Name = "Map ID", Offset = 4)]
                    public Int32 _4_Map_ID;
                    [Value(Name = "Scenario Path", Offset = 8)]
                    public H2.DataTypes.String _8_Scenario_Path = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                }
            }
            public _376_ui_level_data[] _376_UI_Level_Data;
            [Serializable]
            [Reflexive(Name = "UI Level Data", Offset = 376, ChunkSize = 24, Label = "")]
            public class _376_ui_level_data
            {
                private int __StartOffset__;
                public _0_campaigns[] _0_Campaigns;
                [Serializable]
                [Reflexive(Name = "Campaigns", Offset = 0, ChunkSize = 2884, Label = "")]
                public class _0_campaigns
                {
                    private int __StartOffset__;
                    [Value(Name = "Campaign ID", Offset = 0)]
                    public Int32 _0_Campaign_ID;
                    [Value(Offset = 4)]
                    public Byte[] _4_ = new byte[2880];
                }
                public _8_campaign_levels[] _8_Campaign_Levels;
                [Serializable]
                [Reflexive(Name = "Campaign Levels", Offset = 8, ChunkSize = 2392, Label = "")]
                public class _8_campaign_levels
                {
                    private int __StartOffset__;
                    [Value(Name = "Campaign ID", Offset = 0)]
                    public Int32 _0_Campaign_ID;
                    [Value(Name = "Map ID", Offset = 4)]
                    public Int32 _4_Map_ID;
                    [Value(Name = "Preview Image", Offset = 8)]
                    public Dependancy _8_Preview_Image;
                    [Value(Name = "English Name", Offset = 16)]
                    public H2.DataTypes.String _16_English_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Japanese Name", Offset = 24)]
                    public H2.DataTypes.String _24_Japanese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "German Name", Offset = 32)]
                    public H2.DataTypes.String _32_German_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "French Name", Offset = 40)]
                    public H2.DataTypes.String _40_French_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Spanish Name", Offset = 48)]
                    public H2.DataTypes.String _48_Spanish_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Italian Name", Offset = 56)]
                    public H2.DataTypes.String _56_Italian_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Korean Name", Offset = 64)]
                    public H2.DataTypes.String _64_Korean_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Chinese Name", Offset = 72)]
                    public H2.DataTypes.String _72_Chinese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Portuguese Name", Offset = 80)]
                    public H2.DataTypes.String _80_Portuguese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "English Description", Offset = 88)]
                    public H2.DataTypes.String _88_English_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Japanese Description", Offset = 344)]
                    public H2.DataTypes.String _344_Japanese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "German Description", Offset = 600)]
                    public H2.DataTypes.String _600_German_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "French Description", Offset = 856)]
                    public H2.DataTypes.String _856_French_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Spanish Description", Offset = 1112)]
                    public H2.DataTypes.String _1112_Spanish_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Italian Description", Offset = 1368)]
                    public H2.DataTypes.String _1368_Italian_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Korean Description", Offset = 1624)]
                    public H2.DataTypes.String _1624_Korean_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Chinese Description", Offset = 1880)]
                    public H2.DataTypes.String _1880_Chinese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Portuguese Description", Offset = 2136)]
                    public H2.DataTypes.String _2136_Portuguese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                }
                public _16_multiplayer[] _16_Multiplayer;
                [Serializable]
                [Reflexive(Name = "Multiplayer", Offset = 16, ChunkSize = 2666, Label = "")]
                public class _16_multiplayer
                {
                    private int __StartOffset__;
                    [Value(Name = "Map ID", Offset = 0)]
                    public Int32 _0_Map_ID;
                    [Value(Name = "Preview Image", Offset = 4)]
                    public Dependancy _4_Preview_Image;
                    [Value(Name = "English Name", Offset = 12)]
                    public H2.DataTypes.String _12_English_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Japanese Name", Offset = 20)]
                    public H2.DataTypes.String _20_Japanese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Dutch Name", Offset = 28)]
                    public H2.DataTypes.String _28_Dutch_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "French Name", Offset = 36)]
                    public H2.DataTypes.String _36_French_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Spanish Name", Offset = 44)]
                    public H2.DataTypes.String _44_Spanish_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Italian Name", Offset = 52)]
                    public H2.DataTypes.String _52_Italian_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Korean Name", Offset = 60)]
                    public H2.DataTypes.String _60_Korean_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Chinese Name", Offset = 68)]
                    public H2.DataTypes.String _68_Chinese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "Portuguese Name", Offset = 76)]
                    public H2.DataTypes.String _76_Portuguese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.ASCII);
                    [Value(Name = "English Description", Offset = 84)]
                    public H2.DataTypes.String _84_English_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Japanese Description", Offset = 340)]
                    public H2.DataTypes.String _340_Japanese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Dutch Description", Offset = 596)]
                    public H2.DataTypes.String _596_Dutch_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "French Description", Offset = 852)]
                    public H2.DataTypes.String _852_French_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Spanish Description", Offset = 1108)]
                    public H2.DataTypes.String _1108_Spanish_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Italian Description", Offset = 1364)]
                    public H2.DataTypes.String _1364_Italian_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Korean Description", Offset = 1620)]
                    public H2.DataTypes.String _1620_Korean_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Chinese Description", Offset = 1876)]
                    public H2.DataTypes.String _1876_Chinese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Portuguese Description", Offset = 2132)]
                    public H2.DataTypes.String _2132_Portuguese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Scenario Path", Offset = 2388)]
                    public H2.DataTypes.String _2388_Scenario_Path = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.ASCII);
                    [Value(Name = "Sort Order", Offset = 2644)]
                    public Int32 _2644_Sort_Order;
                    [Value(Name = "Flags", Offset = 2648)]
                    public Bitmask _2648_Flags = new Bitmask(new string[] { "Unlockable" }, 2);
                    [Value(Name = "Max Teams None", Offset = 2650)]
                    public Byte _2650_Max_Teams_None;
                    [Value(Name = "Max Teams CTF", Offset = 2651)]
                    public Byte _2651_Max_Teams_CTF;
                    [Value(Name = "Max Teams Slayer", Offset = 2652)]
                    public Byte _2652_Max_Teams_Slayer;
                    [Value(Name = "Max Teams Oddball", Offset = 2653)]
                    public Byte _2653_Max_Teams_Oddball;
                    [Value(Name = "Max Teams KOTH", Offset = 2654)]
                    public Byte _2654_Max_Teams_KOTH;
                    [Value(Name = "Max Teams Race", Offset = 2655)]
                    public Byte _2655_Max_Teams_Race;
                    [Value(Name = "Max Teams Headhunter", Offset = 2656)]
                    public Byte _2656_Max_Teams_Headhunter;
                    [Value(Name = "Max Teams Juggernaught", Offset = 2657)]
                    public Byte _2657_Max_Teams_Juggernaught;
                    [Value(Name = "Max Teams Territories", Offset = 2658)]
                    public Byte _2658_Max_Teams_Territories;
                    [Value(Name = "Max Teams Assault", Offset = 2659)]
                    public Byte _2659_Max_Teams_Assault;
                    [Value(Name = "Max Teams Stub 10", Offset = 2660)]
                    public Byte _2660_Max_Teams_Stub_10;
                    [Value(Name = "Max Teams Stub 11", Offset = 2661)]
                    public Byte _2661_Max_Teams_Stub_11;
                    [Value(Name = "Max Teams Stub 12", Offset = 2662)]
                    public Byte _2662_Max_Teams_Stub_12;
                    [Value(Name = "Max Teams Stub 13", Offset = 2663)]
                    public Byte _2663_Max_Teams_Stub_13;
                    [Value(Name = "Max Teams Stub 14", Offset = 2664)]
                    public Byte _2664_Max_Teams_Stub_14;
                    [Value(Name = "Max Teams Stub 15", Offset = 2665)]
                    public Byte _2665_Max_Teams_Stub_15;
                }
            }
            [Value(Name = "Default Global Lighting", Offset = 384)]
            public Dependancy _384_Default_Global_Lighting;
            [Value(Offset = 392)]
            public Byte[] _392_ = new byte[8];
            [Value(Name = "English String Count", Offset = 400)]
            public Int32 _400_English_String_Count;
            [Value(Name = "English String Table Size", Offset = 404)]
            public Int32 _404_English_String_Table_Size;
            [Value(Name = "English String Index Offset", Offset = 408)]
            public Int32 _408_English_String_Index_Offset;
            [Value(Name = "English String Table Offset", Offset = 412)]
            public Int32 _412_English_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 416)]
            public Int32 _416_Unknown;
            [Value(Offset = 420)]
            public Byte[] _420_ = new byte[8];
            [Value(Name = "Japanese String Count", Offset = 428)]
            public Int32 _428_Japanese_String_Count;
            [Value(Name = "Japanese String Table Size", Offset = 432)]
            public Int32 _432_Japanese_String_Table_Size;
            [Value(Name = "Japanese String Index Offset", Offset = 436)]
            public Int32 _436_Japanese_String_Index_Offset;
            [Value(Name = "Japanese String Table Offset", Offset = 440)]
            public Int32 _440_Japanese_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 444)]
            public Int32 _444_Unknown;
            [Value(Offset = 448)]
            public Byte[] _448_ = new byte[8];
            [Value(Name = "Dutch String Count", Offset = 456)]
            public Int32 _456_Dutch_String_Count;
            [Value(Name = "Dutch String Table Size", Offset = 460)]
            public Int32 _460_Dutch_String_Table_Size;
            [Value(Name = "Dutch String Index Offset", Offset = 464)]
            public Int32 _464_Dutch_String_Index_Offset;
            [Value(Name = "Dutch String Table Offset", Offset = 468)]
            public Int32 _468_Dutch_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 472)]
            public Int32 _472_Unknown;
            [Value(Offset = 476)]
            public Byte[] _476_ = new byte[8];
            [Value(Name = "French String Count", Offset = 484)]
            public Int32 _484_French_String_Count;
            [Value(Name = "French String Table Size", Offset = 488)]
            public Int32 _488_French_String_Table_Size;
            [Value(Name = "French String Index Offset", Offset = 492)]
            public Int32 _492_French_String_Index_Offset;
            [Value(Name = "French String Table Offset", Offset = 496)]
            public Int32 _496_French_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 500)]
            public Int32 _500_Unknown;
            [Value(Offset = 504)]
            public Byte[] _504_ = new byte[8];
            [Value(Name = "Spanish String Count", Offset = 512)]
            public Int32 _512_Spanish_String_Count;
            [Value(Name = "Spanish String Table Size", Offset = 516)]
            public Int32 _516_Spanish_String_Table_Size;
            [Value(Name = "Spanish String Index Offset", Offset = 520)]
            public Int32 _520_Spanish_String_Index_Offset;
            [Value(Name = "Spanish String Table Offset", Offset = 524)]
            public Int32 _524_Spanish_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 528)]
            public Int32 _528_Unknown;
            [Value(Offset = 532)]
            public Byte[] _532_ = new byte[8];
            [Value(Name = "Italian String Count", Offset = 540)]
            public Int32 _540_Italian_String_Count;
            [Value(Name = "Italian String Table Size", Offset = 544)]
            public Int32 _544_Italian_String_Table_Size;
            [Value(Name = "Italian String Index Offset", Offset = 548)]
            public Int32 _548_Italian_String_Index_Offset;
            [Value(Name = "Italian String Table Offset", Offset = 552)]
            public Int32 _552_Italian_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 556)]
            public Int32 _556_Unknown;
            [Value(Offset = 560)]
            public Byte[] _560_ = new byte[8];
            [Value(Name = "Korean String Count", Offset = 568)]
            public Int32 _568_Korean_String_Count;
            [Value(Name = "Korean String Table Size", Offset = 572)]
            public Int32 _572_Korean_String_Table_Size;
            [Value(Name = "Korean String Index Offset", Offset = 576)]
            public Int32 _576_Korean_String_Index_Offset;
            [Value(Name = "Korean String Table Offset", Offset = 580)]
            public Int32 _580_Korean_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 584)]
            public Int32 _584_Unknown;
            [Value(Offset = 588)]
            public Byte[] _588_ = new byte[8];
            [Value(Name = "Chinese String Count", Offset = 596)]
            public Int32 _596_Chinese_String_Count;
            [Value(Name = "Chinese String Table Size", Offset = 600)]
            public Int32 _600_Chinese_String_Table_Size;
            [Value(Name = "Chinese String Index Offset", Offset = 604)]
            public Int32 _604_Chinese_String_Index_Offset;
            [Value(Name = "Chinese String Table Offset", Offset = 608)]
            public Int32 _608_Chinese_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 612)]
            public Int32 _612_Unknown;
            [Value(Offset = 616)]
            public Byte[] _616_ = new byte[8];
            [Value(Name = "Portuguese String Count", Offset = 624)]
            public Int32 _624_Portuguese_String_Count;
            [Value(Name = "Portuguese String Table Size", Offset = 628)]
            public Int32 _628_Portuguese_String_Table_Size;
            [Value(Name = "Portuguese String Index Offset", Offset = 632)]
            public Int32 _632_Portuguese_String_Index_Offset;
            [Value(Name = "Portuguese String Table Offset", Offset = 636)]
            public Int32 _636_Portuguese_String_Table_Offset;
            [Value(Name = "Unknown", Offset = 640)]
            public Int32 _640_Unknown;
        }
    }
}