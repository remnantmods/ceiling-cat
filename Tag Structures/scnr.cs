using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable]
        [Tag("scnr")]
        public class scnr
        {
            [Value(Name = "Unused", Offset = 0)]
            public Dependancy _0_Unused;
            public _8_sky_palette[] _8_Sky_Palette;
            [Serializable]
            [Reflexive(Name = "Sky Palette", Offset = 8, ChunkSize = 8, Label = "")]
            public class _8_sky_palette
            {
                private int __StartOffset__;
                [Value(Name = "Sky", Offset = 0)]
                public Dependancy _0_Sky;
            }
            [Value(Name = "Map Type", Offset = 16)]
            public H2.DataTypes.Enum _16_Map_Type = new H2.DataTypes.Enum(new string[] { "Single Player", "Multiplayer", "Main Menu", "Shared", "Single Player Shared" }, 2);
            [Value(Name = "Local North", Offset = 18)]
            public Int16 _18_Local_North;
            [Value(Offset = 20)]
            public Byte[] _20_ = new byte[12];
            public _32_predicted_resources[] _32_Predicted_Resources;
            [Serializable]
            [Reflexive(Name = "Predicted Resources", Offset = 32, ChunkSize = 8, Label = "")]
            public class _32_predicted_resources
            {
                private int __StartOffset__;
                [Value(Name = "Bitmap Index", Offset = 0)]
                public Int32 _0_Bitmap_Index;
                [Value(Name = "Bitmap", Offset = 4)]
                public Int32 _4_Bitmap;
            }
            [Value(Offset = 40)]
            public Byte[] _40_ = new byte[32];
            public _72_script_objects[] _72_Script_Objects;
            [Serializable]
            [Reflexive(Name = "Script Objects", Offset = 72, ChunkSize = 36, Label = "")]
            public class _72_script_objects
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Type", Offset = 32)]
                public H2.DataTypes.Enum _32_Type = new H2.DataTypes.Enum(new string[] { "Biped", "Vehicle", "Weapon", "Scenery", "Machine", "Control", "Sound Scenery", "Obstacle" }, 2);
                [Value(Name = "Placement Index", Offset = 34)]
                public Int16 _34_Placement_Index;
            }
            public _80_scenery[] _80_Scenery;
            [Serializable]
            [Reflexive(Name = "Scenery", Offset = 80, ChunkSize = 92, Label = "")]
            public class _80_scenery
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Offset = 54)]
                public Byte[] _54_ = new byte[4];
                [Value(Name = "Variant Name", Offset = 58)]
                public StringID _58_Variant_Name;
                [Value(Name = "Active Change Colors", Offset = 62)]
                public Bitmask _62_Active_Change_Colors = new Bitmask(new string[] { "Primary", "Secondary", "Tertiary", "Quaternary" }, 4);
                [Value(Name = "Primary Color R", Offset = 66)]
                public Byte _66_Primary_Color_R;
                [Value(Name = "Primary Color G", Offset = 67)]
                public Byte _67_Primary_Color_G;
                [Value(Name = "Primary Color B", Offset = 68)]
                public Byte _68_Primary_Color_B;
                [Value(Name = "Secondary Color R", Offset = 69)]
                public Byte _69_Secondary_Color_R;
                [Value(Name = "Secondary Color G", Offset = 70)]
                public Byte _70_Secondary_Color_G;
                [Value(Name = "Secondary Color B", Offset = 71)]
                public Byte _71_Secondary_Color_B;
                [Value(Name = "Tertiary Color R", Offset = 72)]
                public Byte _72_Tertiary_Color_R;
                [Value(Name = "Tertiary Color G", Offset = 73)]
                public Byte _73_Tertiary_Color_G;
                [Value(Name = "Tertiary Color B", Offset = 74)]
                public Byte _74_Tertiary_Color_B;
                [Value(Name = "Quaternary Color R", Offset = 75)]
                public Byte _75_Quaternary_Color_R;
                [Value(Name = "Quaternary Color G", Offset = 76)]
                public Byte _76_Quaternary_Color_G;
                [Value(Name = "Quaternary Color B", Offset = 77)]
                public Byte _77_Quaternary_Color_B;
                [Value(Name = "Pathfinding Policy", Offset = 78)]
                public H2.DataTypes.Enum _78_Pathfinding_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Cut Out", "Static", "None" }, 1);
                [Value(Name = "Lightmapping Policy", Offset = 79)]
                public H2.DataTypes.Enum _79_Lightmapping_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Per Vertex" }, 1);
                public _80_pathfinding_references[] _80_Pathfinding_References;
                [Serializable]
                [Reflexive(Name = "Pathfinding References", Offset = 80, ChunkSize = 4, Label = "")]
                public class _80_pathfinding_references
                {
                    private int __StartOffset__;
                    [Value(Name = "BSP Index", Offset = 0)]
                    public Int16 _0_BSP_Index;
                    [Value(Name = "Pathfinding Object Index", Offset = 2)]
                    public Int16 _2_Pathfinding_Object_Index;
                }
                [Value(Name = "Valid Multiplayer Games", Offset = 88)]
                public Bitmask _88_Valid_Multiplayer_Games = new Bitmask(new string[] { "CTF", "Slayer", "Oddball", "KOTH", "Juggernaut", "Territories", "Assault" }, 4);
            }
            public _88_scenery_palette[] _88_Scenery_Palette;
            [Serializable]
            [Reflexive(Name = "Scenery Palette", Offset = 88, ChunkSize = 40, Label = "")]
            public class _88_scenery_palette
            {
                private int __StartOffset__;
                [Value(Name = "Scenery", Offset = 0)]
                public Dependancy _0_Scenery;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _96_bipeds[] _96_Bipeds;
            [Serializable]
            [Reflexive(Name = "Bipeds", Offset = 96, ChunkSize = 84, Label = "")]
            public class _96_bipeds
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Offset = 54)]
                public Byte[] _54_ = new byte[4];
                [Value(Name = "Variant Name", Offset = 58)]
                public StringID _58_Variant_Name;
                [Value(Name = "Active Change Colors", Offset = 62)]
                public Bitmask _62_Active_Change_Colors = new Bitmask(new string[] { "Primary", "Secondary", "Tertiary", "Quaternary" }, 4);
                [Value(Name = "Primary Color R", Offset = 66)]
                public Byte _66_Primary_Color_R;
                [Value(Name = "Primary Color G", Offset = 67)]
                public Byte _67_Primary_Color_G;
                [Value(Name = "Primary Color B", Offset = 68)]
                public Byte _68_Primary_Color_B;
                [Value(Name = "Secondary Color R", Offset = 69)]
                public Byte _69_Secondary_Color_R;
                [Value(Name = "Secondary Color G", Offset = 70)]
                public Byte _70_Secondary_Color_G;
                [Value(Name = "Secondary Color B", Offset = 71)]
                public Byte _71_Secondary_Color_B;
                [Value(Name = "Tertiary Color R", Offset = 72)]
                public Byte _72_Tertiary_Color_R;
                [Value(Name = "Tertiary Color G", Offset = 73)]
                public Byte _73_Tertiary_Color_G;
                [Value(Name = "Tertiary Color B", Offset = 74)]
                public Byte _74_Tertiary_Color_B;
                [Value(Name = "Quaternary Color R", Offset = 75)]
                public Byte _75_Quaternary_Color_R;
                [Value(Name = "Quaternary Color G", Offset = 76)]
                public Byte _76_Quaternary_Color_G;
                [Value(Name = "Quaternary Color B", Offset = 77)]
                public Byte _77_Quaternary_Color_B;
                [Value(Name = "Pathfinding Policy", Offset = 78)]
                public H2.DataTypes.Enum _78_Pathfinding_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Cut Out", "Static", "None" }, 1);
                [Value(Name = "Lightmapping Policy", Offset = 79)]
                public H2.DataTypes.Enum _79_Lightmapping_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Per Vertex" }, 1);
                [Value(Name = "Valid Multiplayer Games", Offset = 80)]
                public Bitmask _80_Valid_Multiplayer_Games = new Bitmask(new string[] { "CTF", "Slayer", "Oddball", "KOTH", "Juggernaut", "Territories", "Assault" }, 4);
            }
            public _104_biped_palette[] _104_Biped_Palette;
            [Serializable]
            [Reflexive(Name = "Biped Palette", Offset = 104, ChunkSize = 40, Label = "")]
            public class _104_biped_palette
            {
                private int __StartOffset__;
                [Value(Name = "Biped", Offset = 0)]
                public Dependancy _0_Biped;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _112_vehicles[] _112_Vehicles;
            [Serializable]
            [Reflexive(Name = "Vehicles", Offset = 112, ChunkSize = 84, Label = "")]
            public class _112_vehicles
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Offset = 54)]
                public Byte[] _54_ = new byte[4];
                [Value(Name = "Variant Name", Offset = 58)]
                public StringID _58_Variant_Name;
                [Value(Name = "Active Change Colors", Offset = 62)]
                public Bitmask _62_Active_Change_Colors = new Bitmask(new string[] { "Primary", "Secondary", "Tertiary", "Quaternary" }, 4);
                [Value(Name = "Primary Color R", Offset = 66)]
                public Byte _66_Primary_Color_R;
                [Value(Name = "Primary Color G", Offset = 67)]
                public Byte _67_Primary_Color_G;
                [Value(Name = "Primary Color B", Offset = 68)]
                public Byte _68_Primary_Color_B;
                [Value(Name = "Secondary Color R", Offset = 69)]
                public Byte _69_Secondary_Color_R;
                [Value(Name = "Secondary Color G", Offset = 70)]
                public Byte _70_Secondary_Color_G;
                [Value(Name = "Secondary Color B", Offset = 71)]
                public Byte _71_Secondary_Color_B;
                [Value(Name = "Tertiary Color R", Offset = 72)]
                public Byte _72_Tertiary_Color_R;
                [Value(Name = "Tertiary Color G", Offset = 73)]
                public Byte _73_Tertiary_Color_G;
                [Value(Name = "Tertiary Color B", Offset = 74)]
                public Byte _74_Tertiary_Color_B;
                [Value(Name = "Quaternary Color R", Offset = 75)]
                public Byte _75_Quaternary_Color_R;
                [Value(Name = "Quaternary Color G", Offset = 76)]
                public Byte _76_Quaternary_Color_G;
                [Value(Name = "Quaternary Color B", Offset = 77)]
                public Byte _77_Quaternary_Color_B;
                [Value(Name = "Pathfinding Policy", Offset = 78)]
                public H2.DataTypes.Enum _78_Pathfinding_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Cut Out", "Static", "None" }, 1);
                [Value(Name = "Lightmapping Policy", Offset = 79)]
                public H2.DataTypes.Enum _79_Lightmapping_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Per Vertex" }, 1);
                [Value(Name = "Valid Multiplayer Games", Offset = 80)]
                public Bitmask _80_Valid_Multiplayer_Games = new Bitmask(new string[] { "CTF", "Slayer", "Oddball", "KOTH", "Juggernaut", "Territories", "Assault" }, 4);
            }
            public _120_vehicle_palette[] _120_Vehicle_Palette;
            [Serializable]
            [Reflexive(Name = "Vehicle Palette", Offset = 120, ChunkSize = 40, Label = "")]
            public class _120_vehicle_palette
            {
                private int __StartOffset__;
                [Value(Name = "Vehicle", Offset = 0)]
                public Dependancy _0_Vehicle;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _128_equipment[] _128_Equipment;
            [Serializable]
            [Reflexive(Name = "Equipment", Offset = 128, ChunkSize = 56, Label = "")]
            public class _128_equipment
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Offset = 54)]
                public Byte[] _54_ = new byte[2];
            }
            public _136_equipment_palette[] _136_Equipment_Palette;
            [Serializable]
            [Reflexive(Name = "Equipment Palette", Offset = 136, ChunkSize = 40, Label = "")]
            public class _136_equipment_palette
            {
                private int __StartOffset__;
                [Value(Name = "Equipment", Offset = 0)]
                public Dependancy _0_Equipment;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _144_weapons[] _144_Weapons;
            [Serializable]
            [Reflexive(Name = "Weapons", Offset = 144, ChunkSize = 84, Label = "")]
            public class _144_weapons
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Offset = 54)]
                public Byte[] _54_ = new byte[4];
                [Value(Name = "Variant Name", Offset = 58)]
                public StringID _58_Variant_Name;
                [Value(Name = "Active Change Colors", Offset = 62)]
                public Bitmask _62_Active_Change_Colors = new Bitmask(new string[] { "Primary", "Secondary", "Tertiary", "Quaternary" }, 4);
                [Value(Name = "Primary Color R", Offset = 66)]
                public Byte _66_Primary_Color_R;
                [Value(Name = "Primary Color G", Offset = 67)]
                public Byte _67_Primary_Color_G;
                [Value(Name = "Primary Color B", Offset = 68)]
                public Byte _68_Primary_Color_B;
                [Value(Name = "Secondary Color R", Offset = 69)]
                public Byte _69_Secondary_Color_R;
                [Value(Name = "Secondary Color G", Offset = 70)]
                public Byte _70_Secondary_Color_G;
                [Value(Name = "Secondary Color B", Offset = 71)]
                public Byte _71_Secondary_Color_B;
                [Value(Name = "Tertiary Color R", Offset = 72)]
                public Byte _72_Tertiary_Color_R;
                [Value(Name = "Tertiary Color G", Offset = 73)]
                public Byte _73_Tertiary_Color_G;
                [Value(Name = "Tertiary Color B", Offset = 74)]
                public Byte _74_Tertiary_Color_B;
                [Value(Name = "Quaternary Color R", Offset = 75)]
                public Byte _75_Quaternary_Color_R;
                [Value(Name = "Quaternary Color G", Offset = 76)]
                public Byte _76_Quaternary_Color_G;
                [Value(Name = "Quaternary Color B", Offset = 77)]
                public Byte _77_Quaternary_Color_B;
                [Value(Name = "Pathfinding Policy", Offset = 78)]
                public H2.DataTypes.Enum _78_Pathfinding_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Cut Out", "Static", "None" }, 1);
                [Value(Name = "Lightmapping Policy", Offset = 79)]
                public H2.DataTypes.Enum _79_Lightmapping_Policy = new H2.DataTypes.Enum(new string[] { "Tag Default", "Dynamic", "Per Vertex" }, 1);
                [Value(Name = "Valid Multiplayer Games", Offset = 80)]
                public Bitmask _80_Valid_Multiplayer_Games = new Bitmask(new string[] { "CTF", "Slayer", "Oddball", "KOTH", "Juggernaut", "Territories", "Assault" }, 4);
            }
            public _152_weapon_palette[] _152_Weapon_Palette;
            [Serializable]
            [Reflexive(Name = "Weapon Palette", Offset = 152, ChunkSize = 40, Label = "")]
            public class _152_weapon_palette
            {
                private int __StartOffset__;
                [Value(Name = "Weapon", Offset = 0)]
                public Dependancy _0_Weapon;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _160_power_groups[] _160_Power_Groups;
            [Serializable]
            [Reflexive(Name = "Power Groups", Offset = 160, ChunkSize = 40, Label = "")]
            public class _160_power_groups
            {
                private int __StartOffset__;
                [Value(Name = "Device", Offset = 0)]
                public H2.DataTypes.String _0_Device = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Inital Value", Offset = 32)]
                public Single _32_Inital_Value;
                [Value(Name = "Flags", Offset = 36)]
                public Bitmask _36_Flags = new Bitmask(new string[] { "Only Use Once" }, 4);
            }
            public _168_machines[] _168_Machines;
            [Serializable]
            [Reflexive(Name = "Machines", Offset = 168, ChunkSize = 72, Label = "")]
            public class _168_machines
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Name = "Power Group Chunk #", Offset = 54)]
                public Int16 _54_Power_Group_Chunk_No;
                [Value(Name = "Position Group Chunk #", Offset = 56)]
                public Int16 _56_Position_Group_Chunk_No;
                [Value(Name = "Flags", Offset = 58)]
                public Bitmask _58_Flags = new Bitmask(new string[] { "Initially Open", "Initially Off", "Can Change Only Once", "Position Reversed", "Not Usable From Any Side" }, 4);
                [Value(Name = "Flags", Offset = 62)]
                public Bitmask _62_Flags = new Bitmask(new string[] { "Does Not Operate Automatically", "One-Sided", "Never Appears Locked", "Opened By Melee Attack", "One-Sided For Player", "Does Not Close Automatically" }, 2);
                public _64_pathfinding_references[] _64_Pathfinding_References;
                [Serializable]
                [Reflexive(Name = "Pathfinding References", Offset = 64, ChunkSize = 4, Label = "")]
                public class _64_pathfinding_references
                {
                    private int __StartOffset__;
                    [Value(Name = "BSP Index", Offset = 0)]
                    public Int16 _0_BSP_Index;
                    [Value(Name = "Pathfinding Object Index", Offset = 2)]
                    public Int16 _2_Pathfinding_Object_Index;
                }
            }
            public _176_machine_palette[] _176_Machine_Palette;
            [Serializable]
            [Reflexive(Name = "Machine Palette", Offset = 176, ChunkSize = 40, Label = "")]
            public class _176_machine_palette
            {
                private int __StartOffset__;
                [Value(Name = "Machine", Offset = 0)]
                public Dependancy _0_Machine;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _184_controls[] _184_Controls;
            [Serializable]
            [Reflexive(Name = "Controls", Offset = 184, ChunkSize = 68, Label = "")]
            public class _184_controls
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Name = "Power Group Chunk #", Offset = 54)]
                public Int16 _54_Power_Group_Chunk_No;
                [Value(Name = "Position Group Chunk #", Offset = 56)]
                public Int16 _56_Position_Group_Chunk_No;
                [Value(Name = "Flags", Offset = 58)]
                public Bitmask _58_Flags = new Bitmask(new string[] { "Initially Open", "Initially Off", "Can Change Only Once", "Position Reversed", "Not Usable From Any Side" }, 4);
                [Value(Name = "Flags", Offset = 62)]
                public Bitmask _62_Flags = new Bitmask(new string[] { "Usable From Both Sides" }, 4);
                [Value(Offset = 66)]
                public Byte[] _66_ = new byte[2];
            }
            public _192_control_palette[] _192_Control_Palette;
            [Serializable]
            [Reflexive(Name = "Control Palette", Offset = 192, ChunkSize = 40, Label = "")]
            public class _192_control_palette
            {
                private int __StartOffset__;
                [Value(Name = "Control", Offset = 0)]
                public Dependancy _0_Control;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _200_light_fixtures[] _200_Light_Fixtures;
            [Serializable]
            [Reflexive(Name = "Light Fixtures", Offset = 200, ChunkSize = 80, Label = "")]
            public class _200_light_fixtures
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Name = "Power Group Chunk #", Offset = 54)]
                public Int16 _54_Power_Group_Chunk_No;
                [Value(Name = "Position Group Chunk #", Offset = 56)]
                public Int16 _56_Position_Group_Chunk_No;
                [Value(Name = "Flags", Offset = 58)]
                public Bitmask _58_Flags = new Bitmask(new string[] { "Initially Open", "Initially Off", "Can Change Only Once", "Position Reversed", "Not Usable From Any Side" }, 4);
                [Value(Name = "Color R", Offset = 62)]
                public Int16 _62_Color_R;
                [Value(Name = "Color G", Offset = 64)]
                public Int16 _64_Color_G;
                [Value(Name = "Color B", Offset = 66)]
                public Int16 _66_Color_B;
                [Value(Name = "Intensity", Offset = 68)]
                public Single _68_Intensity;
                [Value(Name = "Falloff Angle (Degrees)", Offset = 72)]
                public Single _72_Falloff_Angle__Degrees_;
                [Value(Name = "Cutoff Angle (Degrees)", Offset = 76)]
                public Single _76_Cutoff_Angle__Degrees_;
            }
            public _208_light_fixtures_palette[] _208_Light_Fixtures_Palette;
            [Serializable]
            [Reflexive(Name = "Light Fixtures Palette", Offset = 208, ChunkSize = 40, Label = "")]
            public class _208_light_fixtures_palette
            {
                private int __StartOffset__;
                [Value(Name = "Light Fixture", Offset = 0)]
                public Dependancy _0_Light_Fixture;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _216_sound_scenery[] _216_Sound_Scenery;
            [Serializable]
            [Reflexive(Name = "Sound Scenery", Offset = 216, ChunkSize = 80, Label = "")]
            public class _216_sound_scenery
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Name = "Volume Type", Offset = 54)]
                public H2.DataTypes.Enum _54_Volume_Type = new H2.DataTypes.Enum(new string[] { "Sphere", "Vertical Cylinder" }, 2);
                [Value(Name = "Height", Offset = 56)]
                public Single _56_Height;
                [Value(Name = "Override Distance Bounds Lower", Offset = 60)]
                public Single _60_Override_Distance_Bounds_Lower;
                [Value(Name = "Override Distance Bounds Upper", Offset = 64)]
                public Single _64_Override_Distance_Bounds_Upper;
                [Value(Name = "Override Cone Angle Bounds Lower", Offset = 68)]
                public Single _68_Override_Cone_Angle_Bounds_Lower;
                [Value(Name = "Override Cone Angle Bounds Upper", Offset = 72)]
                public Single _72_Override_Cone_Angle_Bounds_Upper;
                [Value(Name = "Override Outer Cone Gain", Offset = 76)]
                public Single _76_Override_Outer_Cone_Gain;
            }
            public _224_sound_scenery_palette[] _224_Sound_Scenery_Palette;
            [Serializable]
            [Reflexive(Name = "Sound Scenery Palette", Offset = 224, ChunkSize = 40, Label = "")]
            public class _224_sound_scenery_palette
            {
                private int __StartOffset__;
                [Value(Name = "Sound Scenery", Offset = 0)]
                public Dependancy _0_Sound_Scenery;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _232_light_volumes_placement[] _232_Light_Volumes_Placement;
            [Serializable]
            [Reflexive(Name = "Light Volumes Placement", Offset = 232, ChunkSize = 108, Label = "")]
            public class _232_light_volumes_placement
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Name = "Power Group Chunk #", Offset = 54)]
                public Int16 _54_Power_Group_Chunk_No;
                [Value(Name = "Position Group Chunk #", Offset = 56)]
                public Int16 _56_Position_Group_Chunk_No;
                [Value(Name = "Flags", Offset = 58)]
                public Bitmask _58_Flags = new Bitmask(new string[] { "Initially Open", "Initially Off", "Can Change Only Once", "Position Reversed", "Not Usable From Any Side" }, 4);
                [Value(Name = "Type", Offset = 62)]
                public H2.DataTypes.Enum _62_Type = new H2.DataTypes.Enum(new string[] { "Sphere", "Orthogonal", "Projective", "Pyramid" }, 2);
                [Value(Name = "Flags", Offset = 64)]
                public Bitmask _64_Flags = new Bitmask(new string[] { "Custom Geometry", "unused", "Cinematic Only" }, 1);
                [Value(Name = "Lightmap Type", Offset = 65)]
                public H2.DataTypes.Enum _65_Lightmap_Type = new H2.DataTypes.Enum(new string[] { "Use Light Tag Setting", "Dynamic Only", "Dynamic Lightmaps", "Lightmaps Only" }, 2);
                [Value(Name = "Lightmap Flags", Offset = 67)]
                public Bitmask _67_Lightmap_Flags = new Bitmask(new string[] { "Unused" }, 1);
                [Value(Name = "Lightmap Half Light Scale", Offset = 68)]
                public Single _68_Lightmap_Half_Light_Scale;
                [Value(Name = "Lightmap Light Scale", Offset = 72)]
                public Single _72_Lightmap_Light_Scale;
                [Value(Name = "Target Point X", Offset = 76)]
                public Single _76_Target_Point_X;
                [Value(Name = "Target Point Y", Offset = 80)]
                public Single _80_Target_Point_Y;
                [Value(Name = "Target Point Z", Offset = 84)]
                public Single _84_Target_Point_Z;
                [Value(Name = "Width", Offset = 88)]
                public Single _88_Width;
                [Value(Name = "Height Scale", Offset = 92)]
                public Single _92_Height_Scale;
                [Value(Name = "Field Of View", Offset = 96)]
                public Single _96_Field_Of_View;
                [Value(Name = "Falloff Distance", Offset = 100)]
                public Single _100_Falloff_Distance;
                [Value(Name = "Cutoff Distance", Offset = 104)]
                public Single _104_Cutoff_Distance;
            }
            public _240_light_volumes_palette[] _240_Light_Volumes_Palette;
            [Serializable]
            [Reflexive(Name = "Light Volumes Palette", Offset = 240, ChunkSize = 40, Label = "")]
            public class _240_light_volumes_palette
            {
                private int __StartOffset__;
                [Value(Name = "Light Volume", Offset = 0)]
                public Dependancy _0_Light_Volume;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            public _248_player_starting_profile[] _248_Player_Starting_Profile;
            [Serializable]
            [Reflexive(Name = "Player Starting Profile", Offset = 248, ChunkSize = 68, Label = "")]
            public class _248_player_starting_profile
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Starting Health Damage", Offset = 32)]
                public Single _32_Starting_Health_Damage;
                [Value(Name = "Starting Shield Damage", Offset = 36)]
                public Single _36_Starting_Shield_Damage;
                [Value(Name = "Primary Weapon", Offset = 40)]
                public Dependancy _40_Primary_Weapon;
                [Value(Name = "Rounds loaded", Offset = 48)]
                public Int16 _48_Rounds_loaded;
                [Value(Name = "Rounds Total", Offset = 50)]
                public Int16 _50_Rounds_Total;
                [Value(Name = "Secondary Weapon", Offset = 52)]
                public Dependancy _52_Secondary_Weapon;
                [Value(Name = "Rounds loaded", Offset = 60)]
                public Int16 _60_Rounds_loaded;
                [Value(Name = "Rounds Total", Offset = 62)]
                public Int16 _62_Rounds_Total;
                [Value(Name = "Starting Fragmentation Grenade Count", Offset = 64)]
                public Byte _64_Starting_Fragmentation_Grenade_Count;
                [Value(Name = "Starting Plasma Grenade Count", Offset = 65)]
                public Byte _65_Starting_Plasma_Grenade_Count;
                [Value(Name = "Starting Type3 Grenade Count", Offset = 66)]
                public Byte _66_Starting_Type3_Grenade_Count;
                [Value(Name = "Starting Type4 Grenade Count", Offset = 67)]
                public Byte _67_Starting_Type4_Grenade_Count;
            }
            public _256_player_starting_locations[] _256_Player_Starting_Locations;
            [Serializable]
            [Reflexive(Name = "Player Starting Locations", Offset = 256, ChunkSize = 52, Label = "")]
            public class _256_player_starting_locations
            {
                private int __StartOffset__;
                [Value(Name = "Position X", Offset = 0)]
                public Single _0_Position_X;
                [Value(Name = "Position Y", Offset = 4)]
                public Single _4_Position_Y;
                [Value(Name = "Position Z", Offset = 8)]
                public Single _8_Position_Z;
                [Value(Name = "Facing Degrees", Offset = 12)]
                public Single _12_Facing_Degrees;
                [Value(Name = "Team Designator", Offset = 16)]
                public H2.DataTypes.Enum _16_Team_Designator = new H2.DataTypes.Enum(new string[] { "Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Brown", "Pink", "Neutral" }, 2);
                [Value(Name = "BSP Index", Offset = 18)]
                public Int16 _18_BSP_Index;
                [Value(Name = "Game Type 1", Offset = 20)]
                public H2.DataTypes.Enum _20_Game_Type_1 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 2", Offset = 22)]
                public H2.DataTypes.Enum _22_Game_Type_2 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 3", Offset = 24)]
                public H2.DataTypes.Enum _24_Game_Type_3 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 4", Offset = 26)]
                public H2.DataTypes.Enum _26_Game_Type_4 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Spawn Type 1", Offset = 28)]
                public H2.DataTypes.Enum _28_Spawn_Type_1 = new H2.DataTypes.Enum(new string[] { "Both", "Initial Only", "Respawn Only" }, 2);
                [Value(Name = "Spawn Type 2", Offset = 30)]
                public H2.DataTypes.Enum _30_Spawn_Type_2 = new H2.DataTypes.Enum(new string[] { "Both", "Initial Only", "Respawn Only" }, 2);
                [Value(Name = "Spawn Type 3", Offset = 32)]
                public H2.DataTypes.Enum _32_Spawn_Type_3 = new H2.DataTypes.Enum(new string[] { "Both", "Initial Only", "Respawn Only" }, 2);
                [Value(Name = "Spawn Type 4", Offset = 34)]
                public H2.DataTypes.Enum _34_Spawn_Type_4 = new H2.DataTypes.Enum(new string[] { "Both", "Initial Only", "Respawn Only" }, 2);
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[8];
                [Value(Name = "Campaign Player Type", Offset = 44)]
                public H2.DataTypes.Enum _44_Campaign_Player_Type = new H2.DataTypes.Enum(new string[] { "Type0 Masterchief", "Type1 Dervish", "Type2 Chief Multiplayer", "Type3 Elite Multiplayer" }, 2);
                [Value(Offset = 46)]
                public Byte[] _46_ = new byte[6];
            }
            public _264_trigger_volumes[] _264_Trigger_Volumes;
            [Serializable]
            [Reflexive(Name = "Trigger Volumes", Offset = 264, ChunkSize = 68, Label = "")]
            public class _264_trigger_volumes
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Name = "Object Name", Offset = 4)]
                public Int16 _4_Object_Name;
                [Value(Offset = 6)]
                public Byte[] _6_ = new byte[2];
                [Value(Name = "Node Name", Offset = 8)]
                public StringID _8_Node_Name;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[28];
                [Value(Name = "Position X", Offset = 40)]
                public Single _40_Position_X;
                [Value(Name = "Position Y", Offset = 44)]
                public Single _44_Position_Y;
                [Value(Name = "Position Z", Offset = 48)]
                public Single _48_Position_Z;
                [Value(Name = "Extents X", Offset = 52)]
                public Single _52_Extents_X;
                [Value(Name = "Extents Y", Offset = 56)]
                public Single _56_Extents_Y;
                [Value(Name = "Extents Z", Offset = 60)]
                public Single _60_Extents_Z;
                [Value(Offset = 64)]
                public Byte[] _64_ = new byte[4];
            }
            [Value(Offset = 272)]
            public Byte[] _272_ = new byte[8];
            public _280_netgame_flags[] _280_Netgame_Flags;
            [Serializable]
            [Reflexive(Name = "Netgame Flags", Offset = 280, ChunkSize = 32, Label = "")]
            public class _280_netgame_flags
            {
                private int __StartOffset__;
                [Value(Name = "Position X", Offset = 0)]
                public Single _0_Position_X;
                [Value(Name = "Position Y", Offset = 4)]
                public Single _4_Position_Y;
                [Value(Name = "Position Z", Offset = 8)]
                public Single _8_Position_Z;
                [Value(Name = "Facing Degrees", Offset = 12)]
                public Single _12_Facing_Degrees;
                [Value(Name = "Type", Offset = 16)]
                public H2.DataTypes.Enum _16_Type = new H2.DataTypes.Enum(new string[] { "CTF Flag Spawn", "CTF Flag Return", "Assault Bomb Spawn", "Assault Bomb Return", "Oddball Spawn", "unused", "Race Checkpoint", "Teleporter Source", "Teleporter Destination", "Headhunter Bin", "Territories Flag", "King Hill 0", "King Hill 1 vertex", "King Hill 2 vertex", "King Hill 3 vertex", "King Hill 4 vertex", "King Hill 5 vertex", "King Hill 6 vertex", "King Hill 7 vertex" }, 2);
                [Value(Name = "Team Designator", Offset = 18)]
                public H2.DataTypes.Enum _18_Team_Designator = new H2.DataTypes.Enum(new string[] { "Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Brown", "Pink", "Neutral" }, 2);
                [Value(Name = "Identifier", Offset = 20)]
                public Int16 _20_Identifier;
                [Value(Name = "Flags", Offset = 22)]
                public Bitmask _22_Flags = new Bitmask(new string[] { "Multiple Flag Bomb", "Single Flag Bomb", "Neutral Flag Bomb" }, 2);
                [Value(Offset = 24)]
                public Byte[] _24_ = new byte[8];
            }
            public _288_netgame_equipment[] _288_Netgame_Equipment;
            [Serializable]
            [Reflexive(Name = "Netgame Equipment", Offset = 288, ChunkSize = 144, Label = "")]
            public class _288_netgame_equipment
            {
                private int __StartOffset__;
                [Value(Name = "Flags", Offset = 0)]
                public Bitmask _0_Flags = new Bitmask(new string[] { "Levitate", "Destroy Existing On New Spawn" }, 4);
                [Value(Name = "Game Type 1", Offset = 4)]
                public H2.DataTypes.Enum _4_Game_Type_1 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 2", Offset = 6)]
                public H2.DataTypes.Enum _6_Game_Type_2 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 3", Offset = 8)]
                public H2.DataTypes.Enum _8_Game_Type_3 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 4", Offset = 10)]
                public H2.DataTypes.Enum _10_Game_Type_4 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Team Index", Offset = 12)]
                public Int16 _12_Team_Index;
                [Value(Name = "Spawn Time Seconds", Offset = 14)]
                public Int16 _14_Spawn_Time_Seconds;
                [Value(Name = "Respawn on Empty Time", Offset = 16)]
                public Int16 _16_Respawn_on_Empty_Time;
                [Value(Name = "Respawn Timer Starts", Offset = 18)]
                public H2.DataTypes.Enum _18_Respawn_Timer_Starts = new H2.DataTypes.Enum(new string[] { "On Pick Up", "On Body Depletion" }, 2);
                [Value(Name = "Classifier", Offset = 20)]
                public H2.DataTypes.Enum _20_Classifier = new H2.DataTypes.Enum(new string[] { "Weapon", "Primary Light Land", "Secondary Light Land", "Primary Heavy Land", "Primary Flying", "Seconary Heavy Land", "Primary Turret", "Secondary Turret", "Grenade", "Powerup" }, 4);
                [Value(Offset = 24)]
                public Byte[] _24_ = new byte[40];
                [Value(Name = "Position X", Offset = 64)]
                public Single _64_Position_X;
                [Value(Name = "Position Y", Offset = 68)]
                public Single _68_Position_Y;
                [Value(Name = "Position Z", Offset = 72)]
                public Single _72_Position_Z;
                [Value(Name = "Orientation Yaw", Offset = 76)]
                public Single _76_Orientation_Yaw;
                [Value(Name = "Orientation Pitch", Offset = 80)]
                public Single _80_Orientation_Pitch;
                [Value(Name = "Orientation Roll", Offset = 84)]
                public Single _84_Orientation_Roll;
                [Value(Name = "Collection", Offset = 88)]
                public Dependancy _88_Collection;
                [Value(Offset = 96)]
                public Byte[] _96_ = new byte[48];
            }
            public _296_starting_equipment[] _296_Starting_Equipment;
            [Serializable]
            [Reflexive(Name = "Starting Equipment", Offset = 296, ChunkSize = 156, Label = "")]
            public class _296_starting_equipment
            {
                private int __StartOffset__;
                [Value(Name = "Starting Grenades", Offset = 0)]
                public Bitmask _0_Starting_Grenades = new Bitmask(new string[] { "No Grenades", "Plasma Grenades" }, 4);
                [Value(Name = "Game Type 1", Offset = 4)]
                public H2.DataTypes.Enum _4_Game_Type_1 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 2", Offset = 6)]
                public H2.DataTypes.Enum _6_Game_Type_2 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 3", Offset = 8)]
                public H2.DataTypes.Enum _8_Game_Type_3 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Name = "Game Type 4", Offset = 10)]
                public H2.DataTypes.Enum _10_Game_Type_4 = new H2.DataTypes.Enum(new string[] { "None", "Capture The Flag", "Slayer", "Oddball", "King Of The Hill", "Race", "Juggernaught", "Territories", "Assault", "All Team Game Types", "", "", "All Game Types", "All Except CTF" }, 2);
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[48];
                [Value(Name = "Item Collection 1", Offset = 60)]
                public Dependancy _60_Item_Collection_1;
                [Value(Name = "Item Collection 2", Offset = 68)]
                public Dependancy _68_Item_Collection_2;
                [Value(Name = "Item Collection 3", Offset = 76)]
                public Dependancy _76_Item_Collection_3;
                [Value(Name = "Item Collection 4", Offset = 84)]
                public Dependancy _84_Item_Collection_4;
                [Value(Name = "Item Collection 5", Offset = 92)]
                public Dependancy _92_Item_Collection_5;
                [Value(Name = "Item Collection 6", Offset = 100)]
                public Dependancy _100_Item_Collection_6;
                [Value(Offset = 108)]
                public Byte[] _108_ = new byte[48];
            }
            public _304_bsp_switch_trigger_volumes[] _304_BSP_Switch_Trigger_Volumes;
            [Serializable]
            [Reflexive(Name = "BSP Switch Trigger Volumes", Offset = 304, ChunkSize = 14, Label = "")]
            public class _304_bsp_switch_trigger_volumes
            {
                private int __StartOffset__;
                [Value(Name = "Trigger Volume", Offset = 0)]
                public Int16 _0_Trigger_Volume;
                [Value(Name = "Source BSP", Offset = 2)]
                public Int16 _2_Source_BSP;
                [Value(Name = "Destination BSP", Offset = 4)]
                public Int16 _4_Destination_BSP;
                [Value(Offset = 6)]
                public Byte[] _6_ = new byte[8];
            }
            public _312_decals[] _312_Decals;
            [Serializable]
            [Reflexive(Name = "Decals", Offset = 312, ChunkSize = 16, Label = "")]
            public class _312_decals
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Yaw", Offset = 2)]
                public Byte _2_Yaw;
                [Value(Name = "Pitch", Offset = 3)]
                public Byte _3_Pitch;
                [Value(Name = "Position X", Offset = 4)]
                public Single _4_Position_X;
                [Value(Name = "Position Y", Offset = 8)]
                public Single _8_Position_Y;
                [Value(Name = "Position Z", Offset = 12)]
                public Single _12_Position_Z;
            }
            public _320_decal_palette[] _320_Decal_Palette;
            [Serializable]
            [Reflexive(Name = "Decal Palette", Offset = 320, ChunkSize = 8, Label = "")]
            public class _320_decal_palette
            {
                private int __StartOffset__;
                [Value(Name = "Decal", Offset = 0)]
                public Dependancy _0_Decal;
            }
            public _328_detail_object_collection_palette[] _328_Detail_Object_Collection_Palette;
            [Serializable]
            [Reflexive(Name = "Detail Object Collection Palette", Offset = 328, ChunkSize = 8, Label = "")]
            public class _328_detail_object_collection_palette
            {
                private int __StartOffset__;
                [Value(Name = "Detail Object Collection", Offset = 0)]
                public Dependancy _0_Detail_Object_Collection;
            }
            public _336_actor_palette[] _336_Actor_Palette;
            [Serializable]
            [Reflexive(Name = "Actor Palette", Offset = 336, ChunkSize = 8, Label = "")]
            public class _336_actor_palette
            {
                private int __StartOffset__;
                [Value(Name = "Actor", Offset = 0)]
                public Dependancy _0_Actor;
            }
            public _344_ai_squad_types[] _344_AI_Squad_Types;
            [Serializable]
            [Reflexive(Name = "AI Squad Types", Offset = 344, ChunkSize = 36, Label = "")]
            public class _344_ai_squad_types
            {
                private int __StartOffset__;
                [Value(Name = "Squad Type", Offset = 0)]
                public H2.DataTypes.String _0_Squad_Type = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Unknown", Offset = 32)]
                public Int32 _32_Unknown;
            }
            public _352_ai_squads[] _352_AI_Squads;
            [Serializable]
            [Reflexive(Name = "AI Squads", Offset = 352, ChunkSize = 116, Label = "")]
            public class _352_ai_squads
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Flags", Offset = 32)]
                public Bitmask _32_Flags = new Bitmask(new string[] { "Not Initially Created", "Respawn Enabled", "Initially Blind", "Initially Deaf", "Initially Braindead", "3D Firirng Positions", "Manual BSP Index Specified", "MP Respawn Enabled" }, 4);
                [Value(Name = "Team Designator", Offset = 36)]
                public H2.DataTypes.Enum _36_Team_Designator = new H2.DataTypes.Enum(new string[] { "Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Brown", "Pink", "Neutral" }, 2);
                [Value(Name = "Platoons", Offset = 38)]
                public Int16 _38_Platoons;
                [Value(Name = "Unknown", Offset = 40)]
                public Int16 _40_Unknown;
                [Value(Name = "Unknown", Offset = 42)]
                public Int16 _42_Unknown;
                [Value(Name = "# of AI that spawn on Easy", Offset = 44)]
                public Int16 _44_No_of_AI_that_spawn_on_Easy;
                [Value(Name = "# of AI that spawn on Legendary", Offset = 46)]
                public Int16 _46_No_of_AI_that_spawn_on_Legendary;
                [Value(Name = "Vehicle Spawn", Offset = 48)]
                public H2.DataTypes.Enum _48_Vehicle_Spawn = new H2.DataTypes.Enum(new string[] { "Nulled", "Spawn In Passenger", "Spawn In Gunner", "Spawn In Driver", "Spawn Out Of Vehicle", "Spawn Vehicle Only", "Spawn In Passenger" }, 2);
                [Value(Name = "Unknown", Offset = 50)]
                public Int16 _50_Unknown;
                [Value(Name = "Vehicle", Offset = 52)]
                public Int16 _52_Vehicle;
                [Value(Name = "Character", Offset = 54)]
                public Int16 _54_Character;
                [Value(Name = "Initial State", Offset = 56)]
                public H2.DataTypes.Enum _56_Initial_State = new H2.DataTypes.Enum(new string[] { "None", "Sleeping", "Alert", "Moving-Repeat Same Position", "Moving-Loop", "Moving-Loop Back And Forth", "Moving-Loop Randomly", "Moving-Randomly", "Guarding", "Guarding At Guard Position", "Searching", "Fleeing" }, 2);
                [Value(Name = "Return State", Offset = 58)]
                public H2.DataTypes.Enum _58_Return_State = new H2.DataTypes.Enum(new string[] { "None", "Sleeping", "Alert", "Moving-Repeat Same Position", "Moving-Loop", "Moving-Loop Back And Forth", "Moving-Loop Randomly", "Moving-Randomly", "Guarding", "Guarding At Guard Position", "Searching", "Fleeing" }, 2);
                [Value(Name = "Primary Weapon", Offset = 60)]
                public Int16 _60_Primary_Weapon;
                [Value(Name = "Secondary Weapon", Offset = 62)]
                public Int16 _62_Secondary_Weapon;
                [Value(Name = "Grenade Type", Offset = 64)]
                public H2.DataTypes.Enum _64_Grenade_Type = new H2.DataTypes.Enum(new string[] { "None", "Frag", "Plasma" }, 2);
                [Value(Name = "AI Orders Index", Offset = 66)]
                public Int16 _66_AI_Orders_Index;
                [Value(Name = "Vehicle Permutation", Offset = 68)]
                public StringID _68_Vehicle_Permutation;
                public _72_starting_loctations[] _72_Starting_Loctations;
                [Serializable]
                [Reflexive(Name = "Starting Loctations", Offset = 72, ChunkSize = 100, Label = "")]
                public class _72_starting_loctations
                {
                    private int __StartOffset__;
                    [Value(Name = "Name", Offset = 0)]
                    public StringID _0_Name;
                    [Value(Name = "X", Offset = 4)]
                    public Single _4_X;
                    [Value(Name = "Y", Offset = 8)]
                    public Single _8_Y;
                    [Value(Name = "Z", Offset = 12)]
                    public Single _12_Z;
                    [Value(Name = "Unknown", Offset = 16)]
                    public Int16 _16_Unknown;
                    [Value(Name = "Unknown", Offset = 18)]
                    public Int16 _18_Unknown;
                    [Value(Name = "Facing Direction", Offset = 20)]
                    public Single _20_Facing_Direction;
                    [Value(Name = "Unknown", Offset = 24)]
                    public Int16 _24_Unknown;
                    [Value(Name = "Unknown", Offset = 26)]
                    public Int16 _26_Unknown;
                    [Value(Name = "Unknown", Offset = 28)]
                    public Int16 _28_Unknown;
                    [Value(Name = "Unknown", Offset = 30)]
                    public Int16 _30_Unknown;
                    [Value(Name = "Character", Offset = 32)]
                    public Int16 _32_Character;
                    [Value(Name = "Primary Weapon", Offset = 34)]
                    public Int16 _34_Primary_Weapon;
                    [Value(Name = "Secondary Weapon", Offset = 36)]
                    public Int16 _36_Secondary_Weapon;
                    [Value(Name = "Unknown", Offset = 38)]
                    public Int16 _38_Unknown;
                    [Value(Name = "Vehicle", Offset = 40)]
                    public Int16 _40_Vehicle;
                    [Value(Name = "Vehicle Spawn", Offset = 42)]
                    public H2.DataTypes.Enum _42_Vehicle_Spawn = new H2.DataTypes.Enum(new string[] { "Back Gunner?", "Spawn In Passenger", "Spawn In Gunner", "Spawn In Driver", "Spawn Out Of Vehicle", "Spawn Vehicle Only", "Spawn In Passenger" }, 2);
                    [Value(Offset = 44)]
                    public Byte[] _44_ = new byte[4];
                    [Value(Name = "Spawned Permutation", Offset = 48)]
                    public StringID _48_Spawned_Permutation;
                    [Value(Name = "Spawned Properties", Offset = 52)]
                    public StringID _52_Spawned_Properties;
                    [Value(Offset = 56)]
                    public Byte[] _56_ = new byte[4];
                    [Value(Name = "Unknown", Offset = 60)]
                    public Int16 _60_Unknown;
                    [Value(Name = "Unknown", Offset = 62)]
                    public Int16 _62_Unknown;
                    [Value(Name = "Unknown", Offset = 64)]
                    public H2.DataTypes.String _64_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "(440)index", Offset = 96)]
                    public Int16 _96__440_index;
                    [Value(Name = "unknown", Offset = 98)]
                    public Int16 _98_unknown;
                }
                [Value(Name = "Unknown", Offset = 80)]
                public H2.DataTypes.String _80_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "(440)index", Offset = 112)]
                public Int16 _112__440_index;
                [Value(Name = "Unknown", Offset = 114)]
                public Int16 _114_Unknown;
            }
            public _360_command_lists[] _360_Command_Lists;
            [Serializable]
            [Reflexive(Name = "Command Lists", Offset = 360, ChunkSize = 56, Label = "")]
            public class _360_command_lists
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Flags", Offset = 32)]
                public Bitmask _32_Flags = new Bitmask(new string[] { "Allow Initiative", "Allow Targeting", "Disable Looking", "Disable Communication", "Disable Falling Damage", "Manual BSP Index" }, 4);
                [Value(Name = "Manual BSP Index", Offset = 36)]
                public Int16 _36_Manual_BSP_Index;
                [Value(Offset = 38)]
                public Byte[] _38_ = new byte[2];
                public _40_commands[] _40_Commands;
                [Serializable]
                [Reflexive(Name = "Commands", Offset = 40, ChunkSize = 32, Label = "")]
                public class _40_commands
                {
                    private int __StartOffset__;
                    [Value(Name = "Atom Type", Offset = 0)]
                    public H2.DataTypes.Enum _0_Atom_Type = new H2.DataTypes.Enum(new string[] { }, 4);
                    [Value(Name = "Atom Modifier", Offset = 4)]
                    public Int16 _4_Atom_Modifier;
                    [Value(Offset = 6)]
                    public Byte[] _6_ = new byte[2];
                    [Value(Name = "Parameter1", Offset = 8)]
                    public Single _8_Parameter1;
                    [Value(Name = "Parameter2", Offset = 12)]
                    public Single _12_Parameter2;
                    [Value(Name = "Point 1", Offset = 16)]
                    public Int16 _16_Point_1;
                    [Value(Name = "Point 2", Offset = 18)]
                    public Int16 _18_Point_2;
                    [Value(Name = "Animation", Offset = 20)]
                    public Int16 _20_Animation;
                    [Value(Name = "Script", Offset = 22)]
                    public Int16 _22_Script;
                    [Value(Name = "Recording", Offset = 24)]
                    public Int16 _24_Recording;
                    [Value(Name = "Command", Offset = 26)]
                    public Int16 _26_Command;
                    [Value(Name = "Object Name", Offset = 28)]
                    public Int16 _28_Object_Name;
                    [Value(Offset = 30)]
                    public Byte[] _30_ = new byte[2];
                }
                public _48_points[] _48_Points;
                [Serializable]
                [Reflexive(Name = "Points", Offset = 48, ChunkSize = 136, Label = "")]
                public class _48_points
                {
                    private int __StartOffset__;
                    [Value(Name = "Name", Offset = 0)]
                    public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Position X", Offset = 32)]
                    public Single _32_Position_X;
                    [Value(Name = "Position Y", Offset = 36)]
                    public Single _36_Position_Y;
                    [Value(Name = "Position Z", Offset = 40)]
                    public Single _40_Position_Z;
                    [Value(Name = "Unknown", Offset = 44)]
                    public Single _44_Unknown;
                    [Value(Name = "Unknown", Offset = 48)]
                    public Int32 _48_Unknown;
                    [Value(Name = "Unknown", Offset = 52)]
                    public Single _52_Unknown;
                    [Value(Name = "Unknown", Offset = 56)]
                    public Int32 _56_Unknown;
                    [Value(Name = "Unknown", Offset = 60)]
                    public Int32 _60_Unknown;
                    [Value(Name = "Unknown", Offset = 64)]
                    public Int32 _64_Unknown;
                    [Value(Name = "Unknown", Offset = 68)]
                    public Int32 _68_Unknown;
                    [Value(Name = "Unknown", Offset = 72)]
                    public Int32 _72_Unknown;
                    [Value(Name = "Unknown", Offset = 76)]
                    public Int32 _76_Unknown;
                    [Value(Offset = 80)]
                    public Byte[] _80_ = new byte[48];
                    public _128_unknown[] _128_Unknown;
                    [Serializable]
                    [Reflexive(Name = "Unknown", Offset = 128, ChunkSize = 4, Label = "")]
                    public class _128_unknown
                    {
                        private int __StartOffset__;
                        [Value(Name = "Unknown", Offset = 0)]
                        public Int32 _0_Unknown;
                    }
                }
            }
            public _368_scene_triggers_[] _368_Scene_triggers_;
            [Serializable]
            [Reflexive(Name = "Scene triggers?", Offset = 368, ChunkSize = 24, Label = "")]
            public class _368_scene_triggers_
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Offset = 4)]
                public Byte[] _4_ = new byte[4];
                public _8_unknown[] _8_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 8, ChunkSize = 12, Label = "")]
                public class _8_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                    public _4_unknown[] _4_Unknown;
                    [Serializable]
                    [Reflexive(Name = "Unknown", Offset = 4, ChunkSize = 8, Label = "")]
                    public class _4_unknown
                    {
                        private int __StartOffset__;
                        [Value(Name = "Unknown", Offset = 0)]
                        public Int32 _0_Unknown;
                        [Value(Name = "Unknown", Offset = 4)]
                        public Int32 _4_Unknown;
                    }
                }
                public _16_unknown[] _16_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 16, ChunkSize = 16, Label = "")]
                public class _16_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public StringID _0_Unknown;
                    [Value(Offset = 4)]
                    public Byte[] _4_ = new byte[4];
                    public _8_unknown[] _8_Unknown;
                    [Serializable]
                    [Reflexive(Name = "Unknown", Offset = 8, ChunkSize = 4, Label = "")]
                    public class _8_unknown
                    {
                        private int __StartOffset__;
                        [Value(Name = "Base permutation", Offset = 0)]
                        public StringID _0_Base_permutation;
                    }
                }
            }
            public _376_characters[] _376_Characters;
            [Serializable]
            [Reflexive(Name = "Characters", Offset = 376, ChunkSize = 8, Label = "")]
            public class _376_characters
            {
                private int __StartOffset__;
                [Value(Name = "Character", Offset = 0)]
                public Dependancy _0_Character;
            }
            [Value(Offset = 384)]
            public Byte[] _384_ = new byte[40];
            public _424_script_syntax_data[] _424_Script_Syntax_Data;
            [Serializable]
            [Reflexive(Name = "Script Syntax Data", Offset = 424, ChunkSize = 1, Label = "")]
            public class _424_script_syntax_data
            {
                private int __StartOffset__;
                [Value(Name = "Data", Offset = 0)]
                public Byte _0_Data;
            }
            public _432_script_string_data[] _432_Script_String_Data;
            [Serializable]
            [Reflexive(Name = "Script String Data", Offset = 432, ChunkSize = 1, Label = "")]
            public class _432_script_string_data
            {
                private int __StartOffset__;
                [Value(Name = "Character", Offset = 0)]
                public Byte _0_Character;
            }
            public _440_scripts[] _440_Scripts;
            [Serializable]
            [Reflexive(Name = "Scripts", Offset = 440, ChunkSize = 40, Label = "")]
            public class _440_scripts
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Script Type", Offset = 32)]
                public H2.DataTypes.Enum _32_Script_Type = new H2.DataTypes.Enum(new string[] { "Startup", "Dormant", "Continuous", "Static", "Stub" }, 2);
                [Value(Name = "Return Type", Offset = 34)]
                public H2.DataTypes.Enum _34_Return_Type = new H2.DataTypes.Enum(new string[] { "Unparsed", "Special Form", "Function Name", "Passthrough", "Viod", "Boolean", "Real", "Short", "Long", "String", "Script", "Trigger Volume", "Cutscene Flag", "Cutscene Camera Point", "Cutscene Title", "Cutscene Recording", "Device Group", "AI", "AI Command List", "Starting Profile", "Conversation", "Navpoint", "Hud Message", "Object List", "Sound", "Effect", "Damage", "Looping Sound", "Animation Graph", "Actor Variant", "Dammge Effect", "Object Definition", "Game Difficulty", "Team", "AI Default State", "Actor Type", "Hud Corner", "Object", "Unit", "Vehicle", "Weapon", "Device", "Scenery", "Object Name", "Unit Name", "Vehicle Name", "Weapon Name", "Device Name", "Scenery Name" }, 2);
                [Value(Name = "Root Expression Index", Offset = 36)]
                public Int32 _36_Root_Expression_Index;
            }
            public _448_globals[] _448_Globals;
            [Serializable]
            [Reflexive(Name = "Globals", Offset = 448, ChunkSize = 40, Label = "")]
            public class _448_globals
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Type", Offset = 32)]
                public H2.DataTypes.Enum _32_Type = new H2.DataTypes.Enum(new string[] { "Startup", "Dormant", "Continuous", "Static", "Stub" }, 2);
                [Value(Name = "Return Type", Offset = 34)]
                public H2.DataTypes.Enum _34_Return_Type = new H2.DataTypes.Enum(new string[] { "Unparsed", "Special Form", "Function Name", "Passthrough", "Viod", "Boolean", "Real", "Short", "Long", "String", "Script", "Trigger Volume", "Cutscene Flag", "Cutscene Camera Point", "Cutscene Title", "Cutscene Recording", "Device Group", "AI", "AI Command List", "Starting Profile", "Conversation", "Navpoint", "Hud Message", "Object List", "Sound", "Effect", "Damage", "Looping Sound", "Animation Graph", "Actor Variant", "Dammge Effect", "Object Definition", "Game Difficulty", "Team", "AI Default State", "Actor Type", "Hud Corner", "Object", "Unit", "Vehicle", "Weapon", "Device", "Scenery", "Object Name", "Unit Name", "Vehicle Name", "Weapon Name", "Device Name", "Scenery Name" }, 2);
                [Value(Name = "Initialization Expression Index", Offset = 36)]
                public Int32 _36_Initialization_Expression_Index;
            }
            public _456_references[] _456_References;
            [Serializable]
            [Reflexive(Name = "References", Offset = 456, ChunkSize = 8, Label = "")]
            public class _456_references
            {
                private int __StartOffset__;
                [Value(Name = "Reference", Offset = 0)]
                public Dependancy _0_Reference;
            }
            [Value(Offset = 464)]
            public Byte[] _464_ = new byte[8];
            public _472_script_locations[] _472_Script_Locations;
            [Serializable]
            [Reflexive(Name = "Script Locations", Offset = 472, ChunkSize = 128, Label = "")]
            public class _472_script_locations
            {
                private int __StartOffset__;
                public _0_groups[] _0_Groups;
                [Serializable]
                [Reflexive(Name = "Groups", Offset = 0, ChunkSize = 48, Label = "")]
                public class _0_groups
                {
                    private int __StartOffset__;
                    [Value(Name = "Group name", Offset = 0)]
                    public H2.DataTypes.String _0_Group_name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                    public _32_points[] _32_Points;
                    [Serializable]
                    [Reflexive(Name = "Points", Offset = 32, ChunkSize = 60, Label = "")]
                    public class _32_points
                    {
                        private int __StartOffset__;
                        [Value(Name = "Point name", Offset = 0)]
                        public H2.DataTypes.String _0_Point_name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                        [Value(Offset = 32)]
                        public Byte[] _32_ = new byte[16];
                        [Value(Name = "X", Offset = 48)]
                        public Single _48_X;
                        [Value(Name = "Y", Offset = 52)]
                        public Single _52_Y;
                        [Value(Name = "Z", Offset = 56)]
                        public Single _56_Z;
                    }
                    [Value(Name = "Unknown", Offset = 40)]
                    public Int32 _40_Unknown;
                    [Value(Offset = 44)]
                    public Byte[] _44_ = new byte[4];
                }
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[120];
            }
            public _480_cutscene_flags[] _480_Cutscene_Flags;
            [Serializable]
            [Reflexive(Name = "Cutscene Flags", Offset = 480, ChunkSize = 56, Label = "")]
            public class _480_cutscene_flags
            {
                private int __StartOffset__;
                [Value(Offset = 0)]
                public Byte[] _0_ = new byte[4];
                [Value(Name = "Name", Offset = 4)]
                public H2.DataTypes.String _4_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Position X", Offset = 36)]
                public Single _36_Position_X;
                [Value(Name = "Position Y", Offset = 40)]
                public Single _40_Position_Y;
                [Value(Name = "Position Z", Offset = 44)]
                public Single _44_Position_Z;
                [Value(Name = "Facing Yaw", Offset = 48)]
                public Single _48_Facing_Yaw;
                [Value(Name = "Facing Pitch", Offset = 52)]
                public Single _52_Facing_Pitch;
            }
            public _488_cutscene_camera[] _488_Cutscene_Camera;
            [Serializable]
            [Reflexive(Name = "Cutscene Camera", Offset = 488, ChunkSize = 64, Label = "")]
            public class _488_cutscene_camera
            {
                private int __StartOffset__;
                [Value(Offset = 0)]
                public Byte[] _0_ = new byte[4];
                [Value(Name = "Name", Offset = 4)]
                public H2.DataTypes.String _4_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Position X", Offset = 36)]
                public Single _36_Position_X;
                [Value(Name = "Position Y", Offset = 40)]
                public Single _40_Position_Y;
                [Value(Name = "Position Z", Offset = 44)]
                public Single _44_Position_Z;
                [Value(Name = "Orientation Y", Offset = 48)]
                public Single _48_Orientation_Y;
                [Value(Name = "Orientation P", Offset = 52)]
                public Single _52_Orientation_P;
                [Value(Name = "Orientation R", Offset = 56)]
                public Single _56_Orientation_R;
                [Value(Name = "Field of View", Offset = 60)]
                public Single _60_Field_of_View;
            }
            public _496_cutscene_titles[] _496_Cutscene_Titles;
            [Serializable]
            [Reflexive(Name = "Cutscene Titles", Offset = 496, ChunkSize = 36, Label = "")]
            public class _496_cutscene_titles
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Name = "Text Bounds On Screen Top", Offset = 4)]
                public Int16 _4_Text_Bounds_On_Screen_Top;
                [Value(Name = "Text Bounds On Screen Left", Offset = 6)]
                public Int16 _6_Text_Bounds_On_Screen_Left;
                [Value(Name = "Text Bounds On Screen Bottom", Offset = 8)]
                public Int16 _8_Text_Bounds_On_Screen_Bottom;
                [Value(Name = "Text Bounds On Screen Right", Offset = 10)]
                public Int16 _10_Text_Bounds_On_Screen_Right;
                [Value(Name = "String Index", Offset = 12)]
                public Int16 _12_String_Index;
                [Value(Name = "Justification", Offset = 14)]
                public H2.DataTypes.Enum _14_Justification = new H2.DataTypes.Enum(new string[] { "Left", "Right", "Center" }, 1);
                [Value(Name = "Text Color A", Offset = 15)]
                public Byte _15_Text_Color_A;
                [Value(Name = "Text Color R", Offset = 16)]
                public Byte _16_Text_Color_R;
                [Value(Name = "Text Color G", Offset = 17)]
                public Byte _17_Text_Color_G;
                [Value(Name = "Text Color B", Offset = 18)]
                public Byte _18_Text_Color_B;
                [Value(Name = "Shadow Color A", Offset = 19)]
                public Byte _19_Shadow_Color_A;
                [Value(Name = "Shadow Color R", Offset = 20)]
                public Byte _20_Shadow_Color_R;
                [Value(Name = "Shadow Color G", Offset = 21)]
                public Byte _21_Shadow_Color_G;
                [Value(Name = "Shadow Color B", Offset = 22)]
                public Byte _22_Shadow_Color_B;
                [Value(Offset = 23)]
                public Byte[] _23_ = new byte[1];
                [Value(Name = "Fade In Time Seconds", Offset = 24)]
                public Single _24_Fade_In_Time_Seconds;
                [Value(Name = "Up Time Seconds", Offset = 28)]
                public Single _28_Up_Time_Seconds;
                [Value(Name = "Fade Out Time Seconds", Offset = 32)]
                public Single _32_Fade_Out_Time_Seconds;
            }
            [Value(Name = "Custom Object Names", Offset = 504)]
            public Dependancy _504_Custom_Object_Names;
            [Value(Name = "Chapter Title Text", Offset = 512)]
            public Dependancy _512_Chapter_Title_Text;
            [Value(Name = "HUD Messages", Offset = 520)]
            public Dependancy _520_HUD_Messages;
            public _528_structure_bsps[] _528_Structure_BSPs;
            [Serializable]
            [Reflexive(Name = "Structure BSPs", Offset = 528, ChunkSize = 68, Label = "")]
            public class _528_structure_bsps
            {
                private int __StartOffset__;
                [Value(Name = "Structure BSP Offset", Offset = 0)]
                public UInt32 _0_Structure_BSP_Offset;
                [Value(Name = "Structure BSP Size", Offset = 4)]
                public UInt32 _4_Structure_BSP_Size;
                [Value(Name = "Primary Magic Meta Offset", Offset = 8)]
                public UInt32 _8_Primary_Magic_Meta_Offset;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[4];
                [Value(Name = "Structure BSP", Offset = 16)]
                public Dependancy _16_Structure_BSP;
                [Value(Name = "Light Map", Offset = 24)]
                public Dependancy _24_Light_Map;
                [Value(Name = "UNUSED Radiance Est Search", Offset = 32)]
                public Single _32_UNUSED_Radiance_Est_Search;
                [Value(Name = "UNUSED Luminels Per World", Offset = 36)]
                public Single _36_UNUSED_Luminels_Per_World;
                [Value(Name = "Unused Output White Reference", Offset = 40)]
                public Single _40_Unused_Output_White_Reference;
                [Value(Offset = 44)]
                public Byte[] _44_ = new byte[16];
                [Value(Name = "Flags", Offset = 60)]
                public Bitmask _60_Flags = new Bitmask(new string[] { "Default Sky Enabled" }, 4);
                [Value(Name = "Default Sky", Offset = 64)]
                public Int16 _64_Default_Sky;
                [Value(Offset = 66)]
                public Byte[] _66_ = new byte[2];
            }
            public _536_scenario_resources[] _536_Scenario_Resources;
            [Serializable]
            [Reflexive(Name = "Scenario Resources", Offset = 536, ChunkSize = 24, Label = "")]
            public class _536_scenario_resources
            {
                private int __StartOffset__;
                public _0_references[] _0_References;
                [Serializable]
                [Reflexive(Name = "References", Offset = 0, ChunkSize = 8, Label = "")]
                public class _0_references
                {
                    private int __StartOffset__;
                    [Value(Name = "Reference", Offset = 0)]
                    public Dependancy _0_Reference;
                }
                public _8_script_source[] _8_Script_Source;
                [Serializable]
                [Reflexive(Name = "Script Source", Offset = 8, ChunkSize = 8, Label = "")]
                public class _8_script_source
                {
                    private int __StartOffset__;
                    [Value(Name = "Reference", Offset = 0)]
                    public Dependancy _0_Reference;
                }
                public _16_ai_resources[] _16_AI_Resources;
                [Serializable]
                [Reflexive(Name = "AI Resources", Offset = 16, ChunkSize = 8, Label = "")]
                public class _16_ai_resources
                {
                    private int __StartOffset__;
                    [Value(Name = "Reference", Offset = 0)]
                    public Dependancy _0_Reference;
                }
            }
            public _544_scenario_resources[] _544_Scenario_Resources;
            [Serializable]
            [Reflexive(Name = "Scenario Resources", Offset = 544, ChunkSize = 16, Label = "")]
            public class _544_scenario_resources
            {
                private int __StartOffset__;
                public _0_unused[] _0_Unused;
                [Serializable]
                [Reflexive(Name = "Unused", Offset = 0, ChunkSize = 1, Label = "")]
                public class _0_unused
                {
                    private int __StartOffset__;
                    [Value(Name = "Data", Offset = 0)]
                    public Byte _0_Data;
                }
                public _8_unused[] _8_Unused;
                [Serializable]
                [Reflexive(Name = "Unused", Offset = 8, ChunkSize = 10, Label = "")]
                public class _8_unused
                {
                    private int __StartOffset__;
                    [Value(Name = "Unique ID", Offset = 0)]
                    public Int32 _0_Unique_ID;
                    [Value(Name = "Origin BSP", Offset = 4)]
                    public Int16 _4_Origin_BSP;
                    [Value(Name = "Type", Offset = 6)]
                    public H2.DataTypes.Enum _6_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                    [Value(Name = "Source", Offset = 8)]
                    public H2.DataTypes.Enum _8_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                }
            }
            [Value(Offset = 552)]
            public Byte[] _552_ = new byte[8];
            public _560_scenario_kill_triggers[] _560_Scenario_Kill_Triggers;
            [Serializable]
            [Reflexive(Name = "Scenario Kill Triggers", Offset = 560, ChunkSize = 2, Label = "")]
            public class _560_scenario_kill_triggers
            {
                private int __StartOffset__;
                [Value(Name = "Trigger Volume", Offset = 0)]
                public Int16 _0_Trigger_Volume;
            }
            public _568_unknown[] _568_Unknown;
            [Serializable]
            [Reflexive(Name = "Unknown", Offset = 568, ChunkSize = 20, Label = "")]
            public class _568_unknown
            {
                private int __StartOffset__;
                [Value(Name = "Unknown", Offset = 0)]
                public Int32 _0_Unknown;
                [Value(Name = "Unknown", Offset = 4)]
                public Int32 _4_Unknown;
                [Value(Name = "Unknown", Offset = 8)]
                public Int32 _8_Unknown;
                [Value(Name = "Unknown", Offset = 12)]
                public Int32 _12_Unknown;
                [Value(Name = "Unknown", Offset = 16)]
                public Int32 _16_Unknown;
            }
            public _576_ai_zones[] _576_AI_Zones;
            [Serializable]
            [Reflexive(Name = "AI Zones", Offset = 576, ChunkSize = 124, Label = "")]
            public class _576_ai_zones
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Unknown", Offset = 32)]
                public Int32 _32_Unknown;
                [Value(Name = "Unknown", Offset = 36)]
                public Int32 _36_Unknown;
                [Value(Name = "Unknown", Offset = 40)]
                public Int32 _40_Unknown;
                [Value(Name = "Unknown", Offset = 44)]
                public H2.DataTypes.String _44_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Unknown", Offset = 76)]
                public Single _76_Unknown;
                [Value(Name = "Unknown", Offset = 80)]
                public Single _80_Unknown;
                public _84_unknown[] _84_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 84, ChunkSize = 8, Label = "")]
                public class _84_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                    [Value(Name = "Unknown", Offset = 4)]
                    public Int32 _4_Unknown;
                }
                public _92_unknown[] _92_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 92, ChunkSize = 8, Label = "")]
                public class _92_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                    [Value(Name = "Unknown", Offset = 4)]
                    public Int32 _4_Unknown;
                }
                public _100_unknown[] _100_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 100, ChunkSize = 12, Label = "")]
                public class _100_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                    public _4_unknown[] _4_Unknown;
                    [Serializable]
                    [Reflexive(Name = "Unknown", Offset = 4, ChunkSize = 8, Label = "")]
                    public class _4_unknown
                    {
                        private int __StartOffset__;
                        [Value(Name = "Unknown", Offset = 0)]
                        public Int32 _0_Unknown;
                        [Value(Name = "Unknown", Offset = 4)]
                        public Int32 _4_Unknown;
                    }
                }
                public _108_unknown[] _108_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 108, ChunkSize = 4, Label = "")]
                public class _108_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                }
                public _116_unknown[] _116_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 116, ChunkSize = 20, Label = "")]
                public class _116_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                    [Value(Name = "Unknown", Offset = 4)]
                    public Single _4_Unknown;
                    [Value(Name = "Unknown", Offset = 8)]
                    public Int32 _8_Unknown;
                    public _12_unknown[] _12_Unknown;
                    [Serializable]
                    [Reflexive(Name = "Unknown", Offset = 12, ChunkSize = 8, Label = "")]
                    public class _12_unknown
                    {
                        private int __StartOffset__;
                        [Value(Name = "Unknown", Offset = 0)]
                        public Int32 _0_Unknown;
                        [Value(Name = "Unknown", Offset = 4)]
                        public Int32 _4_Unknown;
                    }
                }
            }
            public _584_ai_triggers[] _584_AI_Triggers;
            [Serializable]
            [Reflexive(Name = "AI Triggers", Offset = 584, ChunkSize = 48, Label = "")]
            public class _584_ai_triggers
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Unknown", Offset = 32)]
                public Int32 _32_Unknown;
                [Value(Name = "Unknown", Offset = 36)]
                public Int32 _36_Unknown;
                public _40_unknown[] _40_Unknown;
                [Serializable]
                [Reflexive(Name = "Unknown", Offset = 40, ChunkSize = 56, Label = "")]
                public class _40_unknown
                {
                    private int __StartOffset__;
                    [Value(Name = "Unknown", Offset = 0)]
                    public Int32 _0_Unknown;
                    [Value(Name = "Unknown", Offset = 4)]
                    public Int32 _4_Unknown;
                    [Value(Name = "Unknown", Offset = 8)]
                    public Single _8_Unknown;
                    [Value(Name = "Unknown", Offset = 12)]
                    public Int32 _12_Unknown;
                    [Value(Name = "Unknown", Offset = 16)]
                    public H2.DataTypes.String _16_Unknown = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Unknown", Offset = 48)]
                    public Int32 _48_Unknown;
                    [Value(Offset = 52)]
                    public Byte[] _52_ = new byte[4];
                }
            }
            public _592_background_sound_pallete[] _592_Background_Sound_Pallete;
            [Serializable]
            [Reflexive(Name = "Background Sound Pallete", Offset = 592, ChunkSize = 100, Label = "")]
            public class _592_background_sound_pallete
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Background Sound", Offset = 32)]
                public Dependancy _32_Background_Sound;
                [Value(Name = "Inside Cluster Sound", Offset = 40)]
                public Dependancy _40_Inside_Cluster_Sound;
                [Value(Name = "Cutoff Distance", Offset = 48)]
                public Single _48_Cutoff_Distance;
                [Value(Name = "Scale Flags", Offset = 52)]
                public Bitmask _52_Scale_Flags = new Bitmask(new string[] { "Override Default Scale", "Use Adjacent Cluster As Portal Scale", "Use Adjacent Cluster As Exterior Scale", "Scale With Weather Intensity" }, 4);
                [Value(Offset = 56)]
                public Byte[] _56_ = new byte[12];
                [Value(Name = "Interior Scale", Offset = 68)]
                public Single _68_Interior_Scale;
                [Value(Name = "Portal Scale", Offset = 72)]
                public Single _72_Portal_Scale;
                [Value(Name = "Exterior Scale", Offset = 76)]
                public Single _76_Exterior_Scale;
                [Value(Offset = 80)]
                public Byte[] _80_ = new byte[8];
                [Value(Name = "Interpolation Speed Per Sec", Offset = 88)]
                public Single _88_Interpolation_Speed_Per_Sec;
                [Value(Offset = 92)]
                public Byte[] _92_ = new byte[8];
            }
            public _600_sound_environment_pallete[] _600_Sound_Environment_Pallete;
            [Serializable]
            [Reflexive(Name = "Sound Environment Pallete", Offset = 600, ChunkSize = 72, Label = "")]
            public class _600_sound_environment_pallete
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Sound Environment", Offset = 32)]
                public Dependancy _32_Sound_Environment;
                [Value(Name = "Cuttoff Distance", Offset = 40)]
                public Single _40_Cuttoff_Distance;
                [Value(Name = "Interpolation Speed Per Sec", Offset = 44)]
                public Single _44_Interpolation_Speed_Per_Sec;
                [Value(Offset = 48)]
                public Byte[] _48_ = new byte[24];
            }
            public _608_weather_palette[] _608_Weather_Palette;
            [Serializable]
            [Reflexive(Name = "Weather Palette", Offset = 608, ChunkSize = 136, Label = "")]
            public class _608_weather_palette
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.BigEndianUnicode);
                [Value(Name = "Weather System", Offset = 32)]
                public Dependancy _32_Weather_System;
                [Value(Offset = 40)]
                public Byte[] _40_ = new byte[36];
                [Value(Name = "Wind", Offset = 76)]
                public Dependancy _76_Wind;
                [Value(Name = "Wind Direction i", Offset = 84)]
                public Single _84_Wind_Direction_i;
                [Value(Name = "Wind Direction j", Offset = 88)]
                public Single _88_Wind_Direction_j;
                [Value(Name = "Wind Direction k", Offset = 92)]
                public Single _92_Wind_Direction_k;
                [Value(Name = "Wind Magnitude", Offset = 96)]
                public Single _96_Wind_Magnitude;
                [Value(Name = "Wind Scale Function", Offset = 100)]
                public StringID _100_Wind_Scale_Function;
                [Value(Offset = 104)]
                public Byte[] _104_ = new byte[32];
            }
            [Value(Offset = 616)]
            public Byte[] _616_ = new byte[176];
            public _792_spawn_data[] _792_Spawn_Data;
            [Serializable]
            [Reflexive(Name = "Spawn Data", Offset = 792, ChunkSize = 96, Label = "")]
            public class _792_spawn_data
            {
                private int __StartOffset__;
                [Value(Name = "Dynamic Spawn Lower Height", Offset = 0)]
                public Single _0_Dynamic_Spawn_Lower_Height;
                [Value(Name = "Dynamic Spawn Upper Height", Offset = 4)]
                public Single _4_Dynamic_Spawn_Upper_Height;
                [Value(Name = "Game Objective Reset Height", Offset = 8)]
                public Single _8_Game_Objective_Reset_Height;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[60];
                public _72_dynamic_spawn_overloads[] _72_Dynamic_Spawn_Overloads;
                [Serializable]
                [Reflexive(Name = "Dynamic Spawn Overloads", Offset = 72, ChunkSize = 16, Label = "")]
                public class _72_dynamic_spawn_overloads
                {
                    private int __StartOffset__;
                    [Value(Name = "Overload Type", Offset = 0)]
                    public H2.DataTypes.Enum _0_Overload_Type = new H2.DataTypes.Enum(new string[] { "Enemy", "Friend", "Enemy Vehicle", "Friendly Vehicle", "Empty Vehicle", "Oddball Inclusion", "Oddball Exclusion", "Hill Inclusion", "Hill Exclusion", "Last Race Flag", "Dead Ally", "Controlled Territory" }, 4);
                    [Value(Name = "Inner Radius", Offset = 4)]
                    public Single _4_Inner_Radius;
                    [Value(Name = "Outer Radius", Offset = 8)]
                    public Single _8_Outer_Radius;
                    [Value(Name = "Weight", Offset = 12)]
                    public Single _12_Weight;
                }
                public _80_static_respawn_zones[] _80_Static_Respawn_Zones;
                [Serializable]
                [Reflexive(Name = "Static Respawn Zones", Offset = 80, ChunkSize = 48, Label = "")]
                public class _80_static_respawn_zones
                {
                    private int __StartOffset__;
                    [Value(Name = "Name", Offset = 0)]
                    public StringID _0_Name;
                    [Value(Name = "Relevant Team", Offset = 4)]
                    public Bitmask _4_Relevant_Team = new Bitmask(new string[] { "Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Brown", "Pink", "Neutral" }, 4);
                    [Value(Name = "Relevant Games", Offset = 8)]
                    public Bitmask _8_Relevant_Games = new Bitmask(new string[] { "Slayer", "Oddball", "King Of The Hill", "CTF", "Race", "Headhunter", "Juggernaut", "Territories" }, 4);
                    [Value(Name = "Flags", Offset = 12)]
                    public Bitmask _12_Flags = new Bitmask(new string[] { "Disabled If Flag Home", "Disabled If Flag Away", "Disabled If Bomb Home", "Disabled If Bomb Away" }, 4);
                    [Value(Name = "Position X", Offset = 16)]
                    public Single _16_Position_X;
                    [Value(Name = "Position Y", Offset = 20)]
                    public Single _20_Position_Y;
                    [Value(Name = "Position Z", Offset = 24)]
                    public Single _24_Position_Z;
                    [Value(Name = "Lower Height", Offset = 28)]
                    public Single _28_Lower_Height;
                    [Value(Name = "Upper Height", Offset = 32)]
                    public Single _32_Upper_Height;
                    [Value(Name = "Inner Radius", Offset = 36)]
                    public Single _36_Inner_Radius;
                    [Value(Name = "Outer Radius", Offset = 40)]
                    public Single _40_Outer_Radius;
                    [Value(Name = "Weight", Offset = 44)]
                    public Single _44_Weight;
                }
                public _88_static_initial_spawn_zones[] _88_Static_Initial_Spawn_Zones;
                [Serializable]
                [Reflexive(Name = "Static Initial Spawn Zones", Offset = 88, ChunkSize = 48, Label = "")]
                public class _88_static_initial_spawn_zones
                {
                    private int __StartOffset__;
                    [Value(Name = "Name", Offset = 0)]
                    public StringID _0_Name;
                    [Value(Name = "Relevant Team", Offset = 4)]
                    public Bitmask _4_Relevant_Team = new Bitmask(new string[] { "Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Brown", "Pink", "Neutral" }, 4);
                    [Value(Name = "Relevant Games", Offset = 8)]
                    public Bitmask _8_Relevant_Games = new Bitmask(new string[] { "Slayer", "Oddball", "King Of The Hill", "CTF", "Race", "Headhunter", "Juggernaut", "Territories" }, 4);
                    [Value(Name = "Flags", Offset = 12)]
                    public Bitmask _12_Flags = new Bitmask(new string[] { "Disabled If Flag Home", "Disabled If Flag Away", "Disabled If Bomb Home", "Disabled If Bomb Away" }, 4);
                    [Value(Name = "Position X", Offset = 16)]
                    public Single _16_Position_X;
                    [Value(Name = "Position Y", Offset = 20)]
                    public Single _20_Position_Y;
                    [Value(Name = "Position Z", Offset = 24)]
                    public Single _24_Position_Z;
                    [Value(Name = "Lower Height", Offset = 28)]
                    public Single _28_Lower_Height;
                    [Value(Name = "Upper Height", Offset = 32)]
                    public Single _32_Upper_Height;
                    [Value(Name = "Inner Radius", Offset = 36)]
                    public Single _36_Inner_Radius;
                    [Value(Name = "Outer Radius", Offset = 40)]
                    public Single _40_Outer_Radius;
                    [Value(Name = "Weight", Offset = 44)]
                    public Single _44_Weight;
                }
            }
            [Value(Name = "Sound Effect Collection", Offset = 800)]
            public Dependancy _800_Sound_Effect_Collection;
            public _808_crates[] _808_Crates;
            [Serializable]
            [Reflexive(Name = "Crates", Offset = 808, ChunkSize = 76, Label = "")]
            public class _808_crates
            {
                private int __StartOffset__;
                [Value(Name = "Type", Offset = 0)]
                public Int16 _0_Type;
                [Value(Name = "Name", Offset = 2)]
                public Int16 _2_Name;
                [Value(Name = "Placement Flags", Offset = 4)]
                public Bitmask _4_Placement_Flags = new Bitmask(new string[] { "Not Automatically", "Not On Easy", "Not On Normal", "Not On Hard", "Lock Type To Env Object", "Lock Transform To Env Object", "Never Placed", "Lock Name to Env Object", "Create At Rest" }, 4);
                [Value(Name = "Position X", Offset = 8)]
                public Single _8_Position_X;
                [Value(Name = "Position Y", Offset = 12)]
                public Single _12_Position_Y;
                [Value(Name = "Position Z", Offset = 16)]
                public Single _16_Position_Z;
                [Value(Name = "Rotation Yaw", Offset = 20)]
                public Single _20_Rotation_Yaw;
                [Value(Name = "Rotation Pitch", Offset = 24)]
                public Single _24_Rotation_Pitch;
                [Value(Name = "Rotation Roll", Offset = 28)]
                public Single _28_Rotation_Roll;
                [Value(Name = "Scale", Offset = 32)]
                public Single _32_Scale;
                [Value(Offset = 36)]
                public Byte[] _36_ = new byte[2];
                [Value(Name = "Manual BSP Flags", Offset = 38)]
                public Int16 _38_Manual_BSP_Flags;
                [Value(Name = "Unique ID", Offset = 40)]
                public Int32 _40_Unique_ID;
                [Value(Name = "Origin BSP", Offset = 44)]
                public Int16 _44_Origin_BSP;
                [Value(Name = "Type", Offset = 46)]
                public H2.DataTypes.Enum _46_Type = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Source", Offset = 48)]
                public H2.DataTypes.Enum _48_Source = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "BSP Policy", Offset = 50)]
                public H2.DataTypes.Enum _50_BSP_Policy = new H2.DataTypes.Enum(new string[] { "Default", "Always Places", "Manual BSP Placement" }, 2);
                [Value(Name = "Editor Folder", Offset = 52)]
                public Int16 _52_Editor_Folder;
                [Value(Offset = 54)]
                public Byte[] _54_ = new byte[4];
                [Value(Name = "Variant Name", Offset = 58)]
                public StringID _58_Variant_Name;
                [Value(Name = "Active Change Colors", Offset = 62)]
                public Bitmask _62_Active_Change_Colors = new Bitmask(new string[] { "Primary", "Secondary", "Tertiary", "Quaternary" }, 2);
                [Value(Name = "Primary Color R", Offset = 64)]
                public Byte _64_Primary_Color_R;
                [Value(Name = "Primary Color G", Offset = 65)]
                public Byte _65_Primary_Color_G;
                [Value(Name = "Primary Color B", Offset = 66)]
                public Byte _66_Primary_Color_B;
                [Value(Name = "Secondary Color R", Offset = 67)]
                public Byte _67_Secondary_Color_R;
                [Value(Name = "Secondary Color G", Offset = 68)]
                public Byte _68_Secondary_Color_G;
                [Value(Name = "Secondary Color B", Offset = 69)]
                public Byte _69_Secondary_Color_B;
                [Value(Name = "Tertiary Color R", Offset = 70)]
                public Byte _70_Tertiary_Color_R;
                [Value(Name = "Tertiary Color G", Offset = 71)]
                public Byte _71_Tertiary_Color_G;
                [Value(Name = "Tertiary Color B", Offset = 72)]
                public Byte _72_Tertiary_Color_B;
                [Value(Name = "Quaternary Color R", Offset = 73)]
                public Byte _73_Quaternary_Color_R;
                [Value(Name = "Quaternary Color G", Offset = 74)]
                public Byte _74_Quaternary_Color_G;
                [Value(Name = "Quaternary Color B", Offset = 75)]
                public Byte _75_Quaternary_Color_B;
            }
            public _816_crate_palette[] _816_Crate_Palette;
            [Serializable]
            [Reflexive(Name = "Crate Palette", Offset = 816, ChunkSize = 40, Label = "")]
            public class _816_crate_palette
            {
                private int __StartOffset__;
                [Value(Name = "Crate", Offset = 0)]
                public Dependancy _0_Crate;
                [Value(Offset = 8)]
                public Byte[] _8_ = new byte[32];
            }
            [Value(Name = "Global Lighting", Offset = 824)]
            public Dependancy _824_Global_Lighting;
            public _832_atmospheric_fog_palette[] _832_Atmospheric_Fog_Palette;
            [Serializable]
            [Reflexive(Name = "Atmospheric Fog Palette", Offset = 832, ChunkSize = 244, Label = "")]
            public class _832_atmospheric_fog_palette
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Offset = 4)]
                public Byte[] _4_ = new byte[1];
                [Value(Name = "Atmospheric Fog Color R", Offset = 5)]
                public Byte _5_Atmospheric_Fog_Color_R;
                [Value(Name = "Atmospheric Fog Color G", Offset = 6)]
                public Byte _6_Atmospheric_Fog_Color_G;
                [Value(Name = "Atmospheric Fog Color B", Offset = 7)]
                public Byte _7_Atmospheric_Fog_Color_B;
                [Value(Name = "Atmospheric Fog Spread Distance", Offset = 8)]
                public Single _8_Atmospheric_Fog_Spread_Distance;
                [Value(Name = "Atmospheric Fog Max Density", Offset = 12)]
                public Single _12_Atmospheric_Fog_Max_Density;
                [Value(Name = "Atmospheric Fog Start Distance", Offset = 16)]
                public Single _16_Atmospheric_Fog_Start_Distance;
                [Value(Name = "Atmospheric Fog Opaque Distance", Offset = 20)]
                public Single _20_Atmospheric_Fog_Opaque_Distance;
                [Value(Offset = 24)]
                public Byte[] _24_ = new byte[25];
                [Value(Name = "Secondary Fog Color R", Offset = 49)]
                public Byte _49_Secondary_Fog_Color_R;
                [Value(Name = "Secondary Fog Color G", Offset = 50)]
                public Byte _50_Secondary_Fog_Color_G;
                [Value(Name = "Secondary Fog Color B", Offset = 51)]
                public Byte _51_Secondary_Fog_Color_B;
                [Value(Name = "Secondary Fog Max Density", Offset = 52)]
                public Single _52_Secondary_Fog_Max_Density;
                [Value(Name = "Secondary Fog Start Distance", Offset = 56)]
                public Single _56_Secondary_Fog_Start_Distance;
                [Value(Name = "Secondary Fog Opaque Distance", Offset = 60)]
                public Single _60_Secondary_Fog_Opaque_Distance;
                [Value(Offset = 64)]
                public Byte[] _64_ = new byte[69];
                [Value(Name = "Planar Color R", Offset = 133)]
                public Byte _133_Planar_Color_R;
                [Value(Name = "Planar Color G", Offset = 134)]
                public Byte _134_Planar_Color_G;
                [Value(Name = "Planar Color B", Offset = 135)]
                public Byte _135_Planar_Color_B;
                [Value(Name = "Planar Max Density", Offset = 136)]
                public Single _136_Planar_Max_Density;
                [Value(Name = "Planar Override Amount", Offset = 140)]
                public Single _140_Planar_Override_Amount;
                [Value(Name = "Planar Min Distance Bias", Offset = 144)]
                public Single _144_Planar_Min_Distance_Bias;
                [Value(Offset = 148)]
                public Byte[] _148_ = new byte[17];
                [Value(Name = "Patchy Color R", Offset = 165)]
                public Byte _165_Patchy_Color_R;
                [Value(Name = "Patchy Color G", Offset = 166)]
                public Byte _166_Patchy_Color_G;
                [Value(Name = "Patchy Color B", Offset = 167)]
                public Byte _167_Patchy_Color_B;
                [Value(Name = "Patchy Density", Offset = 168)]
                public Single _168_Patchy_Density;
                [Value(Name = "Patchy Distance", Offset = 172)]
                public Single _172_Patchy_Distance;
                [Value(Offset = 176)]
                public Byte[] _176_ = new byte[32];
                [Value(Name = "Patchy Fog", Offset = 208)]
                public Dependancy _208_Patchy_Fog;
                public _216_mixers[] _216_Mixers;
                [Serializable]
                [Reflexive(Name = "Mixers", Offset = 216, ChunkSize = 8, Label = "")]
                public class _216_mixers
                {
                    private int __StartOffset__;
                    [Value(Name = "Atmospheric Fog Source", Offset = 0)]
                    public StringID _0_Atmospheric_Fog_Source;
                    [Value(Name = "Interpolator", Offset = 4)]
                    public StringID _4_Interpolator;
                }
                [Value(Name = "Amount", Offset = 224)]
                public Single _224_Amount;
                [Value(Name = "Thershold", Offset = 228)]
                public Single _228_Thershold;
                [Value(Name = "Brightness", Offset = 232)]
                public Single _232_Brightness;
                [Value(Name = "Gamma Power", Offset = 236)]
                public Single _236_Gamma_Power;
                [Value(Name = "Camera Immersion Flags", Offset = 240)]
                public Bitmask _240_Camera_Immersion_Flags = new Bitmask(new string[] { "Disable Atmospheric Fog", "Disable Secondary Fog", "Disable Planar Fog", "Invert Planar Fog Priorites", "Disable Water" }, 4);
            }
            public _840_planar_fog_palette[] _840_Planar_Fog_Palette;
            [Serializable]
            [Reflexive(Name = "Planar Fog Palette", Offset = 840, ChunkSize = 16, Label = "")]
            public class _840_planar_fog_palette
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Name = "Planar Fog", Offset = 4)]
                public Dependancy _4_Planar_Fog;
                [Value(Offset = 12)]
                public Byte[] _12_ = new byte[4];
            }
            public _848_creature_spawning[] _848_Creature_Spawning;
            [Serializable]
            [Reflexive(Name = "Creature Spawning", Offset = 848, ChunkSize = 132, Label = "")]
            public class _848_creature_spawning
            {
                private int __StartOffset__;
                [Value(Offset = 0)]
                public Byte[] _0_ = new byte[12];
                public _12_starting_location[] _12_Starting_Location;
                [Serializable]
                [Reflexive(Name = "Starting Location", Offset = 12, ChunkSize = 28, Label = "")]
                public class _12_starting_location
                {
                    private int __StartOffset__;
                    [Value(Name = "X", Offset = 0)]
                    public Single _0_X;
                    [Value(Name = "Y", Offset = 4)]
                    public Single _4_Y;
                    [Value(Name = "Z", Offset = 8)]
                    public Single _8_Z;
                    [Value(Name = "Unknown", Offset = 12)]
                    public Single _12_Unknown;
                    [Value(Name = "Yaw", Offset = 16)]
                    public Single _16_Yaw;
                    [Value(Name = "Pitch", Offset = 20)]
                    public Single _20_Pitch;
                    [Value(Name = "Roll", Offset = 24)]
                    public Single _24_Roll;
                }
                [Value(Offset = 20)]
                public Byte[] _20_ = new byte[20];
                [Value(Name = "Creature", Offset = 40)]
                public Dependancy _40_Creature;
                [Value(Name = "Easy Spawn Count", Offset = 48)]
                public Int16 _48_Easy_Spawn_Count;
                [Value(Name = "Legendary Spawn Count", Offset = 50)]
                public Int16 _50_Legendary_Spawn_Count;
                [Value(Offset = 52)]
                public Byte[] _52_ = new byte[80];
            }
            [Value(Name = "Subtitles", Offset = 856)]
            public Dependancy _856_Subtitles;
            [Value(Offset = 864)]
            public Byte[] _864_ = new byte[24];
            public _888_decorator_palette[] _888_Decorator_Palette;
            [Serializable]
            [Reflexive(Name = "Decorator Palette", Offset = 888, ChunkSize = 8, Label = "")]
            public class _888_decorator_palette
            {
                private int __StartOffset__;
                [Value(Name = "Decorator", Offset = 0)]
                public Dependancy _0_Decorator;
            }
            [Value(Offset = 896)]
            public Byte[] _896_ = new byte[8];
            public _904_sbsp_lighting[] _904_SBSP_Lighting;
            [Serializable]
            [Reflexive(Name = "SBSP Lighting", Offset = 904, ChunkSize = 16, Label = "")]
            public class _904_sbsp_lighting
            {
                private int __StartOffset__;
                [Value(Name = "SBSP", Offset = 0)]
                public Dependancy _0_SBSP;
                public _8_lighting_points[] _8_Lighting_Points;
                [Serializable]
                [Reflexive(Name = "Lighting Points", Offset = 8, ChunkSize = 12, Label = "")]
                public class _8_lighting_points
                {
                    private int __StartOffset__;
                    [Value(Name = "Position X", Offset = 0)]
                    public Single _0_Position_X;
                    [Value(Name = "Position Y", Offset = 4)]
                    public Single _4_Position_Y;
                    [Value(Name = "Position Z", Offset = 8)]
                    public Single _8_Position_Z;
                }
            }
            [Value(Offset = 912)]
            public Byte[] _912_ = new byte[8];
            public _920_map_description[] _920_Map_Description;
            [Serializable]
            [Reflexive(Name = "Map Description", Offset = 920, ChunkSize = 24, Label = "")]
            public class _920_map_description
            {
                private int __StartOffset__;
                [Value(Name = "Map Description", Offset = 0)]
                public Dependancy _0_Map_Description;
                public _8_campaign_level_data[] _8_Campaign_Level_Data;
                [Serializable]
                [Reflexive(Name = "Campaign Level Data", Offset = 8, ChunkSize = 2392, Label = "")]
                public class _8_campaign_level_data
                {
                    private int __StartOffset__;
                    [Value(Name = "Campaign ID", Offset = 0)]
                    public Int32 _0_Campaign_ID;
                    [Value(Name = "Map ID", Offset = 4)]
                    public Int32 _4_Map_ID;
                    [Value(Name = "Preview Image", Offset = 8)]
                    public Dependancy _8_Preview_Image;
                    [Value(Name = "English Name", Offset = 16)]
                    public H2.DataTypes.String _16_English_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Japanese Name", Offset = 24)]
                    public H2.DataTypes.String _24_Japanese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "German Name", Offset = 32)]
                    public H2.DataTypes.String _32_German_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "French Name", Offset = 40)]
                    public H2.DataTypes.String _40_French_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Spanish Name", Offset = 48)]
                    public H2.DataTypes.String _48_Spanish_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Italian Name", Offset = 56)]
                    public H2.DataTypes.String _56_Italian_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Korean Name", Offset = 64)]
                    public H2.DataTypes.String _64_Korean_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Chinese Name", Offset = 72)]
                    public H2.DataTypes.String _72_Chinese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Portuguese Name", Offset = 80)]
                    public H2.DataTypes.String _80_Portuguese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "English Description", Offset = 88)]
                    public H2.DataTypes.String _88_English_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Japanese Description", Offset = 344)]
                    public H2.DataTypes.String _344_Japanese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "German Description", Offset = 600)]
                    public H2.DataTypes.String _600_German_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "French Description", Offset = 856)]
                    public H2.DataTypes.String _856_French_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Spanish Description", Offset = 1112)]
                    public H2.DataTypes.String _1112_Spanish_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Italian Description", Offset = 1368)]
                    public H2.DataTypes.String _1368_Italian_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Korean Description", Offset = 1624)]
                    public H2.DataTypes.String _1624_Korean_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Chinese Description", Offset = 1880)]
                    public H2.DataTypes.String _1880_Chinese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Portuguese Description", Offset = 2136)]
                    public H2.DataTypes.String _2136_Portuguese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                }
                public _16_multiplayer[] _16_Multiplayer;
                [Serializable]
                [Reflexive(Name = "Multiplayer", Offset = 16, ChunkSize = 2670, Label = "")]
                public class _16_multiplayer
                {
                    private int __StartOffset__;
                    [Value(Name = "Map ID", Offset = 0)]
                    public Int32 _0_Map_ID;
                    [Value(Name = "Preview Image", Offset = 4)]
                    public Dependancy _4_Preview_Image;
                    [Value(Name = "English Name", Offset = 12)]
                    public H2.DataTypes.String _12_English_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Japanese Name", Offset = 20)]
                    public H2.DataTypes.String _20_Japanese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "German Name", Offset = 28)]
                    public H2.DataTypes.String _28_German_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "French Name", Offset = 36)]
                    public H2.DataTypes.String _36_French_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Spanish Name", Offset = 44)]
                    public H2.DataTypes.String _44_Spanish_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Italian Name", Offset = 52)]
                    public H2.DataTypes.String _52_Italian_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Korean Name", Offset = 60)]
                    public H2.DataTypes.String _60_Korean_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Chinese Name", Offset = 68)]
                    public H2.DataTypes.String _68_Chinese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Portuguese Name", Offset = 76)]
                    public H2.DataTypes.String _76_Portuguese_Name = new H2.DataTypes.String((H2.DataTypes.Length)64, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "English Description", Offset = 84)]
                    public H2.DataTypes.String _84_English_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Japanese Description", Offset = 340)]
                    public H2.DataTypes.String _340_Japanese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "German Description", Offset = 596)]
                    public H2.DataTypes.String _596_German_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "French Description", Offset = 852)]
                    public H2.DataTypes.String _852_French_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Spanish Description", Offset = 1108)]
                    public H2.DataTypes.String _1108_Spanish_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Italian Description", Offset = 1364)]
                    public H2.DataTypes.String _1364_Italian_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Korean Description", Offset = 1620)]
                    public H2.DataTypes.String _1620_Korean_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Chinese Description", Offset = 1876)]
                    public H2.DataTypes.String _1876_Chinese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Portuguese Description", Offset = 2132)]
                    public H2.DataTypes.String _2132_Portuguese_Description = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Scenario Path", Offset = 2388)]
                    public H2.DataTypes.String _2388_Scenario_Path = new H2.DataTypes.String((H2.DataTypes.Length)256, System.Text.Encoding.BigEndianUnicode);
                    [Value(Name = "Sort Order", Offset = 2644)]
                    public Int32 _2644_Sort_Order;
                    [Value(Name = "Flags", Offset = 2648)]
                    public Bitmask _2648_Flags = new Bitmask(new string[] { "Unlockable" }, 4);
                    [Value(Name = "Max Teams None", Offset = 2652)]
                    public Byte _2652_Max_Teams_None;
                    [Value(Name = "Max Teams CTF", Offset = 2653)]
                    public Byte _2653_Max_Teams_CTF;
                    [Value(Name = "Max Teams Slayer", Offset = 2654)]
                    public Byte _2654_Max_Teams_Slayer;
                    [Value(Name = "Max Teams Oddball", Offset = 2655)]
                    public Byte _2655_Max_Teams_Oddball;
                    [Value(Name = "Max Teams KOTH", Offset = 2656)]
                    public Byte _2656_Max_Teams_KOTH;
                    [Value(Name = "Max Teams Race", Offset = 2657)]
                    public Byte _2657_Max_Teams_Race;
                    [Value(Name = "Max Teams Headhunter", Offset = 2658)]
                    public Byte _2658_Max_Teams_Headhunter;
                    [Value(Name = "Max Teams Juggernaught", Offset = 2659)]
                    public Byte _2659_Max_Teams_Juggernaught;
                    [Value(Name = "Max Teams Territories", Offset = 2660)]
                    public Byte _2660_Max_Teams_Territories;
                    [Value(Name = "Max Teams Assault", Offset = 2661)]
                    public Byte _2661_Max_Teams_Assault;
                    [Value(Name = "Max Teams Stub 10", Offset = 2662)]
                    public Byte _2662_Max_Teams_Stub_10;
                    [Value(Name = "Max Teams Stub 11", Offset = 2663)]
                    public Byte _2663_Max_Teams_Stub_11;
                    [Value(Name = "Max Teams Stub 12", Offset = 2664)]
                    public Byte _2664_Max_Teams_Stub_12;
                    [Value(Name = "Max Teams Stub 13", Offset = 2665)]
                    public Byte _2665_Max_Teams_Stub_13;
                    [Value(Name = "Max Teams Stub 14", Offset = 2666)]
                    public Byte _2666_Max_Teams_Stub_14;
                    [Value(Name = "Max Teams Stub 15", Offset = 2667)]
                    public Byte _2667_Max_Teams_Stub_15;
                    [Value(Offset = 2668)]
                    public Byte[] _2668_ = new byte[2];
                }
            }
            [Value(Name = "Territory Location Names", Offset = 928)]
            public Dependancy _928_Territory_Location_Names;
            [Value(Offset = 936)]
            public Byte[] _936_ = new byte[8];
            public _944_level_dialog_palette[] _944_Level_Dialog_Palette;
            [Serializable]
            [Reflexive(Name = "Level Dialog Palette", Offset = 944, ChunkSize = 8, Label = "")]
            public class _944_level_dialog_palette
            {
                private int __StartOffset__;
                [Value(Name = "Level Dialog", Offset = 0)]
                public Dependancy _0_Level_Dialog;
            }
            [Value(Name = "Location Names", Offset = 952)]
            public Dependancy _952_Location_Names;
            public _960_interpolators[] _960_Interpolators;
            [Serializable]
            [Reflexive(Name = "Interpolators", Offset = 960, ChunkSize = 24, Label = "")]
            public class _960_interpolators
            {
                private int __StartOffset__;
                [Value(Name = "Name", Offset = 0)]
                public StringID _0_Name;
                [Value(Name = "Accelerator Name", Offset = 4)]
                public StringID _4_Accelerator_Name;
                [Value(Name = "Multiplier Name", Offset = 8)]
                public StringID _8_Multiplier_Name;
                public _12_function[] _12_Function;
                [Serializable]
                [Reflexive(Name = "Function", Offset = 12, ChunkSize = 1, Label = "")]
                public class _12_function
                {
                    private int __StartOffset__;
                    [Value(Name = "Data", Offset = 0)]
                    public Byte _0_Data;
                }
                [Value(Offset = 20)]
                public Byte[] _20_ = new byte[4];
            }
            [Value(Offset = 968)]
            public Byte[] _968_ = new byte[8];
            public _976_screen_effects[] _976_Screen_Effects;
            [Serializable]
            [Reflexive(Name = "Screen Effects", Offset = 976, ChunkSize = 36, Label = "")]
            public class _976_screen_effects
            {
                private int __StartOffset__;
                [Value(Offset = 0)]
                public Byte[] _0_ = new byte[16];
                [Value(Name = "Screen Effect", Offset = 16)]
                public Dependancy _16_Screen_Effect;
                [Value(Name = "Name", Offset = 24)]
                public StringID _24_Name;
                [Value(Offset = 28)]
                public Byte[] _28_ = new byte[4];
                [Value(Name = "Unknown", Offset = 32)]
                public Single _32_Unknown;
            }
            public _984_predicted_resources[] _984_Predicted_Resources;
            [Serializable]
            [Reflexive(Name = "Predicted Resources", Offset = 984, ChunkSize = 4, Label = "")]
            public class _984_predicted_resources
            {
                private int __StartOffset__;
                [Value(Name = "Resource", Offset = 0)]
                public Int32 _0_Resource;
            }
        }
    }
}