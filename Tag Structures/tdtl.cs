using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("tdtl")]
        public class tdtl
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[2];
[Value(Name="Type", Offset=2)]
public H2.DataTypes.Enum _2_Type = new H2.DataTypes.Enum(new string[] {  "Standard", "Weapon To Projectile", "Projectile From Weapon" }, 2);
[Value(Name="Attachment Marker Name", Offset=4)]
public StringID _4_Attachment_Marker_Name;
[Value(Offset=8)]
public Byte[] _8_ = new byte[56];
[Value(Name="Falloff Distance From Camera", Offset=64)]
public Single _64_Falloff_Distance_From_Camera;
[Value(Name="Cutoff Distance From Camera", Offset=68)]
public Single _68_Cutoff_Distance_From_Camera;
[Value(Offset=72)]
public Byte[] _72_ = new byte[32];
public _104_arcs[] _104_Arcs;
[Serializable][Reflexive(Name="Arcs", Offset=104, ChunkSize=236, Label="")]
public class _104_arcs
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Basis Marker-Relative","Spread By External Input","Collide With Stuff","No Perspective Midpoints" }, 2);
[Value(Name="Sprite Count", Offset=2)]
public H2.DataTypes.Enum _2_Sprite_Count = new H2.DataTypes.Enum(new string[] {  "4 Sprites", "8 Sprites", "16 Sprites", "32 Sprites", "64 Sprites", "128 Sprites", "256 Sprites" }, 2);
[Value(Name="Natural Length", Offset=4)]
public Single _4_Natural_Length;
[Value(Name="Instances", Offset=8)]
public Int16 _8_Instances;
[Value(Offset=10)]
public Byte[] _10_ = new byte[2];
[Value(Name="Instance Spread Angle", Offset=12)]
public Single _12_Instance_Spread_Angle;
[Value(Name="Instance Rotation Period", Offset=16)]
public Single _16_Instance_Rotation_Period;
[Value(Offset=20)]
public Byte[] _20_ = new byte[8];
[Value(Name="Material Effects", Offset=28)]
public Dependancy _28_Material_Effects;
[Value(Name="Bitmap", Offset=36)]
public Dependancy _36_Bitmap;
[Value(Offset=44)]
public Byte[] _44_ = new byte[8];
public _52_horizontal_range[] _52_Horizontal_Range;
[Serializable][Reflexive(Name="Horizontal Range", Offset=52, ChunkSize=1, Label="")]
public class _52_horizontal_range
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _60_vertical_range[] _60_Vertical_Range;
[Serializable][Reflexive(Name="Vertical Range", Offset=60, ChunkSize=1, Label="")]
public class _60_vertical_range
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Vertical Negative Scale", Offset=68)]
public Single _68_Vertical_Negative_Scale;
public _72_roughness[] _72_Roughness;
[Serializable][Reflexive(Name="Roughness", Offset=72, ChunkSize=1, Label="")]
public class _72_roughness
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=80)]
public Byte[] _80_ = new byte[64];
[Value(Name="Octave 1 Frequency Cycles Per Sec", Offset=144)]
public Single _144_Octave_1_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 2 Frequency Cycles Per Sec", Offset=148)]
public Single _148_Octave_2_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 3 Frequency Cycles Per Sec", Offset=152)]
public Single _152_Octave_3_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 4 Frequency Cycles Per Sec", Offset=156)]
public Single _156_Octave_4_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 5 Frequency Cycles Per Sec", Offset=160)]
public Single _160_Octave_5_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 6 Frequency Cycles Per Sec", Offset=164)]
public Single _164_Octave_6_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 7 Frequency Cycles Per Sec", Offset=168)]
public Single _168_Octave_7_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 8 Frequency Cycles Per Sec", Offset=172)]
public Single _172_Octave_8_Frequency_Cycles_Per_Sec;
[Value(Name="Octave 9 Frequency Cycles Per Sec", Offset=176)]
public Single _176_Octave_9_Frequency_Cycles_Per_Sec;
[Value(Offset=180)]
public Byte[] _180_ = new byte[28];
[Value(Name="Octave Flags", Offset=208)]
public Bitmask _208_Octave_Flags = new Bitmask(new string[] { "Octave 1","Octave 2","Octave 3","Octave 4","Octave 5","Octave 6","Octave 7","Octave 8","Octave 9" }, 4);
public _212_cores[] _212_Cores;
[Serializable][Reflexive(Name="Cores", Offset=212, ChunkSize=56, Label="")]
public class _212_cores
{
private int __StartOffset__;
[Value(Name="Bitmap Index", Offset=0)]
public Int16 _0_Bitmap_Index;
[Value(Offset=2)]
public Byte[] _2_ = new byte[14];
public _16_thickness[] _16_Thickness;
[Serializable][Reflexive(Name="Thickness", Offset=16, ChunkSize=1, Label="")]
public class _16_thickness
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _24_color[] _24_Color;
[Serializable][Reflexive(Name="Color", Offset=24, ChunkSize=1, Label="")]
public class _24_color
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _32_brightness_time[] _32_Brightness_Time;
[Serializable][Reflexive(Name="Brightness/Time", Offset=32, ChunkSize=1, Label="")]
public class _32_brightness_time
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _40_brightness_facing[] _40_Brightness_Facing;
[Serializable][Reflexive(Name="Brightness/Facing", Offset=40, ChunkSize=1, Label="")]
public class _40_brightness_facing
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _48_along_axis_scale[] _48_Along_Axis_Scale;
[Serializable][Reflexive(Name="Along-Axis Scale", Offset=48, ChunkSize=1, Label="")]
public class _48_along_axis_scale
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
public _220_range_collision_scale[] _220_Range_Collision_Scale;
[Serializable][Reflexive(Name="Range-Collision Scale", Offset=220, ChunkSize=1, Label="")]
public class _220_range_collision_scale
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _228_brightness_collision_scale[] _228_Brightness_Collision_Scale;
[Serializable][Reflexive(Name="Brightness-Collision Scale", Offset=228, ChunkSize=1, Label="")]
public class _228_brightness_collision_scale
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
        }
    }
}