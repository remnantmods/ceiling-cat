using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("phmo")]
        public class phmo
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
[Value(Name="Object Weight", Offset=4)]
public Single _4_Object_Weight;
[Value(Name="Flags", Offset=8)]
public Bitmask _8_Flags = new Bitmask(new string[] { "Alive" }, 4);
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Offset=16)]
public Byte[] _16_ = new byte[24];
public _40_unknown9[] _40_Unknown9;
[Serializable][Reflexive(Name="Unknown9", Offset=40, ChunkSize=104, Label="")]
public class _40_unknown9
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="", Offset=8)]
public StringID _8_;
[Value(Name="", Offset=12)]
public StringID _12_;
[Value(Offset=16)]
public Byte[] _16_ = new byte[88];
}
public _48_unknown9[] _48_Unknown9;
[Serializable][Reflexive(Name="Unknown9", Offset=48, ChunkSize=24, Label="")]
public class _48_unknown9
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
public _8_unknown10[] _8_Unknown10;
[Serializable][Reflexive(Name="Unknown10", Offset=8, ChunkSize=12, Label="")]
public class _8_unknown10
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[12];
}
[Value(Name="", Offset=16)]
public StringID _16_;
[Value(Name="", Offset=20)]
public StringID _20_;
}
public _56_phmo_variations[] _56_Phmo_Variations;
[Serializable][Reflexive(Name="Phmo Variations", Offset=56, ChunkSize=144, Label="")]
public class _56_phmo_variations
{
private int __StartOffset__;
[Value(Name="index", Offset=0)]
public Int16 _0_index;
[Value(Name="Unknown", Offset=2)]
public Bitmask _2_Unknown = new Bitmask(new string[] {  }, 2);
[Value(Name="Unknown", Offset=4)]
public H2.DataTypes.Enum _4_Unknown = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Unknown", Offset=6)]
public Int16 _6_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Offset=36)]
public Byte[] _36_ = new byte[4];
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Offset=48)]
public Byte[] _48_ = new byte[4];
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Int32 _56_Unknown;
[Value(Name="Mass", Offset=60)]
public Single _60_Mass;
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Int32 _72_Unknown;
[Value(Offset=76)]
public Byte[] _76_ = new byte[4];
[Value(Name="Moment of Inertia about X axis", Offset=80)]
public Int32 _80_Moment_of_Inertia_about_X_axis;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Int32 _88_Unknown;
[Value(Offset=92)]
public Byte[] _92_ = new byte[4];
[Value(Name="Unknown", Offset=96)]
public Int32 _96_Unknown;
[Value(Name="Moment of Inertia about Y axis", Offset=100)]
public Int32 _100_Moment_of_Inertia_about_Y_axis;
[Value(Name="Unknown", Offset=104)]
public Int32 _104_Unknown;
[Value(Offset=108)]
public Byte[] _108_ = new byte[4];
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Moment of Inertia about Z axis", Offset=120)]
public Int32 _120_Moment_of_Inertia_about_Z_axis;
[Value(Name="Unknown", Offset=124)]
public Int32 _124_Unknown;
[Value(Offset=128)]
public Byte[] _128_ = new byte[4];
[Value(Name="Unknown", Offset=132)]
public Int32 _132_Unknown;
[Value(Name="Unknown", Offset=136)]
public Int32 _136_Unknown;
[Value(Offset=140)]
public Byte[] _140_ = new byte[4];
}
public _64_materials[] _64_Materials;
[Serializable][Reflexive(Name="Materials", Offset=64, ChunkSize=12, Label="")]
public class _64_materials
{
private int __StartOffset__;
[Value(Name="Material Base", Offset=0)]
public StringID _0_Material_Base;
[Value(Name="Material", Offset=4)]
public StringID _4_Material;
[Value(Name="Unknown", Offset=8)]
public StringID _8_Unknown;
}
public _72_unknown48[] _72_Unknown48;
[Serializable][Reflexive(Name="Unknown48", Offset=72, ChunkSize=128, Label="")]
public class _72_unknown48
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Int32 _52_Unknown;
[Value(Offset=56)]
public Byte[] _56_ = new byte[4];
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Offset=68)]
public Byte[] _68_ = new byte[16];
[Value(Name="Unknown", Offset=84)]
public Single _84_Unknown;
[Value(Offset=88)]
public Byte[] _88_ = new byte[16];
[Value(Name="Unknown", Offset=104)]
public Single _104_Unknown;
[Value(Offset=108)]
public Byte[] _108_ = new byte[4];
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Offset=124)]
public Byte[] _124_ = new byte[4];
}
[Value(Offset=80)]
public Byte[] _80_ = new byte[8];
public _88_powered_mass_point[] _88_Powered_Mass_Point;
[Serializable][Reflexive(Name="Powered Mass Point", Offset=88, ChunkSize=80, Label="")]
public class _88_powered_mass_point
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Power Lift","Air Lift" }, 4);
[Value(Name="Antigrav strength", Offset=8)]
public Single _8_Antigrav_strength;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Offset=48)]
public Byte[] _48_ = new byte[8];
[Value(Name="Unknown", Offset=56)]
public Int32 _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Offset=64)]
public Byte[] _64_ = new byte[8];
[Value(Name="Unknown", Offset=72)]
public Single _72_Unknown;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
}
public _96_mass_points[] _96_Mass_Points;
[Serializable][Reflexive(Name="Mass Points", Offset=96, ChunkSize=144, Label="")]
public class _96_mass_points
{
private int __StartOffset__;
[Value(Name="Physics ID", Offset=0)]
public StringID _0_Physics_ID;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Friction", Offset=12)]
public Single _12_Friction;
[Value(Name="Bounce Factor", Offset=16)]
public Single _16_Bounce_Factor;
[Value(Name="Volume", Offset=20)]
public Int32 _20_Volume;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Offset=44)]
public Byte[] _44_ = new byte[4];
[Value(Name="x halfspace", Offset=48)]
public Single _48_x_halfspace;
[Value(Name="y halfspace", Offset=52)]
public Single _52_y_halfspace;
[Value(Name="z halfspace", Offset=56)]
public Single _56_z_halfspace;
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Offset=72)]
public Byte[] _72_ = new byte[4];
[Value(Name="Unknown", Offset=76)]
public Int32 _76_Unknown;
[Value(Name="Unknown", Offset=80)]
public Int32 _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Int32 _88_Unknown;
[Value(Offset=92)]
public Byte[] _92_ = new byte[4];
[Value(Name="Unknown", Offset=96)]
public Int32 _96_Unknown;
[Value(Name="Unknown", Offset=100)]
public Int32 _100_Unknown;
[Value(Name="Unknown", Offset=104)]
public Int32 _104_Unknown;
[Value(Offset=108)]
public Byte[] _108_ = new byte[4];
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Offset=124)]
public Byte[] _124_ = new byte[4];
[Value(Name="Unknown", Offset=128)]
public Int32 _128_Unknown;
[Value(Name="Unknown", Offset=132)]
public Int32 _132_Unknown;
[Value(Name="Unknown", Offset=136)]
public Int32 _136_Unknown;
[Value(Offset=140)]
public Byte[] _140_ = new byte[4];
}
[Value(Offset=104)]
public Byte[] _104_ = new byte[8];
public _112_vehicle_parts[] _112_Vehicle_Parts;
[Serializable][Reflexive(Name="Vehicle Parts", Offset=112, ChunkSize=256, Label="")]
public class _112_vehicle_parts
{
private int __StartOffset__;
[Value(Name="Physics ID", Offset=0)]
public StringID _0_Physics_ID;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] {  }, 4);
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Mass", Offset=24)]
public Single _24_Mass;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Offset=44)]
public Byte[] _44_ = new byte[4];
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Int32 _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Int32 _56_Unknown;
[Value(Offset=60)]
public Byte[] _60_ = new byte[4];
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Int32 _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Int32 _72_Unknown;
[Value(Offset=76)]
public Byte[] _76_ = new byte[4];
[Value(Name="Unknown", Offset=80)]
public Int32 _80_Unknown;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Name="Unknown", Offset=88)]
public Int32 _88_Unknown;
[Value(Offset=92)]
public Byte[] _92_ = new byte[4];
[Value(Name="Unknown", Offset=96)]
public Int32 _96_Unknown;
[Value(Name="Unknown", Offset=100)]
public Int32 _100_Unknown;
[Value(Name="Unknown", Offset=104)]
public Int32 _104_Unknown;
[Value(Name="Unknown", Offset=108)]
public Int32 _108_Unknown;
[Value(Name="Unknown", Offset=112)]
public Int32 _112_Unknown;
[Value(Name="Unknown", Offset=116)]
public Int32 _116_Unknown;
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Name="Unknown", Offset=124)]
public Int32 _124_Unknown;
[Value(Name="Unknown", Offset=128)]
public Int32 _128_Unknown;
[Value(Name="Unknown", Offset=132)]
public Int32 _132_Unknown;
[Value(Name="Unknown", Offset=136)]
public Int32 _136_Unknown;
[Value(Name="Unknown", Offset=140)]
public Int32 _140_Unknown;
[Value(Name="Unknown", Offset=144)]
public Int32 _144_Unknown;
[Value(Name="Unknown", Offset=148)]
public Int32 _148_Unknown;
[Value(Name="Unknown", Offset=152)]
public Int32 _152_Unknown;
[Value(Name="Unknown", Offset=156)]
public Int32 _156_Unknown;
[Value(Name="Unknown", Offset=160)]
public Int32 _160_Unknown;
[Value(Name="Unknown", Offset=164)]
public Int32 _164_Unknown;
[Value(Name="Unknown", Offset=168)]
public Int32 _168_Unknown;
[Value(Name="Unknown", Offset=172)]
public Int32 _172_Unknown;
[Value(Name="Unknown", Offset=176)]
public Int32 _176_Unknown;
[Value(Name="Unknown", Offset=180)]
public Int32 _180_Unknown;
[Value(Name="Unknown", Offset=184)]
public Int32 _184_Unknown;
[Value(Name="Unknown", Offset=188)]
public Int32 _188_Unknown;
[Value(Name="Unknown", Offset=192)]
public Int32 _192_Unknown;
[Value(Name="Unknown", Offset=196)]
public Int32 _196_Unknown;
[Value(Name="Unknown", Offset=200)]
public Int32 _200_Unknown;
[Value(Name="Unknown", Offset=204)]
public Int32 _204_Unknown;
[Value(Name="Unknown", Offset=208)]
public Int32 _208_Unknown;
[Value(Name="Unknown", Offset=212)]
public Int32 _212_Unknown;
[Value(Name="Unknown", Offset=216)]
public Int32 _216_Unknown;
[Value(Name="Unknown", Offset=220)]
public Int32 _220_Unknown;
[Value(Name="Unknown", Offset=224)]
public Int32 _224_Unknown;
[Value(Name="Unknown", Offset=228)]
public Int32 _228_Unknown;
[Value(Name="Unknown", Offset=232)]
public Int32 _232_Unknown;
[Value(Name="Unknown", Offset=236)]
public Int32 _236_Unknown;
[Value(Name="Unknown", Offset=240)]
public Int32 _240_Unknown;
[Value(Name="Unknown", Offset=244)]
public Int32 _244_Unknown;
[Value(Name="Unknown", Offset=248)]
public Int32 _248_Unknown;
[Value(Name="Unknown", Offset=252)]
public Int32 _252_Unknown;
}
public _120_scalers[] _120_Scalers;
[Serializable][Reflexive(Name="Scalers", Offset=120, ChunkSize=48, Label="")]
public class _120_scalers
{
private int __StartOffset__;
[Value(Name="Length Vector i", Offset=0)]
public Single _0_Length_Vector_i;
[Value(Name="Length Vector j", Offset=4)]
public Single _4_Length_Vector_j;
[Value(Name="Length Vector k", Offset=8)]
public Single _8_Length_Vector_k;
[Value(Name="Length Vector w", Offset=12)]
public Single _12_Length_Vector_w;
[Value(Name="Width Vector i", Offset=16)]
public Single _16_Width_Vector_i;
[Value(Name="Width Vector j", Offset=20)]
public Single _20_Width_Vector_j;
[Value(Name="Width Vector k", Offset=24)]
public Single _24_Width_Vector_k;
[Value(Name="Width Vector w", Offset=28)]
public Single _28_Width_Vector_w;
[Value(Name="Height Vector i", Offset=32)]
public Single _32_Height_Vector_i;
[Value(Name="Height Vector j", Offset=36)]
public Single _36_Height_Vector_j;
[Value(Name="Height Vector k", Offset=40)]
public Single _40_Height_Vector_k;
[Value(Name="Height Vector w", Offset=44)]
public Single _44_Height_Vector_w;
}
public _128_planes[] _128_Planes;
[Serializable][Reflexive(Name="Planes", Offset=128, ChunkSize=16, Label="")]
public class _128_planes
{
private int __StartOffset__;
[Value(Name="X", Offset=0)]
public Single _0_X;
[Value(Name="y", Offset=4)]
public Single _4_y;
[Value(Name="z", Offset=8)]
public Single _8_z;
[Value(Name="d", Offset=12)]
public Single _12_d;
}
[Value(Offset=136)]
public Byte[] _136_ = new byte[8];
public _144_unknown223[] _144_Unknown223;
[Serializable][Reflexive(Name="Unknown223", Offset=144, ChunkSize=56, Label="")]
public class _144_unknown223
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Offset=28)]
public Byte[] _28_ = new byte[4];
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Offset=36)]
public Byte[] _36_ = new byte[4];
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Offset=44)]
public Byte[] _44_ = new byte[4];
[Value(Name="Unknown", Offset=48)]
public Int32 _48_Unknown;
[Value(Offset=52)]
public Byte[] _52_ = new byte[4];
}
public _152_unknown239[] _152_Unknown239;
[Serializable][Reflexive(Name="Unknown239", Offset=152, ChunkSize=8, Label="")]
public class _152_unknown239
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Offset=4)]
public Byte[] _4_ = new byte[4];
}
public _160_unknown243[] _160_Unknown243;
[Serializable][Reflexive(Name="Unknown243", Offset=160, ChunkSize=24, Label="")]
public class _160_unknown243
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
}
public _168_unknown253[] _168_Unknown253;
[Serializable][Reflexive(Name="Unknown253", Offset=168, ChunkSize=1, Label="")]
public class _168_unknown253
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Byte _0_Unknown;
}
[Value(Offset=176)]
public Byte[] _176_ = new byte[8];
public _184_unknown25[] _184_Unknown25;
[Serializable][Reflexive(Name="Unknown25", Offset=184, ChunkSize=148, Label="")]
public class _184_unknown25
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Offset=4)]
public Byte[] _4_ = new byte[144];
}
public _192_damage_permutations[] _192_Damage_Permutations;
[Serializable][Reflexive(Name="Damage Permutations", Offset=192, ChunkSize=12, Label="")]
public class _192_damage_permutations
{
private int __StartOffset__;
[Value(Name="Part", Offset=0)]
public StringID _0_Part;
public _4_damage_levels[] _4_Damage_Levels;
[Serializable][Reflexive(Name="Damage Levels", Offset=4, ChunkSize=12, Label="")]
public class _4_damage_levels
{
private int __StartOffset__;
[Value(Name="Level of Damage", Offset=0)]
public StringID _0_Level_of_Damage;
public _4_phmo_variation[] _4_Phmo_Variation;
[Serializable][Reflexive(Name="Phmo Variation", Offset=4, ChunkSize=2, Label="")]
public class _4_phmo_variation
{
private int __StartOffset__;
[Value(Name="Phmo Variation Chunk#", Offset=0)]
public Int16 _0_Phmo_Variation_ChunkNo;
}
}
}
public _200_bones[] _200_Bones;
[Serializable][Reflexive(Name="Bones", Offset=200, ChunkSize=12, Label="")]
public class _200_bones
{
private int __StartOffset__;
[Value(Name="Bone", Offset=0)]
public StringID _0_Bone;
[Value(Name="Parent", Offset=4)]
public Int16 _4_Parent;
[Value(Name="Child", Offset=6)]
public Int16 _6_Child;
[Value(Name="Next Sibling", Offset=8)]
public Int16 _8_Next_Sibling;
[Value(Name="Final Sibling?", Offset=10)]
public Int16 _10_Final_Sibling_;
}
[Value(Offset=208)]
public Byte[] _208_ = new byte[24];
public _232_hinges[] _232_Hinges;
[Serializable][Reflexive(Name="Hinges", Offset=232, ChunkSize=132, Label="")]
public class _232_hinges
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Start Permutation", Offset=4)]
public Int16 _4_Start_Permutation;
[Value(Name="End Permutation", Offset=6)]
public Int16 _6_End_Permutation;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[120];
}
public _240_unknown268[] _240_Unknown268;
[Serializable][Reflexive(Name="Unknown268", Offset=240, ChunkSize=120, Label="")]
public class _240_unknown268
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Offset=4)]
public Byte[] _4_ = new byte[116];
}
[Value(Offset=248)]
public Byte[] _248_ = new byte[16];
public _264_unknown264[] _264_Unknown264;
[Serializable][Reflexive(Name="Unknown264", Offset=264, ChunkSize=32, Label="")]
public class _264_unknown264
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[32];
}
        }
    }
}