using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("egor")]
        public class egor
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[64];
[Value(Name="Shader", Offset=64)]
public Dependancy _64_Shader;
[Value(Offset=68)]
public Byte[] _68_ = new byte[64];
public _132_pass_references[] _132_Pass_References;
[Serializable][Reflexive(Name="Pass References", Offset=132, ChunkSize=172, Label="")]
public class _132_pass_references
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
[Value(Name="Layer Pass Index", Offset=8)]
public Int16 _8_Layer_Pass_Index;
[Value(Name="If Primary Equals 0 And Secondary Equals 0", Offset=10)]
public Int16 _10_If_Primary_Equals_0_And_Secondary_Equals_0;
[Value(Name="If Primary Greater Than 0 And Secondary Equals 0", Offset=12)]
public Int16 _12_If_Primary_Greater_Than_0_And_Secondary_Equals_0;
[Value(Name="If Primary Equals 0 And Secondary Greater Than 0", Offset=14)]
public Int16 _14_If_Primary_Equals_0_And_Secondary_Greater_Than_0;
[Value(Name="If Primary Greater Than 0 And Secondary Greater Than 0", Offset=16)]
public Int16 _16_If_Primary_Greater_Than_0_And_Secondary_Greater_Than_0;
[Value(Offset=18)]
public Byte[] _18_ = new byte[62];
[Value(Name="Stage 0 Mode", Offset=80)]
public H2.DataTypes.Enum _80_Stage_0_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Viewport Normalized", "Viewport Relative", "Frame Buffer Relative", "Zero" }, 2);
[Value(Name="Stage 1 Mode", Offset=82)]
public H2.DataTypes.Enum _82_Stage_1_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Viewport Normalized", "Viewport Relative", "Frame Buffer Relative", "Zero" }, 2);
[Value(Name="Stage 2 Mode", Offset=84)]
public H2.DataTypes.Enum _84_Stage_2_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Viewport Normalized", "Viewport Relative", "Frame Buffer Relative", "Zero" }, 2);
[Value(Name="Stage 3 Mode", Offset=86)]
public H2.DataTypes.Enum _86_Stage_3_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Viewport Normalized", "Viewport Relative", "Frame Buffer Relative", "Zero" }, 2);
public _88_advanced_control[] _88_Advanced_Control;
[Serializable][Reflexive(Name="Advanced Control", Offset=88, ChunkSize=0, Label="")]
public class _88_advanced_control
{
private int __StartOffset__;
}
[Value(Name="Target", Offset=96)]
public H2.DataTypes.Enum _96_Target = new H2.DataTypes.Enum(new string[] {  "Frame Buffer", "Texaccum", "Texaccum Small", "Texaccum Tiny", "Copy FB To Texaccum" }, 4);
[Value(Offset=100)]
public Byte[] _100_ = new byte[64];
public _164_convolution[] _164_Convolution;
[Serializable][Reflexive(Name="Convolution", Offset=164, ChunkSize=92, Label="")]
public class _164_convolution
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[64];
[Value(Name="Flags", Offset=64)]
public Bitmask _64_Flags = new Bitmask(new string[] { "Only When Primary Is Active","Only When Secondary Is Active","Predator Zoom" }, 4);
[Value(Name="Convolution Amount", Offset=68)]
public Single _68_Convolution_Amount;
[Value(Name="Filter Scale", Offset=72)]
public Single _72_Filter_Scale;
[Value(Name="Filter Box Factor", Offset=76)]
public Single _76_Filter_Box_Factor;
[Value(Name="Zoom Falloff Radius", Offset=80)]
public Single _80_Zoom_Falloff_Radius;
[Value(Name="Zoom Cutoff Radius", Offset=84)]
public Single _84_Zoom_Cutoff_Radius;
[Value(Name="Resolution Scale", Offset=88)]
public Single _88_Resolution_Scale;
}
}
        }
    }
}