using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("DECR")]
        public class DECR
        {
public _0_shaders[] _0_Shaders;
[Serializable][Reflexive(Name="Shaders", Offset=0, ChunkSize=8, Label="")]
public class _0_shaders
{
private int __StartOffset__;
[Value(Name="Shader", Offset=0)]
public Dependancy _0_Shader;
}
[Value(Name="Lighting Min Scale", Offset=8)]
public Single _8_Lighting_Min_Scale;
[Value(Name="Lighting Max Scale", Offset=12)]
public Single _12_Lighting_Max_Scale;
public _16_classes[] _16_Classes;
[Serializable][Reflexive(Name="Classes", Offset=16, ChunkSize=20, Label="")]
public class _16_classes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Type", Offset=4)]
public H2.DataTypes.Enum _4_Type = new H2.DataTypes.Enum(new string[] {  "Model", "Floating Decal", "Projected Decal", "Screen Facing Quad", "Axis Rotating Quad", "Cross Quad" }, 4);
[Value(Name="Scale", Offset=8)]
public Single _8_Scale;
public _12_permutations[] _12_Permutations;
[Serializable][Reflexive(Name="Permutations", Offset=12, ChunkSize=40, Label="")]
public class _12_permutations
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Shader #", Offset=4)]
public Int16 _4_Shader_No;
[Value(Name="Flags", Offset=6)]
public Bitmask _6_Flags = new Bitmask(new string[] { "Align To Normal","Only On Ground","Upright" }, 1);
[Value(Name="Fade Distance", Offset=7)]
public H2.DataTypes.Enum _7_Fade_Distance = new H2.DataTypes.Enum(new string[] {  "Close", "Medium", "Far" }, 1);
[Value(Name="Index", Offset=8)]
public Int16 _8_Index;
[Value(Name="Distribution Weight", Offset=10)]
public Int16 _10_Distribution_Weight;
[Value(Name="Scale", Offset=12)]
public Single _12_Scale;
[Value(Name="...To", Offset=16)]
public Single _16____To;
[Value(Name="Tint 1 R", Offset=20)]
public Byte _20_Tint_1_R;
[Value(Name="Tint 1 G", Offset=21)]
public Byte _21_Tint_1_G;
[Value(Name="Tint 1 B", Offset=22)]
public Byte _22_Tint_1_B;
[Value(Offset=23)]
public Byte[] _23_ = new byte[1];
[Value(Name="Tint 2 R", Offset=24)]
public Byte _24_Tint_2_R;
[Value(Name="Tint 2 G", Offset=25)]
public Byte _25_Tint_2_G;
[Value(Name="Tint 2 B", Offset=26)]
public Byte _26_Tint_2_B;
[Value(Offset=27)]
public Byte[] _27_ = new byte[1];
[Value(Name="Base Map Tint Percentage", Offset=28)]
public Single _28_Base_Map_Tint_Percentage;
[Value(Name="Lightmap Tint Percentage", Offset=32)]
public Single _32_Lightmap_Tint_Percentage;
[Value(Name="Wind Scale", Offset=36)]
public Single _36_Wind_Scale;
}
}
public _24_models[] _24_Models;
[Serializable][Reflexive(Name="Models", Offset=24, ChunkSize=8, Label="")]
public class _24_models
{
private int __StartOffset__;
[Value(Name="Model Name", Offset=0)]
public StringID _0_Model_Name;
[Value(Name="Index Start", Offset=4)]
public Int16 _4_Index_Start;
[Value(Name="Index Count", Offset=6)]
public Int16 _6_Index_Count;
}
public _32_raw_vertices[] _32_Raw_Vertices;
[Serializable][Reflexive(Name="Raw Vertices", Offset=32, ChunkSize=56, Label="")]
public class _32_raw_vertices
{
private int __StartOffset__;
[Value(Name="X", Offset=0)]
public Single _0_X;
[Value(Name="Y", Offset=4)]
public Single _4_Y;
[Value(Name="Z", Offset=8)]
public Single _8_Z;
[Value(Name="Normal i", Offset=12)]
public Single _12_Normal_i;
[Value(Name="Normal j", Offset=16)]
public Single _16_Normal_j;
[Value(Name="Normal k", Offset=20)]
public Single _20_Normal_k;
[Value(Name="Tangent i", Offset=24)]
public Single _24_Tangent_i;
[Value(Name="Tangent j", Offset=28)]
public Single _28_Tangent_j;
[Value(Name="Tangent k", Offset=32)]
public Single _32_Tangent_k;
[Value(Name="Binormal i", Offset=36)]
public Single _36_Binormal_i;
[Value(Name="Binormal j", Offset=40)]
public Single _40_Binormal_j;
[Value(Name="Binormal k", Offset=44)]
public Single _44_Binormal_k;
[Value(Name="U", Offset=48)]
public Single _48_U;
[Value(Name="V", Offset=52)]
public Single _52_V;
}
public _40_indices[] _40_Indices;
[Serializable][Reflexive(Name="Indices", Offset=40, ChunkSize=2, Label="")]
public class _40_indices
{
private int __StartOffset__;
[Value(Name="Index", Offset=0)]
public Int16 _0_Index;
}
public _48_cached_data[] _48_Cached_Data;
[Serializable][Reflexive(Name="Cached Data", Offset=48, ChunkSize=0, Label="")]
public class _48_cached_data
{
private int __StartOffset__;
}
[Value(Name="Block Offset", Offset=56)]
public UInt32 _56_Block_Offset;
[Value(Name="Block Size", Offset=60)]
public UInt32 _60_Block_Size;
[Value(Name="Section Data Size", Offset=64)]
public UInt32 _64_Section_Data_Size;
[Value(Name="Resource Data Size", Offset=68)]
public UInt32 _68_Resource_Data_Size;
public _72_resources[] _72_Resources;
[Serializable][Reflexive(Name="Resources", Offset=72, ChunkSize=16, Label="")]
public class _72_resources
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public H2.DataTypes.Enum _0_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Name="Primary Locator", Offset=4)]
public Int16 _4_Primary_Locator;
[Value(Name="Secondary Locator", Offset=6)]
public Int16 _6_Secondary_Locator;
[Value(Name="Data Size", Offset=8)]
public UInt32 _8_Data_Size;
[Value(Name="Data Offset", Offset=12)]
public UInt32 _12_Data_Offset;
}
[Value(Name="Owner Tag Section Offset", Offset=80)]
public Int16 _80_Owner_Tag_Section_Offset;
[Value(Offset=82)]
public Byte[] _82_ = new byte[24];
        }
    }
}