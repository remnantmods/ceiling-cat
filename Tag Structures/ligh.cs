using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("ligh")]
        public class ligh
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "No Illumination","No Specular","Force Cast Environment Shadow","No Shadow","Force Frustum Visibility On Small","Only Render In 1st Person","Only Render In 3rd Person","Don't Fade When Invisible","Multiplayer Override","Animated Gel","Only In Dynamic Envmap","Ignore Parent Object","Don't Show Parent","Ignore All Parents","March Milestone Hack","Force Light Inside World","Environment Doesn't Cast Stencil Shadow","1st Person From Camera","Texture Camera Gel","Light Framerate Killer","Allowed In Splitscreen","Only On Parent Bipeds" }, 4);
[Value(Name="Type", Offset=4)]
public H2.DataTypes.Enum _4_Type = new H2.DataTypes.Enum(new string[] {  "Sphere", "Orthogonal", "Projective", "Pyramid" }, 4);
[Value(Name="Size Modifier", Offset=8)]
public Single _8_Size_Modifier;
[Value(Name="...To", Offset=12)]
public Single _12____To;
[Value(Name="Shadow Quality Bias", Offset=16)]
public Single _16_Shadow_Quality_Bias;
[Value(Name="Shadow Tap Bias", Offset=20)]
public H2.DataTypes.Enum _20_Shadow_Tap_Bias = new H2.DataTypes.Enum(new string[] {  "3 Tap", "unused", "1 Tap" }, 4);
[Value(Name="Sphere Light Radius", Offset=24)]
public Single _24_Sphere_Light_Radius;
[Value(Name="Sphere Light Specular Radius", Offset=28)]
public Single _28_Sphere_Light_Specular_Radius;
[Value(Name="Frustum Light Near Width", Offset=32)]
public Single _32_Frustum_Light_Near_Width;
[Value(Name="Frustum Light Height Stretch", Offset=36)]
public Single _36_Frustum_Light_Height_Stretch;
[Value(Name="Frustum Light Field Of View", Offset=40)]
public Single _40_Frustum_Light_Field_Of_View;
[Value(Name="Frustum Light Falloff Distance", Offset=44)]
public Single _44_Frustum_Light_Falloff_Distance;
[Value(Name="Frustum Light Cutoff Distance", Offset=48)]
public Single _48_Frustum_Light_Cutoff_Distance;
[Value(Name="Interpolation Flags", Offset=52)]
public Bitmask _52_Interpolation_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Bloom Bounds", Offset=56)]
public Single _56_Bloom_Bounds;
[Value(Name="..To", Offset=60)]
public Single _60___To;
[Value(Name="Specular Lower Bound R", Offset=64)]
public Single _64_Specular_Lower_Bound_R;
[Value(Name="Specular Lower Bound G", Offset=68)]
public Single _68_Specular_Lower_Bound_G;
[Value(Name="Specular Lower Bound B", Offset=72)]
public Single _72_Specular_Lower_Bound_B;
[Value(Name="Specular Upper Bound R", Offset=76)]
public Single _76_Specular_Upper_Bound_R;
[Value(Name="Specular Upper Bound G", Offset=80)]
public Single _80_Specular_Upper_Bound_G;
[Value(Name="Specular Upper Bound B", Offset=84)]
public Single _84_Specular_Upper_Bound_B;
[Value(Name="Diffuse Lower Bound R", Offset=88)]
public Single _88_Diffuse_Lower_Bound_R;
[Value(Name="Diffuse Lower Bound G", Offset=92)]
public Single _92_Diffuse_Lower_Bound_G;
[Value(Name="Diffuse Lower Bound B", Offset=96)]
public Single _96_Diffuse_Lower_Bound_B;
[Value(Name="Diffuse Upper Bound R", Offset=100)]
public Single _100_Diffuse_Upper_Bound_R;
[Value(Name="Diffuse Upper Bound G", Offset=104)]
public Single _104_Diffuse_Upper_Bound_G;
[Value(Name="Diffuse Upper Bound B", Offset=108)]
public Single _108_Diffuse_Upper_Bound_B;
[Value(Name="Brightness Bounds", Offset=112)]
public Single _112_Brightness_Bounds;
[Value(Name="...To", Offset=116)]
public Single _116____To;
[Value(Name="Gel Map", Offset=120)]
public Dependancy _120_Gel_Map;
[Value(Name="Specular Mask", Offset=124)]
public H2.DataTypes.Enum _124_Specular_Mask = new H2.DataTypes.Enum(new string[] {  "Default", "None (no mask)", "Gel Alpha", "Ge Color" }, 4);
[Value(Offset=128)]
public Byte[] _128_ = new byte[4];
[Value(Name="Falloff Function", Offset=132)]
public H2.DataTypes.Enum _132_Falloff_Function = new H2.DataTypes.Enum(new string[] {  "Default", "Narrow", "Broad", "Very Broad" }, 2);
[Value(Name="Diffuse Contrast", Offset=134)]
public H2.DataTypes.Enum _134_Diffuse_Contrast = new H2.DataTypes.Enum(new string[] {  "Default (linear)", "High", "Low", "Very Low" }, 2);
[Value(Name="Specular Contrast", Offset=136)]
public H2.DataTypes.Enum _136_Specular_Contrast = new H2.DataTypes.Enum(new string[] {  "Default (none)", "High (linear)", "Low", "Very Low" }, 2);
[Value(Name="Falloff Geometry", Offset=138)]
public H2.DataTypes.Enum _138_Falloff_Geometry = new H2.DataTypes.Enum(new string[] {  "Default", "Directional", "Spherical" }, 2);
[Value(Name="Lens Flare", Offset=140)]
public Dependancy _140_Lens_Flare;
[Value(Name="Bounding Radius", Offset=144)]
public Single _144_Bounding_Radius;
[Value(Name="Light Volume", Offset=148)]
public Dependancy _148_Light_Volume;
[Value(Name="Default Lightmap Setting", Offset=152)]
public H2.DataTypes.Enum _152_Default_Lightmap_Setting = new H2.DataTypes.Enum(new string[] {  "Dynamic Only", "Dynamic With Lightmaps", "Lightmaps Only" }, 4);
[Value(Name="Lightmap Half Life", Offset=156)]
public Single _156_Lightmap_Half_Life;
[Value(Name="Lightmap Light Scale", Offset=160)]
public Single _160_Lightmap_Light_Scale;
[Value(Name="Duration", Offset=164)]
public Single _164_Duration;
[Value(Name="Falloff Function", Offset=168)]
public H2.DataTypes.Enum _168_Falloff_Function = new H2.DataTypes.Enum(new string[] {  "Linear", "Late", "Very Late", "Early", "Very Early", "Cosine", "Zero", "One" }, 2);
[Value(Offset=170)]
public Byte[] _170_ = new byte[2];
[Value(Name="Illumination Fade", Offset=172)]
public H2.DataTypes.Enum _172_Illumination_Fade = new H2.DataTypes.Enum(new string[] {  "Fade Very Far", "Fade Far", "Fade Medium", "Fade Close", "Fade Very Close" }, 2);
[Value(Name="Shadow Fade", Offset=174)]
public H2.DataTypes.Enum _174_Shadow_Fade = new H2.DataTypes.Enum(new string[] {  "Fade Very Far", "Fade Far", "Fade Medium", "Fade Close", "Fade Very Close" }, 2);
[Value(Name="Specular Fade", Offset=176)]
public H2.DataTypes.Enum _176_Specular_Fade = new H2.DataTypes.Enum(new string[] {  "Fade Very Far", "Fade Far", "Fade Medium", "Fade Close", "Fade Very Close" }, 2);
[Value(Offset=178)]
public Byte[] _178_ = new byte[2];
[Value(Name="Flags", Offset=180)]
public Bitmask _180_Flags = new Bitmask(new string[] { "Synchronized" }, 4);
public _184_brightness_animation[] _184_Brightness_Animation;
[Serializable][Reflexive(Name="Brightness Animation", Offset=184, ChunkSize=76, Label="")]
public class _184_brightness_animation
{
private int __StartOffset__;
public _0_function[] _0_Function;
[Serializable][Reflexive(Name="Function", Offset=0, ChunkSize=1, Label="")]
public class _0_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=8)]
public Byte[] _8_ = new byte[68];
}
public _192_color_animation[] _192_Color_Animation;
[Serializable][Reflexive(Name="Color Animation", Offset=192, ChunkSize=188, Label="")]
public class _192_color_animation
{
private int __StartOffset__;
public _0_function[] _0_Function;
[Serializable][Reflexive(Name="Function", Offset=0, ChunkSize=1, Label="")]
public class _0_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Offset=8)]
public Byte[] _8_ = new byte[180];
}
public _200_gel_animation[] _200_Gel_Animation;
[Serializable][Reflexive(Name="Gel Animation", Offset=200, ChunkSize=16, Label="")]
public class _200_gel_animation
{
private int __StartOffset__;
public _0_function[] _0_Function;
[Serializable][Reflexive(Name="Function", Offset=0, ChunkSize=1, Label="")]
public class _0_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _8_function[] _8_Function;
[Serializable][Reflexive(Name="Function", Offset=8, ChunkSize=1, Label="")]
public class _8_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
[Value(Name="Shader", Offset=208)]
public Dependancy _208_Shader;
        }
    }
}