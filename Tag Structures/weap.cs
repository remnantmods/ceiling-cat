using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("weap")]
        public class weap
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Does Not Cast Shadow","Search Cardinal Direction Lightmaps","unused","Not A Pathfinding Obstacle","Extension Of Parent","Does Not Cause Collision Damage","Early Mover","Early Mover Localized Physics","Use Static Massive Lightmap Sample","Object Scales Attachments","Inherits Player's Appearance","Dead Bipeds Can't Localize","Attach To Clusters By Dynamic Sphere","Effects Created By This Object Do Not" }, 4);
[Value(Name="Bounding Radius", Offset=4)]
public Single _4_Bounding_Radius;
[Value(Name="Bounding Offset X", Offset=8)]
public Single _8_Bounding_Offset_X;
[Value(Name="Bounding Offset Y", Offset=12)]
public Single _12_Bounding_Offset_Y;
[Value(Name="Bounding Offset Z", Offset=16)]
public Single _16_Bounding_Offset_Z;
[Value(Name="Acceleration Scale", Offset=20)]
public Single _20_Acceleration_Scale;
[Value(Name="Lightmap Shadow Mode", Offset=24)]
public H2.DataTypes.Enum _24_Lightmap_Shadow_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Never", "Always" }, 4);
[Value(Name="Sweetener Size", Offset=28)]
public H2.DataTypes.Enum _28_Sweetener_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Medium", "Large" }, 4);
[Value(Name="Dynamic Light Sphere Radius", Offset=32)]
public Single _32_Dynamic_Light_Sphere_Radius;
[Value(Name="Dynamic Light Sphere Offset X", Offset=36)]
public Single _36_Dynamic_Light_Sphere_Offset_X;
[Value(Name="Dynamic Light Sphere Offset Y", Offset=40)]
public Single _40_Dynamic_Light_Sphere_Offset_Y;
[Value(Name="Dynamic Light Sphere Offset Z", Offset=44)]
public Single _44_Dynamic_Light_Sphere_Offset_Z;
[Value(Name="Default Model Variant", Offset=48)]
public StringID _48_Default_Model_Variant;
[Value(Name="Model", Offset=52)]
public Dependancy _52_Model;
[Value(Name="Crate Object", Offset=56)]
public Dependancy _56_Crate_Object;
[Value(Name="Modifier Shader", Offset=60)]
public Dependancy _60_Modifier_Shader;
[Value(Name="Creation Effect", Offset=64)]
public Dependancy _64_Creation_Effect;
[Value(Name="Material Effects", Offset=68)]
public Dependancy _68_Material_Effects;
public _72_ai_properties[] _72_Ai_Properties;
[Serializable][Reflexive(Name="Ai Properties", Offset=72, ChunkSize=16, Label="")]
public class _72_ai_properties
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Destroyable Cover","Pathfinding Ignore When Dead","Dynamic Cover" }, 4);
[Value(Name="Type Name", Offset=4)]
public StringID _4_Type_Name;
[Value(Name="Size", Offset=8)]
public H2.DataTypes.Enum _8_Size = new H2.DataTypes.Enum(new string[] {  "Default", "Tiny", "Small", "Medium", "Large", "Huge", "Immobile" }, 4);
[Value(Name="Leap Jump Speed", Offset=12)]
public H2.DataTypes.Enum _12_Leap_Jump_Speed = new H2.DataTypes.Enum(new string[] {  "None", "Down", "Step", "Crouch", "Stand", "Storey", "Tower", "Infinite" }, 4);
}
public _80_functions[] _80_Functions;
[Serializable][Reflexive(Name="Functions", Offset=80, ChunkSize=32, Label="")]
public class _80_functions
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invert","Mapping Does Not Controls Active","Always Active","Random Time Offset" }, 4);
[Value(Name="Import Name", Offset=4)]
public StringID _4_Import_Name;
[Value(Name="Export Name", Offset=8)]
public StringID _8_Export_Name;
[Value(Name="Turn Off With", Offset=12)]
public StringID _12_Turn_Off_With;
[Value(Name="Min Value", Offset=16)]
public Single _16_Min_Value;
public _20_function_type__graph_[] _20_Function_Type__Graph_;
[Serializable][Reflexive(Name="Function Type (Graph)", Offset=20, ChunkSize=1, Label="")]
public class _20_function_type__graph_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Scale By", Offset=28)]
public StringID _28_Scale_By;
}
[Value(Name="Apply Collision Damage Scale", Offset=88)]
public Single _88_Apply_Collision_Damage_Scale;
[Value(Name="Min Game Acc (Default)", Offset=92)]
public Single _92_Min_Game_Acc__Default_;
[Value(Name="Max Game Acc (Default)", Offset=96)]
public Single _96_Max_Game_Acc__Default_;
[Value(Name="Min Gam Scale (Default)", Offset=100)]
public Single _100_Min_Gam_Scale__Default_;
[Value(Name="Max Gam Scale (Default)", Offset=104)]
public Single _104_Max_Gam_Scale__Default_;
[Value(Name="Min abs Acc (Default)", Offset=108)]
public Single _108_Min_abs_Acc__Default_;
[Value(Name="Max abs Acc (Default)", Offset=112)]
public Single _112_Max_abs_Acc__Default_;
[Value(Name="Min abs Scale (Default)", Offset=116)]
public Single _116_Min_abs_Scale__Default_;
[Value(Name="Max abs Scale (Default)", Offset=120)]
public Single _120_Max_abs_Scale__Default_;
[Value(Name="Hud Text Message Index", Offset=124)]
public Int16 _124_Hud_Text_Message_Index;
[Value(Offset=126)]
public Byte[] _126_ = new byte[2];
public _128_attachments[] _128_Attachments;
[Serializable][Reflexive(Name="Attachments", Offset=128, ChunkSize=24, Label="")]
public class _128_attachments
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
[Value(Name="Marker", Offset=8)]
public StringID _8_Marker;
[Value(Name="Change Color", Offset=12)]
public StringID _12_Change_Color;
[Value(Name="Primary Scale", Offset=16)]
public StringID _16_Primary_Scale;
[Value(Name="Secondary Scale", Offset=20)]
public StringID _20_Secondary_Scale;
}
public _136_widgets[] _136_Widgets;
[Serializable][Reflexive(Name="Widgets", Offset=136, ChunkSize=8, Label="")]
public class _136_widgets
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
}
public _144_old_functions[] _144_Old_Functions;
[Serializable][Reflexive(Name="Old Functions", Offset=144, ChunkSize=0, Label="")]
public class _144_old_functions
{
private int __StartOffset__;
}
public _152_change_colors[] _152_Change_Colors;
[Serializable][Reflexive(Name="Change Colors", Offset=152, ChunkSize=16, Label="")]
public class _152_change_colors
{
private int __StartOffset__;
public _0_initial_permutations[] _0_Initial_Permutations;
[Serializable][Reflexive(Name="Initial Permutations", Offset=0, ChunkSize=32, Label="")]
public class _0_initial_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Variant Name", Offset=28)]
public StringID _28_Variant_Name;
}
public _8_functions[] _8_Functions;
[Serializable][Reflexive(Name="Functions", Offset=8, ChunkSize=36, Label="")]
public class _8_functions
{
private int __StartOffset__;
[Value(Name="Scale Flags", Offset=0)]
public Bitmask _0_Scale_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Darken By", Offset=28)]
public StringID _28_Darken_By;
[Value(Name="Scale By", Offset=32)]
public StringID _32_Scale_By;
}
}
public _160_predicted_resource[] _160_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=160, ChunkSize=8, Label="")]
public class _160_predicted_resource
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Int16 _0_Type;
[Value(Name="Resource Index", Offset=2)]
public Int16 _2_Resource_Index;
[Value(Name="Tag Index", Offset=4)]
public Int32 _4_Tag_Index;
}
[Value(Name="Item Flags", Offset=168)]
public Bitmask _168_Item_Flags = new Bitmask(new string[] { "Always Maintains Z Up","Destroyed By Explosions","Unaffected By Gravity" }, 4);
[Value(Name="Old Message Index", Offset=172)]
public Int16 _172_Old_Message_Index;
[Value(Name="Sort Order", Offset=174)]
public Int16 _174_Sort_Order;
[Value(Name="Multiplayer On-ground Scale", Offset=176)]
public Single _176_Multiplayer_On_ground_Scale;
[Value(Name="Campaign On-Ground Scale", Offset=180)]
public Single _180_Campaign_On_Ground_Scale;
[Value(Name="Pickup", Offset=184)]
public StringID _184_Pickup;
[Value(Name="Swap", Offset=188)]
public StringID _188_Swap;
[Value(Name="Pickup or Dual Wield", Offset=192)]
public StringID _192_Pickup_or_Dual_Wield;
[Value(Name="Swap or Dual Wield", Offset=196)]
public StringID _196_Swap_or_Dual_Wield;
[Value(Name="Dual Wield Only", Offset=200)]
public StringID _200_Dual_Wield_Only;
[Value(Name="Picked Up", Offset=204)]
public StringID _204_Picked_Up;
[Value(Name="Ammo Singular", Offset=208)]
public StringID _208_Ammo_Singular;
[Value(Name="Ammo Plural", Offset=212)]
public StringID _212_Ammo_Plural;
[Value(Name="Switched To", Offset=216)]
public StringID _216_Switched_To;
[Value(Name="Swap AI", Offset=220)]
public StringID _220_Swap_AI;
[Value(Name="unused", Offset=224)]
public Dependancy _224_unused;
[Value(Name="Collision Sound", Offset=228)]
public Dependancy _228_Collision_Sound;
public _232_predicted_bitmaps[] _232_Predicted_Bitmaps;
[Serializable][Reflexive(Name="Predicted Bitmaps", Offset=232, ChunkSize=8, Label="")]
public class _232_predicted_bitmaps
{
private int __StartOffset__;
[Value(Name="Bitmap", Offset=0)]
public Dependancy _0_Bitmap;
}
[Value(Name="Detonation Damage Effect", Offset=240)]
public Dependancy _240_Detonation_Damage_Effect;
[Value(Name="Detonation Delay", Offset=244)]
public Single _244_Detonation_Delay;
[Value(Name="...To", Offset=248)]
public Single _248____To;
[Value(Name="Deonating Effect", Offset=252)]
public Dependancy _252_Deonating_Effect;
[Value(Name="Detonation Effect", Offset=256)]
public Dependancy _256_Detonation_Effect;
[Value(Name="Flags", Offset=260)]
public Bitmask _260_Flags = new Bitmask(new string[] { "Vertical Heat Display","Mutually Exclusive Triggers","Attacks Automatically on Bump","Must Be Readied","Doesn't Count Toward Maximum","Aim Assists Only When Zoomed","Prevents Grenade Throwing","Must Be Picked Up","Holds Triggers When Dropped","Prevents Melee Attack","Detonates When Dropped","Cannot Fire At Maximum Range","Secondary Trigger Overrides Grenades","(OBSOLETE)Does Not Depower","Enables Integrated Night Vision","AIs Use Weapon Melee Damage","Forces No Binoculars","Loop FP Firing Animation","Prevents Sprinting","Cannot Fire While Boosting","Prevents Driving","Prevents Gunning","Can Be Dual Wielded","Can Only Be Dual Wielded","Melee Only","Cant Fire If Parent Dead","Weapon Ages With Each Kill","Weapon Uses Old Dual Fire Error","Primary Trigger Melee Attacks","Cannot Be Used By Player" }, 4);
[Value(Name="Secondary Trigger Mode", Offset=264)]
public H2.DataTypes.Enum _264_Secondary_Trigger_Mode = new H2.DataTypes.Enum(new string[] {  "Normal", "Slaved To Primary", "Inhibits Primary", "Loads Alternate Ammunition", "Loads Multiple Primary Ammunition" }, 4);
[Value(Name="Max Alternate Shots Loaded", Offset=268)]
public Single _268_Max_Alternate_Shots_Loaded;
[Value(Name="Turn On Time Sec", Offset=272)]
public Single _272_Turn_On_Time_Sec;
[Value(Name="Ready Time Sec", Offset=276)]
public Single _276_Ready_Time_Sec;
[Value(Name="Ready Effect", Offset=280)]
public Dependancy _280_Ready_Effect;
[Value(Name="Ready Damage Effect", Offset=284)]
public Dependancy _284_Ready_Damage_Effect;
[Value(Name="Heat Recovery Threshold", Offset=288)]
public Single _288_Heat_Recovery_Threshold;
[Value(Name="Overheated Threshold", Offset=292)]
public Single _292_Overheated_Threshold;
[Value(Name="Heat Detonation Threshold", Offset=296)]
public Single _296_Heat_Detonation_Threshold;
[Value(Name="Heat Detonation Fraction", Offset=300)]
public Single _300_Heat_Detonation_Fraction;
[Value(Name="Heat Loss Per Second", Offset=304)]
public Single _304_Heat_Loss_Per_Second;
[Value(Name="Heat Illumination", Offset=308)]
public Single _308_Heat_Illumination;
[Value(Name="Overheadted Heat Loss Per Second", Offset=312)]
public Single _312_Overheadted_Heat_Loss_Per_Second;
[Value(Name="Overheated", Offset=316)]
public Dependancy _316_Overheated;
[Value(Name="Overheated Damage Effect", Offset=320)]
public Dependancy _320_Overheated_Damage_Effect;
[Value(Name="Detonation", Offset=324)]
public Dependancy _324_Detonation;
[Value(Name="Detonation Damage Effect", Offset=328)]
public Dependancy _328_Detonation_Damage_Effect;
[Value(Name="Player Melee Damage", Offset=332)]
public Dependancy _332_Player_Melee_Damage;
[Value(Name="Player Melee Response", Offset=336)]
public Dependancy _336_Player_Melee_Response;
[Value(Name="Melee Aim Assist Magnetism Angle", Offset=340)]
public Single _340_Melee_Aim_Assist_Magnetism_Angle;
[Value(Name="Melee Aim Assist Magnetism Range", Offset=344)]
public Single _344_Melee_Aim_Assist_Magnetism_Range;
[Value(Name="Melee Aim Assist Throttle Magnitude", Offset=348)]
public Single _348_Melee_Aim_Assist_Throttle_Magnitude;
[Value(Name="Melee Aim Assist Throttle Minimum Distance", Offset=352)]
public Single _352_Melee_Aim_Assist_Throttle_Minimum_Distance;
[Value(Name="Melee Aim Assist Throttle Maximum Adjustment Angle", Offset=356)]
public Single _356_Melee_Aim_Assist_Throttle_Maximum_Adjustment_Angle;
[Value(Name="Melee Damage Parameters Damage Pyramid Angles Yaw", Offset=360)]
public Single _360_Melee_Damage_Parameters_Damage_Pyramid_Angles_Yaw;
[Value(Name="Melee Damage Parameters Damage Pyramid Angles Pitch", Offset=364)]
public Single _364_Melee_Damage_Parameters_Damage_Pyramid_Angles_Pitch;
[Value(Name="Melee Damage Parameters Damage Pyramid Depth", Offset=368)]
public Single _368_Melee_Damage_Parameters_Damage_Pyramid_Depth;
[Value(Name="1st Hit Melee Damage", Offset=372)]
public Dependancy _372_1st_Hit_Melee_Damage;
[Value(Name="1st Hit Melee Response Damage", Offset=376)]
public Dependancy _376_1st_Hit_Melee_Response_Damage;
[Value(Name="2nd Hit Melee Damage", Offset=380)]
public Dependancy _380_2nd_Hit_Melee_Damage;
[Value(Name="2nd Hit Melee Response Damage", Offset=384)]
public Dependancy _384_2nd_Hit_Melee_Response_Damage;
[Value(Name="3rd Hit Melee Damage", Offset=388)]
public Dependancy _388_3rd_Hit_Melee_Damage;
[Value(Name="3rd Hit Melee Response", Offset=392)]
public Dependancy _392_3rd_Hit_Melee_Response;
[Value(Name="Lunge Melee Damage", Offset=396)]
public Dependancy _396_Lunge_Melee_Damage;
[Value(Name="Lunge Melee Response", Offset=400)]
public Dependancy _400_Lunge_Melee_Response;
[Value(Name="Melee Damage Reporting Type", Offset=404)]
public H2.DataTypes.Enum _404_Melee_Damage_Reporting_Type = new H2.DataTypes.Enum(new string[] {  "The Guardians", "Falling Damage", "Generic Collision Damage", "Generic Melee Damage", "Generic Explosion", "Magnum Pistol", "Plasma Pistol", "Needler", "SMG", "Plasma Rifle", "Battle Rifle", "Carbine", "Shotgun", "Sniper Rifle", "Beam Rifle", "Rocket Launcher", "Flak Cannon", "Brute Shot", "Disintegrator", "Brute Plasma Rifle", "Energy Sword", "Frag Grenade", "Plasma Grenade", "Flag Melee Damage", "Bomb Melee Damage", "Bomb Explosion Damage", "Ball Melee Damage", "Human Turret", "Plasma Turret", "Banshee", "Ghost", "Mongoose", "Scorpion", "Spectre Driver", "Spectre Gunner", "Warthog Driver", "Warthog Gunner", "Wraith", "Tank", "Sentinal Beam", "Sentinal RPG", "Teleporter" }, 2);
[Value(Name="Magnification Levels", Offset=406)]
public Int16 _406_Magnification_Levels;
[Value(Name="Magnification Range Lower", Offset=408)]
public Single _408_Magnification_Range_Lower;
[Value(Name="Magnification Range Upper", Offset=412)]
public Single _412_Magnification_Range_Upper;
[Value(Name="Auto Aim Angle Radians", Offset=416)]
public Single _416_Auto_Aim_Angle_Radians;
[Value(Name="Auto Aim Range", Offset=420)]
public Single _420_Auto_Aim_Range;
[Value(Name="Magnetism Angle Radians", Offset=424)]
public Single _424_Magnetism_Angle_Radians;
[Value(Name="Magnetism Range", Offset=428)]
public Single _428_Magnetism_Range;
[Value(Name="Deviation Angle Radians", Offset=432)]
public Single _432_Deviation_Angle_Radians;
[Value(Name="Movement Penalized", Offset=436)]
public H2.DataTypes.Enum _436_Movement_Penalized = new H2.DataTypes.Enum(new string[] {  "Always", "When Zoomed", "When Zoomed or Reloading" }, 4);
[Value(Name="Forwards Movement Penalty", Offset=440)]
public Single _440_Forwards_Movement_Penalty;
[Value(Name="Sideways Movement Penalty", Offset=444)]
public Single _444_Sideways_Movement_Penalty;
[Value(Offset=448)]
public Byte[] _448_ = new byte[16];
[Value(Name="AI Scariness", Offset=464)]
public Single _464_AI_Scariness;
[Value(Name="Weapon Power-on Time Sec", Offset=468)]
public Single _468_Weapon_Power_on_Time_Sec;
[Value(Name="Weapon Power-off Time Sec", Offset=472)]
public Single _472_Weapon_Power_off_Time_Sec;
[Value(Name="Weapon Power-on Effect", Offset=476)]
public Dependancy _476_Weapon_Power_on_Effect;
[Value(Name="Weapon Power-off Effect", Offset=480)]
public Dependancy _480_Weapon_Power_off_Effect;
[Value(Name="Age Heat Recovery Penalty", Offset=484)]
public Single _484_Age_Heat_Recovery_Penalty;
[Value(Name="Age Rate of Fire Penalty", Offset=488)]
public Single _488_Age_Rate_of_Fire_Penalty;
[Value(Name="Age Misfire Start", Offset=492)]
public Single _492_Age_Misfire_Start;
[Value(Name="Age Misfire Chance", Offset=496)]
public Single _496_Age_Misfire_Chance;
[Value(Name="Pickup Sound", Offset=500)]
public Dependancy _500_Pickup_Sound;
[Value(Name="Zoom-In Sound", Offset=504)]
public Dependancy _504_Zoom_In_Sound;
[Value(Name="Zoom-Out Sound", Offset=508)]
public Dependancy _508_Zoom_Out_Sound;
[Value(Name="Active Camo Ding", Offset=512)]
public Single _512_Active_Camo_Ding;
[Value(Name="Active Camo Regrowth Rate", Offset=516)]
public Single _516_Active_Camo_Regrowth_Rate;
[Value(Name="Handle Node", Offset=520)]
public StringID _520_Handle_Node;
[Value(Name="Weapon Class", Offset=524)]
public StringID _524_Weapon_Class;
[Value(Name="Weapon Name", Offset=528)]
public StringID _528_Weapon_Name;
[Value(Name="Multiplayer Weapon Type", Offset=532)]
public H2.DataTypes.Enum _532_Multiplayer_Weapon_Type = new H2.DataTypes.Enum(new string[] {  "None", "CTF Flag", "Oddball Ball", "Headhunter Head", "Juggernaught Powerup" }, 2);
[Value(Name="Weapon Type", Offset=534)]
public H2.DataTypes.Enum _534_Weapon_Type = new H2.DataTypes.Enum(new string[] {  "Undefined", "Shotgun", "Needler", "Plasma Pistol", "Plasma Rifle", "Rocket Launcher" }, 2);
[Value(Name="Tracking Type", Offset=536)]
public H2.DataTypes.Enum _536_Tracking_Type = new H2.DataTypes.Enum(new string[] {  }, 4);
[Value(Offset=540)]
public Byte[] _540_ = new byte[16];
public _556_first_person[] _556_First_Person;
[Serializable][Reflexive(Name="First Person", Offset=556, ChunkSize=16, Label="")]
public class _556_first_person
{
private int __StartOffset__;
[Value(Name="First Person Model", Offset=0)]
public Dependancy _0_First_Person_Model;
[Value(Name="First Person Animations", Offset=8)]
public Dependancy _8_First_Person_Animations;
}
[Value(Name="New HUD Interface", Offset=564)]
public Dependancy _564_New_HUD_Interface;
public _568_predicted_resource[] _568_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=568, ChunkSize=8, Label="")]
public class _568_predicted_resource
{
private int __StartOffset__;
[Value(Name="Resource", Offset=0)]
public Dependancy _0_Resource;
}
public _576_magazine[] _576_Magazine;
[Serializable][Reflexive(Name="Magazine", Offset=576, ChunkSize=92, Label="")]
public class _576_magazine
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Wastes Rounds When Reloaded","Every Round Must Be Chambered" }, 4);
[Value(Name="Rounds Recharged Per Second", Offset=4)]
public Int16 _4_Rounds_Recharged_Per_Second;
[Value(Name="Rounds Total Initial", Offset=6)]
public Int16 _6_Rounds_Total_Initial;
[Value(Name="Rounds Total Maximum", Offset=8)]
public Int16 _8_Rounds_Total_Maximum;
[Value(Name="Rounds Total Loaded Maximum", Offset=10)]
public Int16 _10_Rounds_Total_Loaded_Maximum;
[Value(Name="Max Rounds Held", Offset=12)]
public Int32 _12_Max_Rounds_Held;
[Value(Name="Reload Time Seconds", Offset=16)]
public Single _16_Reload_Time_Seconds;
[Value(Name="Rounds Reloaded", Offset=20)]
public Int16 _20_Rounds_Reloaded;
[Value(Offset=22)]
public Byte[] _22_ = new byte[2];
[Value(Name="Chamber Time Seconds", Offset=24)]
public Single _24_Chamber_Time_Seconds;
[Value(Offset=28)]
public Byte[] _28_ = new byte[24];
[Value(Name="Reloading Effect", Offset=52)]
public Dependancy _52_Reloading_Effect;
[Value(Name="Reloading Damage Effect", Offset=60)]
public Dependancy _60_Reloading_Damage_Effect;
[Value(Name="Chambering Effect", Offset=68)]
public Dependancy _68_Chambering_Effect;
[Value(Name="Chambering Damage Effect", Offset=76)]
public Dependancy _76_Chambering_Damage_Effect;
public _84_magazines[] _84_Magazines;
[Serializable][Reflexive(Name="Magazines", Offset=84, ChunkSize=12, Label="")]
public class _84_magazines
{
private int __StartOffset__;
[Value(Name="Rounds", Offset=0)]
public Int16 _0_Rounds;
[Value(Offset=2)]
public Byte[] _2_ = new byte[2];
[Value(Name="Equipment", Offset=4)]
public Dependancy _4_Equipment;
}
}
public _584_new_triggers[] _584_New_Triggers;
[Serializable][Reflexive(Name="New Triggers", Offset=584, ChunkSize=64, Label="")]
public class _584_new_triggers
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Autofire Single Action Only" }, 4);
[Value(Name="Button Used", Offset=4)]
public H2.DataTypes.Enum _4_Button_Used = new H2.DataTypes.Enum(new string[] {  "Right Trigger", "Left Trigger", "Melee Attack (SP Only)", "Automated Fire (Unassigned)" }, 2);
[Value(Name="Behavior", Offset=6)]
public H2.DataTypes.Enum _6_Behavior = new H2.DataTypes.Enum(new string[] {  "Spew", "Latch", "1-Latch 2-Autofire", "Charge", "1-Latch 2-Zoom", "1-Latch 2-Lockon" }, 2);
[Value(Name="Primary Barrel", Offset=8)]
public Int16 _8_Primary_Barrel;
[Value(Name="Secondary Barrel", Offset=10)]
public Int16 _10_Secondary_Barrel;
[Value(Name="Prediction", Offset=12)]
public H2.DataTypes.Enum _12_Prediction = new H2.DataTypes.Enum(new string[] {  "None", "Spew", "Charge" }, 4);
[Value(Name="Autofire Time", Offset=16)]
public Single _16_Autofire_Time;
[Value(Name="Autofire Throw", Offset=20)]
public Single _20_Autofire_Throw;
[Value(Name="Secondary Action", Offset=24)]
public H2.DataTypes.Enum _24_Secondary_Action = new H2.DataTypes.Enum(new string[] {  "Fire", "Charge", "Track", "Fire Other" }, 4);
[Value(Name="Charging Time", Offset=28)]
public Single _28_Charging_Time;
[Value(Name="Charged Time", Offset=32)]
public Single _32_Charged_Time;
[Value(Name="Overcharged Action", Offset=36)]
public H2.DataTypes.Enum _36_Overcharged_Action = new H2.DataTypes.Enum(new string[] {  "None", "Explode", "Discharge" }, 4);
[Value(Name="Charged Illumination", Offset=40)]
public Single _40_Charged_Illumination;
[Value(Name="Spew Time", Offset=44)]
public Single _44_Spew_Time;
[Value(Name="Charging Effect", Offset=48)]
public Dependancy _48_Charging_Effect;
[Value(Name="Charging Damage Effect", Offset=56)]
public Dependancy _56_Charging_Damage_Effect;
}
public _592_barrels[] _592_Barrels;
[Serializable][Reflexive(Name="Barrels", Offset=592, ChunkSize=236, Label="")]
public class _592_barrels
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Tracks Fired Projectile","Random Firing Effects","Can Fire With Partial Ammo","Projectiles use Weapon Origin","Ejects During Chamber","Use Error When Unzoomed","Projectile Vector Cannot Be Adjusted","Projectiles Have Identical Error","Projectiles Fire Parallel","Cant Fire When Others Firing","Cant Fire When Others Recovering","Don't Clear Fire Bit After Recovering","Stagger Fire Across Multiple Markers","Fires Locked Projectiles" }, 4);
[Value(Name="Rounds per Second Min", Offset=4)]
public Single _4_Rounds_per_Second_Min;
[Value(Name="Rounds per Second Max", Offset=8)]
public Single _8_Rounds_per_Second_Max;
[Value(Name="Acceleration Time Sec", Offset=12)]
public Single _12_Acceleration_Time_Sec;
[Value(Name="Deceleration Time Sec", Offset=16)]
public Single _16_Deceleration_Time_Sec;
[Value(Name="Barrel Spin Scale", Offset=20)]
public Single _20_Barrel_Spin_Scale;
[Value(Name="Blurred Rate Of Fire", Offset=24)]
public Single _24_Blurred_Rate_Of_Fire;
[Value(Name="Shots Per Fire Lower", Offset=28)]
public Int16 _28_Shots_Per_Fire_Lower;
[Value(Name="Shots Per Fire Upper", Offset=30)]
public Int16 _30_Shots_Per_Fire_Upper;
[Value(Name="Fire Recovery Time Sec", Offset=32)]
public Single _32_Fire_Recovery_Time_Sec;
[Value(Name="Soft Recovery Fraction", Offset=36)]
public Single _36_Soft_Recovery_Fraction;
[Value(Name="Magazine", Offset=40)]
public Int16 _40_Magazine;
[Value(Name="Rounds per Shot", Offset=42)]
public Int16 _42_Rounds_per_Shot;
[Value(Name="Minimum Rounds Loaded", Offset=44)]
public Int16 _44_Minimum_Rounds_Loaded;
[Value(Name="Rounds Between Tracers", Offset=46)]
public Int16 _46_Rounds_Between_Tracers;
[Value(Name="Optional Barrel Marker Name", Offset=48)]
public StringID _48_Optional_Barrel_Marker_Name;
[Value(Name="Prediction Type", Offset=52)]
public H2.DataTypes.Enum _52_Prediction_Type = new H2.DataTypes.Enum(new string[] {  "None", "Continuous", "Instant" }, 2);
[Value(Name="Firing Noise", Offset=54)]
public H2.DataTypes.Enum _54_Firing_Noise = new H2.DataTypes.Enum(new string[] {  "Silent", "Medium", "Loud", "Shout", "Quiet" }, 2);
[Value(Name="Error Acceleration Time Sec", Offset=56)]
public Single _56_Error_Acceleration_Time_Sec;
[Value(Name="Error Deceleration Time Sec", Offset=60)]
public Single _60_Error_Deceleration_Time_Sec;
[Value(Name="Damage Error Lower", Offset=64)]
public Single _64_Damage_Error_Lower;
[Value(Name="Damage Error Upper", Offset=68)]
public Single _68_Damage_Error_Upper;
[Value(Name="Dual Weapon Error Acceleration Time Sec", Offset=72)]
public Single _72_Dual_Weapon_Error_Acceleration_Time_Sec;
[Value(Name="Dual Weapon Error Deceleration Time Sec", Offset=76)]
public Single _76_Dual_Weapon_Error_Deceleration_Time_Sec;
[Value(Name="Unknown Calculated Constant", Offset=80)]
public Single _80_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=84)]
public Single _84_Unknown_Calculated_Constant;
[Value(Name="Dual Weapon Minimum Error Radians", Offset=88)]
public Single _88_Dual_Weapon_Minimum_Error_Radians;
[Value(Name="Dual Weapon Error Angle Lower", Offset=92)]
public Single _92_Dual_Weapon_Error_Angle_Lower;
[Value(Name="Dual Weapon Error Angle Upper", Offset=96)]
public Single _96_Dual_Weapon_Error_Angle_Upper;
[Value(Name="Dual Wield Damage Scale", Offset=100)]
public Single _100_Dual_Wield_Damage_Scale;
[Value(Name="Distribution Function", Offset=104)]
public H2.DataTypes.Enum _104_Distribution_Function = new H2.DataTypes.Enum(new string[] {  "Point", "Horizontal Fan" }, 2);
[Value(Name="Projectiles per Shot", Offset=106)]
public Int16 _106_Projectiles_per_Shot;
[Value(Name="Distribution Angle Radians", Offset=108)]
public Single _108_Distribution_Angle_Radians;
[Value(Name="Minimum Error", Offset=112)]
public Single _112_Minimum_Error;
[Value(Name="Error Angle Min", Offset=116)]
public Single _116_Error_Angle_Min;
[Value(Name="Error Angle Max", Offset=120)]
public Single _120_Error_Angle_Max;
[Value(Name="First Person Offset X", Offset=124)]
public Single _124_First_Person_Offset_X;
[Value(Name="First Person Offset Y", Offset=128)]
public Single _128_First_Person_Offset_Y;
[Value(Name="First Person Offset Z", Offset=132)]
public Single _132_First_Person_Offset_Z;
[Value(Name="Damage Reporting Type", Offset=136)]
public H2.DataTypes.Enum _136_Damage_Reporting_Type = new H2.DataTypes.Enum(new string[] {  "The Guardians", "Falling Damage", "Generic Collision Damage", "Generic Melee Damage", "Generic Explosion", "Magnum Pistol", "Plasma Pistol", "Needler", "SMG", "Plasma Rifle", "Battle Rifle", "Carbine", "Shotgun", "Sniper Rifle", "Beam Rifle", "Rocket Launcher", "Flak Cannon", "Brute Shot", "Disintegrator", "Brute Plasma Rifle", "Energy Sword", "Frag Grenade", "Plasma Grenade", "Flag Melee Damage", "Bomb Melee Damage", "Bomb Explosion Damage", "Ball Melee Damage", "Human Turret", "Plasma Turret", "Banshee", "Ghost", "Mongoose", "Scorpion", "Spectre Driver", "Spectre Gunner", "Warthog Driver", "Warthog Gunner", "Wraith", "Tank", "Sentinal Beam", "Sentinal RPG", "Teleporter" }, 4);
[Value(Name="Projectile", Offset=140)]
public Dependancy _140_Projectile;
[Value(Name="Damage Effect", Offset=148)]
public Dependancy _148_Damage_Effect;
[Value(Name="Ejection Port Recovery Time", Offset=156)]
public Single _156_Ejection_Port_Recovery_Time;
[Value(Name="Illumination Recovery Time", Offset=160)]
public Single _160_Illumination_Recovery_Time;
[Value(Name="Heat Generated Per Round", Offset=164)]
public Single _164_Heat_Generated_Per_Round;
[Value(Name="Age Generated Per Round", Offset=168)]
public Single _168_Age_Generated_Per_Round;
[Value(Name="Overload Time Sec", Offset=172)]
public Single _172_Overload_Time_Sec;
[Value(Name="Angle Change Per Shot Lower", Offset=176)]
public Single _176_Angle_Change_Per_Shot_Lower;
[Value(Name="Angle Change Per Shot Upper", Offset=180)]
public Single _180_Angle_Change_Per_Shot_Upper;
[Value(Name="Angle Change Acceleration Time Sec", Offset=184)]
public Single _184_Angle_Change_Acceleration_Time_Sec;
[Value(Name="Angle Change Deceleration Time Sec", Offset=188)]
public Single _188_Angle_Change_Deceleration_Time_Sec;
[Value(Name="Angle Change Function", Offset=192)]
public H2.DataTypes.Enum _192_Angle_Change_Function = new H2.DataTypes.Enum(new string[] {  "Linear", "Early", "Very Early", "Late", "Very Late", "Cosine", "Zero", "One" }, 4);
[Value(Name="Unknown Calculated Constant", Offset=196)]
public Single _196_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=200)]
public Single _200_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=204)]
public Single _204_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=208)]
public Single _208_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=212)]
public Single _212_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=216)]
public Single _216_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=220)]
public Single _220_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=224)]
public Single _224_Unknown_Calculated_Constant;
public _228_firing_effects[] _228_Firing_Effects;
[Serializable][Reflexive(Name="Firing Effects", Offset=228, ChunkSize=52, Label="")]
public class _228_firing_effects
{
private int __StartOffset__;
[Value(Name="Shot Count Lower Bound", Offset=0)]
public Int16 _0_Shot_Count_Lower_Bound;
[Value(Name="Shot Count Upper Bound", Offset=2)]
public Int16 _2_Shot_Count_Upper_Bound;
[Value(Name="Firing Effect", Offset=4)]
public Dependancy _4_Firing_Effect;
[Value(Name="Misfire Effect", Offset=12)]
public Dependancy _12_Misfire_Effect;
[Value(Name="Empty Effect", Offset=20)]
public Dependancy _20_Empty_Effect;
[Value(Name="Firing Damage", Offset=28)]
public Dependancy _28_Firing_Damage;
[Value(Name="Misfire Damage", Offset=36)]
public Dependancy _36_Misfire_Damage;
[Value(Name="Empty Damage", Offset=44)]
public Dependancy _44_Empty_Damage;
}
}
[Value(Name="Unknown Calculated Constant", Offset=600)]
public Single _600_Unknown_Calculated_Constant;
[Value(Name="Unknown Calculated Constant", Offset=604)]
public Single _604_Unknown_Calculated_Constant;
[Value(Name="Max Movement Acceleration", Offset=608)]
public Single _608_Max_Movement_Acceleration;
[Value(Name="Max Movement Velocity", Offset=612)]
public Single _612_Max_Movement_Velocity;
[Value(Name="Max Turning Acceleration", Offset=616)]
public Single _616_Max_Turning_Acceleration;
[Value(Name="Max Turning Velocity", Offset=620)]
public Single _620_Max_Turning_Velocity;
[Value(Name="Deployed Vehicle", Offset=624)]
public Dependancy _624_Deployed_Vehicle;
[Value(Name="Aged Effect", Offset=628)]
public Dependancy _628_Aged_Effect;
[Value(Name="Aged Weapon", Offset=632)]
public Dependancy _632_Aged_Weapon;
[Value(Name="FP Weapon Offset i", Offset=636)]
public Single _636_FP_Weapon_Offset_i;
[Value(Name="FP Weapon Offset j", Offset=640)]
public Single _640_FP_Weapon_Offset_j;
[Value(Name="FP Weapon Offset k", Offset=644)]
public Single _644_FP_Weapon_Offset_k;
[Value(Name="FP Scope Size i", Offset=648)]
public Single _648_FP_Scope_Size_i;
[Value(Name="FP Scope Size j", Offset=652)]
public Single _652_FP_Scope_Size_j;
        }
    }
}