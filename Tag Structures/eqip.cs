using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("eqip")]
        public class eqip
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Does Not Cast Shadow","Search Cardinal Direction Lightmaps","unused","Not A Pathfinding Obstacle","Extension Of Parent","Does Not Cause Collision Damage","Early Mover","Early Mover Localized Physics","Use Static Massive Lightmap Sample","Object Scales Attachments","Inherits Player's Appearance","Dead Bipeds Can't Localize","Attach To Clusters By Dynamic Sphere","Effects Created By This Object Do Not" }, 4);
[Value(Name="Bounding Radius", Offset=4)]
public Single _4_Bounding_Radius;
[Value(Name="Bounding Offset X", Offset=8)]
public Single _8_Bounding_Offset_X;
[Value(Name="Bounding Offset Y", Offset=12)]
public Single _12_Bounding_Offset_Y;
[Value(Name="Bounding Offset Z", Offset=16)]
public Single _16_Bounding_Offset_Z;
[Value(Name="Acceleration Scale", Offset=20)]
public Single _20_Acceleration_Scale;
[Value(Name="Lightmap Shadow Mode", Offset=24)]
public H2.DataTypes.Enum _24_Lightmap_Shadow_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Never", "Always" }, 4);
[Value(Name="Sweetener Size", Offset=28)]
public H2.DataTypes.Enum _28_Sweetener_Size = new H2.DataTypes.Enum(new string[] {  "Small", "Medium", "Large" }, 4);
[Value(Name="Dynamic Light Sphere Radius", Offset=32)]
public Single _32_Dynamic_Light_Sphere_Radius;
[Value(Name="Dynamic Light Sphere Offset X", Offset=36)]
public Single _36_Dynamic_Light_Sphere_Offset_X;
[Value(Name="Dynamic Light Sphere Offset Y", Offset=40)]
public Single _40_Dynamic_Light_Sphere_Offset_Y;
[Value(Name="Dynamic Light Sphere Offset Z", Offset=44)]
public Single _44_Dynamic_Light_Sphere_Offset_Z;
[Value(Name="Default Model Variant", Offset=48)]
public StringID _48_Default_Model_Variant;
[Value(Name="Model", Offset=52)]
public Dependancy _52_Model;
[Value(Name="Crate Object", Offset=56)]
public Dependancy _56_Crate_Object;
[Value(Name="Modifier Shader", Offset=60)]
public Dependancy _60_Modifier_Shader;
[Value(Name="Creation Effect", Offset=64)]
public Dependancy _64_Creation_Effect;
[Value(Name="Material Effects", Offset=68)]
public Dependancy _68_Material_Effects;
public _72_ai_properties[] _72_Ai_Properties;
[Serializable][Reflexive(Name="Ai Properties", Offset=72, ChunkSize=16, Label="")]
public class _72_ai_properties
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Destroyable Cover","Pathfinding Ignore When Dead","Dynamic Cover" }, 4);
[Value(Name="Type Name", Offset=4)]
public StringID _4_Type_Name;
[Value(Name="Size", Offset=8)]
public H2.DataTypes.Enum _8_Size = new H2.DataTypes.Enum(new string[] {  "Default", "Tiny", "Small", "Medium", "Large", "Huge", "Immobile" }, 4);
[Value(Name="Leap Jump Speed", Offset=12)]
public H2.DataTypes.Enum _12_Leap_Jump_Speed = new H2.DataTypes.Enum(new string[] {  "None", "Down", "Step", "Crouch", "Stand", "Storey", "Tower", "Infinite" }, 4);
}
public _80_functions[] _80_Functions;
[Serializable][Reflexive(Name="Functions", Offset=80, ChunkSize=32, Label="")]
public class _80_functions
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Invert","Mapping Does Not Controls Active","Always Active","Random Time Offset" }, 4);
[Value(Name="Import Name", Offset=4)]
public StringID _4_Import_Name;
[Value(Name="Export Name", Offset=8)]
public StringID _8_Export_Name;
[Value(Name="Turn Off With", Offset=12)]
public StringID _12_Turn_Off_With;
[Value(Name="Min Value", Offset=16)]
public Single _16_Min_Value;
public _20_function_type__graph_[] _20_Function_Type__Graph_;
[Serializable][Reflexive(Name="Function Type (Graph)", Offset=20, ChunkSize=1, Label="")]
public class _20_function_type__graph_
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Scale By", Offset=28)]
public StringID _28_Scale_By;
}
[Value(Name="Apply Collision Damage Scale", Offset=88)]
public Single _88_Apply_Collision_Damage_Scale;
[Value(Name="Min Game Acc (Default)", Offset=92)]
public Single _92_Min_Game_Acc__Default_;
[Value(Name="Max Game Acc (Default)", Offset=96)]
public Single _96_Max_Game_Acc__Default_;
[Value(Name="Min Gam Scale (Default)", Offset=100)]
public Single _100_Min_Gam_Scale__Default_;
[Value(Name="Max Gam Scale (Default)", Offset=104)]
public Single _104_Max_Gam_Scale__Default_;
[Value(Name="Min abs Acc (Default)", Offset=108)]
public Single _108_Min_abs_Acc__Default_;
[Value(Name="Max abs Acc (Default)", Offset=112)]
public Single _112_Max_abs_Acc__Default_;
[Value(Name="Min abs Scale (Default)", Offset=116)]
public Single _116_Min_abs_Scale__Default_;
[Value(Name="Max abs Scale (Default)", Offset=120)]
public Single _120_Max_abs_Scale__Default_;
[Value(Name="Hud Text Message Index", Offset=124)]
public Int16 _124_Hud_Text_Message_Index;
[Value(Offset=126)]
public Byte[] _126_ = new byte[2];
public _128_attachments[] _128_Attachments;
[Serializable][Reflexive(Name="Attachments", Offset=128, ChunkSize=24, Label="")]
public class _128_attachments
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
[Value(Name="Marker", Offset=8)]
public StringID _8_Marker;
[Value(Name="Change Color", Offset=12)]
public StringID _12_Change_Color;
[Value(Name="Primary Scale", Offset=16)]
public StringID _16_Primary_Scale;
[Value(Name="Secondary Scale", Offset=20)]
public StringID _20_Secondary_Scale;
}
public _136_widgets[] _136_Widgets;
[Serializable][Reflexive(Name="Widgets", Offset=136, ChunkSize=8, Label="")]
public class _136_widgets
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Dependancy _0_Type;
}
public _144_old_functions[] _144_Old_Functions;
[Serializable][Reflexive(Name="Old Functions", Offset=144, ChunkSize=0, Label="")]
public class _144_old_functions
{
private int __StartOffset__;
}
public _152_change_colors[] _152_Change_Colors;
[Serializable][Reflexive(Name="Change Colors", Offset=152, ChunkSize=16, Label="")]
public class _152_change_colors
{
private int __StartOffset__;
public _0_initial_permutations[] _0_Initial_Permutations;
[Serializable][Reflexive(Name="Initial Permutations", Offset=0, ChunkSize=32, Label="")]
public class _0_initial_permutations
{
private int __StartOffset__;
[Value(Name="Weight", Offset=0)]
public Single _0_Weight;
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Variant Name", Offset=28)]
public StringID _28_Variant_Name;
}
public _8_functions[] _8_Functions;
[Serializable][Reflexive(Name="Functions", Offset=8, ChunkSize=36, Label="")]
public class _8_functions
{
private int __StartOffset__;
[Value(Name="Scale Flags", Offset=0)]
public Bitmask _0_Scale_Flags = new Bitmask(new string[] { "Blend In HSV","...More Colors" }, 4);
[Value(Name="Color Lower Bound R", Offset=4)]
public Single _4_Color_Lower_Bound_R;
[Value(Name="Color Lower Bound G", Offset=8)]
public Single _8_Color_Lower_Bound_G;
[Value(Name="Color Lower Bound B", Offset=12)]
public Single _12_Color_Lower_Bound_B;
[Value(Name="Color Upper Bound R", Offset=16)]
public Single _16_Color_Upper_Bound_R;
[Value(Name="Color Upper Bound G", Offset=20)]
public Single _20_Color_Upper_Bound_G;
[Value(Name="Color Upper Bound B", Offset=24)]
public Single _24_Color_Upper_Bound_B;
[Value(Name="Darken By", Offset=28)]
public StringID _28_Darken_By;
[Value(Name="Scale By", Offset=32)]
public StringID _32_Scale_By;
}
}
public _160_predicted_resource[] _160_Predicted_Resource;
[Serializable][Reflexive(Name="Predicted Resource", Offset=160, ChunkSize=8, Label="")]
public class _160_predicted_resource
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public Int16 _0_Type;
[Value(Name="Resource Index", Offset=2)]
public Int16 _2_Resource_Index;
[Value(Name="Tag Index", Offset=4)]
public Int32 _4_Tag_Index;
}
[Value(Name="Item Flags", Offset=168)]
public Bitmask _168_Item_Flags = new Bitmask(new string[] { "Always Maintains Z Up","Destroyed By Explosions","Unaffected By Gravity" }, 4);
[Value(Name="Old Message Index", Offset=172)]
public Int16 _172_Old_Message_Index;
[Value(Name="Sort Order", Offset=174)]
public Int16 _174_Sort_Order;
[Value(Name="Multiplayer On-ground Scale", Offset=176)]
public Single _176_Multiplayer_On_ground_Scale;
[Value(Name="Campaign On-Ground Scale", Offset=180)]
public Single _180_Campaign_On_Ground_Scale;
[Value(Name="Pickup", Offset=184)]
public StringID _184_Pickup;
[Value(Name="Swap", Offset=188)]
public StringID _188_Swap;
[Value(Name="Pickup or Dual Wield", Offset=192)]
public StringID _192_Pickup_or_Dual_Wield;
[Value(Name="Swap or Dual Wield", Offset=196)]
public StringID _196_Swap_or_Dual_Wield;
[Value(Name="Dual Wield Only", Offset=200)]
public StringID _200_Dual_Wield_Only;
[Value(Name="Picked Up", Offset=204)]
public StringID _204_Picked_Up;
[Value(Name="Ammo Singular", Offset=208)]
public StringID _208_Ammo_Singular;
[Value(Name="Ammo Plural", Offset=212)]
public StringID _212_Ammo_Plural;
[Value(Name="Switched To", Offset=216)]
public StringID _216_Switched_To;
[Value(Name="Swap AI", Offset=220)]
public StringID _220_Swap_AI;
[Value(Name="unused", Offset=224)]
public Dependancy _224_unused;
[Value(Name="Collision Sound", Offset=228)]
public Dependancy _228_Collision_Sound;
public _232_predicted_bitmaps[] _232_Predicted_Bitmaps;
[Serializable][Reflexive(Name="Predicted Bitmaps", Offset=232, ChunkSize=8, Label="")]
public class _232_predicted_bitmaps
{
private int __StartOffset__;
[Value(Name="Bitmap", Offset=0)]
public Dependancy _0_Bitmap;
}
[Value(Name="Detonation Damage Effect", Offset=240)]
public Dependancy _240_Detonation_Damage_Effect;
[Value(Name="Detonation Delay", Offset=244)]
public Single _244_Detonation_Delay;
[Value(Name="...To", Offset=248)]
public Single _248____To;
[Value(Name="Deonating Effect", Offset=252)]
public Dependancy _252_Deonating_Effect;
[Value(Name="Detonation Effect", Offset=256)]
public Dependancy _256_Detonation_Effect;
[Value(Name="Powerup Type", Offset=260)]
public H2.DataTypes.Enum _260_Powerup_Type = new H2.DataTypes.Enum(new string[] {  "None", "Double Speed", "Oversheild", "Active Camouflage", "Full-Spectrum Vision", "Health", "Grenade" }, 2);
[Value(Name="Grenade Type", Offset=262)]
public H2.DataTypes.Enum _262_Grenade_Type = new H2.DataTypes.Enum(new string[] {  "Human Fragmentation", "Covenant Plasma" }, 2);
[Value(Name="Powerup Time", Offset=264)]
public Single _264_Powerup_Time;
[Value(Name="Pickup Sound", Offset=268)]
public Dependancy _268_Pickup_Sound;
        }
    }
}