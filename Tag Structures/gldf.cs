using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("gldf")]
        public class gldf
        {
public _0_light_variables[] _0_Light_Variables;
[Serializable][Reflexive(Name="Light Variables", Offset=0, ChunkSize=144, Label="")]
public class _0_light_variables
{
private int __StartOffset__;
[Value(Name="Objects Affected", Offset=0)]
public Bitmask _0_Objects_Affected = new Bitmask(new string[] { "All","Biped","Vehicle","Weapon","Equipment","Garbage","Projectile","Scenery","Machine","Control","Light Fixture","Sound Scenery","Crate","Creature" }, 4);
[Value(Name="Lightmap Brightness Offset", Offset=4)]
public Single _4_Lightmap_Brightness_Offset;
[Value(Name="Primary Min Lightmap Color R", Offset=8)]
public Single _8_Primary_Min_Lightmap_Color_R;
[Value(Name="Primary Min Lightmap Color G", Offset=12)]
public Single _12_Primary_Min_Lightmap_Color_G;
[Value(Name="Primary Min Lightmap Color B", Offset=16)]
public Single _16_Primary_Min_Lightmap_Color_B;
[Value(Name="Primary Max Lightmap Color R", Offset=20)]
public Single _20_Primary_Max_Lightmap_Color_R;
[Value(Name="Primary Max Lightmap Color G", Offset=24)]
public Single _24_Primary_Max_Lightmap_Color_G;
[Value(Name="Primary Max Lightmap Color B", Offset=28)]
public Single _28_Primary_Max_Lightmap_Color_B;
[Value(Name="Exclusion Angle From Up", Offset=32)]
public Single _32_Exclusion_Angle_From_Up;
public _36_primary_light_function[] _36_Primary_Light_Function;
[Serializable][Reflexive(Name="Primary Light Function", Offset=36, ChunkSize=1, Label="")]
public class _36_primary_light_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Secondary Min Lightmap Color R", Offset=44)]
public Single _44_Secondary_Min_Lightmap_Color_R;
[Value(Name="Secondary Min Lightmap Color G", Offset=48)]
public Single _48_Secondary_Min_Lightmap_Color_G;
[Value(Name="Secondary Min Lightmap Color B", Offset=52)]
public Single _52_Secondary_Min_Lightmap_Color_B;
[Value(Name="Secondary Max Lightmap Color R", Offset=56)]
public Single _56_Secondary_Max_Lightmap_Color_R;
[Value(Name="Secondary Max Lightmap Color G", Offset=60)]
public Single _60_Secondary_Max_Lightmap_Color_G;
[Value(Name="Secondary Max Lightmap Color B", Offset=64)]
public Single _64_Secondary_Max_Lightmap_Color_B;
[Value(Name="Secondary Min Diffuse Sample R", Offset=68)]
public Single _68_Secondary_Min_Diffuse_Sample_R;
[Value(Name="Secondary Min Diffuse Sample G", Offset=72)]
public Single _72_Secondary_Min_Diffuse_Sample_G;
[Value(Name="Secondary Min Diffuse Sample B", Offset=76)]
public Single _76_Secondary_Min_Diffuse_Sample_B;
[Value(Name="Secondary Max Diffuse Sample R", Offset=80)]
public Single _80_Secondary_Max_Diffuse_Sample_R;
[Value(Name="Secondary Max Diffuse Sample G", Offset=84)]
public Single _84_Secondary_Max_Diffuse_Sample_G;
[Value(Name="Secondary Max Diffuse Sample B", Offset=88)]
public Single _88_Secondary_Max_Diffuse_Sample_B;
[Value(Name="Secondary Z-Axsis Rotation", Offset=92)]
public Single _92_Secondary_Z_Axsis_Rotation;
public _96_secondary_light_function[] _96_Secondary_Light_Function;
[Serializable][Reflexive(Name="Secondary Light Function", Offset=96, ChunkSize=1, Label="")]
public class _96_secondary_light_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Ambient Min Lightmap Sample R", Offset=104)]
public Single _104_Ambient_Min_Lightmap_Sample_R;
[Value(Name="Ambient Min Lightmap Sample G", Offset=108)]
public Single _108_Ambient_Min_Lightmap_Sample_G;
[Value(Name="Ambient Min Lightmap Sample B", Offset=112)]
public Single _112_Ambient_Min_Lightmap_Sample_B;
[Value(Name="Ambient Max Lightmap Sample R", Offset=116)]
public Single _116_Ambient_Max_Lightmap_Sample_R;
[Value(Name="Ambient Max Lightmap Sample G", Offset=120)]
public Single _120_Ambient_Max_Lightmap_Sample_G;
[Value(Name="Ambient Max Lightmap Sample B", Offset=124)]
public Single _124_Ambient_Max_Lightmap_Sample_B;
public _128_ambient_light_function[] _128_Ambient_Light_Function;
[Serializable][Reflexive(Name="Ambient Light Function", Offset=128, ChunkSize=1, Label="")]
public class _128_ambient_light_function
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _136_lightmap_shadows[] _136_Lightmap_Shadows;
[Serializable][Reflexive(Name="Lightmap Shadows", Offset=136, ChunkSize=1, Label="")]
public class _136_lightmap_shadows
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
}
        }
    }
}