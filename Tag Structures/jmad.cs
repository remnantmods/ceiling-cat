using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("jmad")]
        public class jmad
        {
[Value(Name="Unknown", Offset=0)]
public Dependancy _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
public _8_bones[] _8_Bones;
[Serializable][Reflexive(Name="Bones", Offset=8, ChunkSize=32, Label="")]
public class _8_bones
{
private int __StartOffset__;
[Value(Name="Bone", Offset=0)]
public StringID _0_Bone;
[Value(Name="Parent", Offset=4)]
public Int16 _4_Parent;
[Value(Name="Child", Offset=6)]
public Int16 _6_Child;
[Value(Name="Next Sibling", Offset=8)]
public Int16 _8_Next_Sibling;
[Value(Name="Final Sibling?", Offset=10)]
public Int16 _10_Final_Sibling_;
[Value(Offset=12)]
public Byte[] _12_ = new byte[16];
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
}
public _16_sounds[] _16_Sounds;
[Serializable][Reflexive(Name="Sounds", Offset=16, ChunkSize=12, Label="")]
public class _16_sounds
{
private int __StartOffset__;
[Value(Name="Sound", Offset=0)]
public Dependancy _0_Sound;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
}
public _24_effects[] _24_Effects;
[Serializable][Reflexive(Name="Effects", Offset=24, ChunkSize=12, Label="")]
public class _24_effects
{
private int __StartOffset__;
[Value(Name="Effect", Offset=0)]
public Dependancy _0_Effect;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
}
public _32_screen_bounds[] _32_Screen_Bounds;
[Serializable][Reflexive(Name="Screen Bounds", Offset=32, ChunkSize=28, Label="")]
public class _32_screen_bounds
{
private int __StartOffset__;
[Value(Name="label", Offset=0)]
public StringID _0_label;
[Value(Name="Right yaw per frame", Offset=4)]
public Single _4_Right_yaw_per_frame;
[Value(Name="Left yaw per frame", Offset=8)]
public Single _8_Left_yaw_per_frame;
[Value(Name="Right frame count", Offset=12)]
public Int16 _12_Right_frame_count;
[Value(Name="Left frame count", Offset=14)]
public Int16 _14_Left_frame_count;
[Value(Name="Down pitch per frame", Offset=16)]
public Single _16_Down_pitch_per_frame;
[Value(Name="Up pitch per frame", Offset=20)]
public Single _20_Up_pitch_per_frame;
[Value(Name="Down pitch frame count", Offset=24)]
public Int16 _24_Down_pitch_frame_count;
[Value(Name="Up pitch frame count", Offset=26)]
public Int16 _26_Up_pitch_frame_count;
}
public _40_animations[] _40_Animations;
[Serializable][Reflexive(Name="Animations", Offset=40, ChunkSize=108, Label="")]
public class _40_animations
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Byte _8_Unknown;
[Value(Name="Unknown", Offset=9)]
public Byte _9_Unknown;
[Value(Name="Unknown", Offset=10)]
public Int16 _10_Unknown;
[Value(Name="Unknown", Offset=12)]
public Byte _12_Unknown;
[Value(Name="Unknown", Offset=13)]
public Byte _13_Unknown;
[Value(Name="Unknown", Offset=14)]
public Int16 _14_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int16 _16_Unknown;
[Value(Name="Unknown", Offset=18)]
public Byte _18_Unknown;
[Value(Name="# of Bones", Offset=19)]
public Byte _19_No_of_Bones;
[Value(Name="Max Cycle Length (milliseconds)", Offset=20)]
public Int16 _20_Max_Cycle_Length__milliseconds_;
[Value(Name="Unknown (Do not Change)", Offset=22)]
public Int16 _22_Unknown__Do_not_Change_;
[Value(Name="Transition Speed?", Offset=24)]
public Int16 _24_Transition_Speed_;
[Value(Name="Unknown", Offset=26)]
public Int16 _26_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Offset=32)]
public Byte[] _32_ = new byte[4];
[Value(Name="Raw Chunk #", Offset=36)]
public Int32 _36_Raw_Chunk_No;
[Value(Name="Offset in raw", Offset=40)]
public Int16 _40_Offset_in_raw;
[Value(Name="Unused", Offset=42)]
public Int16 _42_Unused;
[Value(Name="Unknown", Offset=44)]
public Int16 _44_Unknown;
[Value(Name="Doesn't Repeat", Offset=46)]
public Bitmask _46_Doesn_t_Repeat = new Bitmask(new string[] { "Doesn't Repeat" }, 2);
[Value(Name="Unknown", Offset=48)]
public Int16 _48_Unknown;
[Value(Name="Unknown", Offset=50)]
public Int16 _50_Unknown;
[Value(Offset=52)]
public Byte[] _52_ = new byte[8];
[Value(Name="Unknown", Offset=60)]
public Byte _60_Unknown;
[Value(Name="Unknown", Offset=61)]
public Byte _61_Unknown;
[Value(Name="Unknown", Offset=62)]
public Byte _62_Unknown;
[Value(Name="Unknown", Offset=63)]
public Byte _63_Unknown;
[Value(Name="Unknown", Offset=64)]
public Int16 _64_Unknown;
[Value(Name="Animation Raw Something", Offset=66)]
public Int16 _66_Animation_Raw_Something;
[Value(Offset=68)]
public Byte[] _68_ = new byte[4];
[Value(Name="Unknown", Offset=72)]
public Byte _72_Unknown;
[Value(Name="Unknown", Offset=73)]
public Byte _73_Unknown;
[Value(Name="Unknown", Offset=74)]
public Int16 _74_Unknown;
public _76_unknown[] _76_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=76, ChunkSize=4, Label="")]
public class _76_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
}
public _84_sounds[] _84_Sounds;
[Serializable][Reflexive(Name="Sounds", Offset=84, ChunkSize=8, Label="")]
public class _84_sounds
{
private int __StartOffset__;
[Value(Name="Chunk #", Offset=0)]
public Int16 _0_Chunk_No;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
[Value(Name="Unknown", Offset=4)]
public StringID _4_Unknown;
}
public _92_effects[] _92_Effects;
[Serializable][Reflexive(Name="Effects", Offset=92, ChunkSize=4, Label="")]
public class _92_effects
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
}
public _100_unknown[] _100_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=100, ChunkSize=28, Label="")]
public class _100_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
}
}
public _48_animation_calls[] _48_Animation_Calls;
[Serializable][Reflexive(Name="Animation Calls", Offset=48, ChunkSize=20, Label="")]
public class _48_animation_calls
{
private int __StartOffset__;
[Value(Name="Postion Name", Offset=0)]
public StringID _0_Postion_Name;
public _4_class[] _4_Class;
[Serializable][Reflexive(Name="Class", Offset=4, ChunkSize=20, Label="")]
public class _4_class
{
private int __StartOffset__;
[Value(Name="Class Name", Offset=0)]
public StringID _0_Class_Name;
public _4_object[] _4_Object;
[Serializable][Reflexive(Name="Object", Offset=4, ChunkSize=52, Label="")]
public class _4_object
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public StringID _0_Type;
public _4_full_object_animations[] _4_Full_Object_Animations;
[Serializable][Reflexive(Name="Full Object Animations", Offset=4, ChunkSize=8, Label="")]
public class _4_full_object_animations
{
private int __StartOffset__;
[Value(Name="Condition", Offset=0)]
public StringID _0_Condition;
[Value(Name="Unknown", Offset=4)]
public Int16 _4_Unknown;
[Value(Name="Animation", Offset=6)]
public Int16 _6_Animation;
}
public _12_half_upper_object_animations[] _12_Half_Upper_Object_Animations;
[Serializable][Reflexive(Name="Half/Upper Object Animations", Offset=12, ChunkSize=8, Label="")]
public class _12_half_upper_object_animations
{
private int __StartOffset__;
[Value(Name="Condition", Offset=0)]
public StringID _0_Condition;
[Value(Name="Unknown", Offset=4)]
public Int16 _4_Unknown;
[Value(Name="Animation", Offset=6)]
public Int16 _6_Animation;
}
public _20_death_damage[] _20_Death_Damage;
[Serializable][Reflexive(Name="Death/Damage", Offset=20, ChunkSize=12, Label="")]
public class _20_death_damage
{
private int __StartOffset__;
[Value(Name="Condition", Offset=0)]
public StringID _0_Condition;
public _4_values[] _4_Values;
[Serializable][Reflexive(Name="Values", Offset=4, ChunkSize=8, Label="")]
public class _4_values
{
private int __StartOffset__;
public _0_values[] _0_Values;
[Serializable][Reflexive(Name="Values", Offset=0, ChunkSize=4, Label="")]
public class _0_values
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Chunk #", Offset=2)]
public Int16 _2_Chunk_No;
}
}
}
public _28_ai[] _28_AI;
[Serializable][Reflexive(Name="AI", Offset=28, ChunkSize=20, Label="")]
public class _28_ai
{
private int __StartOffset__;
[Value(Name="Currently Performing", Offset=0)]
public StringID _0_Currently_Performing;
[Value(Name="Currently Performing", Offset=4)]
public StringID _4_Currently_Performing;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
public _12_options[] _12_Options;
[Serializable][Reflexive(Name="Options", Offset=12, ChunkSize=20, Label="")]
public class _12_options
{
private int __StartOffset__;
[Value(Name="Full Name", Offset=0)]
public StringID _0_Full_Name;
[Value(Name="State Name", Offset=4)]
public StringID _4_State_Name;
[Value(Name="Animation Name", Offset=8)]
public StringID _8_Animation_Name;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int16 _16_Unknown;
[Value(Name="Chunk #", Offset=18)]
public Int16 _18_Chunk_No;
}
}
public _36_unknown[] _36_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=36, ChunkSize=4, Label="")]
public class _36_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _44_unknown[] _44_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=44, ChunkSize=4, Label="")]
public class _44_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
}
public _12_limb_positioning[] _12_Limb_Positioning;
[Serializable][Reflexive(Name="Limb Positioning", Offset=12, ChunkSize=8, Label="")]
public class _12_limb_positioning
{
private int __StartOffset__;
[Value(Name="Local Marker", Offset=0)]
public StringID _0_Local_Marker;
[Value(Name="Remote Marker", Offset=4)]
public StringID _4_Remote_Marker;
}
}
public _12_limb_positioning[] _12_Limb_Positioning;
[Serializable][Reflexive(Name="Limb Positioning", Offset=12, ChunkSize=8, Label="")]
public class _12_limb_positioning
{
private int __StartOffset__;
[Value(Name="Local Marker", Offset=0)]
public StringID _0_Local_Marker;
[Value(Name="Remote Marker", Offset=4)]
public StringID _4_Remote_Marker;
}
}
public _56_vehicle_suspension[] _56_Vehicle_Suspension;
[Serializable][Reflexive(Name="Vehicle Suspension", Offset=56, ChunkSize=40, Label="")]
public class _56_vehicle_suspension
{
private int __StartOffset__;
[Value(Name="Suspension part", Offset=0)]
public StringID _0_Suspension_part;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public StringID _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Single _20_Unknown;
[Value(Name="Marker", Offset=24)]
public StringID _24_Marker;
[Value(Offset=28)]
public Byte[] _28_ = new byte[4];
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
}
public _64_unknown[] _64_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=64, ChunkSize=20, Label="")]
public class _64_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Offset=8)]
public Byte[] _8_ = new byte[4];
[Value(Name="Unknown", Offset=12)]
public StringID _12_Unknown;
[Value(Offset=16)]
public Byte[] _16_ = new byte[4];
}
public _72_unknown[] _72_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=72, ChunkSize=32, Label="")]
public class _72_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Dependancy _0_Unknown;
public _8_unknown[] _8_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=8, ChunkSize=2, Label="")]
public class _8_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
}
public _16_unknown[] _16_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=16, ChunkSize=4, Label="")]
public class _16_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
[Value(Name="Unknown", Offset=24)]
public Single _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Int32 _28_Unknown;
}
public _80_weapon_associations[] _80_Weapon_Associations;
[Serializable][Reflexive(Name="Weapon Associations", Offset=80, ChunkSize=8, Label="")]
public class _80_weapon_associations
{
private int __StartOffset__;
[Value(Name="Short", Offset=0)]
public StringID _0_Short;
[Value(Name="Class", Offset=4)]
public StringID _4_Class;
}
[Value(Name="Unknown", Offset=88)]
public Int32 _88_Unknown;
[Value(Name="Unknown", Offset=92)]
public Int32 _92_Unknown;
[Value(Offset=96)]
public Byte[] _96_ = new byte[24];
[Value(Name="Unknown", Offset=120)]
public Int32 _120_Unknown;
[Value(Name="Unknown", Offset=124)]
public Int32 _124_Unknown;
[Value(Name="Unknown", Offset=128)]
public Int32 _128_Unknown;
[Value(Offset=132)]
public Byte[] _132_ = new byte[36];
public _168_unknown[] _168_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=168, ChunkSize=20, Label="")]
public class _168_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Raw Size", Offset=4)]
public Int32 _4_Raw_Size;
[Value(Name="Raw Offset", Offset=8)]
public Int32 _8_Raw_Offset;
[Value(Offset=12)]
public Byte[] _12_ = new byte[4];
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
}
public _176_unknown[] _176_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=176, ChunkSize=24, Label="")]
public class _176_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
}
        }
    }
}