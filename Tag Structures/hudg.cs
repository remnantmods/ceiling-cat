using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("hudg")]
        public class hudg
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[36];
[Value(Name="Anchor", Offset=36)]
public H2.DataTypes.Enum _36_Anchor = new H2.DataTypes.Enum(new string[] {  "Top Left", "Top Right", "Bottom Left", "Bottom Right", "Center" }, 2);
[Value(Name="Anchor Offset (y)", Offset=38)]
public Byte _38_Anchor_Offset__y_;
[Value(Name="Anchor Offset (x)", Offset=39)]
public Byte _39_Anchor_Offset__x_;
[Value(Offset=40)]
public Byte[] _40_ = new byte[32];
[Value(Name="obsolete1", Offset=72)]
public Dependancy _72_obsolete1;
[Value(Name="obsolete2", Offset=76)]
public Dependancy _76_obsolete2;
[Value(Name="Message Up time", Offset=80)]
public Single _80_Message_Up_time;
[Value(Name="Message Fade time", Offset=84)]
public Single _84_Message_Fade_time;
[Value(Name="Icon Color (alpha)", Offset=88)]
public Single _88_Icon_Color__alpha_;
[Value(Name="Icon Color (red)", Offset=92)]
public Single _92_Icon_Color__red_;
[Value(Name="Icon Color (green)", Offset=96)]
public Single _96_Icon_Color__green_;
[Value(Name="Icon Color (blue)", Offset=100)]
public Single _100_Icon_Color__blue_;
[Value(Name="Text Color (alpha)", Offset=104)]
public Single _104_Text_Color__alpha_;
[Value(Name="Text Color (red)", Offset=108)]
public Single _108_Text_Color__red_;
[Value(Name="Text Color (green)", Offset=112)]
public Single _112_Text_Color__green_;
[Value(Name="Text Color (blue)", Offset=116)]
public Single _116_Text_Color__blue_;
[Value(Name="Text Spacing", Offset=120)]
public Single _120_Text_Spacing;
[Value(Name="Item message text", Offset=124)]
public Dependancy _124_Item_message_text;
[Value(Name="Icon Bitmap", Offset=128)]
public Dependancy _128_Icon_Bitmap;
[Value(Name="Alternate Icon", Offset=132)]
public Dependancy _132_Alternate_Icon;
public _136_button_icons[] _136_Button_Icons;
[Serializable][Reflexive(Name="Button Icons", Offset=136, ChunkSize=16, Label="")]
public class _136_button_icons
{
private int __StartOffset__;
[Value(Name="Sequence Index", Offset=0)]
public Int16 _0_Sequence_Index;
[Value(Name="Width Offset", Offset=2)]
public Int16 _2_Width_Offset;
[Value(Name="Offset from Reference Corner (x)", Offset=4)]
public Int16 _4_Offset_from_Reference_Corner__x_;
[Value(Name="Offset from Reference Corner (y)", Offset=6)]
public Int16 _6_Offset_from_Reference_Corner__y_;
[Value(Name="Override Icon Color (alpha)", Offset=8)]
public Byte _8_Override_Icon_Color__alpha_;
[Value(Name="Override Icon Color (red)", Offset=9)]
public Byte _9_Override_Icon_Color__red_;
[Value(Name="Override Icon Color (greeb)", Offset=10)]
public Byte _10_Override_Icon_Color__greeb_;
[Value(Name="Override Icon Color (blue)", Offset=11)]
public Byte _11_Override_Icon_Color__blue_;
[Value(Name="Frame Rate (0-30)", Offset=12)]
public Byte _12_Frame_Rate__0_30_;
[Value(Name="Flags", Offset=13)]
public Bitmask _13_Flags = new Bitmask(new string[] { "Use text from string_list instead","Override default color","Width offset is absolute icon width" }, 1);
[Value(Name="Text Index", Offset=14)]
public Int16 _14_Text_Index;
}
[Value(Name="Hud Help Default Color (alpha)", Offset=144)]
public Byte _144_Hud_Help_Default_Color__alpha_;
[Value(Name="Hud Help Default Color (red)", Offset=145)]
public Byte _145_Hud_Help_Default_Color__red_;
[Value(Name="Hud Help Default Color (green)", Offset=146)]
public Byte _146_Hud_Help_Default_Color__green_;
[Value(Name="Hud Help Default Color (blue)", Offset=147)]
public Byte _147_Hud_Help_Default_Color__blue_;
[Value(Name="Hud Help Flashing Color (alpha)", Offset=148)]
public Byte _148_Hud_Help_Flashing_Color__alpha_;
[Value(Name="Hud Help Flashing Color (red)", Offset=149)]
public Byte _149_Hud_Help_Flashing_Color__red_;
[Value(Name="Hud Help Flashing Color (green)", Offset=150)]
public Byte _150_Hud_Help_Flashing_Color__green_;
[Value(Name="Hud Help Flashing Color (blue)", Offset=151)]
public Byte _151_Hud_Help_Flashing_Color__blue_;
[Value(Name="Hud Help Flash Period", Offset=152)]
public Single _152_Hud_Help_Flash_Period;
[Value(Name="Hud Help Flash Delay", Offset=156)]
public Single _156_Hud_Help_Flash_Delay;
[Value(Name="Number of Flashes", Offset=160)]
public Int16 _160_Number_of_Flashes;
[Value(Name="Hud Help Flash Flags", Offset=162)]
public Bitmask _162_Hud_Help_Flash_Flags = new Bitmask(new string[] { "Reverse Default/Flashing Colors" }, 2);
[Value(Name="Hud Help Flash Length", Offset=164)]
public Single _164_Hud_Help_Flash_Length;
[Value(Name="Disabled Color (alpha)", Offset=168)]
public Byte _168_Disabled_Color__alpha_;
[Value(Name="Disabled Color (red)", Offset=169)]
public Byte _169_Disabled_Color__red_;
[Value(Name="Disabled Color (green)", Offset=170)]
public Byte _170_Disabled_Color__green_;
[Value(Name="Disabled Color (blue)", Offset=171)]
public Byte _171_Disabled_Color__blue_;
[Value(Name="Disabled Color (alpha)", Offset=172)]
public Byte _172_Disabled_Color__alpha_;
[Value(Name="Disabled Color (red)", Offset=173)]
public Byte _173_Disabled_Color__red_;
[Value(Name="Disabled Color (green)", Offset=174)]
public Byte _174_Disabled_Color__green_;
[Value(Name="Disabled Color (blue)", Offset=175)]
public Byte _175_Disabled_Color__blue_;
[Value(Name="Hud messages", Offset=176)]
public Dependancy _176_Hud_messages;
[Value(Name="Objective Default Color (alpha)", Offset=180)]
public Byte _180_Objective_Default_Color__alpha_;
[Value(Name="Objective Default (red)", Offset=181)]
public Byte _181_Objective_Default__red_;
[Value(Name="Objective Default (green)", Offset=182)]
public Byte _182_Objective_Default__green_;
[Value(Name="Objective Default (blue)", Offset=183)]
public Byte _183_Objective_Default__blue_;
[Value(Name="Objective Flashing (alpha)", Offset=184)]
public Byte _184_Objective_Flashing__alpha_;
[Value(Name="Objective Flashing (red)", Offset=185)]
public Byte _185_Objective_Flashing__red_;
[Value(Name="Objective Flashing (green)", Offset=186)]
public Byte _186_Objective_Flashing__green_;
[Value(Name="Objective Flashing (blue)", Offset=187)]
public Byte _187_Objective_Flashing__blue_;
[Value(Name="Objective Flash Period", Offset=188)]
public Single _188_Objective_Flash_Period;
[Value(Name="Objective Flash Delay", Offset=192)]
public Single _192_Objective_Flash_Delay;
[Value(Name="Number of Flashes", Offset=196)]
public Int16 _196_Number_of_Flashes;
[Value(Name="Hud Help Flash Flags", Offset=198)]
public Bitmask _198_Hud_Help_Flash_Flags = new Bitmask(new string[] { "Reverse Default/Flashing Colors" }, 2);
[Value(Name="Objective Flash Length", Offset=200)]
public Single _200_Objective_Flash_Length;
[Value(Name="Disabled Color (alpha)", Offset=204)]
public Byte _204_Disabled_Color__alpha_;
[Value(Name="Disabled Color (red)", Offset=205)]
public Byte _205_Disabled_Color__red_;
[Value(Name="Disabled Color (green)", Offset=206)]
public Byte _206_Disabled_Color__green_;
[Value(Name="Disabled Color (blue)", Offset=207)]
public Byte _207_Disabled_Color__blue_;
[Value(Name="Objective uptime ticks", Offset=208)]
public Int16 _208_Objective_uptime_ticks;
[Value(Name="Objective fade ticks", Offset=210)]
public Int16 _210_Objective_fade_ticks;
[Value(Name=" Waypoint Top offset", Offset=212)]
public Single _212__Waypoint_Top_offset;
[Value(Name="Waypoint Bottom offset", Offset=216)]
public Single _216_Waypoint_Bottom_offset;
[Value(Name="Waypoint Left offset", Offset=220)]
public Single _220_Waypoint_Left_offset;
[Value(Name="Waypoint Right offset", Offset=224)]
public Single _224_Waypoint_Right_offset;
[Value(Offset=228)]
public Byte[] _228_ = new byte[32];
[Value(Name="Arrow bitmap", Offset=260)]
public Dependancy _260_Arrow_bitmap;
public _264_waypoints_arrows[] _264_Waypoints_Arrows;
[Serializable][Reflexive(Name="Waypoints Arrows", Offset=264, ChunkSize=104, Label="")]
public class _264_waypoints_arrows
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public H2.DataTypes.String _0_Name = new H2.DataTypes.String((H2.DataTypes.Length)32, System.Text.Encoding.ASCII);
[Value(Offset=32)]
public Byte[] _32_ = new byte[8];
[Value(Name="Red", Offset=40)]
public Byte _40_Red;
[Value(Name="Green", Offset=41)]
public Byte _41_Green;
[Value(Name="Blue", Offset=42)]
public Byte _42_Blue;
[Value(Name="Opacity", Offset=43)]
public Byte _43_Opacity;
[Value(Name="Translucency", Offset=44)]
public Single _44_Translucency;
[Value(Offset=48)]
public Byte[] _48_ = new byte[4];
[Value(Name="On Screen Sequence Index", Offset=52)]
public Int16 _52_On_Screen_Sequence_Index;
[Value(Name="Off Screen Sequence Index", Offset=54)]
public Int16 _54_Off_Screen_Sequence_Index;
[Value(Name="Unused Sequence Index", Offset=56)]
public Int16 _56_Unused_Sequence_Index;
[Value(Name="Flags", Offset=58)]
public Bitmask _58_Flags = new Bitmask(new string[] { "Do not rotate when pointing offscreen" }, 2);
[Value(Offset=60)]
public Byte[] _60_ = new byte[44];
}
[Value(Offset=272)]
public Byte[] _272_ = new byte[80];
[Value(Name="Multiplayer Hud Scale", Offset=352)]
public Single _352_Multiplayer_Hud_Scale;
[Value(Offset=356)]
public Byte[] _356_ = new byte[272];
[Value(Name="Motion Sensor Range", Offset=628)]
public Single _628_Motion_Sensor_Range;
[Value(Name="Motion Sensor Velocity Sensitivity", Offset=632)]
public Single _632_Motion_Sensor_Velocity_Sensitivity;
[Value(Name="Motion Sensor Scale", Offset=636)]
public Single _636_Motion_Sensor_Scale;
[Value(Name="Default Title Chapter Bounds (top)", Offset=640)]
public Int16 _640_Default_Title_Chapter_Bounds__top_;
[Value(Name="Default Title Chapter Bounds (left)", Offset=642)]
public Int16 _642_Default_Title_Chapter_Bounds__left_;
[Value(Name="Default Title Chapter Bounds (bottom)", Offset=644)]
public Int16 _644_Default_Title_Chapter_Bounds__bottom_;
[Value(Name="Default Title Chapter Bounds (right)", Offset=646)]
public Int16 _646_Default_Title_Chapter_Bounds__right_;
[Value(Offset=648)]
public Byte[] _648_ = new byte[44];
[Value(Name="Hud Damage Indicator Offset (top)", Offset=692)]
public Int16 _692_Hud_Damage_Indicator_Offset__top_;
[Value(Name="Hud Damage Indicator Offset (bottom)", Offset=694)]
public Int16 _694_Hud_Damage_Indicator_Offset__bottom_;
[Value(Name="Hud Damage Indicator Offset (left)", Offset=696)]
public Int16 _696_Hud_Damage_Indicator_Offset__left_;
[Value(Name="Hud Damage Indicator Offset (right)", Offset=698)]
public Int16 _698_Hud_Damage_Indicator_Offset__right_;
[Value(Offset=700)]
public Byte[] _700_ = new byte[32];
[Value(Name="Damage Indicator Bitmap", Offset=732)]
public Dependancy _732_Damage_Indicator_Bitmap;
[Value(Name="Sequence Index", Offset=736)]
public Int16 _736_Sequence_Index;
[Value(Name="Multiplayer Sequence Index", Offset=738)]
public Int16 _738_Multiplayer_Sequence_Index;
[Value(Name="Damage Indicator Color (blue)", Offset=740)]
public Byte _740_Damage_Indicator_Color__blue_;
[Value(Name="Damage Indicator Color (green)", Offset=741)]
public Byte _741_Damage_Indicator_Color__green_;
[Value(Name="Damage Indicator Color (red)", Offset=742)]
public Byte _742_Damage_Indicator_Color__red_;
[Value(Name="Damage Indicator Color (alpha)", Offset=743)]
public Byte _743_Damage_Indicator_Color__alpha_;
[Value(Offset=744)]
public Byte[] _744_ = new byte[16];
[Value(Name="Not Much Time Left Default Color (blue)", Offset=760)]
public Byte _760_Not_Much_Time_Left_Default_Color__blue_;
[Value(Name="Not Much Time Left Default Color (green)", Offset=761)]
public Byte _761_Not_Much_Time_Left_Default_Color__green_;
[Value(Name="Not Much Time Left Default Color (red)", Offset=762)]
public Byte _762_Not_Much_Time_Left_Default_Color__red_;
[Value(Name="Not Much Time Left Default Color (alpha)", Offset=763)]
public Byte _763_Not_Much_Time_Left_Default_Color__alpha_;
[Value(Name="Not Much Time Left Flash Color (blue)", Offset=764)]
public Byte _764_Not_Much_Time_Left_Flash_Color__blue_;
[Value(Name="Not Much Time Left Flash (green)", Offset=765)]
public Byte _765_Not_Much_Time_Left_Flash__green_;
[Value(Name="Not Much Time Left Flash (red)", Offset=766)]
public Byte _766_Not_Much_Time_Left_Flash__red_;
[Value(Name="Not Much Time Left Flash (alpha)", Offset=767)]
public Byte _767_Not_Much_Time_Left_Flash__alpha_;
[Value(Name="Not Much Time Left Flash Period", Offset=768)]
public Single _768_Not_Much_Time_Left_Flash_Period;
[Value(Name="Not Much Time Left Flash Delay", Offset=772)]
public Single _772_Not_Much_Time_Left_Flash_Delay;
[Value(Name="Number of Flashes", Offset=776)]
public Int16 _776_Number_of_Flashes;
[Value(Name="Not Much Time Left Flash Flags", Offset=778)]
public Bitmask _778_Not_Much_Time_Left_Flash_Flags = new Bitmask(new string[] { "Reverse Default/Flashing Colors" }, 2);
[Value(Name="Not Much Time Left Flash Length", Offset=780)]
public Single _780_Not_Much_Time_Left_Flash_Length;
[Value(Offset=784)]
public Byte[] _784_ = new byte[8];
[Value(Name="Time Out Default Color (alpha)", Offset=792)]
public Byte _792_Time_Out_Default_Color__alpha_;
[Value(Name="Time Out Default Color (red)", Offset=793)]
public Byte _793_Time_Out_Default_Color__red_;
[Value(Name="Time Out Default Color (green)", Offset=794)]
public Byte _794_Time_Out_Default_Color__green_;
[Value(Name="Time Out Default Color (blue)", Offset=795)]
public Byte _795_Time_Out_Default_Color__blue_;
[Value(Name="Time Out Flashing Color (alpha)", Offset=796)]
public Byte _796_Time_Out_Flashing_Color__alpha_;
[Value(Name="Time Out Flashing Color (red)", Offset=797)]
public Byte _797_Time_Out_Flashing_Color__red_;
[Value(Name="Time Out Flashing Color (green)", Offset=798)]
public Byte _798_Time_Out_Flashing_Color__green_;
[Value(Name="Time Out Flashing Color (blue)", Offset=799)]
public Byte _799_Time_Out_Flashing_Color__blue_;
[Value(Name="Time Out Flash Period", Offset=800)]
public Single _800_Time_Out_Flash_Period;
[Value(Name="Time Out Flash Delay", Offset=804)]
public Single _804_Time_Out_Flash_Delay;
[Value(Name="Number of Flashes", Offset=808)]
public Int16 _808_Number_of_Flashes;
[Value(Name="Time Out Flash Flags", Offset=810)]
public Bitmask _810_Time_Out_Flash_Flags = new Bitmask(new string[] { "Reverse Default/Flashing Colors" }, 2);
[Value(Name="Time Out Flash Length", Offset=812)]
public Single _812_Time_Out_Flash_Length;
[Value(Name="Disabled Time Out Default Color (alpha)", Offset=816)]
public Byte _816_Disabled_Time_Out_Default_Color__alpha_;
[Value(Name="Disabled Time Out Default Color (red)", Offset=817)]
public Byte _817_Disabled_Time_Out_Default_Color__red_;
[Value(Name="Disabled Time Out Default Color (green)", Offset=818)]
public Byte _818_Disabled_Time_Out_Default_Color__green_;
[Value(Name="Disabled Time Out Default Color (blue)", Offset=819)]
public Byte _819_Disabled_Time_Out_Default_Color__blue_;
[Value(Offset=820)]
public Byte[] _820_ = new byte[44];
[Value(Name="Carnage Report Bitmap", Offset=864)]
public Dependancy _864_Carnage_Report_Bitmap;
[Value(Name="Loading Begin Text", Offset=868)]
public Int16 _868_Loading_Begin_Text;
[Value(Name="Loading End Text", Offset=870)]
public Int16 _870_Loading_End_Text;
[Value(Name="Checkpoint Begin Text", Offset=872)]
public Int16 _872_Checkpoint_Begin_Text;
[Value(Name="checkpoint End Text", Offset=874)]
public Int16 _874_checkpoint_End_Text;
[Value(Name="Checkpoint Sound", Offset=876)]
public Dependancy _876_Checkpoint_Sound;
[Value(Offset=880)]
public Byte[] _880_ = new byte[96];
[Value(Name="Hud text", Offset=976)]
public Dependancy _976_Hud_text;
public _980_dashlights[] _980_Dashlights;
[Serializable][Reflexive(Name="Dashlights", Offset=980, ChunkSize=28, Label="")]
public class _980_dashlights
{
private int __StartOffset__;
[Value(Name="Bitmap", Offset=0)]
public Dependancy _0_Bitmap;
[Value(Name="Shader", Offset=8)]
public Dependancy _8_Shader;
[Value(Name="Sequence Index", Offset=16)]
public Int16 _16_Sequence_Index;
[Value(Name="Time Out Flash Flags", Offset=18)]
public Bitmask _18_Time_Out_Flash_Flags = new Bitmask(new string[] { "Don't scale when pulsing" }, 2);
[Value(Name="Sound", Offset=20)]
public Dependancy _20_Sound;
}
public _988_waypoint_arrows[] _988_Waypoint_Arrows;
[Serializable][Reflexive(Name="Waypoint Arrows", Offset=988, ChunkSize=36, Label="")]
public class _988_waypoint_arrows
{
private int __StartOffset__;
[Value(Name="Bitmap", Offset=0)]
public Dependancy _0_Bitmap;
[Value(Name="Shader", Offset=8)]
public Dependancy _8_Shader;
[Value(Name="Sequence Index", Offset=16)]
public Single _16_Sequence_Index;
[Value(Name="Smallest Size", Offset=20)]
public Single _20_Smallest_Size;
[Value(Name="Smallest Distance", Offset=24)]
public Single _24_Smallest_Distance;
[Value(Name="Border Bitmap", Offset=28)]
public Dependancy _28_Border_Bitmap;
}
public _996_waypoints[] _996_WayPoints;
[Serializable][Reflexive(Name="WayPoints", Offset=996, ChunkSize=24, Label="")]
public class _996_waypoints
{
private int __StartOffset__;
[Value(Name="Bitmap", Offset=0)]
public Dependancy _0_Bitmap;
[Value(Name="Sahder", Offset=8)]
public Dependancy _8_Sahder;
[Value(Name="Onscreen Sequence Index", Offset=16)]
public Int16 _16_Onscreen_Sequence_Index;
[Value(Name="Occluded Sequence Index", Offset=18)]
public Int16 _18_Occluded_Sequence_Index;
[Value(Name="Offscreen Sequence Index", Offset=20)]
public Int16 _20_Offscreen_Sequence_Index;
[Value(Name="Unused", Offset=22)]
public Int16 _22_Unused;
}
public _1004_hud_sounds[] _1004_Hud_sounds;
[Serializable][Reflexive(Name="Hud sounds", Offset=1004, ChunkSize=24, Label="")]
public class _1004_hud_sounds
{
private int __StartOffset__;
[Value(Name="Chief sound", Offset=0)]
public Dependancy _0_Chief_sound;
[Value(Name="Flags", Offset=8)]
public Bitmask _8_Flags = new Bitmask(new string[] { "Shield recharging","Shield damaged","Shield low","Shield empty","Health low","Health minor damage","Health major damage","Rocket locking","Rocket Locked" }, 4);
[Value(Name="Scale", Offset=12)]
public Single _12_Scale;
[Value(Name="Devrush sound", Offset=16)]
public Dependancy _16_Devrush_sound;
}
public _1012_player_training_data[] _1012_Player_Training_Data;
[Serializable][Reflexive(Name="Player Training Data", Offset=1012, ChunkSize=20, Label="")]
public class _1012_player_training_data
{
private int __StartOffset__;
[Value(Name="Display string", Offset=0)]
public StringID _0_Display_string;
[Value(Name="Display string2", Offset=4)]
public StringID _4_Display_string2;
[Value(Name="Display string3", Offset=8)]
public StringID _8_Display_string3;
[Value(Name="max display time", Offset=12)]
public Single _12_max_display_time;
[Value(Name="display count", Offset=16)]
public Byte _16_display_count;
[Value(Name="disapear delay", Offset=17)]
public Byte _17_disapear_delay;
[Value(Name="redisplay delay", Offset=18)]
public Byte _18_redisplay_delay;
[Value(Name="display delay (s)", Offset=19)]
public Byte _19_display_delay__s_;
}
[Value(Name="Primary Message Sound", Offset=1020)]
public Dependancy _1020_Primary_Message_Sound;
[Value(Name="Secondary Message Sound", Offset=1024)]
public Dependancy _1024_Secondary_Message_Sound;
[Value(Name="boot griefer string", Offset=1028)]
public StringID _1028_boot_griefer_string;
[Value(Name="cannot boot griefer string", Offset=1032)]
public StringID _1032_cannot_boot_griefer_string;
[Value(Name="Training Shader", Offset=1036)]
public Dependancy _1036_Training_Shader;
[Value(Name="Human Training Top Right ", Offset=1040)]
public Dependancy _1040_Human_Training_Top_Right_;
[Value(Name="Human Training Top Center ", Offset=1044)]
public Dependancy _1044_Human_Training_Top_Center_;
[Value(Name="Human Training Top Left ", Offset=1048)]
public Dependancy _1048_Human_Training_Top_Left_;
[Value(Name="Human Training Center ", Offset=1052)]
public Dependancy _1052_Human_Training_Center_;
[Value(Name="Elite Training Top Right ", Offset=1056)]
public Dependancy _1056_Elite_Training_Top_Right_;
[Value(Name="Elite Training Top Center ", Offset=1060)]
public Dependancy _1060_Elite_Training_Top_Center_;
[Value(Name="Elite Training Top Left ", Offset=1064)]
public Dependancy _1064_Elite_Training_Top_Left_;
[Value(Name="Elite Training Center ", Offset=1068)]
public Dependancy _1068_Elite_Training_Center_;
        }
    }
}