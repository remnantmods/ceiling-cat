using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("unic")]
        public class unic
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[16];
[Value(Name="English String Offset", Offset=16)]
public UInt16 _16_English_String_Offset;
[Value(Name="English String Count", Offset=18)]
public UInt16 _18_English_String_Count;
[Value(Name="Japanese String Offset", Offset=20)]
public UInt16 _20_Japanese_String_Offset;
[Value(Name="Japanese String Count", Offset=22)]
public UInt16 _22_Japanese_String_Count;
[Value(Name="German String Offset", Offset=24)]
public UInt16 _24_German_String_Offset;
[Value(Name="German String Count", Offset=26)]
public UInt16 _26_German_String_Count;
[Value(Name="French String Offset", Offset=28)]
public UInt16 _28_French_String_Offset;
[Value(Name="French String Count", Offset=30)]
public UInt16 _30_French_String_Count;
[Value(Name="Spanish String Offset", Offset=32)]
public UInt16 _32_Spanish_String_Offset;
[Value(Name="Spanish String Count", Offset=34)]
public UInt16 _34_Spanish_String_Count;
[Value(Name="Italian String Offset", Offset=36)]
public UInt16 _36_Italian_String_Offset;
[Value(Name="Italian String Count", Offset=38)]
public UInt16 _38_Italian_String_Count;
[Value(Name="Korean String Offset", Offset=40)]
public UInt16 _40_Korean_String_Offset;
[Value(Name="Korean String Count", Offset=42)]
public UInt16 _42_Korean_String_Count;
[Value(Name="Chinese String Offset", Offset=44)]
public UInt16 _44_Chinese_String_Offset;
[Value(Name="Chinese String Count", Offset=46)]
public UInt16 _46_Chinese_String_Count;
[Value(Name="Portuguese String Offset", Offset=48)]
public UInt16 _48_Portuguese_String_Offset;
[Value(Name="Portuguese String Count", Offset=50)]
public UInt16 _50_Portuguese_String_Count;
        }
    }
}