using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("clwd")]
        public class clwd
        {
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Doesn't Use Wind","Uses Grid Attach Top" }, 4);
[Value(Name="Marker Attachment Name", Offset=4)]
public StringID _4_Marker_Attachment_Name;
[Value(Name="Shader", Offset=8)]
public Dependancy _8_Shader;
[Value(Name="Grid X Dimension", Offset=12)]
public Int16 _12_Grid_X_Dimension;
[Value(Name="Grid Y Dimension", Offset=14)]
public Int16 _14_Grid_Y_Dimension;
[Value(Name="Grid Spacing X", Offset=16)]
public Single _16_Grid_Spacing_X;
[Value(Name="Grid Spacing Y", Offset=20)]
public Single _20_Grid_Spacing_Y;
[Value(Name="Integration Type", Offset=24)]
public H2.DataTypes.Enum _24_Integration_Type = new H2.DataTypes.Enum(new string[] {  }, 2);
[Value(Name="Number Iterations", Offset=26)]
public Int16 _26_Number_Iterations;
[Value(Name="Weight", Offset=28)]
public Single _28_Weight;
[Value(Name="Drag", Offset=32)]
public Single _32_Drag;
[Value(Name="Wind Scale", Offset=36)]
public Single _36_Wind_Scale;
[Value(Name="Wind Flappiness Scale", Offset=40)]
public Single _40_Wind_Flappiness_Scale;
[Value(Name="Longest Rod", Offset=44)]
public Single _44_Longest_Rod;
[Value(Offset=48)]
public Byte[] _48_ = new byte[24];
public _72_vertices[] _72_Vertices;
[Serializable][Reflexive(Name="Vertices", Offset=72, ChunkSize=20, Label="")]
public class _72_vertices
{
private int __StartOffset__;
[Value(Name="Initial Position X", Offset=0)]
public Single _0_Initial_Position_X;
[Value(Name="Initial Position Y", Offset=4)]
public Single _4_Initial_Position_Y;
[Value(Name="Initial Position Z", Offset=8)]
public Single _8_Initial_Position_Z;
[Value(Name="UV i", Offset=12)]
public Single _12_UV_i;
[Value(Name="UV j", Offset=16)]
public Single _16_UV_j;
}
public _80_indices[] _80_Indices;
[Serializable][Reflexive(Name="Indices", Offset=80, ChunkSize=2, Label="")]
public class _80_indices
{
private int __StartOffset__;
[Value(Name="Index", Offset=0)]
public Int16 _0_Index;
}
public _88_strip_indices[] _88_Strip_Indices;
[Serializable][Reflexive(Name="Strip Indices", Offset=88, ChunkSize=2, Label="")]
public class _88_strip_indices
{
private int __StartOffset__;
[Value(Name="Index", Offset=0)]
public Int16 _0_Index;
}
public _96_links[] _96_Links;
[Serializable][Reflexive(Name="Links", Offset=96, ChunkSize=16, Label="")]
public class _96_links
{
private int __StartOffset__;
[Value(Name="Attachment Bits", Offset=0)]
public Single _0_Attachment_Bits;
[Value(Name="Index 1", Offset=4)]
public Int16 _4_Index_1;
[Value(Name="Index 2", Offset=6)]
public Int16 _6_Index_2;
[Value(Name="Default Distance", Offset=8)]
public Single _8_Default_Distance;
[Value(Name="Damping Multiplier", Offset=12)]
public Single _12_Damping_Multiplier;
}
        }
    }
}