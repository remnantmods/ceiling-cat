using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("vrtx")]
        public class vrtx
        {
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown5[] _4_Unknown5;
[Serializable][Reflexive(Name="Unknown5", Offset=4, ChunkSize=28, Label="")]
public class _4_unknown5
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
public _4_unknown7[] _4_Unknown7;
[Serializable][Reflexive(Name="Unknown7", Offset=4, ChunkSize=2, Label="")]
public class _4_unknown7
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
}
public _12_unknown10[] _12_Unknown10;
[Serializable][Reflexive(Name="Unknown10", Offset=12, ChunkSize=1, Label="")]
public class _12_unknown10
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Byte _0_Unknown;
}
[Value(Offset=20)]
public Byte[] _20_ = new byte[8];
}
        }
    }
}