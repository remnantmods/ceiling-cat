using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("snmx")]
        public class snmx
        {
[Value(Name="First Person Left Side Left Stereo Gain", Offset=0)]
public Single _0_First_Person_Left_Side_Left_Stereo_Gain;
[Value(Name="First Person Left Side Right Stereo Gain", Offset=4)]
public Single _4_First_Person_Left_Side_Right_Stereo_Gain;
[Value(Name="First Person Middle Side Left Stereo Gain", Offset=8)]
public Single _8_First_Person_Middle_Side_Left_Stereo_Gain;
[Value(Name="First Person Middle Side Right Stereo Gain", Offset=12)]
public Single _12_First_Person_Middle_Side_Right_Stereo_Gain;
[Value(Name="First Person Right Side Left Stereo Gain", Offset=16)]
public Single _16_First_Person_Right_Side_Left_Stereo_Gain;
[Value(Name="First Person Right Side Right Stereo Gain", Offset=20)]
public Single _20_First_Person_Right_Side_Right_Stereo_Gain;
[Value(Name="First Person Stereo Front Speaker Gain dB", Offset=24)]
public Single _24_First_Person_Stereo_Front_Speaker_Gain_dB;
[Value(Name="First Person Stereo Rear Speaker Gain dB", Offset=28)]
public Single _28_First_Person_Stereo_Rear_Speaker_Gain_dB;
[Value(Name="Ambient Stereo Front Speaker Gain dB", Offset=32)]
public Single _32_Ambient_Stereo_Front_Speaker_Gain_dB;
[Value(Name="Ambient Stereo Rear Speaker Gain dB", Offset=36)]
public Single _36_Ambient_Stereo_Rear_Speaker_Gain_dB;
[Value(Name="Global Mono Unspatialized Gain dB", Offset=40)]
public Single _40_Global_Mono_Unspatialized_Gain_dB;
[Value(Name="Global Stereo To 3D Gain dB", Offset=44)]
public Single _44_Global_Stereo_To_3D_Gain_dB;
[Value(Name="Global Rear Surround To Front Stereo Gain dB", Offset=48)]
public Single _48_Global_Rear_Surround_To_Front_Stereo_Gain_dB;
[Value(Name="Surround Center Speaker Gain dB", Offset=52)]
public Single _52_Surround_Center_Speaker_Gain_dB;
[Value(Name="Surround Center Speaker Gain dB", Offset=56)]
public Single _56_Surround_Center_Speaker_Gain_dB;
[Value(Name="Stereo Front Speaker Gain dB", Offset=60)]
public Single _60_Stereo_Front_Speaker_Gain_dB;
[Value(Name="Stereo Center Speaker Gain dB", Offset=64)]
public Single _64_Stereo_Center_Speaker_Gain_dB;
[Value(Name="Stereo Unspatialized Gain dB", Offset=68)]
public Single _68_Stereo_Unspatialized_Gain_dB;
[Value(Name="Solo Player Fade Out Delay", Offset=72)]
public Single _72_Solo_Player_Fade_Out_Delay;
[Value(Name="Solo Player Fade Out Time Sec", Offset=76)]
public Single _76_Solo_Player_Fade_Out_Time_Sec;
[Value(Name="Solo Player Fade In Time Sec", Offset=80)]
public Single _80_Solo_Player_Fade_In_Time_Sec;
[Value(Name="Game Music Fade Out Time Sec", Offset=84)]
public Single _84_Game_Music_Fade_Out_Time_Sec;
        }
    }
}