using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("MGS2")]
        public class MGS2
        {
[Value(Name="Falloff Distance From Camera", Offset=0)]
public Single _0_Falloff_Distance_From_Camera;
[Value(Name="Cutoff Distance From Camera", Offset=4)]
public Single _4_Cutoff_Distance_From_Camera;
public _8_volumes[] _8_Volumes;
[Serializable][Reflexive(Name="Volumes", Offset=8, ChunkSize=152, Label="")]
public class _8_volumes
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Force Linear Radius Function","Force Linear Offset","Force Differential Evaluation","Fuzzy","Not Scaled By Event Duration","Scaled By Marker" }, 4);
[Value(Name="Bitmap", Offset=4)]
public Dependancy _4_Bitmap;
[Value(Name="Sprite Count", Offset=12)]
public Int32 _12_Sprite_Count;
public _16_offset[] _16_Offset;
[Serializable][Reflexive(Name="Offset", Offset=16, ChunkSize=1, Label="")]
public class _16_offset
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _24_radius[] _24_Radius;
[Serializable][Reflexive(Name="Radius", Offset=24, ChunkSize=1, Label="")]
public class _24_radius
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _32_brightness[] _32_Brightness;
[Serializable][Reflexive(Name="Brightness", Offset=32, ChunkSize=1, Label="")]
public class _32_brightness
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _40_color[] _40_Color;
[Serializable][Reflexive(Name="Color", Offset=40, ChunkSize=1, Label="")]
public class _40_color
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _48_facing[] _48_Facing;
[Serializable][Reflexive(Name="Facing", Offset=48, ChunkSize=1, Label="")]
public class _48_facing
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _56_aspect[] _56_Aspect;
[Serializable][Reflexive(Name="Aspect", Offset=56, ChunkSize=28, Label="")]
public class _56_aspect
{
private int __StartOffset__;
public _0_along_axis_scale[] _0_Along_Axis_Scale;
[Serializable][Reflexive(Name="Along-Axis Scale", Offset=0, ChunkSize=1, Label="")]
public class _0_along_axis_scale
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
public _8_away_from_axis_scale[] _8_Away_From_Axis_Scale;
[Serializable][Reflexive(Name="Away-From-Axis Scale", Offset=8, ChunkSize=1, Label="")]
public class _8_away_from_axis_scale
{
private int __StartOffset__;
[Value(Name="Data", Offset=0)]
public Byte _0_Data;
}
[Value(Name="Parallel Scale", Offset=16)]
public Single _16_Parallel_Scale;
[Value(Name="Parallel Threshold Angle", Offset=20)]
public Single _20_Parallel_Threshold_Angle;
[Value(Name="Parallel Exponent", Offset=24)]
public Single _24_Parallel_Exponent;
}
[Value(Name="Radius Frac Min", Offset=64)]
public Single _64_Radius_Frac_Min;
[Value(Name="DEPRECATED X-Step Exponent", Offset=68)]
public Single _68_DEPRECATED_X_Step_Exponent;
[Value(Name="DEPRECATED X-Buffer Length", Offset=72)]
public Int32 _72_DEPRECATED_X_Buffer_Length;
[Value(Name="X-Buffer Spacing", Offset=76)]
public Int32 _76_X_Buffer_Spacing;
[Value(Name="X-Buffer Min Iterations", Offset=80)]
public Int32 _80_X_Buffer_Min_Iterations;
[Value(Name="X-Buffer Max Iterations", Offset=84)]
public Int32 _84_X_Buffer_Max_Iterations;
[Value(Name="X-Delta Max Error", Offset=88)]
public Single _88_X_Delta_Max_Error;
[Value(Offset=92)]
public Byte[] _92_ = new byte[4];
public _96_[] _96__;
[Serializable][Reflexive(Name="", Offset=96, ChunkSize=8, Label="")]
public class _96_
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[8];
}
[Value(Offset=104)]
public Byte[] _104_ = new byte[48];
}
        }
    }
}