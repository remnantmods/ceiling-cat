using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("sky ")]
        public class sky_
        {
[Value(Name="Render Model", Offset=0)]
public Dependancy _0_Render_Model;
[Value(Name="Animation Graph", Offset=4)]
public Dependancy _4_Animation_Graph;
[Value(Name="Flags", Offset=8)]
public Bitmask _8_Flags = new Bitmask(new string[] { "Fixed In World Space","Depreciated","Sky Casts Light From Below","Disable Sky In Lightmaps","Fog Only Affects Containing Cluster","Use Clear Color" }, 4);
[Value(Name="Render Model Scale", Offset=12)]
public Single _12_Render_Model_Scale;
[Value(Name="Movement Scale", Offset=16)]
public Single _16_Movement_Scale;
public _20_cube_map[] _20_Cube_Map;
[Serializable][Reflexive(Name="Cube Map", Offset=20, ChunkSize=12, Label="")]
public class _20_cube_map
{
private int __StartOffset__;
[Value(Name="Cube Map", Offset=0)]
public Dependancy _0_Cube_Map;
[Value(Name="Power Scale", Offset=8)]
public Single _8_Power_Scale;
}
[Value(Name="Indoor Ambient Light Color R", Offset=28)]
public Single _28_Indoor_Ambient_Light_Color_R;
[Value(Name="Indoor Ambient Light Color G", Offset=32)]
public Single _32_Indoor_Ambient_Light_Color_G;
[Value(Name="Indoor Ambient Light Color B", Offset=36)]
public Single _36_Indoor_Ambient_Light_Color_B;
[Value(Name="Indoor Ambient Light Color A", Offset=40)]
public Single _40_Indoor_Ambient_Light_Color_A;
[Value(Name="Outdoor Ambient Light Color R", Offset=44)]
public Single _44_Outdoor_Ambient_Light_Color_R;
[Value(Name="Outdoor Ambient Light Color G", Offset=48)]
public Single _48_Outdoor_Ambient_Light_Color_G;
[Value(Name="Outdoor Ambient Light Color B", Offset=52)]
public Single _52_Outdoor_Ambient_Light_Color_B;
[Value(Name="Outdoor Ambient Light Color A", Offset=56)]
public Single _56_Outdoor_Ambient_Light_Color_A;
[Value(Name="Fog Spread Distance", Offset=60)]
public Single _60_Fog_Spread_Distance;
public _64_atmospheric_fog[] _64_Atmospheric_Fog;
[Serializable][Reflexive(Name="Atmospheric Fog", Offset=64, ChunkSize=24, Label="")]
public class _64_atmospheric_fog
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Name="Maximum Density", Offset=12)]
public Single _12_Maximum_Density;
[Value(Name="Start Distance", Offset=16)]
public Single _16_Start_Distance;
[Value(Name="Opaque Distnace", Offset=20)]
public Single _20_Opaque_Distnace;
}
public _72_secondary_fog[] _72_Secondary_Fog;
[Serializable][Reflexive(Name="Secondary Fog", Offset=72, ChunkSize=24, Label="")]
public class _72_secondary_fog
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Name="Color A", Offset=12)]
public Single _12_Color_A;
[Value(Name="Maximum Density", Offset=16)]
public Single _16_Maximum_Density;
[Value(Name="Opaque Distnace", Offset=20)]
public Single _20_Opaque_Distnace;
}
public _80_sky_fog[] _80_Sky_Fog;
[Serializable][Reflexive(Name="Sky Fog", Offset=80, ChunkSize=16, Label="")]
public class _80_sky_fog
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Name="Color A", Offset=12)]
public Single _12_Color_A;
}
public _88_patchy_fog[] _88_Patchy_Fog;
[Serializable][Reflexive(Name="Patchy Fog", Offset=88, ChunkSize=80, Label="")]
public class _88_patchy_fog
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Name="Color A", Offset=12)]
public Single _12_Color_A;
[Value(Offset=16)]
public Byte[] _16_ = new byte[8];
[Value(Name="Density Lower", Offset=24)]
public Single _24_Density_Lower;
[Value(Name="Density Upper", Offset=28)]
public Single _28_Density_Upper;
[Value(Name="Distance Lower", Offset=32)]
public Single _32_Distance_Lower;
[Value(Name="Distance Upper", Offset=36)]
public Single _36_Distance_Upper;
[Value(Offset=40)]
public Byte[] _40_ = new byte[32];
[Value(Name="Patch Fog", Offset=72)]
public Dependancy _72_Patch_Fog;
}
[Value(Name="Bloom Override Amount", Offset=96)]
public Single _96_Bloom_Override_Amount;
[Value(Name="Bloom Override Threshold", Offset=100)]
public Single _100_Bloom_Override_Threshold;
[Value(Name="Bloom Override Brightness", Offset=104)]
public Single _104_Bloom_Override_Brightness;
[Value(Name="Bloom Override Gamma Power", Offset=108)]
public Single _108_Bloom_Override_Gamma_Power;
public _112_lights[] _112_Lights;
[Serializable][Reflexive(Name="Lights", Offset=112, ChunkSize=52, Label="")]
public class _112_lights
{
private int __StartOffset__;
[Value(Name="Direction Vector i", Offset=0)]
public Single _0_Direction_Vector_i;
[Value(Name="Direction Vector j", Offset=4)]
public Single _4_Direction_Vector_j;
[Value(Name="Direction Vector k", Offset=8)]
public Single _8_Direction_Vector_k;
[Value(Name="Direction Y", Offset=12)]
public Single _12_Direction_Y;
[Value(Name="Direction P", Offset=16)]
public Single _16_Direction_P;
[Value(Name="Lens Flare", Offset=20)]
public Dependancy _20_Lens_Flare;
public _28_fog[] _28_Fog;
[Serializable][Reflexive(Name="Fog", Offset=28, ChunkSize=44, Label="")]
public class _28_fog
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Name="Color A", Offset=12)]
public Single _12_Color_A;
[Value(Name="Maximum Density", Offset=16)]
public Single _16_Maximum_Density;
[Value(Name="Opaque Distnace", Offset=20)]
public Single _20_Opaque_Distnace;
[Value(Name="Cone Lower", Offset=24)]
public Single _24_Cone_Lower;
[Value(Name="Cone Upper", Offset=28)]
public Single _28_Cone_Upper;
[Value(Name="Atmospheroc Fog Influence", Offset=32)]
public Single _32_Atmospheroc_Fog_Influence;
[Value(Name="Secondary Fog Influence", Offset=36)]
public Single _36_Secondary_Fog_Influence;
[Value(Name="Sky Fog Influence", Offset=40)]
public Single _40_Sky_Fog_Influence;
}
public _36_fog_opposite[] _36_Fog_Opposite;
[Serializable][Reflexive(Name="Fog Opposite", Offset=36, ChunkSize=44, Label="")]
public class _36_fog_opposite
{
private int __StartOffset__;
[Value(Name="Color R", Offset=0)]
public Single _0_Color_R;
[Value(Name="Color G", Offset=4)]
public Single _4_Color_G;
[Value(Name="Color B", Offset=8)]
public Single _8_Color_B;
[Value(Name="Color A", Offset=12)]
public Single _12_Color_A;
[Value(Name="Maximum Density", Offset=16)]
public Single _16_Maximum_Density;
[Value(Name="Opaque Distnace", Offset=20)]
public Single _20_Opaque_Distnace;
[Value(Name="Cone Lower", Offset=24)]
public Single _24_Cone_Lower;
[Value(Name="Cone Upper", Offset=28)]
public Single _28_Cone_Upper;
[Value(Name="Atmospheroc Fog Influence", Offset=32)]
public Single _32_Atmospheroc_Fog_Influence;
[Value(Name="Secondary Fog Influence", Offset=36)]
public Single _36_Secondary_Fog_Influence;
[Value(Name="Sky Fog Influence", Offset=40)]
public Single _40_Sky_Fog_Influence;
}
public _44_radiosity[] _44_Radiosity;
[Serializable][Reflexive(Name="Radiosity", Offset=44, ChunkSize=40, Label="")]
public class _44_radiosity
{
private int __StartOffset__;
[Value(Name="Flags", Offset=0)]
public Bitmask _0_Flags = new Bitmask(new string[] { "Affects Interiors","Affects Exteriors","Direct Illumunation In Lightmaps","Indirect Illumunation In Lightmaps" }, 4);
[Value(Name="Color R", Offset=4)]
public Single _4_Color_R;
[Value(Name="Color G", Offset=8)]
public Single _8_Color_G;
[Value(Name="Color B", Offset=12)]
public Single _12_Color_B;
[Value(Name="Power", Offset=16)]
public Single _16_Power;
[Value(Name="Test Distance", Offset=20)]
public Single _20_Test_Distance;
[Value(Offset=24)]
public Byte[] _24_ = new byte[12];
[Value(Name="Diameter", Offset=36)]
public Single _36_Diameter;
}
}
[Value(Name="Global Sky Rotation", Offset=120)]
public Single _120_Global_Sky_Rotation;
[Value(Offset=124)]
public Byte[] _124_ = new byte[28];
[Value(Name="Clear Color R", Offset=152)]
public Single _152_Clear_Color_R;
[Value(Name="Clear Color G", Offset=156)]
public Single _156_Clear_Color_G;
[Value(Name="Clear Color B", Offset=160)]
public Single _160_Clear_Color_B;
        }
    }
}