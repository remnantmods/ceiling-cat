using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("mode")]
        public class mode
        {
[Value(Name="Model Name", Offset=0)]
public StringID _0_Model_Name;
[Value(Name="Flags", Offset=4)]
public Bitmask _4_Flags = new Bitmask(new string[] { "Blend Shared Normals","Parts Have Local Nodes","Ignore Skinning" }, 4);
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[8];
public _20_bounding_box[] _20_Bounding_Box;
[Serializable][Reflexive(Name="Bounding Box", Offset=20, ChunkSize=56, Label="")]
public class _20_bounding_box
{
private int __StartOffset__;
[Value(Name="X (min)", Offset=0)]
public Single _0_X__min_;
[Value(Name="X (max)", Offset=4)]
public Single _4_X__max_;
[Value(Name="Y (min)", Offset=8)]
public Single _8_Y__min_;
[Value(Name="Y (max)", Offset=12)]
public Single _12_Y__max_;
[Value(Name="Z (min)", Offset=16)]
public Single _16_Z__min_;
[Value(Name="Z (max)", Offset=20)]
public Single _20_Z__max_;
[Value(Name="U (min)", Offset=24)]
public Single _24_U__min_;
[Value(Name="U (max)", Offset=28)]
public Single _28_U__max_;
[Value(Name="V (min)", Offset=32)]
public Single _32_V__min_;
[Value(Name="V (max)", Offset=36)]
public Single _36_V__max_;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
}
public _28_compression_info[] _28_Compression_Info;
[Serializable][Reflexive(Name="Compression Info", Offset=28, ChunkSize=16, Label="")]
public class _28_compression_info
{
private int __StartOffset__;
[Value(Name="Part Name", Offset=0)]
public StringID _0_Part_Name;
[Value(Name="Unknown", Offset=4)]
public Int16 _4_Unknown;
[Value(Name="Unknown", Offset=6)]
public Int16 _6_Unknown;
public _8_permutation[] _8_Permutation;
[Serializable][Reflexive(Name="Permutation", Offset=8, ChunkSize=16, Label="")]
public class _8_permutation
{
private int __StartOffset__;
[Value(Name="Permutation Name", Offset=0)]
public StringID _0_Permutation_Name;
[Value(Name="Lowest Piece (Index)", Offset=4)]
public Int16 _4_Lowest_Piece__Index_;
[Value(Name="Low Piece (Index)", Offset=6)]
public Int16 _6_Low_Piece__Index_;
[Value(Name="Medium-Low Piece (Index)", Offset=8)]
public Int16 _8_Medium_Low_Piece__Index_;
[Value(Name="Medium-High Piece (Index)", Offset=10)]
public Int16 _10_Medium_High_Piece__Index_;
[Value(Name="High Piece (Index)", Offset=12)]
public Int16 _12_High_Piece__Index_;
[Value(Name="Highest Piece (Index)", Offset=14)]
public Int16 _14_Highest_Piece__Index_;
}
}
public _36_sections[] _36_Sections;
[Serializable][Reflexive(Name="Sections", Offset=36, ChunkSize=92, Label="")]
public class _36_sections
{
private int __StartOffset__;
[Value(Name="Type", Offset=0)]
public UInt32 _0_Type;
[Value(Name="Vertex Count", Offset=4)]
public UInt16 _4_Vertex_Count;
[Value(Name="Unknown", Offset=6)]
public UInt16 _6_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
[Value(Name="Bone Type", Offset=20)]
public UInt16 _20_Bone_Type;
[Value(Name="Unknown", Offset=22)]
public UInt16 _22_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Offset=28)]
public Byte[] _28_ = new byte[8];
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Int32 _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Int32 _44_Unknown;
[Value(Offset=48)]
public Byte[] _48_ = new byte[8];
[Value(Name="Raw Offset", Offset=56)]
public UInt32 _56_Raw_Offset;
[Value(Name="Raw Size", Offset=60)]
public UInt32 _60_Raw_Size;
[Value(Name="Raw Header Size", Offset=64)]
public UInt32 _64_Raw_Header_Size;
[Value(Name="Raw Data Size", Offset=68)]
public UInt32 _68_Raw_Data_Size;
public _72_resource[] _72_Resource;
[Serializable][Reflexive(Name="Resource", Offset=72, ChunkSize=16, Label="")]
public class _72_resource
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Size", Offset=8)]
public UInt32 _8_Size;
[Value(Name="Offset", Offset=12)]
public UInt32 _12_Offset;
}
[Value(Name="Model Identifier", Offset=80)]
public Int32 _80_Model_Identifier;
[Value(Name="Unknown", Offset=84)]
public Int32 _84_Unknown;
[Value(Offset=88)]
public Byte[] _88_ = new byte[4];
}
public _44_unused[] _44_Unused;
[Serializable][Reflexive(Name="Unused", Offset=44, ChunkSize=4, Label="")]
public class _44_unused
{
private int __StartOffset__;
[Value(Offset=0)]
public Byte[] _0_ = new byte[4];
}
public _52_unknown[] _52_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=52, ChunkSize=12, Label="")]
public class _52_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown[] _4_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=4, ChunkSize=16, Label="")]
public class _4_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Single _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[4];
}
}
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Offset=68)]
public Byte[] _68_ = new byte[4];
public _72_nodes[] _72_Nodes;
[Serializable][Reflexive(Name="Nodes", Offset=72, ChunkSize=96, Label="")]
public class _72_nodes
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
[Value(Name="Parent (Index)", Offset=4)]
public Int16 _4_Parent__Index_;
[Value(Name="First Child (Index)", Offset=6)]
public Int16 _6_First_Child__Index_;
[Value(Name="Next Sibling (Index)", Offset=8)]
public Int16 _8_Next_Sibling__Index_;
[Value(Name="Unknown", Offset=10)]
public Int16 _10_Unknown;
[Value(Name="X", Offset=12)]
public Single _12_X;
[Value(Name="Y", Offset=16)]
public Single _16_Y;
[Value(Name="Z", Offset=20)]
public Single _20_Z;
[Value(Name="i", Offset=24)]
public Single _24_i;
[Value(Name="j", Offset=28)]
public Single _28_j;
[Value(Name="k", Offset=32)]
public Single _32_k;
[Value(Name="w (0 stops movement)", Offset=36)]
public Single _36_w__0_stops_movement_;
[Value(Name="Scale Factor", Offset=40)]
public Single _40_Scale_Factor;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Single _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Single _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Single _72_Unknown;
[Value(Name="Unknown", Offset=76)]
public Single _76_Unknown;
[Value(Name="X Center", Offset=80)]
public Single _80_X_Center;
[Value(Name="Y Center", Offset=84)]
public Single _84_Y_Center;
[Value(Name="Z Center", Offset=88)]
public Single _88_Z_Center;
[Value(Name="Distance from Parent", Offset=92)]
public Single _92_Distance_from_Parent;
}
[Value(Offset=80)]
public Byte[] _80_ = new byte[8];
public _88_markers[] _88_Markers;
[Serializable][Reflexive(Name="Markers", Offset=88, ChunkSize=12, Label="")]
public class _88_markers
{
private int __StartOffset__;
[Value(Name="Name", Offset=0)]
public StringID _0_Name;
public _4_instances[] _4_Instances;
[Serializable][Reflexive(Name="Instances", Offset=4, ChunkSize=36, Label="")]
public class _4_instances
{
private int __StartOffset__;
[Value(Name="Permutation index", Offset=0)]
public Int16 _0_Permutation_index;
[Value(Name="Parent Node index", Offset=2)]
public Int16 _2_Parent_Node_index;
[Value(Name="X", Offset=4)]
public Single _4_X;
[Value(Name="Y (when seen from back)", Offset=8)]
public Single _8_Y__when_seen_from_back_;
[Value(Name="Z", Offset=12)]
public Single _12_Z;
[Value(Name="i", Offset=16)]
public Single _16_i;
[Value(Name="j", Offset=20)]
public Single _20_j;
[Value(Name="k", Offset=24)]
public Single _24_k;
[Value(Name="w", Offset=28)]
public Single _28_w;
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
}
}
public _96_shader[] _96_Shader;
[Serializable][Reflexive(Name="Shader", Offset=96, ChunkSize=32, Label="")]
public class _96_shader
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Dependancy _0_Unused;
[Value(Name="Shader", Offset=8)]
public Dependancy _8_Shader;
public _16_unknown[] _16_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=16, ChunkSize=8, Label="")]
public class _16_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
}
[Value(Offset=24)]
public Byte[] _24_ = new byte[8];
}
[Value(Offset=104)]
public Byte[] _104_ = new byte[8];
[Value(Name="Unknown", Offset=112)]
public Single _112_Unknown;
public _116_unknown[] _116_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=116, ChunkSize=88, Label="")]
public class _116_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Int32 _16_Unknown;
public _20_unknown[] _20_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=20, ChunkSize=12, Label="")]
public class _20_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
public _4_unknown[] _4_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=4, ChunkSize=8, Label="")]
public class _4_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
}
public _28_unknown[] _28_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=28, ChunkSize=4, Label="")]
public class _28_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
[Value(Offset=36)]
public Byte[] _36_ = new byte[16];
[Value(Name="Raw Offset", Offset=52)]
public Int32 _52_Raw_Offset;
[Value(Name="Raw Size", Offset=56)]
public Int32 _56_Raw_Size;
[Value(Name="Raw Header Size", Offset=60)]
public Int32 _60_Raw_Header_Size;
[Value(Name="Raw Data Size", Offset=64)]
public Int32 _64_Raw_Data_Size;
public _68_resource[] _68_Resource;
[Serializable][Reflexive(Name="Resource", Offset=68, ChunkSize=16, Label="")]
public class _68_resource
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Size", Offset=8)]
public UInt32 _8_Size;
[Value(Name="Offset", Offset=12)]
public UInt32 _12_Offset;
}
[Value(Name="Model Identifier", Offset=76)]
public Int32 _76_Model_Identifier;
[Value(Name="Unknown", Offset=80)]
public Int32 _80_Unknown;
[Value(Offset=84)]
public Byte[] _84_ = new byte[4];
}
public _124_section_block_fields[] _124_Section_Block_Fields;
[Serializable][Reflexive(Name="Section Block Fields", Offset=124, ChunkSize=8, Label="")]
public class _124_section_block_fields
{
private int __StartOffset__;
public _0_section_data_tag_field_new[] _0_Section_Data_Tag_Field_New;
[Serializable][Reflexive(Name="Section Data Tag Field New", Offset=0, ChunkSize=16, Label="")]
public class _0_section_data_tag_field_new
{
private int __StartOffset__;
public _0_section_data_old[] _0_Section_Data_Old;
[Serializable][Reflexive(Name="Section Data Old", Offset=0, ChunkSize=8, Label="")]
public class _0_section_data_old
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
public _8_section_data[] _8_Section_Data;
[Serializable][Reflexive(Name="Section Data", Offset=8, ChunkSize=8, Label="")]
public class _8_section_data
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
}
}
}
        }
    }
}