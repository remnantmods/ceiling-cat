using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("adlg")]
        public class adlg
        {
public _0_death_taunting_dialog[] _0_Death_Taunting_Dialog;
[Serializable][Reflexive(Name="Death/Taunting Dialog", Offset=0, ChunkSize=96, Label="")]
public class _0_death_taunting_dialog
{
private int __StartOffset__;
[Value(Name="Situation", Offset=0)]
public StringID _0_Situation;
[Value(Name="Root Situation", Offset=4)]
public StringID _4_Root_Situation;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Offset=12)]
public Byte[] _12_ = new byte[8];
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Name="Unknown", Offset=28)]
public Single _28_Unknown;
[Value(Name="Unknown", Offset=32)]
public Single _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Single _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public Single _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Single _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public Single _48_Unknown;
[Value(Name="Unknown", Offset=52)]
public Single _52_Unknown;
[Value(Name="Unknown", Offset=56)]
public Single _56_Unknown;
[Value(Name="Unknown", Offset=60)]
public Single _60_Unknown;
[Value(Name="Unknown", Offset=64)]
public Int32 _64_Unknown;
[Value(Name="Unknown", Offset=68)]
public Single _68_Unknown;
[Value(Name="Unknown", Offset=72)]
public Single _72_Unknown;
[Value(Name="Spoken Dialog", Offset=76)]
public StringID _76_Spoken_Dialog;
public _80_unknown[] _80_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=80, ChunkSize=12, Label="")]
public class _80_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public StringID _0_Unknown;
[Value(Name="Unknown", Offset=4)]
public Int32 _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
}
[Value(Offset=88)]
public Byte[] _88_ = new byte[8];
}
public _8_killing_cautious_dialog[] _8_Killing_Cautious_Dialog;
[Serializable][Reflexive(Name="Killing/Cautious Dialog", Offset=8, ChunkSize=64, Label="")]
public class _8_killing_cautious_dialog
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
[Value(Name="Situation", Offset=4)]
public StringID _4_Situation;
[Value(Name="Unknown", Offset=8)]
public Int32 _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Int32 _12_Unknown;
[Value(Offset=16)]
public Byte[] _16_ = new byte[4];
[Value(Name="Unknown", Offset=20)]
public Int32 _20_Unknown;
[Value(Name="Unknown", Offset=24)]
public Int32 _24_Unknown;
[Value(Offset=28)]
public Byte[] _28_ = new byte[4];
[Value(Name="Unknown", Offset=32)]
public Int32 _32_Unknown;
[Value(Name="Unknown", Offset=36)]
public Int32 _36_Unknown;
[Value(Name="Unknown", Offset=40)]
public StringID _40_Unknown;
[Value(Name="Unknown", Offset=44)]
public Int32 _44_Unknown;
[Value(Name="Unknown", Offset=48)]
public StringID _48_Unknown;
[Value(Offset=52)]
public Byte[] _52_ = new byte[8];
[Value(Name="Unknown", Offset=60)]
public Int32 _60_Unknown;
}
[Value(Offset=16)]
public Byte[] _16_ = new byte[12];
public _28_unknown[] _28_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=28, ChunkSize=4, Label="")]
public class _28_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
public _36_unknown[] _36_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=36, ChunkSize=4, Label="")]
public class _36_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int32 _0_Unknown;
}
        }
    }
}