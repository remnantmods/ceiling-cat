using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable]
        [Tag("sncl")]
        public class sncl
        {
            public _0_sound_classes[] _0_Sound_Classes;
            [Serializable]
            [Reflexive(Name = "Sound Classes", Offset = 0, ChunkSize = 92, Label = "")]
            public class _0_sound_classes
            {
                private int __StartOffset__;
                [Value(Name = "Max Sounds Per Tag", Offset = 0)]
                public Int16 _0_Max_Sounds_Per_Tag;
                [Value(Name = "Max Sounds Per Object", Offset = 2)]
                public Int16 _2_Max_Sounds_Per_Object;
                [Value(Name = "Preemption Time", Offset = 4)]
                public Int32 _4_Preemption_Time;
                [Value(Name = "Internal Flags", Offset = 8)]
                public Bitmask _8_Internal_Flags = new Bitmask(new string[] { "Valid", "Is Speech", "Scripted", "Stops With Object", "unused", "Valid Doppler Factor", "Valid Obstruction Factor", "Multilingual" }, 2);
                [Value(Name = "Flags", Offset = 10)]
                public Bitmask _10_Flags = new Bitmask(new string[] { "Plays During Pause", "Dry Stereo Mix", "No Object Obstruction", "Use Center Speaker Unspatialized", "Send (Mono) To LFE", "Deterministic", "Use Huge Transmission", "Always Use Speakers", "Don't Strip From Mainmenu", "Ignore Stereo Headroom", "Loop Fade Out Is Linear", "Stop When Object Dies", "Allow Cache File Editing" }, 2);
                [Value(Name = "Priority", Offset = 12)]
                public Int16 _12_Priority;
                [Value(Name = "Cache Miss Mode", Offset = 14)]
                public H2.DataTypes.Enum _14_Cache_Miss_Mode = new H2.DataTypes.Enum(new string[] { }, 2);
                [Value(Name = "Reverb Gain dB", Offset = 16)]
                public Single _16_Reverb_Gain_dB;
                [Value(Name = "Override Speaker Gain", Offset = 20)]
                public Single _20_Override_Speaker_Gain;
                [Value(Name = "Distance Bounds Lower", Offset = 24)]
                public Single _24_Distance_Bounds_Lower;
                [Value(Name = "Distance Bounds Upper", Offset = 28)]
                public Single _28_Distance_Bounds_Upper;
                [Value(Name = "Gain Bounds Lower", Offset = 32)]
                public Single _32_Gain_Bounds_Lower;
                [Value(Name = "Gain Bounds Upper", Offset = 36)]
                public Single _36_Gain_Bounds_Upper;
                [Value(Name = "Cutscene Ducking dB", Offset = 40)]
                public Single _40_Cutscene_Ducking_dB;
                [Value(Name = "Cutscene Ducking Fade In Time Sec", Offset = 44)]
                public Single _44_Cutscene_Ducking_Fade_In_Time_Sec;
                [Value(Name = "Cutscene Ducking Sustain Time Sec", Offset = 48)]
                public Single _48_Cutscene_Ducking_Sustain_Time_Sec;
                [Value(Name = "Cutscene Ducking Fade Out Time Sec", Offset = 52)]
                public Single _52_Cutscene_Ducking_Fade_Out_Time_Sec;
                [Value(Name = "Scripted Dialog Ducking dB", Offset = 56)]
                public Single _56_Scripted_Dialog_Ducking_dB;
                [Value(Name = "Scripted Dialog Ducking Fade In Time Sec", Offset = 60)]
                public Single _60_Scripted_Dialog_Ducking_Fade_In_Time_Sec;
                [Value(Name = "Scripted Dialog Ducking Sustain Time Sec", Offset = 64)]
                public Single _64_Scripted_Dialog_Ducking_Sustain_Time_Sec;
                [Value(Name = "Scripted Dialog Ducking Fade Out Time Sec", Offset = 68)]
                public Single _68_Scripted_Dialog_Ducking_Fade_Out_Time_Sec;
                [Value(Name = "Doppler Factor", Offset = 72)]
                public Single _72_Doppler_Factor;
                [Value(Name = "Stereo Playback Type", Offset = 76)]
                public H2.DataTypes.Enum _76_Stereo_Playback_Type = new H2.DataTypes.Enum(new string[] { "First Person", "Ambient" }, 4);
                [Value(Name = "Transmission Multiplier", Offset = 80)]
                public Single _80_Transmission_Multiplier;
                [Value(Name = "Obstruction Max Bend", Offset = 84)]
                public Single _84_Obstruction_Max_Bend;
                [Value(Name = "Occlusion Max Bend", Offset = 88)]
                public Single _88_Occlusion_Max_Bend;
            }
        }
    }
}