using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("wigl")]
        public class wigl
        {
[Value(Offset=0)]
public Byte[] _0_ = new byte[68];
[Value(Name="Unknown", Offset=68)]
public Single _68_Unknown;
[Value(Name="Auto typing text: time", Offset=72)]
public Int16 _72_Auto_typing_text__time;
[Value(Name="Auto typing text: words per line", Offset=74)]
public Int16 _74_Auto_typing_text__words_per_line;
[Value(Name="Auto typing text: size", Offset=76)]
public Single _76_Auto_typing_text__size;
[Value(Name="Auto typing text: Alpha?", Offset=80)]
public Single _80_Auto_typing_text__Alpha_;
[Value(Name="Red", Offset=84)]
public Single _84_Red;
[Value(Name="Green", Offset=88)]
public Single _88_Green;
[Value(Name="Blue", Offset=92)]
public Single _92_Blue;
[Value(Name="Unknown", Offset=96)]
public Single _96_Unknown;
[Value(Name="Blur blending speedUnknown", Offset=100)]
public Single _100_Blur_blending_speedUnknown;
[Value(Name="Blur blending speedUnknown", Offset=104)]
public Single _104_Blur_blending_speedUnknown;
[Value(Name="Blur color opacity", Offset=108)]
public Single _108_Blur_color_opacity;
[Value(Name="Blur Red", Offset=112)]
public Single _112_Blur_Red;
[Value(Name="Blur Green", Offset=116)]
public Single _116_Blur_Green;
[Value(Name="Blur Blue", Offset=120)]
public Single _120_Blur_Blue;
[Value(Name="Unused", Offset=124)]
public Single _124_Unused;
[Value(Name="Unused", Offset=128)]
public Single _128_Unused;
[Value(Name="Unused", Offset=132)]
public Single _132_Unused;
public _136_dialogs[] _136_Dialogs;
[Serializable][Reflexive(Name="Dialogs", Offset=136, ChunkSize=40, Label="")]
public class _136_dialogs
{
private int __StartOffset__;
[Value(Name="Dialog Group", Offset=0)]
public StringID _0_Dialog_Group;
[Value(Name="Unused", Offset=4)]
public Int16 _4_Unused;
[Value(Name="Window type? (wgit palette)", Offset=6)]
public Int16 _6_Window_type___wgit_palette_;
[Value(Name="Unic Text", Offset=8)]
public Dependancy _8_Unic_Text;
[Value(Name="Default Header", Offset=16)]
public StringID _16_Default_Header;
[Value(Name="Default Content", Offset=20)]
public StringID _20_Default_Content;
[Value(Name="Option 1", Offset=24)]
public StringID _24_Option_1;
[Value(Name="Option 2", Offset=28)]
public StringID _28_Option_2;
public _32_variations[] _32_Variations;
[Serializable][Reflexive(Name="Variations", Offset=32, ChunkSize=24, Label="")]
public class _32_variations
{
private int __StartOffset__;
[Value(Name="", Offset=0)]
public Int16 _0_;
[Value(Name="", Offset=2)]
public Int16 _2_;
[Value(Name="", Offset=4)]
public Int16 _4_;
[Value(Name="", Offset=6)]
public Int16 _6_;
[Value(Name="Header", Offset=8)]
public StringID _8_Header;
[Value(Name="Content", Offset=12)]
public StringID _12_Content;
[Value(Name="Option 1", Offset=16)]
public StringID _16_Option_1;
[Value(Name="Option 2", Offset=20)]
public StringID _20_Option_2;
}
}
[Value(Name="Selection Change Sound 1", Offset=144)]
public Dependancy _144_Selection_Change_Sound_1;
[Value(Name="Forward Sound 1", Offset=148)]
public Dependancy _148_Forward_Sound_1;
[Value(Name="Unknown", Offset=152)]
public Dependancy _152_Unknown;
[Value(Name="Advance Sound", Offset=156)]
public Dependancy _156_Advance_Sound;
[Value(Name="Back Sound", Offset=160)]
public Dependancy _160_Back_Sound;
[Value(Name="Shortcut Sound", Offset=164)]
public Dependancy _164_Shortcut_Sound;
[Value(Name="Selection Change Sound 2", Offset=168)]
public Dependancy _168_Selection_Change_Sound_2;
[Value(Name="Keyboard Click Sound", Offset=172)]
public Dependancy _172_Keyboard_Click_Sound;
[Value(Name="Message Recieved Sound", Offset=176)]
public Dependancy _176_Message_Recieved_Sound;
[Value(Name="Forward Sound 2", Offset=180)]
public Dependancy _180_Forward_Sound_2;
[Value(Name="Countdown Sound", Offset=184)]
public Dependancy _184_Countdown_Sound;
[Value(Offset=188)]
public Byte[] _188_ = new byte[8];
[Value(Name="Pickup Health Sound", Offset=196)]
public Dependancy _196_Pickup_Health_Sound;
[Value(Name="Unused", Offset=200)]
public Dependancy _200_Unused;
[Value(Name="Unused", Offset=204)]
public Dependancy _204_Unused;
[Value(Name="Unused", Offset=208)]
public Dependancy _208_Unused;
[Value(Name="Global Bitmaps", Offset=212)]
public Dependancy _212_Global_Bitmaps;
[Value(Name="Global Unic", Offset=216)]
public Dependancy _216_Global_Unic;
public _220_virtual_keyboard_buttons_settings[] _220_Virtual_Keyboard_Buttons_settings;
[Serializable][Reflexive(Name="Virtual Keyboard Buttons settings", Offset=220, ChunkSize=44, Label="")]
public class _220_virtual_keyboard_buttons_settings
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Int16 _0_Unused;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Selected button Brightness", Offset=4)]
public Int16 _4_Selected_button_Brightness;
[Value(Name="Unused", Offset=6)]
public Int16 _6_Unused;
public _8_button_switching_effects[] _8_Button_switching_effects;
[Serializable][Reflexive(Name="Button switching effects", Offset=8, ChunkSize=20, Label="")]
public class _8_button_switching_effects
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
[Value(Name="Selected button Brightness", Offset=16)]
public Int16 _16_Selected_button_Brightness;
[Value(Name="Unused", Offset=18)]
public Int16 _18_Unused;
public _20_button_switching_effects[] _20_Button_switching_effects;
[Serializable][Reflexive(Name="Button switching effects", Offset=20, ChunkSize=20, Label="")]
public class _20_button_switching_effects
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unknown", Offset=8)]
public Single _8_Unknown;
[Value(Name="Unknown", Offset=12)]
public Single _12_Unknown;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
[Value(Name="Unknown", Offset=28)]
public Int16 _28_Unknown;
[Value(Name="Unused", Offset=30)]
public Int16 _30_Unused;
[Value(Name="Unknown", Offset=32)]
public Int16 _32_Unknown;
[Value(Name="Unused", Offset=34)]
public Int16 _34_Unused;
public _36_unused[] _36_Unused;
[Serializable][Reflexive(Name="Unused", Offset=36, ChunkSize=20, Label="")]
public class _36_unused
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Single _0_Unused;
[Value(Name="Unused", Offset=4)]
public Single _4_Unused;
[Value(Name="Unused", Offset=8)]
public Single _8_Unused;
[Value(Name="Unused", Offset=12)]
public Single _12_Unused;
[Value(Name="Unused", Offset=16)]
public Single _16_Unused;
}
}
[Value(Offset=228)]
public Byte[] _228_ = new byte[8];
public _236_button_effects_[] _236_Button_effects_;
[Serializable][Reflexive(Name="Button effects?", Offset=236, ChunkSize=16, Label="")]
public class _236_button_effects_
{
private int __StartOffset__;
[Value(Name="Unused", Offset=0)]
public Int16 _0_Unused;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Brightness", Offset=4)]
public Int16 _4_Brightness;
[Value(Name="Unused", Offset=6)]
public Int16 _6_Unused;
public _8_button_switching_effects_[] _8_Button_switching_effects_;
[Serializable][Reflexive(Name="Button switching effects?", Offset=8, ChunkSize=20, Label="")]
public class _8_button_switching_effects_
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unused", Offset=2)]
public Int16 _2_Unused;
[Value(Name="Unknown", Offset=4)]
public Single _4_Unknown;
[Value(Name="Unused", Offset=8)]
public Single _8_Unused;
[Value(Name="Unused", Offset=12)]
public Single _12_Unused;
[Value(Name="Unknown", Offset=16)]
public Single _16_Unknown;
}
}
public _244_buttons_palette[] _244_Buttons_Palette;
[Serializable][Reflexive(Name="Buttons Palette", Offset=244, ChunkSize=8, Label="")]
public class _244_buttons_palette
{
private int __StartOffset__;
[Value(Name="Skin", Offset=0)]
public Dependancy _0_Skin;
}
[Value(Name="unic", Offset=252)]
public Dependancy _252_unic;
[Value(Name="Built-in Gametype Names", Offset=256)]
public Dependancy _256_Built_in_Gametype_Names;
[Value(Offset=260)]
public Byte[] _260_ = new byte[8];
public _268_unknown[] _268_Unknown;
[Serializable][Reflexive(Name="Unknown", Offset=268, ChunkSize=4, Label="")]
public class _268_unknown
{
private int __StartOffset__;
[Value(Name="Unknown", Offset=0)]
public Int16 _0_Unknown;
[Value(Name="Unknown", Offset=2)]
public Int16 _2_Unknown;
}
[Value(Name="Main window Header text #Font type#", Offset=276)]
public Int16 _276_Main_window_Header_text_NoFont_typeNo;
[Value(Name="Pop up window Header text #Font type#", Offset=278)]
public Int16 _278_Pop_up_window_Header_text_NoFont_typeNo;
[Value(Name="Pop up window Confirmation dialog Header text #Font type#", Offset=280)]
public Int16 _280_Pop_up_window_Confirmation_dialog_Header_text_NoFont_typeNo;
[Value(Name="Unknown Header Fontype", Offset=282)]
public Int16 _282_Unknown_Header_Fontype;
[Value(Name="Unknown Header Fontype", Offset=284)]
public Single _284_Unknown_Header_Fontype;
[Value(Name="Red Header Tittle", Offset=288)]
public Single _288_Red_Header_Tittle;
[Value(Name="Green Header Tittle", Offset=292)]
public Single _292_Green_Header_Tittle;
[Value(Name="Blue Header Tittle", Offset=296)]
public Single _296_Blue_Header_Tittle;
[Value(Name="Main Window Header text Coord Y", Offset=300)]
public Int16 _300_Main_Window_Header_text_Coord_Y;
[Value(Name="Main window Header text Coord X", Offset=302)]
public Int16 _302_Main_window_Header_text_Coord_X;
[Value(Name="Pop up window Confirmation dialog Header text Coord X", Offset=304)]
public Int16 _304_Pop_up_window_Confirmation_dialog_Header_text_Coord_X;
[Value(Name="Pop up window Confirmation dialog Header text Coord X", Offset=306)]
public Int16 _306_Pop_up_window_Confirmation_dialog_Header_text_Coord_X;
[Value(Name="Main window Buttons Coord Y", Offset=308)]
public Int16 _308_Main_window_Buttons_Coord_Y;
[Value(Name="Main window Buttons Coord x", Offset=310)]
public Int16 _310_Main_window_Buttons_Coord_x;
[Value(Name="Main window Buttons Coord Y", Offset=312)]
public Int16 _312_Main_window_Buttons_Coord_Y;
[Value(Name="Main window Buttons Coord X", Offset=314)]
public Int16 _314_Main_window_Buttons_Coord_X;
[Value(Name="Game setup Header text Coord Y", Offset=316)]
public Int16 _316_Game_setup_Header_text_Coord_Y;
[Value(Name="Game setup Header text Coord X", Offset=318)]
public Int16 _318_Game_setup_Header_text_Coord_X;
[Value(Name="Pop up Confirmation dialog Header text Coord Y", Offset=320)]
public Int16 _320_Pop_up_Confirmation_dialog_Header_text_Coord_Y;
[Value(Name="Pop up Confirmation dialog Header text Coord X", Offset=322)]
public Int16 _322_Pop_up_Confirmation_dialog_Header_text_Coord_X;
[Value(Name="Game setup Buttons Coord Y", Offset=324)]
public Int16 _324_Game_setup_Buttons_Coord_Y;
[Value(Name="Game setup Buttons Coord X", Offset=326)]
public Int16 _326_Game_setup_Buttons_Coord_X;
[Value(Name="Keyboard window Buttons Coord Y", Offset=328)]
public Int16 _328_Keyboard_window_Buttons_Coord_Y;
[Value(Name="Keyboard window Buttons Coord X", Offset=330)]
public Int16 _330_Keyboard_window_Buttons_Coord_X;
[Value(Name="Unknown Header text coords Y", Offset=332)]
public Int16 _332_Unknown_Header_text_coords_Y;
[Value(Name="Unknown Header text coords X", Offset=334)]
public Int16 _334_Unknown_Header_text_coords_X;
[Value(Name="Unknown Header text coords Y", Offset=336)]
public Int16 _336_Unknown_Header_text_coords_Y;
[Value(Name="Unknown Header text coords X", Offset=338)]
public Int16 _338_Unknown_Header_text_coords_X;
[Value(Name="Unknown Button coords Y", Offset=340)]
public Int16 _340_Unknown_Button_coords_Y;
[Value(Name="Unknown Button coords X", Offset=342)]
public Int16 _342_Unknown_Button_coords_X;
[Value(Name="Campaign/gametype settings Pop Up dialog Button Y", Offset=344)]
public Int16 _344_Campaign_gametype_settings_Pop_Up_dialog_Button_Y;
[Value(Name="Campaign/gametype settings Pop Up dialog Button X", Offset=346)]
public Int16 _346_Campaign_gametype_settings_Pop_Up_dialog_Button_X;
[Value(Name="Pop Up confirmation dialog Header Text Y", Offset=348)]
public Int16 _348_Pop_Up_confirmation_dialog_Header_Text_Y;
[Value(Name="Pop Up confirmation dialog Header Text X", Offset=350)]
public Int16 _350_Pop_Up_confirmation_dialog_Header_Text_X;
[Value(Name="Unknown Header text coords Y", Offset=352)]
public Int16 _352_Unknown_Header_text_coords_Y;
[Value(Name="Unknown Header text coords X", Offset=354)]
public Int16 _354_Unknown_Header_text_coords_X;
[Value(Name="Unknown Button coords Y", Offset=356)]
public Int16 _356_Unknown_Button_coords_Y;
[Value(Name="Unknown Button coords X", Offset=358)]
public Int16 _358_Unknown_Button_coords_X;
[Value(Name="Pop up Confirmation dialog Button Coord Y", Offset=360)]
public Int16 _360_Pop_up_Confirmation_dialog_Button_Coord_Y;
[Value(Name="Pop up Confirmation dialog Button Coord X", Offset=362)]
public Int16 _362_Pop_up_Confirmation_dialog_Button_Coord_X;
[Value(Name="Music", Offset=364)]
public Dependancy _364_Music;
[Value(Name="Music duration time?", Offset=368)]
public UInt32 _368_Music_duration_time_;
        }
    }
}