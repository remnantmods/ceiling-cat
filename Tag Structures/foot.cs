using System;
using H2.DataTypes;

namespace H2
{
    public sealed partial class TagStructures
    {
        [Serializable][Tag("foot")]
        public class foot
        {
public _0_effects[] _0_Effects;
[Serializable][Reflexive(Name="Effects", Offset=0, ChunkSize=24, Label="")]
public class _0_effects
{
private int __StartOffset__;
public _0_old_materials__do_not_use_[] _0_Old_Materials__Do_Not_Use_;
[Serializable][Reflexive(Name="Old Materials (Do Not Use)", Offset=0, ChunkSize=24, Label="")]
public class _0_old_materials__do_not_use_
{
private int __StartOffset__;
[Value(Name="Effect", Offset=0)]
public Dependancy _0_Effect;
[Value(Name="Sound", Offset=8)]
public Dependancy _8_Sound;
[Value(Name="Material Name", Offset=16)]
public StringID _16_Material_Name;
[Value(Name="Sweetener Mode", Offset=20)]
public H2.DataTypes.Enum _20_Sweetener_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Enabled", "Disabled" }, 4);
}
public _8_sounds[] _8_Sounds;
[Serializable][Reflexive(Name="Sounds", Offset=8, ChunkSize=24, Label="")]
public class _8_sounds
{
private int __StartOffset__;
[Value(Name="Sound 1", Offset=0)]
public Dependancy _0_Sound_1;
[Value(Name="Sound 2", Offset=8)]
public Dependancy _8_Sound_2;
[Value(Name="Material Name", Offset=16)]
public StringID _16_Material_Name;
[Value(Name="Sweetener Mode", Offset=20)]
public H2.DataTypes.Enum _20_Sweetener_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Enabled", "Disabled" }, 4);
}
public _16_effects[] _16_Effects;
[Serializable][Reflexive(Name="Effects", Offset=16, ChunkSize=24, Label="")]
public class _16_effects
{
private int __StartOffset__;
[Value(Name="Effect 1", Offset=0)]
public Dependancy _0_Effect_1;
[Value(Name="Effect 2", Offset=8)]
public Dependancy _8_Effect_2;
[Value(Name="Material Name", Offset=16)]
public StringID _16_Material_Name;
[Value(Name="Sweetener Mode", Offset=20)]
public H2.DataTypes.Enum _20_Sweetener_Mode = new H2.DataTypes.Enum(new string[] {  "Default", "Enabled", "Disabled" }, 4);
}
}
        }
    }
}