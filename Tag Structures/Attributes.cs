using System;
using System.Collections.Generic;
using System.Text;

namespace H2
{
    public sealed partial class TagStructures
    {
        public class Tag : Attribute
        {
            public string Name;
            public string Description;

            public Tag(string name, string description)
            { Name = name; Description = description; }

            public Tag(string name)
            { Name = name; }
        }

        public class Value : Attribute
        {
            public string Name;
            public int Offset;
        }

        public class Reflexive : Value
        {
            public int ChunkSize;
            public string Label;
        }

        public static int GetStartOffset(object Object)
        {
            return (int)Object.GetType().GetField("__StartOffset__", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(Object);
        }

        public static Value GetValueAttributes(object Object)
        {
            return (Value)Object.GetType().GetCustomAttributes(typeof(Value), false)[0];
        }

        public static Reflexive GetReflexiveAttributes(Array Object)
        {
            return (Reflexive)Object.GetType().GetElementType().GetCustomAttributes(typeof(Reflexive), false)[0];
        }
    }
}
