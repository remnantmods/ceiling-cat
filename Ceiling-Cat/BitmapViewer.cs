using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using H2;

namespace Ceiling_Cat
{
    public partial class BitmapViewer : UserControl
    {
        public BitmapViewer()
        {
            InitializeComponent();
        }

        public BitmapViewer(MapStream map,string mainMenuPath, string sharedPath, string spSharedPath)
        {
            InitializeComponent();
            MainMenuPath = mainMenuPath;
            SharedPath = sharedPath;
            SpSharedPath = spSharedPath;
            Map = map;
            picImage.BackColor = Color.FromArgb(BGColorSlide.Value, Color.Black);
        }

        string MainMenuPath;
        string SharedPath;
        string SpSharedPath;
        MapStream Map;

        string Location;
        string Format;

        private H2.Sections.BitmapCollection _bitmap;
        public H2.Sections.BitmapCollection Bitmap
        {
            get { return _bitmap; }
            set
            {
                _bitmap = value;
                cboChunks.Items.Clear();
                for (int i = 0; i < value.Count; i++)
                    cboChunks.Items.Add(value[i]);
                cboChunks.SelectedIndex = 0;
                //Location = value[0].RawLocation.ToString();
                //Format = value[0].Format.ToString();
                //lblLocation.Text = "Location: " + Location + "\nFormat: " + Format;
                //if (Location == H2.DataTypes.RawLocations.Internal.ToString()) cmdInternalise.Enabled = false;
                //else cmdInternalise.Enabled = true;
                //BitmapViewer viewer = new BitmapViewer();
                //BinaryReader mm = new BinaryReader(new FileStream(MainMenuPath, FileMode.Open));
                //BinaryReader s = new BinaryReader(new FileStream(SharedPath, FileMode.Open));
                //BinaryReader sps = new BinaryReader(new FileStream(SpSharedPath, FileMode.Open));
                //picImage.Image = value[0].LoadPreview(mm, s, sps);
                //this.Height = picImage.Image.Height + tableLayoutPanel1.Height + BGColorSlide.Height;
                //mm.Close();
                //s.Close();
                //sps.Close();
            }
        }

        private void cmdExtract_Click(object sender, EventArgs e)
        {

        }

        private void cmdInject_Click(object sender, EventArgs e)
        {

        }

        private void cmdInternalise_Click(object sender, EventArgs e)
        {
            BinaryReader mm = new BinaryReader(new FileStream(MainMenuPath, FileMode.Open));
            BinaryReader s = new BinaryReader(new FileStream(SharedPath, FileMode.Open));
            BinaryReader sps = new BinaryReader(new FileStream(SpSharedPath, FileMode.Open));
            Map.ReOpen();
            _bitmap.Internalise(mm, s, sps);
            Map.Close();
            mm.Close();
            s.Close();
            sps.Close();

            lblLocation.Text = "Location: Internal\nFormat: " + Format;
            cmdInternalise.Enabled = false;
            MessageBox.Show("Done.", "Bitmap Internalise", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void BGColorSlide_ValueChanged(object sender, EventArgs e)
        {
            picImage.BackColor = Color.FromArgb(BGColorSlide.Value, Color.Black);
        }

        private void cboChunks_SelectedIndexChanged(object sender, EventArgs e)
        {
            Map.ReOpen();
            Location = Bitmap[cboChunks.SelectedIndex].RawLocation.ToString();
            Format = Bitmap[cboChunks.SelectedIndex].Format.ToString();
            lblLocation.Text = "Location: " + Location + "\nFormat: " + Format;
            lblTotal.Text = "Total: " + Bitmap.Count.ToString();
            if (Location == H2.DataTypes.RawLocations.Internal.ToString()) cmdInternalise.Enabled = false;
            else cmdInternalise.Enabled = true;
            BitmapViewer viewer = new BitmapViewer();
            BinaryReader mm = new BinaryReader(new FileStream(MainMenuPath, FileMode.Open));
            BinaryReader s = new BinaryReader(new FileStream(SharedPath, FileMode.Open));
            BinaryReader sps = new BinaryReader(new FileStream(SpSharedPath, FileMode.Open));
            picImage.Image = ((H2.Sections.Bitmap)cboChunks.SelectedItem).LoadPreview(mm, s, sps);
            this.Height = picImage.Image.Height + panel1.Height + tableLayoutPanel1.Height + BGColorSlide.Height;
            mm.Close();
            s.Close();
            sps.Close();
            Map.Close();
        }

        private void cboChunks_Format(object sender, ListControlConvertEventArgs e)
        {
            if (e.ListItem.GetType() == typeof(H2.Sections.Bitmap))
                e.Value = Bitmap.IndexOf((H2.Sections.Bitmap)e.ListItem).ToString();
        }
    }
}
