namespace Ceiling_Cat
{
    partial class BitmapViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picImage = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLocation = new System.Windows.Forms.Label();
            this.cmdInject = new DevComponents.DotNetBar.ButtonX();
            this.cmdExtract = new DevComponents.DotNetBar.ButtonX();
            this.cmdInternalise = new DevComponents.DotNetBar.ButtonX();
            this.BGColorSlide = new DevComponents.DotNetBar.Controls.Slider();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboChunks = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.lblTotal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // picImage
            // 
            this.picImage.BackColor = System.Drawing.Color.Black;
            this.picImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picImage.Location = new System.Drawing.Point(0, 23);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(252, 191);
            this.picImage.TabIndex = 0;
            this.picImage.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblLocation, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmdInject, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmdExtract, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmdInternalise, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 214);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(252, 54);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLocation.Location = new System.Drawing.Point(3, 0);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(120, 27);
            this.lblLocation.TabIndex = 9;
            this.lblLocation.Text = "Location: \r\nFormat:";
            // 
            // cmdInject
            // 
            this.cmdInject.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdInject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdInject.Location = new System.Drawing.Point(3, 30);
            this.cmdInject.Name = "cmdInject";
            this.cmdInject.Size = new System.Drawing.Size(120, 21);
            this.cmdInject.TabIndex = 7;
            this.cmdInject.Text = "Inject";
            this.cmdInject.Click += new System.EventHandler(this.cmdInject_Click);
            // 
            // cmdExtract
            // 
            this.cmdExtract.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdExtract.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdExtract.Location = new System.Drawing.Point(129, 30);
            this.cmdExtract.Name = "cmdExtract";
            this.cmdExtract.Size = new System.Drawing.Size(120, 21);
            this.cmdExtract.TabIndex = 6;
            this.cmdExtract.Text = "Extract";
            this.cmdExtract.Click += new System.EventHandler(this.cmdExtract_Click);
            // 
            // cmdInternalise
            // 
            this.cmdInternalise.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdInternalise.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdInternalise.Location = new System.Drawing.Point(129, 3);
            this.cmdInternalise.Name = "cmdInternalise";
            this.cmdInternalise.Size = new System.Drawing.Size(120, 21);
            this.cmdInternalise.TabIndex = 5;
            this.cmdInternalise.Text = "Internalize";
            this.cmdInternalise.Click += new System.EventHandler(this.cmdInternalise_Click);
            // 
            // BGColorSlide
            // 
            this.BGColorSlide.Dock = System.Windows.Forms.DockStyle.Top;
            this.BGColorSlide.Location = new System.Drawing.Point(0, 0);
            this.BGColorSlide.Maximum = 255;
            this.BGColorSlide.Name = "BGColorSlide";
            this.BGColorSlide.Size = new System.Drawing.Size(252, 23);
            this.BGColorSlide.Step = 10;
            this.BGColorSlide.TabIndex = 6;
            this.BGColorSlide.Text = "Light";
            this.BGColorSlide.Value = 128;
            this.BGColorSlide.ValueChanged += new System.EventHandler(this.BGColorSlide_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTotal);
            this.panel1.Controls.Add(this.cboChunks);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 193);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(252, 21);
            this.panel1.TabIndex = 8;
            // 
            // cboChunks
            // 
            this.cboChunks.DisplayMember = "Text";
            this.cboChunks.Dock = System.Windows.Forms.DockStyle.Left;
            this.cboChunks.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cboChunks.FormattingEnabled = true;
            this.cboChunks.Location = new System.Drawing.Point(0, 0);
            this.cboChunks.Name = "cboChunks";
            this.cboChunks.Size = new System.Drawing.Size(171, 21);
            this.cboChunks.TabIndex = 8;
            this.cboChunks.SelectedIndexChanged += new System.EventHandler(this.cboChunks_SelectedIndexChanged);
            this.cboChunks.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cboChunks_Format);
            // 
            // lblTotal
            // 
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotal.Location = new System.Drawing.Point(171, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(81, 21);
            this.lblTotal.TabIndex = 9;
            this.lblTotal.Text = "Total: ";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BitmapViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.picImage);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.BGColorSlide);
            this.Name = "BitmapViewer";
            this.Size = new System.Drawing.Size(252, 268);
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevComponents.DotNetBar.ButtonX cmdInternalise;
        private System.Windows.Forms.Label lblLocation;
        private DevComponents.DotNetBar.ButtonX cmdInject;
        private DevComponents.DotNetBar.ButtonX cmdExtract;
        private DevComponents.DotNetBar.Controls.Slider BGColorSlide;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cboChunks;
        private System.Windows.Forms.Label lblTotal;
    }
}
