namespace Ceiling_Cat
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Settings));
            this.tabs = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.cmdMM = new DevComponents.DotNetBar.ButtonX();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMainMenu = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmdSPShared = new DevComponents.DotNetBar.ButtonX();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSPShared = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmdShared = new DevComponents.DotNetBar.ButtonX();
            this.label1 = new System.Windows.Forms.Label();
            this.txtShared = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.SharedMapsTab = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.cbTagsNewBackup = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cmdTagsBackupDir = new DevComponents.DotNetBar.ButtonX();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTagsBackupDir = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmdTagsBaseDir = new DevComponents.DotNetBar.ButtonX();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTagsBaseDir = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TagsTab = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.cbMapsAutoBackup = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cmdMapsBackupDir = new DevComponents.DotNetBar.ButtonX();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMapsBackupDir = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmdMapsDir = new DevComponents.DotNetBar.ButtonX();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMapsDir = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.TabMaps = new DevComponents.DotNetBar.TabItem(this.components);
            this.OFD = new System.Windows.Forms.OpenFileDialog();
            this.FBD = new System.Windows.Forms.FolderBrowserDialog();
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.cmdSave = new DevComponents.DotNetBar.ButtonItem();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.cmdBitmapLightColor = new DevComponents.DotNetBar.ButtonX();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tabs)).BeginInit();
            this.tabs.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.tabControlPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.CanReorderTabs = true;
            this.tabs.CloseButtonOnTabsAlwaysDisplayed = false;
            this.tabs.Controls.Add(this.tabControlPanel4);
            this.tabs.Controls.Add(this.tabControlPanel2);
            this.tabs.Controls.Add(this.tabControlPanel1);
            this.tabs.Controls.Add(this.tabControlPanel3);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabs.SelectedTabIndex = 0;
            this.tabs.Size = new System.Drawing.Size(617, 136);
            this.tabs.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabs.TabIndex = 0;
            this.tabs.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabs.Tabs.Add(this.SharedMapsTab);
            this.tabs.Tabs.Add(this.TabMaps);
            this.tabs.Tabs.Add(this.TagsTab);
            this.tabs.Tabs.Add(this.tabItem1);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.cmdMM);
            this.tabControlPanel1.Controls.Add(this.label3);
            this.tabControlPanel1.Controls.Add(this.txtMainMenu);
            this.tabControlPanel1.Controls.Add(this.cmdSPShared);
            this.tabControlPanel1.Controls.Add(this.label2);
            this.tabControlPanel1.Controls.Add(this.txtSPShared);
            this.tabControlPanel1.Controls.Add(this.cmdShared);
            this.tabControlPanel1.Controls.Add(this.label1);
            this.tabControlPanel1.Controls.Add(this.txtShared);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(617, 114);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.SharedMapsTab;
            // 
            // cmdMM
            // 
            this.cmdMM.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdMM.Location = new System.Drawing.Point(563, 60);
            this.cmdMM.Name = "cmdMM";
            this.cmdMM.Size = new System.Drawing.Size(42, 20);
            this.cmdMM.TabIndex = 8;
            this.cmdMM.Text = "...";
            this.cmdMM.Click += new System.EventHandler(this.cmdMM_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(47, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Main Menu";
            // 
            // txtMainMenu
            // 
            // 
            // 
            // 
            this.txtMainMenu.Border.Class = "TextBoxBorder";
            this.txtMainMenu.Location = new System.Drawing.Point(113, 60);
            this.txtMainMenu.Name = "txtMainMenu";
            this.txtMainMenu.Size = new System.Drawing.Size(444, 20);
            this.txtMainMenu.TabIndex = 6;
            // 
            // cmdSPShared
            // 
            this.cmdSPShared.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdSPShared.Location = new System.Drawing.Point(563, 34);
            this.cmdSPShared.Name = "cmdSPShared";
            this.cmdSPShared.Size = new System.Drawing.Size(42, 20);
            this.cmdSPShared.TabIndex = 5;
            this.cmdSPShared.Text = "...";
            this.cmdSPShared.Click += new System.EventHandler(this.cmdSPShared_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(4, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Single Player Shared";
            // 
            // txtSPShared
            // 
            // 
            // 
            // 
            this.txtSPShared.Border.Class = "TextBoxBorder";
            this.txtSPShared.Location = new System.Drawing.Point(113, 34);
            this.txtSPShared.Name = "txtSPShared";
            this.txtSPShared.Size = new System.Drawing.Size(444, 20);
            this.txtSPShared.TabIndex = 3;
            // 
            // cmdShared
            // 
            this.cmdShared.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdShared.Location = new System.Drawing.Point(563, 8);
            this.cmdShared.Name = "cmdShared";
            this.cmdShared.Size = new System.Drawing.Size(42, 20);
            this.cmdShared.TabIndex = 2;
            this.cmdShared.Text = "...";
            this.cmdShared.Click += new System.EventHandler(this.cmdShared_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(66, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Shared";
            // 
            // txtShared
            // 
            // 
            // 
            // 
            this.txtShared.Border.Class = "TextBoxBorder";
            this.txtShared.Location = new System.Drawing.Point(113, 8);
            this.txtShared.Name = "txtShared";
            this.txtShared.Size = new System.Drawing.Size(444, 20);
            this.txtShared.TabIndex = 0;
            // 
            // SharedMapsTab
            // 
            this.SharedMapsTab.AttachedControl = this.tabControlPanel1;
            this.SharedMapsTab.Name = "SharedMapsTab";
            this.SharedMapsTab.Text = "Shared Maps";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.cbTagsNewBackup);
            this.tabControlPanel2.Controls.Add(this.cmdTagsBackupDir);
            this.tabControlPanel2.Controls.Add(this.label4);
            this.tabControlPanel2.Controls.Add(this.txtTagsBackupDir);
            this.tabControlPanel2.Controls.Add(this.cmdTagsBaseDir);
            this.tabControlPanel2.Controls.Add(this.label5);
            this.tabControlPanel2.Controls.Add(this.txtTagsBaseDir);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(617, 114);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.TagsTab;
            // 
            // cbTagsNewBackup
            // 
            this.cbTagsNewBackup.BackColor = System.Drawing.Color.Transparent;
            this.cbTagsNewBackup.Location = new System.Drawing.Point(113, 60);
            this.cbTagsNewBackup.Name = "cbTagsNewBackup";
            this.cbTagsNewBackup.Size = new System.Drawing.Size(147, 21);
            this.cbTagsNewBackup.TabIndex = 15;
            this.cbTagsNewBackup.Text = "Auto Backup New Tags";
            // 
            // cmdTagsBackupDir
            // 
            this.cmdTagsBackupDir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdTagsBackupDir.Location = new System.Drawing.Point(563, 34);
            this.cmdTagsBackupDir.Name = "cmdTagsBackupDir";
            this.cmdTagsBackupDir.Size = new System.Drawing.Size(42, 20);
            this.cmdTagsBackupDir.TabIndex = 14;
            this.cmdTagsBackupDir.Text = "...";
            this.cmdTagsBackupDir.Click += new System.EventHandler(this.cmdTagsBackupDir_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(18, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Backup Directory";
            // 
            // txtTagsBackupDir
            // 
            // 
            // 
            // 
            this.txtTagsBackupDir.Border.Class = "TextBoxBorder";
            this.txtTagsBackupDir.Location = new System.Drawing.Point(113, 34);
            this.txtTagsBackupDir.Name = "txtTagsBackupDir";
            this.txtTagsBackupDir.Size = new System.Drawing.Size(444, 20);
            this.txtTagsBackupDir.TabIndex = 12;
            // 
            // cmdTagsBaseDir
            // 
            this.cmdTagsBaseDir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdTagsBaseDir.Location = new System.Drawing.Point(563, 8);
            this.cmdTagsBaseDir.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.cmdTagsBaseDir.Name = "cmdTagsBaseDir";
            this.cmdTagsBaseDir.Size = new System.Drawing.Size(42, 20);
            this.cmdTagsBaseDir.TabIndex = 11;
            this.cmdTagsBaseDir.Text = "...";
            this.cmdTagsBaseDir.Click += new System.EventHandler(this.cmdTagsBaseDir_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(31, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Base Directory";
            // 
            // txtTagsBaseDir
            // 
            // 
            // 
            // 
            this.txtTagsBaseDir.Border.Class = "TextBoxBorder";
            this.txtTagsBaseDir.Location = new System.Drawing.Point(113, 8);
            this.txtTagsBaseDir.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.txtTagsBaseDir.Name = "txtTagsBaseDir";
            this.txtTagsBaseDir.Size = new System.Drawing.Size(444, 20);
            this.txtTagsBaseDir.TabIndex = 9;
            // 
            // TagsTab
            // 
            this.TagsTab.AttachedControl = this.tabControlPanel2;
            this.TagsTab.Name = "TagsTab";
            this.TagsTab.Text = "Tags";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.cbMapsAutoBackup);
            this.tabControlPanel3.Controls.Add(this.cmdMapsBackupDir);
            this.tabControlPanel3.Controls.Add(this.label6);
            this.tabControlPanel3.Controls.Add(this.txtMapsBackupDir);
            this.tabControlPanel3.Controls.Add(this.cmdMapsDir);
            this.tabControlPanel3.Controls.Add(this.label7);
            this.tabControlPanel3.Controls.Add(this.txtMapsDir);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(617, 114);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.TabMaps;
            // 
            // cbMapsAutoBackup
            // 
            this.cbMapsAutoBackup.BackColor = System.Drawing.Color.Transparent;
            this.cbMapsAutoBackup.Location = new System.Drawing.Point(113, 60);
            this.cbMapsAutoBackup.Name = "cbMapsAutoBackup";
            this.cbMapsAutoBackup.Size = new System.Drawing.Size(287, 21);
            this.cbMapsAutoBackup.TabIndex = 22;
            this.cbMapsAutoBackup.Text = "Auto Backup Maps Before Adding Or Removing Data";
            // 
            // cmdMapsBackupDir
            // 
            this.cmdMapsBackupDir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdMapsBackupDir.Location = new System.Drawing.Point(563, 34);
            this.cmdMapsBackupDir.Name = "cmdMapsBackupDir";
            this.cmdMapsBackupDir.Size = new System.Drawing.Size(42, 20);
            this.cmdMapsBackupDir.TabIndex = 21;
            this.cmdMapsBackupDir.Text = "...";
            this.cmdMapsBackupDir.Click += new System.EventHandler(this.cmdMapsBackupDir_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(18, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Backup Directory";
            // 
            // txtMapsBackupDir
            // 
            // 
            // 
            // 
            this.txtMapsBackupDir.Border.Class = "TextBoxBorder";
            this.txtMapsBackupDir.Location = new System.Drawing.Point(113, 34);
            this.txtMapsBackupDir.Name = "txtMapsBackupDir";
            this.txtMapsBackupDir.Size = new System.Drawing.Size(444, 20);
            this.txtMapsBackupDir.TabIndex = 19;
            // 
            // cmdMapsDir
            // 
            this.cmdMapsDir.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdMapsDir.Location = new System.Drawing.Point(563, 8);
            this.cmdMapsDir.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.cmdMapsDir.Name = "cmdMapsDir";
            this.cmdMapsDir.Size = new System.Drawing.Size(42, 20);
            this.cmdMapsDir.TabIndex = 18;
            this.cmdMapsDir.Text = "...";
            this.cmdMapsDir.Click += new System.EventHandler(this.cmdMapsDir_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(58, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Directory";
            // 
            // txtMapsDir
            // 
            // 
            // 
            // 
            this.txtMapsDir.Border.Class = "TextBoxBorder";
            this.txtMapsDir.Location = new System.Drawing.Point(113, 8);
            this.txtMapsDir.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.txtMapsDir.Name = "txtMapsDir";
            this.txtMapsDir.Size = new System.Drawing.Size(444, 20);
            this.txtMapsDir.TabIndex = 16;
            // 
            // TabMaps
            // 
            this.TabMaps.AttachedControl = this.tabControlPanel3;
            this.TabMaps.Name = "TabMaps";
            this.TabMaps.Text = "Maps";
            // 
            // OFD
            // 
            this.OFD.Title = "Open Halo 2 Map File...";
            // 
            // bar1
            // 
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cmdSave});
            this.bar1.Location = new System.Drawing.Point(0, 136);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(617, 25);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 1;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // cmdSave
            // 
            this.cmdSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdSave.Image = global::Ceiling_Cat.Properties.Resources.save_16;
            this.cmdSave.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Stretch = true;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // tabItem1
            // 
            this.tabItem1.AttachedControl = this.tabControlPanel4;
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "Raw Viewer";
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.label8);
            this.tabControlPanel4.Controls.Add(this.cmdBitmapLightColor);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(617, 114);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 4;
            this.tabControlPanel4.TabItem = this.tabItem1;
            // 
            // cmdBitmapLightColor
            // 
            this.cmdBitmapLightColor.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.cmdBitmapLightColor.Location = new System.Drawing.Point(101, 4);
            this.cmdBitmapLightColor.Name = "cmdBitmapLightColor";
            this.cmdBitmapLightColor.Size = new System.Drawing.Size(86, 19);
            this.cmdBitmapLightColor.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(3, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Bitmap Light Color";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 161);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.bar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Settings";
            this.ShowInTaskbar = false;
            this.Text = "Settings";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Settings_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.tabs)).EndInit();
            this.tabs.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.tabControlPanel1.PerformLayout();
            this.tabControlPanel2.ResumeLayout(false);
            this.tabControlPanel2.PerformLayout();
            this.tabControlPanel3.ResumeLayout(false);
            this.tabControlPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.tabControlPanel4.ResumeLayout(false);
            this.tabControlPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.TabControl tabs;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem SharedMapsTab;
        private DevComponents.DotNetBar.ButtonX cmdMM;
        private System.Windows.Forms.Label label3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMainMenu;
        private DevComponents.DotNetBar.ButtonX cmdSPShared;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtSPShared;
        private DevComponents.DotNetBar.ButtonX cmdShared;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.Controls.TextBoxX txtShared;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem TagsTab;
        private DevComponents.DotNetBar.ButtonX cmdTagsBackupDir;
        private System.Windows.Forms.Label label4;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTagsBackupDir;
        private DevComponents.DotNetBar.ButtonX cmdTagsBaseDir;
        private System.Windows.Forms.Label label5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTagsBaseDir;
        private System.Windows.Forms.OpenFileDialog OFD;
        private System.Windows.Forms.FolderBrowserDialog FBD;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbTagsNewBackup;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbMapsAutoBackup;
        private DevComponents.DotNetBar.ButtonX cmdMapsBackupDir;
        private System.Windows.Forms.Label label6;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMapsBackupDir;
        private DevComponents.DotNetBar.ButtonX cmdMapsDir;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.Controls.TextBoxX txtMapsDir;
        private DevComponents.DotNetBar.TabItem TabMaps;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.ButtonItem cmdSave;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private DevComponents.DotNetBar.ButtonX cmdBitmapLightColor;
        private System.Windows.Forms.Label label8;
    }
}