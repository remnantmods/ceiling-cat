using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ceiling_Cat
{
    public partial class TagAddOptionDialogue : Form
    {
        public TagAddOptionDialogue()
        {
            InitializeComponent();
        }

        public string TagName { get { return txtTagName.Text; } set { txtTagName.Text = value; } }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (TagName.Length > 0)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}