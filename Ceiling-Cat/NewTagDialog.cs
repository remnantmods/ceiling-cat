using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ceiling_Cat
{
    public partial class NewTagDialog : Form
    {
        public NewTagDialog()
        {
            InitializeComponent();

            Type[] tagTypes = typeof(H2.TagStructures).GetNestedTypes();
            foreach (Type t in tagTypes)
                cboTags.Items.Add(t.Name);
        }

        private void NewTagDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void cmdCreate_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}