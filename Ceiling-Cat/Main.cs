using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySettings = Ceiling_Cat.Properties.Settings;
using System.IO;
using H2;

namespace Ceiling_Cat
{
    public partial class Main : Form
    {
        Settings settings = new Settings();
        NewTagDialog newTagDialog = new NewTagDialog();

        public Stream MainMenuMap()
        { return new FileStream(MySettings.Default.MainMenu, FileMode.Open); }

        public Stream SharedMap()
        { return new FileStream(MySettings.Default.Shared, FileMode.Open); }

        public Stream SpSharedMap()
        { return new FileStream(MySettings.Default.SingleplayerShared, FileMode.Open); }

        public Main()
        {
            InitializeComponent();

            Type[] tagTypes = typeof(H2.TagStructures).GetNestedTypes();
            foreach (Type t in tagTypes)
                tagOFD.Filter += "|" + t.Name + " (*." + t.Name +")|*." + t.Name;

            if (!File.Exists(MySettings.Default.MainMenu) || !File.Exists(MySettings.Default.Shared) || !File.Exists(MySettings.Default.SingleplayerShared))
                settingsToolStripMenuItem_Click(null, null);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapEditor m = new MapEditor(this, "");
            m.MdiParent = this;
            m.Show();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (mapOFD.ShowDialog() == DialogResult.OK)
            {
                MapEditor m = new MapEditor(this, mapOFD.FileName);
                m.MdiParent = this;
                m.Show();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settings.Show();
        }

        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            newTagDialog.Show();
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (tagOFD.ShowDialog() == DialogResult.OK)
            { }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Form f in this.MdiChildren)
                f.Close();
        }
    }
}