using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySettings = Ceiling_Cat.Properties.Settings;

namespace Ceiling_Cat
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();

            cbMapsAutoBackup.Checked = MySettings.Default.AutoBackupMaps;
            cbTagsNewBackup.Checked = MySettings.Default.AutoBackupNewTags;
            txtMainMenu.Text = MySettings.Default.MainMenu;
            txtMapsBackupDir.Text = MySettings.Default.MapsBackupDir;
            txtMapsDir.Text = MySettings.Default.MapsDir;
            txtShared.Text = MySettings.Default.Shared;
            txtSPShared.Text = MySettings.Default.SingleplayerShared;
            txtTagsBackupDir.Text = MySettings.Default.TagsBackupDir;
            txtTagsBaseDir.Text = MySettings.Default.TagsRootDir;
        }

        private void cmdShared_Click(object sender, EventArgs e)
        {
            OFD.Filter = "Shared (shared.map)|shared.map";
            if (OFD.ShowDialog() == DialogResult.OK)
                txtShared.Text = OFD.FileName;
        }

        private void cmdSPShared_Click(object sender, EventArgs e)
        {
            OFD.Filter = "Singleplayer Shared (single_player_shared.map)|single_player_shared.map";
            if (OFD.ShowDialog() == DialogResult.OK)
                txtSPShared.Text = OFD.FileName;
        }

        private void cmdMM_Click(object sender, EventArgs e)
        {
            OFD.Filter = "Main Menu (mainmenu.map)|mainmenu.map";
            if (OFD.ShowDialog() == DialogResult.OK)
                txtMainMenu.Text = OFD.FileName;
        }

        private void cmdTagsBaseDir_Click(object sender, EventArgs e)
        {
            FBD.Description = "Select The Folder That Will Be Used As The Base Directory For Tags.";
            if (FBD.ShowDialog() == DialogResult.OK)
                txtTagsBaseDir.Text = FBD.SelectedPath;
        }

        private void cmdTagsBackupDir_Click(object sender, EventArgs e)
        {
            FBD.Description = "Select The Folder That Will Be Used To Store Backups Of Tags.";
            if (FBD.ShowDialog() == DialogResult.OK)
                txtTagsBackupDir.Text = FBD.SelectedPath;
        }

        private void cmdMapsDir_Click(object sender, EventArgs e)
        {
            FBD.Description = "Select The Folder That Contains Your Halo 2 Map Files.";
            if (FBD.ShowDialog() == DialogResult.OK)
                txtMapsDir.Text = FBD.SelectedPath;
        }

        private void cmdMapsBackupDir_Click(object sender, EventArgs e)
        {
            FBD.Description = "Select The Folder That Will Be Used To Store Backups Of Your Halo 2 Map Files.";
            if (FBD.ShowDialog() == DialogResult.OK)
                txtMapsBackupDir.Text = FBD.SelectedPath;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            MySettings.Default.AutoBackupMaps = cbMapsAutoBackup.Checked;
            MySettings.Default.AutoBackupNewTags = cbTagsNewBackup.Checked;
            MySettings.Default.MainMenu = txtMainMenu.Text;
            MySettings.Default.MapsBackupDir = txtMapsBackupDir.Text;
            MySettings.Default.MapsDir = txtMapsDir.Text;
            MySettings.Default.Shared = txtShared.Text;
            MySettings.Default.SingleplayerShared = txtSPShared.Text;
            MySettings.Default.TagsBackupDir = txtTagsBackupDir.Text;
            MySettings.Default.TagsRootDir = txtTagsBaseDir.Text;
            MySettings.Default.Save();
        }

        private void Settings_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}