namespace Ceiling_Cat
{
    partial class MapEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapEditor));
            this.tabs = new DevComponents.DotNetBar.TabControl();
            this.TagsPanel = new DevComponents.DotNetBar.TabControlPanel();
            this.trvTags = new System.Windows.Forms.TreeView();
            this.expandableSplitter1 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.TagsBar = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem1 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem1 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem16 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem2 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagClass = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem4 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagPath = new DevComponents.DotNetBar.TextBoxItem();
            this.cmdTagInfoSave = new DevComponents.DotNetBar.ButtonItem();
            this.RawBar = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.explorerBarGroupItem13 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.cmdMetaExtract = new DevComponents.DotNetBar.ButtonItem();
            this.cmdMetaInject = new DevComponents.DotNetBar.ButtonItem();
            this.cmdMetaRemove = new DevComponents.DotNetBar.ButtonItem();
            this.cmdMetaDuplicate = new DevComponents.DotNetBar.ButtonItem();
            this.Tags = new DevComponents.DotNetBar.TabItem(this.components);
            this.HeaderPanel = new DevComponents.DotNetBar.TabControlPanel();
            this.itemPanel2 = new DevComponents.DotNetBar.ItemPanel();
            this.lblHead = new DevComponents.DotNetBar.LabelItem();
            this.lblHead2 = new DevComponents.DotNetBar.LabelItem();
            this.txtHead = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem17 = new DevComponents.DotNetBar.LabelItem();
            this.txtVersion = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem18 = new DevComponents.DotNetBar.LabelItem();
            this.txtFileSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem19 = new DevComponents.DotNetBar.LabelItem();
            this.txtIndexOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem20 = new DevComponents.DotNetBar.LabelItem();
            this.txtIndexSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem21 = new DevComponents.DotNetBar.LabelItem();
            this.txtMetaStart = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem22 = new DevComponents.DotNetBar.LabelItem();
            this.txtMetaSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem23 = new DevComponents.DotNetBar.LabelItem();
            this.txtNonRawSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem24 = new DevComponents.DotNetBar.LabelItem();
            this.txtOrigin = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem25 = new DevComponents.DotNetBar.LabelItem();
            this.txtBuildDate = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem26 = new DevComponents.DotNetBar.LabelItem();
            this.cboMapType = new DevComponents.DotNetBar.ComboBoxItem();
            this.cboItemSinglePlayerMap = new DevComponents.Editors.ComboItem();
            this.cboItemMultiplayerMap = new DevComponents.Editors.ComboItem();
            this.cboItemMainMenu = new DevComponents.Editors.ComboItem();
            this.cboItemShared = new DevComponents.Editors.ComboItem();
            this.cboItemSinglePlayerShared = new DevComponents.Editors.ComboItem();
            this.labelItem27 = new DevComponents.DotNetBar.LabelItem();
            this.txtStrangeFileStringsSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem30 = new DevComponents.DotNetBar.LabelItem();
            this.txtStrangeFileStringsOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem31 = new DevComponents.DotNetBar.LabelItem();
            this.txtStringIDs128Offset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem32 = new DevComponents.DotNetBar.LabelItem();
            this.txtStringIDCount = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem33 = new DevComponents.DotNetBar.LabelItem();
            this.txtStringIDsSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem34 = new DevComponents.DotNetBar.LabelItem();
            this.txtStringIDsIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem35 = new DevComponents.DotNetBar.LabelItem();
            this.txtStringIDsOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem36 = new DevComponents.DotNetBar.LabelItem();
            this.txtInternalName = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem37 = new DevComponents.DotNetBar.LabelItem();
            this.txtScenarioName = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem38 = new DevComponents.DotNetBar.LabelItem();
            this.txtFileTableCount = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem39 = new DevComponents.DotNetBar.LabelItem();
            this.txtFileTableOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem40 = new DevComponents.DotNetBar.LabelItem();
            this.txtFileTableSize = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem41 = new DevComponents.DotNetBar.LabelItem();
            this.txtFileTableIndexOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem42 = new DevComponents.DotNetBar.LabelItem();
            this.txtChecksum = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem43 = new DevComponents.DotNetBar.LabelItem();
            this.txtFoot = new DevComponents.DotNetBar.TextBoxItem();
            this.cmdHeaderSave = new DevComponents.DotNetBar.ButtonItem();
            this.Header = new DevComponents.DotNetBar.TabItem(this.components);
            this.UnicodePanel = new DevComponents.DotNetBar.TabControlPanel();
            this.UnicodeLanTabs = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstEnglish = new System.Windows.Forms.ListBox();
            this.expandableSplitter3 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar3 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem4 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem5 = new DevComponents.DotNetBar.LabelItem();
            this.txtEnglishIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULEnglish = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel5 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstSpanish = new System.Windows.Forms.ListBox();
            this.expandableSplitter7 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar7 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem8 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem48 = new DevComponents.DotNetBar.LabelItem();
            this.txtSpanishIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULSpanish = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel9 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstPortuguese = new System.Windows.Forms.ListBox();
            this.expandableSplitter11 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar11 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem12 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem52 = new DevComponents.DotNetBar.LabelItem();
            this.txtPortugeseIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULPortuguese = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel8 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstChinese = new System.Windows.Forms.ListBox();
            this.expandableSplitter10 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar10 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem11 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem51 = new DevComponents.DotNetBar.LabelItem();
            this.txtChineseIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULChinese = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel7 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstKorean = new System.Windows.Forms.ListBox();
            this.expandableSplitter9 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar9 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem10 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem50 = new DevComponents.DotNetBar.LabelItem();
            this.txtKoreanIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULKorean = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel6 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstItalian = new System.Windows.Forms.ListBox();
            this.expandableSplitter8 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar8 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem9 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem49 = new DevComponents.DotNetBar.LabelItem();
            this.txtItalianIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULItalian = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel4 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstFrench = new System.Windows.Forms.ListBox();
            this.expandableSplitter6 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar6 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem7 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem47 = new DevComponents.DotNetBar.LabelItem();
            this.txtFrenchIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULFrench = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstDutch = new System.Windows.Forms.ListBox();
            this.expandableSplitter5 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar5 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem6 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem46 = new DevComponents.DotNetBar.LabelItem();
            this.txtDutchIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULDutch = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.lstJapanese = new System.Windows.Forms.ListBox();
            this.expandableSplitter4 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar4 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem5 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem45 = new DevComponents.DotNetBar.LabelItem();
            this.txtJapaneseIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.ULJapanese = new DevComponents.DotNetBar.TabItem(this.components);
            this.Unicode = new DevComponents.DotNetBar.TabItem(this.components);
            this.IndexPanel = new DevComponents.DotNetBar.TabControlPanel();
            this.itemPanel1 = new DevComponents.DotNetBar.ItemPanel();
            this.labelItem6 = new DevComponents.DotNetBar.LabelItem();
            this.txtPrimaryMagicConstant = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem7 = new DevComponents.DotNetBar.LabelItem();
            this.txtPrimaryMagic = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem8 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagInfoIndexPointer = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem9 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagInfoIndexOffset = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem10 = new DevComponents.DotNetBar.LabelItem();
            this.txtScenarioID = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem11 = new DevComponents.DotNetBar.LabelItem();
            this.txtGlobalsID = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem12 = new DevComponents.DotNetBar.LabelItem();
            this.txtTagCount = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem13 = new DevComponents.DotNetBar.LabelItem();
            this.txtTags = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem14 = new DevComponents.DotNetBar.LabelItem();
            this.txtSecondaryMagicConstant = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem15 = new DevComponents.DotNetBar.LabelItem();
            this.txtSecondaryMagic = new DevComponents.DotNetBar.TextBoxItem();
            this.cmdIndexSave = new DevComponents.DotNetBar.ButtonItem();
            this.Index = new DevComponents.DotNetBar.TabItem(this.components);
            this.StringIDsPanel = new DevComponents.DotNetBar.TabControlPanel();
            this.lstSIDs = new System.Windows.Forms.ListBox();
            this.expandableSplitter2 = new DevComponents.DotNetBar.ExpandableSplitter();
            this.explorerBar2 = new DevComponents.DotNetBar.ExplorerBar();
            this.explorerBarGroupItem3 = new DevComponents.DotNetBar.ExplorerBarGroupItem();
            this.labelItem3 = new DevComponents.DotNetBar.LabelItem();
            this.txtSIDIndex = new DevComponents.DotNetBar.TextBoxItem();
            this.labelItem44 = new DevComponents.DotNetBar.LabelItem();
            this.txtSIDName = new DevComponents.DotNetBar.TextBoxItem();
            this.cmdSIDSave = new DevComponents.DotNetBar.ButtonItem();
            this.StringIDs = new DevComponents.DotNetBar.TabItem(this.components);
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.lblStatus = new DevComponents.DotNetBar.LabelItem();
            this.probar = new DevComponents.DotNetBar.ProgressBarItem();
            this.labelItem28 = new DevComponents.DotNetBar.LabelItem();
            this.labelItem29 = new DevComponents.DotNetBar.LabelItem();
            this.ribbonControl1 = new DevComponents.DotNetBar.RibbonControl();
            this.office2007StartButton1 = new DevComponents.DotNetBar.Office2007StartButton();
            this.itemContainer1 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer2 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer3 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem7 = new DevComponents.DotNetBar.ButtonItem();
            this.itemContainer4 = new DevComponents.DotNetBar.ItemContainer();
            this.itemContainer5 = new DevComponents.DotNetBar.ItemContainer();
            this.buttonItem12 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem13 = new DevComponents.DotNetBar.ButtonItem();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.cmdAddTagFromFile = new DevComponents.DotNetBar.ButtonItem();
            this.cmdExtractAllTags = new DevComponents.DotNetBar.ButtonItem();
            this.cmdResign = new DevComponents.DotNetBar.ButtonItem();
            this.RawDataButton = new DevComponents.DotNetBar.ButtonItem();
            this.TagInfoPaneButton = new DevComponents.DotNetBar.ButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.tabs)).BeginInit();
            this.tabs.SuspendLayout();
            this.TagsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TagsBar)).BeginInit();
            this.HeaderPanel.SuspendLayout();
            this.UnicodePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnicodeLanTabs)).BeginInit();
            this.UnicodeLanTabs.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar3)).BeginInit();
            this.tabControlPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar7)).BeginInit();
            this.tabControlPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar11)).BeginInit();
            this.tabControlPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar10)).BeginInit();
            this.tabControlPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar9)).BeginInit();
            this.tabControlPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar8)).BeginInit();
            this.tabControlPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar6)).BeginInit();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar5)).BeginInit();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar4)).BeginInit();
            this.IndexPanel.SuspendLayout();
            this.StringIDsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.CanReorderTabs = true;
            this.tabs.CloseButtonOnTabsAlwaysDisplayed = false;
            this.tabs.Controls.Add(this.StringIDsPanel);
            this.tabs.Controls.Add(this.HeaderPanel);
            this.tabs.Controls.Add(this.TagsPanel);
            this.tabs.Controls.Add(this.UnicodePanel);
            this.tabs.Controls.Add(this.IndexPanel);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 45);
            this.tabs.Name = "tabs";
            this.tabs.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabs.SelectedTabIndex = 4;
            this.tabs.Size = new System.Drawing.Size(1059, 679);
            this.tabs.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document;
            this.tabs.TabIndex = 0;
            this.tabs.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabs.Tabs.Add(this.Header);
            this.tabs.Tabs.Add(this.Index);
            this.tabs.Tabs.Add(this.StringIDs);
            this.tabs.Tabs.Add(this.Unicode);
            this.tabs.Tabs.Add(this.Tags);
            this.tabs.Text = "Map";
            // 
            // TagsPanel
            // 
            this.TagsPanel.Controls.Add(this.trvTags);
            this.TagsPanel.Controls.Add(this.expandableSplitter1);
            this.TagsPanel.Controls.Add(this.TagsBar);
            this.TagsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TagsPanel.Location = new System.Drawing.Point(0, 22);
            this.TagsPanel.Name = "TagsPanel";
            this.TagsPanel.Padding = new System.Windows.Forms.Padding(1);
            this.TagsPanel.Size = new System.Drawing.Size(1059, 657);
            this.TagsPanel.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.TagsPanel.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.TagsPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.TagsPanel.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.TagsPanel.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.TagsPanel.Style.GradientAngle = 90;
            this.TagsPanel.TabIndex = 5;
            this.TagsPanel.TabItem = this.Tags;
            // 
            // trvTags
            // 
            this.trvTags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trvTags.Location = new System.Drawing.Point(1, 1);
            this.trvTags.Name = "trvTags";
            this.trvTags.Size = new System.Drawing.Size(771, 655);
            this.trvTags.TabIndex = 3;
            this.trvTags.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trvTags_AfterSelect);
            // 
            // expandableSplitter1
            // 
            this.expandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter1.Location = new System.Drawing.Point(772, 1);
            this.expandableSplitter1.Name = "expandableSplitter1";
            this.expandableSplitter1.Size = new System.Drawing.Size(10, 655);
            this.expandableSplitter1.TabIndex = 5;
            this.expandableSplitter1.TabStop = false;
            // 
            // TagsBar
            // 
            this.TagsBar.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.TagsBar.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.TagsBar.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.TagsBar.BackStyle.BackColorGradientAngle = 90;
            this.TagsBar.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.TagsBar.Cursor = System.Windows.Forms.Cursors.Default;
            this.TagsBar.Dock = System.Windows.Forms.DockStyle.Right;
            this.TagsBar.GroupImages = null;
            this.TagsBar.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem1,
            this.RawBar,
            this.explorerBarGroupItem13});
            this.TagsBar.Images = null;
            this.TagsBar.Location = new System.Drawing.Point(782, 1);
            this.TagsBar.Name = "TagsBar";
            this.TagsBar.Size = new System.Drawing.Size(276, 655);
            this.TagsBar.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.TagsBar.TabIndex = 4;
            this.TagsBar.ThemeAware = true;
            // 
            // explorerBarGroupItem1
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem1.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem1.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem1.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem1.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem1.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem1.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem1.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem1.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem1.CanCustomize = false;
            this.explorerBarGroupItem1.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem1.ExpandBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(189)))), ((int)(((byte)(203)))));
            this.explorerBarGroupItem1.Expanded = true;
            this.explorerBarGroupItem1.ExpandForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(115)))));
            this.explorerBarGroupItem1.ExpandHotBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(195)))), ((int)(((byte)(208)))));
            this.explorerBarGroupItem1.ExpandHotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.explorerBarGroupItem1.Name = "explorerBarGroupItem1";
            this.explorerBarGroupItem1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem1,
            this.txtTagOffset,
            this.labelItem16,
            this.txtTagSize,
            this.labelItem2,
            this.txtTagClass,
            this.labelItem4,
            this.txtTagPath,
            this.cmdTagInfoSave});
            this.explorerBarGroupItem1.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem1.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem1.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem1.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem1.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem1.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem1.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem1.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem1.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem1.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem1.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem1.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem1.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem1
            // 
            this.labelItem1.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem1.Name = "labelItem1";
            this.labelItem1.Text = "Offset";
            this.labelItem1.ThemeAware = true;
            // 
            // txtTagOffset
            // 
            this.txtTagOffset.AlwaysShowCaption = false;
            this.txtTagOffset.ControlText = "";
            this.txtTagOffset.Enabled = false;
            this.txtTagOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagOffset.Name = "txtTagOffset";
            this.txtTagOffset.RecentlyUsed = false;
            this.txtTagOffset.SelectedText = "";
            this.txtTagOffset.SelectionLength = 0;
            this.txtTagOffset.SelectionStart = 0;
            this.txtTagOffset.TextBoxWidth = 64;
            this.txtTagOffset.ThemeAware = true;
            // 
            // labelItem16
            // 
            this.labelItem16.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem16.Name = "labelItem16";
            this.labelItem16.Text = "Size";
            this.labelItem16.ThemeAware = true;
            // 
            // txtTagSize
            // 
            this.txtTagSize.AlwaysShowCaption = false;
            this.txtTagSize.ControlText = "";
            this.txtTagSize.Enabled = false;
            this.txtTagSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagSize.Name = "txtTagSize";
            this.txtTagSize.RecentlyUsed = false;
            this.txtTagSize.SelectedText = "";
            this.txtTagSize.SelectionLength = 0;
            this.txtTagSize.SelectionStart = 0;
            this.txtTagSize.TextBoxWidth = 64;
            this.txtTagSize.ThemeAware = true;
            // 
            // labelItem2
            // 
            this.labelItem2.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem2.Name = "labelItem2";
            this.labelItem2.Text = "Class";
            this.labelItem2.ThemeAware = true;
            // 
            // txtTagClass
            // 
            this.txtTagClass.AlwaysShowCaption = false;
            this.txtTagClass.ControlText = "";
            this.txtTagClass.Enabled = false;
            this.txtTagClass.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagClass.Name = "txtTagClass";
            this.txtTagClass.RecentlyUsed = false;
            this.txtTagClass.SelectedText = "";
            this.txtTagClass.SelectionLength = 0;
            this.txtTagClass.SelectionStart = 0;
            this.txtTagClass.Text = "textBoxItem1";
            this.txtTagClass.TextBoxWidth = 64;
            this.txtTagClass.ThemeAware = true;
            // 
            // labelItem4
            // 
            this.labelItem4.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem4.Name = "labelItem4";
            this.labelItem4.Text = "Path";
            this.labelItem4.ThemeAware = true;
            // 
            // txtTagPath
            // 
            this.txtTagPath.AlwaysShowCaption = false;
            this.txtTagPath.ControlText = "";
            this.txtTagPath.Enabled = false;
            this.txtTagPath.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagPath.Name = "txtTagPath";
            this.txtTagPath.RecentlyUsed = false;
            this.txtTagPath.SelectedText = "";
            this.txtTagPath.SelectionLength = 0;
            this.txtTagPath.SelectionStart = 0;
            this.txtTagPath.Text = "textBoxItem1";
            this.txtTagPath.TextBoxWidth = 64;
            this.txtTagPath.ThemeAware = true;
            // 
            // cmdTagInfoSave
            // 
            this.cmdTagInfoSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdTagInfoSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdTagInfoSave.Enabled = false;
            this.cmdTagInfoSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.cmdTagInfoSave.HotFontUnderline = true;
            this.cmdTagInfoSave.HotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.cmdTagInfoSave.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None;
            this.cmdTagInfoSave.Image = global::Ceiling_Cat.Properties.Resources.save_16;
            this.cmdTagInfoSave.Name = "cmdTagInfoSave";
            this.cmdTagInfoSave.Text = "Save";
            this.cmdTagInfoSave.Click += new System.EventHandler(this.cmdTagInfoSave_Click);
            // 
            // RawBar
            // 
            // 
            // 
            // 
            this.RawBar.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.RawBar.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.RawBar.BackStyle.BorderBottomWidth = 1;
            this.RawBar.BackStyle.BorderColor = System.Drawing.Color.White;
            this.RawBar.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.RawBar.BackStyle.BorderLeftWidth = 1;
            this.RawBar.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.RawBar.BackStyle.BorderRightWidth = 1;
            this.RawBar.CanCustomize = false;
            this.RawBar.Cursor = System.Windows.Forms.Cursors.Default;
            this.RawBar.ExpandBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(189)))), ((int)(((byte)(203)))));
            this.RawBar.Expanded = true;
            this.RawBar.ExpandForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(115)))));
            this.RawBar.ExpandHotBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(195)))), ((int)(((byte)(208)))));
            this.RawBar.ExpandHotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.RawBar.Name = "RawBar";
            this.RawBar.Text = "Raw Data";
            // 
            // 
            // 
            this.RawBar.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.RawBar.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.RawBar.TitleHotStyle.CornerDiameter = 3;
            this.RawBar.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.RawBar.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.RawBar.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.RawBar.TitleStyle.BackColor = System.Drawing.Color.White;
            this.RawBar.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.RawBar.TitleStyle.CornerDiameter = 3;
            this.RawBar.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.RawBar.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.RawBar.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // explorerBarGroupItem13
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem13.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem13.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem13.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem13.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem13.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem13.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem13.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem13.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem13.CanCustomize = false;
            this.explorerBarGroupItem13.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem13.ExpandBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(189)))), ((int)(((byte)(203)))));
            this.explorerBarGroupItem13.Expanded = true;
            this.explorerBarGroupItem13.ExpandForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(68)))), ((int)(((byte)(115)))));
            this.explorerBarGroupItem13.ExpandHotBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(195)))), ((int)(((byte)(208)))));
            this.explorerBarGroupItem13.ExpandHotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.explorerBarGroupItem13.Name = "explorerBarGroupItem13";
            this.explorerBarGroupItem13.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cmdMetaExtract,
            this.cmdMetaInject,
            this.cmdMetaRemove,
            this.cmdMetaDuplicate});
            this.explorerBarGroupItem13.Text = "Tag Data";
            // 
            // 
            // 
            this.explorerBarGroupItem13.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem13.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem13.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem13.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem13.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem13.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem13.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem13.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem13.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem13.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem13.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem13.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // cmdMetaExtract
            // 
            this.cmdMetaExtract.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdMetaExtract.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMetaExtract.Enabled = false;
            this.cmdMetaExtract.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.cmdMetaExtract.HotFontUnderline = true;
            this.cmdMetaExtract.HotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.cmdMetaExtract.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None;
            this.cmdMetaExtract.Image = global::Ceiling_Cat.Properties.Resources.UNDO_16;
            this.cmdMetaExtract.Name = "cmdMetaExtract";
            this.cmdMetaExtract.Text = "Extract";
            this.cmdMetaExtract.Click += new System.EventHandler(this.cmdMetaExtract_Click);
            // 
            // cmdMetaInject
            // 
            this.cmdMetaInject.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdMetaInject.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMetaInject.Enabled = false;
            this.cmdMetaInject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.cmdMetaInject.HotFontUnderline = true;
            this.cmdMetaInject.HotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.cmdMetaInject.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None;
            this.cmdMetaInject.Image = global::Ceiling_Cat.Properties.Resources.REDO_16;
            this.cmdMetaInject.Name = "cmdMetaInject";
            this.cmdMetaInject.Text = "Inject";
            this.cmdMetaInject.Click += new System.EventHandler(this.cmdMetaInject_Click);
            // 
            // cmdMetaRemove
            // 
            this.cmdMetaRemove.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdMetaRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMetaRemove.Enabled = false;
            this.cmdMetaRemove.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.cmdMetaRemove.HotFontUnderline = true;
            this.cmdMetaRemove.HotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.cmdMetaRemove.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None;
            this.cmdMetaRemove.Image = global::Ceiling_Cat.Properties.Resources.CUT_16;
            this.cmdMetaRemove.Name = "cmdMetaRemove";
            this.cmdMetaRemove.Text = "Remove";
            this.cmdMetaRemove.Visible = false;
            this.cmdMetaRemove.Click += new System.EventHandler(this.cmdMetaRemove_Click);
            // 
            // cmdMetaDuplicate
            // 
            this.cmdMetaDuplicate.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdMetaDuplicate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMetaDuplicate.Enabled = false;
            this.cmdMetaDuplicate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.cmdMetaDuplicate.HotFontUnderline = true;
            this.cmdMetaDuplicate.HotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.cmdMetaDuplicate.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None;
            this.cmdMetaDuplicate.Image = global::Ceiling_Cat.Properties.Resources.COPY_16;
            this.cmdMetaDuplicate.Name = "cmdMetaDuplicate";
            this.cmdMetaDuplicate.Text = "Duplicate";
            this.cmdMetaDuplicate.Click += new System.EventHandler(this.cmdMetaDuplicate_Click);
            // 
            // Tags
            // 
            this.Tags.AttachedControl = this.TagsPanel;
            this.Tags.Name = "Tags";
            this.Tags.Text = "Tags";
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.AutoScroll = true;
            this.HeaderPanel.Controls.Add(this.itemPanel2);
            this.HeaderPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HeaderPanel.Location = new System.Drawing.Point(0, 22);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Padding = new System.Windows.Forms.Padding(1);
            this.HeaderPanel.Size = new System.Drawing.Size(1059, 675);
            this.HeaderPanel.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.HeaderPanel.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.HeaderPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.HeaderPanel.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.HeaderPanel.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.HeaderPanel.Style.GradientAngle = 90;
            this.HeaderPanel.TabIndex = 1;
            this.HeaderPanel.TabItem = this.Header;
            // 
            // itemPanel2
            // 
            this.itemPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.itemPanel2.AntiAlias = false;
            // 
            // 
            // 
            this.itemPanel2.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.itemPanel2.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel2.BackgroundStyle.BorderBottomWidth = 1;
            this.itemPanel2.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.itemPanel2.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel2.BackgroundStyle.BorderLeftWidth = 1;
            this.itemPanel2.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel2.BackgroundStyle.BorderRightWidth = 1;
            this.itemPanel2.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel2.BackgroundStyle.BorderTopWidth = 1;
            this.itemPanel2.BackgroundStyle.PaddingBottom = 1;
            this.itemPanel2.BackgroundStyle.PaddingLeft = 1;
            this.itemPanel2.BackgroundStyle.PaddingRight = 1;
            this.itemPanel2.BackgroundStyle.PaddingTop = 1;
            this.itemPanel2.CausesValidation = false;
            this.itemPanel2.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblHead,
            this.lblHead2,
            this.txtHead,
            this.labelItem17,
            this.txtVersion,
            this.labelItem18,
            this.txtFileSize,
            this.labelItem19,
            this.txtIndexOffset,
            this.labelItem20,
            this.txtIndexSize,
            this.labelItem21,
            this.txtMetaStart,
            this.labelItem22,
            this.txtMetaSize,
            this.labelItem23,
            this.txtNonRawSize,
            this.labelItem24,
            this.txtOrigin,
            this.labelItem25,
            this.txtBuildDate,
            this.labelItem26,
            this.cboMapType,
            this.labelItem27,
            this.txtStrangeFileStringsSize,
            this.labelItem30,
            this.txtStrangeFileStringsOffset,
            this.labelItem31,
            this.txtStringIDs128Offset,
            this.labelItem32,
            this.txtStringIDCount,
            this.labelItem33,
            this.txtStringIDsSize,
            this.labelItem34,
            this.txtStringIDsIndex,
            this.labelItem35,
            this.txtStringIDsOffset,
            this.labelItem36,
            this.txtInternalName,
            this.labelItem37,
            this.txtScenarioName,
            this.labelItem38,
            this.txtFileTableCount,
            this.labelItem39,
            this.txtFileTableOffset,
            this.labelItem40,
            this.txtFileTableSize,
            this.labelItem41,
            this.txtFileTableIndexOffset,
            this.labelItem42,
            this.txtChecksum,
            this.labelItem43,
            this.txtFoot,
            this.cmdHeaderSave});
            this.itemPanel2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemPanel2.Location = new System.Drawing.Point(1, -14);
            this.itemPanel2.Name = "itemPanel2";
            this.itemPanel2.Size = new System.Drawing.Size(1058, 950);
            this.itemPanel2.TabIndex = 1;
            // 
            // lblHead
            // 
            this.lblHead.BeginGroup = true;
            this.lblHead.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.lblHead.Name = "lblHead";
            this.lblHead.Text = "Head";
            // 
            // lblHead2
            // 
            this.lblHead2.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.lblHead2.Name = "lblHead2";
            this.lblHead2.Text = "Head";
            // 
            // txtHead
            // 
            this.txtHead.AlwaysShowCaption = false;
            this.txtHead.ControlText = "";
            this.txtHead.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtHead.Name = "txtHead";
            this.txtHead.RecentlyUsed = false;
            this.txtHead.SelectedText = "";
            this.txtHead.SelectionLength = 0;
            this.txtHead.SelectionStart = 0;
            this.txtHead.TextBoxWidth = 64;
            // 
            // labelItem17
            // 
            this.labelItem17.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem17.Name = "labelItem17";
            this.labelItem17.Text = "Version";
            // 
            // txtVersion
            // 
            this.txtVersion.AlwaysShowCaption = false;
            this.txtVersion.ControlText = "";
            this.txtVersion.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.RecentlyUsed = false;
            this.txtVersion.SelectedText = "";
            this.txtVersion.SelectionLength = 0;
            this.txtVersion.SelectionStart = 0;
            this.txtVersion.TextBoxWidth = 64;
            // 
            // labelItem18
            // 
            this.labelItem18.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem18.Name = "labelItem18";
            this.labelItem18.Text = "Filesize";
            // 
            // txtFileSize
            // 
            this.txtFileSize.AlwaysShowCaption = false;
            this.txtFileSize.ControlText = "";
            this.txtFileSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFileSize.Name = "txtFileSize";
            this.txtFileSize.RecentlyUsed = false;
            this.txtFileSize.SelectedText = "";
            this.txtFileSize.SelectionLength = 0;
            this.txtFileSize.SelectionStart = 0;
            this.txtFileSize.TextBoxWidth = 64;
            // 
            // labelItem19
            // 
            this.labelItem19.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem19.Name = "labelItem19";
            this.labelItem19.Text = "Index Offset";
            // 
            // txtIndexOffset
            // 
            this.txtIndexOffset.AlwaysShowCaption = false;
            this.txtIndexOffset.ControlText = "";
            this.txtIndexOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtIndexOffset.Name = "txtIndexOffset";
            this.txtIndexOffset.RecentlyUsed = false;
            this.txtIndexOffset.SelectedText = "";
            this.txtIndexOffset.SelectionLength = 0;
            this.txtIndexOffset.SelectionStart = 0;
            this.txtIndexOffset.TextBoxWidth = 64;
            // 
            // labelItem20
            // 
            this.labelItem20.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem20.Name = "labelItem20";
            this.labelItem20.Text = "Index Size";
            // 
            // txtIndexSize
            // 
            this.txtIndexSize.AlwaysShowCaption = false;
            this.txtIndexSize.ControlText = "";
            this.txtIndexSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtIndexSize.Name = "txtIndexSize";
            this.txtIndexSize.RecentlyUsed = false;
            this.txtIndexSize.SelectedText = "";
            this.txtIndexSize.SelectionLength = 0;
            this.txtIndexSize.SelectionStart = 0;
            this.txtIndexSize.TextBoxWidth = 64;
            // 
            // labelItem21
            // 
            this.labelItem21.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem21.Name = "labelItem21";
            this.labelItem21.Text = "Meta Start";
            // 
            // txtMetaStart
            // 
            this.txtMetaStart.AlwaysShowCaption = false;
            this.txtMetaStart.ControlText = "";
            this.txtMetaStart.Enabled = false;
            this.txtMetaStart.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtMetaStart.Name = "txtMetaStart";
            this.txtMetaStart.RecentlyUsed = false;
            this.txtMetaStart.SelectedText = "";
            this.txtMetaStart.SelectionLength = 0;
            this.txtMetaStart.SelectionStart = 0;
            this.txtMetaStart.TextBoxWidth = 64;
            // 
            // labelItem22
            // 
            this.labelItem22.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem22.Name = "labelItem22";
            this.labelItem22.Text = "Meta Size";
            // 
            // txtMetaSize
            // 
            this.txtMetaSize.AlwaysShowCaption = false;
            this.txtMetaSize.ControlText = "";
            this.txtMetaSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtMetaSize.Name = "txtMetaSize";
            this.txtMetaSize.RecentlyUsed = false;
            this.txtMetaSize.SelectedText = "";
            this.txtMetaSize.SelectionLength = 0;
            this.txtMetaSize.SelectionStart = 0;
            this.txtMetaSize.TextBoxWidth = 64;
            // 
            // labelItem23
            // 
            this.labelItem23.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem23.Name = "labelItem23";
            this.labelItem23.Text = "Non Raw Size";
            // 
            // txtNonRawSize
            // 
            this.txtNonRawSize.AlwaysShowCaption = false;
            this.txtNonRawSize.ControlText = "";
            this.txtNonRawSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtNonRawSize.Name = "txtNonRawSize";
            this.txtNonRawSize.RecentlyUsed = false;
            this.txtNonRawSize.SelectedText = "";
            this.txtNonRawSize.SelectionLength = 0;
            this.txtNonRawSize.SelectionStart = 0;
            this.txtNonRawSize.TextBoxWidth = 64;
            // 
            // labelItem24
            // 
            this.labelItem24.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem24.Name = "labelItem24";
            this.labelItem24.Text = "Origin";
            // 
            // txtOrigin
            // 
            this.txtOrigin.AlwaysShowCaption = false;
            this.txtOrigin.ControlText = "";
            this.txtOrigin.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtOrigin.Name = "txtOrigin";
            this.txtOrigin.RecentlyUsed = false;
            this.txtOrigin.SelectedText = "";
            this.txtOrigin.SelectionLength = 0;
            this.txtOrigin.SelectionStart = 0;
            this.txtOrigin.TextBoxWidth = 64;
            // 
            // labelItem25
            // 
            this.labelItem25.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem25.Name = "labelItem25";
            this.labelItem25.Text = "Build Date";
            // 
            // txtBuildDate
            // 
            this.txtBuildDate.AlwaysShowCaption = false;
            this.txtBuildDate.ControlText = "";
            this.txtBuildDate.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtBuildDate.Name = "txtBuildDate";
            this.txtBuildDate.RecentlyUsed = false;
            this.txtBuildDate.SelectedText = "";
            this.txtBuildDate.SelectionLength = 0;
            this.txtBuildDate.SelectionStart = 0;
            this.txtBuildDate.TextBoxWidth = 64;
            // 
            // labelItem26
            // 
            this.labelItem26.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem26.Name = "labelItem26";
            this.labelItem26.Text = "Map Type";
            // 
            // cboMapType
            // 
            this.cboMapType.ItemHeight = 12;
            this.cboMapType.Items.AddRange(new object[] {
            this.cboItemSinglePlayerMap,
            this.cboItemMultiplayerMap,
            this.cboItemMainMenu,
            this.cboItemShared,
            this.cboItemSinglePlayerShared});
            this.cboMapType.Name = "cboMapType";
            // 
            // cboItemSinglePlayerMap
            // 
            this.cboItemSinglePlayerMap.Text = "Single Player Map";
            // 
            // cboItemMultiplayerMap
            // 
            this.cboItemMultiplayerMap.Text = "Multiplayer Map";
            // 
            // cboItemMainMenu
            // 
            this.cboItemMainMenu.Text = "MainMenu";
            // 
            // cboItemShared
            // 
            this.cboItemShared.Text = "Shared";
            // 
            // cboItemSinglePlayerShared
            // 
            this.cboItemSinglePlayerShared.Text = "Single Player Shared";
            // 
            // labelItem27
            // 
            this.labelItem27.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem27.Name = "labelItem27";
            this.labelItem27.Text = "Strange File Strings Size";
            // 
            // txtStrangeFileStringsSize
            // 
            this.txtStrangeFileStringsSize.AlwaysShowCaption = false;
            this.txtStrangeFileStringsSize.ControlText = "";
            this.txtStrangeFileStringsSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStrangeFileStringsSize.Name = "txtStrangeFileStringsSize";
            this.txtStrangeFileStringsSize.RecentlyUsed = false;
            this.txtStrangeFileStringsSize.SelectedText = "";
            this.txtStrangeFileStringsSize.SelectionLength = 0;
            this.txtStrangeFileStringsSize.SelectionStart = 0;
            this.txtStrangeFileStringsSize.TextBoxWidth = 64;
            // 
            // labelItem30
            // 
            this.labelItem30.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem30.Name = "labelItem30";
            this.labelItem30.Text = "Strange File Strings Offset";
            // 
            // txtStrangeFileStringsOffset
            // 
            this.txtStrangeFileStringsOffset.AlwaysShowCaption = false;
            this.txtStrangeFileStringsOffset.ControlText = "";
            this.txtStrangeFileStringsOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStrangeFileStringsOffset.Name = "txtStrangeFileStringsOffset";
            this.txtStrangeFileStringsOffset.RecentlyUsed = false;
            this.txtStrangeFileStringsOffset.SelectedText = "";
            this.txtStrangeFileStringsOffset.SelectionLength = 0;
            this.txtStrangeFileStringsOffset.SelectionStart = 0;
            this.txtStrangeFileStringsOffset.TextBoxWidth = 64;
            // 
            // labelItem31
            // 
            this.labelItem31.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem31.Name = "labelItem31";
            this.labelItem31.Text = "StringIDs 128 Offset";
            // 
            // txtStringIDs128Offset
            // 
            this.txtStringIDs128Offset.AlwaysShowCaption = false;
            this.txtStringIDs128Offset.ControlText = "";
            this.txtStringIDs128Offset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStringIDs128Offset.Name = "txtStringIDs128Offset";
            this.txtStringIDs128Offset.RecentlyUsed = false;
            this.txtStringIDs128Offset.SelectedText = "";
            this.txtStringIDs128Offset.SelectionLength = 0;
            this.txtStringIDs128Offset.SelectionStart = 0;
            this.txtStringIDs128Offset.TextBoxWidth = 64;
            // 
            // labelItem32
            // 
            this.labelItem32.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem32.Name = "labelItem32";
            this.labelItem32.Text = "StringID Count";
            // 
            // txtStringIDCount
            // 
            this.txtStringIDCount.AlwaysShowCaption = false;
            this.txtStringIDCount.ControlText = "";
            this.txtStringIDCount.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStringIDCount.Name = "txtStringIDCount";
            this.txtStringIDCount.RecentlyUsed = false;
            this.txtStringIDCount.SelectedText = "";
            this.txtStringIDCount.SelectionLength = 0;
            this.txtStringIDCount.SelectionStart = 0;
            this.txtStringIDCount.TextBoxWidth = 64;
            // 
            // labelItem33
            // 
            this.labelItem33.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem33.Name = "labelItem33";
            this.labelItem33.Text = "StringIDs Size";
            // 
            // txtStringIDsSize
            // 
            this.txtStringIDsSize.AlwaysShowCaption = false;
            this.txtStringIDsSize.ControlText = "";
            this.txtStringIDsSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStringIDsSize.Name = "txtStringIDsSize";
            this.txtStringIDsSize.RecentlyUsed = false;
            this.txtStringIDsSize.SelectedText = "";
            this.txtStringIDsSize.SelectionLength = 0;
            this.txtStringIDsSize.SelectionStart = 0;
            this.txtStringIDsSize.TextBoxWidth = 64;
            // 
            // labelItem34
            // 
            this.labelItem34.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem34.Name = "labelItem34";
            this.labelItem34.Text = "StringIDs Index";
            // 
            // txtStringIDsIndex
            // 
            this.txtStringIDsIndex.AlwaysShowCaption = false;
            this.txtStringIDsIndex.ControlText = "";
            this.txtStringIDsIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStringIDsIndex.Name = "txtStringIDsIndex";
            this.txtStringIDsIndex.RecentlyUsed = false;
            this.txtStringIDsIndex.SelectedText = "";
            this.txtStringIDsIndex.SelectionLength = 0;
            this.txtStringIDsIndex.SelectionStart = 0;
            this.txtStringIDsIndex.TextBoxWidth = 64;
            // 
            // labelItem35
            // 
            this.labelItem35.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem35.Name = "labelItem35";
            this.labelItem35.Text = "StringIDs Offset";
            // 
            // txtStringIDsOffset
            // 
            this.txtStringIDsOffset.AlwaysShowCaption = false;
            this.txtStringIDsOffset.ControlText = "";
            this.txtStringIDsOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtStringIDsOffset.Name = "txtStringIDsOffset";
            this.txtStringIDsOffset.RecentlyUsed = false;
            this.txtStringIDsOffset.SelectedText = "";
            this.txtStringIDsOffset.SelectionLength = 0;
            this.txtStringIDsOffset.SelectionStart = 0;
            this.txtStringIDsOffset.TextBoxWidth = 64;
            // 
            // labelItem36
            // 
            this.labelItem36.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem36.Name = "labelItem36";
            this.labelItem36.Text = "Internal Name";
            // 
            // txtInternalName
            // 
            this.txtInternalName.AlwaysShowCaption = false;
            this.txtInternalName.ControlText = "";
            this.txtInternalName.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtInternalName.Name = "txtInternalName";
            this.txtInternalName.RecentlyUsed = false;
            this.txtInternalName.SelectedText = "";
            this.txtInternalName.SelectionLength = 0;
            this.txtInternalName.SelectionStart = 0;
            this.txtInternalName.TextBoxWidth = 64;
            // 
            // labelItem37
            // 
            this.labelItem37.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem37.Name = "labelItem37";
            this.labelItem37.Text = "Scenario Name";
            // 
            // txtScenarioName
            // 
            this.txtScenarioName.AlwaysShowCaption = false;
            this.txtScenarioName.ControlText = "";
            this.txtScenarioName.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtScenarioName.Name = "txtScenarioName";
            this.txtScenarioName.RecentlyUsed = false;
            this.txtScenarioName.SelectedText = "";
            this.txtScenarioName.SelectionLength = 0;
            this.txtScenarioName.SelectionStart = 0;
            this.txtScenarioName.TextBoxWidth = 64;
            // 
            // labelItem38
            // 
            this.labelItem38.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem38.Name = "labelItem38";
            this.labelItem38.Text = "File Table Count";
            // 
            // txtFileTableCount
            // 
            this.txtFileTableCount.AlwaysShowCaption = false;
            this.txtFileTableCount.ControlText = "";
            this.txtFileTableCount.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFileTableCount.Name = "txtFileTableCount";
            this.txtFileTableCount.RecentlyUsed = false;
            this.txtFileTableCount.SelectedText = "";
            this.txtFileTableCount.SelectionLength = 0;
            this.txtFileTableCount.SelectionStart = 0;
            this.txtFileTableCount.TextBoxWidth = 64;
            // 
            // labelItem39
            // 
            this.labelItem39.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem39.Name = "labelItem39";
            this.labelItem39.Text = "File Table Offset";
            // 
            // txtFileTableOffset
            // 
            this.txtFileTableOffset.AlwaysShowCaption = false;
            this.txtFileTableOffset.ControlText = "";
            this.txtFileTableOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFileTableOffset.Name = "txtFileTableOffset";
            this.txtFileTableOffset.RecentlyUsed = false;
            this.txtFileTableOffset.SelectedText = "";
            this.txtFileTableOffset.SelectionLength = 0;
            this.txtFileTableOffset.SelectionStart = 0;
            this.txtFileTableOffset.TextBoxWidth = 64;
            // 
            // labelItem40
            // 
            this.labelItem40.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem40.Name = "labelItem40";
            this.labelItem40.Text = "File Table Size";
            // 
            // txtFileTableSize
            // 
            this.txtFileTableSize.AlwaysShowCaption = false;
            this.txtFileTableSize.ControlText = "";
            this.txtFileTableSize.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFileTableSize.Name = "txtFileTableSize";
            this.txtFileTableSize.RecentlyUsed = false;
            this.txtFileTableSize.SelectedText = "";
            this.txtFileTableSize.SelectionLength = 0;
            this.txtFileTableSize.SelectionStart = 0;
            this.txtFileTableSize.TextBoxWidth = 64;
            // 
            // labelItem41
            // 
            this.labelItem41.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem41.Name = "labelItem41";
            this.labelItem41.Text = "File Table Index Offset";
            // 
            // txtFileTableIndexOffset
            // 
            this.txtFileTableIndexOffset.AlwaysShowCaption = false;
            this.txtFileTableIndexOffset.ControlText = "";
            this.txtFileTableIndexOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFileTableIndexOffset.Name = "txtFileTableIndexOffset";
            this.txtFileTableIndexOffset.RecentlyUsed = false;
            this.txtFileTableIndexOffset.SelectedText = "";
            this.txtFileTableIndexOffset.SelectionLength = 0;
            this.txtFileTableIndexOffset.SelectionStart = 0;
            this.txtFileTableIndexOffset.TextBoxWidth = 64;
            // 
            // labelItem42
            // 
            this.labelItem42.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem42.Name = "labelItem42";
            this.labelItem42.Text = "Checksum";
            // 
            // txtChecksum
            // 
            this.txtChecksum.AlwaysShowCaption = false;
            this.txtChecksum.ControlText = "";
            this.txtChecksum.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtChecksum.Name = "txtChecksum";
            this.txtChecksum.RecentlyUsed = false;
            this.txtChecksum.SelectedText = "";
            this.txtChecksum.SelectionLength = 0;
            this.txtChecksum.SelectionStart = 0;
            this.txtChecksum.TextBoxWidth = 64;
            // 
            // labelItem43
            // 
            this.labelItem43.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem43.Name = "labelItem43";
            this.labelItem43.Text = "Foot";
            // 
            // txtFoot
            // 
            this.txtFoot.AlwaysShowCaption = false;
            this.txtFoot.ControlText = "";
            this.txtFoot.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFoot.Name = "txtFoot";
            this.txtFoot.RecentlyUsed = false;
            this.txtFoot.SelectedText = "";
            this.txtFoot.SelectionLength = 0;
            this.txtFoot.SelectionStart = 0;
            this.txtFoot.TextBoxWidth = 64;
            // 
            // cmdHeaderSave
            // 
            this.cmdHeaderSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdHeaderSave.Image = global::Ceiling_Cat.Properties.Resources.save_16;
            this.cmdHeaderSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.cmdHeaderSave.Name = "cmdHeaderSave";
            this.cmdHeaderSave.Text = "Save";
            this.cmdHeaderSave.Click += new System.EventHandler(this.cmdHeaderSave_Click);
            // 
            // Header
            // 
            this.Header.AttachedControl = this.HeaderPanel;
            this.Header.Name = "Header";
            this.Header.Text = "Header";
            // 
            // UnicodePanel
            // 
            this.UnicodePanel.Controls.Add(this.UnicodeLanTabs);
            this.UnicodePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnicodePanel.Location = new System.Drawing.Point(0, 22);
            this.UnicodePanel.Name = "UnicodePanel";
            this.UnicodePanel.Padding = new System.Windows.Forms.Padding(1);
            this.UnicodePanel.Size = new System.Drawing.Size(1059, 657);
            this.UnicodePanel.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.UnicodePanel.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.UnicodePanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.UnicodePanel.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.UnicodePanel.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.UnicodePanel.Style.GradientAngle = 90;
            this.UnicodePanel.TabIndex = 4;
            this.UnicodePanel.TabItem = this.Unicode;
            // 
            // UnicodeLanTabs
            // 
            this.UnicodeLanTabs.CanReorderTabs = true;
            this.UnicodeLanTabs.CloseButtonOnTabsAlwaysDisplayed = false;
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel1);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel5);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel9);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel8);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel7);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel6);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel4);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel3);
            this.UnicodeLanTabs.Controls.Add(this.tabControlPanel2);
            this.UnicodeLanTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UnicodeLanTabs.Location = new System.Drawing.Point(1, 1);
            this.UnicodeLanTabs.Name = "UnicodeLanTabs";
            this.UnicodeLanTabs.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.UnicodeLanTabs.SelectedTabIndex = 0;
            this.UnicodeLanTabs.Size = new System.Drawing.Size(1057, 655);
            this.UnicodeLanTabs.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Dock;
            this.UnicodeLanTabs.TabIndex = 1;
            this.UnicodeLanTabs.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.UnicodeLanTabs.Tabs.Add(this.ULEnglish);
            this.UnicodeLanTabs.Tabs.Add(this.ULJapanese);
            this.UnicodeLanTabs.Tabs.Add(this.ULDutch);
            this.UnicodeLanTabs.Tabs.Add(this.ULFrench);
            this.UnicodeLanTabs.Tabs.Add(this.ULSpanish);
            this.UnicodeLanTabs.Tabs.Add(this.ULItalian);
            this.UnicodeLanTabs.Tabs.Add(this.ULKorean);
            this.UnicodeLanTabs.Tabs.Add(this.ULChinese);
            this.UnicodeLanTabs.Tabs.Add(this.ULPortuguese);
            this.UnicodeLanTabs.Text = "tabControl1";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.lstEnglish);
            this.tabControlPanel1.Controls.Add(this.expandableSplitter3);
            this.tabControlPanel1.Controls.Add(this.explorerBar3);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.ULEnglish;
            // 
            // lstEnglish
            // 
            this.lstEnglish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstEnglish.FormattingEnabled = true;
            this.lstEnglish.Location = new System.Drawing.Point(1, 1);
            this.lstEnglish.Name = "lstEnglish";
            this.lstEnglish.Size = new System.Drawing.Size(840, 628);
            this.lstEnglish.TabIndex = 9;
            this.lstEnglish.SelectedIndexChanged += new System.EventHandler(this.lstEnglish_SelectedIndexChanged);
            // 
            // expandableSplitter3
            // 
            this.expandableSplitter3.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter3.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter3.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter3.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter3.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter3.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter3.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter3.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter3.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter3.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter3.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter3.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter3.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter3.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter3.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter3.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter3.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter3.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter3.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter3.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter3.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter3.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter3.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter3.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter3.Name = "expandableSplitter3";
            this.expandableSplitter3.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter3.TabIndex = 8;
            this.expandableSplitter3.TabStop = false;
            // 
            // explorerBar3
            // 
            this.explorerBar3.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar3.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar3.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar3.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar3.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar3.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar3.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar3.GroupImages = null;
            this.explorerBar3.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem4});
            this.explorerBar3.Images = null;
            this.explorerBar3.Location = new System.Drawing.Point(851, 1);
            this.explorerBar3.Name = "explorerBar3";
            this.explorerBar3.Size = new System.Drawing.Size(205, 631);
            this.explorerBar3.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar3.TabIndex = 7;
            this.explorerBar3.ThemeAware = true;
            // 
            // explorerBarGroupItem4
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem4.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem4.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem4.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem4.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem4.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem4.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem4.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem4.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem4.CanCustomize = false;
            this.explorerBarGroupItem4.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem4.Expanded = true;
            this.explorerBarGroupItem4.Name = "explorerBarGroupItem4";
            this.explorerBarGroupItem4.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem4.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem5,
            this.txtEnglishIndex});
            this.explorerBarGroupItem4.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem4.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem4.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem4.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem4.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem4.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem4.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem4.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem4.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem4.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem4.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem4.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem4.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem5
            // 
            this.labelItem5.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem5.Name = "labelItem5";
            this.labelItem5.Text = "Index";
            this.labelItem5.ThemeAware = true;
            // 
            // txtEnglishIndex
            // 
            this.txtEnglishIndex.AlwaysShowCaption = false;
            this.txtEnglishIndex.ControlText = "";
            this.txtEnglishIndex.Enabled = false;
            this.txtEnglishIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtEnglishIndex.Name = "txtEnglishIndex";
            this.txtEnglishIndex.RecentlyUsed = false;
            this.txtEnglishIndex.SelectedText = "";
            this.txtEnglishIndex.SelectionLength = 0;
            this.txtEnglishIndex.SelectionStart = 0;
            this.txtEnglishIndex.TextBoxWidth = 64;
            this.txtEnglishIndex.ThemeAware = true;
            // 
            // ULEnglish
            // 
            this.ULEnglish.AttachedControl = this.tabControlPanel1;
            this.ULEnglish.Name = "ULEnglish";
            this.ULEnglish.Text = "English";
            // 
            // tabControlPanel5
            // 
            this.tabControlPanel5.Controls.Add(this.lstSpanish);
            this.tabControlPanel5.Controls.Add(this.expandableSplitter7);
            this.tabControlPanel5.Controls.Add(this.explorerBar7);
            this.tabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel5.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel5.Name = "tabControlPanel5";
            this.tabControlPanel5.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel5.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel5.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel5.Style.GradientAngle = 90;
            this.tabControlPanel5.TabIndex = 5;
            this.tabControlPanel5.TabItem = this.ULSpanish;
            // 
            // lstSpanish
            // 
            this.lstSpanish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSpanish.FormattingEnabled = true;
            this.lstSpanish.Location = new System.Drawing.Point(1, 1);
            this.lstSpanish.Name = "lstSpanish";
            this.lstSpanish.Size = new System.Drawing.Size(840, 628);
            this.lstSpanish.TabIndex = 9;
            this.lstSpanish.SelectedIndexChanged += new System.EventHandler(this.lstSpanish_SelectedIndexChanged);
            // 
            // expandableSplitter7
            // 
            this.expandableSplitter7.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter7.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter7.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter7.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter7.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter7.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter7.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter7.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter7.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter7.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter7.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter7.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter7.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter7.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter7.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter7.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter7.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter7.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter7.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter7.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter7.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter7.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter7.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter7.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter7.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter7.Name = "expandableSplitter7";
            this.expandableSplitter7.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter7.TabIndex = 8;
            this.expandableSplitter7.TabStop = false;
            // 
            // explorerBar7
            // 
            this.explorerBar7.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar7.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar7.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar7.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar7.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar7.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar7.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar7.GroupImages = null;
            this.explorerBar7.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem8});
            this.explorerBar7.Images = null;
            this.explorerBar7.Location = new System.Drawing.Point(851, 1);
            this.explorerBar7.Name = "explorerBar7";
            this.explorerBar7.Size = new System.Drawing.Size(205, 631);
            this.explorerBar7.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar7.TabIndex = 7;
            this.explorerBar7.ThemeAware = true;
            // 
            // explorerBarGroupItem8
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem8.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem8.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem8.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem8.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem8.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem8.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem8.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem8.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem8.CanCustomize = false;
            this.explorerBarGroupItem8.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem8.Expanded = true;
            this.explorerBarGroupItem8.Name = "explorerBarGroupItem8";
            this.explorerBarGroupItem8.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem8.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem48,
            this.txtSpanishIndex});
            this.explorerBarGroupItem8.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem8.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem8.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem8.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem8.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem8.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem8.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem8.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem8.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem8.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem8.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem8.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem8.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem48
            // 
            this.labelItem48.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem48.Name = "labelItem48";
            this.labelItem48.Text = "Index";
            this.labelItem48.ThemeAware = true;
            // 
            // txtSpanishIndex
            // 
            this.txtSpanishIndex.AlwaysShowCaption = false;
            this.txtSpanishIndex.ControlText = "";
            this.txtSpanishIndex.Enabled = false;
            this.txtSpanishIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtSpanishIndex.Name = "txtSpanishIndex";
            this.txtSpanishIndex.RecentlyUsed = false;
            this.txtSpanishIndex.SelectedText = "";
            this.txtSpanishIndex.SelectionLength = 0;
            this.txtSpanishIndex.SelectionStart = 0;
            this.txtSpanishIndex.TextBoxWidth = 64;
            this.txtSpanishIndex.ThemeAware = true;
            // 
            // ULSpanish
            // 
            this.ULSpanish.AttachedControl = this.tabControlPanel5;
            this.ULSpanish.Name = "ULSpanish";
            this.ULSpanish.Text = "Spanish";
            // 
            // tabControlPanel9
            // 
            this.tabControlPanel9.Controls.Add(this.lstPortuguese);
            this.tabControlPanel9.Controls.Add(this.expandableSplitter11);
            this.tabControlPanel9.Controls.Add(this.explorerBar11);
            this.tabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel9.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel9.Name = "tabControlPanel9";
            this.tabControlPanel9.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel9.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel9.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel9.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel9.Style.GradientAngle = 90;
            this.tabControlPanel9.TabIndex = 9;
            this.tabControlPanel9.TabItem = this.ULPortuguese;
            // 
            // lstPortuguese
            // 
            this.lstPortuguese.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstPortuguese.FormattingEnabled = true;
            this.lstPortuguese.Location = new System.Drawing.Point(1, 1);
            this.lstPortuguese.Name = "lstPortuguese";
            this.lstPortuguese.Size = new System.Drawing.Size(840, 628);
            this.lstPortuguese.TabIndex = 9;
            this.lstPortuguese.SelectedIndexChanged += new System.EventHandler(this.lstPortuguese_SelectedIndexChanged);
            // 
            // expandableSplitter11
            // 
            this.expandableSplitter11.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter11.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter11.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter11.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter11.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter11.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter11.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter11.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter11.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter11.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter11.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter11.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter11.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter11.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter11.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter11.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter11.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter11.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter11.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter11.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter11.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter11.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter11.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter11.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter11.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter11.Name = "expandableSplitter11";
            this.expandableSplitter11.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter11.TabIndex = 8;
            this.expandableSplitter11.TabStop = false;
            // 
            // explorerBar11
            // 
            this.explorerBar11.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar11.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar11.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar11.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar11.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar11.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar11.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar11.GroupImages = null;
            this.explorerBar11.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem12});
            this.explorerBar11.Images = null;
            this.explorerBar11.Location = new System.Drawing.Point(851, 1);
            this.explorerBar11.Name = "explorerBar11";
            this.explorerBar11.Size = new System.Drawing.Size(205, 631);
            this.explorerBar11.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar11.TabIndex = 7;
            this.explorerBar11.ThemeAware = true;
            // 
            // explorerBarGroupItem12
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem12.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem12.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem12.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem12.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem12.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem12.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem12.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem12.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem12.CanCustomize = false;
            this.explorerBarGroupItem12.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem12.Expanded = true;
            this.explorerBarGroupItem12.Name = "explorerBarGroupItem12";
            this.explorerBarGroupItem12.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem12.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem52,
            this.txtPortugeseIndex});
            this.explorerBarGroupItem12.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem12.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem12.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem12.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem12.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem12.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem12.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem12.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem12.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem12.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem12.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem12.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem12.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem52
            // 
            this.labelItem52.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem52.Name = "labelItem52";
            this.labelItem52.Text = "Index";
            this.labelItem52.ThemeAware = true;
            // 
            // txtPortugeseIndex
            // 
            this.txtPortugeseIndex.AlwaysShowCaption = false;
            this.txtPortugeseIndex.ControlText = "";
            this.txtPortugeseIndex.Enabled = false;
            this.txtPortugeseIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtPortugeseIndex.Name = "txtPortugeseIndex";
            this.txtPortugeseIndex.RecentlyUsed = false;
            this.txtPortugeseIndex.SelectedText = "";
            this.txtPortugeseIndex.SelectionLength = 0;
            this.txtPortugeseIndex.SelectionStart = 0;
            this.txtPortugeseIndex.TextBoxWidth = 64;
            this.txtPortugeseIndex.ThemeAware = true;
            // 
            // ULPortuguese
            // 
            this.ULPortuguese.AttachedControl = this.tabControlPanel9;
            this.ULPortuguese.Name = "ULPortuguese";
            this.ULPortuguese.Text = "Portuguese";
            // 
            // tabControlPanel8
            // 
            this.tabControlPanel8.Controls.Add(this.lstChinese);
            this.tabControlPanel8.Controls.Add(this.expandableSplitter10);
            this.tabControlPanel8.Controls.Add(this.explorerBar10);
            this.tabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel8.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel8.Name = "tabControlPanel8";
            this.tabControlPanel8.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel8.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel8.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel8.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel8.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel8.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel8.Style.GradientAngle = 90;
            this.tabControlPanel8.TabIndex = 8;
            this.tabControlPanel8.TabItem = this.ULChinese;
            // 
            // lstChinese
            // 
            this.lstChinese.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstChinese.FormattingEnabled = true;
            this.lstChinese.Location = new System.Drawing.Point(1, 1);
            this.lstChinese.Name = "lstChinese";
            this.lstChinese.Size = new System.Drawing.Size(840, 628);
            this.lstChinese.TabIndex = 9;
            this.lstChinese.SelectedIndexChanged += new System.EventHandler(this.lstChinese_SelectedIndexChanged);
            // 
            // expandableSplitter10
            // 
            this.expandableSplitter10.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter10.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter10.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter10.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter10.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter10.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter10.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter10.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter10.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter10.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter10.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter10.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter10.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter10.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter10.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter10.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter10.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter10.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter10.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter10.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter10.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter10.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter10.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter10.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter10.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter10.Name = "expandableSplitter10";
            this.expandableSplitter10.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter10.TabIndex = 8;
            this.expandableSplitter10.TabStop = false;
            // 
            // explorerBar10
            // 
            this.explorerBar10.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar10.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar10.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar10.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar10.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar10.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar10.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar10.GroupImages = null;
            this.explorerBar10.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem11});
            this.explorerBar10.Images = null;
            this.explorerBar10.Location = new System.Drawing.Point(851, 1);
            this.explorerBar10.Name = "explorerBar10";
            this.explorerBar10.Size = new System.Drawing.Size(205, 631);
            this.explorerBar10.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar10.TabIndex = 7;
            this.explorerBar10.ThemeAware = true;
            // 
            // explorerBarGroupItem11
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem11.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem11.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem11.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem11.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem11.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem11.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem11.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem11.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem11.CanCustomize = false;
            this.explorerBarGroupItem11.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem11.Expanded = true;
            this.explorerBarGroupItem11.Name = "explorerBarGroupItem11";
            this.explorerBarGroupItem11.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem11.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem51,
            this.txtChineseIndex});
            this.explorerBarGroupItem11.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem11.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem11.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem11.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem11.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem11.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem11.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem11.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem11.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem11.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem11.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem11.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem11.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem51
            // 
            this.labelItem51.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem51.Name = "labelItem51";
            this.labelItem51.Text = "Index";
            this.labelItem51.ThemeAware = true;
            // 
            // txtChineseIndex
            // 
            this.txtChineseIndex.AlwaysShowCaption = false;
            this.txtChineseIndex.ControlText = "";
            this.txtChineseIndex.Enabled = false;
            this.txtChineseIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtChineseIndex.Name = "txtChineseIndex";
            this.txtChineseIndex.RecentlyUsed = false;
            this.txtChineseIndex.SelectedText = "";
            this.txtChineseIndex.SelectionLength = 0;
            this.txtChineseIndex.SelectionStart = 0;
            this.txtChineseIndex.TextBoxWidth = 64;
            this.txtChineseIndex.ThemeAware = true;
            // 
            // ULChinese
            // 
            this.ULChinese.AttachedControl = this.tabControlPanel8;
            this.ULChinese.Name = "ULChinese";
            this.ULChinese.Text = "Chinese";
            // 
            // tabControlPanel7
            // 
            this.tabControlPanel7.Controls.Add(this.lstKorean);
            this.tabControlPanel7.Controls.Add(this.expandableSplitter9);
            this.tabControlPanel7.Controls.Add(this.explorerBar9);
            this.tabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel7.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel7.Name = "tabControlPanel7";
            this.tabControlPanel7.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel7.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel7.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel7.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel7.Style.GradientAngle = 90;
            this.tabControlPanel7.TabIndex = 7;
            this.tabControlPanel7.TabItem = this.ULKorean;
            // 
            // lstKorean
            // 
            this.lstKorean.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstKorean.FormattingEnabled = true;
            this.lstKorean.Location = new System.Drawing.Point(1, 1);
            this.lstKorean.Name = "lstKorean";
            this.lstKorean.Size = new System.Drawing.Size(840, 628);
            this.lstKorean.TabIndex = 9;
            this.lstKorean.SelectedIndexChanged += new System.EventHandler(this.lstKorean_SelectedIndexChanged);
            // 
            // expandableSplitter9
            // 
            this.expandableSplitter9.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter9.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter9.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter9.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter9.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter9.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter9.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter9.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter9.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter9.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter9.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter9.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter9.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter9.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter9.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter9.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter9.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter9.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter9.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter9.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter9.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter9.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter9.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter9.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter9.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter9.Name = "expandableSplitter9";
            this.expandableSplitter9.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter9.TabIndex = 8;
            this.expandableSplitter9.TabStop = false;
            // 
            // explorerBar9
            // 
            this.explorerBar9.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar9.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar9.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar9.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar9.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar9.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar9.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar9.GroupImages = null;
            this.explorerBar9.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem10});
            this.explorerBar9.Images = null;
            this.explorerBar9.Location = new System.Drawing.Point(851, 1);
            this.explorerBar9.Name = "explorerBar9";
            this.explorerBar9.Size = new System.Drawing.Size(205, 631);
            this.explorerBar9.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar9.TabIndex = 7;
            this.explorerBar9.ThemeAware = true;
            // 
            // explorerBarGroupItem10
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem10.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem10.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem10.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem10.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem10.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem10.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem10.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem10.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem10.CanCustomize = false;
            this.explorerBarGroupItem10.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem10.Expanded = true;
            this.explorerBarGroupItem10.Name = "explorerBarGroupItem10";
            this.explorerBarGroupItem10.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem10.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem50,
            this.txtKoreanIndex});
            this.explorerBarGroupItem10.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem10.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem10.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem10.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem10.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem10.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem10.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem10.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem10.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem10.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem10.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem10.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem10.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem50
            // 
            this.labelItem50.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem50.Name = "labelItem50";
            this.labelItem50.Text = "Index";
            this.labelItem50.ThemeAware = true;
            // 
            // txtKoreanIndex
            // 
            this.txtKoreanIndex.AlwaysShowCaption = false;
            this.txtKoreanIndex.ControlText = "";
            this.txtKoreanIndex.Enabled = false;
            this.txtKoreanIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtKoreanIndex.Name = "txtKoreanIndex";
            this.txtKoreanIndex.RecentlyUsed = false;
            this.txtKoreanIndex.SelectedText = "";
            this.txtKoreanIndex.SelectionLength = 0;
            this.txtKoreanIndex.SelectionStart = 0;
            this.txtKoreanIndex.TextBoxWidth = 64;
            this.txtKoreanIndex.ThemeAware = true;
            // 
            // ULKorean
            // 
            this.ULKorean.AttachedControl = this.tabControlPanel7;
            this.ULKorean.Name = "ULKorean";
            this.ULKorean.Text = "Korean";
            // 
            // tabControlPanel6
            // 
            this.tabControlPanel6.Controls.Add(this.lstItalian);
            this.tabControlPanel6.Controls.Add(this.expandableSplitter8);
            this.tabControlPanel6.Controls.Add(this.explorerBar8);
            this.tabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel6.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel6.Name = "tabControlPanel6";
            this.tabControlPanel6.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel6.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel6.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel6.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel6.Style.GradientAngle = 90;
            this.tabControlPanel6.TabIndex = 6;
            this.tabControlPanel6.TabItem = this.ULItalian;
            // 
            // lstItalian
            // 
            this.lstItalian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstItalian.FormattingEnabled = true;
            this.lstItalian.Location = new System.Drawing.Point(1, 1);
            this.lstItalian.Name = "lstItalian";
            this.lstItalian.Size = new System.Drawing.Size(840, 628);
            this.lstItalian.TabIndex = 9;
            this.lstItalian.SelectedIndexChanged += new System.EventHandler(this.lstItalian_SelectedIndexChanged);
            // 
            // expandableSplitter8
            // 
            this.expandableSplitter8.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter8.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter8.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter8.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter8.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter8.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter8.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter8.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter8.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter8.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter8.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter8.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter8.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter8.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter8.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter8.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter8.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter8.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter8.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter8.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter8.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter8.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter8.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter8.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter8.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter8.Name = "expandableSplitter8";
            this.expandableSplitter8.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter8.TabIndex = 8;
            this.expandableSplitter8.TabStop = false;
            // 
            // explorerBar8
            // 
            this.explorerBar8.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar8.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar8.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar8.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar8.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar8.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar8.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar8.GroupImages = null;
            this.explorerBar8.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem9});
            this.explorerBar8.Images = null;
            this.explorerBar8.Location = new System.Drawing.Point(851, 1);
            this.explorerBar8.Name = "explorerBar8";
            this.explorerBar8.Size = new System.Drawing.Size(205, 631);
            this.explorerBar8.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar8.TabIndex = 7;
            this.explorerBar8.ThemeAware = true;
            // 
            // explorerBarGroupItem9
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem9.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem9.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem9.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem9.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem9.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem9.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem9.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem9.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem9.CanCustomize = false;
            this.explorerBarGroupItem9.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem9.Expanded = true;
            this.explorerBarGroupItem9.Name = "explorerBarGroupItem9";
            this.explorerBarGroupItem9.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem9.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem49,
            this.txtItalianIndex});
            this.explorerBarGroupItem9.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem9.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem9.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem9.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem9.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem9.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem9.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem9.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem9.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem9.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem9.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem9.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem9.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem49
            // 
            this.labelItem49.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem49.Name = "labelItem49";
            this.labelItem49.Text = "Index";
            this.labelItem49.ThemeAware = true;
            // 
            // txtItalianIndex
            // 
            this.txtItalianIndex.AlwaysShowCaption = false;
            this.txtItalianIndex.ControlText = "";
            this.txtItalianIndex.Enabled = false;
            this.txtItalianIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtItalianIndex.Name = "txtItalianIndex";
            this.txtItalianIndex.RecentlyUsed = false;
            this.txtItalianIndex.SelectedText = "";
            this.txtItalianIndex.SelectionLength = 0;
            this.txtItalianIndex.SelectionStart = 0;
            this.txtItalianIndex.TextBoxWidth = 64;
            this.txtItalianIndex.ThemeAware = true;
            // 
            // ULItalian
            // 
            this.ULItalian.AttachedControl = this.tabControlPanel6;
            this.ULItalian.Name = "ULItalian";
            this.ULItalian.Text = "Italian";
            // 
            // tabControlPanel4
            // 
            this.tabControlPanel4.Controls.Add(this.lstFrench);
            this.tabControlPanel4.Controls.Add(this.expandableSplitter6);
            this.tabControlPanel4.Controls.Add(this.explorerBar6);
            this.tabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel4.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel4.Name = "tabControlPanel4";
            this.tabControlPanel4.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel4.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel4.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel4.Style.GradientAngle = 90;
            this.tabControlPanel4.TabIndex = 4;
            this.tabControlPanel4.TabItem = this.ULFrench;
            // 
            // lstFrench
            // 
            this.lstFrench.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstFrench.FormattingEnabled = true;
            this.lstFrench.Location = new System.Drawing.Point(1, 1);
            this.lstFrench.Name = "lstFrench";
            this.lstFrench.Size = new System.Drawing.Size(840, 628);
            this.lstFrench.TabIndex = 9;
            this.lstFrench.SelectedIndexChanged += new System.EventHandler(this.lstFrench_SelectedIndexChanged);
            // 
            // expandableSplitter6
            // 
            this.expandableSplitter6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter6.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter6.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter6.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter6.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter6.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter6.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter6.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter6.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter6.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter6.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter6.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter6.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter6.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter6.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter6.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter6.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter6.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter6.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter6.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter6.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter6.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter6.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter6.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter6.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter6.Name = "expandableSplitter6";
            this.expandableSplitter6.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter6.TabIndex = 8;
            this.expandableSplitter6.TabStop = false;
            // 
            // explorerBar6
            // 
            this.explorerBar6.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar6.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar6.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar6.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar6.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar6.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar6.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar6.GroupImages = null;
            this.explorerBar6.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem7});
            this.explorerBar6.Images = null;
            this.explorerBar6.Location = new System.Drawing.Point(851, 1);
            this.explorerBar6.Name = "explorerBar6";
            this.explorerBar6.Size = new System.Drawing.Size(205, 631);
            this.explorerBar6.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar6.TabIndex = 7;
            this.explorerBar6.ThemeAware = true;
            // 
            // explorerBarGroupItem7
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem7.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem7.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem7.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem7.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem7.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem7.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem7.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem7.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem7.CanCustomize = false;
            this.explorerBarGroupItem7.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem7.Expanded = true;
            this.explorerBarGroupItem7.Name = "explorerBarGroupItem7";
            this.explorerBarGroupItem7.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem7.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem47,
            this.txtFrenchIndex});
            this.explorerBarGroupItem7.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem7.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem7.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem7.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem7.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem7.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem7.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem7.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem7.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem7.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem7.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem7.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem7.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem47
            // 
            this.labelItem47.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem47.Name = "labelItem47";
            this.labelItem47.Text = "Index";
            this.labelItem47.ThemeAware = true;
            // 
            // txtFrenchIndex
            // 
            this.txtFrenchIndex.AlwaysShowCaption = false;
            this.txtFrenchIndex.ControlText = "";
            this.txtFrenchIndex.Enabled = false;
            this.txtFrenchIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtFrenchIndex.Name = "txtFrenchIndex";
            this.txtFrenchIndex.RecentlyUsed = false;
            this.txtFrenchIndex.SelectedText = "";
            this.txtFrenchIndex.SelectionLength = 0;
            this.txtFrenchIndex.SelectionStart = 0;
            this.txtFrenchIndex.TextBoxWidth = 64;
            this.txtFrenchIndex.ThemeAware = true;
            // 
            // ULFrench
            // 
            this.ULFrench.AttachedControl = this.tabControlPanel4;
            this.ULFrench.Name = "ULFrench";
            this.ULFrench.Text = "French";
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.lstDutch);
            this.tabControlPanel3.Controls.Add(this.expandableSplitter5);
            this.tabControlPanel3.Controls.Add(this.explorerBar5);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.ULDutch;
            // 
            // lstDutch
            // 
            this.lstDutch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDutch.FormattingEnabled = true;
            this.lstDutch.Location = new System.Drawing.Point(1, 1);
            this.lstDutch.Name = "lstDutch";
            this.lstDutch.Size = new System.Drawing.Size(840, 628);
            this.lstDutch.TabIndex = 9;
            this.lstDutch.SelectedIndexChanged += new System.EventHandler(this.lstDutch_SelectedIndexChanged);
            // 
            // expandableSplitter5
            // 
            this.expandableSplitter5.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter5.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter5.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter5.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter5.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter5.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter5.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter5.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter5.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter5.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter5.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter5.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter5.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter5.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter5.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter5.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter5.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter5.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter5.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter5.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter5.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter5.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter5.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter5.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter5.Name = "expandableSplitter5";
            this.expandableSplitter5.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter5.TabIndex = 8;
            this.expandableSplitter5.TabStop = false;
            // 
            // explorerBar5
            // 
            this.explorerBar5.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar5.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar5.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar5.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar5.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar5.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar5.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar5.GroupImages = null;
            this.explorerBar5.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem6});
            this.explorerBar5.Images = null;
            this.explorerBar5.Location = new System.Drawing.Point(851, 1);
            this.explorerBar5.Name = "explorerBar5";
            this.explorerBar5.Size = new System.Drawing.Size(205, 631);
            this.explorerBar5.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar5.TabIndex = 7;
            this.explorerBar5.ThemeAware = true;
            // 
            // explorerBarGroupItem6
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem6.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem6.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem6.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem6.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem6.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem6.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem6.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem6.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem6.CanCustomize = false;
            this.explorerBarGroupItem6.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem6.Expanded = true;
            this.explorerBarGroupItem6.Name = "explorerBarGroupItem6";
            this.explorerBarGroupItem6.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem6.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem46,
            this.txtDutchIndex});
            this.explorerBarGroupItem6.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem6.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem6.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem6.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem6.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem6.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem6.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem6.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem6.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem6.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem6.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem6.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem6.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem46
            // 
            this.labelItem46.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem46.Name = "labelItem46";
            this.labelItem46.Text = "Index";
            this.labelItem46.ThemeAware = true;
            // 
            // txtDutchIndex
            // 
            this.txtDutchIndex.AlwaysShowCaption = false;
            this.txtDutchIndex.ControlText = "";
            this.txtDutchIndex.Enabled = false;
            this.txtDutchIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtDutchIndex.Name = "txtDutchIndex";
            this.txtDutchIndex.RecentlyUsed = false;
            this.txtDutchIndex.SelectedText = "";
            this.txtDutchIndex.SelectionLength = 0;
            this.txtDutchIndex.SelectionStart = 0;
            this.txtDutchIndex.TextBoxWidth = 64;
            this.txtDutchIndex.ThemeAware = true;
            // 
            // ULDutch
            // 
            this.ULDutch.AttachedControl = this.tabControlPanel3;
            this.ULDutch.Name = "ULDutch";
            this.ULDutch.Text = "Dutch";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.lstJapanese);
            this.tabControlPanel2.Controls.Add(this.expandableSplitter4);
            this.tabControlPanel2.Controls.Add(this.explorerBar4);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 22);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1057, 633);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.ULJapanese;
            // 
            // lstJapanese
            // 
            this.lstJapanese.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstJapanese.FormattingEnabled = true;
            this.lstJapanese.Location = new System.Drawing.Point(1, 1);
            this.lstJapanese.Name = "lstJapanese";
            this.lstJapanese.Size = new System.Drawing.Size(840, 628);
            this.lstJapanese.TabIndex = 9;
            this.lstJapanese.SelectedIndexChanged += new System.EventHandler(this.lstJapanese_SelectedIndexChanged);
            // 
            // expandableSplitter4
            // 
            this.expandableSplitter4.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter4.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter4.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter4.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter4.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter4.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter4.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter4.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter4.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter4.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter4.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter4.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter4.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter4.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter4.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter4.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter4.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter4.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter4.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter4.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter4.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter4.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter4.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter4.Location = new System.Drawing.Point(841, 1);
            this.expandableSplitter4.Name = "expandableSplitter4";
            this.expandableSplitter4.Size = new System.Drawing.Size(10, 631);
            this.expandableSplitter4.TabIndex = 8;
            this.expandableSplitter4.TabStop = false;
            // 
            // explorerBar4
            // 
            this.explorerBar4.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar4.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar4.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar4.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar4.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar4.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar4.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar4.GroupImages = null;
            this.explorerBar4.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem5});
            this.explorerBar4.Images = null;
            this.explorerBar4.Location = new System.Drawing.Point(851, 1);
            this.explorerBar4.Name = "explorerBar4";
            this.explorerBar4.Size = new System.Drawing.Size(205, 631);
            this.explorerBar4.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar4.TabIndex = 7;
            this.explorerBar4.ThemeAware = true;
            // 
            // explorerBarGroupItem5
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem5.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem5.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem5.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem5.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem5.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem5.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem5.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem5.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem5.CanCustomize = false;
            this.explorerBarGroupItem5.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem5.Expanded = true;
            this.explorerBarGroupItem5.Name = "explorerBarGroupItem5";
            this.explorerBarGroupItem5.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem45,
            this.txtJapaneseIndex});
            this.explorerBarGroupItem5.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem5.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem5.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem5.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem5.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem5.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem5.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem5.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem5.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem5.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem5.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem5.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem5.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem45
            // 
            this.labelItem45.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem45.Name = "labelItem45";
            this.labelItem45.Text = "Index";
            this.labelItem45.ThemeAware = true;
            // 
            // txtJapaneseIndex
            // 
            this.txtJapaneseIndex.AlwaysShowCaption = false;
            this.txtJapaneseIndex.ControlText = "";
            this.txtJapaneseIndex.Enabled = false;
            this.txtJapaneseIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtJapaneseIndex.Name = "txtJapaneseIndex";
            this.txtJapaneseIndex.RecentlyUsed = false;
            this.txtJapaneseIndex.SelectedText = "";
            this.txtJapaneseIndex.SelectionLength = 0;
            this.txtJapaneseIndex.SelectionStart = 0;
            this.txtJapaneseIndex.TextBoxWidth = 64;
            this.txtJapaneseIndex.ThemeAware = true;
            // 
            // ULJapanese
            // 
            this.ULJapanese.AttachedControl = this.tabControlPanel2;
            this.ULJapanese.Name = "ULJapanese";
            this.ULJapanese.Text = "Japanese";
            // 
            // Unicode
            // 
            this.Unicode.AttachedControl = this.UnicodePanel;
            this.Unicode.Name = "Unicode";
            this.Unicode.Text = "Unicode";
            // 
            // IndexPanel
            // 
            this.IndexPanel.Controls.Add(this.itemPanel1);
            this.IndexPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IndexPanel.Location = new System.Drawing.Point(0, 22);
            this.IndexPanel.Name = "IndexPanel";
            this.IndexPanel.Padding = new System.Windows.Forms.Padding(1);
            this.IndexPanel.Size = new System.Drawing.Size(1059, 657);
            this.IndexPanel.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.IndexPanel.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.IndexPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.IndexPanel.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.IndexPanel.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.IndexPanel.Style.GradientAngle = 90;
            this.IndexPanel.TabIndex = 2;
            this.IndexPanel.TabItem = this.Index;
            // 
            // itemPanel1
            // 
            this.itemPanel1.AntiAlias = false;
            // 
            // 
            // 
            this.itemPanel1.BackgroundStyle.BackColor = System.Drawing.Color.White;
            this.itemPanel1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel1.BackgroundStyle.BorderBottomWidth = 1;
            this.itemPanel1.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(157)))), ((int)(((byte)(185)))));
            this.itemPanel1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel1.BackgroundStyle.BorderLeftWidth = 1;
            this.itemPanel1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel1.BackgroundStyle.BorderRightWidth = 1;
            this.itemPanel1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.itemPanel1.BackgroundStyle.BorderTopWidth = 1;
            this.itemPanel1.BackgroundStyle.PaddingBottom = 1;
            this.itemPanel1.BackgroundStyle.PaddingLeft = 1;
            this.itemPanel1.BackgroundStyle.PaddingRight = 1;
            this.itemPanel1.BackgroundStyle.PaddingTop = 1;
            this.itemPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemPanel1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem6,
            this.txtPrimaryMagicConstant,
            this.labelItem7,
            this.txtPrimaryMagic,
            this.labelItem8,
            this.txtTagInfoIndexPointer,
            this.labelItem9,
            this.txtTagInfoIndexOffset,
            this.labelItem10,
            this.txtScenarioID,
            this.labelItem11,
            this.txtGlobalsID,
            this.labelItem12,
            this.txtTagCount,
            this.labelItem13,
            this.txtTags,
            this.labelItem14,
            this.txtSecondaryMagicConstant,
            this.labelItem15,
            this.txtSecondaryMagic,
            this.cmdIndexSave});
            this.itemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemPanel1.Location = new System.Drawing.Point(1, 1);
            this.itemPanel1.Name = "itemPanel1";
            this.itemPanel1.Size = new System.Drawing.Size(1057, 655);
            this.itemPanel1.TabIndex = 0;
            this.itemPanel1.Text = "itemPanel1";
            // 
            // labelItem6
            // 
            this.labelItem6.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem6.Name = "labelItem6";
            this.labelItem6.Text = "Primary Magic Constant";
            // 
            // txtPrimaryMagicConstant
            // 
            this.txtPrimaryMagicConstant.AlwaysShowCaption = false;
            this.txtPrimaryMagicConstant.ControlText = "";
            this.txtPrimaryMagicConstant.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtPrimaryMagicConstant.Name = "txtPrimaryMagicConstant";
            this.txtPrimaryMagicConstant.RecentlyUsed = false;
            this.txtPrimaryMagicConstant.SelectedText = "";
            this.txtPrimaryMagicConstant.SelectionLength = 0;
            this.txtPrimaryMagicConstant.SelectionStart = 0;
            this.txtPrimaryMagicConstant.TextBoxWidth = 64;
            // 
            // labelItem7
            // 
            this.labelItem7.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem7.Name = "labelItem7";
            this.labelItem7.Text = "Primary Magic";
            // 
            // txtPrimaryMagic
            // 
            this.txtPrimaryMagic.AlwaysShowCaption = false;
            this.txtPrimaryMagic.ControlText = "";
            this.txtPrimaryMagic.Enabled = false;
            this.txtPrimaryMagic.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtPrimaryMagic.Name = "txtPrimaryMagic";
            this.txtPrimaryMagic.RecentlyUsed = false;
            this.txtPrimaryMagic.SelectedText = "";
            this.txtPrimaryMagic.SelectionLength = 0;
            this.txtPrimaryMagic.SelectionStart = 0;
            this.txtPrimaryMagic.TextBoxWidth = 64;
            // 
            // labelItem8
            // 
            this.labelItem8.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem8.Name = "labelItem8";
            this.labelItem8.Text = "Tag Info Index Pointer";
            // 
            // txtTagInfoIndexPointer
            // 
            this.txtTagInfoIndexPointer.AlwaysShowCaption = false;
            this.txtTagInfoIndexPointer.ControlText = "";
            this.txtTagInfoIndexPointer.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagInfoIndexPointer.Name = "txtTagInfoIndexPointer";
            this.txtTagInfoIndexPointer.RecentlyUsed = false;
            this.txtTagInfoIndexPointer.SelectedText = "";
            this.txtTagInfoIndexPointer.SelectionLength = 0;
            this.txtTagInfoIndexPointer.SelectionStart = 0;
            this.txtTagInfoIndexPointer.TextBoxWidth = 64;
            // 
            // labelItem9
            // 
            this.labelItem9.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem9.Name = "labelItem9";
            this.labelItem9.Text = "Tag Info Index Offset";
            // 
            // txtTagInfoIndexOffset
            // 
            this.txtTagInfoIndexOffset.AlwaysShowCaption = false;
            this.txtTagInfoIndexOffset.ControlText = "";
            this.txtTagInfoIndexOffset.Enabled = false;
            this.txtTagInfoIndexOffset.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagInfoIndexOffset.Name = "txtTagInfoIndexOffset";
            this.txtTagInfoIndexOffset.RecentlyUsed = false;
            this.txtTagInfoIndexOffset.SelectedText = "";
            this.txtTagInfoIndexOffset.SelectionLength = 0;
            this.txtTagInfoIndexOffset.SelectionStart = 0;
            this.txtTagInfoIndexOffset.TextBoxWidth = 64;
            // 
            // labelItem10
            // 
            this.labelItem10.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem10.Name = "labelItem10";
            this.labelItem10.Text = "Scenario ID";
            // 
            // txtScenarioID
            // 
            this.txtScenarioID.AlwaysShowCaption = false;
            this.txtScenarioID.ControlText = "";
            this.txtScenarioID.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtScenarioID.Name = "txtScenarioID";
            this.txtScenarioID.RecentlyUsed = false;
            this.txtScenarioID.SelectedText = "";
            this.txtScenarioID.SelectionLength = 0;
            this.txtScenarioID.SelectionStart = 0;
            this.txtScenarioID.TextBoxWidth = 64;
            // 
            // labelItem11
            // 
            this.labelItem11.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem11.Name = "labelItem11";
            this.labelItem11.Text = "Globals ID";
            // 
            // txtGlobalsID
            // 
            this.txtGlobalsID.AlwaysShowCaption = false;
            this.txtGlobalsID.ControlText = "";
            this.txtGlobalsID.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtGlobalsID.Name = "txtGlobalsID";
            this.txtGlobalsID.RecentlyUsed = false;
            this.txtGlobalsID.SelectedText = "";
            this.txtGlobalsID.SelectionLength = 0;
            this.txtGlobalsID.SelectionStart = 0;
            this.txtGlobalsID.TextBoxWidth = 64;
            // 
            // labelItem12
            // 
            this.labelItem12.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem12.Name = "labelItem12";
            this.labelItem12.Text = "Tag Count";
            // 
            // txtTagCount
            // 
            this.txtTagCount.AlwaysShowCaption = false;
            this.txtTagCount.ControlText = "";
            this.txtTagCount.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTagCount.Name = "txtTagCount";
            this.txtTagCount.RecentlyUsed = false;
            this.txtTagCount.SelectedText = "";
            this.txtTagCount.SelectionLength = 0;
            this.txtTagCount.SelectionStart = 0;
            this.txtTagCount.TextBoxWidth = 64;
            // 
            // labelItem13
            // 
            this.labelItem13.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem13.Name = "labelItem13";
            this.labelItem13.Text = "Tags";
            // 
            // txtTags
            // 
            this.txtTags.AlwaysShowCaption = false;
            this.txtTags.ControlText = "";
            this.txtTags.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtTags.Name = "txtTags";
            this.txtTags.RecentlyUsed = false;
            this.txtTags.SelectedText = "";
            this.txtTags.SelectionLength = 0;
            this.txtTags.SelectionStart = 0;
            this.txtTags.TextBoxWidth = 64;
            // 
            // labelItem14
            // 
            this.labelItem14.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem14.Name = "labelItem14";
            this.labelItem14.Text = "Secondary Magic Constant";
            // 
            // txtSecondaryMagicConstant
            // 
            this.txtSecondaryMagicConstant.AlwaysShowCaption = false;
            this.txtSecondaryMagicConstant.ControlText = "";
            this.txtSecondaryMagicConstant.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtSecondaryMagicConstant.Name = "txtSecondaryMagicConstant";
            this.txtSecondaryMagicConstant.RecentlyUsed = false;
            this.txtSecondaryMagicConstant.SelectedText = "";
            this.txtSecondaryMagicConstant.SelectionLength = 0;
            this.txtSecondaryMagicConstant.SelectionStart = 0;
            this.txtSecondaryMagicConstant.TextBoxWidth = 64;
            // 
            // labelItem15
            // 
            this.labelItem15.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem15.Name = "labelItem15";
            this.labelItem15.Text = "Secondary Magic";
            // 
            // txtSecondaryMagic
            // 
            this.txtSecondaryMagic.AlwaysShowCaption = false;
            this.txtSecondaryMagic.ControlText = "";
            this.txtSecondaryMagic.Enabled = false;
            this.txtSecondaryMagic.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtSecondaryMagic.Name = "txtSecondaryMagic";
            this.txtSecondaryMagic.RecentlyUsed = false;
            this.txtSecondaryMagic.SelectedText = "";
            this.txtSecondaryMagic.SelectionLength = 0;
            this.txtSecondaryMagic.SelectionStart = 0;
            this.txtSecondaryMagic.TextBoxWidth = 64;
            // 
            // cmdIndexSave
            // 
            this.cmdIndexSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdIndexSave.Image = global::Ceiling_Cat.Properties.Resources.save_16;
            this.cmdIndexSave.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right;
            this.cmdIndexSave.Name = "cmdIndexSave";
            this.cmdIndexSave.Text = "Save";
            this.cmdIndexSave.Click += new System.EventHandler(this.cmdIndexSave_Click);
            // 
            // Index
            // 
            this.Index.AttachedControl = this.IndexPanel;
            this.Index.Name = "Index";
            this.Index.Text = "Index";
            // 
            // StringIDsPanel
            // 
            this.StringIDsPanel.Controls.Add(this.lstSIDs);
            this.StringIDsPanel.Controls.Add(this.expandableSplitter2);
            this.StringIDsPanel.Controls.Add(this.explorerBar2);
            this.StringIDsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StringIDsPanel.Location = new System.Drawing.Point(0, 22);
            this.StringIDsPanel.Name = "StringIDsPanel";
            this.StringIDsPanel.Padding = new System.Windows.Forms.Padding(1);
            this.StringIDsPanel.Size = new System.Drawing.Size(1059, 657);
            this.StringIDsPanel.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(254)))));
            this.StringIDsPanel.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(188)))), ((int)(((byte)(227)))));
            this.StringIDsPanel.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.StringIDsPanel.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(165)))), ((int)(((byte)(199)))));
            this.StringIDsPanel.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.StringIDsPanel.Style.GradientAngle = 90;
            this.StringIDsPanel.TabIndex = 3;
            this.StringIDsPanel.TabItem = this.StringIDs;
            // 
            // lstSIDs
            // 
            this.lstSIDs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSIDs.FormattingEnabled = true;
            this.lstSIDs.Location = new System.Drawing.Point(1, 1);
            this.lstSIDs.Name = "lstSIDs";
            this.lstSIDs.Size = new System.Drawing.Size(842, 654);
            this.lstSIDs.TabIndex = 3;
            this.lstSIDs.SelectedIndexChanged += new System.EventHandler(this.lstSIDs_SelectedIndexChanged);
            // 
            // expandableSplitter2
            // 
            this.expandableSplitter2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter2.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter2.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.expandableSplitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.expandableSplitter2.ExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter2.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter2.ExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter2.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter2.GripDarkColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter2.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter2.GripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter2.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter2.HotBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(142)))), ((int)(((byte)(75)))));
            this.expandableSplitter2.HotBackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(207)))), ((int)(((byte)(139)))));
            this.expandableSplitter2.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2;
            this.expandableSplitter2.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground;
            this.expandableSplitter2.HotExpandFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter2.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter2.HotExpandLineColor = System.Drawing.SystemColors.ControlText;
            this.expandableSplitter2.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText;
            this.expandableSplitter2.HotGripDarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(124)))), ((int)(((byte)(124)))), ((int)(((byte)(148)))));
            this.expandableSplitter2.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.expandableSplitter2.HotGripLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(244)))), ((int)(((byte)(250)))));
            this.expandableSplitter2.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.expandableSplitter2.Location = new System.Drawing.Point(843, 1);
            this.expandableSplitter2.Name = "expandableSplitter2";
            this.expandableSplitter2.Size = new System.Drawing.Size(10, 655);
            this.expandableSplitter2.TabIndex = 2;
            this.expandableSplitter2.TabStop = false;
            // 
            // explorerBar2
            // 
            this.explorerBar2.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            this.explorerBar2.BackColor = System.Drawing.SystemColors.Control;
            // 
            // 
            // 
            this.explorerBar2.BackStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground2;
            this.explorerBar2.BackStyle.BackColorGradientAngle = 90;
            this.explorerBar2.BackStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ExplorerBarBackground;
            this.explorerBar2.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBar2.Dock = System.Windows.Forms.DockStyle.Right;
            this.explorerBar2.GroupImages = null;
            this.explorerBar2.Groups.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.explorerBarGroupItem3});
            this.explorerBar2.Images = null;
            this.explorerBar2.Location = new System.Drawing.Point(853, 1);
            this.explorerBar2.Name = "explorerBar2";
            this.explorerBar2.Size = new System.Drawing.Size(205, 655);
            this.explorerBar2.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBar2.TabIndex = 1;
            this.explorerBar2.ThemeAware = true;
            // 
            // explorerBarGroupItem3
            // 
            // 
            // 
            // 
            this.explorerBarGroupItem3.BackStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(241)))), ((int)(((byte)(245)))));
            this.explorerBarGroupItem3.BackStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem3.BackStyle.BorderBottomWidth = 1;
            this.explorerBarGroupItem3.BackStyle.BorderColor = System.Drawing.Color.White;
            this.explorerBarGroupItem3.BackStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem3.BackStyle.BorderLeftWidth = 1;
            this.explorerBarGroupItem3.BackStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.explorerBarGroupItem3.BackStyle.BorderRightWidth = 1;
            this.explorerBarGroupItem3.CanCustomize = false;
            this.explorerBarGroupItem3.Cursor = System.Windows.Forms.Cursors.Default;
            this.explorerBarGroupItem3.Expanded = true;
            this.explorerBarGroupItem3.Name = "explorerBarGroupItem3";
            this.explorerBarGroupItem3.StockStyle = DevComponents.DotNetBar.eExplorerBarStockStyle.SystemColors;
            this.explorerBarGroupItem3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.labelItem3,
            this.txtSIDIndex,
            this.labelItem44,
            this.txtSIDName,
            this.cmdSIDSave});
            this.explorerBarGroupItem3.Text = "Information";
            // 
            // 
            // 
            this.explorerBarGroupItem3.TitleHotStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem3.TitleHotStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem3.TitleHotStyle.CornerDiameter = 3;
            this.explorerBarGroupItem3.TitleHotStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem3.TitleHotStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem3.TitleHotStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            // 
            // 
            // 
            this.explorerBarGroupItem3.TitleStyle.BackColor = System.Drawing.Color.White;
            this.explorerBarGroupItem3.TitleStyle.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(215)))), ((int)(((byte)(224)))));
            this.explorerBarGroupItem3.TitleStyle.CornerDiameter = 3;
            this.explorerBarGroupItem3.TitleStyle.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem3.TitleStyle.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Rounded;
            this.explorerBarGroupItem3.TitleStyle.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            // 
            // labelItem3
            // 
            this.labelItem3.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem3.Name = "labelItem3";
            this.labelItem3.Text = "Index";
            this.labelItem3.ThemeAware = true;
            // 
            // txtSIDIndex
            // 
            this.txtSIDIndex.AlwaysShowCaption = false;
            this.txtSIDIndex.ControlText = "";
            this.txtSIDIndex.Enabled = false;
            this.txtSIDIndex.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtSIDIndex.Name = "txtSIDIndex";
            this.txtSIDIndex.RecentlyUsed = false;
            this.txtSIDIndex.SelectedText = "";
            this.txtSIDIndex.SelectionLength = 0;
            this.txtSIDIndex.SelectionStart = 0;
            this.txtSIDIndex.TextBoxWidth = 64;
            this.txtSIDIndex.ThemeAware = true;
            // 
            // labelItem44
            // 
            this.labelItem44.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem44.Name = "labelItem44";
            this.labelItem44.Text = "Name";
            this.labelItem44.ThemeAware = true;
            // 
            // txtSIDName
            // 
            this.txtSIDName.AlwaysShowCaption = false;
            this.txtSIDName.ControlText = "";
            this.txtSIDName.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.txtSIDName.Name = "txtSIDName";
            this.txtSIDName.RecentlyUsed = false;
            this.txtSIDName.SelectedText = "";
            this.txtSIDName.SelectionLength = 0;
            this.txtSIDName.SelectionStart = 0;
            this.txtSIDName.Text = "textBoxItem1";
            this.txtSIDName.TextBoxWidth = 64;
            this.txtSIDName.ThemeAware = true;
            // 
            // cmdSIDSave
            // 
            this.cmdSIDSave.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.cmdSIDSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSIDSave.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(61)))), ((int)(((byte)(61)))));
            this.cmdSIDSave.HotFontUnderline = true;
            this.cmdSIDSave.HotForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.cmdSIDSave.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.None;
            this.cmdSIDSave.Image = global::Ceiling_Cat.Properties.Resources.save_16;
            this.cmdSIDSave.Name = "cmdSIDSave";
            this.cmdSIDSave.Text = "Save";
            this.cmdSIDSave.Click += new System.EventHandler(this.cmdSIDSave_Click);
            // 
            // StringIDs
            // 
            this.StringIDs.AttachedControl = this.StringIDsPanel;
            this.StringIDs.Name = "StringIDs";
            this.StringIDs.Text = "String ID Table";
            // 
            // bar1
            // 
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.lblStatus,
            this.probar});
            this.bar1.Location = new System.Drawing.Point(0, 724);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(1059, 19);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2003;
            this.bar1.TabIndex = 1;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // lblStatus
            // 
            this.lblStatus.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Text = "Ready.";
            // 
            // probar
            // 
            this.probar.ChunkGradientAngle = 0F;
            this.probar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far;
            this.probar.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways;
            this.probar.Name = "probar";
            this.probar.RecentlyUsed = false;
            // 
            // labelItem28
            // 
            this.labelItem28.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem28.Name = "labelItem28";
            this.labelItem28.Text = "labelItem26";
            // 
            // labelItem29
            // 
            this.labelItem29.BorderType = DevComponents.DotNetBar.eBorderType.None;
            this.labelItem29.Name = "labelItem29";
            this.labelItem29.Text = "labelItem26";
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.CanCustomize = false;
            this.ribbonControl1.CaptionVisible = true;
            this.ribbonControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl1.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.ribbonControl1.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.office2007StartButton1,
            this.buttonItem1,
            this.cmdResign});
            this.ribbonControl1.Size = new System.Drawing.Size(1059, 45);
            this.ribbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.ribbonControl1.TabGroupHeight = 14;
            this.ribbonControl1.TabIndex = 2;
            this.ribbonControl1.Text = "ribbonControl1";
            // 
            // office2007StartButton1
            // 
            this.office2007StartButton1.AutoExpandOnClick = true;
            this.office2007StartButton1.CanCustomize = false;
            this.office2007StartButton1.Enabled = false;
            this.office2007StartButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.office2007StartButton1.Image = global::Ceiling_Cat.Properties.Resources.disc_media_32;
            this.office2007StartButton1.ImagePaddingHorizontal = 2;
            this.office2007StartButton1.ImagePaddingVertical = 2;
            this.office2007StartButton1.Name = "office2007StartButton1";
            this.office2007StartButton1.ShowSubItems = false;
            this.office2007StartButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer1});
            this.office2007StartButton1.Text = "&File";
            // 
            // itemContainer1
            // 
            // 
            // 
            // 
            this.itemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer1.MinimumSize = new System.Drawing.Size(0, 0);
            this.itemContainer1.Name = "itemContainer1";
            this.itemContainer1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer2,
            this.itemContainer5});
            // 
            // itemContainer2
            // 
            // 
            // 
            // 
            this.itemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer";
            this.itemContainer2.ItemSpacing = 0;
            this.itemContainer2.MinimumSize = new System.Drawing.Size(0, 0);
            this.itemContainer2.Name = "itemContainer2";
            this.itemContainer2.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemContainer3,
            this.itemContainer4});
            // 
            // itemContainer3
            // 
            // 
            // 
            // 
            this.itemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer";
            this.itemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer3.MinimumSize = new System.Drawing.Size(120, 0);
            this.itemContainer3.Name = "itemContainer3";
            this.itemContainer3.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem7});
            // 
            // buttonItem7
            // 
            this.buttonItem7.BeginGroup = true;
            this.buttonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem7.Name = "buttonItem7";
            this.buttonItem7.SubItemsExpandWidth = 24;
            this.buttonItem7.Text = "&Close";
            // 
            // itemContainer4
            // 
            // 
            // 
            // 
            this.itemContainer4.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer";
            this.itemContainer4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemContainer4.MinimumSize = new System.Drawing.Size(180, 0);
            this.itemContainer4.Name = "itemContainer4";
            // 
            // itemContainer5
            // 
            // 
            // 
            // 
            this.itemContainer5.BackgroundStyle.Class = "RibbonFileMenuBottomContainer";
            this.itemContainer5.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right;
            this.itemContainer5.MinimumSize = new System.Drawing.Size(0, 0);
            this.itemContainer5.Name = "itemContainer5";
            this.itemContainer5.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItem12,
            this.buttonItem13});
            // 
            // buttonItem12
            // 
            this.buttonItem12.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem12.Name = "buttonItem12";
            this.buttonItem12.SubItemsExpandWidth = 24;
            this.buttonItem12.Text = "Opt&ions";
            // 
            // buttonItem13
            // 
            this.buttonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.buttonItem13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonItem13.Name = "buttonItem13";
            this.buttonItem13.SubItemsExpandWidth = 24;
            this.buttonItem13.Text = "E&xit";
            // 
            // buttonItem1
            // 
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.cmdAddTagFromFile,
            this.cmdExtractAllTags});
            this.buttonItem1.Text = "Tags";
            // 
            // cmdAddTagFromFile
            // 
            this.cmdAddTagFromFile.Name = "cmdAddTagFromFile";
            this.cmdAddTagFromFile.Text = "Add From File...";
            this.cmdAddTagFromFile.Click += new System.EventHandler(this.cmdAddTagFromFile_Click);
            // 
            // cmdExtractAllTags
            // 
            this.cmdExtractAllTags.Name = "cmdExtractAllTags";
            this.cmdExtractAllTags.Text = "Extract All...";
            this.cmdExtractAllTags.Click += new System.EventHandler(this.cmdExtractAllTags_Click);
            // 
            // cmdResign
            // 
            this.cmdResign.Name = "cmdResign";
            this.cmdResign.Text = "Resign";
            this.cmdResign.Click += new System.EventHandler(this.cmdResign_Click);
            // 
            // RawDataButton
            // 
            this.RawDataButton.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.RawDataButton.Checked = true;
            this.RawDataButton.Image = global::Ceiling_Cat.Properties.Resources.disc_media_32;
            this.RawDataButton.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.RawDataButton.Name = "RawDataButton";
            this.RawDataButton.OptionGroup = "navBar";
            this.RawDataButton.Text = "Raw Data";
            // 
            // TagInfoPaneButton
            // 
            this.TagInfoPaneButton.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.TagInfoPaneButton.Image = global::Ceiling_Cat.Properties.Resources.applications_32;
            this.TagInfoPaneButton.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.TagInfoPaneButton.Name = "TagInfoPaneButton";
            this.TagInfoPaneButton.OptionGroup = "navBar";
            this.TagInfoPaneButton.Text = "Information";
            // 
            // MapEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 743);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.ribbonControl1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MapEditor";
            this.ShowInTaskbar = false;
            this.Text = "Map";
            ((System.ComponentModel.ISupportInitialize)(this.tabs)).EndInit();
            this.tabs.ResumeLayout(false);
            this.TagsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TagsBar)).EndInit();
            this.HeaderPanel.ResumeLayout(false);
            this.UnicodePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UnicodeLanTabs)).EndInit();
            this.UnicodeLanTabs.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar3)).EndInit();
            this.tabControlPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar7)).EndInit();
            this.tabControlPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar11)).EndInit();
            this.tabControlPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar10)).EndInit();
            this.tabControlPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar9)).EndInit();
            this.tabControlPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar8)).EndInit();
            this.tabControlPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar6)).EndInit();
            this.tabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar5)).EndInit();
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar4)).EndInit();
            this.IndexPanel.ResumeLayout(false);
            this.StringIDsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.TabControl tabs;
        private DevComponents.DotNetBar.TabControlPanel HeaderPanel;
        private DevComponents.DotNetBar.TabItem Header;
        private DevComponents.DotNetBar.TabControlPanel UnicodePanel;
        private DevComponents.DotNetBar.TabItem Unicode;
        private DevComponents.DotNetBar.TabControlPanel StringIDsPanel;
        private DevComponents.DotNetBar.TabItem StringIDs;
        private DevComponents.DotNetBar.TabControlPanel IndexPanel;
        private DevComponents.DotNetBar.TabItem Index;
        private DevComponents.DotNetBar.TabControlPanel TagsPanel;
        private DevComponents.DotNetBar.TabItem Tags;
        private DevComponents.DotNetBar.ButtonItem TagInfoPaneButton;
        private System.Windows.Forms.TreeView trvTags;
        private DevComponents.DotNetBar.ButtonItem RawDataButton;
        private DevComponents.DotNetBar.ExplorerBar TagsBar;
        private DevComponents.DotNetBar.ExplorerBarGroupItem RawBar;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter1;
        private DevComponents.DotNetBar.Bar bar1;
        private DevComponents.DotNetBar.LabelItem lblStatus;
        private DevComponents.DotNetBar.ProgressBarItem probar;
        private DevComponents.DotNetBar.ExplorerBar explorerBar2;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem3;
        private DevComponents.DotNetBar.LabelItem labelItem3;
        private DevComponents.DotNetBar.TextBoxItem txtSIDIndex;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter2;
        private System.Windows.Forms.ListBox lstSIDs;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem1;
        private DevComponents.DotNetBar.LabelItem labelItem1;
        private DevComponents.DotNetBar.TextBoxItem txtTagOffset;
        private DevComponents.DotNetBar.LabelItem labelItem2;
        private DevComponents.DotNetBar.TextBoxItem txtTagClass;
        private DevComponents.DotNetBar.LabelItem labelItem4;
        private DevComponents.DotNetBar.TextBoxItem txtTagPath;
        private DevComponents.DotNetBar.ButtonItem cmdTagInfoSave;
        private DevComponents.DotNetBar.ItemPanel itemPanel1;
        private DevComponents.DotNetBar.LabelItem labelItem6;
        private DevComponents.DotNetBar.TextBoxItem txtPrimaryMagicConstant;
        private DevComponents.DotNetBar.LabelItem labelItem7;
        private DevComponents.DotNetBar.TextBoxItem txtPrimaryMagic;
        private DevComponents.DotNetBar.LabelItem labelItem8;
        private DevComponents.DotNetBar.TextBoxItem txtTagInfoIndexPointer;
        private DevComponents.DotNetBar.LabelItem labelItem9;
        private DevComponents.DotNetBar.TextBoxItem txtTagInfoIndexOffset;
        private DevComponents.DotNetBar.LabelItem labelItem10;
        private DevComponents.DotNetBar.TextBoxItem txtScenarioID;
        private DevComponents.DotNetBar.LabelItem labelItem11;
        private DevComponents.DotNetBar.TextBoxItem txtGlobalsID;
        private DevComponents.DotNetBar.LabelItem labelItem12;
        private DevComponents.DotNetBar.TextBoxItem txtTagCount;
        private DevComponents.DotNetBar.LabelItem labelItem13;
        private DevComponents.DotNetBar.TextBoxItem txtTags;
        private DevComponents.DotNetBar.LabelItem labelItem14;
        private DevComponents.DotNetBar.TextBoxItem txtSecondaryMagicConstant;
        private DevComponents.DotNetBar.LabelItem labelItem15;
        private DevComponents.DotNetBar.TextBoxItem txtSecondaryMagic;
        private DevComponents.DotNetBar.ButtonItem cmdIndexSave;
        private DevComponents.DotNetBar.ItemPanel itemPanel2;
        private DevComponents.DotNetBar.LabelItem lblHead;
        private DevComponents.DotNetBar.TextBoxItem txtHead;
        private DevComponents.DotNetBar.LabelItem labelItem17;
        private DevComponents.DotNetBar.TextBoxItem txtVersion;
        private DevComponents.DotNetBar.LabelItem labelItem18;
        private DevComponents.DotNetBar.TextBoxItem txtFileSize;
        private DevComponents.DotNetBar.LabelItem labelItem19;
        private DevComponents.DotNetBar.TextBoxItem txtIndexOffset;
        private DevComponents.DotNetBar.TextBoxItem txtIndexSize;
        private DevComponents.DotNetBar.LabelItem labelItem21;
        private DevComponents.DotNetBar.TextBoxItem txtMetaStart;
        private DevComponents.DotNetBar.LabelItem labelItem22;
        private DevComponents.DotNetBar.TextBoxItem txtMetaSize;
        private DevComponents.DotNetBar.LabelItem labelItem23;
        private DevComponents.DotNetBar.TextBoxItem txtNonRawSize;
        private DevComponents.DotNetBar.LabelItem labelItem24;
        private DevComponents.DotNetBar.TextBoxItem txtOrigin;
        private DevComponents.DotNetBar.LabelItem labelItem25;
        private DevComponents.DotNetBar.TextBoxItem txtBuildDate;
        private DevComponents.DotNetBar.ButtonItem cmdHeaderSave;
        private DevComponents.DotNetBar.LabelItem labelItem20;
        private DevComponents.DotNetBar.LabelItem labelItem26;
        private DevComponents.DotNetBar.ComboBoxItem cboMapType;
        private DevComponents.DotNetBar.LabelItem labelItem27;
        private DevComponents.DotNetBar.TextBoxItem txtStrangeFileStringsSize;
        private DevComponents.DotNetBar.LabelItem labelItem30;
        private DevComponents.DotNetBar.LabelItem labelItem28;
        private DevComponents.DotNetBar.LabelItem labelItem29;
        private DevComponents.DotNetBar.TextBoxItem txtStrangeFileStringsOffset;
        private DevComponents.DotNetBar.LabelItem labelItem31;
        private DevComponents.DotNetBar.TextBoxItem txtStringIDs128Offset;
        private DevComponents.DotNetBar.LabelItem labelItem32;
        private DevComponents.DotNetBar.TextBoxItem txtStringIDCount;
        private DevComponents.DotNetBar.LabelItem labelItem33;
        private DevComponents.DotNetBar.TextBoxItem txtStringIDsSize;
        private DevComponents.DotNetBar.LabelItem labelItem34;
        private DevComponents.DotNetBar.TextBoxItem txtStringIDsIndex;
        private DevComponents.DotNetBar.LabelItem labelItem35;
        private DevComponents.DotNetBar.TextBoxItem txtStringIDsOffset;
        private DevComponents.DotNetBar.LabelItem labelItem36;
        private DevComponents.DotNetBar.TextBoxItem txtInternalName;
        private DevComponents.DotNetBar.LabelItem labelItem37;
        private DevComponents.DotNetBar.TextBoxItem txtScenarioName;
        private DevComponents.DotNetBar.LabelItem labelItem38;
        private DevComponents.DotNetBar.TextBoxItem txtFileTableCount;
        private DevComponents.DotNetBar.LabelItem labelItem39;
        private DevComponents.DotNetBar.TextBoxItem txtFileTableOffset;
        private DevComponents.DotNetBar.LabelItem labelItem40;
        private DevComponents.DotNetBar.TextBoxItem txtFileTableSize;
        private DevComponents.DotNetBar.LabelItem labelItem41;
        private DevComponents.DotNetBar.TextBoxItem txtFileTableIndexOffset;
        private DevComponents.DotNetBar.LabelItem labelItem42;
        private DevComponents.DotNetBar.TextBoxItem txtChecksum;
        private DevComponents.DotNetBar.LabelItem labelItem43;
        private DevComponents.DotNetBar.TextBoxItem txtFoot;
        private DevComponents.Editors.ComboItem cboItemSinglePlayerMap;
        private DevComponents.Editors.ComboItem cboItemMultiplayerMap;
        private DevComponents.Editors.ComboItem cboItemMainMenu;
        private DevComponents.Editors.ComboItem cboItemShared;
        private DevComponents.Editors.ComboItem cboItemSinglePlayerShared;
        private DevComponents.DotNetBar.LabelItem labelItem44;
        private DevComponents.DotNetBar.TextBoxItem txtSIDName;
        private DevComponents.DotNetBar.ButtonItem cmdSIDSave;
        private DevComponents.DotNetBar.LabelItem lblHead2;
        private DevComponents.DotNetBar.RibbonControl ribbonControl1;
        private DevComponents.DotNetBar.Office2007StartButton office2007StartButton1;
        private DevComponents.DotNetBar.ItemContainer itemContainer1;
        private DevComponents.DotNetBar.ItemContainer itemContainer2;
        private DevComponents.DotNetBar.ItemContainer itemContainer3;
        private DevComponents.DotNetBar.ButtonItem buttonItem7;
        private DevComponents.DotNetBar.ItemContainer itemContainer4;
        private DevComponents.DotNetBar.ItemContainer itemContainer5;
        private DevComponents.DotNetBar.ButtonItem buttonItem12;
        private DevComponents.DotNetBar.ButtonItem buttonItem13;
        private DevComponents.DotNetBar.ButtonItem cmdResign;
        private DevComponents.DotNetBar.TabControl UnicodeLanTabs;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private System.Windows.Forms.ListBox lstEnglish;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter3;
        private DevComponents.DotNetBar.ExplorerBar explorerBar3;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem4;
        private DevComponents.DotNetBar.LabelItem labelItem5;
        private DevComponents.DotNetBar.TextBoxItem txtEnglishIndex;
        private DevComponents.DotNetBar.TabItem ULEnglish;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel7;
        private System.Windows.Forms.ListBox lstKorean;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter9;
        private DevComponents.DotNetBar.ExplorerBar explorerBar9;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem10;
        private DevComponents.DotNetBar.LabelItem labelItem50;
        private DevComponents.DotNetBar.TextBoxItem txtKoreanIndex;
        private DevComponents.DotNetBar.TabItem ULKorean;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel6;
        private System.Windows.Forms.ListBox lstItalian;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter8;
        private DevComponents.DotNetBar.ExplorerBar explorerBar8;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem9;
        private DevComponents.DotNetBar.LabelItem labelItem49;
        private DevComponents.DotNetBar.TextBoxItem txtItalianIndex;
        private DevComponents.DotNetBar.TabItem ULItalian;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel5;
        private System.Windows.Forms.ListBox lstSpanish;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter7;
        private DevComponents.DotNetBar.ExplorerBar explorerBar7;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem8;
        private DevComponents.DotNetBar.LabelItem labelItem48;
        private DevComponents.DotNetBar.TextBoxItem txtSpanishIndex;
        private DevComponents.DotNetBar.TabItem ULSpanish;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel4;
        private System.Windows.Forms.ListBox lstFrench;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter6;
        private DevComponents.DotNetBar.ExplorerBar explorerBar6;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem7;
        private DevComponents.DotNetBar.LabelItem labelItem47;
        private DevComponents.DotNetBar.TextBoxItem txtFrenchIndex;
        private DevComponents.DotNetBar.TabItem ULFrench;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel9;
        private System.Windows.Forms.ListBox lstPortuguese;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter11;
        private DevComponents.DotNetBar.ExplorerBar explorerBar11;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem12;
        private DevComponents.DotNetBar.LabelItem labelItem52;
        private DevComponents.DotNetBar.TextBoxItem txtPortugeseIndex;
        private DevComponents.DotNetBar.TabItem ULPortuguese;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel8;
        private System.Windows.Forms.ListBox lstChinese;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter10;
        private DevComponents.DotNetBar.ExplorerBar explorerBar10;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem11;
        private DevComponents.DotNetBar.LabelItem labelItem51;
        private DevComponents.DotNetBar.TextBoxItem txtChineseIndex;
        private DevComponents.DotNetBar.TabItem ULChinese;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private System.Windows.Forms.ListBox lstDutch;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter5;
        private DevComponents.DotNetBar.ExplorerBar explorerBar5;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem6;
        private DevComponents.DotNetBar.LabelItem labelItem46;
        private DevComponents.DotNetBar.TextBoxItem txtDutchIndex;
        private DevComponents.DotNetBar.TabItem ULDutch;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private System.Windows.Forms.ListBox lstJapanese;
        private DevComponents.DotNetBar.ExpandableSplitter expandableSplitter4;
        private DevComponents.DotNetBar.ExplorerBar explorerBar4;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem5;
        private DevComponents.DotNetBar.LabelItem labelItem45;
        private DevComponents.DotNetBar.TextBoxItem txtJapaneseIndex;
        private DevComponents.DotNetBar.TabItem ULJapanese;
        private DevComponents.DotNetBar.ExplorerBarGroupItem explorerBarGroupItem13;
        private DevComponents.DotNetBar.ButtonItem cmdMetaExtract;
        private DevComponents.DotNetBar.ButtonItem cmdMetaInject;
        private DevComponents.DotNetBar.ButtonItem cmdMetaRemove;
        private DevComponents.DotNetBar.LabelItem labelItem16;
        private DevComponents.DotNetBar.TextBoxItem txtTagSize;
        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonItem cmdAddTagFromFile;
        private DevComponents.DotNetBar.ButtonItem cmdExtractAllTags;
        private DevComponents.DotNetBar.ButtonItem cmdMetaDuplicate;
    }
}